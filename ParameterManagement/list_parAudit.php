<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">List Parameter Audit</h3>
		</header>
		<center>
			<fieldset>
				<input type="button" class="span3" value="Tipe Pengawasan" Onclick="window.open('main_page.php?method=par_audit_type', '_self');">
				<input type="button" class="span3" value="Sub Tipe Pengawasan" Onclick="window.open('main_page.php?method=par_subaudit_type', '_self');">
			</fieldset>
			<fieldset>
				<input type="button" class="span3" value="Kelompok Temuan" Onclick="window.open('main_page.php?method=par_temuan_type', '_self');">
				<input type="button" class="span3" value="Sub Kode Temuan" Onclick="window.open('main_page.php?method=par_sub_type', '_self');">
			</fieldset>
			<fieldset>
				<input type="button" class="span3" value="Sub-Sub Kode Temuan" Onclick="window.open('main_page.php?method=par_jenis_temuan', '_self');">
				<input type="button" class="span3" value="Kelompok Rekomendasi" Onclick="window.open('main_page.php?method=par_kode_rek', '_self');">
			</fieldset>
			<fieldset>
				<input type="button" class="span3" value="Kelompok Penyebab" Onclick="window.open('main_page.php?method=par_kelompok_penyebab', '_self');">
				<input type="button" class="span3" value="Sub Kode Penyebab" Onclick="window.open('main_page.php?method=par_kode_penyebab', '_self');">
				
			</fieldset>
			<fieldset>
				<input type="button" class="span3" value="Sub Sub Kode Penyebab" Onclick="window.open('main_page.php?method=par_sub_sub_penyebab', '_self');">
				<input type="button" class="span3" value="Aspek Program Audit" Onclick="window.open('main_page.php?method=par_aspek', '_self');">
			</fieldset>
			<fieldset>
				<input type="button" class="span3" value="Kategori Referensi Audit" Onclick="window.open('main_page.php?method=par_kategori_ref', '_self');">
				<input type="button" class="span3" value="Status Tidak Lanjut" Onclick="window.open('main_page.php?method=par_status_tl', '_self');">
			</fieldset>
			<fieldset>
				<input type="button" class="span3" value="Hari Libur" Onclick="window.open('main_page.php?method=par_holiday', '_self');">
			</fieldset>
		</center>
	</article>
</section>