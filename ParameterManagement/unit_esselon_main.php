<?
include_once "_includes/classes/param_class.php";

$params = new param ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=par_esselon";
$acc_page_request = "unit_esselon_acc.php";
$list_page_request = "param_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Unit Eselon I", "Nama Pejabat");
$gridDetail = array ("esselon_name", "esselon_name_pic");
$gridWidth = array ("35", "50");

$key_by = array ("Unit Eselon I", "Nama Pejabat");
$key_field = array ("esselon_name", "esselon_name_pic");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Unit Eselon I";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->esselon_data_viewlist ( $fdata_id );
		$page_title = "Ubah Unit Eselon I";
		break;
	case "postadd" :
		$fname = $comfunc->replacetext ( $_POST ["name"] );
        $fpic = $comfunc->replacetext ( $_POST ["pic"] );
		if ($fname != "") {
			$rs_nama = $params->cek_nama_esselon ( $fname );
			$arr_nama = $rs_nama->FetchRow ();
			$fdata_id = $arr_nama ['esselon_id'];
			$del_st = $arr_nama ['esselon_del_st'];
			if ($fdata_id == "") {
				$params->esselon_add ( $fname, $fpic );
				$comfunc->js_alert_act ( 3 );
			} else {
				if ($del_st == "0") {
					$params->update_esselon_del ( $fdata_id );
					$comfunc->js_alert_act ( 3 );
				} else {
					$comfunc->js_alert_act ( 4, $fname );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
        $fpic = $comfunc->replacetext ( $_POST ["pic"] );
		if ($fname != "") {
			$rs_nama = $params->cek_nama_esselon ( $fname, $fdata_id );
			$arr_nama = $rs_nama->FetchRow ();
			$fesselon_id = $arr_nama ['esselon_id'];
			$del_st = $arr_nama ['esselon_del_st'];
			if ($fesselon_id == "") {
				$params->esselon_edit ( $fdata_id, $fname, $fpic );
				$comfunc->js_alert_act ( 1 );
			} else {
				if ($del_st == "0") {
					$params->update_esselon_del ( $fesselon_id );
					$params->esselon_delete ( $fdata_id );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fname );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$params->esselon_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $params->esselon_count ($key_search, $val_search, $key_field);
		$rs = $params->esselon_view_grid ($key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Unit Eselon I";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
