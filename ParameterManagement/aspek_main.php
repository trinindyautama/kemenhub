<?
include_once "_includes/classes/param_class.php";

$params = new param ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=par_aspek";
$acc_page_request = "aspek_acc.php";
$list_page_request = "param_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Urutan", "Aspek Program Audit", "Ikhtisar");
$gridDetail = array ("urutan", "aspek_name", "ikhtisar");
$gridWidth = array ("5", "30", "50");

$key_by = array ("Urutan", "Bidang Substansi", "Ikhtisar");
$key_field = array ("urutan", "aspek_name", "ikhtisar");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Aspek Program Audit";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->bidang_sub_data_viewlist ( $fdata_id );
		$page_title = "Ubah Aspek Program Audit";
		break;
	case "postadd" :
	    $furutan = $comfunc->replacetext ( $_POST ["urutan"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		$fikhtisar = $comfunc->replacetext ( $_POST ["ikhtisar"] );
		if ($fname != "") {
			$rs_nama = $params->cek_nama_bidang_sub ( $fname );
			$arr_nama = $rs_nama->FetchRow ();
			$faspek_id = $arr_nama ['aspek_id'];
			$del_st = $arr_nama ['aspek_del_st'];
			if ($faspek_id == "") {
				$params->bidang_sub_add ( $furutan, $fname, $fikhtisar );
				$comfunc->js_alert_act ( 3 );
			} else {
				if ($del_st == "0") {
					$params->update_bidang_sub_del ( $faspek_id, $fname );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fname );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$furutan = $comfunc->replacetext ( $_POST ["urutan"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		$fikhtisar = $comfunc->replacetext ( $_POST ["ikhtisar"] );
		if ($fname != "") {
			$rs_nama = $params->cek_nama_bidang_sub ( $fname, $fdata_id );
			$arr_nama = $rs_nama->FetchRow ();
			$faspek_id = $arr_nama ['aspek_id'];
			$del_st = $arr_nama ['aspek_del_st'];
			if ($faspek_id == "") {
				$params->bidang_sub_edit ( $fdata_id, $furutan, $fname, $fikhtisar );
				$comfunc->js_alert_act ( 1 );
			} else {
				if ($del_st == "0") {
					$params->update_bidang_sub_del ( $faspek_id, $fname );
					$params->bidang_sub_delete ( $fdata_id );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fname );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$params->bidang_sub_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $params->bidang_sub_count ($key_search, $val_search, $key_field);
		$rs = $params->bidang_sub_view_grid ($key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Aspek Program Audit";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
