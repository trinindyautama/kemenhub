<?php
include_once "../_includes/classes/param_class.php";
include_once "../_includes/common.php";
$params = new param ();
$comfunc = new comfunction ();

$year = date ( "Y" ) + 1;
$date = "01-01-" . $year;
$date1 = str_replace ( '-', '/', $date );
for($i = 1; $i <= 366; $i ++) {
	$all_date = date ( 'd-m-Y', strtotime ( $date1 . "+" . $i . " days" ) );
	if (date ( "N", strtotime ( $all_date ) ) == 6 || date ( "N", strtotime ( $all_date ) ) == 7) {
		if (date ( "Y", strtotime ( $all_date ) ) == $year) {
			$weekend = $comfunc->date_db ( $all_date );
			$params->insert_weekend ( $weekend, "2" );
		}
	}
}
?>