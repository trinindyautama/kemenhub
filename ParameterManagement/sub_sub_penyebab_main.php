<?
include_once "_includes/classes/param_class.php";

$params = new param ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=par_sub_sub_penyebab";
$acc_page_request = "sub_sub_penyebab_acc.php";
$list_page_request = "param_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Kode Sub Penyebab", "Jenis Penyebab Temuan", "Sub Kode Kelompok", "Kelompok");
$gridDetail = array ("penyebab_sub_sub_code", "penyebab_sub_sub_name", "kode_penyebab_desc", "kel_penyebab_desc");
$gridWidth = array ("10", "25", "25", "20");

$key_by = array ("Kode Sub Penyebab", "Jenis Penyebab Temuan", "Sub Kode Kelompok", "Kelompok");
$key_field = array ("penyebab_sub_sub_code", "penyebab_sub_sub_name", "kode_penyebab_desc", "kel_penyebab_desc");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Sub-Sub Penyebab Temuan";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->sub_sub_penyebab_data_viewlist ( $fdata_id );
		$page_title = "Ubah Sub-Sub Penyebab Temuan";
		break;
	case "postadd" :
		$fkode_penyebab_kel_penyebab_id = $comfunc->replacetext ( $_POST ["kode_penyebab_kel_penyebab_id"] );
		$fpenyebab_sub_sub_kode_penyebab_id = $comfunc->replacetext ( $_POST ["penyebab_sub_sub_kode_penyebab_id"] );
		$fcode = $comfunc->replacetext ( $_POST ["code"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		if ($fpenyebab_sub_sub_kode_penyebab_id != "" && $fkode_penyebab_kel_penyebab_id != "" && $fcode != "" && $fname != "") {
			$rs_cek = $params->cek_kode_jenis_temuan ( $fcode, $fpenyebab_sub_sub_kode_penyebab_id, $fkode_penyebab_kel_penyebab_id );
			$arr_cek = $rs_cek->FetchRow ();
			$fpenyebab_sub_sub_id = $arr_cek ['penyebab_sub_sub_id'];
			$del_st = $arr_cek ['penyebab_sub_sub_del_st'];
			if ($fpenyebab_sub_sub_id == "") {
				$params->sub_sub_penyebab_add ( $fpenyebab_sub_sub_kode_penyebab_id, $fcode, $fname );
				$comfunc->js_alert_act ( 3 );
			} else {
				if ($del_st == "0") {
					$params->sub_sub_penyebab_temuan_del ( $fpenyebab_sub_sub_id, $fpenyebab_sub_sub_kode_penyebab_id, $fcode, $fname );
					$comfunc->js_alert_act ( 3 );
				} else {
					$comfunc->js_alert_act ( 4, $fcode);
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fkode_penyebab_kel_penyebab_id = $comfunc->replacetext ( $_POST ["kode_penyebab_kel_penyebab_id"] );
		$fpenyebab_sub_sub_kode_penyebab_id = $comfunc->replacetext ( $_POST ["penyebab_sub_sub_kode_penyebab_id"] );
		$fcode = $comfunc->replacetext ( $_POST ["code"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		if ($fpenyebab_sub_sub_kode_penyebab_id != "" && $fkode_penyebab_kel_penyebab_id != "" && $fcode != "" && $fname != "") {
			$fpenyebab_sub_sub_id = "";
			if ($fpenyebab_sub_sub_id == "") {
				$params->sub_sub_penyebab_edit ( $fdata_id, $fpenyebab_sub_sub_kode_penyebab_id, $fcode, $fname );
				$comfunc->js_alert_act ( 1 );
			} else {
				if ($del_st == "0") {
					$params->sub_sub_penyebab_edit ( $fpenyebab_sub_sub_id, $fsub_id, $fcode, $fname );
					$params->sub_sub_penyebab_delete ( $fdata_id );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fcode );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$params->sub_sub_penyebab_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $params->sub_sub_penyebab_count ($key_search, $val_search, $key_field);
		$rs = $params->sub_sub_penyebab_view_grid ($key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Sub-Sub Penyebab Temuan";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
