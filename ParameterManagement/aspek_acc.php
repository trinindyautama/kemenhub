<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span2">Urutan</label> 
				<input type="text" class="span1" name="urutan" id="urutan">
			</fieldset>	
			<fieldset class="hr">
				<label class="span2">Aspek Program Audit</label> 
				<input type="text" class="span7" name="name" id="name">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Muncul di Ikhtisar</label>
				<select name="ikhtisar" id="ikhtisar">
					<option value="">Pilih Satu</option>
					<option value="Iya">Iya</option>
					<option value="Tidak">Tidak</option>
				</select>
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span2">Urutan</label> 
				<input type="text" class="span1" name="urutan" id="urutan" value="<?=$arr['urutan']?>">
			</fieldset>	
			<fieldset class="hr">
				<label class="span2">Aspek Program Audit</label> 
				<input type="text" class="span3" name="name" id="name" value="<?=$arr['aspek_name']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Muncul di Ikhtisar</label> <select name="ikhtisar"
					id="ikhtisar">
					<option value="">Pilih Satu</option>
					<option value="Iya"
						<? if($arr['ikhtisar']=="Iya") echo "selected";?>>Iya</option>
					<option value="Tidak"
						<? if($arr['ikhtisar']=="Tidak") echo "selected";?>>Tidak</option>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['aspek_id']?>">	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script>  
$(function() {
	$("#validation-form").validate({
		rules: {
			name: "required",
			urutan: "required",
			ikhtisar: "required",
		},
		messages: {
			name: "Silahkan Masukan Aspek Program Audit"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>