<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span2">Kelompok Penyebab Temuan</label>
				<?
				$rs_kel = $params->sub_penyebab_data_viewlist ();
				$arr_kel = $rs_kel->GetArray ();
				echo $comfunc->buildCombo ( "kode_penyebab_kel_penyebab_id", $arr_kel, 0, 2, "", "subOnChange(this.value, 'penyebab_sub_sub_kode_penyebab_id')", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Kode Sub Penyebab</label>
				<select name="penyebab_sub_sub_kode_penyebab_id" id="penyebab_sub_sub_kode_penyebab_id">
					<option value="0">Pilih Satu</option>
				</select>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Kode Sub-Sub Penyebab</label> <input type="text" class="span1" name="code" id="code"><span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Keterangan</label> <input type="text" class="span5" name="name" id="name"><span class="mandatory">*</span>
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				$rs_get_type =  $params->sub_kel_penyebab_data_viewlist ( $arr['penyebab_sub_sub_kode_penyebab_id'] );
				$arr_get_type = $rs_get_type->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span2">Kelompok Temuan</label>
				<?
				$rs_kel = $params->sub_penyebab_data_viewlist ();
				$arr_kel = $rs_kel->GetArray ();
				echo $comfunc->buildCombo ( "kode_penyebab_kel_penyebab_id", $arr_kel, 0, 2, $arr_get_type['kode_penyebab_kel_penyebab_id'], "subOnChange(this.value, 'sub_id')", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Kode Sub Kelompok</label>
				<?
				$rs_sub = $params->sub_to_kel_penyebab ($arr_get_type['kode_penyebab_kel_penyebab_id']);
				$arr_sub = $rs_sub->GetArray ();
				echo $comfunc->buildCombo ( "penyebab_sub_sub_kode_penyebab_id", $arr_sub, 0, 1, $arr['penyebab_sub_sub_kode_penyebab_id'], "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Kode Sub-Sub Penyebab</label> <input type="text" class="span1" name="code" id="code" value="<?=$arr['penyebab_sub_sub_code']?>"><span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Jenis Temuan</label> <input type="text" class="span5" name="name" id="name" value="<?=$arr['penyebab_sub_sub_name']?>"><span class="mandatory">*</span>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['penyebab_sub_sub_id']?>">	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali"
						onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp; <input
						type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action"
					value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script>  
$(function() {
	$("#validation-form").validate({
		rules: {
			kode_penyebab_kel_penyebab_id: "required",
			penyebab_sub_sub_kode_penyebab_id: "required",
			code: "required",
			name: "required"
			
		},
		messages: {
			kode_penyebab_kel_penyebab_id: "Silahkan Pilih Satu",
			penyebab_sub_sub_kode_penyebab_id: "Silahkan Pilih Satu",
			code: "Silahkan Masukkan Kode Sub-Sub Penyebab",
			name: "Silakan Masukaan Keterangan"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});

function selectRemoveAll(objSel) {
	document.getElementById(objSel).options.length = 0;
}

function selectAdd(objSel, objVal, objCap, isSelected) {
	var nextLength = document.getElementById(objSel).options.length;
	document.getElementById(objSel).options[nextLength] = new Option(objCap, objVal, false, isSelected);
}

function subOnChange(objValue, cmbNext){
	objSel = cmbNext;
	selectRemoveAll(objSel);
	
	selectAdd(objSel, "", "Pilih Satu");
	switch (objValue) {
	<?
		$rs1 = $params->sub_penyebab_data_viewlist ();
		$arr1 = $rs1->GetArray();
		$rs1->Close();
		foreach ($arr1 as $value1) {
			echo("case \"$value1[0]\":\n");
			$rs2 = $params->sub_to_kel_penyebab($value1[0]);
			$arr2 = $rs2->GetArray();
			$rs2->Close();
				foreach ($arr2 as $value2) {
					$isSelected="false";
					echo("\tselectAdd(objSel, \"$value2[0]\", \"$value2[1]\", $isSelected);\n");
				}
			echo("\tbreak;\n");
		}
	?>
	}
}
</script>