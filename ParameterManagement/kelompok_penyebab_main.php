<?
include_once "_includes/classes/param_class.php";

$params = new param ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=par_kelompok_penyebab";
$acc_page_request = "kelompok_penyebab_acc.php";
$list_page_request = "param_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Kode Kelompok Penyebab", "Nama Kelompok");
$gridDetail = array ("kel_penyebab_code", "kel_penyebab_desc");
$gridWidth = array ("30", "50");

$key_by = array ("Kode Kelompok Penyebab", "Nama Kelompok");
$key_field = array ("kel_penyebab_code", "kel_penyebab_desc");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Kelompok Penyebab Temuan";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->kelompok_penyebab_data_viewlist ( $fdata_id );
		$page_title = "Ubah Kelompok Penyebab Temuan";
		break;
	case "postadd" :
		$fcode = $comfunc->replacetext ( $_POST ["code"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		if ($fcode != "" && $fname != "") {
			$rs_nama = $params->cek_nama_kelompok_penyebab ( $fname );
			$arr_nama = $rs_nama->FetchRow ();
			$fkel_penyebab_id = $arr_nama ['kel_penyebab_id'];
			$del_st = $arr_nama ['kel_penyebab_del_st'];
			if ($fkel_penyebab_id == "") {
				$params->kelompok_penyebab_add ( $fcode, $fname );
				$comfunc->js_alert_act ( 3 );
			} else {
				if ($del_st == "0") {
					$params->update_kelompok_penyebab_del ( $fkel_penyebab_id, $fcode, $fname );
					$comfunc->js_alert_act ( 3 );
				} else {
					$comfunc->js_alert_act ( 4, $fname );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fcode = $comfunc->replacetext ( $_POST ["code"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		if ($fcode != "" && $fname != "") {
			$fkel_penyebab_id = "";
			if ($fkel_penyebab_id == "") {
				$params->kelompok_penyebab_edit ( $fdata_id, $fcode, $fname );
				$comfunc->js_alert_act ( 1 );
			} else {
				if ($del_st == "0") {
					$params->update_kelompok_penyebab_del ( $fkel_penyebab_id, $fcode, $fname );
					$params->kelompok_penyebab_delete ( $fdata_id );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fname );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$params->kelompok_penyebab_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $params->kelompok_penyebab_count ($key_search, $val_search, $key_field);
		$rs = $params->kelompok_penyebab_view_grid ($key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Kelompok Penyebab Temuan";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
