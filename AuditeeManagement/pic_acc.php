<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<article class="module width_3_quarter">
	<header>
		<h3 class="tabs_involved"><?=$page_title?></h3>
	</header>
	<form method="post" name="f" action="#" class="form-horizontal"
		id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
			<label class="span2">NIP</label> <input type="text" class="span3"
				name="nip" id="nip"><span class="mandatory">*</span>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Nama</label> <input type="text"
				class="span3" name="name" id="name"><span class="mandatory">*</span>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Jabatan</label>
				<?=$comfunc->dbCombo("jabatan_id", "par_jabatan_pic", "jabatan_pic_id", "jabatan_pic_name", "and jabatan_pic_del_st = 1 ", "", "order by jabatan_pic_sort", 1)?><span
				class="mandatory">*</span>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Mobile</label> <input type="text" class="span2"
				name="mobile" id="mobile">
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Telp</label> <input type="text" class="span2"
				name="telp" id="telp">
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Email</label> <input type="text" class="span2"
				name="email_pic" id="email_pic">
		</fieldset>
        <fieldset class="hr">
        <label class="span2">No SK</label>
            <input type="text" class="span2" name="pic_no_sk" id="pic_no_sk">
            <span class="mandatory">*</span>
        </fieldset>
        <fieldset class="hr">
        <label class="span2">Tanggal SK</label>
            <input type="text" class="span2" name="pic_tgl_sk" id="pic_tgl_sk">
            <span class="mandatory">*</span>
        </fieldset>
        <fieldset class="hr">
				<label class="span2">Tahun</label> 
				<select class="span1" name="tahun" id="tahun">
					<option value="">Pilih Satu</option>
					<?
				$thn = date ( "Y" ) - 5;
				for($i = 1; $i <= 11; $i ++) {
					?>
					<option value="<?=$thn?>" <? if($thn==date("Y")) echo "selected";?>><?=$thn?></option>
					<?
					$thn ++;
				}
				?>
				</select>
		</fieldset>
        <fieldset class="hr">
            <label class="span2">Golongan/Pangkat</label>
            <?=$comfunc->dbCombo("pangkat", "par_pangkat_auditor", "pangkat_id", "concat(pangkat_name, ' - ', pangkat_desc) as pangkat_lengkap", "and pangkat_del_st = 1 ", "", "", 1, " order by pangkat_name ")?>
        </fieldset>
        <input type="hidden" name="auditee_id" value="<?=$fdata_id?>">
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
			<label class="span2">NIP</label> <input type="text" class="span3"
				name="nip" id="nip" value="<?=$arr['pic_nip']?>"><span
				class="mandatory">*</span>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Nama</label> <input type="text"
				class="span3" name="name" id="name" value="<?=$arr['pic_name']?>"><span
				class="mandatory">*</span>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Jabatan</label>
				<?=$comfunc->dbCombo("jabatan_id", "par_jabatan_pic", "jabatan_pic_id", "jabatan_pic_name", "and jabatan_pic_del_st = 1 ", $arr['pic_jabatan_id'], "order by jabatan_pic_sort", 1)?><span
				class="mandatory">*</span>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Mobile</label> <input type="text" class="span2"
				name="mobile" id="mobile" value="<?=$arr['pic_mobile']?>">
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Telp</label> <input type="text" class="span2"
				name="telp" id="telp" value="<?=$arr['pic_telp']?>">
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Email</label> <input type="text" class="span2"
				name="email_pic" id="email_pic" value="<?=$arr['pic_email']?>">
		</fieldset>
        <fieldset class="hr">
        <label class="span2">No SK</label>
            <input type="text" class="span2" name="pic_no_sk" id="pic_no_sk" value="<?=$arr['pic_no_sk']?>">
            <span class="mandatory">*</span>
        </fieldset>
        <fieldset class="hr">
        <label class="span2">Tanggal SK</label>
            <input type="text" class="span2" name="pic_tgl_sk" id="pic_tgl_sk" value="<?=$comfunc->dateIndo($arr['pic_tgl_sk'])?>">
            <span class="mandatory">*</span>
        </fieldset>
        <fieldset class="hr">
				<label class="span2">Tahun</label> 
				<select class="span1" name="tahun" id="tahun">
					<option value="">Pilih Satu</option>
					<?
				$thn = date ( "Y" ) - 5;
				for($i = 1; $i <= 11; $i ++) {
					?>
					<option value="<?=$thn?>"
						<? if($thn==$arr['pic_tahun']) echo "selected";?>><?=$thn?></option>
					<?
					$thn ++;
				}
				?>
				</select>
			</fieldset>
        <fieldset class="hr">
            <label class="span2">Golongan/Pangkat</label>
            <?=$comfunc->dbCombo("pangkat", "par_pangkat_auditor", "pangkat_id", "concat(pangkat_name, ' - ', pangkat_desc) as pangkat_lengkap", "and pangkat_del_st = 1 ", $arr['pic_pangkat_id'], "", 1, " order by pangkat_name ")?>
        </fieldset>
        <input type="hidden" name="auditee_id" value="<?=$fdata_id?>"> <input
			type="hidden" name="data_id" value="<?=$arr['pic_id']?>">	
		<?
				break;
			case "getdetail" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
			<label class="span2">NIP</label>
			<?=$arr['pic_nip']?>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Nama</label>
			<?=$arr['pic_name']?>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Jabatan</label>
			<?=$arr['jabatan_pic_name']?>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Mobile</label>
			<?=$arr['pic_mobile']?>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Telp</label>
			<?=$arr['pic_telp']?>
		</fieldset>
		<fieldset class="hr">
			<label class="span2">Email</label>
			<?=$arr['pic_email']?>
		</fieldset>
        <fieldset class="hr">
			<label class="span2">No SK</label>
			<?=$arr['pic_no_sk']?>
		</fieldset>
        <fieldset class="hr">
			<label class="span2">Tanggal SK</label>
			<?=$comfunc->dateIndo($arr['pic_tgl_sk'])?>
		</fieldset>
        <fieldset class="hr">
            <label class="span2">Tahun</label>
            <?=$arr['pic_tahun']?>
        </fieldset>
        <fieldset class="hr">
            <label class="span2">Golongan/Pangkat</label>
            <?=$arr['pangkat_lengkap']?>
        </fieldset>
        <?
				break;
		}
		?>
			<fieldset>
			<center>
				<input type="button" class="blue_btn" value="Kembali"
					onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp; <input
					type="submit" class="blue_btn" value="Simpan">
			</center>
			<input type="hidden" name="data_action" id="data_action"
				value="<?=$_nextaction?>">
		</fieldset>
	</form>
</article>
<script>  
$(function() {
	$("#validation-form").validate({
		rules: {
			nip: "required",
			name: "required",
			jabatan_id: "required",
            pic_no_sk: "required",
            pic_tgl_sk: "required"
		},
		messages: {
			nip: "Silahkan masukan NIP PIC",
			name: "Silahkan masukan Nama PIC",
			jabatan_id: "Silahkan Pilih Jabatan PIC",
            pic_no_sk: "Silahkan Masukkan No SK",
            pic_tgl_sk: "Silahkan Masukkan Tanggal SK"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
    $("#pic_tgl_sk").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });
});
</script>