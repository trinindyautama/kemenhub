<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
			<p><span style="color:#fff;">_____</span><span style="color:red; font-size:15pt;">*</span> wajib diisi</p>
			<?
			switch ($_action) {
				case "getadd" :
					?>
			<fieldset class="hr">
				<label class="span3">Kode Auditi</label> <input type="text"
					class="span2" name="kode" id="kode"><span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Nama Auditi</label> <input type="text"
					class="span5" name="name" id="name"><span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Unit Eselon 1</label>
				<?=$comfunc->dbCombo("esselon_id", "par_esselon", "esselon_id", "esselon_name", "and esselon_del_st = 1 ", "", "", 1)?>
                <span class="mandatory">*</span>
			</fieldset>
			<!-- <fieldset class="hr">
				<label class="span3">Unit Penanggung Jawab</label>
				<?=$comfunc->dbCombo("parrent_id", "auditee", "auditee_id", "auditee_name", "and auditee_del_st = 1", "", "",1)?>
			</fieldset> -->
			<fieldset class="hr">
				<label class="span3">Inspektorat Penanggung Jawab</label>
				<?=$comfunc->dbCombo("inspektorat_id", "par_inspektorat", "inspektorat_id", "inspektorat_name", "and inspektorat_del_st = 1 ", "", "", 1, "order by inspektorat_name")?>
                <span class="mandatory">*</span>
            </fieldset>
			<fieldset class="hr">
				<label class="span3">Propinsi</label>
				<?
				$rs_prov = $params->propinsi_data_viewlist ();
				$arr_prov = $rs_prov->GetArray ();
				echo $comfunc->buildCombo ( "propinsi_id", $arr_prov, 0, 1, "", "propinsiOnChange(this.value, 'kabupaten_id')", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Kabupaten/Kota</label>
				<select name="kabupaten_id" id="kabupaten_id">
					<option value="0">Pilih Propinsi</option>
				</select>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Alamat</label>
				<textarea name="alamat" class="span6"></textarea>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Telp</label> 
				<input type="text" class="span2" name="telp" id="telp">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Ext</label> 
				<input type="text" class="span2" name="ext" id="ext">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Fax</label> 
				<input type="text" class="span2" name="fax" id="fax">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Email</label> 
				<input type="text" class="span2" name="email" id="email">
			</fieldset>
            <fieldset class="hr">
				<label class="span3">KPPN</label> 
				<input type="text" class="span2" name="auditee_kppn" id="kppn">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Bank</label> 
				<input type="text" class="span2" name="auditee_bank" id="bank">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Fasilitas</label> <br><br>
				<textarea name="auditee_fasilitas" id="fasilitas" class="ckeditor"></textarea>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Pegawai</label> 
				<input type="text" class="span2" name="auditee_jml_pegawai" id="jmlpegawai">
			</fieldset>
            <?
					break;
				case "getedit" :
					?>			
			<fieldset class="hr">
				<label class="span3">Kode Auditi</label> <input type="text"
					class="span2" name="kode" id="kode"
					value="<?=$arr['auditee_kode']?>"><span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Nama Auditi</label> <input type="text"
					class="span5" name="name" id="name"
					value="<?=$arr['auditee_name']?>"><span class="mandatory">*</span>
			</fieldset>
			
			<fieldset class="hr">
				<label class="span3">Unit Eselon 1</label>
				<?=$comfunc->dbCombo("esselon_id", "par_esselon", "esselon_id", "esselon_name", "and esselon_del_st = 1 ", $arr['auditee_esselon'], "", 1)?>
                <span class="mandatory">*</span>
            </fieldset>
			<fieldset class="hr">
				<label class="span3">Inspektorat Penanggung Jawab</label>
				<?=$comfunc->dbCombo("inspektorat_id", "par_inspektorat", "inspektorat_id", "inspektorat_name", "and inspektorat_del_st = 1 ", $arr['auditee_inspektorat_id'], "", 1, "order by inspektorat_name")?>
                <span class="mandatory">*</span>
            </fieldset>
			<fieldset class="hr">
				<label class="span3">Propinsi</label>
				<?
				$rs_prov = $params->propinsi_data_viewlist ();
				$arr_prov = $rs_prov->GetArray ();
				echo $comfunc->buildCombo ( "propinsi_id", $arr_prov, 0, 1, $arr['auditee_propinsi_id'], "propinsiOnChange(this.value, 'kabupaten_id')", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Kabupaten/Kota</label>
				<?
				$rs_kab = $params->propinsi_kabupaten ($arr['auditee_propinsi_id']);
				$arr_kab = $rs_kab->GetArray ();
				echo $comfunc->buildCombo ( "kabupaten_id", $arr_kab, 0, 1, $arr['auditee_kabupaten_id'], "", "", false, true, false );
				?>
				</fieldset>
			<fieldset class="hr">
				<label class="span3">Alamat</label>
				<textarea name="alamat" class="span6"><?=$arr['auditee_alamat']?></textarea>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Telp</label> 
				<input type="text" class="span2" name="telp" id="telp" value="<?=$arr['auditee_telp']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Ext</label> 
				<input type="text" class="span2" name="ext" id="ext" value="<?=$arr['auditee_ext']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Fax</label>
				<input type="text" class="span2" name="fax" id="fax" value="<?=$arr['auditee_fax']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Email</label> 
				<input type="text" class="span2" name="email" id="email" value="<?=$arr['auditee_email']?>">
			</fieldset>
            <fieldset class="hr">
				<label class="span3">KPPN</label> 
				<input type="text" class="span2" name="auditee_kppn" id="kppn" value="<?=$arr['auditee_kppn']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Bank</label> 
				<input type="text" class="span2" name="auditee_bank" id="bank" value="<?=$arr['auditee_bank']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Fasilitas</label> <br><br>
				<textarea name="auditee_fasilitas" id="fasilitas" class="ckeditor"><?=$arr['auditee_fasilitas']?></textarea>
			</fieldset>
				<fieldset class="hr">
				<label class="span3">Jumlah Pegawai</label> 
				<input type="text" class="span2" name="auditee_jml_pegawai" id="jmlpegawai" value="<?=$arr['auditee_jml_pegawai']?>">
			</fieldset>
            <input type="hidden" name="data_id" value="<?=$arr['auditee_id']?>">
			<?
					break;
			}
			?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali"
						onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp; <input
						type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action"
					value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script>  
$(function() {
	$("#validation-form").validate({
		rules: {
			kode: "required",
			name: "required",
            esselon_id: "required",
            inspektorat_id: "required",
			password: {
                required: true,
                minlength: 8
            },
			propinsi_id : "required"
		},
		messages: {
			kode: "Silahkan masukan Kode Auditi",
			name: "Silahkan masukan Nama Auditi",
            esselon_id: "Silahkan masukan Unit Eselon 1",
            inspektorat_id: "Silahkan masukan Inspektorat Penanggung Jawab",
            propinsi_id : "Pilih Propinsi"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});

function selectRemoveAll(objSel) {
	document.getElementById(objSel).options.length = 0;
}

function selectAdd(objSel, objVal, objCap, isSelected) {
	var nextLength = document.getElementById(objSel).options.length;
	document.getElementById(objSel).options[nextLength] = new Option(objCap, objVal, false, isSelected);
}

function propinsiOnChange(objValue, cmbNext){
	objSel = cmbNext;
	selectRemoveAll(objSel);
	
	selectAdd(objSel, "0", "Pilih Satu");
	switch (objValue) {
	<?
		$rs1 = $params->propinsi_data_viewlist ();
		$arr1 = $rs1->GetArray();
		$rs1->Close();
		foreach ($arr1 as $value1) {
			echo("case \"$value1[0]\":\n");
			$rs2 = $params->propinsi_kabupaten($value1[0]);
			$arr2 = $rs2->GetArray();
			$rs2->Close();
				foreach ($arr2 as $value2) {
					$isSelected="false";
					echo("\tselectAdd(objSel, \"$value2[0]\", \"$value2[1]\", $isSelected);\n");
				}
			echo("\tbreak;\n");
		}
	?>
	}
}

$( "#esselon_id" ).change(function() {
	if ($( "#esselon_id" ).val() === "a8963b8aa3608dcb206926988c3b9ca91d495e0f") {	
		document.getElementById('pejabat_jabatan').value = 'Direktur Jenderal Perhubungan Darat';
	}else if ($( "#esselon_id" ).val() === "1c56ae798a24600a89c638869b8b9d08f1decc3f") {
		document.getElementById('pejabat_jabatan').value = 'Direktur Jenderal Perhubungan Laut';
	}else if ($( "#esselon_id" ).val() === "81f350a05bf5797a609f2e9bca287db694a9ab49") {
		document.getElementById('pejabat_jabatan').value = 'Direktur Jenderal Perhubungan Udara';
	}else if ($( "#esselon_id" ).val() === "08029bd17bc2cf8a514c3f363be14d491c65362b") {
		document.getElementById('pejabat_jabatan').value = 'Direktur Jenderal Perkeretaapian';
	}else if ($( "#esselon_id" ).val() === "93a401be9c1cb44114e0432f9c31d6a2a662a4fb") {
		document.getElementById('pejabat_jabatan').value = 'Sekretaris Jenderal';	
	}else if ($( "#esselon_id" ).val() === "c837ebf11517f831fd0f0d7fc8a17567ac72c870") {
		document.getElementById('pejabat_jabatan').value = 'Kepala Badan Penelitian dan Pengembangan Perhubungan';	
	}else if ($( "#esselon_id" ).val() === "751a275a83fdaffebc9fd9fb48bc56f9e7c18b20") {
		document.getElementById('pejabat_jabatan').value = 'Kepala Badan Pengembangan Sumber Daya Manusia Perhubungan';	
	}else if ($( "#esselon_id" ).val() === "7f7476dc4fd08ba36ea4ba72f00388690aade41d") {
		document.getElementById('pejabat_jabatan').value = 'Kepala Badan Pengelola Transportasi JABODETABEK';	
	}else if ($( "#esselon_id" ).val() === "3a25c613197b2199ec9b2ead9ac4946379fc5b5d") {
		document.getElementById('pejabat_jabatan').value = 'Inspektur Jenderal';	
	}else {
		document.getElementById('pejabat_jabatan').value = '';	
	}	
});

</script>