<?
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/param_class.php";
$auditees = new auditee ( $ses_userId );
$params = new param ();

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=auditeemgmt";
$acc_page_request = "auditee_acc.php";
$acc_page_request_detil = "main_page.php?method=auditee_detil";
$list_page_request = "auditee_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Kode Auditi", "Nama Auditee", "Email");
$gridDetail = array ("1", "2", "4");
$gridWidth = array ("8", "20", "15");

$key_by = array ("Kode Auditi", "Nama Auditee",  "email");
$key_field = array ("auditee.auditee_kode", "auditee.auditee_name", "auditee.auditee_email");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Auditi";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $auditees->auditee_data_viewlist ( $fdata_id );
		$arr = $rs->FetchRow ();
		$page_title = "Ubah Auditi";
		break;
	case "getdetail" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		echo "<script>window.open('" . $acc_page_request_detil . "&auditee=" . $fdata_id . "', '_self');</script>";
		break;
	//untu menambah
	case "postadd" :
		$fkode = $comfunc->replacetext ( $_POST ["kode"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		$fesselon = $comfunc->replacetext ( $_POST ["esselon_id"] );
		$fparrent_id = $comfunc->replacetext ( $_POST ["parrent_id"] );
		// if ($fparrent_id == "")
		// 	$fparrent_id = "0";
		$finspektorat_id = $comfunc->replacetext ( $_POST ["inspektorat_id"] );
		$fpropinsi_id = $comfunc->replacetext ( $_POST ["propinsi_id"] );
		$fkabupaten_id = $comfunc->replacetext ( $_POST ["kabupaten_id"] );
		$falamat = $comfunc->replacetext ( $_POST ["alamat"] );
		$ftelp = $comfunc->replacetext ( $_POST ["telp"] );
		$fext = $comfunc->replacetext ( $_POST ["ext"] );
		$ffax = $comfunc->replacetext ( $_POST ["fax"] );
		$femail = $comfunc->replacetext ( $_POST ["email"] );
		$fno_file = $comfunc->replacetext($_POST['auditee_no_file']);
		$ftipe = $comfunc->replacetext($_POST['auditee_tipe']);
		$fkppn = $comfunc->replacetext($_POST['auditee_kppn']);
		$fbank = $comfunc->replacetext($_POST['auditee_bank']);
		$ffasilitas = $comfunc->replacetext($_POST['auditee_fasilitas']);
		$fjmlpegawai = $comfunc->replacetext($_POST['auditee_jml_pegawai']);
		$fpejabat_name = $comfunc->replacetext($_POST['pejabat_name']);
		$fpejabat_nip = $comfunc->replacetext($_POST['pejabat_nip']);
		$fpejabat_pangkat = $comfunc->replacetext($_POST['pejabat_pangkat']);
		$fpejabat_jabatan = $comfunc->replacetext($_POST['pejabat_jabatan']); 
		
		if ($fkode != "" && $fname != "" && $fpropinsi_id != "") {
			$rs_cek = $auditees->auidtee_cek_name ( $fkode );
			$arr_cek = $rs_cek->FetchRow ();
			$fauditee_id = $arr_cek ['auditee_id'];
			$del_st = $arr_cek ['auditee_del_st'];
			if ($fauditee_id == "") {
				$auditees->auditee_add ( $fkode, $fname, $fparrent_id, $finspektorat_id, $fpropinsi_id, $fkabupaten_id, $falamat, $ftelp, $ffax, $fext, $femail, $fno_file, $fesselon, $ftipe, $fkppn, $fbank, $ffasilitas, $fjmlpegawai, $fpejabat_name, $fpejabat_nip, $fpejabat_pangkat, $fpejabat_jabatan);
				$comfunc->js_alert_act ( 3 );
			} else {
				if ($del_st == "0") {
					$auditees->auditee_update_del_to_add ( $fauditee_id, $fkode, $fname, $fparrent_id, $finspektorat_id, $fpropinsi_id, $fkabupaten_id, $falamat, $ftelp, $ffax, $fext, $femail, $fno_file, $fesselon, $ftipe, $fkppn, $fbank, $ffasilitas, $fjmlpegawai, $fpejabat_name, $fpejabat_nip, $fpejabat_pangkat, $fpejabat_jabatan);
					$comfunc->js_alert_act ( 3 );
				} else {
					$comfunc->js_alert_act ( 4, $fkode );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
		//untuk edit
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fkode = $comfunc->replacetext ( $_POST ["kode"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		$fesselon = $comfunc->replacetext ( $_POST ["esselon_id"] );
		$fparrent_id = $comfunc->replacetext ( $_POST ["parrent_id"] );
		// if ($fparrent_id == "")
		// 	$fparrent_id = "0";
		$finspektorat_id = $comfunc->replacetext ( $_POST ["inspektorat_id"] );
		$fpropinsi_id = $comfunc->replacetext ( $_POST ["propinsi_id"] );
		$fkabupaten_id = $comfunc->replacetext ( $_POST ["kabupaten_id"] );
		$falamat = $comfunc->replacetext ( $_POST ["alamat"] );
		$ftelp = $comfunc->replacetext ( $_POST ["telp"] );
		$fext = $comfunc->replacetext ( $_POST ["ext"] );
		$ffax = $comfunc->replacetext ( $_POST ["fax"] );
		$femail = $comfunc->replacetext ( $_POST ["email"] );
		$fno_file = $comfunc->replacetext($_POST['auditee_no_file']);
		$ftipe = $comfunc->replacetext($_POST['auditee_tipe']); 
		$fkppn = $comfunc->replacetext($_POST['auditee_kppn']);
		$fbank = $comfunc->replacetext($_POST['auditee_bank']);
		$ffasilitas = $comfunc->replacetext($_POST['auditee_fasilitas']);
		$fjmlpegawai = $comfunc->replacetext($_POST['auditee_jml_pegawai']);
		$fpejabat_name = $comfunc->replacetext($_POST['pejabat_name']);
		$fpejabat_nip = $comfunc->replacetext($_POST['pejabat_nip']);
		$fpejabat_pangkat = $comfunc->replacetext($_POST['pejabat_pangkat']);
		$fpejabat_jabatan = $comfunc->replacetext($_POST['pejabat_jabatan']); 
		
		if ($fkode != "" && $fname != "" && $fpropinsi_id != "") {
//			$rs_cek = $auditees->auidtee_cek_name ( $fkode, $fdata_id );
//			$arr_cek = $rs_cek->FetchRow ();
//			$fauditee_id = $arr_cek ['auditee_id'];
//			$del_st = $arr_cek ['auditee_del_st'];
			if ($fauditee_id == "") {
				$auditees->auditee_edit ( $fdata_id, $fkode, $fname, $fparrent_id, $finspektorat_id, $fpropinsi_id, $fkabupaten_id, $falamat, $ftelp, $ffax, $fext, $femail, $fno_file, $fesselon, $ftipe, $fkppn, $fbank, $ffasilitas, $fjmlpegawai, $fpejabat_name, $fpejabat_nip, $fpejabat_pangkat, $fpejabat_jabatan );
				$comfunc->js_alert_act ( 1 );
			} else {
				if ($del_st == "0") {
					$auditees->auditee_update_del_to_add ( $fauditee_id, $fkode, $fname, $fparrent_id, $finspektorat_id, $fpropinsi_id, $fkabupaten_id, $falamat, $ftelp, $ffax, $fext, $femail, $fno_file, $fesselon, $ftipe, $fkppn, $fbank, $ffasilitas, $fjmlpegawai, $fpejabat_name, $fpejabat_nip, $fpejabat_pangkat, $fpejabat_jabatan );
					$auditees->auditee_delete ( $fdata_id );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fkode );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$auditees->_db->conn->BeginTrans ();
		$auditees->auditee_delete ( $fdata_id );
		$auditees->_db->conn->CommitTrans ();
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $auditees->auditee_count ($base_on_id_eks, $key_search, $val_search, $key_field);
		$rs = $auditees->auditee_viewlist ($base_on_id_eks, $key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Auditi";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
