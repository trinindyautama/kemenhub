<?php 
$notif_show="";
$rs_notif = $comfunc->list_notif($ses_userId);
$count_notif = $rs_notif->RecordCount();
include_once "_includes/classes/dashboard_class.php";
$dashboards = new dashboard ( $ses_userId );
if($count_notif<>0) $notif_show = ", Anda Memiliki ".$count_notif. " Notifikasi, silahkan direviu"
?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">
					Hi <?php if (null != $comfunc->name($ses_userId)): ?>
						&nbsp;<b><?=$comfunc->name($ses_userId)?></b>
					<?php else : ?>
						&nbsp;<b><?=$ses_userName?></b>
					<?php endif?>, Selamat datang di Aplikasi SIAU KEMENHUB <?=$notif_show?><br> Untuk meningkatkan keamanan sistem ini mohon segera ubah password anda</h3>
			<ul class="tabs">
				<li><a href="main_page.php?method=change_pass&data_action=getedit" style="background: #373085; color: white;">Ubah Password</a></li>
			</ul>
		</header>
		<div id="slideshow">
			<div><img src="Upload/dashboard/kemenhub1.jpg"></div>
			<div><img src="Upload/dashboard/kemenhub2.jpeg"></div>
			<div><img src="Upload/dashboard/kemenhub3.jpg"></div>
        </div>
<!--         <?php foreach (range(1,30) as $value): ?>
        	<br>
        <?php endforeach ?> -->
        <?php 
		if($count_notif<>0){
		?>
		<table class="table_grid" cellspacing="0" cellpadding="0">
			<tr>
				<th width="5%">No</th>
				<th width="20%">Catatan Dari</th>
				<th width="40%">Catatan</th>
				<th>Keterangan</th>
				<th width="10%">Aksi</th>
			</tr>
			<?php 
			$no_notif = 0;
			while($arr_notif = $rs_notif->FetchRow()){
				$no_notif++;
				$method = "";
				if($arr_notif['notif_method']=='surattugas') {
					$method="&data_action=viewsurattugas&data_id=".$arr_notif['notif_data_id'];
					$keterangan = $dashboards->get_surat_tugas_no($arr_notif['notif_data_id']);
				}
				if($arr_notif['notif_method']=='kertas_kerja') {
					$method="&id_kka_dash=".$arr_notif['notif_data_id'];
					$keterangan = $dashboards->get_kertas_kerja_no($arr_notif['notif_data_id']);
				}
				if($arr_notif['notif_method']=='finding_kka') {
					$method="&id_temuan_dash=".$arr_notif['notif_data_id'];
					$keterangan = $dashboards->get_temuan_no($arr_notif['notif_data_id']);
				}
				if($arr_notif['notif_method']=='programaudit') {
					$keterangan = $dashboards->get_program_code($arr_notif['notif_data_id']);
					$method="&id_program_dash=".$arr_notif['notif_data_id'];	
				} 
				if($arr_notif['notif_method']=='auditassign') {
					$method="&id_assign_dash=".$arr_notif['notif_data_id'];
					$keterangan = substr($dashboards->get_assign_kegiatan_name($arr_notif['notif_data_id']), 0, 100).'...';
				}
				if ('(LHA)...' == substr($arr_notif['notif_desc'], 0, 8)) {
					$rs = $dashboards->get_auditee_assign_id_per_lha($arr_notif['notif_data_id']);
					//$rs = $rd->FetchRow;
					$method="&data_action=getlha&data_id=".$rs['assign_id'].':'.$rs['assign_auditee_id_auditee'];
				}

			?>
			<tr>
				<td><?=$no_notif?></td>
				<td><?php if (null != $arr_notif['auditor_name']): ?>
					<?=$arr_notif['auditor_name']?>
				<?php else: ?>
					<?=strtoupper($arr_notif['from_auditor'])?>
				<?php endif ?>
				</td>
				<td><?=$comfunc->text_show($arr_notif['notif_desc'])?></td>
				<td><?= $keterangan ?></td>
				<td><a target="_self" href="main_page.php?method=<?=$arr_notif['notif_method'].$method?>">Reviu</a></td>
			</tr>
			<?
			}
			?>
		</table>
		<?
		}
		?>

		<?php 


		 ?>
	</article>
</section>
<script>
$("#slideshow > div:gt(0)").hide();
	setInterval(function() {
	  $('#slideshow > div:first')
		.hide(1000)
		.next()
		.show(1000)
		.end()
		.appendTo('#slideshow');
	}, 3000);
</script>