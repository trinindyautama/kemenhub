<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<?php
$assign_id = $arr['assign_id'];
$assign_id_auditee = $arr_auditee['auditee_id'];
$assign_nama_auditee = $arr_auditee['auditee_name'];
// lha
$rs_lha = $assigns->assign_lha_viewlist ( $assign_id, $assign_id_auditee );
$arr_lha = $rs_lha->FetchRow ();
if($arr_lha['lha_id']==""){
    $lha_id = $assigns->uniq_id();
    $status_lha = 0;
}else{
    $status_lha = $arr_lha ['lha_status'];
    $lha_id = $arr_lha['lha_id'];
}
// spl
$rs_spl = $assigns->assign_spl_viewlist ( $assign_id, $assign_id_auditee );
$arr_spl = $rs_spl->FetchRow ();
// get value finding
$rs_finding = $findings->finding_tl_nha_count ( $arr_lha ['assign_id'], "", "", "");
// temuan dan rekomendasi
$rs_spl_temuan_rekomendasi = $assigns->assign_spl_viewlist ( $assign_id, $assign_id_auditee );
$arr_spl_temuan_rekomendasi = $rs_spl_temuan_rekomendasi->FetchRow ();
// temuan dan rekomendasi
$cek_posisi = $findings->cek_posisi($arr['assign_id']);
// get inspektorat
$rs_auditee_viewlist = $assigns->assign_auditee_get_inspektorat_viewlist($arr ['assign_id'], $auditee_id);
$arr_auditee_viewlist = $rs_auditee_viewlist->FetchRow();
// get eselon
$rs_auditee_eselon = $assigns->assign_auditee_get_eselon_viewlist($arr ['assign_id'], $auditee_id);
$arr_auditee_eselon = $rs_auditee_eselon->FetchRow();
?>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <header>
            <h3 class="tabs_involved">&nbsp;<?=$page_title?></h3>
        </header>
        <form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data"  onsubmit="return cek_data()">
            <fieldset>
                <ul class="rtabs">
                    <li><a href="#view3">Bab I</a></li>
                    <li><a href="#view4">Bab II</a></li>
                    <li><a href="#view5">Bab III</a></li>
                    <li><a href="#view10">SPL</a></li>
                    <li><a href="#view7">Cetak LHA</a></li>
                    <li><a href="#view8">Cetak Matriks</a></li>
                    <li><a href="#view11">Cetak SPL</a></li>
                    <li><a href="#view6">Upload LHA</a></li>
                </ul>
                <div id="view10">
                    <fieldset class="hr">
                        <label class="span2">Nomor SPL</label>
                        <input type="text" class="span3" name="spl_no" id="spl_no" value="<?=$arr_spl['spl_no']?>">
                        <label class="span1">Tanggal SPL</label>
                        <input type="text" class="span1" name="spl_date" id="spl_date" value="<?=$comfunc->dateIndo($arr_spl['spl_date'])?>">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nomor LHA</label>
                        <input type="text" class="span3" name="no_lha" id="no_lha" value="<?=$arr_lha['lha_no']?>">
                        <label class="span1">Tanggal LHA </label>
                        <input type="text" class="span1" name="tanggal_lha" id="tanggal_lha" value="<?=$comfunc->dateIndo($arr_lha['lha_date'])?>">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Klasifikasi</label>
                        <input type="text" class="span6" name="spl_klasifikasi" id="spl_klasifikasi" value="<?
                        if ($arr_spl['spl_klasifikasi']=="") {?>Biasa
<?
                        } else {
                            ?>
<?=$arr_spl['spl_klasifikasi']?>
                                <?
                        }
                        ?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Lampiran</label>
                        <input type="text" class="span6" name="spl_lampiran" id="spl_lampiran" value="<?
                        if ($arr_spl['spl_lampiran']=="") {?>

1 (satu) berkas laporan
<?
                        } else {
                            ?>
<?=$arr_spl['spl_lampiran']?>
                                <?
                        }
                        ?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Perihal</label>
                        <input type="text" class="span6" name="spl_perihal" id="spl_perihal" value="<?
                        if ($arr_spl['spl_perihal']=="") {?>

Laporan Hasil Audit Kinerja pada <?=$assign_nama_auditee?> Provinsi <?=$arr_spl['propinsi_name']?>
<?
                        } else {
                            ?>
<?=$arr_spl['spl_perihal']?>
								<?
                        }
                        ?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Kepada YTH</label>
                        <input type="text" class="span6" name="spl_yth" id="spl_yth" value="<?
                        if ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Sekretariat Jenderal') {?>

Sekretaris Jenderal
                                <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Direktorat Jenderal Perhubungan Darat') {
                            ?>
Direktur Jenderal Perhubungan Darat
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Direktorat Jenderal Perhubungan Laut') {
                            ?>
Direktur Jenderal Perhubungan Laut
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Direktorat Jenderal Perhubungan Udara') {
                            ?>
Direktur Jenderal Perhubungan Udara
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Direktorat Jenderal Perkeretaapian') {
                            ?>
Direktur Jenderal Perkeretaapian
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Badan Penelitian dan Pengembangan Perhubungan') {
                            ?>
Kepala Badan Penelitian dan Pengembangan Perhubungan
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Badan Pengembangan Sumber Daya Manusia Perhubungan') {
                            ?>
Kepala Badan Pengembangan Sumber Daya Manusia Perhubungan
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Badan Pengelola Transportasi JABODETABEK') {
                            ?>
Kepala Badan Pengelola Transportasi JABODETABEK
                               <?
                        } elseif ($arr_spl['spl_yth']=="" && $arr_spl['esselon_name']=='Inspektorat Jenderal') {
                            ?>
Inspektur Jenderal
                               <?
                        } elseif ($arr_spl['spl_yth']!="") {
                               ?>
<?=$arr_spl['spl_yth']?>
                                <?
                        }
                        ?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Tembusan</label>
                        <input type="text" class="span6" name="spl_tembusan" id="spl_tembusan" value="<?
                        if ($arr_spl['spl_tembusan']=="" && $arr_auditee_viewlist['inspektorat_name']=="Inspektorat I") {?>Ketua BPK RI, Sekretaris Jenderal Kemenhub, Inspektur Jenderal Kemenhub, Inspektur I Jenderal Kemenhub, Kepala <?=$assign_nama_auditee?>
                                <?
                        } elseif ($arr_spl['spl_tembusan']=="" && $arr_auditee_viewlist['inspektorat_name']=="Inspektorat II")  {?>Ketua BPK RI, Sekretaris Jenderal Kemenhub, Inspektur Jenderal Kemenhub, Inspektur II Jenderal Kemenhub, Kepala <?=$assign_nama_auditee?>
                                <?
                        } elseif ($arr_spl['spl_tembusan']=="" && $arr_auditee_viewlist['inspektorat_name']=="Inspektorat III")  {?>Ketua BPK RI, Sekretaris Jenderal Kemenhub, Inspektur Jenderal Kemenhub, Inspektur III Jenderal Kemenhub, Kepala <?=$assign_nama_auditee?>
                                <?
                        } elseif ($arr_spl['spl_tembusan']=="" && $arr_auditee_viewlist['inspektorat_name']=="Inspektorat IV")  {?>Ketua BPK RI, Sekretaris Jenderal Kemenhub, Inspektur Jenderal Kemenhub, Inspektur IV Jenderal Kemenhub, Kepala <?=$assign_nama_auditee?>
                                <?
                        } elseif ($arr_spl['spl_tembusan']=="" && $arr_auditee_viewlist['inspektorat_name']=="Inspektorat V")  {?>Ketua BPK RI, Sekretaris Jenderal Kemenhub, Inspektur Jenderal Kemenhub, Inspektur V Jenderal Kemenhub, Kepala <?=$assign_nama_auditee?>
                                <?
                        } elseif ($arr_spl['spl_tembusan']!="")  {
                            ?>
<?=$arr_spl['spl_tembusan']?>
                                <?
                        }
                        ?>">
                        <span class="note">Gunakan koma (,) sebagai pemisah</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Penandatangan</label><br><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="spl_pj" id="spl_pj">
                            <?
                            if ($arr_spl['spl_pj']=="") {
                            ?>
                            <table align="justify">
                                <center>
                                    <?
                                    if ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat I") {
                                        ?>
                                        a.n. INSPEKTUR JENDERAL<br><br><br><br><br><br>
                                        <u>Muhammad Anto Julianto, S.E., M.Si., AK, CA</u><br>
                                    Pembina Utama Muda (IV/c)<br>
                                        NIP. 196807251990031001
                                        <?
                                    } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat II") {
                                        ?>
                                        a.n. INSPEKTUR JENDERAL<br><br><br><br><br><br>
                                        <u>Firdaus Komarno</u><br>
                                    Pembina Utama Muda (IV/c)<br>
                                        NIP. 196110242016021001
                                        <?
                                    } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat III") {
                                        ?>
                                        a.n. INSPEKTUR JENDERAL<br><br><br><br><br><br>
                                        <u>Mohamad Murdiyanto, S.E., M.Si</u><br>
                                    Pembina Utama Muda (IV/c)<br>
                                        NIP. 196703141992031003
                                        <?
                                    } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat IV") {
                                        ?>
                                        a.n. INSPEKTUR JENDERAL<br><br><br><br><br><br>
                                        <u>Drs. Imam Hambali, M.Si.</u><br>
                                    Pembina Utama Muda (IV/c)<br>
                                        NIP. 196204041988031001
                                        <?
                                    } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat V") {
                                        ?>
                                        a.n. INSPEKTUR JENDERAL<br><br><br><br><br><br>
                                        <u>Ir. Heri Sudarmaji, DEA</u><br>
                                    Pembina Utama Muda (IV/c)<br>
                                        NIP. 196609071994031001
                                        <?
                                    }
                                    ?>
                                    <?
                                    } else {
                                        ?>
                                        <?=$arr_spl['spl_pj']?>
                                        <?
                                    }
                                    ?>
                        </center>
                        </table>
                        </textarea>
                    </fieldset>
                </div>
                <div id="view11">
                    <div id="printableArea3">
                    <table align="center" width="100%">
                        <tr>
                            <td align="center">
                                <img align="left" src="images/logo-menhub.png" height="120" width="150"/>
                                <br><br><br><strong><font size="5">KEMENTERIAN PERHUBUNGAN <br>INSPEKTORAT JENDERAL</font></strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>
                    </table>
                    <table width="100%" border="0">
                        <tr>
                            <td width="5%"></td>
                            <td width="68%">
                                <p>Nomor : <?=$arr_spl['spl_no']?></p>
                                <p>Klasifikasi : <?=$arr_spl['spl_klasifikasi']?></p>
                                <p>Lampiran : <?=$arr_spl['spl_lampiran']?></p>
                                <p>Perihal : <?=$arr_spl['spl_perihal']?></p>
                            </td>
                            <td width="45%">
                                <p>Jakarta, <?=$comfunc->dateIndo($arr_spl['spl_date'])?></p>
                                <p>Yth. Kepada <?=$arr_spl['spl_yth']?></p>
                                <p>di</p>
                                <p><u>Jakarta</u></p>
                            </td>
                        </tr>
                    </table>
                    <br>

                        <table align="center" width="80%">
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top" width="1000px">
                                    1. Berdasarkan Surat Tugas Inpektur Jenderal Kementerian Perhubungan Nomor <?=$arr_spl['assign_surat_no']?> tanggal <?=$comfunc->dateIndoLengkap($arr_spl['assign_surat_tgl'])?>, Tim Inspetorat Jenderal telah melaksanakan
                                    pada <?=$assign_nama_auditee?> Provinsi <?=$arr_spl['propinsi_name']?>. Sehubungan dengan hal tersebut, bersama ini disampaikan disampaikan Laporan Hasil Audit (LHA) Nomor <?=$arr_spl['lha_no']?> tanggal
                                </td>
                                <td align="center" valign="top"></td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <br>
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top">
                                    <p>2. Dari Hasil <?=$arr_spl['audit_type_name']?> pada <?=$assign_nama_auditee?>, ditemukan beberapa kelemahan/temuan sebagai berikut:</p>
                                    <table width="700px" align="center">
                                        <?
                                        
                                        $rs_temuan_proses = $findings->get_temuan_proses ( $arr_spl['assign_id']);
                                        $count_tem = $rs_temuan_proses->RecordCount ();
                                        $no_tem = 0;
                                        
                                        $asal = array("a","b","c","d","e","f","g");
                                        while($arr_temuan_proses = $rs_temuan_proses->FetchRow()){
                                            $no_tem++;
                                            $rs_rekomendasi_proses = $findings->get_rekomendasi_proses ( $arr_temuan_proses['finding_id']);
                                            $count_rek = $rs_rekomendasi_proses->RecordCount ();
                                            $no_rek = 0;
                                            while($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()){
                                                $no_rek++;
                                                ?>
                                                <tr>
                                                    <?
                                                    if($no_rek==1){
                                                        ?>
                                                        <td><b><?=$asal[$no_tem -1]?>)&nbsp;<?=$arr_temuan_proses['finding_judul']?></b></td>
                                                        <?
                                                    }
                                                    ?>
                                                <tr>
                                                    <td align="justify"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc'])?></td>
                                                </tr>
                                                </tr>
                                                <?
                                            }
                                        }
                                        ?>
                                    </table>
                                </td>
                                <td align="center" valign="top"></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top">
                                    <p style="margin-top: 0px;">3. Mohon kirannya tindak lanjut atas rekomendasi tersebut kolom 4 (empat) matriks terlampir diisiikan pada kolom 6 (enam), dan setelah ditandatangani dikirim
                                        kepada Inspektorat Jenderal Kementerian Perhubungan selambat-lambatnya 60 (enam puluh) hari setelah diterimanya surat ini, sesuai Peraturan Menteri Perhubungan
                                        Nomor PM.65 Tahun 2011 tanggal 24 Juni 2011 tentang Tata Cara Tetap Pelaksanaan Pengawasan di Lingkungan Kementerian Perhubungan.</p>
                                    4. Demikian disampaikan, atas perhatiannya diucapkan terima kasih.
                                </td>
                                <td align="center" valign="top"></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="10" align="justify"></td>
                            </tr>
                            <table align="center" width="90%">
                                <tr>
                                    <td width="60%">&nbsp;</td>
                                    <td>
                                        <center>
                                            <?=$arr_spl['spl_pj']?>
                                        </center>
                                    </td>
                                </tr>
                                <?
                                if($arr_spl ['spl_tembusan']!=""){
                                    ?>
                                    <tr>
                                        <td colspan="2">
                                            Tembusan :<br>
                                            <?
                                            $cek_tem = explode(",", $arr_spl ['spl_tembusan'] );
                                            $cek_count = count($cek_tem);
                                            $notem = 0;
                                            for($x=0;$x<$cek_count;$x++){
                                                $notem++;
                                                echo $notem." .".trim($cek_tem[$x])."<br>";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?
                                }
                                ?>
                            </table>
                    </div>
                            <?
                            //Daltu
                            if ($arr_lha ['lha_status'] == '2' || $arr_lha ['lha_status'] == '7') {
                                if($cek_posisi=='1fe7f8b8d0d94d54685cbf6c2483308aebe96229'){
                                    ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td colspan="4" align="justify">
                                            Inspektur Inspektorat :<br>
                                            <?=$comfunc->dbCombo("inpektorat_id", "par_inspektorat", "inspektorat_id", "inspektorat_name", "and inspektorat_del_st = 1 ", "", "", 1)?>
                                            <span class="note">Khusus Untuk Mengajukan</span>
                                        </td>
                                    </tr>
                                    <?
                                }
                            }
                            ?>
                        </table>
                        <br>
                </div>
                <div id="view3">
                    <fieldset class="hr">
                        <label class="span2">Dasar Audit</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="dasar_audit" id="dasar_audit">
                            <?
                            if ($arr_lha['lha_dasar_audit']=="") {
                                ?>
                                a. Peraturan Menteri Perhubungan Nomor KM.60 Tahun 2010 tentang Organisasi dan Tata Kerja Kementerian Perhubungan;<br>
                                b. Peraturan Menteri Perhubungan Nomor 65 Tahun 2011 tentang Tata Cara Pelaksanaan Pengawasan di Lingkungan Kementerian Perhubungan;<br>
                                c. Program Kerja Pengawasan Tahunan (PKPT) Inspektorat Jenderal Kementerian Perhubungan Tahun Anggaran <?=$arr_lha['assign_tahun']?>; <br>
                                d. Surat Tugas (ST) Inspektur Jenderal Kementerian Perhubungan Nomor <?=$arr_lha['assign_surat_no']?> tanggal <?=$comfunc->dateIndoLengkap($arr_lha['assign_surat_tgl'])?> .
                                <?
                            } else {
                                ?>
                                <?=$arr_lha['lha_dasar_audit']?>
                                <?
                            }
                            ?>
                        </textarea>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Tujuan Audit</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="tujuan_audit" id="tujuan_audit">
                            <?
                            if ($arr_lha['lha_tujuan_audit']=="") {
                                ?>
                                a. Memperoleh keyakinan yang memadai bahwa <?=$assign_nama_auditee?> telah memenuhi aspek kehematan, efisiensi dan efektivitas dari unsur keuangan, sarana dan prasarana, tugas dan fungsi serta metode kerja;<br>
                                b. Menilai kecukupan pengendalian manajemen guna memperoleh keyakinan bahwa pengelolaan kegiatan belanja modal telah dilaksanakan secara hemat, efisien dan efektif, serta sesuai dengan rencana;<br>
                                c. Menilai kecukupan prosedur yang digunakan dalam pelaksanaan tugas, baik pelaksanaan tugas dan fungsi maupun kegiatan belanja modal; <br>
                                d. Memperoleh keyakinan yang memadai bahwa pertanggungjawaban keuangan telah dibuat sesuai dengan ketentuan yang berlaku. <br>
                                <?
                            } else {
                                ?>
                                <?=$arr_lha['lha_tujuan_audit']?>
                                <?
                            }
                            ?>
                        </textarea>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Ruang Lingkup Audit</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="ruang_lingkup" id="ruang_lingkup">
                            <?
                            if ($arr_lha['lha_ruanglingkup']=="") {
                                ?>
                                <p>a. <?=$arr_lha['audit_type_name']?> dilakukan terhadap seluruh unsur manajemen yang meliputi Unsur Keuangan, Unsur Sarana dan Prasarana, termasuk proses pelaksanaan kegiatan belanja modal serta Belanja Barang/jasa tidak mengikat, Unsur Pelaksanaan Tugas dan Fungsi dan Unsur Metode Kerja melalui penilaian terhadap:<br>
                                &nbsp;&nbsp;&nbsp;1) Ketidakpatuhan Terhadap Peraturan;<br>
                                &nbsp;&nbsp;&nbsp;2) Kelemahan Sistem Pengendalian Intern;<br>
                                &nbsp;&nbsp;&nbsp;3) Adanya Unsur 3E (Ketidakekonomisan, Ketidakefisienan; Ketidakefektifan).
                                </p>
                                <p>b. Periode audit adalah <?=$comfunc->dateIndoLengkap($arr_lha['nha_start_date'])?> s.d. <?=$comfunc->dateIndoLengkap($arr_lha['nha_end_date'])?></p>
                                <p>c. Audit dilakukan sesuai dengan standar audit Inspektorat Jenderal Kementerian Perhubungan dan prosedur lain yang dianggap perlu sesuai kondisi.</p>
                                <p>
                                    d. Batasan Audit<br>
                                    <?=$arr_lha['audit_type_name']?> pada <?=$assign_nama_auditee?> dilakukan secara menyeluruh/komprehensif pada semua unsur, namun karena adanya keterbatasan waktu hari pelaksanaan audit, maka audit hanya difokuskan pada unsur/hal-hal yang dianggap penting yang dapat mengakibatkan adanya hambatan terhadap pelaksanaan tugas dan fungsi/kegiatan Belanja Modal serta kegiatan yang rawan akan terjadinya pemborosan/kerugian keuangan Negara.
                                </p>
                                <?
                            } else {
                                ?>
                                <?=$arr_lha['lha_ruanglingkup']?>
                                <?
                            }
                            ?>
                        </textarea>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Pendekatan Audit</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="pendekatan_audit" id="pendekatan_audit">
                            <?
                            if ($arr_lha['lha_pendekatan']=="") {
                                ?>
                                <p><?=$arr_lha['audit_type_name']?> dilakukan dengan cara:<br>
                                a. Meminta informasi umum dari Auditi termasuk permasalahan yang dihadapi;<br>
                                b. Melakukan penelitian dan evaluasi terhadap pengendalian manajemen melalui data administrasi dan tinjauan lapangan;<br>
                                c. Analisis masalah dan perumusan temuan audit;<br>
                                d. Membahas permasalahan-permasalahan yang ditemukan dalam pelaksanaan audit dengan Auditi;<br>
                                e. Pelaporan hasil audit.<br>
                                </p>
                                <?
                            } else {
                                ?>
                                <?=$arr_lha['lha_pendekatan']?>
                                <?
                            }
                            ?>
                        </textarea>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Data Umum</label><br><br>
                        <center>
                            <fieldset>
                                <input type="button" class="green_btn" value="Input Jenis Kegiatan" Onclick="window.open('main_page.php?method=assign_lha_pagu&assign_id=<?=$assign_id?>&auditee_id=<?=$assign_id_auditee?>', '');">
                            </fieldset>
                        </center>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span4">Status Tindak Lanjut Temuan Hasil Audit Sebelumnya</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="hasil_audit_lalu" id="hasil_audit_lalu">
                             <?
							 $jumlah_temuan =  $assignment_nhas->finding_nha_count ( $arr_lha ['nha_id'], $arr_lha['nha_auditee_id'], "", "", "" );
							 
                             if ($arr_lha['lha_tl_audit_lalu']=="") {
                                ?>
                                <p>Berdasarkan Laporan Hasil Audit (LHA) oleh Inspektorat Jenderal Kementerian Perhubungan Nomor .../LHA/.../.../..., tanggal ... ........... ... dengan Surat Pengantar Laporan (SPL) Nomor PS..../.../.../.../ITJEN-..., tanggal ... ........... ...  terdapat .. (...) temuan dengan status tindak lanjut sebagai berikut:<br>
                                </p>
								<br>
								<table align="left" border="1" cellpadding="0" cellspacing="0" style="width:500px">
									<tbody>
										<tr>
											<td colspan="1" rowspan="2" style="text-align:center">Nomor</td>
											<td colspan="2" rowspan="2" style="text-align:center">Judul Temuan</td>
											<td colspan="3" rowspan="1" style="text-align:center">Status Tindak Lanjut</td>
										</tr>
										<tr>
											<td style="text-align:center">Tuntas</td>
											<td style="text-align:center">Proses</td>
											<td style="text-align:center">Belum TL</td>
										</tr>
										<tr>
											<td style="text-align:center">&nbsp;</td>
											<td colspan="2" rowspan="1">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center">&nbsp;</td>
											<td colspan="2" rowspan="1">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center">&nbsp;</td>
											<td colspan="2">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center">&nbsp;</td>
											<td colspan="2">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
										</tr>
										<tr>
											<td style="text-align:center">&nbsp;</td>
											<td colspan="2">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
											<td style="text-align:center">&nbsp;</td>
										</tr>
									</tbody>
								</table>
                                 <?
                             } else {
                                 ?>
                                 <?=$arr_lha['lha_tl_audit_lalu']?>
                                 <?
                             }
                             ?>
                        </textarea>
                    </fieldset>
                </div>
                <div id="view4">
                    <fieldset class="hr">
                        <label class="span2">Temuan Positif</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="hasil_yang_dicapai" id="hasil_yang_dicapai"><?=$arr_lha['lha_hasil']?></textarea>
                    </fieldset>
                </div>
                <div id="view5">
                    <fieldset class="hr">
                        <label class="span2">Penutup</label><br><br>
                        <textarea class="ckeditor" cols="10" rows="40" name="penutup" id="penutup">
                        <?
                        if ($arr_lha['lha_penutup']=="") {
                            ?>
                            Demikian Laporan Hasil <?=$arr_lha['audit_type_name']?> pada <?=$assign_nama_auditee?> propinsi <?=$arr_lha['propinsi_name']?> disampaikan untuk ditindaklanjuti dan dapat digunakan sebagai bahan masukan dalam penetapan kebijakan lebih lanjut.
                            <?
                        } else {
                            ?>
                            <?=$arr_lha['lha_penutup']?>
                            <?
                        }
                        ?>
                        </textarea>
                    </fieldset>
                </div>
                <div id="view6">
                    <fieldset class="hr">
                        <label class="span3">Lampiran</label> <input type="file" class="span4" name="attach[]" id="attach" multiple="" onChange="makeFileList();">
                        <label class="fileList_label"></label>
                        <ul id="fileList">
                            <li>No Files Selected</li>
                        </ul>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span3">List Lampiran</label>
                        <?php
                        $y=0;
                        $rs_file = $assigns->list_lha_lampiran($arr['assign_id']);
                        while($arr_file = $rs_file->FetchRow()){
                            $y++;
                            ?>
                            <input type="checkbox" name="nama_file_<?=$y?>" value="<?=$arr_file['lha_attach_name']?>">
                            <a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_LHA").$arr_file['lha_attach_name']?>','_blank')"><?=$arr_file['lha_attach_name']?></a>, &nbsp;&nbsp;
                            <?php
                        }
                        ?>
                    </fieldset>
                </div>
                <div id="view7">
                    <br>
                    <br>
                    <ul class="rtabs" style="padding:0px 0px 0px 350px;">
                        <li><a href="#view20">Cover</a></li>
                        <li><a href="#view24">Daftar Isi</a></li>
                        <li><a href="#view21">Bab I</a></li>
                        <li><a href="#view22">Bab II</a></li>
                        <li><a href="#view23">Bab III</a></li>
                    </ul>
                    <div id="view20">
                    <div id="printableArea">
                    <table align="center" width="100%">
                        <tr>
                            <td align="center"><br><br><br><strong>LAPORAN HASIL AUDIT<br>PADA<br><?=$assign_nama_auditee?><br>PROVINSI <?=$arr_lha['propinsi_name']?></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><br><br><br><strong>Nomor : <?=$arr_lha['assign_no_lha']?></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>Tanggal :  <?=$comfunc->dateIndo2($arr_lha['assign_date_lha'])?></strong></td>
                        </tr>
                    </table>
                    <br>
                    </div>
                    <br>
                    <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="blue_btn" onclick="printDiv('printableArea')" value="Cover Print" />
                        </center>
                    </div>

                    <div id="view24">
                    <div id="printableDaftarIsi">
                    <table align="center" whidth="100%">
                        <tr>
                            <td align="center" colspan="4"><br><br><br><strong>DAFTAR ISI</strong></td>
                        </tr>
                        <br>
                        <tr>
                            <td width="20%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>BAB I</strong></td>
                            <td align="left" width="55%"><br><br><br><strong>:&nbsp;&nbsp;&nbsp;Pendahuluan</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;&nbsp;&nbsp;&nbsp;1. Dasar Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;2. Tujuan Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;3. Ruang Lingkup Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;4. Pendekatan Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;5. Data Umum<br>&nbsp;&nbsp;&nbsp;&nbsp;6. Status Tindak Lanjut Temuan Hasil Audit Sebelumnya</td>
                        </tr>
                        </td>
                        </tr>
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>BAB II</strong></td>
                            <td align="left" width="55%"><br><br><br><strong>:&nbsp;&nbsp;&nbsp;Hasil Audit</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;&nbsp;&nbsp;&nbsp;1. Temuan Ketidakpatuhan Terhadap Peraturan<br>&nbsp;&nbsp;&nbsp;&nbsp;2. Temuan Kelemahan Sistem Pengendalian Intern<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Kelemahan Sistem Pengendalian Akuntansi Dan Pelaporan<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Kelemahan Sistem Pengendalian Pelaksanaan Anggaran 16
                                Pendapatan Dan Belanja<br>&nbsp;&nbsp;&nbsp;&nbsp;3. Temuan 3E</td>
                        </tr>
                        </td>
                        </tr>
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>BAB III</strong></td>
                            <td align="left" width="55%"><br><br><br><strong>:&nbsp;&nbsp;&nbsp;Penutup</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;</td>
                        </tr>
                        </td>
                        </tr>
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>LAMPIRAN</strong><br><br><br>1. Berita Acara Pemeriksaan Kas;<br>2. Register Penutupan Kas;<br>3. Surat Tugas (ST).</td>
                            <td align="left" width="55%"><br><br><br><strong>&nbsp;&nbsp;&nbsp;</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;</td>
                        </tr>
                        </td>
                        </tr>
                    </table>
                        </div>
                        <br>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="blue_btn" onclick="printDiv('printableDaftarIsi')" value="Daftar Isi Print" />
                        </center>
                    </div>
                    
                    <div id="view21">
                        <div id="printableAreaBabI">
                        <br>
                        <table align="center" width="100%">
                            <tr>
                                <td align="justify"><?=$comfunc->text_show($arr_lha['lha_ringkasan'])?></td>
                            </tr>
                        </table>

                        <br>

                        <table align="center" width="100%">
                            <tr>
                                <td align="center" colspan="4"><strong>BAGIAN KEDUA</strong></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4"><strong>URAIAN HASIL AUDIT</strong><br></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4"><strong>BAB I</strong></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4"><strong>PENDAHULUAN</strong></td>
                            </tr>
                            <tr>
                                <td width="2%">1.</td>
                                <td colspan="2">Dasar Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_dasar_audit'])?></td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td colspan="2">Tujuan Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_tujuan_audit'])?></td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td colspan="2">Ruang Lingkup Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_ruanglingkup'])?></td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td colspan="2">Pendekatan Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_pendekatan'])?></td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td colspan="2">Data Umum</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify">
                                    <table align="left" width="100%">
                                        <tr>
                                            <td width="15%">a. Nama Auditi</td>
                                            <td align="left" width="55%">:&nbsp;&nbsp;&nbsp;<?=$assign_nama_auditee?></td>
                                        </tr>
                                        <tr>
                                            <td>b. Alamat</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_alamat'])?></td>
                                        </tr>
                                        <tr>
                                            <td>c. Nomor Telepon</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_telp'])?>,&nbsp;<?=$comfunc->text_show($arr_lha['auditee_fax'])?>&nbsp;(Fax)</td>
                                        </tr>
                                        <tr>
                                            <td>d. Kode Kantor</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_kode'])?></td>
                                        </tr>
                                        <tr>
                                            <td>e. Sumber Dana dan Pagu</td>
                                            <td align="left"></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <?php
                                    $no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
                                    ?>
                                    <table width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                        <tr>
                                            <td width="20%"><?=$no_pagu;?>)&nbsp;Tahun&nbsp;<?=$arr_pagu['lha_pagu_tahun']?></td>
                                            <td>:&nbsp;&nbsp;DIPA No : <?=$arr_pagu['lha_pagu_no_dipa']?>, Tanggal <?=$comfunc->dateIndo($arr_pagu['lha_pagu_date'])?></td>
                                        <tr>
                                        <tr>
                                            <td></td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_nilai']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 13px; margin:0px;">a) Belanja Pegawai</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_pegawai']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 13px; margin:0px;">b) Belanja Barang</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_barang']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 13px; margin:0px;">c) Belanja Modal</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_modal']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 25px; margin:0px;">Realisasi Keuangan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_realisasi']?>,-&nbsp;(<?=$arr_pagu['lha_pagu_persentase']?>)</td>
                                        <tr>
                                    </table>
                                            <?
                                        }
                                    ?>
                                    <br>
                                    <!-- <table align="left" width="100%">
                                        <tr>
                                            <td width="15%">f. Pejabat Audit</td>
                                            <td align="left" width="56%">:&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                    <br> -->
									<?php
                                    $no = e;
                                    $no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
                                            $no_pic=0;
											$rs_pic = $params->pic_viewlist_lha($arr_pagu['lha_pagu_id']);
											while($arr_pic = $rs_pic->FetchRow()){
												$no_pic++;
												if ($arr_pic['pic_jabatan_id'] == '80897dc5580beac2752b3da6c0b5b86581205588') {
										?>
									<!-- <table width="100%" align="left" style="margin:0px 0px 0px 12px;"> -->
									<table width="100%" align="left">
                                            <tr>
                                                <td width="20%"><?php echo ++$no.".&nbsp;" ?>&nbsp;<?=$arr_pic['jabatan_pic_name']?></td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">a. Nama</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_name']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">b. NIP</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_nip']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">c. Pangkat</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pangkat_lengkap']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">d. Jabatan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pangkat_desc']?></td>
                                            <tr>
                                    </table>
									<?
									} else {
									?>

                                        <table width="100%" align="left">
                                            <tr>
                                                <td width="20%"><?php echo ++$no.".&nbsp;" ?>&nbsp;<?=$arr_pic['jabatan_pic_name']?></td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">a. Nama</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_name']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">b. NIP</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_nip']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">c. Pangkat</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pangkat_lengkap']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">d. Jabatan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pangkat_desc']?></td>
                                            <tr>
                                        </table>
                                        <?
												}
											}
										}
                                    ?>
                                    </table>
                                    <table align="left" width="100%">
                                        <tr>
                                            <td width="15%">6. KPPN</td>
                                            <td align="left" width="55%">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_kppn'])?></td>
                                        </tr>
                                        <tr>
                                            <td>7. Bank dan Nomor Rekening</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_bank'])?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0px 0px 82px 0px; 0px;">8. Fasilitas Utama</td>
                                            <td align="left"><?=$comfunc->text_show($arr_lha['auditee_fasilitas'])?></td>
                                        </tr>
										<tr>
                                            <td>9. Jumlah Pegawai</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_jml_pegawai'])?> Orang</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">10. Jenis Kegiatan Belanja Modal dan Belanja Barang/Jasa Tidak Mengikat T.A :</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <?php
                                    $no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
                                            $no_damu=0;
                                            $rs_damu = $assigns->assign_damu_view_lha($arr_pagu['lha_pagu_id']);
                                            while($arr_damu = $rs_damu->FetchRow()){
                                                $no_damu++;
                                        ?>
                                        <table align="left" width="100%" style="padding:0px 0px 0px 9px; margin:0px;">
                                            <tr>
                                                <td width="15%"><u>Tahun&nbsp;<?=$arr_pagu['lha_pagu_tahun']?></u></td>
                                                <td align="left" width="55%">&nbsp;&nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                        <table width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                            <tr>
                                                <td width="20%"><?=$no_damu?>)&nbsp;Pekerjaan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_pekerjaan']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nomor Kontrak/SPK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_no_kontrak']?>&nbsp;tanggal&nbsp;<?= $comfunc->dateIndoLengkap($arr_damu['lha_damu_date'])?></td>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nilai Kontrak/SPK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;Rp.&nbsp;<?= $comfunc->format_uang($arr_damu['lha_damu_nilai'])?>,-</td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nama Penyedia barang/Jasa</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_nama']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Jangka Waktu Pelaksanaan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_jangka']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Realisasi Fisik (%)</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_fisik']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Realisasi Keuangan (%)</td>
                                                <td>:&nbsp;&nbsp;&nbsp;Rp.&nbsp;<?= $comfunc->format_uang($arr_damu['lha_damu_keuangan'])?>,-&nbsp;(<?=$arr_damu['lha_damu_persentase']?>%)</td>
                                            <tr>
                                        </table>
                                        <?
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td colspan="2">Tindak Lanjut Temuan Hasil Audit Yang Lalu</td><br>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2" text-align="justify"><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$comfunc->text_show($arr_lha['lha_tl_audit_lalu'])?></td>
                            </tr>
                        </table>
                        </div>
                        <br>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="blue_btn" onclick="printDiv('printableAreaBabI')" value="Bab I Print" />
                        </center>
                    </div>
                    <br>
                    <div id="view22">
                        <div id="printableAreaBabII">
                        <br>
                        <br>
                        <center>
                            <strong>BAB II<br>HASIL AUDIT</strong>
                        </center>
                        <table align="center" width="100%">
                            <tr>
                                <td colspan="5" align="justify"><?=$comfunc->text_show($arr_lha['lha_hasil'])?></td>
                            </tr>
                            <tr>
                                <td width="2%"><b>1.</b></td>
                                <td colspan="4"><b>Temuan Ketidakpatuhan Terhadap Peraturan</b></td>
                            </tr>
                            <?
                            // Temuan Ketidakpatuhan Terhadap Peraturan
                            $rs_temuan_proses = $findings->get_temuan_proses_1 ( $arr_spl['assign_id']);
                            $no_tem = "a";
                            while($arr_temuan_proses = $rs_temuan_proses->FetchRow()) {
                                ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td width="2%"><b><?= $no_tem ?>.</b></td>
                                        <td colspan="3"><strong><?= $arr_temuan_proses['finding_judul'] ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="3"><strong>1) Uraian </strong></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_kondisi']) ?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="3"><strong>2) Sebab & Akibat :</strong></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_sebab']) ?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="3"><strong>3) Tanggapan Auditan :</strong></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_tanggapan_auditee']) ?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="3"><strong>4) Rekomendasi :</strong></td>
                                    </tr>
                                    <?
                                    $rs_rekomendasi_proses = $findings->get_rekomendasi_proses($arr_temuan_proses['finding_id']);
                                    $count_rek = $rs_rekomendasi_proses->RecordCount();
                                    $no_rek = 0;
                                    while ($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()) {
                                        $no_rek++;
                                        ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="2"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc']) ?></td>
                                        </tr>
                                        <?
                                        $no_tem++;
                                }
                            }
                            ?>
                            <?
                            $rs_temuan_proses2 = $findings->get_temuan_proses_1 ( $arr_spl['assign_id']);
                            $arr_temuan_proses2 = $rs_temuan_proses2->FetchRow();
                            if ($arr_temuan_proses2['finding_id'] == "") {
                                ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><?= $comfunc->text_show("Setelah dilakukan audit terhadap Aspek Temuan Ketidakpatuhan Terhadap Peraturan tidak terdapat permasalahan yang perlu diangkat sebagai temuan hasil audit") ?></td>
                                </tr>
                                <?
                            }
                            ?>
                            <br>
                            <br>
                            <br>
                            <tr>
                                <td width="2%"><b>2.</b></td>
                                <td colspan="4"><b>Temuan Kelemahan Sistem Pengendalian Intern</b></td>
                            </tr>
                            <?
                            // Temuan Kelemahan Sistem Pengendalian Internal
                            $rs_temuan_proses = $findings->get_temuan_proses_2 ( $arr_spl['assign_id']);
                            $no_tem = "a";
                            while($arr_temuan_proses = $rs_temuan_proses->FetchRow()) {
                                ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="2%"><b><?= $no_tem ?>.</b></td>
                                    <td colspan="3"><strong><?= $arr_temuan_proses['finding_judul'] ?></strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>1) Uraian </strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_kondisi']) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>2) Sebab & Akibat :</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_sebab']) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>3) Tanggapan Auditan :</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_tanggapan_auditee']) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>4) Rekomendasi :</strong></td>
                                </tr>
                                <?
                                    $rs_rekomendasi_proses = $findings->get_rekomendasi_proses($arr_temuan_proses['finding_id']);
                                    $count_rek = $rs_rekomendasi_proses->RecordCount();
                                    $no_rek = 0;
                                    while ($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()) {
                                        $no_rek++;
                                        ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="2"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc']) ?></td>
                                        </tr>
                                        <?
                                        $no_tem++;
                                    }
                                }
                            ?>
                            <?
                            $rs_temuan_proses2 = $findings->get_temuan_proses_2 ( $arr_spl['assign_id']);
                            $arr_temuan_proses2 = $rs_temuan_proses2->FetchRow();
                            if ($arr_temuan_proses2['finding_id'] == "") {
                                ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><?= $comfunc->text_show("Setelah dilakukan audit terhadap Aspek Temuan Kelemahan Sistem Pengendalian Intern tidak terdapat permasalahan yang perlu diangkat sebagai temuan hasil audit") ?></td>
                                </tr>
                                <?
                            }
                            ?>
                            <br>
                            <br>
                            <br>
                            <tr>
                                <td width="2%"><b>3.</b></td>
                                <td colspan="4"><b>Temuan 3E (Ketidakekonomisan, Ketidakefisienan, Ketidakefektifan)</b></td>
                            </tr>
                            <?
                            // Temuan 3E
                            $rs_temuan_proses = $findings->get_temuan_proses_3 ( $arr_spl['assign_id']);
                            $no_tem = "a";
                            while($arr_temuan_proses = $rs_temuan_proses->FetchRow()) {
                                ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="2%"><b><?= $no_tem ?>.</b></td>
                                    <td colspan="3"><strong><?= $arr_temuan_proses['finding_judul'] ?></strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>1) Uraian </strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_kondisi']) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>2) Sebab & Akibat :</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_sebab']) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>3) Tanggapan Auditan :</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_tanggapan_auditee']) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><strong>4) Rekomendasi :</strong></td>
                                </tr>
                                <?
                                    $rs_rekomendasi_proses = $findings->get_rekomendasi_proses($arr_temuan_proses['finding_id']);
                                    $count_rek = $rs_rekomendasi_proses->RecordCount();
                                    $no_rek = 0;
                                    while ($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()) {
                                        $no_rek++;
                                        ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="2"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc']) ?></td>
                                        </tr>
                                        <?
                                        $no_tem++;
                                }
                            }
                            ?>
                            <?
                            $rs_temuan_proses2 = $findings->get_temuan_proses_3 ( $arr_spl['assign_id']);
                            $arr_temuan_proses2 = $rs_temuan_proses2->FetchRow();
                            if ($arr_temuan_proses2['finding_id'] == "") {
                                ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><?= $comfunc->text_show("Setelah dilakukan audit terhadap Aspek Temuan 3E (Ketidakekonomisan, Ketidakefisienan, Ketidakefektifan) tidak terdapat permasalahan yang perlu diangkat sebagai temuan hasil audit") ?></td>
                                </tr>
                                <?
                            }
                            ?>

                        </table>
                        </div>
                        <br>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="blue_btn" onclick="printDiv('printableAreaBabII')" value="Bab II Print" />
                        </center>
                    </div>
                    <br>
                    <div id="view23">
                        <div id="printableAreaBabIII">
                        <br>
                        <table align="center" width="100%">
                            <tr>
                                <td colspan="3" align="center"><strong>BAB III</strong></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center"><strong>PENUTUP</strong></td>
                            </tr>
                            <tr>
                                <td colspan="5" align="justify"><?=$comfunc->text_show($arr_lha['lha_penutup'])?></td>
                            </tr>
                        </table>

                        <table align="center" width="75%">
                            <tr>
                                <td colspan="3" align="right">Jakarta, <?=$comfunc->dateIndoLengkap($arr_lha['lha_date'])?></td>
                            </tr>
                        </table>

                    <br>

                        <table align="center" width="100%">
                            <tbody>
                            <tr>
                                <?
                                // get pengendali mutu
                                $rs_pm = $assigns->view_auditor_pm_lha($assign_id, $auditee_id);
                                $arr_pm = $rs_pm->FetchRow();
                                ?>
                                <td width="30%" align="center" style="text-align: center; margin-top: 0px; margin-bottom: 0px; padding-bottom: 402px;">
                                Mengetahui<br>
                                Pengendali Mutu<br><br><br><br><br><br>

                                <u><?=$arr_pm ['auditor_name']?></u><br>
                                <?=$arr_pm ['pangkat_desc']?> (<?=$arr_pm['pangkat_name']?>)<br>
                                NIP. <?=$arr_pm ['auditor_nip']?>
                                </td>

                                <td width="80%" align="center">
                                    <table border="0" width="60%" align="right">
                                        <thead>
                                        <tr>
                                            <td width="30%" align="center" style="height:146px;">Tim Audit</td>
                                            <td width="30%">&nbsp;</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no_auditor="";
                                        $rs_auditor = $assigns->view_auditor_lha ( $assign_id, $auditee_id);
                                        while ( $arr_audtor = $rs_auditor->FetchRow () ) {
                                            $no_auditor++;
                                            ?>
                                            <tr class="row_item">
                                                <td>
                                                <?=$no_auditor?>.&nbsp;<u><?=$arr_audtor ['auditor_name']?></u><br>
                                                    <?=$arr_audtor ['pangkat_desc']?> (<?=$arr_audtor['pangkat_name']?>)<br>
                                                    NIP. <?=$arr_audtor ['auditor_nip']?>
                                                    <br><br><br><br><br><br>

                                                </td>
                                                <td><?=$arr_audtor ['posisi_name']?><br><br><br><br><br><br><br><br></td>
                                            </tr>
                                            <?
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        <br>
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify">
                                    <br><br><br><strong>Status LHA :
                                        <?
                                        if($arr_lha ['lha_status']==0){
                                            echo "Belum Diajukan";
                                        } else if($arr_lha ['lha_status']==1){
                                            echo "Sudah Diajukan Katim atau Dalnis, Sedang Direviu Oleh Daltu";
                                        } else if($arr_lha ['lha_status']==2){
                                            echo "Telah Disetujui Daltu";
                                        } else if($arr_lha ['lha_status']==3){
                                            echo "Ditolak oleh Pengendali Mutu";
                                        }
                                        ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify">
                                    <table>
                                        <tr>
                                            <td>Detail komentar</td>
                                            <td>:</td>
                                            <td>
										<?php
										$z = 0;
										$rs_komentar = $assigns->lha_komentar_viewlist ( $arr_lha ['lha_id'] );
										while ( $arr_komentar = $rs_komentar->FetchRow () ) {
											$z ++;
											echo $z.". ".$arr_komentar['auditor_name']." : ".$arr_komentar['lha_comment_desc']."<br>";
										}
										?>
										</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify">
                                    Komentar :<br>
                                    <textarea id="komentar" name="komentar" rows="1" cols="20" style="width: 475px; height: 3em; font-size: 11px;"></textarea>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="blue_btn" onclick="printDiv('printableAreaBabIII')" value="Bab III Print" />
                        </center>
                    </fieldset>
            <br>
            <br>
                </div>
                <div id="view8">
                    <? include "matriks_temuan.php";?>
                </div>
                <fieldset>
                    <center>
                        <input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'">
						&nbsp;&nbsp;&nbsp;
                        <!--<input type="button" class="blue_btn" value="Hasil Audit Print" onclick="window.open('AuditManagement/hasil_audit_print.php?id_assign=<?=$arr['assign_id']?>&id_auditee=<?= $arr_auditee['auditee_id'] ?>','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">-->
						&nbsp;&nbsp;&nbsp;
                        <input type="button" class="blue_btn" onclick="printDiv('printableArea2')" value="Matriks Temuan Print" />
                        <!--<input type="button" class="blue_btn" value="Matriks Temuan MS-Word" onclick="window.open('AuditManagement/matriks_temuan_print.php?id_assign=<?//=$arr['assign_id']?>//&id_auditee=<?//= $arr_auditee['auditee_id'] ?>//','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">-->
						&nbsp;&nbsp;&nbsp;
                        <input type="button" class="blue_btn"  onclick="printDiv('printableArea3')" value="Cetak SPL Print" />
						<!--<input type="button" class="blue_btn" value="Cetak SPL MS-Word" onclick="window.open('AuditManagement/cetak_spl_word.php?id_assign=<?=$arr['assign_id']?>&id_auditee=<?= $arr_auditee['auditee_id'] ?>','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">-->
						<?
                        //0=default, 1=ajukan, 2=approve_dalnis, 3=approve_daltu, 4=approve_inspektur, 5=tolak_dalnis, 6=tolak_daltu, 7=tolak_inspektur
                        //$cek_posisi='8918ca5378a1475cd0fa5491b8dcf3d70c0caba7';
                        if($cek_posisi=='8918ca5378a1475cd0fa5491b8dcf3d70c0caba7' || $cek_posisi=='9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd'){ //katim/dalni
                            if ($arr_lha ['lha_status'] == '' || $arr_lha ['lha_status'] == '0' || $arr_lha ['lha_status'] == '3') {
                                echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" class=\"blue_btn\" value=\"Simpan\">";
                                echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" class=\"blue_btn\" value=\"Simpan Dan Ajukan\" onclick=\"document.getElementById('status_lha').value=1\">";
                            }
                        }  else if($cek_posisi=='1fe7f8b8d0d94d54685cbf6c2483308aebe96229'){ //daltu
                            if ($arr_lha ['lha_status'] == '1') {
                                echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" class=\"blue_btn\" value=\"Tolak Pengajuan\" onclick=\"document.getElementById('status_lha').value=3\">";
                                echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" class=\"blue_btn\" value=\"Setujui\" onclick=\"document.getElementById('status_lha').value=2\">";
                            }
                        } else { //inspektur
                            if ($status_lha == '3') {
                                echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" class=\"blue_btn\" value=\"Tolak Pengajuan\" onclick=\"document.getElementById('status_lha').value=7\">";
                                echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" class=\"blue_btn\" value=\"Setujui\" onclick=\"document.getElementById('status_lha').value=4\">";
                            }
                        }
                        ?>
                        <input type="hidden" name="status_lha" id="status_lha" value="<?=$arr_lha['lha_status']?>">
                        <input type="hidden" name="lha_auditee_id" value="<?=$assign_id_auditee?>">
                        <input type="hidden" name="data_id" value="<?=$arr['assign_id']?>">
                        <input type="hidden" name="lha_id" value="<?=$lha_id?>">
                        <input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
                    </center>
                </fieldset>
        </form>
    </article>
</section>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();
        location.reload();

        document.body.innerHTML = originalContents;
    }

    $('#simpan_histori').click(function(){
        var data = $("#validation-form").serialize();
        $.ajax({
            url: 'AuditManagement/post_histori_lha.php',
            type: 'POST',
            data: data,
            success: function(data) {
                location.reload();
                alert('Berhasil menyimpan Histori LHA!');
            }
        });
    });
    function cek_data(){
        var data = document.getElementById('status_lha').value;
        if(data==1) text = "Anda Yakin Akan Mengajukan?";
        if(data==2) text = "Anda Yakin Akan Menyetujui?";
        return confirm(text);
    }
    function makeFileList() {
        var input = document.getElementById("attach");
        var label = document.getElementById("fileList_label");
        var ul = document.getElementById("fileList");
        while (ul.hasChildNodes()) {
            ul.removeChild(ul.firstChild);
        }
        for (var i = 0; i < input.files.length; i++) {
            var li = document.createElement("li");
            label.text(input.files[i].name);
            li.innerHTML = input.files[i].name;
            ul.appendChild(li);
        }
        if(!ul.hasChildNodes()) {
            var li = document.createElement("li");
            li.innerHTML = 'No Files Selected';
            ul.appendChild(li);
        }
    }
    $("#tanggal_lha").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });
    $("#spl_date").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });
    $(function() {
        $("#validation-form").validate({
            rules: {
                tanggal_lha: "required",
                no_lha: "required",
                tgl_spl: "required"
            },
            messages: {
                tanggal_lha: "Silahkan Masukkan Tanggal LHA",
                no_lha: "Silahkan Masukkan Nomor LHA",
                tgl_spl: "Silahkan Masukkan Tanggal SPL"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    $("#update").click(function(e) {
        $.ajax({
            url: 'AuditManagement/ajax.php?data_action=postmatrix',
            type: 'POST',
            data: {
                assign_id : $("#assign_id").val(),
                <?php
                $rs_list_temuans = $assigns->temuan_list_matrix($assign_id);
                $no_rek = 0;
                foreach ($rs_list_temuans as $temuan) {
                $no_rek++;
                ?>
                finding_id_<?=$temuan['finding_id']?>: $('#finding_id_<?= $temuan['finding_id'] ?>').val(),
                finding_kondisi_<?=$temuan['finding_id']?>: CKEDITOR.instances.finding_kondisi_<?=$temuan['finding_id']?>.getData(),
                finding_sebab_<?=$temuan['finding_id']?>: CKEDITOR.instances.finding_sebab_<?=$temuan['finding_id']?>.getData(),
                finding_akibat_<?=$temuan['finding_id']?>: CKEDITOR.instances.finding_akibat_<?=$temuan['finding_id']?>.getData(),
                <?
                $rs_list_reks = $findings->get_rekomendasi_list_matrix($temuan['finding_id']);
                foreach ($rs_list_reks as $rekomendasi) {
                ?>
                rekomendasi_id_<?=$rekomendasi['rekomendasi_id']?>: $('#rekomendasi_id_<?= $rekomendasi['rekomendasi_id'] ?>').val(),
                rekomendasi_desc_<?=$rekomendasi['rekomendasi_id']?>: CKEDITOR.instances.rekomendasi_desc_<?=$rekomendasi['rekomendasi_id']?>.getData(),
                <?
                }
                }
                ?>
            },
            success: function(data) {
                location.reload();
                alert('Berhasil mengubah Matriks Temuan!');
            }
        });
    });
    function download(idAssign, lhaId, auditeeId){
        window.open("AuditManagement/hasil_audit_pdf.php?id_assign="+idAssign+"&lha_id="+lhaId+"&id_auditee="+auditeeId);
    }
</script>	