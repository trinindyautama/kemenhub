<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <header>
            <h3 class="tabs_involved"><?=$page_title?></h3>
        </header>
        <form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
            <?
            switch ($_action) {
                case "getadd" :
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Inpektorat</label>
                        <?
                        $rs_ins = $assignment_nhas->inspektorat_data_viewlist ();
                        $arr_ins = $rs_ins->GetArray ();
                        echo $comfunc->buildCombo ( "inspektorat_id", $arr_ins, 0, 1, "", "insOnChange(this.value, 'no_spt', 'auditee_id')", "", false, true, false );
                        ?>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">No ST</label>
                        <select name="no_spt" id="no_spt" onchange = "return stOnChange(this.value, 'auditee_id')">
                            <option value="">Pilih Satu</option>
                        </select>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Obyek Pemeriksaan</label>
                        <select name="auditee_id" id="auditee_id">
                            <option value="">Pilih Satu</option>
                        </select>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Provinsi</label>
                        <label class="span2" id="propinsi_id">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Periode Audit</label>
                        <input type="text" class="span1" name="tanggal_awal" id="tanggal_awal">
                        <label class="span0">s/d</label>
                        <input type="text" class="span1" name="tanggal_akhir" id="tanggal_akhir">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <?
                    break;
                case "getedit" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Inpektorat</label>
                        <?
                        $rs_ins = $assignment_nhas->inspektorat_data_viewlist ();
                        $arr_ins = $rs_ins->GetArray ();
                        echo $comfunc->buildCombo ( "inspektorat_id", $arr_ins, 0, 1, $arr['nha_inspektorat_id'], "insOnChange(this.value, 'no_spt', 'auditee_id')", "", false, true, false );
                        ?>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">No ST</label>
                        <?
                        $rs_st = $assignment_nhas->st_data_viewlist ($arr['nha_inspektorat_id']);
                        $arr_st = $rs_st->GetArray ();
                        echo $comfunc->buildCombo ( "no_spt", $arr_st, 0, 2, $arr['nha_no_spt'], "stOnChange(this.value, 'auditee_id')", "", false, true, false );
                        ?>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Obyek Pemeriksaan</label>
                        <?
                        $rs_op = $assignment_nhas->assign_auditee_viewlist ($arr['nha_no_spt']);
                        $arr_op = $rs_op->GetArray ();
                        echo $comfunc->buildCombo ( "auditee_id", $arr_op, 0, 1, $arr['nha_auditee_id'], "", "", false, true, false );
                        ?>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Provinsi</label>
                        <label class="span2" name="propinsi_id" id="propinsi_id"><?=$arr['propinsi_name'];?></label>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Periode Audit</label>
                        <input type="text" class="span1" name="tanggal_awal" id="tanggal_awal" value="<?=$comfunc->dateIndo($arr['nha_start_date'])?>">
                        <label class="span0">s/d</label>
                        <input type="text" class="span1" name="tanggal_akhir" id="tanggal_akhir" value="<?=$comfunc->dateIndo($arr['nha_start_date'])?>">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['nha_id']?>">
                    <?
                    break;
                case "getdetail" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <table class="view_parrent">
                            <tr>
                                <td>Kategori</td>
                                <td>:</td>
                                <td><?=$arr['assign_eks_kategori2']?></td>
                            </tr>
                            <tr>
                                <td>No LHP</td>
                                <td>:</td>
                                <td><?=$arr['assign_eks_no']?></td>
                            </tr>
                            <tr>
                                <td>Judul LHP</td>
                                <td>:</td>
                                <td><?=$arr['assign_eks_judul']?></td>
                            </tr>
                            <tr>
                                <td>Tanggal LHP</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr['assign_eks_tanggal_lhp'])?></td>
                            </tr>
                            <tr>
                                <td>Tahun Audit</td>
                                <td>:</td>
                                <td><?=$arr['assign_eks_tahun_audit']?></td>
                            </tr>
                            <tr>
                                <td>Tanggal audit</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr['assign_eks_tanggal_awal'])?> s.d <?=$comfunc->dateIndo($arr['assign_eks_tanggal_akhir'])?></td>
                            </tr>
                            <tr>
                                <td>lampiran</td>
                                <td>:</td>
                                <td><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Eksternal").$arr['assign_eks_attach']?>','_blank')"><?=$arr['assign_eks_attach']?></a></td>
                            </tr>
                        </table>
                    </fieldset>

                    <?
                    break;
            }
            ?>
            <fieldset>
                <center>
                    <input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
                    <input type="submit" class="blue_btn" value="Simpan">
                </center>
                <input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
            </fieldset>
        </form>
    </article>
</section>
<script>
    $(function() {
        $("#validation-form").validate({
            rules: {
                inspektorat_id: "required",
                auditee_id: "required",
                no_spt: "required",
                tanggal_awal: "required",
                tanggal_akhir: "required"
            },
            messages: {
                inspektorat_id: "Silahkan Pilih Inspektorat!",
                auditee_id: "Silahkan Pilih Obyek Pemeriksaan!",
                no_spt: "Silahkan Masukan Nomor SPT!",
                tanggal_awal: "Silahkan Masukan Tanggal Awal!",
                tanggal_akhir: "Silahkan Masukan Tanggal Akhir!"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

    $("#tanggal_awal").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });

    $("#tanggal_akhir").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });

    function selectRemoveAll(objSel) {
        document.getElementById(objSel).options.length = 0;
    }

    function selectAdd(objSel, objVal, objCap, isSelected) {
        var nextLength = document.getElementById(objSel).options.length;
        document.getElementById(objSel).options[nextLength] = new Option(objCap, objVal, false, isSelected);
    }

    function insOnChange(objValue, cmbNext_1, cmbNext_2){
        objSel_1 = cmbNext_1;
        selectRemoveAll(objSel_1);

        objSel_2 = cmbNext_2;
        selectRemoveAll(objSel_2);
        selectAdd(objSel_1, "", "Pilih Satu");
        selectAdd(objSel_2, "", "Pilih Satu");
        switch (objValue) {
        <?
            $rs1 = $assignment_nhas->inspektorat_data_viewlist ();
            $arr1 = $rs1->GetArray();
            $rs1->Close();
            foreach ($arr1 as $value1) {
                echo("case \"$value1[0]\":\n");
                $rs2 = $assignment_nhas->st_data_viewlist($value1[0]);
                $arr2 = $rs2->GetArray();
                $rs2->Close();
                foreach ($arr2 as $value2) {
                    $isSelected="false";
                    echo("\tselectAdd(objSel_1, \"$value2[0]\", \"$value2[2]\", $isSelected);\n");
                }
                echo("\tbreak;\n");
            }
            ?>
        }
    }

    function stOnChange(objValue, cmbNext){
        objSel = cmbNext;
        selectRemoveAll(objSel);

        selectAdd(objSel, "0", "Pilih Satu");
        switch (objValue) {
        <?
            $rs1 = $assignment_nhas->st_data_viewlist();
            $arr1 = $rs1->GetArray();
            $rs1->Close();
            foreach ($arr1 as $value1) {
                echo("case \"$value1[0]\":\n");
                $rs2 = $assignment_nhas->assign_auditee_viewlist($value1[0]);
                $arr2 = $rs2->GetArray();
                $rs2->Close();
                foreach ($arr2 as $value2) {
                    $isSelected="false";
                    echo("\tselectAdd(objSel, \"$value2[0]\", \"$value2[1]\", $isSelected);\n");
                }
                echo("\tbreak;\n");
            }
            ?>
        }
    }

    $("#auditee_id").on('change', function(){
        var id_auditee = $(this).val();
        $.ajax({
            url: 'AuditManagement/ajax.php?data_action=getpropinsi',
            type: 'POST',
            dataType: 'text',
            data: {id_auditee: id_auditee},
            success: function(data) {
                $("#propinsi_id").text(data);
            }
        });
    });
</script>