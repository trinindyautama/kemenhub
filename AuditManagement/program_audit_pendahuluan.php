<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.loadTemplate-1.4.1.min.js"></script>
<?php
include_once "_includes/classes/report_class.php";

$reports = new report ($ses_userId);
$rs_auditee = $assigns->assign_auditee_viewlist($ses_assign_id, $auditee_id);
$arr_auditee = $rs_auditee->FetchRow();
$intro = $programaudits->intro_pka_viewlist("", $ses_assign_id, "PKA Pendahuluan");
$intros = $intro->FetchRow();
$katim = $reports->get_anggota($ses_assign_id, $auditee_id, 'KT');
$dalnis = $reports->get_anggota($ses_assign_id, $auditee_id, 'PT');
$dalmut = $reports->get_anggota($ses_assign_id, $auditee_id, 'PM');
?>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <table border='0' class="table_report" cellspacing='0' cellpadding="0">
            <tr>
                <td colspan="8">
                    <center>
                        <img src="images/kka-logo1.png" width="55" height="49" style="width:55pt;height:48pt;"/><br>
                        INSPEKTORAT KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA
                    </center>
                    <br>
                </td>
            </tr>
            <tr>
                <td width="5%" style="border-top:1px solid">&nbsp;</td>
                <td width="5%" style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td width="10%" style="border-top:1px solid">No PKA</td>
                <td align="center" style="border-top:1px solid">:</td>
                <td style="border-top:1px solid"><?= $intros['pka_intro_no_pka'] ?></td>
                <td style="border-top:1px solid;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Nama Auditan</td>
                <td align="center">:</td>
                <td><?= $arr_auditee['auditee_name']; ?></td>
                <td>Ref. PKA No</td>
                <td align="center">:</td>
                <td>-</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"">Sasaran Audit</td>
                <td align="center">:</td>
                <td><?= $arr['audit_type_name']; ?></td>
                <td>Disusun Oleh</td>
                <td align="center">:</td>
                <td><?= $katim['nama'] ?></td>
                <td>Paraf :</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Tanggal</td>
                <td align="center">:</td>
                <td><?=$comfunc->dateIndoLengkap($intros['pka_intro_arranged_date'])?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Periode Audit</td>
                <td align="center">:</td>
                <td>TA. <?= $arr['assign_tahun']-1 ?>-<?= $arr['assign_tahun'] ?></td>
                <td>Direviu Oleh</td>
                <td align="center">:</td>
                <td><?= $dalnis['nama'] ?></td>
                <td>Paraf :</td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid;">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">Tanggal</td>
                <td style="border-bottom:1px solid" align="center">:</td>
                <td style="border-bottom:1px solid"><?=$comfunc->dateIndoLengkap($intros['pka_intro_reviewed_date'])?></td>
                <td style="border-bottom:1px solid;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8" align="center"><br><strong>PROGRAM KERJA AUDIT PENDAHULUAN</strong></td>
            </tr>
            <tr>
                <td colspan="8"><strong>Tujuan Audit :</strong> <?=$intros['pka_intro_tujuan']?></td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid;">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8"><strong>A. Langkah Kerja :</strong></td>
            </tr>
        </table>
        <table border='1' class="table_risk" cellspacing='0' cellpadding="0">
            <tr align="center">
                <th width="2%" rowspan="2">No.</th>
                <th width="24%" rowspan="2" colspan="2">Langkah Kerja Audit</th>
                <th width="16%" colspan="2">Dilaksanakan Oleh</th>
                <th width="16%" colspan="2">Waktu yang Diperlukan</th>
                <th width="9%" rowspan="2">Nomor KKA</th>
                <th width="31%" rowspan="2">Catatan</th>
            </tr>
            <tr align="center">
                <th>Rencana</th>
                <th>Realisasi</th>
                <th>Rencana</th>
                <th>Realisasi</th>
            </tr>
            <tbody>
            <?php
            $no_aspek = "1";
            $rs_aspek = $programaudits->get_assign_aspek("",$arr['assign_id'], $arr_auditee['assign_auditee_id_auditee'], "PKA Pendahuluan");
            while ($arr_aspek = $rs_aspek->FetchRow()) {
                ?>
                <tr>
                    <td><b><?= $comfunc->KonDecRomawi($no_aspek) ?></b></td>
                    <td colspan="2"><b><?= $arr_aspek['aspek_name'] ?></b></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <?
                $no = "0";
                $rs_program = $reports->assign_program_audit_list($arr_aspek['aspek_id'], $arr['assign_id'], $arr_auditee['assign_auditee_id_auditee'], "PKA Pendahuluan");
                while ($arr_program = $rs_program->FetchRow()) {
                    $no++;
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2"><?= $comfunc->text_show($arr_program['ref_program_procedure']) ?></td>
                        <td><?= $arr_program['auditor_name'] ?></td>
                        <td><?= $arr_program['auditor_name'] ?></td>
                        <td align="center"><?= $arr_program['program_jam'] ?> Jam</td>
                        <td align="center"><?=(null != $arr_program['kertas_kerja_jam']) ? $arr_program['kertas_kerja_jam']." Jam" : "" ?></td>
                        <td><?= $arr_program['kertas_kerja_no'] ?></td>
                        <td><div style="overflow-x: auto; width: 500px">
                            <?= $comfunc->text_show($arr_program['kertas_kerja_desc']) ?>
                        </div></td>
                    </tr>
                    <?
                }
                $no_aspek++;
            }
            ?>
            </tbody>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td style="border-style: none; border-bottom-style: none" colspan="4">&nbsp;&nbsp;<b>Keterangan : </b><?= $intros['pka_keterangan'] ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Jakarta, <?=$comfunc->dateIndoLengkap($intros['pka_intro_arranged_date'])?></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td><td width="30%">Disetujui</td><td width="30%">Direviu</td><td width="30%">Disusun oleh:</td>
            </tr>
            <tr>
                <td>&nbsp;</td><td>tanggal: <?=$comfunc->dateIndoLengkap($intros['pka_intro_approved_date'])?></td><td>tanggal: <?=$comfunc->dateIndoLengkap($intros['pka_intro_reviewed_date'])?></td><td>KETUA TIM,</td>
            </tr>
            <tr>
                <td></td><td>PENGENDALI MUTU,</td><td>PENGENDALI TEKNIS,</td><td></td>
            </tr>
            <?foreach (range(1,5) as $item): ?>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            <?endforeach;?>
            <tr>
                <td></td><td><u><?=$dalmut['nama']?></u><br><?=$dalmut['jabatan']?></td><td><u><?=$dalnis['nama']?></u><br><?=$dalnis['jabatan']?></td><td><u><?=$katim['nama']?></u><br><?=$katim['jabatan']?></td>
            </tr>
        </table>
        <br>
        <fieldset>
            <center>
                <input type="button" class="blue_btn" value="Kembali" onclick="location='<?= $def_page_request ?>'">
                <input type="button" class="blue_btn" value="ms-word" onclick="window.open('AuditManagement/program_audit_pendahuluan_word.php?id_assign=<?= $ses_assign_id ?>&id_auditee=<?= $auditee_id ?>','toolbar=no, location=no, status=no, menubar=yes, scrollbars=yes, resizable=yes');">
            </center>
        </fieldset>
    </article>
</section>