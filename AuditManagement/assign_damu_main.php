<?
include_once "_includes/classes/assignment_nha_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/param_class.php";

$assigns = new assignment_nha ( $ses_userId );
$assigns = new assign($ses_userId);
$params  = new param($ses_userId);

$ses_pagu_id = $_SESSION ['ses_pagu_id'];
$ses_assign_id = $_SESSION ['ses_assign_id'];
$ses_lha_id = $_SESSION ['ses_lha_id'];
$ses_auditee_id = $_SESSION ['ses_auditee_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=assign_lha_damu";
$acc_page_request = "assign_damu_acc.php";
$list_page_request = "audit_view.php";
// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
    if (is_numeric ( $str_page ) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "lha_view_parrent.php";
$grid = "grid.php";
$gridHeader   = array("Pekerjaan", "No SPK", "Nilai SPK", "Nama Penyedia", "Jangan Waktu", "Realisasi Fisik", "Realisasi Keuangan");
$gridDetail   = array("lha_damu_pekerjaan", "lha_damu_no_kontrak", "lha_damu_nilai", "lha_damu_nama", "lha_damu_jangka", "lha_damu_fisik", "lha_damu_keuangan");
$gridWidth    = array("20", "10", "12", "13", "15", "1", "13");

$key_by = array ("Pekerjaan");
$key_field = array ("lha_damu_pekerjaan");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

$rs_assign = $assigns->assign_lha_pagu_damu_viewlist ( $ses_lha_id, $ses_auditee_id );
$arr_assign = $rs_assign->FetchRow ();

switch ($_action) {
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah Jenis Kegiatan";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assigns->assign_damu_viewlist($fdata_id);
        $page_title = "Ubah Jenis Kegiatan";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assigns->assign_damu_viewlist($fdata_id);
        $page_title = "Rincian Jenis Kegiatan";
        break;
    case "postadd" :
        $flha_damu_lha_id     = "";
        $flha_damu_assign_id  = $arr_assign['lha_id_assign'];
        $flha_damu_tahun          = $comfunc->replacetext($_POST["lha_damu_tahun"]);
        $flha_damu_pekerjaan        = $comfunc->replacetext($_POST["lha_damu_pekerjaan"]);
        $flha_damu_no_kontrak      = $comfunc->replacetext($_POST["lha_damu_no_kontrak"]);
        $flha_damu_nilai    = $comfunc->replacetext($_POST["lha_damu_nilai"]);
        $flha_damu_nama = $comfunc->replacetext($_POST["lha_damu_nama"]);
        $flha_damu_jangka     = $comfunc->replacetext($_POST["lha_damu_jangka"]);
        $flha_damu_start_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_damu_start_date"]));
        $flha_damu_end_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_damu_end_date"]));
        $flha_damu_fisik    = $comfunc->replacetext($_POST["lha_damu_fisik"]);
        $flha_damu_keuangan       = $comfunc->replacetext($_POST["lha_damu_keuangan"]);
        $flha_damu_no_kontrak2      = $comfunc->replacetext($_POST["lha_damu_no_kontrak2"]);
        $flha_damu_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_damu_date"]));
        $flha_damu_persentase      = $comfunc->replacetext($_POST["lha_damu_persentase"]);
        if ($flha_damu_pekerjaan != "") {
            $assigns->assign_damu_add($flha_damu_lha_id, $flha_damu_assign_id, $flha_damu_tahun, $flha_damu_pekerjaan, $flha_damu_no_kontrak, $flha_damu_nilai, $flha_damu_nama, $flha_damu_jangka, $flha_damu_start_date, $flha_damu_end_date, $flha_damu_fisik, $flha_damu_keuangan, $flha_damu_no_kontrak2, $flha_damu_date, $ses_pagu_id, $flha_damu_persentase);
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fdata_id             = $comfunc->replacetext($_POST["data_id"]);
        $flha_damu_tahun          = $comfunc->replacetext($_POST["lha_damu_tahun"]);
        $flha_damu_pekerjaan        = $comfunc->replacetext($_POST["lha_damu_pekerjaan"]);
        $flha_damu_no_kontrak      = $comfunc->replacetext($_POST["lha_damu_no_kontrak"]);
        $flha_damu_nilai    = $comfunc->replacetext($_POST["lha_damu_nilai"]);
        $flha_damu_nama = $comfunc->replacetext($_POST["lha_damu_nama"]);
        $flha_damu_jangka     = $comfunc->replacetext($_POST["lha_damu_jangka"]);
        $flha_damu_start_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_damu_start_date"]));
        $flha_damu_end_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_damu_end_date"]));
        $flha_damu_fisik    = $comfunc->replacetext($_POST["lha_damu_fisik"]);
        $flha_damu_keuangan       = $comfunc->replacetext($_POST["lha_damu_keuangan"]);
        $flha_damu_no_kontrak2      = $comfunc->replacetext($_POST["lha_damu_no_kontrak2"]);
        $flha_damu_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_damu_date"]));
        $flha_damu_persentase      = $comfunc->replacetext($_POST["lha_damu_persentase"]);
        if ($flha_damu_pekerjaan != "") {
            $assigns->assign_damu_edit($fdata_id, $flha_damu_tahun, $flha_damu_pekerjaan, $flha_damu_no_kontrak, $flha_damu_nilai, $flha_damu_nama, $flha_damu_jangka, $flha_damu_start_date, $flha_damu_end_date, $flha_damu_fisik, $flha_damu_keuangan, $flha_damu_no_kontrak2, $flha_damu_date, $flha_damu_persentase);
            $comfunc->js_alert_act(1);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $assigns->assign_damu_delete($fdata_id);
        $comfunc->js_alert_act ( 2 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $assigns->assign_damu_count ( $ses_pagu_id, $key_search, $val_search, $key_field );
        $rs = $assigns->assign_damu_view_grid ( $ses_pagu_id, $key_search, $val_search, $key_field, $offset, $num_row );
        $page_title = "Daftar Jenis Kegiatan";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>
