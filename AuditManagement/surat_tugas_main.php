<?
include_once "_includes/classes/auditor_class.php";
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/assignment_class.php";

$auditors = new auditor ( $ses_userId );
$auditees = new auditee ( $ses_userId );
$assigns = new assign ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=surattugas";
$acc_page_request = "surat_tugas_acc.php";
$surat_page_request = "surat_tugas.php";
$list_page_request = "audit_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;



@$ses_assign_id = $_SESSION ['ses_assign_id'];
@$data_id = $_REQUEST['data_id'];

if ($data_id != '') {
	$def_page_request = "main_page.php?method=auditassign";
} else {
	$def_page_request = $paging_request . "&page=$noPage";
}

$grid = "grid_surat.php";
$gridHeader = array ("Satuan Kerja", "Tanggal Audit", "No Surat Tugas", "Tanggal Surat Tugas", "Penandatangan", "Lihat ST");
$gridDetail = array ("assign_surat_id_assign", "0", "assign_surat_no", "assign_surat_tgl", "auditor_name", "status");
$gridWidth = array ("25", "14", "14", "9", "16", "10");

$key_by = array ("No Surat Tugas", "Penandatangan", "Status");
$key_field = array ("assign_surat_no", "auditor_name", "status");

$widthAksi = "10";
$iconDetail = "0";
// === end grid ===//

$rs_assign = $assigns->assign_viewlist( $ses_assign_id );
$arr_assign = $rs_assign->FetchRow();

switch ($_action) {
	case "viewsurattugas" :
		$_nextaction = "postkomentar";
		$page_request = $surat_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $assigns->surat_assign_viewlist ( $fdata_id );
		$page_title = "Surat Tugas";
		break;
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Surat Tugas";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $assigns->surat_tugas_viewlist ( $fdata_id );
		$page_title = "Ubah Surat Tugas";
		break;
	case "getdetail" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		echo "<script>window.open('" . $acc_page_request_detil . "&auditor=" . $fdata_id . "', '_self');</script>";
		break;
	case "postadd" :
		$fno_surat = $comfunc->replacetext ( $_POST ["no_surat"] );
		$ftanggal_surat = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_surat"] ) );
		$fttd_id = $comfunc->replacetext ( $_POST ["ttd_id"] );
		$fjabatan_surat = $comfunc->replacetext ( $_POST ["jabatan_surat"] );
		$ftembusan = $comfunc->replacetext ( $_POST ["tembusan"] );
        $finspektorat = $comfunc->replacetext ( $_POST ["inspektorat"] );
		if ($fno_surat != "" && $ftanggal_surat != "" && $fttd_id != "" && $fjabatan_surat != "" && $finspektorat != "") {
			$id_surat_tugas = $assigns->uniq_id();
			$assigns->surat_tugas_add ( $ses_assign_id, $id_surat_tugas, $fno_surat, $ftanggal_surat, $fttd_id, $fjabatan_surat, $ftembusan, $finspektorat );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fno_surat = $comfunc->replacetext ( $_POST ["no_surat"] );
		$ftanggal_surat = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_surat"] ) );
		$fttd_id = $comfunc->replacetext ( $_POST ["ttd_id"] );
		$fjabatan_surat = $comfunc->replacetext ( $_POST ["jabatan_surat"] );
		$ftembusan = $comfunc->replacetext ( $_POST ["tembusan"] );
        $finspektorat = $comfunc->replacetext ( $_POST ["inspektorat"] );
		if ($fno_surat != "" && $ftanggal_surat != "" && $fttd_id != "" && $fjabatan_surat != "" && $finspektorat != "") {
			$assigns->surat_tugas_edit ( $fdata_id, $fno_surat, $ftanggal_surat, $fttd_id, $fjabatan_surat, $ftembusan, $finspektorat );
			
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postkomentar" :
		$id_surat = $comfunc->replacetext ( $_POST ["data_id"] );
		$status = $comfunc->replacetext ( $_POST ["status"] );
		$fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
		$ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
		
		$notif_date = $comfunc->dateSql(date("d-m-Y"));
		$comfunc->hapus_notif($id_surat);
		
		if ($status != "") {
			$group_menu_id = "";
			if ($status != "") {
				if($status=='1') { //ajukan
					$group_menu_id = '121';
				}elseif ($status=='3'){//tolak
					$group_menu_id = '120';
				}
				if($group_menu_id!=""){
					$rs_user_accept = $comfunc->notif_user_all_bygroup($group_menu_id);
					while($arr_user_accept = $rs_user_accept->FetchRow()){
						$comfunc->insert_notif($id_surat, $ses_userId, $arr_user_accept['user_id'], 'surattugas', "(Surat Tugas) ".$fkomentar, $notif_date);
					}
				}
				$assigns->surat_tugas_update_status ( $id_surat, $status, $ftanggal );
			}			
		}
		
		if ($fkomentar != "") {
			$assigns->surat_tugas_add_komentar ( $id_surat, $fkomentar, $ftanggal );
			$comfunc->js_alert_act ( 3 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$comfunc->hapus_notif($fdata_id);
		$assigns->surat_tugas_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $assigns->surat_tugas_count ( $ses_assign_id, $key_search, $val_search, $key_field);
		if($recordcount<>0) $iconAdd = "0";
		$rs = $assigns->surat_tugas_view_grid ( $ses_assign_id, $key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Surat Tugas";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
