<?
include_once "_includes/classes/assignment_nha_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/finding_class.php";
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/param_class.php";

$assignment_nhas = new assignment_nha ( $ses_userId );
$assigns = new assign ( $ses_userId );
$auditees = new auditee ( $ses_userId );
$findings = new finding ( $ses_userId );
$params = new param ();

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=assign_lha_arsip";
$acc_page_request = "assign_arsip_acc.php";
$list_page_request = "audit_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
    if (is_numeric ( $str_page ) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Obyek Pemeriksaan", "Unit Esselon I", "Tahun", "File");
$gridDetail = array ("auditee_name", "esselon_name", "lha_arsip_tahun", "file");
$gridWidth = array ("20", "20", "10", "20");

$key_by = array ("Obyek Pemeriksaan", "Unit Esselon I", "Tahun");
$key_field = array ("auditee_name", "esselon_name", "lha_arsip_tahun");

$widthAksi = "10";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah Arsip LHA";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assignment_nhas->assignment_lha_arsip_viewlist ( $fdata_id );
        $page_title = "Ubah Arsip LHA";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $auditeksternals->assignment_lha_arsip_viewlist ( $fdata_id );
        $page_title = "Rincian Arsip LHA";
        break;
    case "postadd" :
        $fauditee   = $comfunc->replacetext ( $_POST ["auditee_id"] );
        $fesselon   = $comfunc->replacetext ( $_POST ["esselon_id"] );
        $ftahun     = $comfunc->replacetext ( $_POST ["tahun"] );
		$ffile      = $comfunc->replacetext ( $_FILES ["file"] ["name"] );
        if ($fauditee != "" && $fesselon != "" && $ftahun != "" ) {
            $assignment_nhas->assignment_lha_arsip_add ($fauditee, $fesselon, $ftahun, $ffile);
            $comfunc->UploadFile ( "Upload_LHA", "file", "" );
            $comfunc->js_alert_act ( 3 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fid        = $comfunc->replacetext ( $_POST ["data_id"] );
        $fauditee   = $comfunc->replacetext ( $_POST ["auditee_id"] );
        $fesselon   = $comfunc->replacetext ( $_POST ["esselon_id"] );
        $ftahun     = $comfunc->replacetext ( $_POST ["tahun"] );
		$ffile      = $comfunc->replacetext ( $_FILES ["file"] ["name"] );
        if ($fauditee != "" && $fesselon != "" && $ftahun != ""  && $ffile == "" ) {
            $assignment_nhas->assignment_lha_arsip_edit ($fid, $fauditee, $fesselon, $ftahun);
            $comfunc->js_alert_act ( 1 );
        } elseif ( $fauditee != "" && $fesselon != "" && $ftahun != ""  && $ffile != "" ) {
            $assignment_nhas->assignment_lha_arsip_edit_upload ($fid, $fauditee, $fesselon, $ftahun, $ffile);
            $comfunc->UploadFile ( "Upload_LHA", "file", "" );
            $comfunc->js_alert_act ( 1 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $assignment_nhas->assignment_lha_arsip_delete ( $fdata_id );
        $comfunc->js_alert_act ( 2 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $assignment_nhas->assignment_lha_arsip_count ($key_search, $val_search, $key_field);
        $rs = $assignment_nhas->assignment_lha_arsip_view_grid ($key_search, $val_search, $key_field, $offset, $num_row );
        $page_title = "Daftar Arsip LHA";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>