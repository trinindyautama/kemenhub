<?php
include_once "../_includes/login_history.php";
include_once "../_includes/classes/auditee_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/classes/finding_class.php";
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/common.php";

$assigns = new assign($ses_userId);
$findings = new finding($ses_userId);
$comfunc = new comfunction($ses_userId);
$programaudits = new programaudit($ses_userId);
$rs_auditee = $assigns->auditee_detil ( $_REQUEST['id_auditee'] );
$arr_auditee = $rs_auditee->FetchRow();
$assign_id_auditee =$arr_auditee['auditee_id'];
$assign_nama_auditee = $arr_auditee['auditee_name'];

header("Content-Type: application/msword");
header("Content-Type: image/jpg");
header('Content-Disposition: attachment; filename=CETAK SPL '.str_replace(',', '', $assign_nama_auditee).' .doc');
header("Pragma: no-cache");
header("Expires: 0");
define ("INBOARD",false);

$rs = $assigns->assign_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr = $rs->FetchRow();
// lha
$rs_lha = $assigns->assign_lha_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_lha = $rs_lha->FetchRow ();
// spl
$rs_spl = $assigns->assign_spl_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_spl = $rs_spl->FetchRow ();
// get value finding
$rs_finding = $findings->finding_tl_nha_count ( $_REQUEST['id_assign'], "", "", "");
// temuan dan rekomendasi
$rs_spl_temuan_rekomendasi = $assigns->assign_spl_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_spl_temuan_rekomendasi = $rs_spl_temuan_rekomendasi->FetchRow ();
// temuan dan rekomendasi
$cek_posisi = $findings->cek_posisi($_REQUEST['id_assign']);

$rs_auditee_viewlist = $assigns->assign_auditee_get_inspektorat_viewlist($_REQUEST['id_assign'], $assign_id_auditee);
$arr_auditee_viewlist = $rs_auditee_viewlist->FetchRow();
?>
<table align="center" width="100%">
                        <tr>
                            <td align="center">
                                <img src="<?= $comfunc->urlImage('images/logo-menhub.png') ?>" width="76" height="59" />
                                <br><br><br><strong><font size="5">KEMENTERIAN PERHUBUNGAN <br>INSPEKTORAT JENDERAL</font></strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>
</table>
                    <table width="100%" border="0">
                        <tr>
                            <td width="5%"></td>
                            <td width="68%">
                                <p>Nomor : <?=$arr_spl['spl_no']?></p>
                                <p>Klasifikasi : <?=$arr_spl['spl_klasifikasi']?></p>
                                <p>Lampiran : <?=$arr_spl['spl_lampiran']?></p>
                                <p>Perihal : <?=$arr_spl['spl_perihal']?></p>
                            </td>
                            <td width="45%">
                                <p>Jakarta, <?=$comfunc->dateIndo($arr_spl['spl_date'])?></p>
                                <p>Yth. Kepada <?=$arr_spl['spl_yth']?></p>
                                <p>di</p>
                                <p><u>Jakarta</u></p>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <fieldset>
                        <table align="center" width="100%">
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top" width="1000px">
                                    1. Berdasarkan Surat Tugas Inpektur Jenderal Kementerian Perhubungan Nomor <?=$arr_spl['assign_surat_no']?> tanggal <?=$comfunc->dateIndoLengkap($arr_spl['assign_surat_tgl'])?>, Tim Inspetorat Jenderal telah melaksanakan
                                    pada <?=$assign_nama_auditee?> Provinsi <?=$arr_spl['propinsi_name']?>. Sehubungan dengan hal tersebut, bersama ini disampaikan disampaikan Laporan Hasil Audit (LHA) Nomor <?=$arr_spl['lha_no']?> tanggal
                                </td>
                                <td align="center" valign="top"></td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <br>
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top">
                                    <p>2. Dari Hasil <?=$arr_spl['audit_type_name']?> pada <?=$assign_nama_auditee?>, ditemukan beberapa kelemahan/temuan sebagai berikut:</p>
                                    <table width="700px" align="center">
                                        <?
                                        $rs_temuan_proses = $findings->get_temuan_proses ( $arr_spl['assign_id']);
                                        $count_tem = $rs_temuan_proses->RecordCount ();
                                        $no_tem = 0;
                                        while($arr_temuan_proses = $rs_temuan_proses->FetchRow()){
                                            $no_tem++;
                                            $rs_rekomendasi_proses = $findings->get_rekomendasi_proses ( $arr_temuan_proses['finding_id']);
                                            $count_rek = $rs_rekomendasi_proses->RecordCount ();
                                            $no_rek = 0;
                                            while($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()){
                                                $no_rek++;
                                                ?>
                                                <tr>
                                                    <?
                                                    if($no_rek==1){
                                                        ?>
                                                        <td><b><?=$no_tem?>.&nbsp;<?=$arr_temuan_proses['finding_judul']?></b></td>
                                                        <?
                                                    }
                                                    ?>
                                                <tr>
                                                    <td align="justify"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc'])?></td>
                                                </tr>
                                                </tr>
                                                <?
                                            }
                                        }
                                        ?>
                                    </table>
                                </td>
                                <td align="center" valign="top"></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top">
                                    <p style="margin-top: 0px;">3. Mohon kirannya tindak lanjut atas rekomendasi tersebut kolom 4 (empat) matriks terlampir diisiikan pada kolom 6 (enam), dan setelah ditandatangani dikirim
                                        kepada Inspektorat Jenderal Kementerian Perhubungan selambat-lambatnya 60 (enam puluh) hari setelah diterimanya surat ini, sesuai Peraturan Menteri Perhubungan
                                        Nomor PM.65 Tahun 2011 tanggal 24 Juni 2011 tentang Tata Cara Tetap Pelaksanaan Pengawasan di Lingkungan Kementerian Perhubungan.</p>
                                    4. Demikian disampaikan, atas perhatiannya diucapkan terima kasih.
                                </td>
                                <td align="center" valign="top"></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="10" align="justify"></td>
                            </tr>
						</table>	
                            <table align="center" width="100%">
                                <tr>
                                    <td width="60%">&nbsp;</td>
                                    <td>
                                        <center>
                                            <?=$comfunc->text_show($arr_spl['spl_pj'])?>
                                        </center>
                                    </td>
                                </tr>
                                <?
                                if($arr_spl ['spl_tembusan']!=""){
                                    ?>
                                    <tr>
                                        <td colspan="2">
                                            Tembusan :<br>
                                            <?php
											$cek_tem = explode(",", $arr_spl ['spl_tembusan'] );
                                            $cek_count = count($cek_tem);
                                            $notem = 0;
                                            for($x=0;$x<$cek_count;$x++){
                                                $notem++;
                                                echo $notem." .".trim($cek_tem[$x])."<br>";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?
                                }
                                ?>
                            </table>
					</fieldset>		
				