<?
include_once "_includes/classes/auditor_class.php";
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/param_class.php";
include_once "_includes/classes/program_audit_class.php";
include_once "_includes/classes/kertas_kerja_class.php";
include_once "_includes/classes/finding_class.php";

$auditors = new auditor ($ses_userId);
$auditees = new auditee ($ses_userId);
$assigns = new assign ($ses_userId);
$params = new param ($ses_userId);
$programaudits = new programaudit ($ses_userId);
$kertas_kerjas = new kertas_kerja ($ses_userId);
$findings = new finding ($ses_userId);


@$_action = $comfunc->replacetext($_REQUEST ["data_action"]);

if (isset($_POST["val_search"])) {
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if (@$method != @$val_method) {
    $key_search = "";
    $val_search = "";
    $val_method = "";
}


// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext($_GET ['page']);
if (isset ($str_page)) {
    if (is_numeric($str_page) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;


@$id_program_dash = $_REQUEST['id_program_dash'];

$paging_request = "main_page.php?method=programaudit";
$acc_page_request = "program_audit_acc.php";
$intro_page_request = "program_audit_intro.php";
$pendahuluan_page_request = "program_audit_pendahuluan.php";
$pelaksanaan_page_request = "program_audit_pelaksanaan.php";
$rinci_page_request = "program_audit_rinci.php";
$ikhtisar_page_request = "ikhtisar_temuan.php";
$list_page_request = "audit_view.php";

if ($id_program_dash != "") {
    $rs_program = $programaudits->program_audit_viewlist($id_program_dash);
    $arr_program = $rs_program->FetchRow();
    $ses_assign_id = $arr_program['program_id_assign'];
    $def_page_request = "main_page.php?method=programaudit&id_program_dash=$id_program_dash";
} else {
    $ses_assign_id = $_SESSION ['ses_assign_id'];
    $def_page_request = $paging_request . "&page=$noPage";
}


$view_parrent = "assign_view_parrent.php";
$grid = "grid_program_audit.php";
$gridHeader = array("Satuan Kerja", "Kode", "Prosedur", "Tahapan", "Auditor", "Status", "KKA");
$gridDetail = array("auditee_name", "ref_program_code", "ref_program_procedure", "program_tahapan", "auditor_name", "program_status", "program_id");
$gridWidth = array("15", "10", "25", "10", "15", "10", "5");

$key_by = array("Satuan Kerja", "Kode", "Sub Bidang Subtansi", "Auditor");
$key_field = array("auditee_name", "ref_program_code", "ref_program_title", "auditor_name");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

$rs_assign = $assigns->assign_viewlist($ses_assign_id);
$arr_assign = $rs_assign->FetchRow();

$status_katim = "";

$status_katim = $assigns->assign_cek_katim($ses_assign_id, $ses_userId);

switch ($_action) {
    case "getpendahuluan" :
        $page_request = $pendahuluan_page_request;

        $auditee_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs_assign = $assigns->assign_viewlist($ses_assign_id);
        $arr = $rs_assign->FetchRow();

        $page_title = "Cetak PKA";
        break;
    case "getpelaksanaan" :
        $page_request = $pelaksanaan_page_request;

        $auditee_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs_assign = $assigns->assign_viewlist($ses_assign_id);
        $arr = $rs_assign->FetchRow();

        $page_title = "Cetak PKA";
        break;
    case "getrinci" :
        $page_request = $rinci_page_request;

        $auditee_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs_assign = $assigns->assign_viewlist($ses_assign_id);
        $arr = $rs_assign->FetchRow();

        $page_title = "Cetak PKA";
        break;
    case "getikhtisar" :
        $page_request = $ikhtisar_page_request;

        $auditee_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs_assign = $assigns->assign_viewlist($ses_assign_id);
        $arr = $rs_assign->FetchRow();

        $page_title = "Cetak Ikhtisar Temuan";
        break;
    case "getintro" :
        $_nextaction = "postintro";
        $page_request = $intro_page_request;

        $auditee_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs_assign = $assigns->assign_viewlist($ses_assign_id);
        $arr = $rs_assign->FetchRow();

//        $page_title = "Isi Pendahuluan PKA";
        break;
    case "getajukan_pka" :
        $_nextaction = "postkomentar";
        $page_request = $acc_page_request;
        $id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $status = $comfunc->replacetext($_REQUEST ["status_pka"]);

        $rs = $programaudits->program_audit_viewlist($id);
        $page_title = "Pengajuan Program Kerja Audit";
        break;
    case "getapprove_pka" :
        $_nextaction = "postkomentar";
        $page_request = $acc_page_request;
        $id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $status = $comfunc->replacetext($_REQUEST ["status_pka"]);

        $rs = $programaudits->program_audit_viewlist($id);
        $page_title = "Reviu Program Kerja Audit";
        break;
    case "postkomentar" :
        $id_pka = $comfunc->replacetext($_POST ["data_id"]);
        $status = $comfunc->replacetext($_POST ["status_pka"]);
        $fkomentar = $comfunc->replacetext($_POST ["komentar"]);
        $ftanggal = $comfunc->date_db(date("d-m-Y H:i:s"));

        $comfunc->hapus_notif($id_pka);
        if ($status != "") {

            if($status==1){ 
                $get_user_id = $programaudits->get_user_by_posisi($ses_assign_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }elseif($status==3){ 
                $get_user_id = $programaudits->get_user_by_posisi($ses_assign_id, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            }

            if($notif_to_user_id!=""){
                $comfunc->insert_notif($id_pka, $ses_userId, $notif_to_user_id, 'programaudit', '(Program Audit) '.$fkomentar, $ftanggal);
            }
    
            $programaudits->program_audit_update_status($id_pka, $status, $ftanggal);
            
            if ($fkomentar != "") {
                $programaudits->program_audit_add_komentar($id_pka, $fkomentar, $ftanggal);
                $comfunc->js_alert_act(3);
            }
        
        } else {
            $comfunc->js_alert_act(10);
        }
            
        
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "kertas_kerja" :
        if (isset($_REQUEST['data_id'])) {
            $_SESSION ['ses_program_id'] = $_REQUEST['data_id'];
            $_SESSION['ses_assign_id'] = $ses_assign_id;
        } elseif (isset($_REQUEST['id_program_dash'])) {
            $_SESSION ['ses_program_id'] = $_REQUEST['id_program_dash'];
            $_SESSION['ses_assign_id'] = $ses_assign_id;
        }
        ?>
        <script>window.open('main_page.php?method=kertas_kerja', '_self');</script>
        <?
        break;
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah Program Audit";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs = $programaudits->program_audit_viewlist($fdata_id);
        $page_title = "Ubah Program Audit";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $rs = $programaudits->program_audit_viewlist($fdata_id);
        $page_title = "Detail Program Audit";
        break;
    case "postadd" :
        $fauditee = $comfunc->replacetext($_POST ["auditee"]);
        $fauditor = $comfunc->replacetext($_POST ["auditor"]);
        $program_jam = $_POST ["waktu"];
        $ftanggal = $comfunc->date_db(date("d-m-Y H:i:s"));
        $fref_program = $comfunc->replacetext($_POST ["ref_program"]);
        $flampiran = $comfunc->replacetext($_FILES ["attach"] ["name"]);
        $ftahapan = $comfunc->replacetext($_POST['tahapan']);
        $ex_ref_program = explode(",", $fref_program);
        $cek_ref_program = count($ex_ref_program);
        if ($fauditee != "" && $fauditor != "" && $cek_ref_program != "0" && $ftahapan != "") {
            $comfunc->UploadFile("Upload_ProgramAudit", "attach", "");
            for ($i = 0; $i < $cek_ref_program; $i++) {
                $programaudits->program_audit_add($ses_assign_id, $fauditee, $ex_ref_program [$i], $fauditor, $program_jam[$i], $flampiran, $ftanggal, $ftahapan, $comfunc->date_db($_POST["program_start"][$i]), '');
            }
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fdata_id = $comfunc->replacetext($_POST ["data_id"]);
        $fauditee = $comfunc->replacetext($_POST ["auditee_id"]);
        $fauditor = $comfunc->replacetext($_POST ["auditor"]);
        $program_jam = $_POST ["waktu"];
        $fref_program = $comfunc->replacetext($_POST ["ref_program"]);
        $flampiran = $comfunc->replacetext($_FILES ["attach"] ["name"]);
        $flampiran_old = $comfunc->replacetext($_POST ["attach_old"]);
        $ftahapan = $comfunc->replacetext($_POST['tahapan']);
        if ($fauditee != "" && $fauditor != "" && $fref_program != "" && $ftahapan != "") {
            if (!empty ($flampiran)) {
                $comfunc->UploadFile("Upload_ProgramAudit", "attach", $flampiran_old);
            } else {
                $flampiran = $flampiran_old;
            }
            $programaudits->program_audit_edit($fdata_id, $fauditee, $fref_program, $fauditor, $program_jam[0], $flampiran, $ftahapan, $comfunc->date_db($_POST['program_start'][0]));
            $comfunc->js_alert_act(1);
        } else {
            if (!empty ($flampiran)) {
                $comfunc->UploadFile("Upload_ProgramAudit", "attach", $flampiran_old);
            } else {
                $flampiran = $flampiran_old;
            }
            $programaudits->program_audit_edit1($fdata_id, $fauditee, $fauditor, $program_jam[0], $flampiran, $ftahapan, $comfunc->date_db($_POST['program_start'][0]));
            $comfunc->js_alert_act(1);

        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext($_REQUEST ["data_id"]);
        $comfunc->hapus_notif($fdata_id);
        $programaudits->program_audit_delete($fdata_id);

        $comfunc->js_alert_act(2);
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $programaudits->program_audit_count($ses_assign_id, $key_search, $val_search, $key_field, $id_program_dash);
        $rs = $programaudits->program_audit_view_grid($ses_assign_id, $key_search, $val_search, $key_field, $offset, $num_row, $id_program_dash);
        $page_title = "Daftar Program Audit";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>
