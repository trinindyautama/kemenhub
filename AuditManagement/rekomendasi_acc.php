<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<script type="text/javascript" src="css/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.maskMoney.js"></script>
<section id="main" class="column">
	<?
	if (! empty ( $view_parrent ))
	include_once $view_parrent;
	?>
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
			<?
			switch ($_action) {
			case "getadd" :
			?>
            <fieldset class="hr">
				<label class="span2">Kode Rekomendasi</label>
				<?=$comfunc->dbCombo("kode_rekomendasi", "par_kode_rekomendasi", "kode_rek_id", "concat(kode_rek_code,' - ',kode_rek_desc) as lengkap", "and kode_rek_del_st = '1' ", "", "", 1)?>
			</fieldset>
			<fieldset>
				<label class="span2">Rekomendasi</label>
			</fieldset>
			<fieldset class="hr">
				<textarea class="ckeditor" cols="10" rows="40" name="rekomendasi_desc" id="rekomendasi_desc"></textarea>
            </fieldset>
			<fieldset class="hr">
                <label class="span2">Nilai Rekomendasi</label>
                <input type="text" class="span2" name="nilai" id="nilai">
            </fieldset>
            <?
			break;
			case "getedit" :
			$arr = $rs->FetchRow ();
			?>
			<fieldset class="hr">
				<label class="span2">Kode Rekomendasi</label>
				<?=$comfunc->dbCombo("kode_rekomendasi", "par_kode_rekomendasi", "kode_rek_id", "concat(kode_rek_code,' - ',kode_rek_desc) as lengkap", "and kode_rek_del_st = '1' ", $arr['rekomendasi_id_code'], "", 1)?>
			</fieldset>
			<fieldset>
				<label class="span2">Rekomendasi</label>
			</fieldset>
			<fieldset class="hr">
				<textarea class="ckeditor" cols="10" rows="40"
				name="rekomendasi_desc" id="rekomendasi_desc"><?php echo $arr['rekomendasi_desc']?></textarea>
            </fieldset>
			<fieldset class="hr">
                <label class="span2">Nilai Rekomendasi</label>
                <input type="text" class="span2" name="nilai" id="nilai" value="<?=$arr['rekomendasi_nilai']?>">
            </fieldset>
			<input type="hidden" name="rekomendasi_date" value="<?=$arr['rekomendasi_date']?>" placeholder="">
			<input type="hidden" name="data_id" value="<?=$arr['rekomendasi_id']?>">
			<input type="hidden" name="finding_id" value="<?=$arr['rekomendasi_finding_id'] ?>">
			<?
			break;
			case "getdetail" :
			$arr = $rs->FetchRow ();
			?>
			<fieldset class="hr">
				<ul class="rtabs">
					<li><a href="#view1">Rincian</a></li>
				</ul>
				<div id="view1">
					<br>
					<table class="view_parrent">
						<tr>
							<td><div style="width: 150px">Kode Rekomendasi</div></td>
							<td>:</td>
							<td><?=$arr['lengkap']?></td>
						</tr>
						<tr>
							<td>Rekomendasi</td>
							<td>:</td>
							<td><?=$comfunc->text_show($arr['rekomendasi_desc'])?></td>
						</tr>
						<tr>
                            <td>Nilai Rekomendasi</td>
                            <td>:</td>
                            <td>Rp. <?=$arr['rekomendasi_nilai']?></td>
                        </tr>
					</table>
				</div>
			</fieldset>
			<div align="center">
				<div class="modal" id="histori_rekomendasi">
					<article class="module" style="width: 60%">
						<header>
							<h3 align="left" class="tabs_involved">List Histori Rekomendasi</h3>
						</header>
						<div class="modal-content">
							
							<br><br>
							<div>
								<input type="button" class="blue_btn" data-dismiss="modal" value="Kembali">
							</div>
						</div>
					</article>
				</div>
			</div>
			<?
			break;
			}
			?>
			<fieldset>
				<center>
				<input type="button" class="blue_btn" value="Kembali"
				onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
                <input type="submit" class="blue_btn" value="Simpan">
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
	<script>
	$("#nilai").maskMoney({precision: 0});

	$('#simpan_histori').click(function(){
		var data = $("#validation-form").serialize();
		$.ajax({
		url: 'AuditManagement/histori_rekomendasi.php',
		type: 'POST',
		data: data,
			success: function(data) {
				location.reload();
				alert('Berhasil menyimpan Histori Rekomendasi!');
			}
		});
	});
	$(function() {
		$("#validation-form").validate({
			rules: {
				kode_rekomendasi: "required",
				rekomendasi_desc: "required"
			},
			messages: {
				kode_rekomendasi: "Silahkan pilih PIC",
				rekomendasi_desc: "Silahkan Isi Rekomendasi"
			},
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>