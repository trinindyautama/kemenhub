<?php
include_once "_includes/classes/report_class.php";
$reports = new report ($ses_userId);
$rs_auditee = $assigns->assign_auditee_viewlist($arr['assign_id'], $auditee_id);
$arr_auditee = $rs_auditee->FetchRow();
// echo $arr['assign_id'];
?>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
    <article class="module width_3_quarter">
        <form method="post" name="f" action="#" id="validation-form" class="form-horizontal">
            <fieldset class="hr">
                <label class="span2">Nama Auditan</label>
                &nbsp;&nbsp;<?= $arr_auditee['auditee_name']; ?>
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Sasaran Auditan</label>
                &nbsp;&nbsp;<?= $arr['audit_type_name']; ?>
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Periode Audit</label>
                &nbsp;&nbsp;TA. <?= $arr['assign_tahun']-1?>-<?= $arr['assign_tahun']; ?>
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Tahapan</label>
                <?= $comfunc->combo_tahapan('tahapan', '') ?>
            </fieldset>
            <fieldset class="hr" id="rinci">
                <label class="span2">Aspek PKA</label>
                <?
                $rs_aspek  = $programaudits->get_assign_aspek("", $ses_assign_id, $auditee_id, "PKA Rinci");
                $arr_aspek = $rs_aspek->GetArray();
                echo $comfunc->buildCombo("fil_aspek_rinci", $arr_aspek, 0, 1, "", "", "", false, true, false, "span2");
                ?>
            </fieldset>
            <fieldset class="hr" id="pelaksanaan">
                <label class="span2">Aspek PKA</label>
                <?
                $rs_aspek_pel = $programaudits->get_assign_aspek("", $ses_assign_id, $auditee_id, "PKA Pelaksanaan");
                $arr_aspek_pel = $rs_aspek_pel->GetArray();
                echo $comfunc->buildCombo("fil_aspek_pel", $arr_aspek_pel, 0, 1, "", "", "", false, true, false);
                ?>
            </fieldset>
            <fieldset class="hr">
                <center><strong>Tujuan PKA</strong></center>
            </fieldset>
            <fieldset class="hr">
                <label class="span2">No PKA</label>
                <input type="text" name="no_pka" id="no_pka" size="100" value="">
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Tujian Audit</label>
                <textarea name="tujuan" id="tujuan" class="span6" style="width: 622px; height: 103px;"></textarea>
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Keterangan</label>
                <input type="text" name="keterangan" id="keterangan" size="100" value="">
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Tanggal Disusun</label> <input type="text"
                class="span1" name="pka_intro_arranged_date" id="pka_intro_arranged_date">
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Tanggal Direviu</label> <input type="text"
                class="span1" name="pka_intro_reviewed_date" id="pka_intro_reviewed_date">
            </fieldset>
            <fieldset class="hr">
                <label class="span2">Tanggal Disetujui</label> <input type="text"
                class="span1" name="pka_intro_approved_date" id="pka_intro_approved_date">
            </fieldset>
            <fieldset>
                <center>
                <input type="button" class="blue_btn" value="Kembali" onclick="location='<?= $def_page_request ?>'">&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="id" id="id">
                <input type="hidden" id="id_assign" name="id_assign" value="<?= $ses_assign_id ?>">
                <input type="hidden" name="audit_type" id="audit_type" value="<?= $arr['audit_type_name'] ?>">
                <input type="hidden" name="auditee_kode" id="auditee_kode" value="<?= $arr_auditee['auditee_kode'] ?>">
                <input type="hidden" name="data_action" id="data_action" value="<?= $_nextaction ?>">
                <input type="button" id="simpan" class="blue_btn" value="Simpan">
                </center>
                </center>
            </fieldset>
        </form>
    </article>
</section>
<script src="js/intro.js" type="text/javascript"></script>
<script>
$("#pka_intro_arranged_date").datepicker({
    dateFormat: 'dd-mm-yy',
    nextText: "",
    prevText: "",
    changeYear: true,
    changeMonth: true
});
$("#pka_intro_reviewed_date").datepicker({
    dateFormat: 'dd-mm-yy',
    nextText: "",
    prevText: "",
    changeYear: true,
    changeMonth: true
});
$("#pka_intro_approved_date").datepicker({
    dateFormat: 'dd-mm-yy',
    nextText: "",
    prevText: "",
    changeYear: true,
    changeMonth: true
});
</script>