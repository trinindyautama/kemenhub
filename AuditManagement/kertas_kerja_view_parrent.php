<?
$status_owner="";

$auditee_kka = "";
if ($method == 'kertas_kerja') {	
	$status_owner =  $kertas_kerjas->cek_owner_program ( $ses_program_id, $ses_userId);
}

$rs_program = $programaudits->program_audit_viewlist ( $ses_program_id );
$arr_program = $rs_program->FetchRow ();
$aspek = ($arr_program['aspek_id'] != '') ? $arr_program['aspek_id'] : '';
// echo $aspek;
$rs_intro	= $programaudits->intro_pka_viewlist('', $ses_assign_id, $arr_program['program_tahapan']);
$arr_intro = $rs_intro->FetchRow();
?>
<article class="module width_3_quarter">
	<fieldset>
		<table class="view_parrent" width="100%">
			<tr>
				<td width="150">No PKA</td>
				<td>:</td>
				<td><?=$arr_intro['pka_intro_no_pka']?></td>
			</tr>
			<tr>
				<td width="150">Satuan Kerja</td>
				<td>:</td>
				<td><?=$arr_program['auditee_name']?></td>
			</tr>
			<tr>
				<td>Auditor</td>
				<td>:</td>
				<td><?=$arr_program['auditor_name']?></td>
			</tr>
			<tr>
				<td>Bidang Substansi</td>
				<td>:</td>
				<td><?=$arr_program['ref_program_title']?></td>
			</tr>
			<tr>
				<td>Prosedur Audit</td>
				<td>:</td>
				<td><?=$comfunc->text_show($arr_program['ref_program_procedure'])?></td>
			</tr>
			<tr>
				<td>Tanggal PKA</td>
				<td>:</td>
				<td><?=$comfunc->dateIndo($arr_program['program_start'])?></td>
			</tr>
			<tr>
				<td>Waktu</td>
				<td>:</td>
				<td><?=$arr_program['program_jam']?> Jam</td>
			</tr>
			<tr>
				<td>Tahapan Audit</td>
				<td>:</td>
				<td><?=$comfunc->text_show($arr_program['program_tahapan'])?></td>	
			</tr>
		</table>
	</fieldset>
</article>