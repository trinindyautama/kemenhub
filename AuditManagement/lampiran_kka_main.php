    <?
include_once "_includes/classes/assignment_class.php";
// include_once "_includes/classes/lampiran_class.php";
include_once "_includes/classes/kertas_kerja_class.php";
include_once "_includes/classes/rekomendasi_class.php";
include_once "_includes/classes/param_class.php";

$assigns = new assign ( $ses_userId );
$kertas_kerjas = new kertas_kerja ( $ses_userId );
// $kertas_kerjas = new lampiran ( $ses_userId );
$rekomendasis = new rekomendasi ( $ses_userId );
$params = new param ( $ses_userId );

$ses_assign_id = $_SESSION ['ses_assign_id'];
@$ses_kka_id = $_SESSION ['ses_kka_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}


$paging_request = "main_page.php?method=lampiran_kka";
$acc_page_request = "lampiran_acc.php";
$list_page_request = "audit_view.php";
// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "assign_view_parrent.php";
$grid = "grid_lampiran.php";
$gridHeader = array ("Lampiran");
$gridDetail = array ("kka_attach_filename");
$gridWidth = array ("60");

$key_by = array ();
$key_field = array ();

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

$status_katim = $assigns->assign_cek_katim($ses_assign_id, $ses_userId);

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Lampiran";
		break;
	case "postadd" :
		$fdata_id           = $comfunc->replacetext($_POST["data_id"]);
        $fkka_attach        = $_FILES["kka_attach"]["name"];
        $jml_attach         = count($fkka_attach);
        if ($jml_attach > 0) {
            for ($i = 0; $i < $jml_attach; $i++) {
                if ($fkka_attach[$i] != "") {
                    $comfunc->UploadFileMulti("Upload_KKA", "kka_attach");
                    $kertas_kerjas->insert_lampiran_kka($fdata_id, $fkka_attach[$i]);
                }
            }
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$nama_file = $kertas_kerjas->lampiran_viewlist($fdata_id);
		// echo $nama_file;
		$kertas_kerjas->destroy_lampiran_kka($fdata_id, $nama_file);
        $comfunc->HapusFile("Upload_KKA", $nama_file);
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $kertas_kerjas->lampiran_count ($ses_kka_id, $key_search, $val_search, $key_field );
		$rs = $kertas_kerjas->lampiran_view_grid ($ses_kka_id, $key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Lampiran KKA";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
