<?

$rs_lha = $assigns->assign_lha_pagu_damu_viewlist ( $ses_assign_id, $ses_auditee_id );
$arr_lha = $rs_lha->FetchRow ();

$rs_pagu = $assigns->assign_pagu_view_parrent ( $ses_assign_id, $ses_auditee_id );
$arr_pagu = $rs_pagu->FetchRow ();

?>
<article class="module width_3_quarter">
    <?
    if ($method == 'assign_lha_pagu') {
        ?>
        <fieldset>
            <table class="view_parrent">
                <tr>
                    <td width="150">Nomor LHA</td>
                    <td>:</td>
                    <td><?= $arr_lha['lha_no'] ?></td>
                </tr>
                <tr>
                    <td width="150">Tanggal LHA</td>
                    <td>:</td>
                    <td><?= $comfunc->dateIndo($arr_lha['lha_date']) ?></td>
                </tr>
                <tr>
                    <td width="150">Auditi</td>
                    <td>:</td>
                    <td><?= $arr_lha['auditee_name'] ?></td>
                </tr>
            </table>
        </fieldset>
        <?
    } else {
        ?>
        <fieldset>
            <table class="view_parrent">
                <tr>
                    <td width="150">DIPA No</td>
                    <td>:</td>
                    <td><?= $arr_pagu['lha_pagu_no_dipa'] ?></td>
                </tr>
                <tr>
                    <td width="150">DIPA Tanggal</td>
                    <td>:</td>
                    <td><?= $comfunc->dateIndo($arr_pagu['lha_pagu_date'])?></td>
                </tr>
                <tr>
                    <td width="150">Nilai</td>
                    <td>:</td>
                    <td>Rp.&nbsp;<?=$comfunc->format_uang($arr_pagu['lha_pagu_nilai'])?></td>
                </tr>
            </table>
        </fieldset>
        <?
    }
    ?>
</article>