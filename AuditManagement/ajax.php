<?
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/param_class.php";
include_once "../_includes/classes/auditor_class.php";
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/common.php";
include_once "../_includes/classes/finding_class.php";
include_once "../_includes/classes/dashboard_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/classes/rekomendasi_class.php";
include_once "../_includes/classes/assignment_nha_class.php";
include_once "../_includes/classes/auditee_class.php";

session_start();
session_regenerate_id();
$ses_UserId = $_SESSION['ses_userId'];
session_write_close();

$reports       = new report($ses_UserId);
$params        = new param($ses_UserId);
$auditors      = new auditor($ses_UserId);
$comfunc       = new comfunction($ses_UserId);
$programaudits = new programaudit($ses_UserId);
$findings      = new finding($ses_UserId);
$dashboards    = new dashboard($ses_UserId);
$assigns       = new assign($ses_UserId);
$rekomendasis  = new rekomendasi($ses_UserId);
$assignment_nhas  = new assignment_nha($ses_UserId);
$auditees      = new auditee($ses_UserId);

$_action = $comfunc->replacetext($_REQUEST["data_action"]);
switch ($_action) {
    case "getsbu_rinci":
        $idsatker = $comfunc->replacetext($_POST["idsatker"]);
        $golongan = $comfunc->replacetext($_POST["golongan"]);
        $rs       = $params->get_sbu_rinci($idsatker, $golongan);
        $data     = array();
        while ($row = $rs->FetchRow()) {
            $data["sbu_rinci"][] = array(
                'sbu'    => $row['sbu_name'],
                'amount' => $row['sbu_rinci_amount'],
                'status' => $row['sbu_status'],
            );
        }
        echo json_encode($data);
        exit();
        break;
    case "getgol_auditor":
        $id_auditor = $comfunc->replacetext($_POST["id_auditor"]);
        $rs         = $auditors->auditor_data_viewlist($id_auditor);
        $row        = $rs->FetchRow();
        echo $row['auditor_golongan'];
        exit();
        break;
    case "get_introProgPendahuluan":
//        $tahapan = $comfunc->replacetext($_POST ["tahapan"]);
        $id_assign    = $comfunc->replacetext($_POST["id_assign"]);
        $audit_type   = $comfunc->replacetext($_POST["audit_type"]);
        $auditee_kode = $comfunc->replacetext($_POST["auditee_kode"]);
        $rs           = $programaudits->intro_pka_viewlist("", $id_assign, "PKA Pendahuluan");
        $row          = $rs->FetchRow();
//        $rs_aspek = $params->bidang_sub_data_viewlist($id_aspek);
        //        $row_aspek = $rs_aspek->FetchRow();

        $data["id"] = $row['pka_intro_id'];
//        $data ["pendahuluan"] = $row ['pka_intro_pendahuluan'];
        // if ($audit_type == 'Audit Operasional') {
        //     $type = 'OPS';
        // }
        switch ($audit_type) {
            case "Audit Dengan Tujuan Tertentu":
                $type = 'KHS';
                break;
            case "Audit Operasional":
                $type = "OPS";
                break;
            case "Audit PBJ":
                $type = "PBJ";
                break;
            case "Audit Kinerja":
                $type = "KIN";
                break;
        }
        if ($row['pka_intro_no_pka'] == '') {
            $pka_max = $programaudits->get_pka_auto('PKA Pendahuluan');
            $arr     = $pka_max->FetchRow();
            $strlen  = strlen($auditee_kode) + 21;
            $no      = (int) substr($arr['maxPKA'], 4, 4);
            $no++;
            $year           = date('Y');
            $month          = date('m');
            $data["no_pka"] = 'PKA/0' . sprintf("%03s", $no) . '/' . $type . '/' . $auditee_kode . '/PKAPEND/' . $month . '/' . $year;
        } else {
            $data["no_pka"] = $row['pka_intro_no_pka'];
        }
        $data["tujuan"]     = $row['pka_intro_tujuan'];
        $data["keterangan"] = $row['pka_keterangan'];
//        $data ['nama_aspek'] = $row_aspek ['aspek_name'];
        $data['pka_intro_arranged_date'] = $comfunc->dateIndo($row['pka_intro_arranged_date']);
        $data['pka_intro_reviewed_date'] = $comfunc->dateIndo($row['pka_intro_reviewed_date']);
        $data['pka_intro_approved_date'] = $comfunc->dateIndo($row['pka_intro_approved_date']);
        echo json_encode($data);
        exit();
        break;
    case "get_introProgPelaksanaan":
        $id_aspek     = $comfunc->replacetext($_POST["id_aspek"]);
        $id_assign    = $comfunc->replacetext($_POST["id_assign"]);
        $audit_type   = $comfunc->replacetext($_POST["audit_type"]);
        $auditee_kode = $comfunc->replacetext($_POST["auditee_kode"]);
        $rs           = $programaudits->intro_pka_viewlist($id_aspek, $id_assign, "PKA Pelaksanaan");
        $row          = $rs->FetchRow();
        $rs_aspek     = $params->bidang_sub_data_viewlist($id_aspek);
        $row_aspek    = $rs_aspek->FetchRow();

        $data["id"] = $row['pka_intro_id'];
        switch ($audit_type) {
            case "Audit Dengan Tujuan Tertentu":
                $type = 'KHS';
                break;
            case "Audit Operasional":
                $type = "OPS";
                break;
            case "Audit PBJ":
                $type = "PBJ";
                break;
            case "Audit Kinerja":
                $type = "KIN";
                break;
        }
        if ($row['pka_intro_no_pka'] == '') {
            $pka_max = $programaudits->get_pka_auto('PKA Pelaksanaan');
            $arr     = $pka_max->FetchRow();
            $strlen  = strlen($auditee_kode) + 21;
            $no      = (int) substr($arr['maxPKA'], 4, 4);
            $no++;
            $year           = date('Y');
            $month          = date('m');
            $data["no_pka"] = 'PKA/0' . sprintf("%03s", $no) . '/' . $type . '/' . $auditee_kode . '/PKAPEL/' . $month . '/' . $year;
        } else {
            $data["no_pka"] = $row['pka_intro_no_pka'];
        }
        $data["tujuan"]                  = $row['pka_intro_tujuan'];
        $data['nama_aspek']              = $row_aspek['aspek_name'];
        $data["keterangan"]              = $row['pka_keterangan'];
        $data['pka_intro_arranged_date'] = $comfunc->dateIndo($row['pka_intro_arranged_date']);
        $data['pka_intro_reviewed_date'] = $comfunc->dateIndo($row['pka_intro_reviewed_date']);
        $data['pka_intro_approved_date'] = $comfunc->dateIndo($row['pka_intro_approved_date']);
        echo json_encode($data);
        exit();
        break;
    case "get_introProgRinci":
        $id_aspek     = $comfunc->replacetext($_POST["id_aspek"]);
        $id_assign    = $comfunc->replacetext($_POST["id_assign"]);
        $audit_type   = $comfunc->replacetext($_POST["audit_type"]);
        $auditee_kode = $comfunc->replacetext($_POST["auditee_kode"]);
        $rs           = $programaudits->intro_pka_viewlist($id_aspek, $id_assign, "PKA Rinci");
        $row          = $rs->FetchRow();
        $rs_aspek     = $params->bidang_sub_data_viewlist($id_aspek);
        $row_aspek    = $rs_aspek->FetchRow();

        $data["id"] = $row['pka_intro_id'];
        switch ($audit_type) {
            case "Audit Dengan Tujuan Tertentu":
                $type = 'KHS';
                break;
            case "Audit Operasional":
                $type = "OPS";
                break;
            case "Audit PBJ":
                $type = "PBJ";
                break;
            case "Audit Kinerja":
                $type = "KIN";
                break;
        }
        if ($row['pka_intro_no_pka'] == '') {
            $pka_max = $programaudits->get_pka_auto('PKA Rinci');
            $arr     = $pka_max->FetchRow();
            $strlen  = strlen($auditee_kode) + 21;
            $no      = (int) substr($arr['maxPKA'], 4, 4);
            $no++;
            $year           = date('Y');
            $month          = date('m');
            $data["no_pka"] = 'PKA/0' . sprintf("%03s", $no) . '/' . $type . '/' . $auditee_kode . '/PKARIN/' . $month . '/' . $year;
        } else {
            $data["no_pka"] = $row['pka_intro_no_pka'];
        }
        $data["tujuan"]                  = $row['pka_intro_tujuan'];
        $data['nama_aspek']              = $row_aspek['aspek_name'];
        $data["keterangan"]              = $row['pka_keterangan'];
        $data['pka_intro_arranged_date'] = $comfunc->dateIndo($row['pka_intro_arranged_date']);
        $data['pka_intro_reviewed_date'] = $comfunc->dateIndo($row['pka_intro_reviewed_date']);
        $data['pka_intro_approved_date'] = $comfunc->dateIndo($row['pka_intro_approved_date']);
        echo json_encode($data);
        exit();
        break;
    case "getDesc_refProg":
        $id_sub = $comfunc->replacetext($_POST["id_sub"]);
        $start = $comfunc->replacetext($_POST['start']);
        $end = $comfunc->replacetext($_POST['end']);
        $rs     = $params->get_ref_desc($id_sub);
        $data   = array();
        $i = 0;
        while ($row = $rs->FetchRow()) {
            $i++;
            $data["desc"][] = array(
                'kode'      => $row['ref_program_code'],
                'judul'     => $row['ref_program_title'],
                'procedure' => $row['ref_program_procedure'],
                'start'       => '<input type="text" class="span8" name="program_start[]" id="program_start_'.$i.'"><script>$("#program_start_'.$i.'").datepicker({
        dateFormat: "dd-mm-yy",
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true,
        minDate: "'.$comfunc->dateIndo($start).'",
        maxDate: "'.$comfunc->dateIndo($end).'"
    });</script>',
                'end'       => '<input type="text" class="span8" name="program_end[]" id="program_end_'.$i.'"><script>$("#program_end_'.$i.'").datepicker({
        dateFormat: "dd-mm-yy",
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true,
        minDate: "'.$comfunc->dateIndo($start).'",
        maxDate: "'.$comfunc->dateIndo($end).'"
    });</script>',
                'jam'       => '<input type="text" class="span4" name="waktu[]" id="waktu"><span class="mandatory">*</span>',
            );
        }
        echo json_encode($data);
        exit();
        break;
    case "getDesc_refProg1":
        $id_sub = $comfunc->replacetext($_POST["id_sub"]);
        $rs     = $params->get_ref_desc($id_sub);
        $data   = array();
        $i = 0;
        while ($row = $rs->FetchRow()) {
            $i++;
            $data["desc"][] = array(
                'kode'      => $row['ref_program_code'],
                'judul'     => $row['ref_program_title'],
                'procedure' => $row['ref_program_procedure']
            );
        }
        echo json_encode($data);
        exit();
        break;
    case "getAspek_pelaksanaan":
        $id_aspek   = $comfunc->replacetext($_POST["id_aspek"]);
        $id_assign  = $comfunc->replacetext($_POST["id_assign"]);
        $id_auditee = $comfunc->replacetext($_POST["id_auditee"]);
        $rs         = $reports->assign_program_audit_list($id_aspek, $id_assign, $id_auditee, "PKA Pelaksanaan");
        $data       = array();
        while ($row = $rs->FetchRow()) {
            $data["aspek_rinci"][] = array(
                'title'     => $row['ref_program_procedure'],
                'auditor'   => $row['auditor_name'],
                'jam'       => $row['program_jam'] . " Jam",
                'catatan'   => $row['kertas_kerja_desc'],
                'no_kka'    => $row['kertas_kerja_no'],
                'realisasi' => $row['kertas_kerja_jam'] . " Jam",
            );
        }
        echo json_encode($data);
        exit();
        break;
    case "getAspek_rinci":
        $id_aspek   = $comfunc->replacetext($_POST["id_aspek"]);
        $id_assign  = $comfunc->replacetext($_POST["id_assign"]);
        $id_auditee = $comfunc->replacetext($_POST["id_auditee"]);
        $rs         = $reports->assign_program_audit_list($id_aspek, $id_assign, $id_auditee, "PKA Rinci");
        $data       = array();
        while ($row = $rs->FetchRow()) {
            $data["aspek_rinci"][] = array(
                'title'     => $row['ref_program_procedure'],
                'auditor'   => $row['auditor_name'],
                'jam'       => $row['program_jam'] . " Jam",
                'catatan'   => $row['kertas_kerja_desc'],
                'no_kka'    => $row['kertas_kerja_no'],
                'realisasi' => $row['kertas_kerja_jam'] . " Jam",
            );
        }
        echo json_encode($data);
        exit();
        break;

    //input program audit intro
    case "postintro":
        $id_assign                = $comfunc->replacetext($_POST["id_assign"]);
        $fid                      = $comfunc->replacetext($_POST["id"]);
        $ffil_aspek_rinci         = $comfunc->replacetext($_POST["fil_aspek_rinci"]);
        $ffil_aspek_pel           = $comfunc->replacetext($_POST["fil_aspek_pel"]);
        $ftujuan                  = $comfunc->replacetext($_POST["tujuan"]);
        $ftahapan                 = $comfunc->replacetext($_POST["tahapan"]);
        $fno_pka                  = $comfunc->replacetext($_POST["no_pka"]);
        $fketerangan              = $comfunc->replacetext($_POST["keterangan"]);
        $fpka_intro_arranged_date = $comfunc->date_db($comfunc->replacetext($_POST["pka_intro_arranged_date"]));
        $fpka_intro_reviewed_date = $comfunc->date_db($comfunc->replacetext($_POST["pka_intro_reviewed_date"]));
        $fpka_intro_approved_date = $comfunc->date_db($comfunc->replacetext($_POST["pka_intro_approved_date"]));
        if ($ffil_aspek_rinci != "" && $ffil_aspek_pel == "") {
            if ($fid == "") {
                $programaudits->intro_pka_add($id_assign, $ffil_aspek_rinci, $ftujuan, $ftahapan, $fno_pka, $fketerangan, $fpka_intro_arranged_date, $fpka_intro_reviewed_date, $fpka_intro_approved_date);
            } else {
                $programaudits->intro_pka_edit($fid, $ffil_aspek_rinci, $ftujuan, $ftahapan, $fno_pka, $fketerangan, $fpka_intro_arranged_date, $fpka_intro_reviewed_date, $fpka_intro_approved_date);
            }
        } elseif ($ffil_aspek_pel != "" && $ffil_aspek_rinci == "") {
            if ($fid == "") {
                $programaudits->intro_pka_add($id_assign, $ffil_aspek_pel, $ftujuan, $ftahapan, $fno_pka, $fketerangan, $fpka_intro_arranged_date, $fpka_intro_reviewed_date, $fpka_intro_approved_date);
            } else {
                $programaudits->intro_pka_edit($fid, $ffil_aspek_pel, $ftujuan, $ftahapan, $fno_pka, $fketerangan, $fpka_intro_arranged_date, $fpka_intro_reviewed_date, $fpka_intro_approved_date);
            }
        } elseif ($ffil_aspek_pel == "" && $ffil_aspek_rinci == "") {
            if ($fid == "") {
                $programaudits->intro_pka_add($id_assign, "", $ftujuan, $ftahapan, $fno_pka, $fketerangan, $fpka_intro_arranged_date, $fpka_intro_reviewed_date, $fpka_intro_approved_date);
            } else {
                $programaudits->intro_pka_edit($fid, "", $ftujuan, $ftahapan, $fno_pka, $fketerangan, $fpka_intro_arranged_date, $fpka_intro_reviewed_date, $fpka_intro_approved_date);
            }
        }
        exit();
        break;

    //referensi program audit
    case "refprogram":
        $tahapan = $comfunc->replacetext($_POST['tahapan_selected']);
        $data    = array();

        $rsRef = $params->ref_program_data_viewlist("", $tahapan);
        $i     = 0;
        foreach ($rsRef->GetArray() as $row) {
            $data[$i] = array(
                "id"   => $row['ref_program_id'],
                "text" => $row['ref_program_code'] . " - " . $row['ref_program_title'],
            );
            $i++;
        }
        echo json_encode($data);

        exit();
        break;
    case "getTemuanCount":
        $finding_type_name     = $comfunc->replacetext($_POST['name']);
        $tahun                 = $comfunc->replacetext($_POST['tahun']);
        $tipe_audit            = $comfunc->replacetext($_POST['tipe_audit']);
        $finding_sub_type_name = $dashboards->get_finding_sub_type($finding_type_name);
        $findings              = array();
        $html = '<canvas id="myChart2"></canvas>';
        foreach ($finding_sub_type_name->GetArray() as $finding) {
            array_push($findings, array(
                "finding_sub_type_name" => $finding['sub_type_name'],
                "counts"                => $dashboards->finding_type_finding_sub_count($finding['sub_type_id'], $tahun, $tipe_audit),
                "canvas" => $html

            ));
        }
        $json = $findings;
        echo json_encode($json);
        break;
    case "postmatrix":
        $assign_id      = $comfunc->replacetext($_POST['assign_id']);
        $rs_list_temuan = $assigns->temuan_list_matrix($assign_id);
        $count          = array();
        foreach ($rs_list_temuan as $value) {
            $findings->finding_matrix_edit($_POST['finding_id_' . $value['finding_id']], $comfunc->replacetext($_POST['finding_kondisi_' . $value['finding_id']]), $comfunc->replacetext($_POST['finding_sebab_' . $value['finding_id']]), $comfunc->replacetext($_POST['finding_akibat_' . $value['finding_id']]));
            $rs_list_rek = $findings->get_rekomendasi_list_matrix($value['finding_id']);
            foreach ($rs_list_rek as $rek) {
                $rekomendasis->rekomendasi_matrix_edit($_POST['rekomendasi_id_' . $rek['rekomendasi_id']], $_POST['rekomendasi_desc_' . $rek['rekomendasi_id']]);
            }
        }
        break;
    case "getassigndata":
        $html       = '';
        $param      = explode('-', $_REQUEST['params']);
        $tahun      = ($_REQUEST['tahun']) ? $_REQUEST['tahun'] : "";
        $tipe_audit = ($_REQUEST['tipe_audit']) ? $_REQUEST['tipe_audit'] : "";

        switch ($_REQUEST['labels']) {
            case "Jumlah Rekomendasi":
                $html .= "<center><h1>Daftar Rekomendasi {$param[1]}</h1></center>";
                $html .= "<br>";
                $html .= "<table class='table_risk' border='1' cellspacing='0' cellpadding='0'";
                $html .= "<tr>";
                $html .= "<th align='center' width='5%'>No</th>";
                $html .= "<th>Kode</th>";
                $html .= "<th width='75%'>Rekomendasi</th>";
                $html .= "<th>Status</th>";
                $html .= "</tr>";
                $rs_list_rek = $dashboards->list_rekomendasi_per_assign($param[0], $tahun, $tipe_audit);
                foreach ($rs_list_rek as $key => $rek) {
                    $html .= "<tr>";
                    $html .= "<td align='center'>" . ++$key . "</td>";
                    $html .= "<td align='center'>" . $rek['kode_rek_code'] . "</td>";
                    $html .= "<td>" . $comfunc->text_show($rek['rekomendasi_desc']) . "</td>";
                    $html .= "<td>" . $rek['rekomendasi_status'] . "</td>";
                    $html .= "</tr>";
                }
                $html .= "</table>";
                break;
            case "Jumlah Temuan":
                $html .= "<center><h1>Daftar Temuan {$param[1]}</h1></center>";
                $html .= "<br>";
                $html .= "<table class='table_risk' border='1' cellspacing='0' cellpadding='0'";
                $html .= "<tr>";
                $html .= "<th align='center' width='5%'>No</th>";
                $html .= "<th width='10%'>Kode Temuan</th>";
                $html .= "<th width='10%'>No Temuan</th>";
                $html .= "<th width='75%'>Judul</th>";
                $html .= "</tr>";
                $rs_list_tem = $dashboards->list_temuan_per_assign($param[0], $tahun, $tipe_audit);
                foreach ($rs_list_tem as $key => $tem) {
                    $html .= "<tr>";
                    $html .= "<td align='center'>" . ++$key . "</td>";
                    $html .= "<td align='center'>" . $tem['kode_temuan'] . "</td>";
                    $html .= "<td align='center'>" . $tem['finding_no'] . "</td>";
                    $html .= "<td>" . $tem['finding_judul'] . "</td>";
                    $html .= "</tr>";
                }
                $html .= "</table>";
                break;
            case "Jumlah Tindak Lanjut":
                $html .= "<center><h1>Daftar Tindak Lanjut {$param[1]}</h1></center>";
                $html .= "<br>";
                $html .= "<table class='table_risk' border='1' cellspacing='0' cellpadding='0'";
                $html .= "<tr>";
                $html .= "<th align='center' width='5%'>No</th>";
                $html .= "<th width='80%'>Tindak Lanjut</th>";
                $html .= "<th width='15%'>Status</th>";
                $html .= "</tr>";
                $rs_list_tl = $dashboards->list_tindak_lanjut_per_assign($param[0], $tahun, $tipe_audit);
                foreach ($rs_list_tl as $key => $tl) {
                    $html .= "<tr>";
                    $html .= "<td align='center'>" . ++$key . "</td>";
                    $html .= "<td>" . $comfunc->text_show($tl['tl_desc']) . "</td>";
                    $html .= "<td align='center'>" . $tl['tl_status'] . "</td>";
                    $html .= "</tr>";
                }
                $html .= "</table>";
                break;
        }
        echo $html;
        break;
    case "getpropinsi":
        $id_auditee = $comfunc->replacetext($_POST["id_auditee"]);
        $rs         = $auditees->auditee_data_viewlist($id_auditee);
        $row        = $rs->FetchRow();
        echo $row['propinsi_name'];
        exit();
        break;
}
