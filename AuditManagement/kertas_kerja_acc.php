<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<script type="text/javascript" src="css/bootstrap.min.js"></script>
<section id="main" class="column">
	<?
	if (!empty($view_parrent)) {
	include_once $view_parrent;
	}
	?>
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
			<?
			switch ($_action) {
			case "getadd":
			?>
			<fieldset class="hr">
				<label class="span2">No Kertas Kerja Audit</label>
				<input type="text" class="span3" name="no_kka" value="<?= $kka ?>" id="no_kka">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Kertas Kerja Audit</label>
				<input type="text" class="span1" name="kertas_kerja_date" id="kertas_kerja_date">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Waktu ( Jam )</label>
				<input type="text" class="span0" name="kertas_kerja_jam" id="kertas_kerja_jam">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<ul class="rtabs">
					<li><a href="#view1">Uraian</a></li>
					<li><a href="#view2">Kesimpulan</a></li>
				</ul>
				<div id="view1">
					<textarea class="ckeditor" cols="10" rows="40" name="kertas_kerja" id="kertas_kerja"></textarea>
					<br>
					<input type="button" id="tambah_auditee" class="blue_btn" value="Tambah Baris">
				<br>
				<br>
				<table id="tabel_auditee" width="60%">
					<thead>
						<tr>
							<th align="center" width="5%">No.</th>
							<th align="left" width="30%">Lampiran</th>
							<th width="5%">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr id="1">
							<td align="center">1.</td>
							<td><input type="file" class="span4" name="kka_attach[]"></td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				</table>
				</div>
				<div id="view2">
					<textarea class="ckeditor" cols="10" rows="40" name="kesimpulan" id="kesimpulan"></textarea>
				</div>
			</fieldset>
			<?
			break;
			case "getedit":
			$arr = $rs->FetchRow();
			?>
			<fieldset class="hr">
				<label class="span2">No Kertas Kerja Audit</label> <input type="text"
				class="span2" name="no_kka" id="no_kka"
				value="<?=$arr['kertas_kerja_no']?>"><span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Kertas Kerja Audit</label> <input type="text"
				class="span1" name="kertas_kerja_date" id="kertas_kerja_date"
				value="<?=$comfunc->dateIndo($arr['kertas_kerja_date'])?>"><span
				class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Waktu ( Jam )</label>
				<input type="text" class="span0" name="kertas_kerja_jam" id="kertas_kerja_jam" value="<?=$arr['kertas_kerja_jam']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset>
				<ul class="rtabs">
					<li><a href="#view1">Uraian</a></li>
					<li><a href="#view2">Kesimpulan</a></li>
				</ul>
				<div id="view1">
					<textarea class="ckeditor" cols="10" rows="40" name="kertas_kerja"
					id="kertas_kerja"><?php echo $arr['kertas_kerja_desc'] ?></textarea>
					<br>
					<label>Lampiran :</label>
					<input type="file" class="span4" name="kka_attach[]" multiple=""> <br>
					<?php
					$z        = 0;
					$rs_file = $kertas_kerjas->list_kka_lampiran($arr['kertas_kerja_id']);
					while ($arr_file = $rs_file->FetchRow()) {
					$z++;
					?>
					<label>
						<input type="checkbox" name="nama_file_<?=$z?>" value="<?=$arr_file['kka_attach_filename']?>">
						<a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_KKA") . $arr_file['kka_attach_filename']?>','_blank')"><?=$arr_file['kka_attach_filename']?></a>
					</label>
					<?php
					}
					?>
				</div>
				<div id="view2">
					<textarea class="ckeditor" cols="10" rows="40" name="kesimpulan" id="kesimpulan"><?php echo $arr['kertas_kerja_kesimpulan'] ?></textarea>
				</div>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['kertas_kerja_id']?>">
			<input type="hidden" name="kka_id" value="<?=$arr['kertas_kerja_id']?>">
			<input type="hidden" name="kka_no" value="<?=$arr['kertas_kerja_no']?>">
			<input type="hidden" name="kka_date" value="<?=$arr['kertas_kerja_date']?>">
			<input type="hidden" name="kka_jam" value="<?=$arr['kertas_kerja_jam']?>">
			<?php
			$f       = 0;
			$rs_file = $kertas_kerjas->list_kka_lampiran($arr['kertas_kerja_id']);
			while ($arr_file = $rs_file->FetchRow()) {
			$f++;
			?>
			<label>
				<input type="hidden" name="kka_filename<?=$f?>" value="<?=$arr_file['kka_attach_filename']?>">
			</label>
			<?php
			}
			?>
			<?
			break;
			case "getdetail":
			$arr         = $rs->FetchRow();
			$rs_komentar = $kertas_kerjas->kka_komentar_viewlist($arr['kertas_kerja_id']);
			?>
			<fieldset class="hr">
				<ul class="rtabs">
					<li><a href="#view1">Rincian</a></li>
					<li><a href="#view2">Histori KKA</a></li>
				</ul>
				<div id="view1">
					<br>
					<table class="view_parrent" width="100%">
						<tr>
							<td><div style="width: 150px">No KKA</div></td>
							<td><div style="width: 10px">:</div></td>
							<td><?=$arr['kertas_kerja_no']?></td>
						</tr>
						<tr>
							<td>Tanggal KKA</td>
							<td>:</td>
							<td><?=$comfunc->dateIndo($arr['kertas_kerja_date'])?></td>
						</tr>
						<tr>
							<td>Realisasi KKA</td>
							<td>:</td>
							<td><?=$arr['kertas_kerja_jam']?> Jam</td>
						</tr>
						<tr>
							<td>Uraian</td>
							<td>:</td>
							<td><div style="overflow-x:hidden;overflow-x: auto; overflow-y: auto; width: 800px;">
								<?=$comfunc->text_show($arr['kertas_kerja_desc'])?>
							</div></td>
						</tr>
						<tr>
							<td>kesimpulan</td>
							<td>:</td>
							<td><div style="overflow-x:hidden;overflow-x: auto; overflow-y: auto; width: 800px;">
								<?=$comfunc->text_show($arr['kertas_kerja_kesimpulan'])?>
							</div></td>
						</tr>
						<tr>
							<td>File Kertas Kerja</td>
							<td>:</td>
							<td>
								<?php
								$z        = 0;
								$rs_file = $kertas_kerjas->list_kka_lampiran($arr['kertas_kerja_id']);
								while ($arr_file = $rs_file->FetchRow()) {
								$z++;
								?>
								<label>
									<a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_KKA") . $arr_file['kka_attach_filename']?>','_blank')"><?=$arr_file['kka_attach_filename']?></a><br>
								</label>
								<?php
								}
								?>
							</td>
						</tr>
						<tr>
							<td>Detail komentar</td>
							<td>:</td>
							<td>
								<?php
								$z = 0;
								while ($arr_komentar = $rs_komentar->FetchRow()) {
								$z++;
								echo $z . ". " . $arr_komentar['auditor_name'] . " : " . $comfunc->text_show($arr_komentar['kertas_kerja_comment_desc']) . "<br>";
								}
								?>
							</td>
						</tr>
					</table>
				</div>
				<div id="view2">
					<?php $counts = $kertas_kerjas->count_list_date_kka_history($arr['kertas_kerja_id']);?>
					<?php if ($counts > 0): ?>
					<br>
					<ul class="rtabs">
						<?php
						$z                = 0;
						$rs_date_history = $kertas_kerjas->list_date_kka_history($arr['kertas_kerja_id']);
						while ($arr_date_history = $rs_date_history->FetchRow()) {
						$z++;
						?>
						<li><a href="#view<?=$comfunc->dateIndo($arr_date_history['kertas_kerja_history_date'])?>"><?=$comfunc->dateIndo($arr_date_history['kertas_kerja_history_date'])?></a></li>
						<?php
						}
						?>
					</ul>
					<?php
					$z                = 0;
					$rs_date_history = $kertas_kerjas->list_date_kka_history($arr['kertas_kerja_id']);
					while ($arr_date_history = $rs_date_history->FetchRow()) {
					$z++;
					?>
					<div id="view<?=$comfunc->dateIndo($arr_date_history['kertas_kerja_history_date'])?>">
						<?php
						$rs_by_date_history = $kertas_kerjas->list_kka_by_history_date($arr_date_history['kertas_kerja_history_date'], $arr['kertas_kerja_id']);
						$t = 0;
						while ($arr_by_date_history = $rs_by_date_history->FetchRow()) {
						$t++;
						?>
						<table class="view_parrent" width="100%">
							<br>
							<tr><td rowspan="8" width="2%"><b><?= $t ?>.</b></td>
							<td><div style="width: 150px">No KKA</div></td>
							<td><div style="width: 10px"><strong>:</strong></div></td>
							<td><?=$arr_by_date_history['kertas_kerja_no']?></td>
						</tr>
						<tr>
							<td>Tanggal KKA</td>
							<td>:</td>
							<td><?=$comfunc->dateIndo($arr_by_date_history['kertas_kerja_date'])?></td>
						</tr>
						<tr>
							<td>Realisasi KKA</td>
							<td>:</td>
							<td><?=$arr_by_date_history['kertas_kerja_jam']?> Jam</td>
						</tr>
						<tr>
							<td>Uraian</td>
							<td>:</td>
							<td><div style="overflow-y: auto; width: 800px">
								<?php echo $comfunc->text_show($arr_by_date_history['kertas_kerja_desc']) ?>
							</div></td>
						</tr>
						<tr>
							<td>Kesimpulan</td>
							<td>:</td>
							<td><div style="overflow-y: auto; width: 800px">
								<?php echo $comfunc->text_show($arr_by_date_history['kertas_kerja_kesimpulan']) ?>
							</div></td>
						</tr>
						<tr>
							<td>File Kertas Kerja</td>
							<td>:</td>
							<td>
								<?php
								$z        = 0;
								$rs_file = explode(', ', $arr_by_date_history['kertas_kerja_attach']);
								foreach($rs_file as $arr_file) {
								$z++;
								?>
								<label>
									<a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_KKA") . $arr_file?>','_blank')"><?=substr($arr_file, 0, 50)?>..</a><br>
								</label>
								<?php
								}
								?>
							</td>
						</tr>
						<tr><td colspan="3"><input type="button" id="hapus-histori" value="Hapus" class="green_btn"></td></tr>
						<tr><td colspan="3">&nbsp;</td></tr>
					</table>
					<?
					}
					?>
				</div>
				<?php
				}
				?>
				<?php else: ?>
				<br>
				<center>Data tidak ditemukan</center>
				<br>
				<?php endif; ?>
			</fieldset>
		<?
		break;
		case "getajukan_kka":
		case "getapprove_kka":
		$arr         = $rs->FetchRow();
		$rs_komentar = $kertas_kerjas->kka_komentar_viewlist($arr['kertas_kerja_id']);
		?>
		<!-- <fieldset class="hr"> -->
		<div style="padding-left: 29px; padding-top: 10px">
			<?php if($_action!='getdetail' && $_action != 'getedit'){
				?>
				<input type="submit" class="blue_btn" value="Simpan">
				<?
				} ?>
				<br>
				<br>
			<table class="view_parrent">
				<tr>
					<td width="160">No KKA</td>
					<td>:</td>
					<td><?=$arr['kertas_kerja_no']?></td>
				</tr>
				<tr>
					<td>Tanggal KKA</td>
					<td>:</td>
					<td><?=$comfunc->dateIndo($arr['kertas_kerja_date'])?></td>
				</tr>
				<tr>
					<td>Uraian</td>
					<td>:</td>
					<td>
						<div style="overflow-x: auto; overflow-y: auto; width: 900px"><?=$comfunc->text_show($arr['kertas_kerja_desc'])?></div></td>
					</tr>
					<tr>
						<td>kesimpulan</td>
						<td>:</td>
						<td>
							<?=$comfunc->text_show($arr['kertas_kerja_kesimpulan'])?>
						</td>
					</tr>
					<tr>
						<td>File Kertas Kerja</td>
						<td>:</td>
						<td>
							<?php
							$z        = 0;
							$rs_file = $kertas_kerjas->list_kka_lampiran($arr['kertas_kerja_id']);
							while ($arr_file = $rs_file->FetchRow()) {
							$z++;
							?>
							<label>
								<a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_KKA") . $arr_file['kka_attach_filename']?>','_blank')"><?=$arr_file['kka_attach_filename']?></a><br>
							</label>
							<?php
							}
							?>
						</td>
					</tr>
					<tr>
						<td>Detail komentar</td>
						<td>:</td>
						<td>
							<?php
							$z = 0;
							while ($arr_komentar = $rs_komentar->FetchRow()) {
							$z++;
							echo $z . ". " . $arr_komentar['auditor_name'] . " : " . $comfunc->text_show($arr_komentar['kertas_kerja_comment_desc']) . "<br>";
							}
							?>
						</td>
					</tr>
				</table>
				
			</div>
			<!-- </article> -->
			<fieldset class="hr">
				<label class="span2">Isi Komentar</label>
				<br><br><br>
				<textarea id="komentar" name="komentar" class="ckeditor" rows="1" cols="20" style="width: 475px; height: 3em; font-size: 11px;"></textarea>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['kertas_kerja_id']?>">
			<input type="hidden" name="status_kka" value="<?=$status?>">
			<?
			break;
			}
			?>
			<fieldset>
				<center>
				<?
				if ($_action == "getedit") {
				?>
				
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<?php $cek_posisi = $kertas_kerjas->cek_posisi($arr_assign ['program_id_assign']);?>
				<input type="submit" class="blue_btn" value="Update">
				&nbsp;&nbsp;&nbsp;
				<input type="button" value="Simpan Histori" id="simpan_history" class="green_btn">
				<?php
				}
				if($_action != "getedit" && $_action != "getdetail"){
				?>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'">
				<input type="submit" class="blue_btn" value="Simpan">
				&nbsp;&nbsp;&nbsp;
				<?php
				}elseif($_action == "getajukan_kka" && $_action == "getapprove_kka") {
				?>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<input type="button" class="blue_btn" value="ms-word" onClick="window.open('AuditManagement/kertas_kerja_print.php?id=<?=$arr['kertas_kerja_id']?>', '_blank','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
				<?
				}
				?>
				<?php if ($_action == "getdetail"): ?>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<input type="button" class="blue_btn" value="ms-word" onClick="window.open('AuditManagement/kertas_kerja_print.php?id=<?=$arr['kertas_kerja_id']?>','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
				<?php endif ?>
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
			</form>
			</center>
		</fieldset>
	</article>
</section>
<script>
$('#tambah_auditee').on('click', function(e){
	e.preventDefault();
	var no = $('#tabel_auditee tr:last').attr('id');
	no++;
	var new_row = '<tr id="'+no+'">'
		+ '<td align="center">'+no+'.</td>'
		+ '<td><input type="file" class="span4" name="kka_attach[]"></td>'
		+ '<td><button class="btn auditee_remove">-</button></td>'
		+ '</tr>';
	$('#tabel_auditee tbody').append(new_row);
});

$("#tabel_auditee").on("click", ".auditee_remove", function(e){
	e.preventDefault();
	$(this).parents("tr").remove();
});

$('#simpan_history').click(function(){
	var data = $("#validation-form").serialize();
	$.ajax({
		url: 'AuditManagement/history_kka.php',
		type: 'POST',
		data: data,
		success: function(data) {
			window.location.reload();
			alert('Berhasil menyimpan history KKA!');
		}
	});
});
$("#kertas_kerja_date").datepicker({
	dateFormat: 'dd-mm-yy',
	nextText: "",
	prevText: "",
	changeYear: true,
	changeMonth: true,
	minDate: '<?= $comfunc->dateIndo($arr_assign['program_start']) ?>'
});
$('textarea').focus(function () {
	checkContentSize("komentar");
});
$('textarea').focusout(function(){
	$(this).animate({ height: "3em" }, 500);
});
$('textarea').keypress(function (e) {
	if (e.which == 13) {
	checkContentSize("komentar");
}
});
function has_scrollbar(elem_id){
	elem = document.getElementById(elem_id);
	if (elem.clientHeight < elem.scrollHeight){
		alert("The element #" + elem_id + " has a vertical scrollbar!");
	} else {
		alert("The element #" + elem_id + " doesn't have a vertical");
	}
}
function checkContentSize(elem_id){
	elem = document.getElementById(elem_id);
	console.log(elem.clientHeight);
	
	if (elem.clientHeight < elem.scrollHeight){
		$( "#" + elem_id + "" ).animate({ height: elem.scrollHeight + 5}, 500);
	}
}
$(function() {
	$("#validation-form").validate({
		rules: {
			no_kka: "required",
			kertas_kerja_date: "required",
			kertas_kerja_jam: "required"
		},
		messages: {
				no_kka: "Masukan Nomor Kertas Kerja",
				kertas_kerja_date : "Silahkan Masukan Tanggal KKA",
				kertas_kerja_jam : "Silahkan Masukan Jumlah Jam KKA"
				},
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>