<?php
include_once "../_includes/common.php";
include_once "../_includes/classes/finding_class.php";
$comfunc  = new comfunction();
$findings = new finding();

$ses_assign_id        = $comfunc->replacetext($_POST["ses_assign_id"]);
$ses_kka_id           = $comfunc->replacetext($_POST["ses_kka_id"]);
$fdata_id             = $comfunc->replacetext($_POST["data_id"]);
$ffinding_auditee     = $comfunc->replacetext($_POST["finding_auditee"]);
$ffinding_no          = $comfunc->replacetext($_POST["finding_no"]);
$ffinding_type        = $comfunc->replacetext($_POST["finding_type"]);
$ffinding_sub_id      = $comfunc->replacetext($_POST["finding_sub_id"]);
$ffinding_jenis_id    = $comfunc->replacetext($_POST["finding_jenis_id"]);
$ffinding_penyebab_id = $comfunc->replacetext($_POST["finding_penyebab_id"]);
$ffinding_judul       = $comfunc->replacetext($_POST["finding_judul"]);
$ffinding_date        = $comfunc->date_db($comfunc->replacetext($_POST["finding_date"]));
$ffinding_kondisi     = $comfunc->replacetext($_POST["finding_kondisi"]);
$ffinding_kriteria    = $comfunc->replacetext($_POST["finding_kriteria"]);
$ffinding_sebab       = $comfunc->replacetext($_POST["finding_sebab"]);
$ffinding_akibat      = $comfunc->replacetext($_POST["finding_akibat"]);
$ftanggapan_auditee   = $comfunc->replacetext($_POST["tanggapan_auditee"]);
$ftanggapan_auditor   = $comfunc->replacetext($_POST["tanggapan_auditor"]);
$ffinding_nilai       = $comfunc->replacetext($_POST["finding_nilai"]);
$ffinding_attach_old  = $comfunc->replacetext($_POST["finding_attach_old"]);

$findings->finding_history_add($ses_assign_id, $ffinding_auditee, $ffinding_no, $ffinding_type, $ffinding_sub_id, $ffinding_jenis_id, $ffinding_judul, $ffinding_date, $ffinding_kondisi, $ffinding_kriteria, $ffinding_sebab, $ffinding_akibat, $ffinding_attach_old, $ses_kka_id, $ftanggapan_auditee, $ftanggapan_auditor, $ffinding_penyebab_id, $ffinding_nilai, $fdata_id, $comfunc->date_db(date('Y-m-d')));
// $comfunc->UploadFile ( "Upload_Temuan", "finding_attach", "" );
