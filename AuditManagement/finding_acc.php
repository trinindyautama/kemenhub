<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<script type="text/javascript" src="css/bootstrap.min.js"></script>
<section id="main" class="column">
	<?
	if (! empty ( $view_parrent ))
	include_once $view_parrent;
	?>
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
			<?
			switch ($_action) {
			case "getadd" :
			?>
			<fieldset>
				<label class="span2">Kode Temuan</label>
                <?
                $rs_kel = $params->finding_type_data_viewlist ();
                $arr_kel = $rs_kel->GetArray ();
                echo $comfunc->buildCombo ( "finding_type", $arr_kel, 0, 2, "", "propinsiOnChange_1(this.value, 'finding_sub_id', 'finding_jenis_id')", "", false, true, false );
                ?>
			</fieldset>
			<fieldset>
				<label class="span2">&nbsp;</label>
				<select name="finding_sub_id" id="finding_sub_id" onchange = "return propinsiOnChange_2(this.value, 'finding_jenis_id')">
					<option value="">Pilih Kelompok Temuan</option>
				</select>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">&nbsp;</label>
				<select name="finding_jenis_id" id="finding_jenis_id">
					<option value="">Pilih Kelompok Temuan</option>
				</select>
				<span class="mandatory">*</span>
			</fieldset>
 			<fieldset class="hr">
				<label class="span2">Judul temuan</label> <input type="text" class="span7" name="finding_judul" id="finding_judul">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset>
				<ul class="rtabs">
					<li><a href="#view1">Uraian</a></li>
					<li><a href="#view3">Sebab & Akibat</a></li>
					<li><a href="#view5">Tanggapan Auditi</a></li>
					<li><a href="#view6">Evaluasi Atas Tanggapan</a></li>
				</ul>
				<div id="view1">
					<textarea class="ckeditor" cols="10" rows="40" name="finding_kondisi" id="finding_kondisi"></textarea>
				</div>
				<div id="view3">
					<textarea class="ckeditor" cols="10" rows="40" name="finding_sebab" id="finding_sebab"></textarea>
                    <fieldset class="hr">
						<label class="span2">Kode Penyebab Temuan</label>
						<?
						$rs_kel = $params->sub_penyebab_data_viewlist ();
						$arr_kel = $rs_kel->GetArray ();
						echo $comfunc->buildCombo ( "finding_penyebab_id", $arr_kel, 0, 2, "", "subOnChange_1(this.value, 'finding_sub_penyebab_id', 'finding_sub_sub_penyebab_id')", "", false, true, false );
						?>
					</fieldset>
					<fieldset class="hr">
					    <label class="span2">&nbsp;</label>
						<select name="finding_sub_penyebab_id" id="finding_sub_penyebab_id" onchange = "return subOnChange_2(this.value, 'finding_sub_sub_penyebab_id')">
							<option value="">Pilih Satu</option>
						</select>
                        <span class="mandatory">*</span>
                    </fieldset>
					<fieldset class="hr">
					    <label class="span2">&nbsp;</label>
						<select name="finding_sub_sub_penyebab_id" id="finding_sub_sub_penyebab_id">
								<option value="">Pilih Satu</option>
						</select>
					</fieldset>
				</div>
				<div id="view5">
					<textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditee" id="tanggapan_auditee"></textarea>
				</div>
 				<div id="view6">
					<textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditor" id="tanggapan_auditor"></textarea>
				</div>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai temuan</label> <input type="text" class="span7" name="finding_nilai" id="finding_nilai">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Lampiran</label> <input type="file" class="span4" name="finding_attach" id="finding_attach">
			</fieldset>
			<input type="hidden" class="span1" name="finding_auditee" id="finding_auditee" value="<?=$auditee_kka?>" />
			<?
			break;
			case "getedit" :
			$arr = $rs->FetchRow ();
			?>
			<fieldset>
				<label class="span2">Kode Temuan</label>
				<?
				$rs_kel = $params->finding_type_data_viewlist ();
				$arr_kel = $rs_kel->GetArray ();
				echo $comfunc->buildCombo ( "finding_type", $arr_kel, 0, 2, $arr['finding_type_id'], "propinsiOnChange_1(this.value, 'finding_sub_id', 'finding_jenis_id')", "", false, true, false );
				?>
			</fieldset>
			<fieldset>
				<label class="span2">&nbsp;</label>
				<?
				$rs_sub = $params->kel_sub_kel ($arr['finding_type_id']);
				$arr_sub = $rs_sub->GetArray ();
				echo $comfunc->buildCombo ( "finding_sub_id", $arr_sub, 0, 1, $arr['finding_sub_id'], "propinsiOnChange_2(this.value, 'finding_jenis_id')", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">&nbsp;</label>
				<?
				$rs_jenis = $params->jenis_temuan_kel ($arr['finding_sub_id']);
				$arr_jenis = $rs_jenis->GetArray();
				echo $comfunc->buildCombo ( "finding_jenis_id", $arr_jenis, 0, 2, $arr['finding_jenis_id'], "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Judul temuan</label> <input type="text"
				class="span7" name="finding_judul" id="finding_judul"
				value="<?=$arr['finding_judul']?>"><span class="mandatory">*</span>
			</fieldset>
			<fieldset>
				<ul class="rtabs">
					<li><a href="#view1">Uraian</a></li>
					<li><a href="#view3">Sebab & Akibat</a></li>
					<li><a href="#view5">Tanggapan Auditi</a></li>
					<li><a href="#view6">Evaluasi Atas Tanggapan</a></li>
				</ul>
				<div id="view1">
					<textarea class="ckeditor" cols="10" rows="40" name="finding_kondisi" id="finding_kondisi"><?=$arr['finding_kondisi']?></textarea>
				</div>
				<div id="view3">
					<textarea class="ckeditor" cols="10" rows="40" name="finding_sebab" id="finding_sebab"><?=$arr['finding_sebab']?></textarea>
                    <fieldset class="hr">
						<label class="span2">Kode Penyebab Temuan</label>
						<?
						$rs_kel2 = $params->sub_penyebab_data_viewlist ();
						$arr_kel2 = $rs_kel2->GetArray ();
						echo $comfunc->buildCombo ( "finding_penyebab_id", $arr_kel2, 0, 2, $arr['finding_penyebab_id'], "subOnChange_1(this.value, 'finding_sub_penyebab_id', 'finding_sub_sub_penyebab_id')", "", false, true, false );
						?>
					</fieldset>
					<fieldset class="hr">
						<label class="span2">&nbsp;</label>
						<?
						$rs_sub2 = $params->sub_to_kel_penyebab ($arr['finding_penyebab_id']);
						$arr_sub2 = $rs_sub2->GetArray ();
						echo $comfunc->buildCombo ( "finding_sub_penyebab_id", $arr_sub2, 0, 1, $arr['finding_sub_penyebab_id'], "subOnChange_2(this.value, 'finding_sub_sub_penyebab_id')", "", false, true, false );
						?>
                        <span class="mandatory">*</span>
					</fieldset>
					<fieldset class="hr">
						<label class="span2">&nbsp;</label>
						<?
						$rs_jenis2 = $params->finding_sub_penyebab_sub_sub ($arr['finding_sub_penyebab_id']);
						$arr_jenis2 = $rs_jenis2->GetArray();
						echo $comfunc->buildCombo ( "finding_sub_sub_penyebab_id", $arr_jenis2, 0, 2, $arr['finding_sub_sub_penyebab_id'], "", "", false, true, false );
						?>
					</fieldset>
				</div>
				<div id="view5">
					<textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditee" id="tanggapan_auditee"><?=$arr['finding_tanggapan_auditee']?></textarea>
				</div>
				<div id="view6">
					<textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditor" id="tanggapan_auditor"><?=$arr['finding_tanggapan_auditor']?></textarea>
				</div>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai temuan</label> <input type="text" class="span7" name="finding_nilai" id="finding_nilai" value="<?=$arr['finding_nilai']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Lampiran</label>
				<input type="hidden" class="span4" name="finding_attach_old" value="<?=$arr['finding_attachment']?>">
				<input type="file" class="span4" name="finding_attach" id="finding_attach">
				<label class="span2"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Temuan").$arr['finding_attachment']?>','_blank')"><?=$arr['finding_attachment']?></a></label>
			</fieldset>
			<input type="hidden" name="ses_assign_id" value="<?=$arr['finding_assign_id']?>">
			<input type="hidden" name="ses_kka_id" value="<?=$arr['finding_kka_id']?>">
			<input type="hidden" name="data_id" value="<?=$arr['finding_id']?>">
			<input type="hidden" class="span1" name="finding_auditee" id="finding_auditee" value="<?=$auditee_kka?>" />
			<?
			break;
			case "getdetail" :
			$arr = $rs->FetchRow ();
			?>
			<fieldset class="hr">
				<ul class="rtabs">
					<li><a href="#view1">Rincian</a></li>
                </ul>
				<div id="view1">
					<br>
					<table class="view_parrent">
						<tr>
							<td>Kode Temuan</td>
							<td>:</td>
							<td><?=$arr['jenis_temuan_code']?> - <?=$arr['jenis_temuan_name']?></td>
						</tr>
						<tr>
							<td>Judul temuan</td>
							<td>:</td>
							<td><?=$arr['finding_judul']?></td>
						</tr>
						<tr>
							<td>Uraian</td>
							<td>:</td>
							<td><?=$comfunc->text_show($arr['finding_kondisi'])?></td>
						</tr>
						<tr>
							<td>Sebab & Akibat</td>
							<td>:</td>
							<td><?=$comfunc->text_show($arr['finding_sebab'])?></td>
						</tr>
                        <tr>
                            <td>Kode Penyebab</td>
                            <td>:</td>
                            <td><?=$comfunc->text_show($arr['kel_penyebab_code'])?> - <?=$comfunc->text_show($arr['kode_penyebab_name'])?> - <?=$comfunc->text_show($arr['penyebab_sub_sub_code'])?></td>
                        </tr>
						<tr>
							<td>Evaluasi Atas Tanggapan</td>
							<td>:</td>
							<td><?=$comfunc->text_show($arr['finding_tanggapan_auditee'])?></td>
						</tr>
						<tr>
							<td>Lampiran</td>
							<td>:</td>
							<td><a href="#"
							Onclick="window.open('<?=$comfunc->baseurl("Upload_Temuan").$arr['finding_attachment']?>','_blank')"><?=$arr['finding_attachment']?></a></td>
						</tr>
					</table>
				</div>
			</fieldset>
			<?
			break;
			case "getajukan_temuan" :
			case "getapprove_temuan" :
			$arr = $rs->FetchRow ();
			?>
			<fieldset class="hr">
				<table class="view_parrent" width="100%">
					<tr>
                        <td>Kode Temuan</td>
                        <td>:</td>
                        <td><?=$arr['jenis_temuan_code']?> - <?=$arr['jenis_temuan_name']?></td>
                    </tr>
                    <tr>
                        <td>Judul temuan</td>
                        <td>:</td>
                        <td><?=$arr['finding_judul']?></td>
                    </tr>
                    <tr>
                        <td>Uraian</td>
                        <td>:</td>
                        <td><?=$comfunc->text_show($arr['finding_kondisi'])?></td>
                    </tr>
                    <tr>
                        <td>Sebab & Akibat</td>
                        <td>:</td>
                        <td><?=$comfunc->text_show($arr['finding_sebab'])?></td>
                    </tr>
                    <tr>
                        <td>Kode Penyebab</td>
                        <td>:</td>
                        <td><?=$comfunc->text_show($arr['kel_penyebab_code'])?> - <?=$comfunc->text_show($arr['kode_penyebab_name'])?> - <?=$comfunc->text_show($arr['penyebab_sub_sub_code'])?></td>
                    </tr>
                    <tr>
                        <td>Evaluasi Atas Tanggapan</td>
                        <td>:</td>
                        <td><?=$comfunc->text_show($arr['finding_tanggapan_auditee'])?></td>
                    </tr>
					<tr>
						<td>Lampiran</td>
						<td>:</td>
						<td><a href="#"
						Onclick="window.open('<?=$comfunc->baseurl("Upload_Temuan").$arr['finding_attachment']?>','_blank')"><?=$arr['finding_attachment']?></a></td>
					</tr>
					<tr>
						<td>Detail komentar</td>
						<td>:</td>
						<td>
							<?php
							$z = 0;
							$rs_komentar = $findings->finding_komentar_viewlist ( $arr ['finding_id'] );
							while ( $arr_komentar = $rs_komentar->FetchRow () ) {
								$z ++;
								echo $z.". ".$arr_komentar['auditor_name']." : ".$comfunc->text_show($arr_komentar['find_comment_desc'])."<br>";
							}
							?>
						</td>
					</tr>
				</table>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Isi Komentar</label>
				<br>
				<br>
				<br>
				<textarea id="komentar" name="komentar" class="ckeditor" rows="1" cols="20" style="width: 475px; height: 3em; font-size: 11px;"></textarea>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['finding_id']?>">
			<input type="hidden" name="program_id_find" value="<?=$prog_id_find?>">
			<input type="hidden" name="status_temuan" value="<?=$status?>">
			<?
			break;
			}
			?>
			<fieldset>
				<center>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<?
				if($_action!='getdetail' && $_action != 'getedit'){
				?>
				<input type="submit" class="blue_btn" value="Simpan">
				<?
				}
				?>
				<?php if ($_action == 'getedit'): ?>
				<input type="submit" class="blue_btn" value="Simpan">
				<?php foreach (range(1,2) as $value): ?>
				&nbsp;
				<?php endforeach ?>
				<?php endif ?>
				</center>
				<input type="hidden" name="data_action" id="data_action"
				value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script>
$("#finding_date").datepicker({
	dateFormat: 'dd-mm-yy',
	nextText: "",
	prevText: "",
	changeYear: true,
	changeMonth: true
});
$('#simpan_history').click(function(){
var data = $("#validation-form").serialize();
$.ajax({
	url: 'AuditManagement/history_matrix_temuan.php',
	type: 'POST',
	data: data,
		success: function(data) {
			location.reload();
			alert('Berhasil menyimpan Histori Temuan!');
		}
	});
});
$(function() {
	$("#validation-form").validate({
		rules: {
			finding_auditee: "required",
			finding_judul: "required",
			finding_jenis_id: "required",
            finding_sub_penyebab_id: "required"
		},
		messages: {
			finding_auditee: "Silahkan Pilih Satuan Kerja",
			finding_judul: "Silahkan Masukan Judul Temuan",
			finding_jenis_id: "Pilih Kode Temuan",
            finding_sub_penyebab_id: "Silahkan Pilih Kode Penyebab"
			},
		submitHandler: function(form) {
			form.submit();
		}
	});
});
function selectRemoveAll(objSel) {
	document.getElementById(objSel).options.length = 0;
}
function selectAdd(objSel, objVal, objCap, isSelected) {
	var nextLength = document.getElementById(objSel).options.length;
	document.getElementById(objSel).options[nextLength] = new Option(objCap, objVal, false, isSelected);
}
function propinsiOnChange_1(objValue, cmbNext_1, cmbNext_2){
	objSel_1 = cmbNext_1;
	selectRemoveAll(objSel_1);
	
	objSel_2 = cmbNext_2;
	selectRemoveAll(objSel_2);
	selectAdd(objSel_1, "", "Pilih Satu");
	selectAdd(objSel_2, "", "Pilih Sub Kelompok");
	switch (objValue) {
	<?
		$rs1 = $params->finding_type_data_viewlist ();
		$arr1 = $rs1->GetArray();
		$rs1->Close();
		foreach ($arr1 as $value1) {
			echo("case \"$value1[0]\":\n");
			$rs2 = $params->kel_sub_kel($value1[0]);
			$arr2 = $rs2->GetArray();
			$rs2->Close();
				foreach ($arr2 as $value2) {
					$isSelected="false";
					echo("\tselectAdd(objSel_1, \"$value2[0]\", \"$value2[1]\", $isSelected);\n");
				}
			echo("\tbreak;\n");
		}	
		?>
	}
}
function propinsiOnChange_2(objValue, cmbNext){
	objSel = cmbNext;
		selectRemoveAll(objSel);
		selectAdd(objSel, "", "Pilih Satu");
		switch (objValue) {
		<?
			$rs1 = $params->kel_sub_kel ();
			$arr1 = $rs1->GetArray();
			$rs1->Close();
			foreach ($arr1 as $value1) {
				echo("case \"$value1[0]\":\n");
				$rs2 = $params->jenis_temuan_kel($value1[0]);
				$arr2 = $rs2->GetArray();
				$rs2->Close();
				foreach ($arr2 as $value2) {
					$isSelected="false";
					echo("\tselectAdd(objSel, \"$value2[0]\", \"$value2[2]\", $isSelected);\n");
				}
				echo("\tbreak;\n");
			}
		?>
	}
}

function subOnChange_1(objValue, cmbNext_1, cmbNext_2){
	objSel_1 = cmbNext_1;
	selectRemoveAll(objSel_1);
	
	objSel_2 = cmbNext_2;
	selectRemoveAll(objSel_2);
	selectAdd(objSel_1, "", "Pilih Satu");
	selectAdd(objSel_2, "", "Pilih Satu");
	switch (objValue) {
	<?
		$rs1 = $params->sub_penyebab_data_viewlist ();
		$arr1 = $rs1->GetArray();
		$rs1->Close();
		foreach ($arr1 as $value1) {
			echo("case \"$value1[0]\":\n");
			$rs2 = $params->sub_to_kel_penyebab($value1[0]);
			$arr2 = $rs2->GetArray();
			$rs2->Close();
				foreach ($arr2 as $value2) {
					$isSelected="false";
					echo("\tselectAdd(objSel_1, \"$value2[0]\", \"$value2[1]\", $isSelected);\n");
				}
			echo("\tbreak;\n");
		}	
		?>
	}
}
function subOnChange_2(objValue, cmbNext){
	objSel = cmbNext;
		selectRemoveAll(objSel);
		selectAdd(objSel, "", "Pilih Satu");
		switch (objValue) {
		<?
			$rs1 = $params->sub_to_kel_penyebab ();
			$arr1 = $rs1->GetArray();
			$rs1->Close();
			foreach ($arr1 as $value1) {
				echo("case \"$value1[0]\":\n");
				$rs2 = $params->finding_sub_penyebab_sub_sub($value1[0]);
				$arr2 = $rs2->GetArray();
				$rs2->Close();
				foreach ($arr2 as $value2) {
					$isSelected="false";
					echo("\tselectAdd(objSel, \"$value2[0]\", \"$value2[2]\", $isSelected);\n");
				}
				echo("\tbreak;\n");
			}
		?>
	}
}

</script>