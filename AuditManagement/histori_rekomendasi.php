<?php
include_once "../_includes/common.php";
include_once "../_includes/classes/rekomendasi_class.php";
$comfunc      = new comfunction();
$rekomendasis = new rekomendasi();

$data_id = $comfunc->replacetext($_POST["data_id"]);
$ffinding_id = $comfunc->replacetext($_POST["finding_id"]);
$fkode_rekomendasi = $comfunc->replacetext($_POST["kode_rekomendasi"]);
$frekomendasi_desc = $comfunc->replacetext($_POST["rekomendasi_desc"]);
$frekomendasi_pic  = $comfunc->replacetext($_POST["rekomendasi_pic"]);
$ftarget_date      = $comfunc->date_db($comfunc->replacetext($_POST["target_date"]));
$frekomendasi_date = $comfunc->replacetext($_POST["rekomendasi_date"]);

$rekomendasis->rekomendasi_histori_add ( $ffinding_id, $data_id, $comfunc->date_db(date('d-m-Y')),$fkode_rekomendasi, $frekomendasi_desc, $frekomendasi_pic, $ftarget_date, $frekomendasi_date );
// $comfunc->UploadFile ( "Upload_Temuan", "finding_attach", "" );
