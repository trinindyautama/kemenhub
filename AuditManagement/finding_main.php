<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/finding_class.php";
include_once "_includes/classes/kertas_kerja_class.php";
include_once "_includes/classes/rekomendasi_class.php";
include_once "_includes/classes/param_class.php";

$assigns       = new assign($ses_userId);
$kertas_kerjas = new kertas_kerja($ses_userId);
$findings      = new finding($ses_userId);
$rekomendasis  = new rekomendasi($ses_userId);
$params        = new param($ses_userId);

@$_action = $comfunc->replacetext($_REQUEST["data_action"]);

if (isset($_POST["val_search"])) {
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if (@$method != @$val_method) {
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

if ($method == 'finding_kka') {
    $paging_request = "main_page.php?method=finding_kka";
} else {
    $paging_request = "main_page.php?method=finding";
}
$acc_page_request  = "finding_acc.php";
$list_page_request = "audit_view.php";
// ==== buat grid ===//
$num_row   = 10;
@$str_page = $comfunc->replacetext($_GET['page']);
if (isset($str_page)) {
    if (is_numeric($str_page) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;


@$id_temuan_dash      = $comfunc->replacetext($_REQUEST["id_temuan_dash"]);
$rs_get_data_parrent  = $findings->finding_viewlist($id_temuan_dash);
$arr_get_data_parrent = $rs_get_data_parrent->FetchRow();
if ($arr_get_data_parrent['finding_kka_id'] == "" && $arr_get_data_parrent['finding_assign_id'] == "") {
    $ses_assign_id = $_SESSION['ses_assign_id'];
    @$ses_kka_id   = $_SESSION['ses_kka_id'];
    $def_page_request = $paging_request . "&page=$noPage";
} else {
    $ses_assign_id = $arr_get_data_parrent['finding_assign_id'];
    @$ses_kka_id   = $arr_get_data_parrent['finding_kka_id'];
    $def_page_request = "main_page.php?method=$method&id_temuan_dash=$id_temuan_dash";
}

$view_parrent = "assign_view_parrent.php";
$grid         = "grid_finding.php";
$gridHeader   = array("No Temuan", "Judul Temuan", "Satuan Kerja", "Rekomendasi", "Status");
$gridDetail   = array("finding_no", "finding_judul", "auditee_name", "finding_id", "finding_status");
$gridWidth    = array("10", "40", "15", "10", "10");

$key_by    = array("No Temuan", "Judul Temuan", "Satuan Kerja");
$key_field = array("finding_no", "finding_judul", "auditee_name");

$widthAksi  = "15";
$iconDetail = "1";
// === end grid ===//

$status_katim = $assigns->assign_cek_katim($ses_assign_id, $ses_userId);

switch ($_action) {
    case "getajukan_temuan":
    case "getapprove_temuan":
        $_nextaction  = "postkomentar";
        $page_request = $acc_page_request;
        $id           = $comfunc->replacetext($_REQUEST["data_id"]);
        $status       = $comfunc->replacetext($_REQUEST["status_temuan"]);

        $rs         = $findings->finding_viewlist($id);
        $page_title = "Reviu Temuan";
        break;
    case "postkomentar":
        $id_temuan       = $comfunc->replacetext($_POST["data_id"]);
        $fkomentar       = $comfunc->replacetext($_POST["komentar"]);
        $status          = $comfunc->replacetext($_POST["status_temuan"]);
        $program_id_find = $comfunc->replacetext($_POST["program_id_find"]);

        $comfunc->hapus_notif($id_temuan);
        if ($status != "") {
            if ($status == 1) {
                $get_user_id      = $kertas_kerjas->get_user_by_posisi($program_id_find, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            } elseif ($status == 2) {
                $get_user_id      = $kertas_kerjas->get_user_by_posisi($program_id_find, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            } elseif ($status == 5 || $status == 0) {
                $get_user_id      = $kertas_kerjas->get_user_owner($program_id_find);
                $notif_to_user_id = $get_user_id; //ke anggota
            } elseif ($status == 3) {
                $get_user_id      = $kertas_kerjas->get_user_by_posisi($program_id_find, '1fe7f8b8d0d94d54685cbf6c2483308aebe96229');
                $notif_to_user_id = $get_user_id; //ke daltu
            } elseif ($status == 6) {
                $get_user_id      = $kertas_kerjas->get_user_by_posisi($program_id_find, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            } elseif ($status == 7) {
                $get_user_id      = $kertas_kerjas->get_user_by_posisi($program_id_find, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }
            if ($notif_to_user_id != "") {
                $comfunc->insert_notif($id_temuan, $ses_userId, $notif_to_user_id, 'finding_kka', '(Temuan) ' . $fkomentar, $comfunc->date_db(date('d-m-Y')));
            }
            $findings->finding_update_status($id_temuan, $status);
        }

        $ftanggal = $comfunc->date_db(date("d-m-Y H:i:s"));
        if ($fkomentar != "") {
            $findings->finding_add_komentar($id_temuan, $fkomentar, $ftanggal);
            $comfunc->js_alert_act(3);
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    case "rekomendasi":
        $_SESSION['ses_finding_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
        ?>
<script>window.open('main_page.php?method=rekomendasi', '_self');</script>
<?
        break;
    case "getadd":
        $_nextaction  = "postadd";
        $page_request = $acc_page_request;
        $page_title   = "Tambah Temuan Audit";
        break;
    case "getedit":
        $_nextaction  = "postedit";
        $page_request = $acc_page_request;
        $fdata_id     = $comfunc->replacetext($_REQUEST["data_id"]);
        $rs           = $findings->finding_viewlist($fdata_id);
        $page_title   = "Ubah Temuan Audit";
        break;
    case "getdetail":
        $page_request = $acc_page_request;
        $fdata_id     = $comfunc->replacetext($_REQUEST["data_id"]);
        $rs           = $findings->finding_viewlist($fdata_id);
        $page_title   = "Rincian Temuan Audit";
        break;
    case "postadd":
        $ffinding_auditee     = $comfunc->replacetext($_POST["finding_auditee"]);
        $ffinding_no          = $comfunc->replacetext($_POST["finding_no"]);
        $ffinding_type        = $comfunc->replacetext($_POST["finding_type"]);
        $ffinding_sub_id      = $comfunc->replacetext($_POST["finding_sub_id"]);
        $ffinding_jenis_id    = $comfunc->replacetext($_POST["finding_jenis_id"]);
        $ffinding_penyebab_id = $comfunc->replacetext($_POST["finding_penyebab_id"]);
        $ffinding_judul       = $comfunc->replacetext($_POST["finding_judul"]);
        $ffinding_date        = $comfunc->date_db($comfunc->replacetext($_POST["finding_date"]));
        $ffinding_kondisi     = $comfunc->replacetext($_POST["finding_kondisi"]);
        $ffinding_kriteria    = $comfunc->replacetext($_POST["finding_kriteria"]);
        $ffinding_sebab       = $comfunc->replacetext($_POST["finding_sebab"]);
        $ffinding_akibat      = $comfunc->replacetext($_POST["finding_akibat"]);
        $ftanggapan_auditee   = $comfunc->replacetext($_POST["tanggapan_auditee"]);
        $ftanggapan_auditor   = $comfunc->replacetext($_POST["tanggapan_auditor"]);
        $ffinding_nilai       = $comfunc->replacetext($_POST["finding_nilai"]);
        $ffinding_attachment  = $comfunc->replacetext($_FILES["finding_attach"]["name"]);
        if ($ffinding_auditee != "" && $ffinding_no != "" && $ffinding_judul != "" && $ffinding_jenis_id != "") {
            $findings->finding_add($ses_assign_id, $ffinding_auditee, $ffinding_no, $ffinding_type, $ffinding_sub_id, $ffinding_jenis_id, $ffinding_judul, $ffinding_date, $ffinding_kondisi, $ffinding_kriteria, $ffinding_sebab, $ffinding_akibat, $ffinding_attachment, $ses_kka_id, $ftanggapan_auditee, $ftanggapan_auditor, $ffinding_penyebab_id, $ffinding_nilai);
            $comfunc->UploadFile("Upload_Temuan", "finding_attach", "");
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    case "postedit":
        $fdata_id             = $comfunc->replacetext($_POST["data_id"]);
        $ffinding_auditee     = $comfunc->replacetext($_POST["finding_auditee"]);
        $ffinding_no          = $comfunc->replacetext($_POST["finding_no"]);
        $ffinding_type        = $comfunc->replacetext($_POST["finding_type"]);
        $ffinding_sub_id      = $comfunc->replacetext($_POST["finding_sub_id"]);
        $ffinding_jenis_id    = $comfunc->replacetext($_POST["finding_jenis_id"]);
        $ffinding_penyebab_id = $comfunc->replacetext($_POST["finding_penyebab_id"]);
        $ffinding_judul       = $comfunc->replacetext($_POST["finding_judul"]);
        $ffinding_date        = $comfunc->date_db($comfunc->replacetext($_POST["finding_date"]));
        $ffinding_kondisi     = $comfunc->replacetext($_POST["finding_kondisi"]);
        $ffinding_kriteria    = $comfunc->replacetext($_POST["finding_kriteria"]);
        $ffinding_sebab       = $comfunc->replacetext($_POST["finding_sebab"]);
        $ffinding_akibat      = $comfunc->replacetext($_POST["finding_akibat"]);
        $ftanggapan_auditee   = $comfunc->replacetext($_POST["tanggapan_auditee"]);
        $ftanggapan_auditor   = $comfunc->replacetext($_POST["tanggapan_auditor"]);
        $ffinding_nilai       = $comfunc->replacetext($_POST["finding_nilai"]);
        $ffinding_attachment  = $comfunc->replacetext($_FILES["finding_attach"]["name"]);
        $ffinding_attach_old  = $comfunc->replacetext($_POST["finding_attach_old"]);
        if ($ffinding_auditee != "" && $ffinding_no != "" && $ffinding_judul != "" && $ffinding_date != "" && $ffinding_jenis_id != "") {
            if (!empty($ffinding_attachment)) {
                $comfunc->UploadFile("Upload_Temuan", "finding_attach", $ffinding_attach_old);
            } else {
                $ffinding_attachment = $ffinding_attach_old;
            }
            $findings->finding_edit($fdata_id, $ffinding_auditee, $ffinding_no, $ffinding_type, $ffinding_sub_id, $ffinding_jenis_id, $ffinding_judul, $ffinding_date, $ffinding_kondisi, $ffinding_kriteria, $ffinding_sebab, $ffinding_akibat, $ffinding_attachment, $ftanggapan_auditee, $ftanggapan_auditor, $ffinding_penyebab_id, $ffinding_nilai);
            $comfunc->js_alert_act(1);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    case "getdelete":
        $fdata_id = $comfunc->replacetext($_REQUEST["data_id"]);
        $comfunc->hapus_notif($fdata_id);
        $findings->finding_delete($fdata_id);
        $comfunc->js_alert_act(2);
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    default:
        $recordcount  = $findings->finding_count($ses_assign_id, $ses_kka_id, $key_search, $val_search, $key_field);
        $rs           = $findings->finding_view_grid($ses_assign_id, $ses_kka_id, $key_search, $val_search, $key_field, $offset, $num_row);
        $page_title   = "Daftar Temuan Audit";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>
