<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<link rel="stylesheet" href="js/select2/select2.css" type="text/css"/>
<script type="text/javascript" src="js/select2/select2.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <header>
            <h3 class="tabs_involved"><?=$page_title?></h3>
        </header>
        <form method="post" name="f" action="#" class="form-horizontal"	id="validation-form" enctype="multipart/form-data">
            <?
            switch ($_action) {
                case "getadd" :
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Obyek Pemeriksaan</label> <input type="hidden" name="auditee" id="auditee" class="select2 multiple" /><span class="mandatory">*</span>
                    </fieldset>
                    <input type="hidden" class="span1" name="tahun" id="tahun">
					<fieldset class="hr">
                        <label class="span2">Tanggal Kegiatan</label>
                        <input type="text" class="span1" name="tanggal_awal" id="tanggal_awal">
                        <label class="span0">s/d</label>
                        <input type="text" class="span1" name="tanggal_akhir" id="tanggal_akhir">
                        <span class="mandatory">*</span>
                    </fieldset>
					<fieldset class="hr">
						<label class="span2">No Surat Tugas</label>
						<input type="text" class="span3" name="no_surat" id="no_surat">
						<span class="mandatory">*</span>
					</fieldset>
					<fieldset class="hr">
						<label class="span2">Tanggal Surat Tugas</label> 
						<input type="text" class="span1" name="tanggal_surat" id="tanggal_surat">
						<span class="mandatory">*</span>
					</fieldset>
					<fieldset class="hr">
						<input type="button" id="tambah_personil" class="btn" value="Tambah Personil Tim">
						<table id="table_personil" width="50%">
							<thead>
								<tr>
									<th align="left" width="3%">No.</th>
									<th align="left" width="40%">Nama</th>
									<th align="left" width="50%">Posisi</span></th>
									<th width="5%">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr id="1">
									<td align="center">1.</td>
									<td><input type="hidden" name="anggota_id[]" id="anggota_id" class="select2 anggota_id" /></td>
									<td><?=$comfunc->dbCombo("posisi_id[]", "par_posisi_penugasan", "posisi_id", "posisi_name", "and posisi_del_st = 1 ", "", "", 1, "order by posisi_sort")?></td>
									<td>&nbsp;</td>
								</tr>
								<span class="mandatory">*</span>
							</tbody>
						</table>
					</fieldset>
                    <?
                    break;
                case "getedit" :
                    $arr = $rs->FetchRow ();
					$arr_surat = $rs_surat->FetchRow ();
					$arr_anggota = $rs_anggota->FetchRow ();
                    $rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
                    $assign_id_auditee = "";
                    while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
                        $assign_id_auditee .= $arr_id_auditee ['assign_auditee_id_auditee'] . ",";
                    }
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Obyek Pemeriksaan</label>
                        <input type="hidden" name="auditee" id="auditee" class="select2 multiple" value="<?=$assign_id_auditee?>" />
                    </fieldset>
                    <input type="hidden" class="span1" name="tahun" id="tahun" value="<?=$arr['assign_tahun']?>">
                    <fieldset class="hr">
                        <label class="span2">Tanggal Kegiatan</label>
                        <input type="text" class="span1" name="tanggal_awal" id="tanggal_awal" value="<?=$comfunc->dateIndo($arr['assign_start_date'])?>">
                        <label class="span0">s/d</label>
                        <input type="text" class="span1" name="tanggal_akhir" id="tanggal_akhir" value="<?=$comfunc->dateIndo($arr['assign_end_date'])?>">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
						<label class="span2">No Surat Tugas</label>
						<input type="text" class="span3" name="no_surat" id="no_surat" value="<?=$arr_surat['assign_surat_no']?>">
						<span class="mandatory">*</span>
					</fieldset>
					<fieldset class="hr">
						<label class="span2">Tanggal Surat Tugas</label> 
						<input type="text" class="span1" name="tanggal_surat" id="tanggal_surat" value="<?=$comfunc->dateIndo($arr_surat['assign_surat_tgl'])?>">
						<span class="mandatory">*</span>
					</fieldset>
					<fieldset class="hr">
						<input type="button" id="tambah_personil" class="btn" value="Tambah Personil Tim">
						<table id="table_personil" width="50%">
							<thead>
								<tr>
									<th align="left" width="3%">No.</th>
									<th align="left" width="40%">Nama</th>
									<th align="left" width="50%">Posisi</span></th>
									<th width="5%">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?
								$no_personil = 0;
								$rs_personil = $assigns->assign_auditor_assignment_viewlist($arr_anggota['assign_auditor_id_assign']);
								while($arr_personil = $rs_personil->FetchRow()){
									$no_personil++;
								?>
								<input type="hidden" name="id_personil[]" value="<?=$arr_personil['assign_auditor_id']?>">
								<tr id="<?=$no_personil?>">
									<td align="center"><?=$no_personil?>.</td>
									<td><input type="hidden" name="anggota_id[]" id="anggota_id" class="select2 anggota_id" value="<?=$arr_personil['assign_auditor_id_auditor']?>"/></td>
									<td><?=$comfunc->dbCombo("posisi_id[]", "par_posisi_penugasan", "posisi_id", "posisi_name", "and posisi_del_st = 1 ", $arr_personil['assign_auditor_id_posisi'], "", 1, "order by posisi_sort")?></td>
									<td>&nbsp;</td>
								</tr>
								<?
								}
								?>
								<tr id="<?=$no_personil+1?>">
									<td align="center"><?=$no_personil+1?>.</td>
									<td><input type="hidden" name="anggota_id[]" id="anggota_id" class="select2 anggota_id" /></td>
									<td><?=$comfunc->dbCombo("posisi_id[]", "par_posisi_penugasan", "posisi_id", "posisi_name", "and posisi_del_st = 1 ", "", "", 1, "order by posisi_sort")?></td>
									<td>&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['assign_id']?>">
                    <?
                    break;
                case "getdetail" :
                    $arr = $rs->FetchRow ();
					$arr_surat = $rs_surat->FetchRow ();
                    $rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
                    $assign_id_auditee = "";
                    while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
                        $assign_id_auditee .= $arr_id_auditee ['auditee_name'] . "<br>";
                    }
                    ?>
                    <fieldset class="hr">
                        <table class="view_parrent">
                            <tr>
                                <td>Obyek Pemeriksaan</td>
                                <td>:</td>
                                <td><?=$assign_id_auditee?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Kegiatan</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr['assign_start_date'])?> s/d <?=$comfunc->dateIndo($arr['assign_end_date'])?></td>
                            </tr>
							<tr>
                                <td>No Surat Tugas</td>
                                <td>:</td>
                                <td><?=$arr_surat['assign_surat_no']?></td>
                            </tr>
							<tr>
                                <td>Tanggal Surat</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr_surat['assign_surat_tgl'])?></td>
                            </tr>
						</table>
                    </fieldset>
                    <fieldset>
                        <label class="span2">Susunan Tim</label>
                        <table border="1" cellpadding="0" cellspacing="0" class="table_risk">
                            <thead>
                            <tr>
                                <th width="80%">Nama Anggota</th>
                                <th width="20%">Posisi</th>
                            </tr>
                            </thead>
                            <tbody id="table_desc">
                            <?php
                            $rs_detail = $assigns->view_auditor_assign ( $arr ['assign_id'] );
                            while ( $arr_detil = $rs_detail->FetchRow () ) {
                                ?>
                                <tr class="row_item">
                                    <td><?=$arr_detil ['auditor_name']?></td>
                                    <td><?=$arr_detil ['posisi_name']?></td>
                                </tr>
                                <?
                            }
                            ?>
                            </tbody>
                        </table>
                    </fieldset>
                    <?
                    break;
                case "getajukan_penugasan" :
                case "getapprove_penugasan" :
                    $arr = $rs->FetchRow ();
                    $rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
                    $assign_id_auditee = "";
                    while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
                        $assign_id_auditee .= $arr_id_auditee ['auditee_name'] . "<br>";
                    }
                    ?>
                    <fieldset class="hr">
                        <table class="view_parrent">
                            <tr>
                                <td>Tipe Pengawasan</td>
                                <td>:</td>
                                <td><?=$arr['audit_type_name']?></td>
                            </tr>
                            <tr>
                                <td>Obyek Pemeriksaan</td>
                                <td>:</td>
                                <td><?=$assign_id_auditee?></td>
                            </tr>
                            <tr>
                                <td>Tahun</td>
                                <td>:</td>
                                <td><?=$arr['assign_tahun']?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Kegiatan</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr['assign_start_date'])?> s/d <?=$comfunc->dateIndo($arr['assign_end_date'])?></td>
                            </tr>
                            <tr>
                                <td>Jumlah Hari Persiapan</td>
                                <td>:</td>
                                <td><?=$arr['assign_hari_persiapan']?></td>
                            </tr>
                            <tr>
                                <td>Jumlah Hari Pelaksanaan</td>
                                <td>:</td>
                                <td><?=$arr['assign_hari_pelaksanaan']?></td>
                            </tr>
                            <tr>
                                <td>Jumlah Hari Pelaporan</td>
                                <td>:</td>
                                <td><?=$arr['assign_hari_pelaporan']?></td>
                            </tr>
                            <tr>
                                <td>Periode</td>
                                <td>:</td>
                                <td><?php echo $comfunc->text_show($arr['assign_periode']);?><td>
                            </tr>
                            <tr>
                                <td>Dasar</td>
                                <td>:</td>
                                <td><?php echo $comfunc->text_show($arr['assign_dasar']);?></td>
                            </tr>
                            <tr>
                                <td>Pendanaan</td>
                                <td>:</td>
                                <td><?php echo $comfunc->text_show($arr['assign_pendanaan']);?></td>
                            </tr>
                            <tr>
                                <td>Keterangan Lain</td>
                                <td>:</td>
                                <td><?php echo $comfunc->text_show($arr['assign_keterangan']);?></td>
                            </tr>
                            <!-- <?php if ($ses_group_id != '25a0bc88a8aca130913d7b35facf9e3505bf12af'): ?> -->
                            <tr>
                                <td>Detail komentar</td>
                                <td>:</td>
                                <td>
											<?php
											$z = 0;
											$rs_komentar = $assigns->assign_komentar_viewlist ( $arr ['assign_id'] );
											while ( $arr_komentar = $rs_komentar->FetchRow () ) {
												$z ++;
												echo $z.". ".$arr_komentar['auditor_name']." : ".$arr_komentar['assign_comment_desc']."<br>";
											}
											?>
										</td>
                            </tr>
                            <!-- <?php endif ?> -->
                        </table>
                    </fieldset>
                    <fieldset>
                        <label class="span2">Susunan Tim</label>
                        <table border="1" cellpadding="0" cellspacing="0" class="table_risk">
                            <thead>
                            <tr>
                                <th width="80%">Nama Anggota</th>
                                <th width="20%">Posisi</th>
                            </tr>
                            </thead>
                            <tbody id="table_desc">
                            <?php
                            $rs_detail = $assigns->view_auditor_assign ( $arr ['assign_id'] );
                            while ( $arr_detil = $rs_detail->FetchRow () ) {
                                ?>
                                <tr class="row_item">
                                    <td><?=$arr_detil ['auditor_name']?></td>
                                    <td><?=$arr_detil ['posisi_name']?></td>
                                </tr>
                                <?
                            }
                            ?>
                            </tbody>
                        </table>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Isi Komentar</label>
                        <textarea id="komentar" name="komentar" rows="1" cols="20" style="width: 475px; height: 3em; font-size: 11px;"></textarea>
                    </fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['assign_id']?>">
                    <input type="hidden" name="status_penugasan" value="<?=$status?>">
                    <?
                    break;
                case "upload_data" :
                    $arr = $rs->FetchRow ();
                    $rs_file = $assigns->assign_upload_viewlist ( $arr ['assign_id'] );
                    $file = array();
                    while ( $arr_file = $rs_file->FetchRow () ) {
                        $file[] = $arr_file ['upload_awal_nama_file'];
                    }
                    ?>
                    <fieldset class="hr">
                        <label class="span1">File 1</label>
                        <input type="hidden" class="span4" name="file_1_old" value="<?=$file[0]?>">
                        <input type="file" class="span2" name="file_1" id="file_1">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[0]?>','_blank')"><?=$file[0]?></a></label>
                        <input type="checkbox" name="del_1" value="1"> Hapus File 1
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 2</label>
                        <input type="hidden" class="span4" name="file_2_old" value="<?=$file[1]?>">
                        <input type="file" class="span2" name="file_2" id="file_2">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[1]?>','_blank')"><?=$file[1]?></a></label>
                        <input type="checkbox" name="del_2" value="1"> Hapus File 2
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 3</label>
                        <input type="hidden" class="span4" name="file_3_old" value="<?=$file[2]?>">
                        <input type="file" class="span2" name="file_3" id="file_3">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[2]?>','_blank')"><?=$file[2]?></a></label>
                        <input type="checkbox" name="del_3" value="1"> Hapus File 3
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 4</label>
                        <input type="hidden" class="span4" name="file_4_old" value="<?=$file[3]?>">
                        <input type="file" class="span2" name="file_4" id="file_4">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[3]?>','_blank')"><?=$file[3]?></a></label>
                        <input type="checkbox" name="del_4" value="1"> Hapus File 4
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 5</label>
                        <input type="hidden" class="span4" name="file_5_old" value="<?=$file[4]?>">
                        <input type="file" class="span2" name="file_5" id="file_5">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[4]?>','_blank')"><?=$file[4]?></a></label>
                        <input type="checkbox" name="del_5" value="1"> Hapus File 5
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 6</label>
                        <input type="hidden" class="span4" name="file_6_old" value="<?=$file[5]?>">
                        <input type="file" class="span2" name="file_6" id="file_6">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[5]?>','_blank')"><?=$file[5]?></a></label>
                        <input type="checkbox" name="del_6" value="1"> Hapus File 6
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 7</label>
                        <input type="hidden" class="span4" name="file_7_old" value="<?=$file[6]?>">
                        <input type="file" class="span2" name="file_7" id="file_7">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[6]?>','_blank')"><?=$file[6]?></a></label>
                        <input type="checkbox" name="del_7" value="1"> Hapus File 7
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 8</label>
                        <input type="hidden" class="span4" name="file_8_old" value="<?=$file[7]?>">
                        <input type="file" class="span2" name="file_8" id="file_8">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[7]?>','_blank')"><?=$file[7]?></a></label>
                        <input type="checkbox" name="del_8" value="1"> Hapus File 8
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 9</label>
                        <input type="hidden" class="span4" name="file_9_old" value="<?=$file[8]?>">
                        <input type="file" class="span2" name="file_9" id="file_9">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[8]?>','_blank')"><?=$file[8]?></a></label>
                        <input type="checkbox" name="del_9" value="1"> Hapus File 9
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 10</label>
                        <input type="hidden" class="span4" name="file_10_old" value="<?=$file[9]?>">
                        <input type="file" class="span2" name="file_10" id="file_10">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[9]?>','_blank')"><?=$file[9]?></a></label>
                        <input type="checkbox" name="del_10" value="1"> Hapus File 10
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 11</label>
                        <input type="hidden" class="span4" name="file_11_old" value="<?=$file[10]?>">
                        <input type="file" class="span2" name="file_11" id="file_11">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[10]?>','_blank')"><?=$file[10]?></a></label>
                        <input type="checkbox" name="del_11" value="1"> Hapus File 11
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 12</label>
                        <input type="hidden" class="span4" name="file_12_old" value="<?=$file[11]?>">
                        <input type="file" class="span2" name="file_12" id="file_12">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[11]?>','_blank')"><?=$file[11]?></a></label>
                        <input type="checkbox" name="del_12" value="1"> Hapus File 12
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 13</label>
                        <input type="hidden" class="span4" name="file_13_old" value="<?=$file[12]?>">
                        <input type="file" class="span2" name="file_13" id="file_13">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[12]?>','_blank')"><?=$file[12]?></a></label>
                        <input type="checkbox" name="del_13" value="1"> Hapus File 13
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 14</label>
                        <input type="hidden" class="span4" name="file_14_old" value="<?=$file[13]?>">
                        <input type="file" class="span2" name="file_14" id="file_14">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[3]?>','_blank')"><?=$file[13]?></a></label>
                        <input type="checkbox" name="del_14" value="1"> Hapus File 14
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 15</label>
                        <input type="hidden" class="span4" name="file_15_old" value="<?=$file[14]?>">
                        <input type="file" class="span2" name="file_15" id="file_15">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[14]?>','_blank')"><?=$file[14]?></a></label>
                        <input type="checkbox" name="del_15" value="1"> Hapus File 15
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 16</label>
                        <input type="hidden" class="span4" name="file_16_old" value="<?=$file[15]?>">
                        <input type="file" class="span2" name="file_16" id="file_16">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[15]?>','_blank')"><?=$file[15]?></a></label>
                        <input type="checkbox" name="del_16" value="1"> Hapus File 16
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 17</label>
                        <input type="hidden" class="span4" name="file_17_old" value="<?=$file[16]?>">
                        <input type="file" class="span2" name="file_17" id="file_17">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[16]?>','_blank')"><?=$file[16]?></a></label>
                        <input type="checkbox" name="del_17" value="1"> Hapus File 17
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 18</label>
                        <input type="hidden" class="span4" name="file_18_old" value="<?=$file[17]?>">
                        <input type="file" class="span2" name="file_18" id="file_18">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[17]?>','_blank')"><?=$file[17]?></a></label>
                        <input type="checkbox" name="del_18" value="1"> Hapus File 18
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 19</label>
                        <input type="hidden" class="span4" name="file_19_old" value="<?=$file[18]?>">
                        <input type="file" class="span2" name="file_19" id="file_19">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[18]?>','_blank')"><?=$file[18]?></a></label>
                        <input type="checkbox" name="del_19" value="1"> Hapus File 19
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span1">File 20</label>
                        <input type="hidden" class="span4" name="file_20_old" value="<?=$file[19]?>">
                        <input type="file" class="span2" name="file_20" id="file_20">
                        <label class="span3"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Pengawasan").$file[19]?>','_blank')"><?=$file[19]?></a></label>
                        <input type="checkbox" name="del_20" value="1"> Hapus File 20
                    </fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['assign_id']?>">
                    <?
                    break;
            }
            ?>
            <fieldset>
                <center>
                    <?
                    if($_action != "getajukan_penugasan" && $_action != "getapprove_penugasan"){
                        ?>
                        <input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
                        <?
                    }
                    if($_action != "getdetail"){
                        ?>
                        <input type="submit" class="blue_btn" value="Simpan">
                        <?php
                    }
                    ?>
                </center>
                <input type="hidden" name="data_action" id="data_action"
                       value="<?=$_nextaction?>">
            </fieldset>
        </form>
    </article>
    <script>
	    var data_anggota = [];
        <?php
        $rsAnggota = $auditors->auditor_data_combo_viewlist ();
        $i = 0;
        foreach ( $rsAnggota->GetArray () as $row ) :
        ?>
        data_anggota[<?php echo $i?>] = {"id":'<?php echo $row['auditor_id']?>', "text":'<?=str_replace("'"," ",$row['auditor_name'])?>'};
        <?php
        $i ++;
        endforeach
        ;
        ?>
		
		$(document).ready(function() {
            $(".anggota_id").select2({
                multiple: false,
                width:'317px',
                data: data_anggota
            });
        });
		
		var data = [];
        <?php
        $rsAuditee = $auditees->auditee_data_viewlist ();
        $i = 0;
        foreach ( $rsAuditee->GetArray () as $row ) :
        ?>
        data[<?php echo $i?>] = {"id":'<?php echo $row['auditee_id']?>', "text":'<?=str_replace("'"," ",$row['auditee_name'])?>'};
        <?php
        $i ++;
        endforeach
        ;
        ?>

        $(document).ready(function() {
            $("#auditee").select2({
                placeholder:"Ketikkan kode atau nama Obyek Pemeriksaan",
                tokenSeparators: [",", " "],
                multiple: true,
                width:'317px',
                data: data
            });
        });
		
		$("#tahun").datepicker({
            dateFormat: 'yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });

        $("#tanggal_awal, #tahun").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });
        $("#tanggal_akhir").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });
        $("#tanggal_surat").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });

        $("#tanggal_persiapan_awal").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });
        $("#tanggal_persiapan_akhir").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });

        $("#tanggal_pelaksanaan_awal").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });
        $("#tanggal_pelaksanaan_akhir").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });

        $("#tanggal_pelaporan_awal").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });
        $("#tanggal_pelaporan_akhir").datepicker({
            dateFormat: 'dd-mm-yy',
            nextText: "",
            prevText: "",
            changeYear: true,
            changeMonth: true
        });
        $(function() {
            $("#validation-form").validate({
                rules: {
                    auditee: "required",
					tanggal_awal: "required",
                    tanggal_akhir: "required",
					no_surat: "required",
					tanggal_surat: "required"
                },
                messages: {
                    auditee: "Silahkan Pilih Auditi",
					tanggal_awal: "Silahkan Pilih Tanggal Awal",
                    tanggal_akhir: "Silahkan Pilih Akhir Audit",
					no_surat: "Silahkan Masukkan Nomor Surat",
					tanggal_surat: "Silahkan Pilih Tanggal Surat"
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
		
		$('#tambah_personil').on('click', function(e){
			e.preventDefault();
			var no = $('#table_personil tr:last').attr('id');
			var select_struktur = '<input type="hidden" name="anggota_id[]" id="anggota_id" class="select2 anggota_id"/>';
			var select_jabatan = $('#table_personil tbody tr:last td:eq(2)').clone().html();
			no++;
			var new_row = '<tr id="'+no+'">'
				+ '<td align="center">'+no+'.</td>'
				+ '<td>'+select_struktur+'</td>'
				+ '<td>'+select_jabatan+'</td>'
				+ '<td><button class="btn personil_remove">-</button></td>'
				+ '</tr>';
			$('#table_personil tbody').append(new_row);
			$(".anggota_id").select2({
                multiple: false,
                width:'317px',
                data: data_anggota
            });
		});

		$("#table_personil").on("click", ".personil_remove", function(e){
			e.preventDefault();
			$(this).parents("tr").remove();
		});
		
		$("#tanggal_awal").datepicker("option", "onSelect", function (dateText, inst) {
			var date1 = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);

			var date2 = new Date(date1.getTime());
			date2.setDate(date2.getDate() + 1);
			$("#tahun").datepicker("setDate", date2);
        });
</script>