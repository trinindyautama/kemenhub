<?php
include_once "../_includes/login_history.php";
include_once "../_includes/classes/auditee_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/classes/finding_class.php";
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/classes/param_class.php";
include_once "../_includes/common.php";

$assigns = new assign($ses_userId);
$findings = new finding($ses_userId);
$comfunc = new comfunction($ses_userId);
$programaudits = new programaudit($ses_userId);
$params = new param($ses_userId);
$rs_auditee = $assigns->auditee_detil ( $_REQUEST['id_auditee'] );
$arr_auditee = $rs_auditee->FetchRow();
$assign_id_auditee =$arr_auditee['auditee_id'];
$assign_nama_auditee = $arr_auditee['auditee_name'];

header("Content-Type: application/msword");
header("Content-Type: image/jpg");
header('Content-Disposition: attachment; filename=LAPORAN HASIL AUDIT '.str_replace(',', '', $assign_nama_auditee).' .doc');
header("Pragma: no-cache");
header("Expires: 0");
define ("INBOARD",false);

$assign_id = $_REQUEST['id_assign'];
$rs = $assigns->assign_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr = $rs->FetchRow();
// lha
$rs_lha = $assigns->assign_lha_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_lha = $rs_lha->FetchRow ();
// spl
$rs_spl = $assigns->assign_spl_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_spl = $rs_spl->FetchRow ();
// get value finding
$rs_finding = $findings->finding_tl_nha_count ( $_REQUEST['id_assign'], "", "", "");
// temuan dan rekomendasi
$rs_spl_temuan_rekomendasi = $assigns->assign_spl_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_spl_temuan_rekomendasi = $rs_spl_temuan_rekomendasi->FetchRow ();
// temuan dan rekomendasi
$cek_posisi = $findings->cek_posisi($_REQUEST['id_assign']);

$rs_auditee_viewlist = $assigns->assign_auditee_get_inspektorat_viewlist($_REQUEST['id_assign'], $assign_id_auditee);
$arr_auditee_viewlist = $rs_auditee_viewlist->FetchRow();
?>
<style>

 body
{
    -webkit-font-smoothing:antialiased;
    font:normal .8764em/1.5em Arial,Verdana,sans-serif;
    margin:0
}

html>body
{
    font-size:13px
}

li
{
    font-size:110%
}

li li
{
    font-size:100%
}

</style>
<table align="center" width="100%">
                        <tr>
                            <td align="center"><br><br><br><strong>LAPORAN HASIL AUDIT<br>PADA<br><?=$assign_nama_auditee?><br>PROVINSI <?=$arr_lha['propinsi_name']?></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><br><br><br><strong>Nomor : <?=$arr_lha['assign_no_lha']?></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>Tanggal :  <?=$comfunc->dateIndo2($arr_lha['assign_date_lha'])?></strong></td>
                        </tr>
                    </table>
                    <br>
                    <table align="center" width="100%" border="0">
                        <tr>
                            <td align="center" colspan="4"><br><br><br><strong>DAFTAR ISI</strong></td>
                        </tr>
                        <br>
                        <tr>
                            <td width="20%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>BAB I</strong></td>
                            <td align="left" width="55%"><br><br><br><strong>:&nbsp;&nbsp;&nbsp;Pendahuluan</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;&nbsp;&nbsp;&nbsp;1. Dasar Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;2. Tujuan Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;3. Ruang Lingkup Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;4. Pendekatan Audit<br>&nbsp;&nbsp;&nbsp;&nbsp;5. Data Umum<br>&nbsp;&nbsp;&nbsp;&nbsp;6. Status Tindak Lanjut Temuan Hasil Audit Sebelumnya</td>
                        </tr>
                        </td>
                        </tr>
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>BAB II</strong></td>
                            <td align="left" width="55%"><br><br><br><strong>:&nbsp;&nbsp;&nbsp;Hasil Audit</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;&nbsp;&nbsp;&nbsp;1. Temuan Ketidakpatuhan Terhadap Peraturan<br>&nbsp;&nbsp;&nbsp;&nbsp;2. Temuan Kelemahan Sistem Pengendalian Intern<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Kelemahan Sistem Pengendalian Akuntansi Dan Pelaporan<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Kelemahan Sistem Pengendalian Pelaksanaan Anggaran 16
                                Pendapatan Dan Belanja<br>&nbsp;&nbsp;&nbsp;&nbsp;3. Temuan 3E</td>
                        </tr>
                        </td>
                        </tr>
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>BAB III</strong></td>
                            <td align="left" width="55%"><br><br><br><strong>:&nbsp;&nbsp;&nbsp;Penutup</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;</td>
                        </tr>
                        </td>
                        </tr>
                        <tr>
                            <td width="10%">&nbsp;</td>
                            <td align="left" width="20%"><br><br><br><strong>LAMPIRAN</strong><br><br><br>1. Berita Acara Pemeriksaan Kas;<br>2. Register Penutupan Kas;<br>3. Surat Tugas (ST).</td>
                            <td align="left" width="55%"><br><br><br><strong>&nbsp;&nbsp;&nbsp;</strong>
                                <br>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td align="left"><br><br><br><strong></strong></td>
                            <td align="left" width="70%">&nbsp;</td>
                        </tr>
                        </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <fieldset>
                        <table align="center" width="100%">
                            <tr>
                                <td align="justify"><?=$comfunc->text_show($arr_lha['lha_ringkasan'])?></td>
                            </tr>
                        </table>
                    </fieldset>
                    <br>
                    <fieldset>
                        <table align="center" width="100%">
                            <tr>
                                <td align="center" colspan="4"><strong>BAGIAN KEDUA</strong></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4"><strong>URAIAN HASIL AUDIT</strong><br></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4"><strong>BAB I</strong></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4"><strong>PENDAHULUAN</strong></td>
                            </tr>
                            <tr>
                                <td width="2%">1.</td>
                                <td colspan="2">Dasar Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_dasar_audit'])?></td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td colspan="2">Tujuan Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_tujuan_audit'])?></td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td colspan="2">Ruang Lingkup Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_ruanglingkup'])?></td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td colspan="2">Pendekatan Audit</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_pendekatan'])?></td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td colspan="2">Data Umum</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify">
                                    <table align="left" width="100%" border="0">
                                        <tr>
                                            <td width="15%">a. Nama Auditi</td>
                                            <td align="left" width="55%">:&nbsp;&nbsp;&nbsp;<?=$assign_nama_auditee?></td>
                                        </tr>
                                        <tr>
                                            <td>b. Alamat</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_alamat'])?></td>
                                        </tr>
                                        <tr>
                                            <td>c. Nomor Telepon</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_telp'])?>,&nbsp;<?=$comfunc->text_show($arr_lha['auditee_fax'])?>&nbsp;(Fax)</td>
                                        </tr>
                                        <tr>
                                            <td>d. Kode Kantor</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_kode'])?></td>
                                        </tr>
                                        <tr>
                                            <td>e. Sumber Dana dan Pagu</td>
                                            <td align="left"></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <?php
                                    $no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
                                    ?>
                                    <table border="0" width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                        <tr>
                                            <td width="20%"><?=$no_pagu;?>)&nbsp;Tahun&nbsp;<?=$arr_pagu['lha_pagu_tahun']?></td>
                                            <td>:&nbsp;&nbsp;DIPA No : <?=$arr_pagu['lha_pagu_no_dipa']?>, Tanggal <?=$comfunc->dateIndo($arr_pagu['lha_pagu_date'])?></td>
                                        <tr>
                                        <tr>
                                            <td></td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_nilai']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 13px; margin:0px;">a) Belanja Pegawai</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_pegawai']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 13px; margin:0px;">b) Belanja Barang</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_barang']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 13px; margin:0px;">c) Belanja Modal</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_modal']?>,-</td>
                                        <tr>
                                        <tr>
                                            <td style="padding:0px 0px 0px 25px; margin:0px;">Realisasi Keuangan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pagu['lha_pagu_realisasi']?>,-&nbsp;(<?=$arr_pagu['lha_pagu_persentase']?>)</td>
                                        <tr>
                                    </table>
                                            <?
                                        }
                                    ?>
                                    <br>
                                    <table align="left" width="100%" border="0">
                                        <tr>
                                            <td width="15%">f. Pejabat Audit</td>
                                            <td align="left" width="56%">:&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                    <br>
									<?php
									$no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
											$no_pic=0;
											$rs_pic = $params->pic_viewlist_lha($arr_pagu['lha_pagu_id']);
											while($arr_pic = $rs_pic->FetchRow()){
												$no_pic++;
												if ($arr_pic['pic_jabatan_id'] == '80897dc5580beac2752b3da6c0b5b86581205588') {
										?>
									<table border="0" width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                            <tr>
                                                <td width="20%">-&nbsp;<?=$arr_pic['jabatan_pic_name']?></td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">a. Nama</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_name']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">b. NIP</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_nip']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">c. Pangkat</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pangkat_lengkap']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">d. Jabatan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_jabatan_struktur_id']?></td>
                                            <tr>
                                    </table>
									<?
									} else {
									?>
                                    
                                        <table border="0" width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                            <tr>
                                                <td width="20%">-&nbsp;<?=$arr_pic['jabatan_pic_name']?></td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">a. Nama</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_name']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">b. NIP</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_nip']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">c. Pangkat</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pangkat_lengkap']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">d. Nomor & Tanggal SK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_pic['pic_no_sk']?>&nbsp;Tahun&nbsp;<?=$arr_pic['pic_tahun']?>&nbsp;tanggal&nbsp;<?= $comfunc->dateIndoLengkap($arr_pic['pic_tgl_sk'])?></td>
                                            <tr>
                                        </table>
                                        <?
												}
											}
										}	
                                    ?>
                                    <table align="left" width="100%" border="0">
                                        <tr>
                                            <td width="15%">g. KPPN</td>
                                            <td align="left" width="55%">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_kppn'])?></td>
                                        </tr>
                                        <tr>
                                            <td>h. Bank dan Nomor Rekening</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_bank'])?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0px 0px 82px 0px; 0px;">i. Fasilitas Utama</td>
                                            <td align="left"><?=$comfunc->text_show($arr_lha['auditee_fasilitas'])?></td>
                                        </tr>
										<tr>
                                            <td>j. Jumlah Pegawai</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_jml_pegawai'])?> Orang</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">k. Jenis Kegiatan Belanja Modal dan Belanja Barang/Jasa Tidak Mengikat T.A :</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <?php
                                    $no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
                                            $no_damu=0;
                                            $rs_damu = $assigns->assign_damu_view_lha($arr_pagu['lha_pagu_id']);
                                            while($arr_damu = $rs_damu->FetchRow()){
                                                $no_damu++;
                                        ?>
                                        <table align="left" width="100%" border="0" style="padding:0px 0px 0px 9px; margin:0px;">
                                            <tr>
                                                <td width="15%"><u>Tahun&nbsp;<?=$arr_pagu['lha_pagu_tahun']?></u></td>
                                                <td align="left" width="55%">&nbsp;&nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                        <table border="0" width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                            <tr>
                                                <td width="20%"><?=$no_damu?>)&nbsp;Pekerjaan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_pekerjaan']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nomor Kontrak/SPK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_no_kontrak']?>&nbsp;tanggal&nbsp;<?= $comfunc->dateIndoLengkap($arr_damu['lha_damu_date'])?></td>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;"Nilai Kontrak/SPK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;Rp.&nbsp;<?= $comfunc->format_uang($arr_damu['lha_damu_nilai'])?>,-</td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nama Penyedia barang/Jasa</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_nama']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Jangka Waktu Pelaksanaan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_jangka']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Realisasi Fisik (%)</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_fisik']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Realisasi Keuangan (%)</td>
                                                <td>:&nbsp;&nbsp;&nbsp;Rp.&nbsp;<?= $comfunc->format_uang($arr_damu['lha_damu_keuangan'])?>,-&nbsp;(<?=$arr_damu['lha_damu_persentase']?>%)</td>
                                            <tr>
                                        </table>
                                        <?
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                                    <table align="left" width="100%" border="0">
                                        <tr>
                                            <td width="15%">g. KPPN</td>
                                            <td align="left" width="55%">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_kppn'])?></td>
                                        </tr>
                                        <tr>
                                            <td>h. Bank dan Nomor Rekening</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_bank'])?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0px 0px 82px 0px; 0px;">i. Fasilitas Utama</td>
                                            <td align="left"><?=$comfunc->text_show($arr_lha['auditee_fasilitas'])?></td>
                                        </tr>
                                        <tr>
                                            <td>j. Jumlah Pegawai</td>
                                            <td align="left">:&nbsp;&nbsp;&nbsp;<?=$comfunc->text_show($arr_lha['auditee_jml_pegawai'])?> Orang</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">k. Jenis Kegiatan Belanja Modal dan Belanja Barang/Jasa Tidak Mengikat T.A :</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <?php
                                    $no_pagu=0;
                                    $rs_pagu = $assigns->assign_pagu_view_lha($assign_id, $assign_id_auditee);
                                        while($arr_pagu = $rs_pagu->FetchRow()){
                                            $no_pagu++;
                                            $no_damu=0;
                                            $rs_damu = $assigns->assign_damu_view_lha($arr_pagu['lha_pagu_id']);
                                            while($arr_damu = $rs_damu->FetchRow()){
                                                $no_damu++;
                                        ?>
                                        <table align="left" width="100%" border="0" style="padding:0px 0px 0px 9px; margin:0px;">
                                            <tr>
                                                <td width="15%"><u>Tahun&nbsp;<?=$arr_pagu['lha_pagu_tahun']?></u></td>
                                                <td align="left" width="55%">&nbsp;&nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                        <table border="0" width="100%" align="left" style="margin:0px 0px 0px 12px;">
                                            <tr>
                                                <td width="20%"><?=$no_damu?>)&nbsp;Pekerjaan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_pekerjaan']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nilai Kontrak/SPK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_no_kontrak']?>&nbsp;tanggal&nbsp;<?= $comfunc->dateIndoLengkap($arr_damu['lha_damu_date'])?></td>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;"Nilai Kontrak/SPK</td>
                                                <td>:&nbsp;&nbsp;&nbsp;Rp.&nbsp;<?= $comfunc->format_uang($arr_damu['lha_damu_nilai'])?>,-</td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Nama Penyedia barang/Jasa</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_nama']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Jangka Waktu Pelaksanaan</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_jangka']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Realisasi Fisik (%)</td>
                                                <td>:&nbsp;&nbsp;&nbsp;<?=$arr_damu['lha_damu_fisik']?></td>
                                            <tr>
                                            <tr>
                                                <td style="padding:0px 0px 0px 13px; margin:0px;">Realisasi Keuangan (%)</td>
                                                <td>:&nbsp;&nbsp;&nbsp;Rp.&nbsp;<?= $comfunc->format_uang($arr_damu['lha_damu_keuangan'])?>,-&nbsp;(<?=$arr_damu['lha_damu_persentase']?>%)</td>
                                            <tr>
                                        </table>
										<br>
                                        <?
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td colspan="2">Tindak Lanjut Temuan Hasil Audit Yang Lalu</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" align="justify"><?=$comfunc->text_show($arr_lha['lha_tl_audit_lalu'])?></td>
                            </tr>
                        </table>
                    </fieldset>
                    <br>
                    <fieldset>
    <table align="center" width="100%">
        <tr>
            <td colspan="5" align="center"><strong>BAB II</strong></td>
        </tr>
        <tr>
            <td colspan="5" align="center"><strong>HASIL AUDIT</strong></td>
        </tr>
        <tr>
            <td colspan="5" align="justify"><?=$comfunc->text_show($arr_lha['lha_hasil'])?></td>
        </tr>
        <tr>
            <td width="2%"><b>1.</b></td>
            <td colspan="4"><b>Temuan Ketidakpatuhan Terhadap Peraturan</b></td>
        </tr>
        <?
        // Temuan Ketidakpatuhan Terhadap Peraturan
        $rs_temuan_proses = $findings->get_temuan_proses_1 ( $arr_spl['assign_id']);
        $no_tem = "a";
        while($arr_temuan_proses = $rs_temuan_proses->FetchRow()) {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td width="2%"><b><?= $no_tem ?>.</b></td>
                <td colspan="3"><strong><?= $arr_temuan_proses['finding_judul'] ?></strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>1) Uraian </strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_kondisi']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>2) Sebab & Akibat :</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_sebab']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>3) Tanggapan Auditan :</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_tanggapan_auditee']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>4) Rekomendasi :</strong></td>
            </tr>
            <?
            $rs_rekomendasi_proses = $findings->get_rekomendasi_proses($arr_temuan_proses['finding_id']);
            $count_rek = $rs_rekomendasi_proses->RecordCount();
            $no_rek = 0;
            while ($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()) {
                $no_rek++;
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc']) ?></td>
                </tr>
                <?
                $no_tem++;
            }
        }
        ?>
        <?
        $rs_temuan_proses2 = $findings->get_temuan_proses_1 ( $arr_spl['assign_id']);
        $arr_temuan_proses2 = $rs_temuan_proses2->FetchRow();
        if ($arr_temuan_proses2['finding_id'] == "") {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><?= $comfunc->text_show("Setelah dilakukan audit terhadap Aspek Temuan Ketidakpatuhan Terhadap Peraturan tidak terdapat permasalahan yang perlu diangkat sebagai temuan hasil audit") ?></td>
            </tr>
            <?
        }
        ?>
        <br>
        <br>
        <br>
        <tr>
            <td width="2%"><b>2.</b></td>
            <td colspan="4"><b>Temuan Kelemahan Sistem Pengendalian Intern</b></td>
        </tr>
        <?
        // Temuan Kelemahan Sistem Pengendalian Internal
        $rs_temuan_proses = $findings->get_temuan_proses_2 ( $arr_spl['assign_id']);
        $no_tem = "a";
        while($arr_temuan_proses = $rs_temuan_proses->FetchRow()) {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td width="2%"><b><?= $no_tem ?>.</b></td>
                <td colspan="3"><strong><?= $arr_temuan_proses['finding_judul'] ?></strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>1) Uraian </strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_kondisi']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>2) Sebab & Akibat :</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_sebab']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>3) Tanggapan Auditan :</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_tanggapan_auditee']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>4) Rekomendasi :</strong></td>
            </tr>
            <?
            $rs_rekomendasi_proses = $findings->get_rekomendasi_proses($arr_temuan_proses['finding_id']);
            $count_rek = $rs_rekomendasi_proses->RecordCount();
            $no_rek = 0;
            while ($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()) {
                $no_rek++;
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc']) ?></td>
                </tr>
                <?
                $no_tem++;
            }
        }
        ?>
        <?
        $rs_temuan_proses2 = $findings->get_temuan_proses_2 ( $arr_spl['assign_id']);
        $arr_temuan_proses2 = $rs_temuan_proses2->FetchRow();
        if ($arr_temuan_proses2['finding_id'] == "") {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><?= $comfunc->text_show("Setelah dilakukan audit terhadap Aspek Temuan Kelemahan Sistem Pengendalian Intern tidak terdapat permasalahan yang perlu diangkat sebagai temuan hasil audit") ?></td>
            </tr>
            <?
        }
        ?>
        <br>
        <br>
        <br>
        <tr>
            <td width="2%"><b>3.</b></td>
            <td colspan="4"><b>Temuan 3E (Ketidakekonomisan, Ketidakefisienan, Ketidakefektifan)</b></td>
        </tr>
        <?
        // Temuan 3E
        $rs_temuan_proses = $findings->get_temuan_proses_3 ( $arr_spl['assign_id']);
        $no_tem = "a";
        while($arr_temuan_proses = $rs_temuan_proses->FetchRow()) {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td width="2%"><b><?= $no_tem ?>.</b></td>
                <td colspan="3"><strong><?= $arr_temuan_proses['finding_judul'] ?></strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>1) Uraian </strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_kondisi']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>2) Sebab & Akibat :</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_sebab']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>3) Tanggapan Auditan :</strong></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"><?= $comfunc->text_show($arr_temuan_proses['finding_tanggapan_auditee']) ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>4) Rekomendasi :</strong></td>
            </tr>
            <?
            $rs_rekomendasi_proses = $findings->get_rekomendasi_proses($arr_temuan_proses['finding_id']);
            $count_rek = $rs_rekomendasi_proses->RecordCount();
            $no_rek = 0;
            while ($arr_rekomendasi_proses = $rs_rekomendasi_proses->FetchRow()) {
                $no_rek++;
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2"><?= $comfunc->text_show($arr_rekomendasi_proses['rekomendasi_desc']) ?></td>
                </tr>
                <?
                $no_tem++;
            }
        }
        ?>
        <?
        $rs_temuan_proses2 = $findings->get_temuan_proses_3 ( $arr_spl['assign_id']);
        $arr_temuan_proses2 = $rs_temuan_proses2->FetchRow();
        if ($arr_temuan_proses2['finding_id'] == "") {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><?= $comfunc->text_show("Setelah dilakukan audit terhadap Aspek Temuan 3E (Ketidakekonomisan, Ketidakefisienan, Ketidakefektifan) tidak terdapat permasalahan yang perlu diangkat sebagai temuan hasil audit") ?></td>
            </tr>
            <?
        }
        ?>

    </table>
</fieldset>
                    <br>
                    <fieldset>
                        <table align="center" width="100%">
                            <tr>
                                <td colspan="3" align="center"><strong>BAB III</strong></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center"><strong>PENUTUP</strong></td>
                            </tr>
                            <tr>
                                <td colspan="5" align="justify"><?=$comfunc->text_show($arr_lha['lha_penutup'])?></td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <table border="0" align="center" width="75%">
                            <tr>
                                <td colspan="3" align="right">Jakarta, <?=$comfunc->dateIndoLengkap($arr_lha['lha_date'])?></td>
                            </tr>
                        </table>
                    </fieldset>
					<br>
                    <fieldset>
                        <table border="0" align="center" width="100%">
                            <tbody>
                            <tr>
                                <?
                                // get pengendali mutu
                                $rs_pm = $assigns->view_auditor_pm_lha($_REQUEST['id_assign'], $assign_id_auditee);
                                $arr_pm = $rs_pm->FetchRow();
                                ?>
                                <td width="30%" align="center" style="text-align: center; margin-top: 0px; margin-bottom: 0px; padding-bottom: 402px;">
                                Mengetahui<br>
                                Pengendali Mutu<br><br><br><br><br><br>

                                <u><?=$arr_pm ['auditor_name']?></u><br>
                                <?=$arr_pm ['pangkat_desc']?> (<?=$arr_pm['pangkat_name']?>)<br>
                                NIP. <?=$arr_pm ['auditor_nip']?>
                                </td>

                                <td width="80%" align="center">
                                    <table border="0" width="60%" align="right">
                                        <thead>
                                        <tr>
                                            <td width="30%" align="center" style="height:146px;">Tim Audit</td>
                                            <td width="30%">&nbsp;</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no_auditor="";
                                        $rs_auditor = $assigns->view_auditor_lha ( $_REQUEST['id_assign'], $assign_id_auditee);
                                        while ( $arr_audtor = $rs_auditor->FetchRow () ) {
                                            $no_auditor++;
                                            ?>
                                            <tr class="row_item">
                                                <td>
                                                <?=$no_auditor?>.&nbsp;<u><?=$arr_audtor ['auditor_name']?></u><br>
                                                    <?=$arr_audtor ['pangkat_desc']?> (<?=$arr_audtor['pangkat_name']?>)<br>
                                                    NIP. <?=$arr_audtor ['auditor_nip']?>
                                                    <br><br><br><br><br><br>

                                                </td>
                                                <td><?=$arr_audtor ['posisi_name']?><br><br><br><br><br><br><br><br></td>
                                            </tr>
                                            <?
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
</table>					