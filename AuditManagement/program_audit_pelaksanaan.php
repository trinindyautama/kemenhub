<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.loadTemplate-1.4.1.min.js"></script>
<?php
include_once "_includes/classes/report_class.php";

$reports = new report ($ses_userId);

$rs_auditee = $assigns->assign_auditee_viewlist($ses_assign_id, $auditee_id);
$arr_auditee = $rs_auditee->FetchRow();
$katim = $reports->get_anggota($ses_assign_id, $auditee_id, 'KT');
$dalnis = $reports->get_anggota($ses_assign_id, $auditee_id, 'PT');
$dalmut = $reports->get_anggota($ses_assign_id, $auditee_id, 'PM');
?>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <table border='0' class="table_report" cellspacing='0' cellpadding="0">
            <tr>
                <td colspan="8">
                    <center>
                        <img src="images/kka-logo1.png" width="55" height="48" style="width:55pt;height:48pt;"/><br>
                        INSPEKTORAT KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA
                    </center>
                    <br>
                </td>
            </tr>
            <tr>
                <td width="5%" style="border-top:1px solid">&nbsp;</td>
                <td width="5%" style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td width="10%" style="border-top:1px solid">No PKA</td>
                <td align="center" style="border-top:1px solid">:</td>
                <td style="border-top:1px solid" id="no_pka"></td>
                <td style="border-top:1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Nama Auditan</td>
                <td align="center">:</td>
                <td><?= $arr_auditee['auditee_name']; ?></td>
                <td>Ref. PKA No</td>
                <td align="center">:</td>
                <td>-</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Sasaran Audit</td>
                <td align="center">:</td>
                <td><?= $arr['audit_type_name']; ?></td>
                <td>Disusun Oleh</td>
                <td align="center">:</td>
                <td><?= $katim['nama'] ?></td>
                <td>Paraf :</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Tanggal</td>
                <td align="center">:</td>
                <td><span id="pka_intro_arranged_date"></span></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Periode Audit</td>
                <td align="center">:</td>
                <td>TA.&nbsp;<?= $arr['assign_tahun']-1 ?>-<?= $arr['assign_tahun'] ?></td>
                <td>Direviu Oleh</td>
                <td align="center">:</td>
                <td><?= $dalnis['nama'] ?></td>
                <td>Paraf :</td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">Tanggal</td>
                <td style="border-bottom:1px solid" align="center">:</td>
                <td style="border-bottom:1px solid"><span id="pka_intro_reviewed_date"></span></td>
                <td style="border-bottom:1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8" align="center"><br><b>PROGRAM KERJA AUDIT PELAKSANAAN</b></td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8"><strong>Tujuan Audit : </strong><label id="tujuan"></label></td>
                <!--                <td colspan="7"></td>-->
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid"></td>
                <td style="border-bottom:1px solid"></td>
                <td style="border-bottom:1px solid">-</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
            </tr>
            <td colspan="6" valign="top"><strong>A. Langkah Kerja:</strong>
                <?
                $rs_aspek = $programaudits->get_assign_aspek("", $ses_assign_id, $auditee_id, "PKA Pelaksanaan");
                $arr_aspek = $rs_aspek->GetArray();
                echo $comfunc->buildCombo("fil_aspek", $arr_aspek, 0, 1, "", "", "", false, true, false);
                ?>
            </td>
            </tr>
        </table>
        <table border='1' class="table_risk" cellspacing='0' cellpadding="0">
            <tr align="center">
                <th width="2%" rowspan="2">No.</th>
                <th width="50%" rowspan="2">Langkah Kerja Audit</th>
                <th width="16%" colspan="2">Dilaksanakan Oleh</th>
                <th width="16%" colspan="2">Waktu yang Diperlukan</th>
                <th width="9%" rowspan="2">Nomor KKA</th>
                <th width="7%" rowspan="2">Catatan</th>
            </tr>
            <tr align="center">
                <th>Rencana</th>
                <th>Realisasi</th>
                <th>Rencana</th>
                <th>Realisasi</th>
            </tr>
            <tr id="table_aspek">
                <td align="center"><b><label id="no"></label></b></td>
                <td><b><label id="aspek"></label></b></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tbody id="table_rinci">
            </tbody>
        </table>
        <table border='0' cellspacing='0' cellpadding="0" width="100%">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="border-style: none; border-bottom-style: none" colspan="4">&nbsp;&nbsp;<b>Keterangan : </b><span id="keterangan"></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Jakarta, <span id="pka_intro_arranged_date1"></span></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="30%">Disetujui</td>
                <td width="30%">Direviu</td>
                <td width="30%">Disusun oleh:</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>tanggal: <span id="pka_intro_reviewed_date1"></span></td>
                <td>tanggal: <span id="pka_intro_approved_date"></span></td>
                <td>KETUA TIM,</td>
            </tr>
            <tr>
                <td></td>
                <td>PENGENDALI MUTU,</td>
                <td>PENGENDALI TEKNIS,</td>
                <td></td>
            </tr>
            <? foreach (range(1, 5) as $item): ?>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            <? endforeach; ?>
            <tr>
                <td></td>
                <td><u><?= $dalmut['nama'] ?></u><br><?= $dalmut['jabatan'] ?></td>
                <td><u><?= $dalnis['nama'] ?></u><br><?= $dalnis['jabatan'] ?></td>
                <td><u><?= $katim['nama'] ?></u><br><?= $katim['jabatan'] ?></td>
            </tr>
        </table>
        <br><br><br>
        <fieldset>
            <center>
                <input type="hidden" id="id_assign" value="<?= $ses_assign_id ?>">
                <input type="hidden" id="id_auditee" value="<?= $auditee_id ?>">
                <input type="hidden" id="auditee_kode" value="<?= $arr_auditee['auditee_kode'] ?>">
                <input type="hidden" id="audit_type" value="<?= $arr['audit_type_name'] ?>">
                <input type="button" class="blue_btn" value="Kembali" onclick="location='<?= $def_page_request ?>'">
                <input type="button" class="blue_btn" value="ms-word"
                       onclick="window.open('AuditManagement/program_audit_pelaksanaan_word.php?id_assign=<?= $ses_assign_id ?>&id_auditee=<?= $auditee_id ?>&fil_aspek='+document.getElementById('fil_aspek').value,'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
            </center>
        </fieldset>
    </article>
</section>
<script>
    $("#table_aspek").hide();
    $("#fil_aspek").on("change", function () {
        if ($(this).val() != "") {
            var id_aspek = $(this).val(),
            id_assign = $("#id_assign").val();
            audit_type = $("#audit_type").val();
            auditee_kode = $("#auditee_kode").val();
            $.ajax({
                url: 'AuditManagement/ajax.php?data_action=get_introProgPelaksanaan',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_aspek: id_aspek, 
                    id_assign: id_assign,
                    audit_type: audit_type,
                    auditee_kode: auditee_kode
                },
                success: function (data) {
                    $("#table_aspek").show();
                    $("#no").text("I");
                    $("#aspek").text(data.nama_aspek);
                    $("#tujuan").text((data.tujuan != null) ? data.tujuan : "Tidak ada");
                    $("#keterangan").text((data.keterangan != null) ? data.keterangan : "Tidak ada");
                    $("#no_pka").text((data.no_pka != null) ? data.no_pka : "Tidak ada");
                    $("#pka_intro_arranged_date").text(data.pka_intro_arranged_date);
                    $("#pka_intro_reviewed_date").text(data.pka_intro_reviewed_date);
                    $("#pka_intro_approved_date").text(data.pka_intro_approved_date);
                    $("#pka_intro_arranged_date1").text(data.pka_intro_arranged_date);
                    $("#pka_intro_reviewed_date1").text(data.pka_intro_reviewed_date);
                    $("#table_rinci").show();        
                    getDataAspek();
                }
            });
        } else {
            $("#table_aspek").hide();
            $("#no").text("");
            $("#aspek").text("");
            $("#tujuan").text("");
            $("#keterangan").text("");
            $("#no_pka").text("");
            $("#pka_intro_arranged_date").text("");
            $("#pka_intro_reviewed_date").text("");
            $("#pka_intro_approved_date").text("");
            $("#pka_intro_arranged_date1").text("");
            $("#pka_intro_reviewed_date1").text("");
            $("#table_rinci").hide();
        }
    });

    function getDataAspek() {
        var id_aspek = $("#fil_aspek option:selected").val(),
            id_assign = $("#id_assign").val(),
            id_auditee = $("#id_auditee").val();
        no = 1;

        console.log('get aspek : ' + id_aspek + ", get assign : " + id_assign + ", get satker : " + id_auditee);
        $.ajax({
            url: 'AuditManagement/ajax.php?data_action=getAspek_pelaksanaan',
            type: 'POST',
            dataType: 'json',
            data: {id_aspek: id_aspek, id_assign: id_assign, id_auditee: id_auditee},
            success: function (data) {
                $("#table_rinci").empty();
                $.each(data.aspek_rinci, function (i, item) {
                    $("#table_rinci").loadTemplate("#table_rinci_tmpl",
                        {
                            title: item.title,
                            auditor: item.auditor,
                            jam: item.jam,
                            catatan: item.catatan,
                            no_kka: item.no_kka,
                            realisasi: item.realisasi
                        },
                        {
                            prepend: true
                        }
                    );
                });
            }
        });
    }
</script>

<script type="text/html" id="table_rinci_tmpl">
    <tr class="row_item">
        <td>&nbsp;</td>
        <td><span data-content="title"></span></td>
        <td><span data-content="auditor"></span></td>
        <td><span data-content="auditor"></span></td>
        <td><span data-content="jam"></span></td>
        <td><span data-content="realisasi"></span></td>
        <td><span data-content="no_kka"></span></td>
        <td><div style="overflow-x: auto; width: 500px">
            <span data-content="catatan"></span>
        </div></td>
    </tr>
</script>