<?
include_once "_includes/classes/auditor_class.php";
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/program_audit_class.php";
include_once "_includes/classes/finding_class.php";

$auditors = new auditor ( $ses_userId );
$auditees = new auditee ( $ses_userId );
$assigns = new assign ( $ses_userId );
$programaudits = new programaudit ( $ses_userId );
$findings = new finding ( $ses_userId );

@$id_assign_dash = $_REQUEST['id_assign_dash'];
// @$id_lha = $_REQUEST['id_lha'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=auditassign";
$acc_page_request = "audit_assign_acc.php";
$lha_page_request = "hasil_audit.php";
$list_page_request = "audit_view.php";

unset ( $_SESSION ['ses_kka_id'] );

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
    if (is_numeric ( $str_page ) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";
$grid = "grid_assign.php";
$gridHeader = array ("Obyek Pemeriksaan", "Tanggal Kegiatan", "No ST", "Tanggal ST");
$gridDetail = array ("0", "1", "5", "9");
$gridWidth = array ("20", "10", "10", "10");

$key_by = array ("No Surat Tugas");
$key_field = array ("assign_surat_no");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

switch ($_action) {
    case "assign_lha_damu" :
        $_SESSION ['ses_lha_id'] = $comfunc->replacetext ( $_POST ["data_id"] );
        echo $_SESSION['ses_lha_id'];
        ?>
        <script>window.open('main_page.php?method=lha_data_umum', '_self');</script>
        <?
        break;
    case "getajukan_penugasan" :
    case "getapprove_penugasan" :
        $_nextaction = "postkomentar";
        $page_request = $acc_page_request;
        $id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $status = $comfunc->replacetext ( $_REQUEST ["status_penugasan"] );

        $rs = $assigns->assign_viewlist ( $id );
        $page_title = "Reviu Penugasan";
        break;
    case "postkomentar" :
        $id_assign = $comfunc->replacetext ( $_POST ["data_id"] );
        $status = $comfunc->replacetext ( $_POST ["status_penugasan"] );
        $fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
        $notif_date = $comfunc->dateSql(date("d-m-Y"));
        $comfunc->hapus_notif($id_assign);
        $group_menu_id = "";
        if ($status != "") {
            if($status=='1') { //ajukan
                $group_menu_id = '106';
            }elseif ($status=='4'){//tolak
                $group_menu_id = '105';
            }
            if($group_menu_id!=""){
                $rs_user_accept = $comfunc->notif_user_all_bygroup($group_menu_id);
                while($arr_user_accept = $rs_user_accept->FetchRow()){
                    $comfunc->insert_notif($id_assign, $ses_userId, $arr_user_accept['user_id'], 'auditassign', "(Penugasan Audit) ".$fkomentar, $notif_date);
                }
            }
            $assigns->assign_update_status ( $id_assign, $status );
        }

        $ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
        if ($fkomentar != "") {
            $assigns->assign_add_komentar ( $id_assign, $fkomentar, $ftanggal );
            $comfunc->js_alert_act ( 3 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getsurattugas" :
        $_SESSION ['ses_assign_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        ?>
        <script>window.open('main_page.php?method=surattugas', '_self');</script>
        <?
        break;
    case "getlha" :
        $_nextaction = "postlha";
        $page_request = $lha_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $get_id = explode(":",$fdata_id );
        $assign_id = $get_id[0];
        $auditee_id = $get_id[1];

        $rs = $assigns->assign_viewlist ( $assign_id );
        $arr = $rs->FetchRow();

        $rs_auditee = $assigns->auditee_detil ( $auditee_id );
        $arr_auditee = $rs_auditee->FetchRow();

        $page_title = "Laporan Hasil Audit". $arr_auditee['auditee_name'];
        break;
    case "anggota_assign" :
        $_SESSION ['ses_assign_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        ?>
        <script>window.open('main_page.php?method=anggota_assign', '_self');</script>
        <?
        break;
    case "programaudit" :
        $_SESSION ['ses_assign_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        ?>
        <script>window.open('main_page.php?method=programaudit', '_self');</script>
        <?
        break;
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah Pelaksanaan Pengawasan";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assigns->assign_viewlist ( $fdata_id );
		$rs_surat = $assigns->surat_tugas_assignment_viewlist ( $fdata_id );
		$rs_anggota = $assigns->assign_auditor_assignment_viewlist ( $fdata_id );
        $page_title = "Ubah Pelaksanaan Pengawasan";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assigns->assign_viewlist ( $fdata_id );
		$rs_surat = $assigns->surat_tugas_assignment_viewlist ( $fdata_id );
		$rs_anggota = $assigns->assign_auditor_assignment_viewlist ( $fdata_id );
        $page_title = "Rincian Pelaksanaan Pengawasan";
        break;
    case "postlha" :
        $fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
        $flha_id = $comfunc->replacetext ( $_POST ["lha_id"] );
        $fno_lha = $comfunc->replacetext ( $_POST ["no_lha"] );
        $ftanggal_lha = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_lha"] ) );
        $fringkasan = $comfunc->replacetext ( $_POST ["ringkasan"] );
        $fhasil_audit_lalu = $comfunc->replacetext($_POST["hasil_audit_lalu"]);
        $fdasar_audit = $comfunc->replacetext ( $_POST ["dasar_audit"] );
        $ftujuan_audit = $comfunc->replacetext ( $_POST ["tujuan_audit"] );
        $fruang_lingkup = $comfunc->replacetext ( $_POST ["ruang_lingkup"] );
        $fbatasan_tanggung_jawab = $comfunc->replacetext ( $_POST ["batasan_tanggung_jawab"] );
        $fmetodologi_audit = $comfunc->replacetext ( $_POST ["metodologi_audit"] );
        $fstrategi_laporan = $comfunc->replacetext ( $_POST ["strategi_laporan"] );
        $fdata_umum_auditan = $comfunc->replacetext ( $_POST ["data_umum_auditan"] );
        $fhasil_yang_dicapai = $comfunc->replacetext ( $_POST ["hasil_yang_dicapai"] );
        $fpenutup = $comfunc->replacetext ( $_POST ["penutup"] );
        $fstatus_lha = $comfunc->replacetext ( $_POST ["status_lha"] );
        $flampiran = $_FILES ["attach"] ["name"];

        // spl
        $fspl_no = $comfunc->replacetext ( $_POST ["spl_no"] );
        $fspl_klasifikasi = $comfunc->replacetext ( $_POST ["spl_klasifikasi"] );
        $fspl_lampiran = $comfunc->replacetext ( $_POST ["spl_lampiran"] );
        $fspl_perihal = $comfunc->replacetext ( $_POST ["spl_perihal"] );
        $fspl_tempat_ttd = $comfunc->replacetext ( $_POST ["spl_tempat_ttd"] );
        $fspl_date = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["spl_date"] ) );
        $fspl_yth = $comfunc->replacetext ( $_POST ["spl_yth"] );
        $fspl_tembusan = $comfunc->replacetext ( $_POST ["spl_tembusan"] );
        $fspl_pj = $comfunc->replacetext ( $_POST ["spl_pj"] );
        $fspl_dasar = $comfunc->replacetext ( $_POST ["spl_dasar"] );
        $fspl_penutup = $comfunc->replacetext ( $_POST ["spl_penutup"] );

        // tambahan field lha
        $fpendekatan_audit = $comfunc->replacetext ( $_POST ["pendekatan_audit"] );


        $rs_file = $assigns->list_lha_lampiran($fdata_id);
        $z=0;
        while($arr_file = $rs_file->FetchRow()){
            $z++;
            $nama_file = $comfunc->replacetext ( @$_POST ["nama_file_".$z] );
            $assigns->delete_lampiran_kka ($fdata_id, $nama_file);
            $comfunc->HapusFile ( "Upload_KKA", $nama_file);
        }

        $jml_attach = count( $flampiran );
        if ($jml_attach <> 0) {
            for($i=0;$i<$jml_attach;$i++){
                if($flampiran[$i]!="") {
                    $comfunc->UploadFileMulti ( "Upload_LHA", "attach");
                    $assigns->insert_lampiran_lha ( $fdata_id, $flampiran[$i] );
                }
            }
        }
        $assigns->assign_update_lha ( $fdata_id, $fno_lha, $ftanggal_lha);
        $assigns->lha_update ( $fdata_id, $fno_lha, $ftanggal_lha, $fringkasan, $fdasar_audit, $ftujuan_audit, $fruang_lingkup, $fbatasan_tanggung_jawab, $fmetodologi_audit, $fstrategi_laporan, $fdata_umum_auditan, $fhasil_yang_dicapai, $fpenutup, $fstatus_lha, $fhasil_audit_lalu, $fspl_no, $fspl_klasifikasi, $fspl_lampiran, $fspl_perihal, $fspl_tempat_ttd, $fspl_date, $fspl_yth, $fspl_tembusan, $fspl_pj, $fspl_dasar, $fspl_penutup, $fpendekatan_audit);

        $fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
        $finpektorat_id = $comfunc->replacetext ( $_POST ["inpektorat_id"] );

        $comfunc->hapus_notif($flha_id);
        if ($fstatus_lha != "") {
            if($fstatus_lha==1){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }elseif($fstatus_lha==2){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '1fe7f8b8d0d94d54685cbf6c2483308aebe96229');
                $notif_to_user_id = $get_user_id; //ke daltu
            }elseif($fstatus_lha==3){
                //inpektorat
            }elseif($fstatus_lha==4){ //penugasan selesai
                $assigns->assign_update_status ( $id, 3 );
            }elseif($fstatus_lha==5){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            }elseif($fstatus_lha==6){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }elseif($fstatus_lha==7){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '1fe7f8b8d0d94d54685cbf6c2483308aebe96229');
                $notif_to_user_id = $get_user_id; //ke daltu
            }
            if($notif_to_user_id!=""){
                $comfunc->insert_notif($flha_id, $ses_userId, $notif_to_user_id, 'auditassign', '(LHA)...'.$fkomentar, $comfunc->date_db(date('d-m-Y')));
            }
        }
        $ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
        if ($fkomentar != "") {
            $assigns->lha_add_komentar ( $flha_id, $fkomentar, $ftanggal );
        }

        $comfunc->js_alert_act ( 3 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postadd" :
        $fkegiatan = $comfunc->replacetext ( $_POST ["kegiatan"] );
        $ftipe_audit = $comfunc->replacetext ( $_POST ["tipe_audit"] );
        $fauditee = $comfunc->replacetext ( $_POST ["auditee"] );
        $ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
        $fperiode = $comfunc->replacetext ( $_POST ["periode"] );
        $ftanggal_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_awal"] ) );
        $ftanggal_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_akhir"] ) );
        $count_assign_date = (($ftanggal_akhir - $ftanggal_awal) / 86400) + 1;
        $count_weekend = $comfunc->cek_holiday ( $ftanggal_awal, $ftanggal_akhir );
        $hari_kerja = $count_assign_date - $count_weekend;
        $fhari_persiapan = $comfunc->replacetext ( $_POST ["hari_persiapan"] );
        $ftanggal_persiapan_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_persiapan_awal"] ) );
        $ftanggal_persiapan_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_persiapan_akhir"] ) );
        $fhari_pelaksanaan = $comfunc->replacetext ( $_POST ["hari_pelaksanaan"] );
        $ftanggal_pelaksanaan_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaksanaan_awal"] ) );
        $ftanggal_pelaksanaan_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaksanaan_akhir"] ) );
        $fhari_pelaporan = $comfunc->replacetext ( $_POST ["hari_pelaporan"] );
        $ftanggal_pelaporan_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaporan_awal"] ) );
        $ftanggal_pelaporan_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaporan_akhir"] ) );
        $fdasar = $comfunc->replacetext ( $_POST ["dasar"] );
        $fpendanaan = $comfunc->replacetext ( $_POST ["pendanaan"] );
        $fketerangan = $comfunc->replacetext ( $_POST ["keterangan"] );
        $fpedahuluan = "";//$comfunc->replacetext ( $_POST ["pedahuluan"] );
        $ftujuan = "";//$comfunc->replacetext ( $_POST ["tujuan"] );
        $finstruksi = "";//$comfunc->replacetext ( $_POST ["instruksi"] );
        $fassign_attach = $comfunc->replacetext ( $_FILES ["assign_attach"] ["name"] );
        $ex_auditee = explode ( ",", $fauditee );
        $cek_auditee = count ( $ex_auditee );
		// surat tugas
		$fno_surat = $comfunc->replacetext ( $_POST ["no_surat"] );
		$ftanggal_surat = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_surat"] ) );
		$finspektorat = $comfunc->replacetext ( $_POST ["inspektorat"] );
		// anggota
		$fanggota_id = $_POST ["anggota_id"];
		$fposisi_id = $_POST ["posisi_id"];

        if ($fauditee != "" && $ftanggal_awal != "" && $ftanggal_akhir != "" && $fno_surat != "" && $ftanggal_surat != "") {
            // simpan ke table assingment
			$id_assign = $assigns->uniq_id ();
            $assigns->assign_add ( $id_assign, $ftipe_audit, $ftahun, $ftanggal_awal, $ftanggal_akhir, $fdasar, $hari_kerja, $fperiode, $fkegiatan, $fhari_persiapan, $fhari_pelaksanaan, $fhari_pelaporan, $fpendanaan, $fketerangan, $fassign_attach, $ftanggal_persiapan_awal, $ftanggal_persiapan_akhir, $ftanggal_pelaksanaan_awal, $ftanggal_pelaksanaan_akhir, $ftanggal_pelaporan_awal, $ftanggal_pelaporan_akhir, $fpedahuluan, $ftujuan, $finstruksi );
            // simpan ke table assignment_auditee
			for($i = 0; $i < $cek_auditee; $i ++) {
                $assigns->assign_add_auditee ( $id_assign, $ex_auditee [$i] );
            }
			// simpan ke table surat_tugas
			$id_surat_tugas = $assigns->uniq_id();
			$assigns->surat_tugas_add ( $id_assign, $id_surat_tugas, $fno_surat, $ftanggal_surat, $fttd_id, $fjabatan_surat, $ftembusan, $finspektorat );
			// simpan ke table assignment_auditor
			$count_anggota = count($fanggota_id);
			for($i=0;$i<=$count_anggota;$i++){
				$fanggota = $comfunc->replacetext ( $_POST ["anggota_id"][$i] );
				$fposisi = $comfunc->replacetext ( $_POST ["posisi_id"][$i] );
				if($fanggota!="" && $fposisi!=""){
					$id_tim = $assigns->uniq_id ();
					$assigns->assign_auditor_add ( $id_tim, $id_assign, "", $fanggota, $fposisi, $ftanggal_awal, $ftanggal_akhir, "", "", "", "", "");
				}
			}
            $comfunc->js_alert_act ( 3 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
        $fkegiatan = $comfunc->replacetext ( $_POST ["kegiatan"] );
        $ftipe_audit = $comfunc->replacetext ( $_POST ["tipe_audit"] );
        $fauditee = $comfunc->replacetext ( $_POST ["auditee"] );
        $ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
        $fperiode = $comfunc->replacetext ( $_POST ["periode"] );
        $ftanggal_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_awal"] ) );
        $ftanggal_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_akhir"] ) );
        $count_assign_date = (($ftanggal_akhir - $ftanggal_awal) / 86400) + 1;
        $count_weekend = $comfunc->cek_holiday ( $ftanggal_awal, $ftanggal_akhir );
        $hari_kerja = $count_assign_date - $count_weekend;
        $fhari_persiapan = $comfunc->replacetext ( $_POST ["hari_persiapan"] );
        $ftanggal_persiapan_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_persiapan_awal"] ) );
        $ftanggal_persiapan_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_persiapan_akhir"] ) );
        $fhari_pelaksanaan = $comfunc->replacetext ( $_POST ["hari_pelaksanaan"] );
        $ftanggal_pelaksanaan_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaksanaan_awal"] ) );
        $ftanggal_pelaksanaan_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaksanaan_akhir"] ) );
        $fhari_pelaporan = $comfunc->replacetext ( $_POST ["hari_pelaporan"] );
        $ftanggal_pelaporan_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaporan_awal"] ) );
        $ftanggal_pelaporan_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_pelaporan_akhir"] ) );
        $fdasar = $comfunc->replacetext ( $_POST ["dasar"] );
        $fpendanaan = $comfunc->replacetext ( $_POST ["pendanaan"] );
        $fketerangan = $comfunc->replacetext ( $_POST ["keterangan"] );
        $fpedahuluan = "";//$comfunc->replacetext ( $_POST ["pedahuluan"] );
        $ftujuan = "";//$comfunc->replacetext ( $_POST ["tujuan"] );
        $finstruksi = "";//$comfunc->replacetext ( $_POST ["instruksi"] );
        $fassign_attach = $comfunc->replacetext ( $_FILES ["assign_attach"] ["name"] );
        $fassign_attach_old = $comfunc->replacetext ( $_POST ["assign_attach_old"] );
        $ex_auditee = explode ( ",", $fauditee );
        $cek_auditee = count ( $ex_auditee );
		// surat tugas
		$fno_surat = $comfunc->replacetext ( $_POST ["no_surat"] );
		$ftanggal_surat = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_surat"] ) );
		$finspektorat = $comfunc->replacetext ( $_POST ["inspektorat"] );
		// anggota
		$fanggota_id = $_POST ["anggota_id"];
		$fposisi_id = $_POST ["posisi_id"];

        if ($fauditee != "" && $ftanggal_awal != "" && $ftanggal_akhir != "" && $fno_surat != "" && $ftanggal_surat != "") {
            $assigns->assign_del_auditee ( $fdata_id );
			// ubah table assignment
            $assigns->assign_edit ( $fdata_id, $ftipe_audit, $ftahun, $ftanggal_awal, $ftanggal_akhir, $fdasar, $hari_kerja, $fperiode, $fkegiatan, $fhari_persiapan, $fhari_pelaksanaan, $fhari_pelaporan, $fpendanaan, $fketerangan, $fassign_attach, $ftanggal_persiapan_awal, $ftanggal_persiapan_akhir, $ftanggal_pelaksanaan_awal, $ftanggal_pelaksanaan_akhir, $ftanggal_pelaporan_awal, $ftanggal_pelaporan_akhir, $fpedahuluan, $ftujuan, $finstruksi );
            for($i = 0; $i < $cek_auditee; $i ++) {
                $assigns->assign_add_auditee ( $fdata_id, $ex_auditee [$i] );
            }
			// ubah ke table assignment_surat_tugas 
			$assigns->surat_tugas_shortcut_edit ( $fdata_id, $fno_surat, $ftanggal_surat, "", "", "", $finspektorat );
			// ubah ke table assignment_auditor
			$count_anggota = count($fanggota_id);
			for($i=0;$i<=$count_anggota;$i++){
				$id_personil = $comfunc->replacetext ( $_POST ["id_personil"][$i] );
				$fanggota = $comfunc->replacetext ( $_POST ["anggota_id"][$i] );
				$fposisi = $comfunc->replacetext ( $_POST ["posisi_id"][$i] );
				if($fanggota!="" && $fposisi!=""){
					if($id_personil!=""){
						$assigns->assign_auditor_edit ( $id_personil, "", $fanggota, $fposisi, $ftanggal_awal, $ftanggal_akhir, "", "", "", "", "");
					}else{
					$id_tim = $assigns->uniq_id ();
					$assigns->assign_auditor_add ( $id_tim, $fdata_id, "", $fanggota, $fposisi, $ftanggal_awal, $ftanggal_akhir, "", "", "", "", "");
					}
				}else{
					$assigns->assign_auditor_delete ( $id_personil );
				}
			}
            $comfunc->js_alert_act ( 1 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $comfunc->hapus_notif($fdata_id);
        $assigns->assign_delete ( $fdata_id );
        $comfunc->js_alert_act ( 2 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $assigns->assign_count ($base_on_id_int, $key_search, $val_search, $key_field, $id_assign_dash);
        $rs = $assigns->assign_view_grid ( $base_on_id_int, $key_search, $val_search, $key_field, $offset, $num_row, $id_assign_dash );
        $page_title = "Daftar Pelaksanaan Pengawasan";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>
