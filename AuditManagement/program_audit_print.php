<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.loadTemplate-1.4.1.min.js"></script>
<?php 
include_once "_includes/classes/report_class.php";

$reports = new report ( $ses_userId );

$rs_auditee = $assigns->assign_auditee_viewlist ( $ses_assign_id, $auditee_id );
$arr_auditee = $rs_auditee->FetchRow();
$katim = $reports->get_anggota($ses_assign_id, $auditee_id, 'KT');
$dalnis = $reports->get_anggota($ses_assign_id, $auditee_id, 'PT');
?>
<section id="main" class="column">	
	<article class="module width_3_quarter">
		<table border='0' class="table_report" cellspacing='0' cellpadding="0">
			<tr>
				<td colspan="8">
					<center>
						<img src="images/kka-logo1.png" width="55" height="48" style="width:55pt;height:48pt;" /><br>
						INSPEKTORAT KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA
					</center><br>
				</td>
			</tr>
			<tr>
				<td width="5%" style="border-top:1px solid">&nbsp;</td>
				<td width="5%" style="border-top:1px solid">&nbsp;</td>
				<td style="border-top:1px solid">&nbsp;</td>
				<td style="border-top:1px solid">&nbsp;</td>
				<td width="10%" style="border-top:1px solid">No PKA</td>
				<td align="center" style="border-top:1px solid">:</td>
				<td style="border-top:1px solid" id="no_pka"></td>
				<td style="border-top:1px solid">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Nama Auditan</td>
				<td align="center">:</td>
				<td><?=$arr_auditee['auditee_name'];?></td>
				<td>Ref. PKA No</td>
				<td align="center">:</td>
				<td>-</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Sasaran Audit</td>
				<td align="center">:</td>
				<td><?=$arr['audit_type_name'];?></td>
				<td>Disusun Oleh</td>
				<td align="center">:</td>
				<td><?=$katim['nama']?></td>
				<td>Paraf :</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Tanggal</td>
				<td align="center">:</td>
				<td>-</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Periode Audit</td>
				<td align="center">:</td>
				<td><?=$arr['assign_tahun']?></td>
				<td>Direviu Oleh</td>
				<td align="center">:</td>
				<td><?=$dalnis['nama'] ?></td>
				<td>Paraf :</td>
			</tr>
			<tr>
				<td colspan="2" style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">Tanggal</td>
				<td style="border-bottom:1px solid" align="center">:</td>
				<td style="border-bottom:1px solid">-</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="8" align="center"><br>PROGRAM KERJA AUDIT RINCI/LANJUTAN</td>
			</tr>
			<tr>
				<td><strong>A.</strong></td>
				<td colspan="7"><strong>Pendahuluan</strong></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="7"><label id="pendahuluan"></label></td>
			</tr>
			<tr>
				<td><strong>B.</strong></td>
				<td colspan="7"><strong>Tujuan Audit</strong></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="7"><label id="tujuan"></label></td>
			</tr>
			<tr>
				<td><strong>C.</strong></td>
				<td colspan="7"><strong>Instruksi Khusus</strong></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid" colspan="7"><label id="instruksi"></label></td>
			</tr>
			<tr>
				<td><strong>D.</strong></td>
				<td colspan="7"><strong>Langkah Kerja:</strong>				
				<?
					$rs_aspek = $programaudits->get_assign_aspek("", $ses_assign_id, $auditee_id);
					$arr_aspek = $rs_aspek->GetArray();
					echo $comfunc->buildCombo("fil_aspek", $arr_aspek, 0, 1, "", "", "", false, true, false);
				?>
				</td>
			</tr>
		</table>
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr align="center">
				<th width="2%" rowspan="2">No.</th>
				<th width="50%" rowspan="2">Langkah Kerja Audit</th>
				<th width="16%" colspan="2">Dilaksanakan Oleh</th>
				<th width="16%" colspan="2">Waktu yang Diperlukan</th>
				<th width="9%" rowspan="2">Nomor KKA</th>
				<th width="7%" rowspan="2">Catatan</th>
			</tr>
			<tr align="center">
				<th>Rencana</th>
				<th>Realisasi</th>
				<th>Rencana</th>
				<th>Realisasi</th>
			</tr>
			<tbody id="table_rinci">
			</tbody>
		</table>
		<table border='0' cellspacing='0' cellpadding="0" width="100%">
			<tr>
				<td width="20%" rowspan="4">&nbsp;</td>
				<td width="30%">&nbsp;</td>
				<td width="20%" width="20%" rowspan="4">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Jakarta, <?=date("d M Y")?></td>
			</tr>
			<tr>
				<td>Direvui</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Tanggal:<?=date("d M Y")?><br>PENGENDALI TEKNIS<br><br><br><br><br><u><?=$dalnis['nama']?></u><br><?=$dalnis['jabatan']?></td>
				<td>Disusun Olek<br>KETUAN TIM<br><br><br><br><br><u><?=$katim['nama']?></u><br><?=$katim['jabatan']?></td>
			</tr>
			<tbody id="table_rinci">
			</tbody>
		</table>
		<fieldset>
			<center>
				<input type="hidden" id="id_assign" value="<?=$ses_assign_id?>">
				<input type="hidden" id="id_auditee" value="<?=$auditee_id?>">
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'">
				<input type="button" class="blue_btn" value="ms-word" onClick="window.open('AuditManagement/program_audit_word.php?id_assign=<?=$ses_assign_id?>&id_auditee=<?=$auditee_id?>&fil_aspek='+document.getElementById('fil_aspek').value, '_blank','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
			</center>
		</fieldset>
	</article>
</section>
<script>
$("#fil_aspek").on("change", function(){
	  var id_aspek = $(this).val(),
		  id_assign = $("#id_assign").val();
	  $.ajax({
		url: 'AuditManagement/ajax.php?data_action=get_introProg',
		type: 'POST',
		dataType: 'json',
		data: {id_aspek: id_aspek, id_assign: id_assign},
		success: function(data) {
			$("#id").text(data.id);
			$("#pendahuluan").text(data.pendahuluan);
			$("#tujuan").text(data.tujuan);
			$("#instruksi").text(data.instruksi);
			$("#no_pka").text(data.no_pka)
			getDataAspek();
		}
	  });
});

function getDataAspek(){
	var id_aspek = $("#fil_aspek option:selected").val(),
		id_assign = $("#id_assign").val(),
		id_auditee = $("#id_auditee").val();
		no = 1;
		
	console.log('get aspek : '+id_aspek+", get assign : "+id_assign+", get satker : "+id_auditee);
	$.ajax({
		url: 'AuditManagement/ajax.php?data_action=getAspek_rinci',
		type: 'POST',
		dataType: 'json',
		data: {id_aspek: id_aspek, id_assign: id_assign, id_auditee: id_auditee},
		success: function(data) {	
			 $("#table_rinci").empty();
			 $.each(data.aspek_rinci, function(i, item) {				
				$("#table_rinci").loadTemplate("#table_rinci_tmpl", 
					{
						title : item.title,
						auditor : item.auditor,
						jam : item.jam,
						catatan : item.catatan,
						no_kka : item.no_kka,
						realisasi : item.realisasi
					},
					{
						prepend: true
					}
				);
			});
		}
	});
}
</script>

<script type="text/html" id="table_rinci_tmpl">
	<tr class="row_item">
		<td>&nbsp;</td>
		<td><span data-content="title"></span></td>
		<td><span data-content="auditor"></span></td>
		<td><span data-content="auditor"></span></td>
		<td><span data-content="jam"></span></td>
		<td><span data-content="realisasi"></span></td>
		<td><span data-content="no_kka"></span></td>
		<td><span data-content="catatan"></span></td>
	</tr>
</script>