<?
include_once "_includes/classes/auditor_class.php";
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/audit_plann_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/param_class.php";

$auditors = new auditor ( $ses_userId );
$auditees = new auditee ( $ses_userId );
$plannings = new planning ( $ses_userId );
$assigns = new assign ( $ses_userId );
$params = new param ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );
if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=auditplan";
$acc_page_request = "audit_plan_acc.php";
$list_page_request = "audit_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "";
$grid = "grid_audit.php";
$gridHeader = array ("Nama Kegiatan", "Obyek Pengawasan", "Tipe Pengawasan", "Rencana Kegiatan", "Tim Audit", "Status");
$gridDetail = array ("3","0", "1", "0", "0", "2");
$gridWidth = array ("15", "20", "10", "15", "5", "10");

$key_by = array ("Nama Kegiatan");
$key_field = array ("audit_plan_kegiatan");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

switch ($_action) {
	case "getajukan" :
	case "getapprove" :
		$_nextaction = "postkomentar";
		$page_request = $acc_page_request;
		$id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$status = $comfunc->replacetext ( $_REQUEST ["status_plan"] );
		
		$rs = $plannings->planning_viewlist ( $id );
		$page_title = "Pengajuan Perencanaan Pengawasan";
		break;
	case "postkomentar" :
		$from = $comfunc->replacetext ( $_POST ["data_from"] );
		$id_plann = $comfunc->replacetext ( $_POST ["data_id"] );
		$status = $comfunc->replacetext ( $_POST ["status_plan"] );
		$fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
		
		$notif_date = $comfunc->dateSql(date("d-m-Y"));
		$comfunc->hapus_notif($id_plann);
		
		if ($status != "") {
			$group_menu_id = "";
			if ($status != "") {
				if($status=='1') { //ajukan
					$group_menu_id = '42';
				}elseif ($status=='3'){//tolak
					$group_menu_id = '41';
				}
				if($group_menu_id!=""){
					$rs_user_accept = $comfunc->notif_user_all_bygroup($group_menu_id);
					while($arr_user_accept = $rs_user_accept->FetchRow()){
						$comfunc->insert_notif($id_plann, $ses_userId, $arr_user_accept['user_id'], 'auditplan', "(Perencanaan Pengawasan) ".$fkomentar, $notif_date);
					}
				}
				$plannings->planning_update_status ( $id_plann, $status, $from );
			}			
		}
		
		if ($status == '2') {
			$plannings->planning_add_assign ( $id_plann );
		}
		
		$ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
		if ($fkomentar != "") {
			$plannings->planning_add_komentar ( $id_plann, $fkomentar, $ftanggal );
			$comfunc->js_alert_act ( 3 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "anggota_plan" :
		$_SESSION ['ses_plan_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		?>
<script>window.open('main_page.php?method=anggota_plan', '_self');</script>
<?
		break;
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Perencanaan Pengawasan";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $plannings->planning_viewlist ( $fdata_id );
		$page_title = "Ubah Perencanaan Pengawasan";
		break;
	case "getdetail" :
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $plannings->planning_viewlist ( $fdata_id );
		$page_title = "Rincian Perencanaan Pengawasan";
		break;
	case "postadd" :
		$ftipe_audit = $comfunc->replacetext ( $_POST ["tipe_audit"] );
		$fsub_type = $comfunc->replacetext ( $_POST ["sub_type"] );
        $fauditee = $comfunc->replacetext ( $_POST ["auditee"] );
        //$fjml_hari = $comfunc->replacetext ( $_POST ["jml_hari"]);
		$ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
		$auto_num = mt_rand(100000, 999999);
		$kode_plan = $ftahun."_".$auto_num;
		$ftujuan = $comfunc->replacetext ( $_POST ["tujuan"] );
		$fperiode = $comfunc->replacetext ( $_POST ["periode"] );
		$fbiaya_audit = $comfunc->replacetext ( $_POST ["biaya_audit"], true );
		$ftanggal_awal = $comfunc->date_db_ori ( $comfunc->replacetext ( $_POST ["tanggal_awal"] ) );
		$ftanggal_akhir = $comfunc->date_db_ori ( $comfunc->replacetext ( $_POST ["tanggal_akhir"] ) );
		$convert_tgl_awal = $comfunc->date_db ($ftanggal_awal);
		$convert_tgl_akhir = $comfunc->date_db ($ftanggal_akhir);
		$count_plan_date = (($convert_tgl_akhir - $convert_tgl_awal) / 86400) + 1;
		$count_weekend = $comfunc->cek_holiday ( $convert_tgl_awal, $convert_tgl_akhir );
		$hari_kerja = $count_plan_date - $count_weekend;
        $ex_auditee = explode ( ",", $fauditee );
        $cek_auditee = count ( $ex_auditee );
//        $ex_jmlhari = explode ( ",", $fjml_hari );
//        $cek_jmlhari = count ( $ex_jmlhari );
		
		if ($ftipe_audit != "" && $ftahun != "" && $ftanggal_akhir != "" && $cek_auditee != "0") {
			$id_plann = $plannings->uniq_id ();
			$plannings->planning_add ( $id_plann, $kode_plan, $ftipe_audit, $ftahun, $ftanggal_awal, $ftanggal_akhir, $hari_kerja, $ftujuan, $fperiode, $fbiaya_audit, $fsub_type );
			//auditee
            for($i = 0; $i < $cek_auditee; $i ++) {
                $plannings->planning_add_auditee ( $id_plann, $ex_auditee[$i] );
            }
			
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$ftipe_audit = $comfunc->replacetext ( $_POST ["tipe_audit"] );
		$fsub_type = $comfunc->replacetext ( $_POST ["sub_type"] );
        $fauditee = $comfunc->replacetext ( $_POST ["auditee"] );
		$ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
		$ftujuan = $comfunc->replacetext ( $_POST ["tujuan"] );
		$fperiode = $comfunc->replacetext ( $_POST ["periode"] );
		$fbiaya_audit = $comfunc->replacetext ( $_POST ["biaya_audit"], true );
		$ftanggal_awal = $comfunc->date_db_ori ( $comfunc->replacetext ( $_POST ["tanggal_awal"] ) );
		$ftanggal_akhir = $comfunc->date_db_ori ( $comfunc->replacetext ( $_POST ["tanggal_akhir"] ) );
		$convert_tgl_awal = $comfunc->date_db ($ftanggal_awal);
		$convert_tgl_akhir = $comfunc->date_db ($ftanggal_akhir);
		$count_plan_date = (($convert_tgl_akhir - $convert_tgl_awal) / 86400) + 1;
		$count_weekend = $comfunc->cek_holiday ( $convert_tgl_awal, $convert_tgl_akhir );
		$hari_kerja = $count_plan_date - $count_weekend;
        $ex_auditee = explode ( ",", $fauditee );
        $cek_auditee = count ( $ex_auditee );

		if ($ftipe_audit != "" && $ftahun != "" && $ftanggal_akhir != "" && $cek_auditee != "0") {
			$plannings->planning_edit ( $fdata_id, $ftipe_audit, $ftahun, $ftanggal_awal, $ftanggal_akhir, $hari_kerja, $ftujuan, $fperiode, $fbiaya_audit, $fsub_type );
			//auditee
            $plannings->planning_del_auditee ( $fdata_id );
            //auditee
            for($i = 0; $i < $cek_auditee; $i ++) {
                $plannings->planning_add_auditee ( $fdata_id, $ex_auditee[$i] );
            }
            $comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$plannings->planning_delete ( $fdata_id );
		$comfunc->hapus_notif($fdata_id);
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $plannings->planning_count ($base_on_id_int, $key_search, $val_search, $key_field);
		$rs = $plannings->planning_view_grid ($base_on_id_int, $key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Perencanaan Pengawasan";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>