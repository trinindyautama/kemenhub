<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/auditor_class.php";
include_once "_includes/classes/finding_class.php";
include_once "_includes/classes/rekomendasi_class.php";
include_once "_includes/classes/tindaklanjut_class.php";

$assigns = new assign ( $ses_userId );
$auditors = new auditor ( $ses_userId );
$findings = new finding ( $ses_userId );
$rekomendasis = new rekomendasi ( $ses_userId );
$tindaklanjuts = new tindaklanjut ( $ses_userId );

$ses_assign_id = $_SESSION ['ses_assign_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

if($method=='matrikstindaklanjut'){
	$method='tindaklanjut';	
	$iconAdd = $comfunc->cek_akses ( $ses_group_id, $method, 'getadd' );
	$_SESSION ['ses_rekomendasi_id'] = $comfunc->replacetext ( $_REQUEST ["idRec"] );
	$paging_request = "main_page.php?method=matrikstindaklanjut&idRec=".$_SESSION ['ses_rekomendasi_id'];
}else{
	$paging_request = "main_page.php?method=tindaklanjut";
}

$ses_rekomendasi_id = $_SESSION ['ses_rekomendasi_id'];

$acc_page_request = "tindaklanjut_acc.php";
$list_page_request = "tindaklanjut_view.php";
// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "tindaklanjut_view_parrent.php";
$grid = "grid_monitoring.php";
$gridHeader = array ("Tindak Lanjut ", "Tanggal", "Status TL", "Nilai");
$gridDetail = array ("1", "2", "3", "4");
$gridWidth = array ("45", "15", "15", "10");

$key_by = array ("Tindak Lanjut");
$key_field = array ("jenis_jabatan", "jenis_jabatan_sub");

$widthAksi = "15";
$iconDetail = "0";
/*
if($method=='matrikstindaklanjut'){
	$iconAdd = $comfunc->cek_akses ( $ses_group_id, "tindaklanjut", 'getadd' );
	$iconEdit = $comfunc->cek_akses ( $ses_group_id, "tindaklanjut", 'getedit' );
	$iconDel = $comfunc->cek_akses ( $ses_group_id, "tindaklanjut", 'getdelete' );
}
*/
// === end grid ===//

switch ($_action) {
	case "update_status" :
		$status = $comfunc->replacetext ( $_POST ["status_rek"] );
		$rekomendasis->update_status_rekomendasi ( $ses_rekomendasi_id ) ;
		$id_temuan = $rekomendasis->get_temuan_id($ses_rekomendasi_id);
		$cek_rek_selesai = $rekomendasis->rekomendasi_count($id_temuan, '1', "", "", "");
		$cek_rek_all = $rekomendasis->rekomendasi_count($id_temuan, "", "", "", "");
		if($cek_rek_selesai==$cek_rek_all) $findings->finding_update_status($id_temuan, '8');
		$comfunc->js_alert_act ( 1 );
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getajukan_tl" :
	case "getapprove_tl" :
		$_nextaction = "postkomentar";
		$page_request = $acc_page_request;
		$id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$status = $comfunc->replacetext ( $_REQUEST ["status_tl"] );

		$rs = $tindaklanjuts->tindaklanjut_viewlist ( $id );
		$page_title = "Reviu Tindak Lanjut";
		break;
	case "postkomentar" :
		$id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
		$ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
		$status = $comfunc->replacetext ( $_REQUEST ["status_tl"] );
		if ($status != "") {
			$comfunc->hapus_notif($id);

			if($status==1){ 
                $get_user_id = $assigns->get_user_by_posisi($ses_assign_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }

            if($notif_to_user_id!=""){
                $comfunc->insert_notif($id, $ses_userId, $notif_to_user_id, 'tindaklanjut', '(Tindak Lanjut) '.$fkomentar, $ftanggal);
            }

			$tindaklanjuts->tindaklanjut_update_ststus ( $id, $status );

			if ($fkomentar != "") {
				$tindaklanjuts->tindaklanjut_add_komentar ( $id, $fkomentar, $ftanggal );
				// $comfunc->js_alert_act ( 3 );
			}

			$comfunc->js_alert_act ( 3 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Tindak Lanjut";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $tindaklanjuts->tindaklanjut_viewlist ( $fdata_id );
		$page_title = "Ubah Tindak Lanjut";
		break;
	case "postadd" :
		$ftl_desc = $comfunc->replacetext ( $_POST ["tl_desc"] );
		$ftl_date = $comfunc->date_db ( date ( "d-m-Y" ) );
		$ftl_attachment = $comfunc->replacetext ( $_FILES ["tl_attach"] ["name"] );
		$ftl_nilai = $comfunc->replacetext ( $_POST ["tl_nilai"] );
		if ($ftl_desc != "") {
			$tindaklanjuts->tindaklanjut_add ( $ses_rekomendasi_id, $ftl_desc, $ftl_date, $ftl_attachment, $ftl_nilai );
			$comfunc->UploadFile ( "Upload_Tindaklanjut", "tl_attach", "" );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$ftl_desc = $comfunc->replacetext ( $_POST ["tl_desc"] );
		$ftl_date = $comfunc->date_db ( date ( "d-m-Y" ) );
		$ftl_attachment = $comfunc->replacetext ( $_FILES ["tl_attach"] ["name"] );
		$ftl_attach_old = $comfunc->replacetext ( $_POST ["tl_attach_old"] );
		$ftl_nilai = $comfunc->replacetext ( $_POST ["tl_nilai"] );
		if ($ftl_desc != "") {
			if (! empty ( $ftl_attachment )) {
				$comfunc->UploadFile ( "Upload_Tindaklanjut", "tl_attach", $ftl_attach_old );
			} else {
				$ftl_attachment = $ftl_attach_old;
			}
			$tindaklanjuts->tindaklanjut_edit ( $fdata_id, $ftl_desc, $ftl_date, $ftl_attachment, $ftl_nilai );
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$comfunc->hapus_notif($id);
		$tindaklanjuts->tindaklanjut_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $tindaklanjuts->tindaklanjut_count ( $ses_rekomendasi_id, $key_search, $val_search, $key_field);
		$rs = $tindaklanjuts->tindaklanjut_view_grid ( $ses_rekomendasi_id, $key_search, $val_search, $key_field, $offset, $num_row);
		$page_title = "Daftar Tindak Lanjut";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
