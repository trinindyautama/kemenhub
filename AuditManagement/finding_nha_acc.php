<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>

<section id="main" class="column">
    <?
    if (! empty ( $view_parrent ))
        include_once $view_parrent;
    ?>
    <article class="module width_3_quarter">
        <header>
            <h3 class="tabs_involved"><?=$page_title?></h3>
        </header>
        <form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
            <?
            switch ($_action) {
                case "getadd" :
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Judul temuan</label> <input type="text" class="span7" name="judul" id="judul">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset>
                        <ul class="rtabs">
                            <li><a href="#view1">Uraian</a></li>
                            <li><a href="#view2">Kriteria</a></li>
                            <li><a href="#view3">Sebab</a></li>
                            <li><a href="#view4">Akibat</a></li>
                            <li><a href="#view5">Tanggapan Auditee</a></li>
                        </ul>
                        <div id="view1">
                            <textarea class="ckeditor" cols="10" rows="40" name="uraian" id="uraian"></textarea>
                        </div>
                        <div id="view2">
                            <textarea class="ckeditor" cols="10" rows="40" name="kriteria" id="kriteria"></textarea>
                        </div>
                        <div id="view3">
                            <textarea class="ckeditor" cols="10" rows="40" name="sebab" id="sebab"></textarea>
                        </div>
                        <div id="view4">
                            <textarea class="ckeditor" cols="10" rows="40" name="akibat" id="akibat"></textarea>
                        </div>
                        <div id="view5">
                            <textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditee" id="tanggapan_auditee"></textarea>
                        </div>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Lampiran</label> <input type="file" class="span4" name="attachment" id="attachment">
                    </fieldset>
                    <?
                    break;
                case "getedit" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Judul temuan</label> <input type="text" class="span7" name="judul" id="judul" value="<?=$arr['finding_judul']?>">
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset>
                        <ul class="rtabs">
                            <li><a href="#view1">Uraian</a></li>
                            <li><a href="#view2">Kriteria</a></li>
                            <li><a href="#view3">Sebab</a></li>
                            <li><a href="#view4">Akibat</a></li>
                            <li><a href="#view5">Tanggapan Auditee</a></li>
                        </ul>
                        <div id="view1">
                            <textarea class="ckeditor" cols="10" rows="40" name="uraian" id="uraian"><?=$arr['finding_uraian']?></textarea>
                        </div>
                        <div id="view2">
                            <textarea class="ckeditor" cols="10" rows="40" name="kriteria" id="kriteria"><?=$arr['finding_kriteria']?></textarea>
                        </div>
                        <div id="view3">
                            <textarea class="ckeditor" cols="10" rows="40" name="sebab" id="sebab"><?=$arr['finding_sebab']?></textarea>
                        </div>
                        <div id="view4">
                            <textarea class="ckeditor" cols="10" rows="40" name="akibat" id="akibat"><?=$arr['finding_akibat']?></textarea>
                        </div>
                        <div id="view5">
                            <textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditee" id="tanggapan_auditee"><?=$arr['finding_tanggapan_auditee']?></textarea>
                        </div>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Lampiran</label>
                        <input type="hidden" class="span4" name="attachment_old" value="<?=$arr['finding_attachment']?>">
                        <input type="file" class="span4" name="attachment" id="finding_attach">
                        <label class="span2"><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Temuan").$arr['finding_attachment']?>','_blank')"><?=$arr['finding_attachment']?></a></label>
                    </fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['finding_id']?>">
                    <?
                    break;
                case "getdetail" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <table class="view_parrent">
                            <tr>
                                <td>Judul temuan</td>
                                <td>:</td>
                                <td><?=$arr['finding_judul']?></td>
                            </tr>
                           <tr>
                                <td>Uraian</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['finding_uraian'])?></td>
                            </tr>
                            <tr>
                                <td>Kriteria</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['finding_kriteria'])?></td>
                            </tr>
                            <tr>
                                <td>Sebab</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['finding_sebab'])?></td>
                            </tr>
                            <tr>
                                <td>Akibat</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['finding_akibat'])?></td>
                            </tr>
                            <tr>
                                <td>Tanggapan Satuan Kerja</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['finding_tanggapan_auditee'])?></td>
                            </tr>
                            <tr>
                                <td>Lampiran</td>
                                <td>:</td>
                                <td><a href="#"
                                       Onclick="window.open('<?=$comfunc->baseurl("Upload_Temuan").$arr['finding_attachment']?>','_blank')"><?=$arr['finding_attachment']?></a></td>
                            </tr>
                        </table>
                    </fieldset>

                    <?
                    break;
            }
            ?>
            <fieldset>
                <center>
                    <input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
                    <?
                    if($_action!='getdetail'){
                        ?>
                        <input type="submit" class="blue_btn" value="Simpan">
                        <?
                    }
                    ?>
                </center>
                <input type="hidden" name="data_action" id="data_action"
                       value="<?=$_nextaction?>">
            </fieldset>
        </form>
    </article>
</section>
<script>
    $(function() {
        $("#validation-form").validate({
            rules: {
                judul: "required"
            },
            messages: {
                judul: "Silahkan Masukan Judul"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>