<?php
//echo $_GET
if (!isset($_GET['finding_id'])) {
    $rs_finding = $findings->finding_viewlist ( $ses_finding_id );
} else {
    $rs_finding = $findings->finding_viewlist ( $_GET['finding_id'] );
}
$arr_finding = $rs_finding->FetchRow ();

$rs_assign = $assigns->assign_viewlist ( $arr_finding['finding_assign_id'] );
$arr_assign = $rs_assign->FetchRow ();

$rs_lha = $assigns->assign_lha_viewlist ( $arr_finding['finding_assign_id'], $arr_finding['finding_auditee_id'] );
$arr_lha = $rs_lha->FetchRow ();
$tgl_lha = $comfunc->dateIndo($arr_lha['lha_date']);
$tgl_lha2 = date('d-m-Y', strtotime('+60 days', strtotime($tgl_lha)));

?>
<article class="module width_3_quarter">
	<fieldset>
		<table class="view_parrent">
			<tr>
				<td>Satuan Kerja</td>
				<td>:</td>
				<td><?=$arr_finding ['auditee_name']?></td>
			</tr>
			<tr>
				<td>Tahun Audit</td>
				<td>:</td>
				<td><?=$arr_assign['assign_tahun']?></td>
			</tr>
			<tr>
				<td>Tanggal Audit</td>
				<td>:</td>
				<td><?=$comfunc->dateIndo($arr_assign['assign_start_date'])?> s/d <?=$comfunc->dateIndo($arr_assign['assign_end_date'])?></td>
			</tr>
			<tr>
				<td>Judul Temuan</td>
				<td>:</td>
				<td><?=$arr_finding ['finding_judul']?></td>
			</tr>
		</table>
	</fieldset>
	<?php
	if ($method == "rekomendasi_nha") {
	?>
	<fieldset>
		<input type="button" class="blue_btn" value="Kembali" onclick="location='main_page.php?method=finding_nha'"> 
	</fieldset>
	<?
		}
	?>
</article>