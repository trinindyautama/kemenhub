<?php
include_once "../_includes/login_history.php";
include_once "../_includes/classes/auditee_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/classes/finding_class.php";
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/common.php";

$assigns = new assign($ses_userId);
$findings = new finding($ses_userId);
$comfunc = new comfunction($ses_userId);
$programaudits = new programaudit($ses_userId);
$rs_auditee = $assigns->auditee_detil ( $_REQUEST['id_auditee'] );
$arr_auditee = $rs_auditee->FetchRow();
$assign_id_auditee =$arr_auditee['auditee_id'];
$assign_nama_auditee = $arr_auditee['auditee_name'];

header("Content-Type: application/msword");
header("Content-Type: image/jpg");
header('Content-Disposition: attachment; filename=MATRIKS TEMUAN '.str_replace(',', '', $assign_nama_auditee).' .doc');
header("Pragma: no-cache");
header("Expires: 0");
define ("INBOARD",false);

$rs = $assigns->assign_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr = $rs->FetchRow();
// lha
$rs_lha = $assigns->assign_lha_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_lha = $rs_lha->FetchRow ();
// spl
$rs_spl = $assigns->assign_spl_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_spl = $rs_spl->FetchRow ();
// get value finding
$rs_finding = $findings->finding_tl_nha_count ( $_REQUEST['id_assign'], "", "", "");
// temuan dan rekomendasi
$rs_spl_temuan_rekomendasi = $assigns->assign_spl_viewlist ( $_REQUEST['id_assign'], $assign_id_auditee );
$arr_spl_temuan_rekomendasi = $rs_spl_temuan_rekomendasi->FetchRow ();
// temuan dan rekomendasi
$cek_posisi = $findings->cek_posisi($_REQUEST['id_assign']);

$rs_auditee_viewlist = $assigns->assign_auditee_get_inspektorat_viewlist($_REQUEST['id_assign'], $assign_id_auditee);
$arr_auditee_viewlist = $rs_auditee_viewlist->FetchRow();
?>
<article class="module width_3_quarter">
	<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
		<tr>
			<td style="border-left:0" colspan="6" align="center" valign="middle">MATRIKS  TEMUAN  HASIL  AUDIT</td>
		</tr>
		<tr>
			<td>NAMA AUDITAN</td>
			<td>:</td>
			<td><?=$assign_nama_auditee?></td>
			<td>Lampiran</td>
			<td>:</td>
			<td>LHA - <?=$arr_lha['audit_type_name']?></td>
		</tr>
		<tr>
			<td>SASARAN AUDIT</td>
			<td>:</td>
			<td><?=$arr_lha['audit_type_name']?></td>
			<td>Nomor</td>
			<td>:</td>
			<td><?=$arr_lha['assign_no_lha']?></td>
		</tr>
		<tr>
			<td>TAHUN ANGGARAN</td>
			<td>:</td>
			<td><?=$arr_lha['assign_tahun']?></td>
			<td>Tanggal</td>
			<td>:</td>
			<td><?=$comfunc->dateIndo($arr_lha['lha_date'])?></td>
		</tr>
	</table>
	<br>
	<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
		<tr>
			<th width="5%">No</th>
			<th width="35%">Uraian Temuan</th>
			<th width="10%">Kode Temuan & Sebab</th>
			<th width="35%">Rekomendasi</th>
			<th width="10%">Kode Rekomendasi</th>
			<th width="5%">Tindak Lanjut</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
		</tr>
        <?
        $no_temuan = 0;
        $rs_list_temuan = $assigns->temuan_list_nha($arr['assign_id']);
        while($arr_list_temuan = $rs_list_temuan->FetchRow()){
            $no_temuan++;
            $no_rek = 0;
            $rs_list_rek = $findings->get_rekomendasi_list($arr_list_temuan['finding_id']);
            $count_rek = $rs_list_rek->RecordCount();
            while($arr_list_rek = $rs_list_rek->FetchRow()){
                $no_rek++;
                if($arr_list_rek['rekomendasi_status']==1) $status = "Selesai";
                else $status = "Proses";
                ?>
                <tr>
                    <?
                    if($no_rek==1){
                        ?>
                        <td rowspan="<?=$count_rek?>"><?=$no_temuan?></td>
                        <td rowspan="<?=$count_rek?>">
                            <?
                            echo "<strong>".$comfunc->text_show($arr_list_temuan['finding_judul'])."</strong><br><br>";
                            echo "<strong>Kondisi</strong><br>";
                            echo $comfunc->text_show($arr_list_temuan['finding_kondisi'])."<br>";
                            echo "<strong>Sebab & Akibat</strong><br>";
                            echo $comfunc->text_show($arr_list_temuan['finding_sebab'])."<br>";
                            ?>
                        </td>
                        <td rowspan="<?=$count_rek?>"><?=$arr_list_temuan['kode_temuan']?><br><?=$arr_list_temuan['kode_penyebab_temuan']?></td>
                        <?
                    }
                    ?>
                    <td><?=$arr_list_rek['rekomendasi_desc']?></td>
                    <td><?=$arr_list_rek['kode_rek_code']?></td>
                </tr>
                <?
            }
        }
        ?>
	</table>
	<table border='0' cellspacing='0' cellpadding="0" width="100%">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="border-style: none; border-bottom-style: none" colspan="4"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td width="10%">&nbsp;</td>
            <td width="30%">&nbsp;</td>
            <td width="30%">&nbsp;</td>
            <td align="center" width="35%">Mengetahui :</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center"><?=$arr_auditee_viewlist['inspektorat_name']?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <? foreach (range(1, 5) as $item): ?>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center">
                <?
                if ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat I") {
                ?>
                <u>
                    Muhammad Anto Julianto, S.E., M.Si., AK, CA
                </u>
                <br>
                        Pembina Utama Muda (IV/c)
                    <br>
                    NIP. 196807251990031001
                    <?
                } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat II") {
                ?>
                    <u>
                        Firdaus Komarno
                    </u>
                    <br>
                        Pembina Utama Muda (IV/c)
                    <br>
                    NIP. 196110242016021001
                    <?
                } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat III") {
                    ?>
                    <u>
                        Mohamad Murdiyanto, S.E., M.Si
                    </u>
                    <br>
                        Pembina Utama Muda (IV/c)
                    <br>
                    NIP. 196703141992031003
                    <?
                } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat IV") {
                    ?>
                    <u>
                        Drs. Imam Hambali, M.Si.
                    </u>
                    <br>
                        Pembina Utama Muda (IV/c)
                    <br>
                    NIP. 196204041988031001
                    <?
                } elseif ($arr_auditee_viewlist['inspektorat_name']=="Inspektorat V") {
                    ?>
                    <u>
                        Ir. Heri Sudarmaji, DEA
                    </u>
                    <br>
                        Pembina Utama Muda (IV/c)
                    <br>
                    NIP. 196609071994031001
                    <?
                }
                ?>
                </td>
        </tr>
    </table>
    <br><br><br>
</article>