<?php
set_time_limit(0);
ini_set('memory_limit', '1024M'); 
require '../_includes/html2pdf/vendor/autoload.php';
define('DOMPDF_ENABLE_AUTOLOAD', false);
define("DOMPDF_ENABLE_FONTSUBSETTING", true);
define("DOMPDF_UNICODE_ENABLED", true);
define("DOMPDF_ENABLE_REMOTE", true);
 
require_once '../_includes/html2pdf/vendor/dompdf/dompdf/dompdf_config.inc.php';
include_once "../_includes/login_history.php";
include_once "../_includes/classes/auditee_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/classes/finding_class.php";
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/common.php";

$assigns = new assign($ses_userId);
$findings = new finding($ses_userId);
$comfunc = new comfunction($ses_userId);
$programaudits = new programaudit($ses_userId);
$rs_auditee = $assigns->auditee_detil ( $_REQUEST['id_auditee'] );
$arr_auditee = $rs_auditee->FetchRow();
$assign_id_auditee =$_REQUEST['id_auditee'];
$assign_nama_auditee = $arr_auditee['auditee_name'];

$rs = $assigns->assign_viewlist ( $_REQUEST['id_assign'] );
$arr = $rs->FetchRow();
$rs_lha = $assigns->assign_lha_viewlist ( $_REQUEST['id_assign'] );
$arr_lha = $rs_lha->FetchRow ();

$htmlString = '';
ob_start();
include('hasil_audit_template.php');
$htmlString .= ob_get_clean();

$dompdf = new DOMPDF();
// $dompdf->set_option( 'dpi' , '300' );
$dompdf->load_html($htmlString);
$dompdf->set_paper('b4','potrait');
$dompdf->render();
$dompdf->stream("LAPORAN HASIL AUDIT ".str_replace(',', '', $assign_nama_auditee).".pdf");
?>