<?
include_once "_includes/classes/assignment_nha_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/finding_class.php";
include_once "_includes/classes/auditee_class.php";
include_once "_includes/classes/param_class.php";

$assignment_nhas = new assignment_nha ( $ses_userId );
$assigns = new assign ( $ses_userId );
$auditees = new auditee ( $ses_userId );
$findings = new finding ( $ses_userId );
$params = new param ();

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=assign_nha";
$acc_page_request = "assign_nha_acc.php";
$lha_page_request = "hasil_audit.php";
$list_page_request = "audit_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
    if (is_numeric ( $str_page ) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("Inspektorat", "Obyek Pemeriksaan", "No ST", "Provinsi", "Periode Audit", "Temuan", "Input LHA");
$gridDetail = array ("inspektorat_name", "auditee_name", "assign_surat_no", "propinsi_name", "0", "0", "nha_assign_id");
$gridWidth = array ("10", "10", "15", "10", 5, "10", "5", "5");

$key_by = array ("Inspektorat", "Obyek Pemeriksaan", "No ST", "Provinsi");
$key_field = array ("inspektorat_name", "auditee_name", "nha_no_spt", "propinsi_name");

$widthAksi = "15";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
    case "getlha_nha" :
        $_nextaction = "postlha";
        $page_request = $lha_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $get_id = explode(":",$fdata_id );
        $assign_id = $get_id[0];
        $auditee_id = $get_id[1];
        
        $rs = $assigns->assign_viewlist ( $assign_id );
        $arr = $rs->FetchRow();

        $rs_auditee = $assigns->auditee_detil ( $auditee_id );
        $arr_auditee = $rs_auditee->FetchRow();


        $page_title = "Laporan Hasil Audit ". $arr_auditee['auditee_name'];
        break;
    case "postlha" :
        $flha_id = $comfunc->replacetext ( $_POST ["lha_id"] );
        $fassign_id = $comfunc->replacetext ( $_POST ["data_id"] );
        $flha_id = $comfunc->replacetext ( $_POST ["lha_id"] );
        $fno_lha = $comfunc->replacetext ( $_POST ["no_lha"] );
        $ftanggal_lha = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_lha"] ) );
        $fringkasan = $comfunc->replacetext ( $_POST ["ringkasan"] );
        $fdasar_audit = $comfunc->replacetext ( $_POST ["dasar_audit"] );
        $ftujuan_audit = $comfunc->replacetext ( $_POST ["tujuan_audit"] );
        $fmetodologi_audit = $comfunc->replacetext ( $_POST ["metodologi_audit"] );
        $fstrategi_laporan = $comfunc->replacetext ( $_POST ["strategi_laporan"] );
        $fdata_umum_auditan = $comfunc->replacetext ( $_POST ["data_umum_auditan"] );
        $fruang_lingkup = $comfunc->replacetext ( $_POST ["ruang_lingkup"] );
        $fbatasan_tanggung_jawab = $comfunc->replacetext ( $_POST ["batasan_tanggung_jawab"] );
        $fkegiatan = $comfunc->replacetext ( $_POST ["kegiatan"] );
        $fhasil_yang_dicapai = $comfunc->replacetext ( $_POST ["hasil_yang_dicapai"] );
        $fpenutup = $comfunc->replacetext ( $_POST ["penutup"] );
        $fhasil_audit_lalu = $comfunc->replacetext($_POST["hasil_audit_lalu"]);
        $fstatus_lha = $comfunc->replacetext ( $_POST ["status_lha"] );
        $flampiran = $_FILES ["attach"] ["name"];

        // spl
        $fspl_no = $comfunc->replacetext ( $_POST ["spl_no"] );
        $fspl_klasifikasi = $comfunc->replacetext ( $_POST ["spl_klasifikasi"] );
        $fspl_lampiran = $comfunc->replacetext ( $_POST ["spl_lampiran"] );
        $fspl_perihal = $comfunc->replacetext ( $_POST ["spl_perihal"] );
        $fspl_tempat_ttd = $comfunc->replacetext ( $_POST ["spl_tempat_ttd"] );
        $fspl_date = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["spl_date"] ) );
        $fspl_yth = $comfunc->replacetext ( $_POST ["spl_yth"] );
        $fspl_dasar = $comfunc->replacetext ( $_POST ["spl_dasar"] );
        $ftemuan_rekomendasi = $comfunc->replacetext ( $_POST ["temuan_rekomendasi"] );
        $fspl_penutup = $comfunc->replacetext ( $_POST ["spl_penutup"] );
        $fspl_tembusan = $comfunc->replacetext ( $_POST ["spl_tembusan"] );
        $fspl_pj = $comfunc->replacetext ( $_POST ["spl_pj"] );

        // tambahan field lha
        $fpendekatan_audit = $comfunc->replacetext ( $_POST ["pendekatan_audit"] );

        // auditee
        $fauditee_id = $comfunc->replacetext ( $_POST ["lha_auditee_id"] );


        $rs_file = $assigns->list_lha_lampiran($fassign_id);
        $z=0;
        while($arr_file = $rs_file->FetchRow()){
            $z++;
            $nama_file = $comfunc->replacetext ( @$_POST ["nama_file_".$z] );
            $assigns->delete_lampiran_kka ($fassign_id, $nama_file);
            $comfunc->HapusFile ( "Upload_KKA", $nama_file);
        }

        $jml_attach = count( $flampiran );
        if ($jml_attach <> 0) {
            for($i=0;$i<$jml_attach;$i++){
                if($flampiran[$i]!="") {
                    $comfunc->UploadFileMulti ( "Upload_LHA", "attach");
                    $assigns->insert_lampiran_lha ( $fassign_id, $flampiran[$i] );
                }
            }
        }
        //$assigns->assign_update_lha ( $fdata_id, $fno_lha, $ftanggal_lha);
        $assigns->lha_update ( $flha_id, $fassign_id, $fno_lha, $ftanggal_lha, $fringkasan, $fdasar_audit, $ftujuan_audit, $fmetodologi_audit, $fstrategi_laporan, $fdata_umum_auditan, $fruang_lingkup, $fbatasan_tanggung_jawab, $fkegiatan, $fhasil_yang_dicapai, $fpenutup, $fhasil_audit_lalu, $fstatus_lha, $fspl_no, $fspl_klasifikasi, $fspl_lampiran, $fspl_perihal, $fspl_tempat_ttd, $fspl_date, $fspl_yth, $fspl_dasar, $ftemuan_rekomendasi, $fspl_penutup, $fspl_tembusan, $fspl_pj, $fpendekatan_audit, $fauditee_id);

        $fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
        $finpektorat_id = $comfunc->replacetext ( $_POST ["inpektorat_id"] );

        $comfunc->hapus_notif($flha_id);
        if ($fstatus_lha != "") {
            if($fstatus_lha==1){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }elseif($fstatus_lha==2){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '1fe7f8b8d0d94d54685cbf6c2483308aebe96229');
                $notif_to_user_id = $get_user_id; //ke daltu
            }elseif($fstatus_lha==3){
                //inpektorat
            }elseif($fstatus_lha==4){ //penugasan selesai
                $assigns->assign_update_status ( $id, 3 );
            }elseif($fstatus_lha==5){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            }elseif($fstatus_lha==6){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }elseif($fstatus_lha==7){
                $get_user_id = $assigns->get_user_by_posisi($fdata_id, '1fe7f8b8d0d94d54685cbf6c2483308aebe96229');
                $notif_to_user_id = $get_user_id; //ke daltu
            }
            if($notif_to_user_id!=""){
                $comfunc->insert_notif($flha_id, $ses_userId, $notif_to_user_id, 'auditassign', '(LHA)...'.$fkomentar, $comfunc->date_db(date('d-m-Y')));
            }
        }
        $ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
        if ($fkomentar != "") {
            $assigns->lha_add_komentar ( $flha_id, $fkomentar, $ftanggal );
        }

        $comfunc->js_alert_act ( 3 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "finding_nha" :
        $_SESSION ['ses_assign_nha_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        ?>
        <script>window.open('main_page.php?method=finding_nha', '_self');</script>
        <?
        break;
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah ST LHA";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assignment_nhas->assignment_nha_viewlist ( $fdata_id );
        $page_title = "Ubah ST LHA";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $auditeksternals->assignment_nha_viewlist ( $fdata_id );
        $page_title = "Rincian ST LHA";
        break;
    case "postadd" :
        $finspektorat_id = $comfunc->replacetext ( $_POST ["inspektorat_id"] );
        $fauditee_id = $comfunc->replacetext ( $_POST ["auditee_id"] );
        $fno_spt = $comfunc->replacetext ( $_POST ["no_spt"] );
        $fpropinsi_id = $comfunc->replacetext ( $_POST ["propinsi_id"] );
        $ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
        $ftanggal_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_awal"] ) );
        $ftanggal_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_akhir"] ) );
        $fketerangan = $comfunc->replacetext ( $_POST ["keterangan"] );
        $fassign_id = $fno_spt;
        if ($finspektorat_id != "" && $fauditee_id != "" && $fno_spt != "" && $ftanggal_awal != "" && $ftanggal_akhir != "") {
            $assignment_nhas->assignment_nha_add ( $finspektorat_id, $fauditee_id, $fno_spt, $fpropinsi_id, $ftahun, $ftanggal_awal, $ftanggal_akhir, $fketerangan, $fassign_id );
            $comfunc->js_alert_act ( 3 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fid = $comfunc->replacetext ( $_POST ["data_id"] );
        $finspektorat_id = $comfunc->replacetext ( $_POST ["inspektorat_id"] );
        $fauditee_id = $comfunc->replacetext ( $_POST ["auditee_id"] );
        $fno_spt = $comfunc->replacetext ( $_POST ["no_spt"] );
        $fpropinsi_id = $comfunc->replacetext ( $_POST ["propinsi_id"] );
        $ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
        $ftanggal_awal = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_awal"] ) );
        $ftanggal_akhir = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["tanggal_akhir"] ) );
        $fketerangan = $comfunc->replacetext ( $_POST ["keterangan"] );
        // get data
        $rs_assign = $assigns->assign_nha_viewlist( $fno_spt );
        $arr_assign = $rs_assign->FetchRow();
        $fassign_id = $arr_assign['assign_surat_id_assign'];
        if ($finspektorat_id != "" && $fauditee_id != "" && $fno_spt != "") {
            $assignment_nhas->assignment_nha_edit ( $fid, $finspektorat_id, $fauditee_id, $fno_spt, $fpropinsi_id, $ftahun, $ftanggal_awal, $ftanggal_akhir, $fketerangan, $fassign_id );
            $comfunc->js_alert_act ( 1 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $assignment_nhas->assignment_nha_delete ( $fdata_id );
        $comfunc->js_alert_act ( 2 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $assignment_nhas->assignment_nha_count ($key_search, $val_search, $key_field);
        $rs = $assignment_nhas->assignment_nha_view_grid ($key_search, $val_search, $key_field, $offset, $num_row );
        $page_title = "Daftar LHA";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>