<?
include_once "_includes/classes/assignment_nha_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/param_class.php";

$assigns = new assignment_nha ( $ses_userId );
$assigns = new assign($ses_userId);
$params  = new param($ses_userId);

$_SESSION ['ses_assign_id'] = $_GET['assign_id'];
$ses_assign_id = $_SESSION ['ses_assign_id'];

$_SESSION ['ses_auditee_id'] = $_GET['auditee_id'];
$ses_auditee_id = $_SESSION ['ses_auditee_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=assign_lha_pagu&assign_id=".$_SESSION['ses_assign_id']."&auditee_id=".$_SESSION['ses_auditee_id']."";
$acc_page_request = "assign_pagu_acc.php";
$list_page_request = "audit_view.php";
// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
    if (is_numeric ( $str_page ) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "lha_view_parrent.php";
$grid = "grid.php";
$gridHeader   = array("Tahun", "DIPA No", "DIPA Tanggal", "Nilai", "Belanja Pegawai", "Belanja Barang", "Belanja Modal", "Realisasi Keuangan Pagu", "Jenis Kegiatan", "Pengelola Anggaran", "Atasan Langsung");
$gridDetail   = array("lha_pagu_tahun", "lha_pagu_no_dipa", "0", "lha_pagu_nilai", "lha_pagu_pegawai", "lha_pagu_barang", "lha_pagu_modal", "lha_pagu_realisasi", "0", "0", "0");
$gridWidth    = array("3", "7", "8", "11", "11", "11", "11", "11", "5", "6", "5");

$key_by = array ("Tahun");
$key_field = array ("lha_pagu_tahun");

$widthAksi = "15";
$iconDetail = "0";
// === end grid ===//

$rs_assign_lha = $assigns->assign_lha_pagu_damu_viewlist ( $ses_assign_id, $ses_auditee_id );
$arr_assign_lha = $rs_assign_lha->FetchRow ();

switch ($_action) {
    case "assign_lha_damu":
        $_SESSION['ses_pagu_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
		$_SESSION ['ses_assign_id'] = $ses_assign_id;
        $_SESSION ['ses_lha_id'] = $ses_lha_id;
        $_SESSION ['ses_auditee_id'] = $ses_auditee_id;
        ?>
        <script>window.open('main_page.php?method=assign_lha_damu', '_self');</script>
        <?
        break;
	case "assign_lha_pengelola_anggaran":
        $_SESSION ['ses_pagu_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
		$_SESSION ['ses_assign_id'] = $ses_assign_id;
        $_SESSION ['ses_auditee_id'] = $ses_auditee_id;
		?>
        <script>window.open('main_page.php?method=assign_lha_pengelola_anggaran', '_self');</script>
        <?
        break;
	case "assign_lha_pejabat":
        $_SESSION ['ses_pagu_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
		$_SESSION ['ses_assign_id'] = $ses_assign_id;
        $_SESSION ['ses_auditee_id'] = $ses_auditee_id;
		?>
        <script>window.open('main_page.php?method=assign_lha_pejabat', '_self');</script>
        <?
        break;	
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah Sumber Dana dan Pagu";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assigns->assign_pagu_viewlist($fdata_id);
        $page_title = "Ubah Sumber Dana dan Pagu";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $assigns->assign_pagu_viewlist($fdata_id);
        $page_title = "Rincian Sumber Dana dan Pagu";
        break;
    case "postadd" :
        $flha_pagu_lha_id     = "";
        $flha_pagu_assign_id  = $ses_assign_id;
        $flha_pagu_tahun          = $comfunc->replacetext($_POST["lha_pagu_tahun"]);
        $flha_pagu_no_dipa        = $comfunc->replacetext($_POST["lha_pagu_no_dipa"]);
        $flha_pagu_nilai      = $comfunc->replacetext($_POST["lha_pagu_nilai"]);
        $flha_pagu_pegawai    = $comfunc->replacetext($_POST["lha_pagu_pegawai"]);
        $flha_pagu_barang = $comfunc->replacetext($_POST["lha_pagu_barang"]);
        $flha_pagu_modal     = $comfunc->replacetext($_POST["lha_pagu_modal"]);
        $flha_pagu_realisasi    = $comfunc->replacetext($_POST["lha_pagu_realisasi"]);
        $flha_pagu_persentase       = $comfunc->replacetext($_POST["lha_pagu_persentase"]);
		$flha_pagu_auditee_id  = $ses_auditee_id;
		$flha_pagu_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_pagu_date"]));
		$flha_pagu_revisi        = $comfunc->replacetext($_POST["lha_pagu_revisi"]);
        $flha_pagu_date_per        = $comfunc->date_db($comfunc->replacetext($_POST["lha_pagu_date_per"]));
        if ($flha_pagu_tahun != "") {
            $assigns->assign_pagu_add($flha_pagu_lha_id, $flha_pagu_assign_id, $flha_pagu_tahun, $flha_pagu_no_dipa, $flha_pagu_nilai, $flha_pagu_pegawai, $flha_pagu_barang, $flha_pagu_modal, $flha_pagu_realisasi, $flha_pagu_persentase, $flha_pagu_auditee_id, $flha_pagu_date, $flha_pagu_revisi, $flha_pagu_date_per);
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fdata_id             = $comfunc->replacetext($_POST["data_id"]);
        $flha_pagu_tahun          = $comfunc->replacetext($_POST["lha_pagu_tahun"]);
        $flha_pagu_no_dipa        = $comfunc->replacetext($_POST["lha_pagu_no_dipa"]);
        $flha_pagu_nilai      = $comfunc->replacetext($_POST["lha_pagu_nilai"]);
        $flha_pagu_pegawai    = $comfunc->replacetext($_POST["lha_pagu_pegawai"]);
        $flha_pagu_barang = $comfunc->replacetext($_POST["lha_pagu_barang"]);
        $flha_pagu_modal     = $comfunc->replacetext($_POST["lha_pagu_modal"]);
        $flha_pagu_realisasi    = $comfunc->replacetext($_POST["lha_pagu_realisasi"]);
        $flha_pagu_persentase       = $comfunc->replacetext($_POST["lha_pagu_persentase"]);
		$flha_pagu_date        = $comfunc->date_db($comfunc->replacetext($_POST["lha_pagu_date"]));
        $flha_pagu_revisi        = $comfunc->replacetext($_POST["lha_pagu_revisi"]);
        $flha_pagu_date_per        = $comfunc->date_db($comfunc->replacetext($_POST["lha_pagu_date_per"]));
        if ($flha_pagu_tahun != "") {
            $assigns->assign_pagu_edit($fdata_id, $flha_pagu_tahun, $flha_pagu_no_dipa, $flha_pagu_nilai, $flha_pagu_pegawai, $flha_pagu_barang, $flha_pagu_modal, $flha_pagu_realisasi, $flha_pagu_persentase, $flha_pagu_date, $flha_pagu_revisi, $flha_pagu_date_per);
            $comfunc->js_alert_act(1);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $assigns->assign_pagu_delete($fdata_id);
        $comfunc->js_alert_act ( 2 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $assigns->assign_pagu_count ( $ses_assign_id, $ses_auditee_id, $key_search, $val_search, $key_field );
        $rs = $assigns->assign_pagu_view_grid ( $ses_assign_id, $ses_auditee_id, $key_search, $val_search, $key_field, $offset, $num_row );
        $page_title = "Daftar Sumber Dana dan Pagu";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>
