<?
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/common.php";
$comfunc = new comfunction ();
$reports       = new report();
$programaudits = new programaudit();
$assigns       = new assign();
$id_assign  = $_REQUEST['id_assign'];
$id_auditee = $_REQUEST['id_auditee'];
$rs_assign = $assigns->assign_viewlist($id_assign);
$arr       = $rs_assign->FetchRow();
$rs_auditee  = $assigns->assign_auditee_viewlist($id_assign, $id_auditee);
$arr_auditee = $rs_auditee->FetchRow();
$intro       = $programaudits->intro_pka_viewlist("", $id_assign, "PKA Pendahuluan");
$intros      = $intro->FetchRow();
$katim       = $reports->get_anggota($id_assign, $id_auditee, "KT");
$dalnis      = $reports->get_anggota($id_assign, $id_auditee, "PT");
$dalmut      = $reports->get_anggota($id_assign, $id_auditee, "PM");
$assign_nama_auditee = $arr_auditee['auditee_name'];
header("Content-Type: application/msword");
header("Content-Type: image/jpg");
header('Content-Disposition: attachment; filename=PKA PENDAHULUAN '.str_replace(',', '', $assign_nama_auditee).'.doc');
header("Pragma: no-cache");
header("Expires: 0");
define("INBOARD", false);
?>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
    <style>
    body{
    width:210mm;
    font-family:Arial;
    font-size: 12;
    }

    </style>
    <body>
        <table border='0' width="100%" cellspacing='0' cellpadding="0">
            <tr>
                <td colspan="2" style="border-top:1px solid; border-left: 1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid; border-right: 1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8" style="border-left: 1px solid; border-right: 1px solid">
                    <center>
                    <img src="http://epengawasan.kemenhub.go.id/dev/images/kka-logo1.png" alt="kka_logo" border="0">
                    <br>
                    <strong>KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA<br>INSPEKTORAT JENDERAL</strong>
                    <br>
                    ================================================================
                    </center>
                    <br>
                </td>
            </tr>
           <tr>
                <td width="5%" style="border-top:1px solid; border-left: 1px solid">&nbsp;</td>
                <td width="5%" style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td style="border-top:1px solid">&nbsp;</td>
                <td width="10%" style="border-top:1px solid">No PKA</td>
                <td align="center" style="border-top:1px solid">:</td>
                <td style="border-top:1px solid"><?= $intros['pka_intro_no_pka'] ?></td>
                <td style="border-top:1px solid; border-right: 1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid">Nama Auditan</td>
                <td align="center">:</td>
                <td><?= $arr_auditee['auditee_name']; ?></td>
                <td>Ref. PKA No</td>
                <td align="center">:</td>
                <td>-</td>
                <td style="border-right: 1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid">Sasaran Audit</td>
                <td align="center">:</td>
                <td><?= $arr['audit_type_name']; ?></td>
                <td>Disusun Oleh</td>
                <td align="center">:</td>
                <td><?= $katim['nama'] ?></td>
                <td style="border-right: 1px solid">Paraf :</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Tanggal</td>
                <td align="center">:</td>
                <td><?=$comfunc->dateIndoLengkap($intros['pka_intro_arranged_date'])?></td>
                <td style="border-right: 1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid">Periode Audit</td>
                <td align="center">:</td>
                <td>TA. <?= $arr['assign_tahun']-1 ?>-<?= $arr['assign_tahun'] ?></td>
                <td>Direviu Oleh</td>
                <td align="center">:</td>
                <td><?= $dalnis['nama'] ?></td>
                <td style="border-right: 1px solid">Paraf :</td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid; border-left: 1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">Tanggal</td>
                <td style="border-bottom:1px solid" align="center">:</td>
                <td style="border-bottom:1px solid"><?=$comfunc->dateIndoLengkap($intros['pka_intro_reviewed_date'])?></td>
                <td style="border-bottom:1px solid; border-right: 1px solid;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8" align="center" style="border-left: 1px solid;border-right: 1px solid"><br><strong>PROGRAM KERJA AUDIT PENDAHULUAN</strong></td>
            </tr>
             <tr>
                <td colspan="8" style="border-left: 1px solid;border-right: 1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8" style="border-left: 1px solid;border-right: 1px solid"><strong>Tujuan Audit :</strong> <?=$intros['pka_intro_tujuan']?></td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid; border-left: 1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid">&nbsp;</td>
                <td style="border-bottom:1px solid; border-right: 1px solid">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8"><strong>&nbsp;&nbsp;A. Langkah Kerja :</strong></td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
        </table>
        <table class="table_risk" cellspacing='0' cellpadding="0" style="vertical-align: top;">
            <tr align="center">
               <th width="5%" style="border-bottom:1px solid; border-top: 1px solid; border-left: 1px solid" rowspan="2">No.</th>
                <th width="50%" rowspan="2" colspan="2" style="border-bottom:1px solid; border-right: 1px solid; border-top: 1px solid; border-left: 1px solid">Langkah Kerja Audit</th>
                <th width="16%" colspan="2" style="border-bottom:1px solid; border-top: 1px solid; ">Dilaksanakan Oleh</th>
                <th width="16%" colspan="2" style="border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid">Waktu yang Diperlukan</th>
                <th width="9%" rowspan="2" style="border-bottom:1px solid; border-top: 1px solid; border-left: 1px solid">Nomor KKA</th>
                <th width="7%" rowspan="2" style="border-bottom:1px solid; border-right: 1px solid; border-top: 1px solid; border-left: 1px solid">Catatan</th>
            </tr>
            <tr align="center">
                <th style="border-bottom:1px solid;">Rencana</th>
                <th style="border-bottom:1px solid; border-left: 1px solid;">Realisasi</th>
                <th style="border-bottom:1px solid; border-left: 1px solid; border-right: 1px solid;">Rencana</th>
                <th style="border-bottom:1px solid;">Realisasi</th>
            </tr>
            <tbody>
                <?php
                $no_aspek = "1";
                $rs_aspek = $programaudits->get_assign_aspek("", $arr['assign_id'], $arr_auditee['assign_auditee_id_auditee'], "PKA Pendahuluan");
                while ($arr_aspek = $rs_aspek->FetchRow()) {
                ?>
                <tr>
                    <td style="border-bottom:1px solid; border-left: 1px solid" align="center"><b><?=$comfunc->KonDecRomawi($no_aspek)?></b></td>
                    <td style="border-bottom:1px solid; border-left: 1px solid; border-right: 1px solid" colspan="2"><b><?=$arr_aspek['aspek_name']?></b></td>
                    <td style="border-bottom:1px solid;">&nbsp;</td>
                    <td style="border-bottom:1px solid; border-left: 1px solid;">&nbsp;</td>
                    <td style="border-bottom:1px solid; border-left: 1px solid; border-right: 1px solid">&nbsp;</td>
                    <td style="border-bottom:1px solid;">&nbsp;</td>
                    <td style="border-bottom:1px solid; border-left: 1px solid;">&nbsp;</td>
                    <td style="border-bottom:1px solid; border-right: 1px solid; border-left: 1px solid">&nbsp;</td>
                </tr>
                <?
                $no         = "0";
                $rs_program = $reports->assign_program_audit_list($arr_aspek['aspek_id'], $arr['assign_id'], $arr_auditee['assign_auditee_id_auditee'], "PKA Pendahuluan");
                while ($arr_program = $rs_program->FetchRow()) {
                $no++;
                ?>
                <tr>
                    <td valign="top" style="border-bottom:1px solid; border-left: 1px solid" align="center"><?= $no ?></td>
                    <td valign="top" style="border-bottom:1px solid; border-right: 1px solid; border-left: 1px solid" colspan="2"><?=$arr_program['ref_program_procedure']?></td>
                    <td valign="top" style="border-bottom:1px solid;"><?=$arr_program['auditor_name']?></td>
                    <td valign="top" style="border-bottom:1px solid; border-left: 1px solid"><?=$arr_program['auditor_name']?></td>
                    <td valign="top" style="border-bottom:1px solid; border-right: 1px solid; border-left: 1px solid" align="center"><?=$arr_program['program_jam']?> Jam</td>
                    <td valign="top" style="border-bottom:1px solid;" align="center"><?=(null != $arr_program['kertas_kerja_jam']) ? $arr_program['kertas_kerja_jam'] . " Jam" : ""?></td>
                    <td valign="top" style="border-bottom:1px solid; border-left: 1px solid"><?=$arr_program['kertas_kerja_no']?></td>
                    <td valign="top" style="border-bottom:1px solid; border-right: 1px solid; border-left: 1px solid"><?=$arr_program['kertas_kerja_desc']?></td>
                </tr>
                <?
                }
                $no_aspek++;
                }
                ?>
            </tbody>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td style="border-style: none; border-bottom-style: none" colspan="4">&nbsp;&nbsp;<b>Keterangan :</b></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Jakarta, <?=$comfunc->dateIndoLengkap($intros['pka_intro_arranged_date'])?></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <!--            <tr>-->
            <!--                <td>Direvui</td>-->
            <!--                <td>&nbsp;</td>-->
            <!--            </tr>-->
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="10%">&nbsp;</td><td width="30%">Disetujui</td><td width="30%">Direviu</td><td width="30%">Disusun oleh:</td>
            </tr>
            <tr>
                <td>&nbsp;</td><td>tanggal: <?=$comfunc->dateIndoLengkap($intros['pka_intro_approved_date'])?></td><td>tanggal: <?=$comfunc->dateIndoLengkap($intros['pka_intro_reviewed_date'])?></td><td>KETUA TIM,</td>
            </tr>
            <tr>
                <td></td><td>PENGENDALI MUTU,</td><td>PENGENDALI TEKNIS,</td><td></td>
            </tr>
            <?foreach (range(1, 5) as $item): ?>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <?endforeach;?>
            <tr>
                <td></td><td><u><?=$dalmut['nama']?></u><br><?=$dalmut['jabatan']?></td><td><u><?=$dalnis['nama']?></u><br><?=$dalnis['jabatan']?></td><td><u><?=$katim['nama']?></u><br><?=$katim['jabatan']?></td>
            </tr>
        </table>
    </body>
</html>