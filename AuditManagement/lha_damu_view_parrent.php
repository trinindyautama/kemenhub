<?

$rs_lha = $assigns->assign_lha_damu_viewlist ( $ses_lha_id );
$arr_lha = $rs_lha->FetchRow ();

?>
<article class="module width_3_quarter">
    <fieldset>
        <table class="view_parrent">
            <tr>
                <td width="150">Nomor LHA</td>
                <td>:</td>
                <td><?=$arr_lha['lha_no']?></td>
            </tr>
            <tr>
                <td width="150">Tanggal LHA</td>
                <td>:</td>
                <td><?=$comfunc->dateIndo($arr_lha['lha_date'])?></td>
            </tr>
            <tr>
                <td width="150">Auditi</td>
                <td>:</td>
                <td><?=$arr_lha['auditee_name']?></td>
            </tr>
        </table>
    </fieldset>
</article>