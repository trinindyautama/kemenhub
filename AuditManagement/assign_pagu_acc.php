<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <header>
            <h3 class="tabs_involved"><?=$page_title?></h3>
        </header>
        <form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
            <?
            switch ($_action) {
                case "getadd" :
                    ?>
                    <fieldset class="hr">
                            <label class="span2">Tahun</label>
                            <select class="span1" name="lha_pagu_tahun" id="lha_pagu_tahun">
                                <option value="">Pilih Satu</option>
                                <?
                                $thn = date ( "Y" ) - 5;
                                for($i = 1; $i <= 11; $i ++) {
                                    ?>
                                    <option value="<?=$thn?>" <? if($thn==date("Y")) echo "selected";?>><?=$thn?></option>
                                    <?
                                    $thn ++;
                                }
                                ?>
                            </select><span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">DIPA No</label>
                        <input class="span2" type="text" name="lha_pagu_no_dipa" id="lha_pagu_no_dipa">
                    </fieldset>
					<fieldset class="hr">
                        <label class="span2">DIPA Tanggal</label>
                        <input class="span2" type="text" name="lha_pagu_date" id="lha_pagu_date">
                        <label class="span1">Revisi ke-</label>
                        <input class="span2" type="text" name="lha_pagu_revisi" id="lha_pagu_revisi" style="height: 20px;" maxlength="3">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nilai</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_nilai" id="lha_pagu_nilai">
                    </fieldset><fieldset class="hr">
                        <label class="span2">Belanja Pegawai</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_pegawai" id="lha_pagu_pegawai">
                    </fieldset><fieldset class="hr">
                        <label class="span2">Belanja Barang</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_barang" id="lha_pagu_barang">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Belanja Modal</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_modal" id="lha_pagu_modal">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Realisasi keuangan</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_realisasi" id="lha_pagu_realisasi">
                        <label class="span0">&nbsp;</label>
                        <input class="span2" type="text" name="lha_pagu_persentase" id="lha_pagu_persentase" style="width: 50px;" maxlength="6" readonly>
                        <label class="span0">%</label>
                        <label class="span1">Per Tanggal</label>
                        <input class="span2" type="text" name="lha_pagu_date_per" id="lha_pagu_date_per">
                    </fieldset>
                    <?
                    break;
                case "getedit" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Tahun</label>
                        <select class="span1" name="lha_pagu_tahun" id="lha_pagu_tahun">
                            <option value="">Pilih Satu</option>
                            <?
                            $thn = date ( "Y" ) - 5;
                            for($i = 1; $i <= 11; $i ++) {
                                ?>s
                                <option value="<?=$thn?>"
                                    <? if($thn==$arr['lha_pagu_tahun']) echo "selected";?>><?=$thn?></option>
                                <?
                                $thn ++;
                            }
                            ?>
                        </select><span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">DIPA No</label>
                        <input class="span2" type="text" name="lha_pagu_no_dipa" id="lha_pagu_no_dipa" value="<?=$arr['lha_pagu_no_dipa']?>">
                    </fieldset>
					<fieldset class="hr">
                        <label class="span2">DIPA Tanggal</label>
                        <input class="span2" type="text" name="lha_pagu_date" id="lha_pagu_date" value="<?=$comfunc->dateIndo($arr['lha_pagu_date'])?>">
                        <label class="span1">Revisi ke-</label>
                        <input class="span2" type="text" name="lha_pagu_revisi" id="lha_pagu_revisi" value="<?=$arr['lha_pagu_revisi']?>" style="height: 20px" maxlength="3">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nilai</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_nilai" id="lha_pagu_nilai" value="<?=$arr['lha_pagu_nilai']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Belanja Pegawai</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_pegawai" id="lha_pagu_pegawai" value="<?=$arr['lha_pagu_pegawai']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Belanja Barang</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_barang" id="lha_pagu_barang" value="<?=$arr['lha_pagu_barang']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Belanja Modal</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_modal" id="lha_pagu_modal" value="<?=$arr['lha_pagu_modal']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Realisasi keuangan</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_pagu_realisasi" id="lha_pagu_realisasi" value="<?=$arr['lha_pagu_realisasi']?>">
                        <label class="span0">&nbsp;</label>
                        <input class="span0" type="text" name="lha_pagu_persentase" id="lha_pagu_persentase" value="<?=$arr['lha_pagu_persentase']?>" style="width: 50px;" maxlength="5" readonly>
                        <label class="span0">%</label>
                        <label class="span1">Per Tanggal</label>
                        <input class="span2" type="text" name="lha_pagu_date_per" id="lha_pagu_date_per" value="<?=$comfunc->dateIndo($arr['lha_pagu_date_per'])?>">
                    </fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['lha_pagu_id']?>">
                    <?
                    break;
                case "getdetail" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <table class="view_parrent">
                            <tr>
                                <td>Tahun</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_tahun']?></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_pekerjaan']?></td>
                            </tr>
                            <tr>
                                <td>Nomor Kontrak/SPK</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_no_kontrak']?></td>
                                <td>&nbsp;&nbsp;&nbsp;<b>Tanggal Kontrak/SPK : </b> <?=$comfunc->dateIndo($arr['lha_damu_date'])?></td>
                            </tr>
                            <tr>
                                <td>Nilai Kontrak/SPK</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_nilai']?></td>
                            </tr>
                            <tr>
                                <td>Nama Penyedia barang/Jasa</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_nama']?></td>
                            </tr>
                            <tr>
                                <td>Jangka Waktu Pelaksanaan</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr['lha_damu_start_date'])?> s.d <?=$comfunc->dateIndo($arr['lha_damu_end_date'])?></td>
                            </tr>
                            <tr>
                                <td>Realisasi Fisik (%)</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_fisik']?>%</td>
                            </tr>
                            <tr>
                                <td>Realisasi Keuangan (%)</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_keuangan']?></td>
                            </tr>
                            <tr>
                                <td>Tambahan Nomor Kontrak/SPK</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['lha_damu_no_kontrak2'])?></td>
                            </tr>
                        </table>
                    </fieldset>

                    <?
                    break;
            }
            ?>
            <fieldset>
                <center>
                    <input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
                    <input type="submit" class="blue_btn" value="Simpan">
                </center>
                <input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
            </fieldset>
        </form>
    </article>
</section>
<script>
    $(document).ready(function() {
        $("#lha_pagu_revisi").attr("placeholder", "Masukkan Angka").change(function(e) {
            var txt = /[a-z]/gi.test($(this).val());
            if (txt) {
                $(this).val("").attr("placeholder", "Hanya Bisa Masukkan Angka")
            };
        });
    });

    $(function() {
        $("#validation-form").validate({
            rules: {
                lha_pagu_tahun: "required"
            },
            messages: {
                lha_pagu_tahun: "Silahkan Masukkan Tahun!"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

    $(function(){

        $('#lha_pagu_nilai').on('input', function() {
            calculate();
        });
        $('#lha_pagu_realisasi').on('input', function() {
            calculate();
        });
        function calculate(){
            var pPos = parseInt($('#lha_pagu_nilai').val());
            var pEarned = parseInt($('#lha_pagu_realisasi').val());
            var perc="";
            if(isNaN(pPos) || isNaN(pEarned)){
                perc="";
            }else{
                perc = ((pEarned/pPos)*100).toFixed(2);
            }

            $('#lha_pagu_persentase').val(perc);
        }

    });

$("#lha_pagu_date, #lha_pagu_date_per").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });
</script>