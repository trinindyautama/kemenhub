<?php
include_once "_includes/classes/report_class.php";
$reports = new report ( $ses_userId );
$rs_auditee = $assigns->assign_auditee_viewlist ( $arr['assign_id'], $auditee_id );
?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<?
		while($arr_auditee = $rs_auditee->FetchRow()){
		$katim = $reports->get_anggota($arr['assign_id'], $arr_auditee['assign_auditee_id_auditee'], 'KT');
		$dalnis = $reports->get_anggota($arr['assign_id'], $arr_auditee['assign_auditee_id_auditee'], 'PT');
		?>
		<table border='0' class="table_report" cellspacing='0' cellpadding="0">
			<tr>
				<td colspan="3">
					<center>
					<img src="images/kka-logo1.png" width="55" height="48" style="width:55pt;height:48pt;" /><br>
					INSPEKTORAT KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA
					</center><br>
				</td>
			</tr>
			<tr>
				<td width="10%"><strong>NAMA AUDITAN</strong></td>
				<td width="5%" align="center"><strong>:</strong></td>
				<td><strong><?=strtoupper($arr_auditee['auditee_name']);?></strong></td>
			</tr>
			<tr>
				<td><strong>SASARAN AUDIT</strong></td>
				<td align="center"><strong>:</strong></td>
				<td><strong><?=strtoupper($arr['audit_type_name']);?></strong></td>
			</tr>
			<tr>
				<td><strong>PERIODE AUDIT</strong></td>
				<td align="center"><strong>:</strong></td>
				<td><strong>TA <?=$arr['assign_tahun']-1?>-<?=$arr['assign_tahun']?></strong></td>
			</tr>
			<tr>
				<td colspan="3" align="center"><br><strong>IKHTISAR TEMUAN HASIL AUDIT</strong></td>
			</tr>
		</table>
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<th width="5%">NO</th>
				<th width="62%">URAIAN TEMUAN HASIL AUDIT</th>
				<th width="33%">TANGGAPAN AUDITAN</th>
			</tr>
			<tr>
				<?php foreach (range(1,3) as $t): ?>
				<td align=" center"><?= $t ?></td>
				<?php endforeach ?>
			</tr>
			<tr>
				<td align="center"><strong>I.</strong></td>
				<td><strong><?=strtoupper($arr['audit_type_name'])?></strong>
				</td>
				<td></td>
			</tr>
			<?php
			$no_aspek="A";
			$rs_aspek = $findings->get_aspek_by_temuan($arr['assign_id'], $arr_auditee['assign_auditee_id_auditee']);
			$no_temuan=0;
			while($arr_aspek = $rs_aspek->FetchRow()){
			?>
			<tr>
				<td align="center"><strong><?=$no_aspek?>.</strong></td>
				<td><strong><?=$arr_aspek['aspek_name']?></strong></td>
				<td>&nbsp;</td>
			</tr>
			<?
			$rs_temuan = $findings->get_list_temuan_by_aspek($arr_aspek['aspek_id'], $arr['assign_id'], $arr_auditee['assign_auditee_id_auditee']);
			foreach($rs_temuan->GetArray() as $arr_temuan){
			$no_temuan++;
			?>
			<tr>
				<td align="center"><strong><?php echo $no_temuan; ?>.</strong></td>
				<td><strong><?=$arr_temuan['finding_judul']?></strong></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><?=$comfunc->text_show($arr_temuan['finding_kondisi'])?><br><br><?=$comfunc->text_show($arr_temuan['finding_kriteria'])?><br><br><?=$comfunc->text_show($arr_temuan['finding_sebab'])?><br><br><?=$comfunc->text_show($arr_temuan['finding_akibat'])?><br><br>Rekomendasi: <br><br><?=$comfunc->text_show($arr_temuan['rekomendasi_desc'])?></td>
				<td><?=$arr_temuan['finding_tanggapan_auditee']?></td>
			</tr>
			<?
			}
			$no_aspek++;
			}
			?>
		</table>
		<?
		}
		?>
		<br>
		<table border='0' class="table_report" cellspacing='0' cellpadding="0">
			<tr>
				<td width="33%">Kepala <?=$arr_auditee['auditee_name'];?></td>
				<td width="33%">Pengendali Teknis</td>
				<td width="33%">Ketua Tim</td>
			</tr>
			<tr>
				<td>&nbsp;<br><br><br></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><?=$dalnis['nama']?></td>
				<td><?=$katim['nama']?></td>
			</tr>
		</table>
		<fieldset>
			<center>
			<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'">
			<input type="button" class="blue_btn" value="ms-word" onClick="window.open('AuditManagement/ikhtisar_temuan_print.php?id_assign=<?=$arr['assign_id']?>&id_auditee=<?=$arr_auditee['assign_auditee_id_auditee']?>','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
			</center>
		</fieldset>
	</article>
</section>