<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link href="css/responsive-tabs.css" rel="stylesheet" type="text/css" />
<script src="js/responsive-tabs.js" type="text/javascript"></script>
<section id="main" class="column">
    <article class="module width_3_quarter">
        <header>
            <h3 class="tabs_involved"><?=$page_title?></h3>
        </header>
        <form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
            <?
            switch ($_action) {
                case "getadd" :
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Pekerjaan</label>
                        <textarea rows="10" cols="50" class="span2" type="text" name="lha_damu_pekerjaan" id="lha_damu_pekerjaan" style="margin: 0px 10px; height: 87px; width: 296px;"></textarea>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nomor Kontrak/SPK</label>
                        <input class="span2" type="text" name="lha_damu_no_kontrak" id="lha_damu_no_kontrak">
                        <label class="span2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanggal Kontrak/SPK</label>
                        <input type="text" class="span1" name="lha_damu_date" id="lha_damu_date">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nilai Kontrak/SPK</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_damu_nilai" id="lha_damu_nilai">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nama Penyedia Barang/Jasa</label>
                        <input class="span2" type="text" name="lha_damu_nama" id="lha_damu_nama">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Jangka Waktu Pelaksanaan</label>
                        <input type="text" class="span1" name="lha_damu_start_date" id="lha_damu_start_date">
                        <label class="span0">s/d</label>
                        <input type="text" class="span1" name="lha_damu_end_date" id="lha_damu_end_date">
                        <label class="span0"></label>
                        <input class="span2" type="text" name="lha_damu_jangka" id="lha_damu_jangka" style="width: 50px;" readonly><label class="span1">hari kalender</label>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Realisasi Fisik (%)</label>
                        <input class="span2" type="text" name="lha_damu_fisik" id="lha_damu_fisik" style="width: 50px;" maxlength="5"><label class="span0">%</label>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Realisasi Keuangan (%)</label>
                        <input class="span2" type="text" name="lha_damu_persentase" id="lha_damu_persentase" style="width: 50px;" maxlength="5">
                        <label class="span0">%</label>
                        <label class="span">&nbsp;&nbsp;&nbsp;Rp</label>
                        <input class="span2" type="text" name="lha_damu_keuangan" id="lha_damu_keuangan" readonly>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Tambahan Nomor Kontrak/SPK</label>
                        <textarea class="span2" rows="10" cols="50" class="span2" type="text" name="lha_damu_no_kontrak2" id="lha_damu_no_kontrak2" style="margin: 0px 10px; height: 87px; width: 296px;"></textarea>
                    </fieldset>
                    <?
                    break;
                case "getedit" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <label class="span2">Pekerjaan</label>
                        <textarea class="span2" rows="10" cols="50" type="text" name="lha_damu_pekerjaan" id="lha_damu_pekerjaan" style="margin: 0px 10px; height: 87px; width: 296px;"><?=$arr['lha_damu_pekerjaan']?></textarea>
                        <span class="mandatory">*</span>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nomor Kontrak/SPK</label>
                        <input class="span2" type="text" name="lha_damu_no_kontrak" id="lha_damu_no_kontrak" value="<?=$arr['lha_damu_no_kontrak']?>">
                        <label class="span2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanggal Kontrak/SPK</label>
                        <input type="text" class="span1" name="lha_damu_date" id="lha_damu_date" value="<?=$comfunc->dateIndo($arr['lha_damu_date'])?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nilai Kontrak/SPK</label>
                        <label class="span0">Rp</label>
                        <input class="span2" type="text" name="lha_damu_nilai" id="lha_damu_nilai" value="<?=$arr['lha_damu_nilai']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Nama Penyedia barang/Jasa</label>
                        <input class="span2" type="text" name="lha_damu_nama" id="lha_damu_nama" value="<?=$arr['lha_damu_nama']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Jangka Waktu Pelaksanaan</label>
                        <input type="text" class="span1" name="lha_damu_start_date" id="lha_damu_start_date" value="<?=$comfunc->dateIndo($arr['lha_damu_start_date'])?>">
                        <label class="span0">s/d</label>
                        <input type="text" class="span1" name="lha_damu_end_date" id="lha_damu_end_date" value="<?=$comfunc->dateIndo($arr['lha_damu_end_date'])?>">
                        <label class="span0"></label>
                        <input class="span2" type="text" name="lha_damu_jangka" id="lha_damu_jangka" style="width: 50px;" value="<?=$arr['lha_damu_jangka']?>" readonly><label class="span1">hari kalender</label>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Realisasi Fisik (%)</label>
                        <input class="span2" type="text" name="lha_damu_fisik" id="lha_damu_fisik" style="width: 50px;" value="<?=$arr['lha_damu_fisik']?>"><label class="span0">%</label>
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Realisasi Keuangan (%)</label>
                        <input class="span2" type="text" name="lha_damu_persentase" id="lha_damu_persentase" style="width: 50px;" maxlength="5" value="<?=$arr['lha_damu_persentase']?>">
                        <label class="span0">%</label>
                        <label class="span">&nbsp;&nbsp;&nbsp;Rp</label>
                        <input class="span2" type="text" name="lha_damu_keuangan" id="lha_damu_keuangan" readonly value="<?=$arr['lha_damu_keuangan']?>">
                    </fieldset>
                    <fieldset class="hr">
                        <label class="span2">Tambahan Nomor Kontrak/SPK</label>
                        <textarea class="span2" rows="10" cols="50" class="span2" type="text" name="lha_damu_no_kontrak2" id="lha_damu_no_kontrak2" style="margin: 0px 10px; height: 87px; width: 296px;"><?=$arr['lha_damu_no_kontrak2']?></textarea>
                    </fieldset>
                    <input type="hidden" name="data_id" value="<?=$arr['lha_damu_id']?>">
                    <?
                    break;
                case "getdetail" :
                    $arr = $rs->FetchRow ();
                    ?>
                    <fieldset class="hr">
                        <table class="view_parrent">
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_pekerjaan']?></td>
                            </tr>
                            <tr>
                                <td>Nomor Kontrak/SPK</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_no_kontrak']?></td>
                                <td>&nbsp;&nbsp;&nbsp;<b>Tanggal Kontrak/SPK : </b> <?=$comfunc->dateIndo($arr['lha_damu_date'])?></td>
                            </tr>
                            <tr>
                                <td>Nilai Kontrak/SPK</td>
                                <td>:</td>
                                <td>Rp. <?=$comfunc->format_uang($arr['lha_damu_nilai'])?></td>
                            </tr>
                            <tr>
                                <td>Nama Penyedia barang/Jasa</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_nama']?></td>
                            </tr>
                            <tr>
                                <td>Jangka Waktu Pelaksanaan</td>
                                <td>:</td>
                                <td><?=$comfunc->dateIndo($arr['lha_damu_start_date'])?> s.d <?=$comfunc->dateIndo($arr['lha_damu_end_date'])?></td>
                            </tr>
                            <tr>
                                <td>Realisasi Fisik (%)</td>
                                <td>:</td>
                                <td><?=$arr['lha_damu_fisik']?>%</td>
                            </tr>
                            <tr>
                                <td>Realisasi Keuangan (%)</td>
                                <td>:</td>
                                <td>Rp. <?=$comfunc->format_uang($arr['lha_damu_keuangan'])?>&nbsp;&nbsp;(<?=$arr['lha_damu_persentase']?>%)</td>
                            </tr>
                            <tr>
                                <td>Tambahan Nomor Kontrak/SPK</td>
                                <td>:</td>
                                <td><?=$comfunc->text_show($arr['lha_damu_no_kontrak2'])?></td>
                            </tr>
                        </table>
                    </fieldset>

                    <?
                    break;
            }
            ?>
            <fieldset>
                <center>
                    <input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp;
                    <input type="submit" class="blue_btn" value="Simpan">
                </center>
                <input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
            </fieldset>
        </form>
    </article>
</section>
<script>
    $("#lha_damu_start_date").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true,
    });

    $("#lha_damu_end_date").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });

    $("#lha_damu_date").datepicker({
        dateFormat: 'dd-mm-yy',
        nextText: "",
        prevText: "",
        changeYear: true,
        changeMonth: true
    });


    $(function() {
        $("#validation-form").validate({
            rules: {
                lha_damu_pekerjaan: "required"
            },
            messages: {
                lha_damu_pekerjaan: "Silahkan Masukkan Pekerjaan!"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

    window.onload=function(){
        $('#lha_damu_end_date').on('change', function() {
            $(function() {
                $( "#lha_damu_start_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
                $( "#lha_damu_end_date" ).datepicker({ dateFormat: 'dd-mm-yy' });

                var start = $('#lha_damu_start_date').datepicker('getDate');
                var end   = $('#lha_damu_end_date').datepicker('getDate');
                var days   = (end - start)/1000/60/60/24+1;
                $('#lha_damu_jangka').val(days);
            });
        });
    };
	
	$(function(){

        $('#lha_damu_nilai').on('input', function() {
            calculate();
        });
        $('#lha_damu_persentase').on('input', function() {
            calculate();
        });
        function calculate(){
            var pPos = parseInt($('#lha_damu_nilai').val());
            var pEarned = parseInt($('#lha_damu_persentase').val());
            var perc="";
            if(isNaN(pPos) || isNaN(pEarned)){
                perc="";
            }else{
                perc = ((pPos*pEarned)/100);
            }

            $('#lha_damu_keuangan').val(perc);
        }

    });
</script>
