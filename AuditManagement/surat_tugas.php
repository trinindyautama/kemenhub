<?php 
$arr = $rs->FetchRow();
?>
<section id="main" class="column">
<article class="module width_3_quarter">
	<header>
		<h3 class="tabs_involved"><?=$page_title?></h3>
	</header>
		<table align="center" width="100%">
			<tr>
				<td align="center" rowspan="2"><img src="images/logo-menhub.png" width="100px" /></td>
				<td align="center">
					<strong>
						<font size="5">KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA</font><br>
						<font size="6">INSPEKTORAT JENDERAL</font>
					</strong>
				</td>
			</tr>
			<tr>
				<td align="center">
					Jl. Merdeka Barat 8 Jakarta 10110<br>
					Telp : (021) 3456919 / 65866231, Fax : (021) 3813154
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
		</table>
		<table align="center" width="95%">
			<tr>
				<td colspan="7" align="center"><strong><u>SURAT PERINTAH TUGAS<u></strong></td>
			</tr>
			<tr>
				<td colspan="7" align="center">NOMOR : <?=$arr['assign_surat_no']?></td>
			</tr>
			<tr>
				<td colspan="7" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td width="10%">&nbsp;</td>
				<td valign="top" width="15%">Dasar</td>
				<td align="center" valign="top" width="5%">:</td>
				<td colspan="4"><?=$comfunc->text_show($arr ['assign_dasar'])?></td>
			</tr>
			<tr>
				<td colspan="7" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="7" align="center"><strong>MEMERINTAHKAN</strong></td>
			</tr>
			<tr>
				<td colspan="7" align="center">&nbsp;</td>
			</tr>
		<?
		if($arr['audit_type_opsi']=='1'){
		?>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Kepada</td>
				<td align="center" valign="top">:</td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="6">
					<table border="1" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">Nama</th>
							<th rowspan="2">Gol.</th>
							<th rowspan="2">Peran</th>
							<th colspan="3">Lama Tugas (Hari Kerja)</th>
						</tr>
						<tr>
							<th>Persiapan</th>
							<th>Pelaksanaan</th>
							<th>Pelaporan</th>
						</tr>
			<?	
				$no=0;
				$rs_auditor = $assigns->view_auditor_assign ( $arr ['assign_surat_id_assign']);
				while ( $arr_auditor = $rs_auditor->FetchRow () ) {
					$no++
			?>
						<tr>
							<td align="center"><?=$no?></td>
							<td><?=$arr_auditor ['auditor_name']?></td>
							<td align="center"><?=$arr_auditor ['pangkat_name']?></td>
							<td align="center"><?=$arr_auditor ['posisi_name']?></td>
							<td align="center"><?=$arr_auditor['assign_auditor_prepday']?></td>
							<td align="center"><?=$arr_auditor['assign_auditor_execday']?></td>
							<td align="center"><?=$arr_auditor['assign_auditor_reportday']?></td>
						</tr>
			<?php 
				}
			?>
					</table>
				</td>
			</tr>
		<?
		} else if ($arr['audit_type_opsi']=='2'){
		?>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Kepada</td>
				<td align="center" valign="top">:</td>
				<td colspan="4">				
					<table border="1" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Gol.</th>
							<th>Jabatan</th>
						</tr>
			<?
				$no=0;
				$rs_auditor = $assigns->view_auditor_assign ( $arr ['assign_surat_id_assign']);
				while ( $arr_auditor = $rs_auditor->FetchRow () ) {
					$no++
			?>
						<tr>
							<td align="center"><?=$no?></td>
							<td><?=$arr_auditor ['auditor_name']?></td>
							<td align="center"><?=$arr_auditor ['pangkat_name']?></td>
							<td align="center"><?=$arr_auditor ['jenis_jabatan_sub']?></td>
						</tr>
			<?
				}
			?>
					</table>
				</td>
			</tr>
		<?
		} else if($arr['audit_type_opsi']=='3'){
		?>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Kepada</td>
				<td align="center" valign="top">:</td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="6">
					<table border="1" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Gol.</th>
							<th>Jabatan</th>
							<th>Lama Tugas (Hari Kerja)</th>
							<th>Ruang Lingkup</th>
						</tr>
			<?	
				$no=0;
				$rs_auditor = $assigns->view_auditor_assign ( $arr ['assign_surat_id_assign']);
				while ( $arr_auditor = $rs_auditor->FetchRow () ) {
					$no++
			?>
						<tr>
							<td align="center"><?=$no?></td>
							<td><?=$arr_auditor ['auditor_name']?></td>
							<td align="center"><?=$arr_auditor ['pangkat_name']?></td>
							<td align="center"><?=$arr_auditor ['jenis_jabatan']?></td>
							<td align="center"><?=$arr ['assign_hari']?></td>
							<td align="center"><?=$arr_auditor ['posisi_name']?></td>
						</tr>
			<?php 
				}
			?>
					</table>
				</td>
			</tr>
		<?
		}
		?>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Untuk</td>
				<td align="center" valign="top">:</td>
				<td colspan="4"><?=$comfunc->text_show($arr['assign_kegiatan'])?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Mulai Tanggal</td>
				<td align="center" valign="top">:</td>
				<td colspan="4"><?=$comfunc->dateIndoLengkap($arr['assign_start_date'])?> s.d <?=$comfunc->dateIndoLengkap($arr['assign_end_date'])?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Pendanaan</td>
				<td align="center" valign="top">:</td>
				<td colspan="4"><?=$comfunc->text_show($arr['assign_pendanaan'])?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td valign="top">Keterangan lain</td>
				<td align="center" valign="top">:</td>
				<td colspan="4"><?=$comfunc->text_show($arr['assign_keterangan'])?></td>
			</tr>
		</table>
		<br>
		<table align="center" width="90%">
			<tr>
				<td width="60%">&nbsp;</td>
				<td>
					Ditepakan di Jakarta<br>
					Pada Tanggal <?=$comfunc->dateIndoLengkap($arr['assign_surat_tgl'])?><br><br>
					<?=$arr['assign_surat_jabatanTTD']?>,<br><br><br><br><br>					
					<u><?=$arr['auditor_name']?></u><br>
					NIP. <?=$arr['auditor_nip']?>					
				</td>
			</tr>
			<?
			if($arr ['assign_surat_tembusan']!=""){
			?>
			<tr>
				<td colspan="2">
					Tembusan :<br>
					<?
					$cek_tem = explode(",", $arr ['assign_surat_tembusan'] );
					$cek_count = count($cek_tem);
					$notem = 0;
					for($x=0;$x<$cek_count;$x++){
						$notem++;
						echo $notem." .".trim($cek_tem[$x])."<br>";
					}
					?>
				</td>
			</tr>
			<?
			}
			?>
		</table>
		<br><br><br><br>
		<form method="post" name="f" action="#" class="form-horizontal" onsubmit="return cek_data()">
		<fieldset>
			<center>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'">
				<input type="button" class="blue_btn" value="ms-word" onClick="window.open('AuditManagement/surat_tugas_print.php?id_surat=<?=$arr['assign_surat_id']?>', '_blank','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
				<input type="hidden" name="data_id" id="data_id" value="<?=$arr['assign_surat_id']?>">
				<input type="hidden" name="status" id="status" value="">
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
		</fieldset>
	</form>
</article>
</section>
<script>
function cek_data(){
	var data = document.getElementById('status').value;
	if(data==1) text = "Anda Yakin Akan Mengajukan?";
	if(data==2) text = "Anda Yakin Akan Menyetujui?";
	if(data==3) text = "Anda Yakin Akan Menolak?";
	return confirm(text);
}
</script>