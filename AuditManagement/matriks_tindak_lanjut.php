<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/finding_class.php";
include_once "_includes/classes/rekomendasi_class.php";
$assigns = new assign ( $ses_userId );
$findings = new finding ( $ses_userId );
// $rekomendasis = new rekomendasi ( $ses_userId );
$ses_assign_id = $_SESSION ['ses_assign_id'];
?>
<script type="text/javascript" src="css/bootstrap.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<th width="2%">No</th>
				<th width="20%">Uraian Temuan</th>
				<th width="20%">Tanggapan Auditee</th>
				<th width="40%">Rekomendasi</th>
				<th width="13%">Status Rekomendasi</th>
				<th width="5%">Tindak Lanjut</th>
			</tr>
			<?
			$no_temuan = 0;
			$rs_list_temuan = $assigns->temuan_list($ses_assign_id);
			while($arr_list_temuan = $rs_list_temuan->FetchRow()){
			$no_temuan++;
			$no_rek = 0;
			$rs_list_rek = $findings->get_rekomendasi_list($arr_list_temuan['finding_id']);
			$count_rek = $rs_list_rek->RecordCount();
			while($arr_list_rek = $rs_list_rek->FetchRow()){
			$no_rek++;
			if($arr_list_rek['rekomendasi_status']==1) $status = "Selesai";
			else $status = "Proses";
			?>
			<tr>
				<?
				if($no_rek==1){
				?>
				<td rowspan="<?=$count_rek?>"><?=$no_temuan?></td>
				<td rowspan="<?=$count_rek?>"><?=$arr_list_temuan['finding_judul']?></td>
				<td align="<?= (null != $arr_list_temuan['finding_tanggapan_auditee']) ? "left" : "center" ?>" rowspan="<?= $count_rek ?>">
					<?php if (null != $arr_list_temuan['finding_tanggapan_auditee']): ?>
					<?=$arr_list_temuan['finding_tanggapan_auditee'] ?>
					<?php else: ?>
					<input type="image" src="images/icn_search.png" title="Tambah Tanggapan Auditee" id="auditee" data-toggle="modal" data-target="#myModal" data-id="<?= $arr_list_temuan['finding_id'] ?>">
					<?php endif ?>
				</td>
				<?
				}
				?>
				<td><?=$arr_list_rek['rekomendasi_desc']?></td>
				<td align="center"><?=$status?></td>
				<td align="center"><input type="image" src="images/icn_search.png" title="View Tindak Lanjut" Onclick="location='main_page.php?method=matrikstindaklanjut&idRec=<?=$arr_list_rek['rekomendasi_id']?>'"></td>
			</tr>
			<?
			}
			}
			if (isset($_POST['submit'])) {
				$ftanggapan_auditee = $comfunc->replacetext($_POST["tanggapan_auditee"]);
				if ($ftanggapan_auditee != "") {
					$findings->add_tanggapan_auditee($_POST['finding_id'], $ftanggapan_auditee);
					$comfunc->js_alert_act(3);	
			?>
					<script>window.open('/latihan/main_page.php?method=matriks_tl', '_self')</script>
			<?
				}
			}
			?>
			<div align="center" class="modal" id="myModal">
					<article class="module" style="width: 60%">
						<header>
							<h3 align="left" class="tabs_involved">Tambah Tanggapan Auditee</h3>
						</header>
						<div class="modal-content">
							<form method="POST" action="#">
								<div>
									<input type="hidden" name="finding_id" id="finding_id">
									<textarea class="ckeditor" cols="10" rows="40" name="tanggapan_auditee" id="tanggapan_auditee"></textarea>
								</div>
								<div align="center">
									<input type="button" class="blue_btn" data-dismiss="modal" value="Kembali">
									&nbsp;
									<input type="submit" name="submit" class="blue_btn" value="Simpan">
								</div>
							</form>
						</div>
					</article>
				</div>
		</table>
	</article>
</section>
<script type="text/javascript">
	$("#myModal").trigger('hide');
	$("#auditee").click(function(e) {
		e.preventDefault();
		$("input[name='finding_id']").val($(this).data('id'));
		$("#myModal").trigger('show');
	});
</script>