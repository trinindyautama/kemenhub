<?
include_once "_includes/classes/assignment_nha_class.php";
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/finding_class.php";
include_once "_includes/classes/kertas_kerja_class.php";
include_once "_includes/classes/rekomendasi_class.php";
include_once "_includes/classes/param_class.php";

$assignment_nhas = new assignment_nha ( $ses_userId );
$assigns       = new assign($ses_userId);
$kertas_kerjas = new kertas_kerja($ses_userId);
$findings      = new finding($ses_userId);
$rekomendasis  = new rekomendasi($ses_userId);
$params        = new param($ses_userId);

$ses_assign_nha_id = $_SESSION ['ses_assign_nha_id'];
$rs_nha = $assignment_nhas->assignment_nha_viewlist ( $ses_assign_nha_id );
$arr_nha = $rs_nha->FetchRow ();
$auditee_id = $arr_nha['nha_auditee_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=finding_nha";
$acc_page_request = "finding_acc.php";
$list_page_request = "audit_view.php";
// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
    if (is_numeric ( $str_page ) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "nha_view_parrent.php";
$grid = "grid.php";
$gridHeader   = array("Judul Temuan", "Satuan Kerja", "Rekomendasi");
$gridDetail   = array("finding_judul", "auditee_name", "0");
$gridWidth    = array("40", "15", "10", "10");

$key_by = array ("Judul Temuan");
$key_field = array ("finding_judul");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

$rs_assign = $assigns->assign_nha_viewlist2( $ses_assign_nha_id );
$arr_assign = $rs_assign->FetchRow();

switch ($_action) {
    case "rekomendasi_nha":
        $_SESSION['ses_finding_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
        ?>
        <script>window.open('main_page.php?method=rekomendasi_nha', '_self');</script>
        <?
        break;
    case "getadd" :
        $_nextaction = "postadd";
        $page_request = $acc_page_request;
        $page_title = "Tambah Temuan";
        break;
    case "getedit" :
        $_nextaction = "postedit";
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $findings->finding_viewlist($fdata_id);
        $page_title = "Ubah Temuan";
        break;
    case "getdetail" :
        $page_request = $acc_page_request;
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $rs = $findings->finding_viewlist($fdata_id);
        $page_title = "Rincian Temuan";
        break;
    case "postadd" :
        $ffinding_assign_id   = $arr_assign['nha_assign_id'];
        $ffinding_auditee     = $arr_assign['nha_auditee_id'];
        $ffinding_no          = $comfunc->replacetext($_POST["finding_no"]);
        $ffinding_type        = $comfunc->replacetext($_POST["finding_type"]);
        $ffinding_sub_id      = $comfunc->replacetext($_POST["finding_sub_id"]);
        $ffinding_jenis_id    = $comfunc->replacetext($_POST["finding_jenis_id"]);
        $ffinding_penyebab_id = $comfunc->replacetext($_POST["finding_penyebab_id"]);
        $ffinding_judul       = $comfunc->replacetext($_POST["finding_judul"]);
        $today = date("d-m-Y");
        $ffinding_date        = $comfunc->date_db($comfunc->replacetext($today));
        $ffinding_kondisi     = $comfunc->replacetext($_POST["finding_kondisi"]);
        $ffinding_kriteria    = $comfunc->replacetext($_POST["finding_kriteria"]);
        $ffinding_sebab       = $comfunc->replacetext($_POST["finding_sebab"]);
        $ffinding_akibat      = $comfunc->replacetext($_POST["finding_akibat"]);
        $ftanggapan_auditee   = $comfunc->replacetext($_POST["tanggapan_auditee"]);
        $ftanggapan_auditor   = $comfunc->replacetext($_POST["tanggapan_auditor"]);
        $ffinding_nilai       = $comfunc->replacetext($_POST["finding_nilai"]);
        $ffinding_attachment  = $comfunc->replacetext($_FILES["finding_attach"]["name"]);
        $finding_id = $findings->uniq_id();
		$ffinding_sub_penyebab_id = $comfunc->replacetext($_POST["finding_sub_penyebab_id"]);
		$ffinding_sub_sub_penyebab_id = $comfunc->replacetext($_POST["finding_sub_sub_penyebab_id"]);
        if ($ffinding_auditee != "" && $ffinding_judul != "" && $ffinding_jenis_id != "" && $ffinding_sub_penyebab_id != "") {
            $findings->finding_nha_add($finding_id, $ffinding_assign_id, $ffinding_auditee, $ffinding_no, $ffinding_type, $ffinding_sub_id, $ffinding_jenis_id, $ffinding_judul, $ffinding_date, $ffinding_kondisi, $ffinding_kriteria, $ffinding_sebab, $ffinding_akibat, $ffinding_attachment, $ses_kka_id, $ftanggapan_auditee, $ftanggapan_auditor, $ffinding_penyebab_id, $ffinding_nilai, $ses_assign_nha_id, $ffinding_sub_penyebab_id, $ffinding_sub_sub_penyebab_id);
            $comfunc->UploadFile("Upload_Temuan", "finding_attach", "");
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>
            window.open('main_page.php?method=rekomendasi_nha&finding_id=<?=$finding_id?>&data_action=getadd', '_self');
        </script>
        <?
        $page_request = "blank.php";
        break;
    case "postedit" :
        $fdata_id             = $comfunc->replacetext($_POST["data_id"]);
        $ffinding_auditee     = $arr_assign['nha_auditee_id'];
        $ffinding_no          = $comfunc->replacetext($_POST["finding_no"]);
        $ffinding_type        = $comfunc->replacetext($_POST["finding_type"]);
        $ffinding_sub_id      = $comfunc->replacetext($_POST["finding_sub_id"]);
        $ffinding_jenis_id    = $comfunc->replacetext($_POST["finding_jenis_id"]);
        $ffinding_penyebab_id = $comfunc->replacetext($_POST["finding_penyebab_id"]);
        $ffinding_judul       = $comfunc->replacetext($_POST["finding_judul"]);
        $today = date("d-m-Y");
        $ffinding_date        = $comfunc->date_db($comfunc->replacetext($today));
        $ffinding_kondisi     = $comfunc->replacetext($_POST["finding_kondisi"]);
        $ffinding_kriteria    = $comfunc->replacetext($_POST["finding_kriteria"]);
        $ffinding_sebab       = $comfunc->replacetext($_POST["finding_sebab"]);
        $ffinding_akibat      = $comfunc->replacetext($_POST["finding_akibat"]);
        $ftanggapan_auditee   = $comfunc->replacetext($_POST["tanggapan_auditee"]);
        $ftanggapan_auditor   = $comfunc->replacetext($_POST["tanggapan_auditor"]);
        $ffinding_nilai       = $comfunc->replacetext($_POST["finding_nilai"]);
        $ffinding_attachment  = $comfunc->replacetext($_FILES["finding_attach"]["name"]);
        $ffinding_attach_old  = $comfunc->replacetext($_POST["finding_attach_old"]);
		$ffinding_sub_penyebab_id = $comfunc->replacetext($_POST["finding_sub_penyebab_id"]);
		$ffinding_sub_sub_penyebab_id = $comfunc->replacetext($_POST["finding_sub_sub_penyebab_id"]);
        if ($ffinding_auditee != "" && $ffinding_judul != "" && $ffinding_jenis_id != "" && $ffinding_sub_penyebab_id != "") {
            if (!empty($ffinding_attachment)) {
                $comfunc->UploadFile("Upload_Temuan", "finding_attach", $ffinding_attach_old);
            } else {
                $ffinding_attachment = $ffinding_attach_old;
            }
            $findings->finding_edit($fdata_id, $ffinding_auditee, $ffinding_no, $ffinding_type, $ffinding_sub_id, $ffinding_jenis_id, $ffinding_judul, $ffinding_date, $ffinding_kondisi, $ffinding_kriteria, $ffinding_sebab, $ffinding_akibat, $ffinding_attachment, $ftanggapan_auditee, $ftanggapan_auditor, $ffinding_penyebab_id, $ffinding_nilai, $ffinding_sub_penyebab_id, $ffinding_sub_sub_penyebab_id);
            $comfunc->js_alert_act(1);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    case "getdelete" :
        $fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
        $findings->finding_delete($fdata_id);
        $comfunc->js_alert_act ( 2 );
        ?>
        <script>window.open('<?=$def_page_request?>', '_self');</script>
        <?
        $page_request = "blank.php";
        break;
    default :
        $recordcount = $assignment_nhas->finding_nha_count ( $ses_assign_nha_id, $auditee_id, $key_search, $val_search, $key_field );
        $rs = $assignment_nhas->finding_nha_view_grid ( $ses_assign_nha_id, $auditee_id, $key_search, $val_search, $key_field, $offset, $num_row );
        $page_title = "Daftar Temuan";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>
