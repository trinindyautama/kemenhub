<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="css/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.maskMoney.js"></script>
<section id="main" class="column">		
<?
if (! empty ( $view_parrent ))
	include_once $view_parrent;
?>   
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset>
				<label class="span2">Tindak Lanjut</label>
			</fieldset>
			<fieldset class="hr">
				<textarea class="ckeditor" cols="10" rows="40" name="tl_desc" id="tl_desc"></textarea>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Attachment</label> 
				<input type="file" class="span4" name="tl_attach" id="tl_attach">
			</fieldset>
			<fieldset class="hr">
                <label class="span2">Nilai Tindak Lanjut</label>
                <input type="text" class="span2" name="tl_nilai" id="tl_nilai">
            </fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset>
				<label class="span2">Tindak Lanjut</label>
			</fieldset>
			<fieldset class="hr">
				<textarea class="ckeditor" cols="10" rows="40" name="tl_desc" id="tl_desc"><?php echo $arr['tl_desc']?></textarea>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Attachment</label> 
				<input type="hidden" class="span4" name="tl_attach_old" value="<?=$arr['tl_attachment']?>">
				<input type="file" class="span4" name="tl_attach" id="tl_attach"> 
				<label class="span2">
					<a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Tindaklanjut").$arr['tl_attachment']?>','_blank')"><?=$arr['tl_attachment']?></a>
				</label>
			</fieldset>
			<fieldset class="hr">
                <label class="span2">Nilai Tindak Lanjut</label>
                <input type="text" class="span2" name="tl_nilai" id="tl_nilai" value="<?=$arr['tl_nilai']?>">
            </fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['tl_id']?>">	
		<?
				break;
			case "getajukan_tl" :
			case "getapprove_tl" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<table class="view_parrent">
					<tr>
						<td width="120px">Tindak Lanjut</td>
						<td>:</td>
						<td><?=$comfunc->text_show($arr['tl_desc'])?></td>
					</tr>
					<tr>
						<td width="120px">Lampiran</td>
						<td>:</td>
						<td><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Tindaklanjut").$arr['tl_attachment']?>','_blank')"><?=$arr['tl_attachment']?></a></td>
					</tr>
					<tr>
						<td width="120px">Nilai Tindak Lanjut</td>
						<td>:</td>
						<td>Rp. <?=$comfunc->text_show($arr['tl_nilai'])?></td>
					</tr>
					<tr>
						<td>Detail komentar</td>
						<td>:</td>
						<td>
						<?php 
						$z = 0;
						$rs_komentar = $tindaklanjuts->tindaklanjut_komentar_viewlist( $arr ['tl_id'] );
						while ( $arr_komentar = $rs_komentar->FetchRow () ) {
							$z ++;
							echo $z.". ".$arr_komentar['auditor_name']." : ".$arr_komentar['tl_comment_desc']."<br>";
						}
						?>
						</td>
					</tr>
				</table>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Isi Komentar</label>
				<textarea id="komentar" name="komentar" rows="1" cols="20" style="width: 475px; height: 3em; font-size: 11px;"></textarea>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['tl_id']?>">
			<input type="hidden" name="status_tl" value="<?=$status?>">	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp; 
					<input type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action"
					value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
	<script>
$(function() {
	$("#tl_nilai").maskMoney({precision: 0});
	
	$("#validation-form").validate({
		rules: {
			tl_desc: "required"
		},
		messages: {
			tl_desc: "Silahkan Isi Tindak Lanjut"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>