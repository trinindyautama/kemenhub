<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
	<style type="text/css">
		body {
			padding-top: 2cm;
			padding-right: 2cm;
			padding-bottom: 2cm;
			padding-left: 2cm;
		}
	</style>
</head>
<body>
	<!-- <table align="center" width="100%">
	<tr>
		<td align="center"> -->
	<center>
		<img src="<?= $comfunc->urlImage('images/logo-menhub.png') ?>"/>
		<br><br><br>
		<strong>KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA<br>INSPEKTORAT JENDERAL</strong>
	</center>
	<hr>
	<center>
		<br><br><br><strong>LAPORAN HASIL AUDIT</strong>
		<br><br><br><strong><?=$assign_nama_auditee?></strong>
		<br><br><br><strong>Nomor : <?=$arr_lha['assign_no_lha']?></strong><br><br><br>
		<strong>Tanggal :  <?=$comfunc->dateIndo2($arr_lha['assign_date_lha'])?></strong>
	</center>
	<!-- </td> -->
<br>
	<center>
		<strong>BAGIAN PERTAMA</strong>
		<strong>SIMPULAN HASIL AUDIT DAN REKOMENDASI</strong>
	</center>
	<div style="text-align: justify;"><?=$arr_lha['lha_ringkasan']?></div>
<br>
	<center>
		<strong>BAGIAN KEDUA</strong><br>
		<strong>URAIAN HASIL AUDIT</strong><br>
		<strong>BAB I</strong><br>
		<strong>PENDAHULUAN</strong>
	</center>
<br>
	<div>1. Dasar Audit</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_dasar_audit'])?></div>
	<br>
	<div>2. Tujuan Audit</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_tujuan_audit'])?></div>
	<br>
	<div>3. Ruang Lingkup Audit</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_ruanglingkup'])?></div>
	<br>
	<div>4. Batasan Audit</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_batasan'])?></div>
	<br>
	<div>5. Metodologi Audit</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_metodologi'])?></div>
	<br>
	<div>6. Strategi pelaporan</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_strategi_laporan'])?></div><br>
	<div>7. Data umum Auditan</div>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_data_auditan'])?></div>
	<br>
	<div>8. Status Dan Tindak Lanjut Temuan Hasil Audit Yang Lalu</div>
	<div  style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_tl_audit_lalu'])?></div>
	<br>
	<br>
	<center>
		<strong>BAB II</strong><br>
		<strong>URAIAN HASIL AUDIT</strong>
	</center>
	<br>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_hasil'])?></div>
	<br>
	<?
		$no_aspek=0;
		$rs_list_aspek = $programaudits->get_assign_aspek1 ( $arr['assign_id'], $assign_id_auditee);
		while($arr_list_aspek = $rs_list_aspek->FetchRow()){
		$no_aspek++;
		?>
		<div><?=$no_aspek?>. <?=ucwords(strtolower($arr_list_aspek['aspek_name']))?></div>
		<div>&nbsp;&nbsp;&nbsp;a. Informasi Umum</div>
		<div>&nbsp;&nbsp;&nbsp;b. Temuan dan Rekomendasi</div>
		<?
		$no_temuan=0;
		$rs_list_temuan = $findings->get_list_temuan_by_aspek ( $arr_list_aspek['aspek_id'], $arr['assign_id'], $assign_id_auditee);
		while($arr_list_temuan = $rs_list_temuan->FetchRow()){
		$no_temuan++;
		?>
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=$no_temuan?>. <?=$arr_list_temuan['finding_judul']?></strong></div>
		<div style="text-align: justify;padding-left: 1cm;"> <?=$comfunc->text_show($arr_list_temuan['finding_kondisi'])?></div>
		<br>
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tanggapan Auditan :</div>
		<div style="text-align: justify;padding-left: 1cm;"><?=$comfunc->text_show($arr_list_temuan['finding_tanggapan_auditee'])?></div>
		<br>
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rekomendasi :</div>
		<?
		$rs_rekomendasi = $findings->get_rekomendasi_list ( $arr_list_temuan['finding_id']);
		$no_rek = 0;
		while($arr_rekomendasi = $rs_rekomendasi->FetchRow()){
		$no_rek++;
		?>
		<div style="text-align: justify;padding-left: 1cm;"><?=$no_rek?>) <?=$comfunc->text_show($arr_rekomendasi['rekomendasi_desc'])?></div><br>
		<?
		}
		}
		}
		?>
	<center>
		<strong>BAB III</strong><br>
		<strong>PENUTUP</strong>
	</center>
	<div style="text-align: justify;"><?=$comfunc->text_show($arr_lha['lha_penutup'])?></div>
	</body>
</html>