<?
include_once "../_includes/common.php";
include_once "../_includes/classes/assignment_class.php";
$comfunc      = new comfunction();
$assigns      = new assign();
$fdata_id     = $comfunc->replacetext($_POST["data_id"]);
$flha_id      = $comfunc->replacetext($_POST["lha_id"]);
$fno_lha      = $comfunc->replacetext($_POST["no_lha"]);
$ftanggal_lha = $comfunc->date_db($comfunc->replacetext($_POST["tanggal_lha"]));
//bab 1
$fringkasan = $comfunc->replacetext($_POST["ringkasan"]);
//bab 2
$fdasar_audit            = $comfunc->replacetext($_POST["dasar_audit"]);
$ftujuan_audit           = $comfunc->replacetext($_POST["tujuan_audit"]);
$fruang_lingkup          = $comfunc->replacetext($_POST["ruang_lingkup"]);
$fbatasan_tanggung_jawab = $comfunc->replacetext($_POST["batasan_tanggung_jawab"]);
$fmetodologi_audit       = $comfunc->replacetext($_POST["metodologi_audit"]);
$fstrategi_laporan       = $comfunc->replacetext($_POST["strategi_laporan"]);
$fdata_umum_auditan      = $comfunc->replacetext($_POST["data_umum_auditan"]);
$fhasil_yang_dicapai     = $comfunc->replacetext($_POST["hasil_yang_dicapai"]);
//bab 3
$fpenutup    = $comfunc->replacetext($_POST["penutup"]);
$fstatus_lha = $comfunc->replacetext($_POST["status_lha"]);

$assigns->lha_histori_add($flha_id, $comfunc->date_db(date('d-m-Y')), $fdata_id, $fno_lha, $ftanggal_lha, $fringkasan, $fdasar_audit, $ftujuan_audit, $fruang_lingkup, $fbatasan_tanggung_jawab, $fmetodologi_audit, $fstrategi_laporan, $fdata_umum_auditan, $fhasil_yang_dicapai, $fpenutup, $fstatus_lha);
