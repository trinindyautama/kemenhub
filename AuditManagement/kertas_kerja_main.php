<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/program_audit_class.php";
include_once "_includes/classes/kertas_kerja_class.php";
include_once "_includes/classes/finding_class.php";

$assigns       = new assign($ses_userId);
$programaudits = new programaudit($ses_userId);
$kertas_kerjas = new kertas_kerja($ses_userId);
$findings      = new finding($ses_userId);


@$_action = $comfunc->replacetext($_REQUEST["data_action"]);

if (isset($_POST["val_search"])) {
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if (@$method != @$val_method) {
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request    = "main_page.php?method=kertas_kerja";
$acc_page_request  = "kertas_kerja_acc.php";
$list_page_request = "audit_view.php";
// ==== buat grid ===//
$num_row   = 10;
@$str_page = $comfunc->replacetext($_GET['page']);
if (isset($str_page)) {
    if (is_numeric($str_page) && $str_page != 0) {
        $noPage = $str_page;
    } else {
        $noPage = 1;
    }
} else {
    $noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$ses_assign_id = $_SESSION ['ses_assign_id'];
@$id_kka_dash = $comfunc->replacetext ( $_REQUEST ["id_kka_dash"] );
$get_id_program = $kertas_kerjas->get_id_program($id_kka_dash);
if($get_id_program==""){
    $ses_program_id = $_SESSION ['ses_program_id'];
    $def_page_request = $paging_request . "&page=$noPage";
}else{
    $ses_program_id = $get_id_program;
    $def_page_request = "main_page.php?method=kertas_kerja&id_kka_dash=$id_kka_dash";
}
$rs_programs = $programaudits->program_audit_viewlist ( $ses_program_id );
$arr_programs = $rs_programs->FetchRow ();
$rs_assigns = $assigns->assign_viewlist($ses_assign_id);
$arr_assigns = $rs_assigns->FetchRow();
$maxKKA = $kertas_kerjas->get_kka_auto($arr_programs['program_tahapan']);
switch ($arr_programs['program_tahapan']) {
    case "PKA Pendahuluan":
        $pka = 'PKAPEND';
        break;
    case "PKA Pelaksanaan":
        $pka = 'PKAPEL';
        break;
    case "PKA Rinci":
        $pka = 'PKARIN';
        break;
}
$type = '';
switch ($arr_assigns['audit_type_name']) {
    case "Audit Dengan Tujuan Tertentu":
        $type = 'KHS';
        break;
    case "Audit Operasional":
        $type = "OPS";
        break;
    case "Audit PBJ":
        $type = "PBJ";
        break;
    case "Audit Kinerja":
        $type = "KIN";
        break;
}

$strlen = strlen($arr_programs['auditee_kode']) + strlen($pka) +13;
$no = (int) substr($maxKKA, 4, 4);
// echo $maxKKA;
// echo $no;
$no++;
// echo '-'.$no;
$year = date('Y');
$month = date('m');
$kka_auto = 'KKA/0'.sprintf("%03s", $no).'/'.$type.'/'.$arr_programs['auditee_kode'].'/'.$pka.'/'.$month.'/'.$year;

$rs_assign  = $kertas_kerjas->getdata_assign($ses_program_id);
$arr_assign = $rs_assign->FetchRow();

$status_katim = $assigns->assign_cek_katim($ses_assign_id, $ses_userId);

$view_parrent = "kertas_kerja_view_parrent.php";
$rs_tahapan   = $kertas_kerjas->tahapan($ses_program_id);

foreach ($rs_tahapan->GetArray() as $arr_tahapan) {

    if ($arr_tahapan['program_tahapan'] == "PKA Pendahuluan") {
        $grid       = "grid_kka.php";
        $gridHeader = array("No KKA", "Kesimpulan", "Tanggal", "Status", "Lampiran");
        $gridDetail = array("kertas_kerja_no", "kertas_kerja_kesimpulan", "kertas_kerja_date", "kertas_kerja_status", "0");
        $gridWidth  = array("15", "35", "10", "10", "10");

        $key_by    = array("No KKA", "Kesimpulan");
        $key_field = array("kertas_kerja_no", "kertas_kerja_kesimpulan");

        $widthAksi  = "15";
        $iconDetail = "1";
    } else {
        $grid       = "grid_kka.php";
        $gridHeader = array("No KKA", "Kesimpulan", "Tanggal", "Status", "Temuan Audit", "Lampiran");
        $gridDetail = array("kertas_kerja_no", "kertas_kerja_kesimpulan", "kertas_kerja_date", "kertas_kerja_status", "kertas_kerja_id", "0");
        $gridWidth  = array("15", "35", "10", "10", "10", "10");

        $key_by    = array("No KKA", "Kesimpulan");
        $key_field = array("kertas_kerja_no", "kertas_kerja_kesimpulan");

        $widthAksi  = "15";
        $iconDetail = "1";
    }

}

switch ($_action) {
    case "getajukan_kka":
    case "getapprove_kka":
        $_nextaction  = "postkomentar";
        $page_request = $acc_page_request;
        $id           = $comfunc->replacetext($_REQUEST["data_id"]);
        $status       = $comfunc->replacetext($_REQUEST["status_kka"]);

        $rs           = $kertas_kerjas->kertas_kerja_viewlist($id);
        $page_title   = "Reviu Kertas Kerja";
        break;
    case "postkomentar":
        $id_kka = $comfunc->replacetext($_POST["data_id"]);

        $status    = $comfunc->replacetext($_POST["status_kka"]);
        $fkomentar = $comfunc->replacetext($_POST["komentar"]);
        $ftanggal = $comfunc->date_db(date("d-m-Y H:i:s"));
            
        $comfunc->hapus_notif($id_kka);
        if ($status != "") {
            if($status==1){ 
                $get_user_id = $kertas_kerjas->get_user_by_posisi($ses_program_id, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            }elseif($status==2){ 
                $get_user_id = $kertas_kerjas->get_user_by_posisi($ses_program_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }elseif($status==5 || $status==0){ 
                $get_user_id = $kertas_kerjas->get_user_owner($ses_program_id);
                $notif_to_user_id = $get_user_id; //ke anggota
            }elseif($status==3){ 
                $get_user_id = $kertas_kerjas->get_user_by_posisi($ses_program_id, '1fe7f8b8d0d94d54685cbf6c2483308aebe96229');
                $notif_to_user_id = $get_user_id; //ke daltu
            }elseif($status==6){ 
                $get_user_id = $kertas_kerjas->get_user_by_posisi($ses_program_id, '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7');
                $notif_to_user_id = $get_user_id; //ke katim
            }elseif($status==7){ 
                $get_user_id = $kertas_kerjas->get_user_by_posisi($ses_program_id, '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd');
                $notif_to_user_id = $get_user_id; //ke dalnis
            }
            if($notif_to_user_id!=""){
                $comfunc->insert_notif($id_kka, $ses_userId, $notif_to_user_id, 'kertas_kerja', '(Kertas Kerja) '.$fkomentar, $ftanggal);
            }
            $kertas_kerjas->kka_update_status($id_kka, $status);

            $ftanggal = $comfunc->date_db(date("d-m-Y H:i:s"));
            if ($fkomentar != "") {
                $kertas_kerjas->kka_add_komentar($id_kka, $fkomentar, $ftanggal);
                $comfunc->js_alert_act(3);
            }
        } else {
            $comfunc->js_alert_act(10);
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    case "lampiran_kka":
        $_SESSION['ses_kka_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
        ?>
<script>window.open('main_page.php?method=lampiran_kka', '_self');</script>
<?
        break;
    case "finding_kka":
        $_SESSION['ses_kka_id'] = $comfunc->replacetext($_REQUEST["data_id"]);
        ?>
<script>window.open('main_page.php?method=finding_kka', '_self');</script>
<?
        break;
    case "getadd":
        $_nextaction  = "postadd";
        $page_request = $acc_page_request;
        $kka = $kka_auto;
        $page_title   = "Tambah Kertas Kerja Audit";
        break;
    case "getedit":
        $_nextaction  = "postedit";
        $page_request = $acc_page_request;
        $fdata_id     = $comfunc->replacetext($_REQUEST["data_id"]);
        $rs           = $kertas_kerjas->kertas_kerja_viewlist($fdata_id);
        $page_title   = "Ubah Kertas Kerja Audit";
        break;
    case "getdetail":
        $page_request = $acc_page_request;
        $fdata_id     = $comfunc->replacetext($_REQUEST["data_id"]);
        $rs           = $kertas_kerjas->kertas_kerja_viewlist($fdata_id);
        $page_title   = "Rincian Kertas Kerja Audit";
        break;

    case "postadd":
        $id_kka             = $kertas_kerjas->uniq_id();
        $fno_kka            = $comfunc->replacetext($_POST["no_kka"]);
        $fkertas_kerja      = $comfunc->replacetext($_POST["kertas_kerja"]);
        $fkesimpulan        = $comfunc->replacetext($_POST["kesimpulan"]);
        $fkertas_kerja_date = $comfunc->date_db($comfunc->replacetext($_POST["kertas_kerja_date"]));
        $fkertas_kerja_jam  = $comfunc->replacetext($_POST["kertas_kerja_jam"]);
        $fkka_attach        = $_FILES["kka_attach"]["name"];
        $jml_attach         = count($fkka_attach);
        if ($fno_kka != "" && $fkertas_kerja_date != "" && $fkertas_kerja_jam != "") {
            $kertas_kerjas->kertas_kerja_add($id_kka, $ses_program_id, $fno_kka, $fkertas_kerja, $fkesimpulan, $fkertas_kerja_date, $fkertas_kerja_jam, "Pindah ke table kka_attach");
            for ($i = 0; $i < $jml_attach; $i++) {
                if ($fkka_attach[$i] != "") {
                    $comfunc->UploadFileMulti("Upload_KKA", "kka_attach");
                    $kertas_kerjas->insert_lampiran_kka($id_kka, $fkka_attach[$i]);
                }
            }
            $comfunc->js_alert_act(3);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    case "postedit":
        $fdata_id           = $comfunc->replacetext($_POST["data_id"]);
        $fno_kka            = $comfunc->replacetext($_POST["no_kka"]);
        $fkertas_kerja      = $comfunc->replacetext($_POST["kertas_kerja"]);
        $fkesimpulan        = $comfunc->replacetext($_POST["kesimpulan"]);
        $fkertas_kerja_date = $comfunc->date_db($comfunc->replacetext($_POST["kertas_kerja_date"]));
        $fkertas_kerja_jam  = $comfunc->replacetext($_POST["kertas_kerja_jam"]);
        $fkka_attach        = $_FILES["kka_attach"]["name"];
        $jml_attach         = count($fkka_attach);
        if ($fno_kka != "" && $fkertas_kerja_date != "" && $fkertas_kerja_jam != "") {
            $rs_file = $kertas_kerjas->list_kka_lampiran($fdata_id);
            $z       = 0;
            while ($arr_file = $rs_file->FetchRow()) {
                $z++;
                $nama_file = $comfunc->replacetext(@$_POST["nama_file_" . $z]);
                $kertas_kerjas->delete_lampiran_kka($fdata_id, $nama_file);
                $comfunc->HapusFile("Upload_KKA", $nama_file);
            }
            $kertas_kerjas->kertas_kerja_edit($fdata_id, $fno_kka, $fkertas_kerja, $fkesimpulan, $fkertas_kerja_date, $fkertas_kerja_jam, "Pindah ke table kka_attach");
            for ($i = 0; $i < $jml_attach; $i++) {
                if ($fkka_attach[$i] != "") {
                    $comfunc->UploadFileMulti("Upload_KKA", "kka_attach");
                    $kertas_kerjas->insert_lampiran_kka($fdata_id, $fkka_attach[$i]);
                }
            }
            $comfunc->js_alert_act(1);
        } else {
            $comfunc->js_alert_act(5);
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    case "getdelete":
        $fdata_id = $comfunc->replacetext($_REQUEST["data_id"]);
        $comfunc->hapus_notif($fdata_id);
        $kertas_kerjas->kertas_kerja_delete($fdata_id);
        $comfunc->js_alert_act(2);
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
        $page_request = "blank.php";
        break;
    default:
        $recordcount  = $kertas_kerjas->kertas_kerja_count($ses_program_id, $key_search, $val_search, $key_field);
        $rs           = $kertas_kerjas->kertas_kerja_view_grid($ses_program_id, $key_search, $val_search, $key_field, $offset, $num_row);
        $page_title   = "Daftar Kertas Kerja Audit";
        $page_request = $list_page_request;
        break;
}
include_once $page_request;
?>