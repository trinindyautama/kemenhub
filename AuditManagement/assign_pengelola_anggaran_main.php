<?
include_once "_includes/classes/param_class.php";
include_once "_includes/classes/assignment_class.php";

$params = new param ( $ses_userId );
$assigns = new assign ( $ses_userId );

$ses_pagu_id = $_SESSION ['ses_pagu_id'];
$ses_assign_id = $_SESSION ['ses_assign_id'];
$ses_auditee_id = $_SESSION ['ses_auditee_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=assign_lha_pengelola_anggaran";
$acc_page_request = "pic_acc.php";
$list_page_request = "audit_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "lha_view_parrent.php";
$grid = "grid.php";
$gridHeader = array ("NIP", "Nama", "Jabatan", "Email", "Mobile");
$gridDetail = array ("1", "2", "3", "4", "13");
$gridWidth = array ("15", "20", "15", "15", "10");

$key_by = array ("Nama");
$key_field = array ("pic_name");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

$rs_pagu = $assigns->assign_pagu_viewlist ( $ses_pagu_id );
$arr_pagu = $rs_pagu->FetchRow ();

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Pengelola Anggaran";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->pic_data_viewlist ( $fdata_id );
		$page_title = "Ubah Pengelola Anggaran";
		break;
	case "getdetail" :
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->pic_data_viewlist ( $fdata_id );
		$page_title = "Rincian Pengelola Anggaran";
		break;
    case "postadd" :
        $fauditee_id = $ses_auditee_id;
        $fnip = $comfunc->replacetext ( $_POST ["nip"] );
        $fname = $comfunc->replacetext ( $_POST ["name"] );
        $fjabatan = $comfunc->replacetext ( $_POST ["jabatan_id"] );
        $fmobile = $comfunc->replacetext ( $_POST ["mobile"] );
        $ftelp = $comfunc->replacetext ( $_POST ["telp"] );
        $femail = $comfunc->replacetext ( $_POST ["email_pic"] );
        $fpic_no_sk = $comfunc->replacetext ( $_POST ["pic_no_sk"] );
        $fpic_tgl_sk = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["pic_tgl_sk"]) );
        $ftahun = $arr_pagu['lha_pagu_tahun'];
        $fpangkat = $comfunc->replacetext ( $_POST ["pangkat"] );
        $fjabatan_struktur = $comfunc->replacetext ( $_POST ["jabatan_struktur"] );
		if ($fnip != "" && $fname != "" && $fpic_no_sk!= "" && $fpic_tgl_sk != "") {
			$fpic_id = "";
			if ($fpic_id == "") {
                $params->pic_pagu_add ( $fnip, $fname, $fjabatan, $fmobile, $ftelp, $femail, $fauditee_id, $fpic_no_sk, $fpic_tgl_sk, $ftahun, $fpangkat, $fjabatan_struktur, $ses_pagu_id);
                $comfunc->js_alert_act ( 3 );
            } else {
                if ($del_st == "0") {
                    $params->update_pic_del ( $fpic_id, $fnip, $fname, $fjabatan, $fmobile, $ftelp, $femail, $fauditee_id, $fpic_no_sk, $fpic_tgl_sk, $ftahun, $fpangkat, $fjabatan_struktur);
                    $comfunc->js_alert_act ( 3 );
                } else {
                    $comfunc->js_alert_act ( 4, $fname );
                }
            }
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
    case "postedit" :
        $fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
        $fauditee_id = $ses_auditee_id;
        $fnip = $comfunc->replacetext ( $_POST ["nip"] );
        $fname = $comfunc->replacetext ( $_POST ["name"] );
        $fjabatan = $comfunc->replacetext ( $_POST ["jabatan_id"] );
        $fmobile = $comfunc->replacetext ( $_POST ["mobile"] );
        $ftelp = $comfunc->replacetext ( $_POST ["telp"] );
        $femail = $comfunc->replacetext ( $_POST ["email_pic"] );
        $fpic_no_sk = $comfunc->replacetext ( $_POST ["pic_no_sk"] );
        $fpic_tgl_sk = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["pic_tgl_sk"]) );
        $ftahun = $arr_pagu['lha_pagu_tahun'];
        $fpangkat = $comfunc->replacetext ( $_POST ["pangkat"] );
        $fjabatan_struktur = $comfunc->replacetext ( $_POST ["jabatan_struktur"] );
        if ($fnip != "" && $fname != "" && $fpic_no_sk!= "") {
			$fpic_id = "";
            if ($fpic_id == "") {
                $params->pic_edit ( $fdata_id, $fnip, $fname, $fjabatan, $fmobile, $ftelp, $femail, $fpic_no_sk, $fpic_tgl_sk, $ftahun, $fpangkat, $fjabatan_struktur );
                $comfunc->js_alert_act ( 1 );
            } else {
                if ($del_st == "0") {
                    $params->update_pic_del ( $fpic_id, $fnip, $fname, $fjabatan, $fmobile, $ftelp, $femail, $fauditee_id, $fpic_no_sk, $fpic_tgl_sk, $ftahun, $fpangkat, $fjabatan_struktur );
                    $params->pic_delete ( $fdata_id );
                    $comfunc->js_alert_act ( 1 );
                } else {
                    $comfunc->js_alert_act ( 4, $fname );
                }
            }
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$params->pic_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $params->pic_pagu_count ( $ses_pagu_id, $key_search, $val_search, $key_field );
		$rs = $params->pic_pagu_viewlist ( $ses_pagu_id, $key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Pengelola Anggaran";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
