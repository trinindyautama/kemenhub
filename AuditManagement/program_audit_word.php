<?
header("Content-Type: application/msword");
header("Content-Disposition: attachment; filename=ikhtisar_temuan.doc");
header("Pragma: no-cache");
header("Expires: 0");
define ("INBOARD",false);

include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/assignment_class.php";

$reports = new report ();
$programaudits = new programaudit ();
$assigns = new assign ( $ses_userId );

$id_assign = $_REQUEST['id_assign'];
$id_auditee = $_REQUEST['id_auditee'];
$fil_aspek = $_REQUEST['fil_aspek'];

$rs_assign = $assigns->assign_viewlist ($id_assign);
$arr = $rs_assign->FetchRow();

$rs_auditee = $assigns->assign_auditee_viewlist ( $id_assign, $id_auditee );
$arr_auditee = $rs_auditee->FetchRow();

$rs_intro = $programaudits->intro_pka_viewlist ( "", $fil_aspek, $id_assign );
$arr_intro = $rs_intro->FetchRow();


$rs_aspek = $programaudits->get_assign_aspek($fil_aspek);
$arr_aspek = $rs_aspek->FetchRow();

$katim = $reports->get_anggota($id_assign, $id_auditee, 'KT');
$dalnis = $reports->get_anggota($id_assign, $id_auditee, 'PT');

?>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<style>
body{
	width:210mm;
	font-family:Arial;
}
</style>
<body>
	<table border='0' class="table_report" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="8">
				<center>
				<img src="images/kka-logo1.png"  width="55" height="48" style="float:left;margin-left: 180px;width:55pt;height:48pt;" />
					<img src="images/kka-logo2.png"  width="55" height="48" style="float:right;margin-right: 180px;width:55pt;height:48pt;" />
					<H2>KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA<br>INSPEKTORAT JENDERAL</H2>
				</center><br>
			</td>
		</tr>
		<tr>
			<td width="5%" style="border-top:1px solid">&nbsp;</td>
			<td width="5%" style="border-top:1px solid">&nbsp;</td>
			<td style="border-top:1px solid">&nbsp;</td>
			<td style="border-top:1px solid">&nbsp;</td>
			<td width="10%" style="border-top:1px solid">No KKA</td>
			<td align="center" style="border-top:1px solid">:</td>
			<td style="border-top:1px solid">-</td>
			<td style="border-top:1px solid">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">Nama Auditan</td>
			<td align="center">:</td>
			<td><?=$arr_auditee['auditee_name'];?></td>
			<td>Ref. PKA No</td>
			<td align="center">:</td>
			<td>-</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">Sasaran Audit</td>
			<td align="center">:</td>
			<td><?=$arr['audit_type_name'];?></td>
			<td>Disusun Oleh</td>
			<td align="center">:</td>
			<td><?=$katim['nama']?></td>
			<td>Paraf :</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>Tanggal</td>
			<td align="center">:</td>
			<td>-</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">Periode Audit</td>
			<td align="center">:</td>
			<td><?=$arr['assign_tahun']?></td>
			<td>Direviu Oleh</td>
			<td align="center">:</td>
			<td><?=$dalnis['nama'] ?></td>
			<td>Paraf :</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid">&nbsp;</td>
			<td style="border-bottom:1px solid">&nbsp;</td>
			<td style="border-bottom:1px solid">&nbsp;</td>
			<td style="border-bottom:1px solid">Tanggal</td>
			<td style="border-bottom:1px solid" align="center">:</td>
			<td style="border-bottom:1px solid">-</td>
			<td style="border-bottom:1px solid">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" align="center"><br>PROGRAM KERJA AUDIT RINCI/LANJUTAN</td>
		</tr>
		<tr>
			<td><strong>A.</strong></td>
			<td colspan="7"><strong>Pendahuluan</strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="7"><?=$arr_intro['pka_intro_pendahuluan']?></td>
		</tr>
		<tr>
			<td><strong>B.</strong></td>
			<td colspan="7"><strong>Tujuan Audit</strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="7"><?=$arr_intro['pka_intro_tujuan']?></td>
		</tr>
		<tr>
			<td><strong>C.</strong></td>
			<td colspan="7"><strong>Instruksi Khusus</strong></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid">&nbsp;</td>
			<td style="border-bottom:1px solid" colspan="7"><?=$arr_intro['pka_intro_instruksi']?></td>
		</tr>
		<tr>
			<td><strong>D.</strong></td>
			<td colspan="7"><strong>Langkah Kerja:</strong><?=$arr_aspek['aspek_name']?></td>
		</tr>
	</table>
	<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
		<tr align="center">
			<th width="2%" rowspan="2">No.</th>
			<th width="50%" rowspan="2">Langkah Kerja Audit</th>
			<th width="16%" colspan="2">Dilaksanakan Oleh</th>
			<th width="16%" colspan="2">Waktu yang Diperlukan</th>
			<th width="9%" rowspan="2">Nomor KKA</th>
			<th width="7%" rowspan="2">Catatan</th>
		</tr>
		<tr align="center">
			<th>Rencana</th>
			<th>Realisasi</th>
			<th>Rencana</th>
			<th>Realisasi</th>
		</tr>
		<?
		$no=0;
		$rs_detail = $reports->assign_program_audit_list ($fil_aspek, $id_assign, $id_auditee );
		while($arr_detail = $rs_detail->FetchRow()){
			$no++;
		?>
		<tr>
			<td><?=$no?></td>
			<td><?=$arr_detail ['ref_program_title']?></td>
			<td><?=$arr_detail ['auditor_name']?></td>
			<td>&nbsp;</td>
			<td><?=$arr_detail ['program_jam']." Jam"?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<?
		}
		?>
	</table>
	<table border='0' cellspacing='0' cellpadding="0" width="100%">
		<tr>
			<td width="20%" rowspan="4">&nbsp;</td>
			<td width="30%">&nbsp;</td>
			<td width="20%" width="20%" rowspan="4">&nbsp;</td>
			<td width="30%">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jakarta, <?=date("d M Y")?></td>
		</tr>
		<tr>
			<td>Direvui</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tanggal:<?=date("d M Y")?><br>PENGENDALI TEKNIS<br><br><br><br><br><u><?=$dalnis['nama']?></u><br><?=$dalnis['jabatan']?></td>
			<td>Disusun Olek<br>KETUAN TIM<br><br><br><br><br><u><?=$katim['nama']?></u><br><?=$katim['jabatan']?></td>
		</tr>
		<tbody id="table_rinci">
		</tbody>
	</table>
</body>
</html>