<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/param_class.php";
include_once "_includes/classes/auditee_class.php";

$assigns = new assign ( $ses_userId );
$auditees = new auditee ( $ses_userId );
$params = new param ( $ses_userId );

$ses_pagu_id = $_SESSION ['ses_pagu_id'];
$ses_assign_id = $_SESSION ['ses_assign_id'];
$ses_auditee_id = $_SESSION ['ses_auditee_id'];

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
    @session_start();
    $_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
    $_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
    $_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
    $key_search = "";
    $val_search = "";
    $val_method = "";
}

$paging_request = "main_page.php?method=assign_lha_pejabat";
$acc_page_request = "assign_pejabat_acc.php";
$list_page_request = "audit_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$view_parrent = "lha_view_parrent.php";
$grid = "grid.php";
$gridHeader = array ("NIP", "Nama", "Pangkat", "Jabatan");
$gridDetail = array ("pic_nip", "pic_name", "pangkat_name", "pic_jabatan_struktur_id");
$gridWidth = array ("15", "20", "15", "15");

$key_by = array ("Nama");
$key_field = array ("pic_name");

$widthAksi = "15";
$iconDetail = "0";
// === end grid ===//

$rs_lha = $auditees->auditee_data_viewlist ( $ses_auditee_id );
$arr_lha = $rs_lha->FetchRow ();

$rs_pagu = $assigns->assign_pagu_viewlist ( $ses_pagu_id );
$arr_pagu = $rs_pagu->FetchRow ();

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Data Atasan Langsung";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->pic_data_viewlist ( $fdata_id );
		$page_title = "Ubah Data Atasan Langsung";
		break;
	case "getdetail" :
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $params->pic_data_viewlist ( $fdata_id );
		$page_title = "Rincian Data Atasan Langsung";
		break;
    case "postadd" :
        $fauditee_id = $ses_auditee_id;
        $fnip = $comfunc->replacetext ( $_POST ["nip"] );
        $fname = $comfunc->replacetext ( $_POST ["name"] );
        $fjabatan = "5c7691bf9e30bd1240646a1590852ca28d7e7175";
        $fmobile = $comfunc->replacetext ( $_POST ["mobile"] );
        $ftelp = $comfunc->replacetext ( $_POST ["telp"] );
        $femail = $comfunc->replacetext ( $_POST ["email_pic"] );
        $fpic_no_sk = $comfunc->replacetext ( $_POST ["pic_no_sk"] );
        $fpic_tgl_sk = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["pic_tgl_sk"]) );
        $ftahun = $arr_pagu['lha_pagu_tahun'];
        $fpangkat = $comfunc->replacetext ( $_POST ["pangkat"] );
        $fjabatan_struktur = $arr_lha['esselon_name'];
        if ($fnip != "" && $fname != "" && $fpangkat!= "" && $fjabatan_struktur != "") {
			$params->pic_pagu_add ( $fnip, $fname, $fjabatan, $fmobile, $ftelp, $femail, $fauditee_id, $fpic_no_sk, $fpic_tgl_sk, $ftahun, $fpangkat, $fjabatan_struktur, $ses_pagu_id);
                $comfunc->js_alert_act ( 3 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
    case "postedit" :
        $fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
        $fauditee_id = $ses_auditee_id;
        $fnip = $comfunc->replacetext ( $_POST ["nip"] );
        $fname = $comfunc->replacetext ( $_POST ["name"] );
        $fjabatan = "5c7691bf9e30bd1240646a1590852ca28d7e7175";
        $fmobile = $comfunc->replacetext ( $_POST ["mobile"] );
        $ftelp = $comfunc->replacetext ( $_POST ["telp"] );
        $femail = $comfunc->replacetext ( $_POST ["email_pic"] );
        $fpic_no_sk = $comfunc->replacetext ( $_POST ["pic_no_sk"] );
        $fpic_tgl_sk = $comfunc->date_db ( $comfunc->replacetext ( $_POST ["pic_tgl_sk"]) );
        $ftahun = $arr_pagu['lha_pagu_tahun'];
        $fpangkat = $comfunc->replacetext ( $_POST ["pangkat"] );
        $fjabatan_struktur = $arr_lha['esselon_name'];
        if ($fnip != "" && $fname != "" && $fpangkat!= "" && $fjabatan_struktur != "") {
			$params->pic_edit ( $fdata_id, $fnip, $fname, $fjabatan, $fmobile, $ftelp, $femail, $fpic_no_sk, $fpic_tgl_sk, $ftahun, $fpangkat, $fjabatan_struktur  );
            $comfunc->js_alert_act ( 1 );
        } else {
            $comfunc->js_alert_act ( 5 );
        }
        ?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$params->pic_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $params->pic_pagu_count_pejabat ( $ses_pagu_id, $key_search, $val_search, $key_field );
		$rs = $params->pic_pagu_viewlist_pejabat ( $ses_pagu_id, $key_search, $val_search, $key_field, $offset, $num_row );
		$rs2 = $params->pic_pagu_viewlist_pejabat ( $ses_pagu_id, $key_search, $val_search, $key_field, $offset, $num_row );
		$arr = $rs2->FetchRow ();
		if($arr['pic_pagu_id'] != '' && $arr['pic_jabatan_id'] == '5c7691bf9e30bd1240646a1590852ca28d7e7175') {
			$iconAdd = "0";
		}
		$page_title = "Daftar Atasan Langsung";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
