<?php
if (@$position == 1) {
    include_once "_includes/DB.class.php";
} else {
    include_once "../_includes/DB.class.php";
}
class finding
{
    public $_db;
    public $userId;
    public function finding($userId = "")
    {
        $this->_db    = new db();
        $this->userId = $userId;
    }
    public function uniq_id()
    {
        return $this->_db->uniqid();
    }

    // finding
    public function finding_tl_count($id_assign, $status = "", $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($status == "1") {
            $condition .= "and finding_status = '8' ";
        }
        //selesai
        if ($status == "2") {
            $condition .= "and finding_status != '8' ";
        }
        //belum selesai

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select count(*) FROM finding_internal
				join auditee on finding_auditee_id = auditee_id
				where finding_assign_id = '" . $id_assign . "' and (finding_status = 3 or finding_status = 4 or finding_status = 8) " . $condition . $condition2;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function finding_tl_nha_count($id_assign, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select count(*) FROM finding_internal
				join auditee on finding_auditee_id = auditee_id
				where finding_assign_id = '" . $id_assign . "'".$condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function finding_tl_view_grid($id_assign, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }
        $sql = "select finding_id, finding_no, finding_judul, auditee_name, finding_date, case finding_status when '8' then 'Selesai' else 'Belum Selesai' end as status, finding_assign_id
				from finding_internal
				join auditee on finding_auditee_id = auditee_id
				where finding_assign_id = '" . $id_assign . "' and (finding_status = 3 or finding_status = 4 or finding_status = 8) " . $condition2 . " order by finding_date DESC
				LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function finding_tl_nha_view_grid($id_assign, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }
        $sql = "select finding_id, finding_no, finding_judul, auditee_name, finding_date, case finding_status when '8' then 'Selesai' else 'Belum Selesai' end as status, finding_assign_id
				from finding_internal
				join auditee on finding_auditee_id = auditee_id
				where finding_assign_id = '" . $id_assign . "' " . $condition2 . " order by finding_date DESC
				LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function finding_count($id_assign, $id_kka = "", $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($id_kka != "") {
            $condition .= "and finding_kka_id = '" . $id_kka . "' ";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select count(*) FROM finding_internal
				left join auditee on finding_auditee_id = auditee_id
				where finding_assign_id = '" . $id_assign . "' " . $condition . $condition2;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function finding_view_grid($id_assign, $id_kka = "", $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($id_kka != "") {
            $condition = "and finding_kka_id = '" . $id_kka . "' ";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select DISTINCT finding_id, finding_no, finding_judul, auditee_name, finding_date, finding_status, finding_assign_id
				from finding_internal
				left join auditee on finding_auditee_id = auditee_id
				where finding_assign_id = '" . $id_assign . "' " . $condition . $condition2 . "
				order by finding_date DESC
				LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function finding_viewlist($id)
    {
        $sql = "select finding_id, finding_auditee_id, finding_no, finding_internal.finding_type_id, finding_judul, finding_date,
				finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_status, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, finding_kka_id,
				auditee_name, finding_type_name, assign_surat_no, finding_sub_id, finding_jenis_id, jenis_temuan_name, jenis_temuan_code, finding_assign_id, finding_nilai, kode_penyebab_name, concat(kode_penyebab_name, ' - ', kode_penyebab_desc) as kode_penyebab_id_name, finding_sub_penyebab_id, finding_sub_sub_penyebab_id, concat(penyebab_sub_sub_code, ' - ' , penyebab_sub_sub_name) as penyebab_sub_sub_code_name, kel_penyebab_code, kel_penyebab_desc, kode_penyebab_name, kode_penyebab_desc, penyebab_sub_sub_code, penyebab_sub_sub_name
				FROM finding_internal
				left join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
				left join par_finding_jenis on finding_jenis_id = jenis_temuan_id
				left join auditee on finding_auditee_id = auditee_id
				left join par_kelompok_penyebab on finding_penyebab_id = kel_penyebab_id
				left join par_kode_penyebab on finding_sub_penyebab_id = kode_penyebab_id
				left join par_kode_penyebab_sub on finding_sub_sub_penyebab_id = penyebab_sub_sub_id
				where finding_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function finding_viewlist2($id)
    {
        $sql = "select finding_id, finding_auditee_id, finding_no, finding_internal.finding_type_id, finding_judul, finding_date,
				finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_status, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, finding_kka_id,
				auditee_name, finding_type_name, assign_surat_no, finding_sub_id, finding_jenis_id, jenis_temuan_name, jenis_temuan_code, finding_assign_id, finding_nilai, kode_penyebab_name, concat(kode_penyebab_name, ' - ', kode_penyebab_desc) as kode_penyebab_id_name
				FROM finding_internal
				left join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
				left join par_finding_jenis on finding_jenis_id = jenis_temuan_id
				left join auditee on finding_auditee_id = auditee_id
				left join par_kode_penyebab on finding_penyebab_id = kode_penyebab_id
				where finding_assign_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function finding_add($id_assign, $finding_auditee, $finding_no, $finding_type, $finding_sub_id, $finding_jenis_id, $finding_judul, $finding_date, $finding_kondisi, $finding_kriteria, $finding_sebab, $finding_akibat, $finding_attachment, $id_kka, $tanggapan_auditee, $tanggapan_auditor, $finding_penyebab_id, $finding_nilai)
    {
        $id = $this->uniq_id();
        $sql = "insert into finding_internal
				(finding_id, finding_assign_id, finding_auditee_id, finding_no, finding_type_id, finding_sub_id, finding_jenis_id, finding_judul, finding_date, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_kka_id, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, finding_nilai)
				values
				('" . $id . "', '" . $id_assign . "', '" . $finding_auditee . "', '" . $finding_no . "', '" . $finding_type . "', '" . $finding_sub_id . "', '" . $finding_jenis_id . "', '" . $finding_judul . "', '" . $finding_date . "', '" . $finding_kondisi . "', '" . $finding_kriteria . "', '" . $finding_sebab . "', '" . $finding_akibat . "', '" . $finding_attachment . "', '" . $id_kka . "', '" . $tanggapan_auditee . "', '" . $tanggapan_auditor . "', '" . $finding_penyebab_id . "', '" . $finding_nilai . "')";

        $sql1 = "insert into finding_internal_matrix
                (finding_id, finding_assign_id, finding_auditee_id, finding_no, finding_type_id, finding_sub_id, finding_jenis_id, finding_judul, finding_date, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_kka_id, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, finding_nilai)
                values
                ('" . $id . "', '" . $id_assign . "', '" . $finding_auditee . "', '" . $finding_no . "', '" . $finding_type . "', '" . $finding_sub_id . "', '" . $finding_jenis_id . "', '" . $finding_judul . "', '" . $finding_date . "', '" . $finding_kondisi . "', '" . $finding_kriteria . "', '" . $finding_sebab . "', '" . $finding_akibat . "', '" . $finding_attachment . "', '" . $id_kka . "', '" . $tanggapan_auditee . "', '" . $tanggapan_auditor . "', '" . $finding_penyebab_id . "', '" . $finding_nilai . "')";

        $aksinyo = "Menambah Temuan No " . $finding_no;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
        $this->_db->_dbquery($sql1);
    }
    public function finding_nha_add($finding_id , $id_assign, $finding_auditee, $finding_no, $finding_type, $finding_sub_id, $finding_jenis_id, $finding_judul, $finding_date, $finding_kondisi, $finding_kriteria, $finding_sebab, $finding_akibat, $finding_attachment, $id_kka, $tanggapan_auditee, $tanggapan_auditor, $finding_penyebab_id, $finding_nilai, $nha_id, $finding_penyebab_sub_id, $finding_penyebab_sub_sub_id)
    {
        $sql = "insert into finding_internal
				(finding_id, finding_assign_id, finding_auditee_id, finding_no, finding_type_id, finding_sub_id, finding_jenis_id, finding_judul, finding_date, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_kka_id, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, finding_nilai, finding_type, finding_nha_id, finding_sub_penyebab_id, finding_sub_sub_penyebab_id)
				values
				('" . $finding_id . "', '" . $id_assign . "', '" . $finding_auditee . "', '" . $finding_no . "', '" . $finding_type . "', '" . $finding_sub_id . "', '" . $finding_jenis_id . "', '" . $finding_judul . "', '" . $finding_date . "', '" . $finding_kondisi . "', '" . $finding_kriteria . "', '" . $finding_sebab . "', '" . $finding_akibat . "', '" . $finding_attachment . "', '" . $id_kka . "', '" . $tanggapan_auditee . "', '" . $tanggapan_auditor . "', '" . $finding_penyebab_id . "', '" . $finding_nilai . "', 'NHA', '" . $nha_id . "', '" . $finding_penyebab_sub_id . "', '" . $finding_penyebab_sub_sub_id . "')";
        
		$aksinyo = "Menambah Temuan No " . $finding_no;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function finding_edit($id, $finding_auditee, $finding_no, $finding_type, $finding_sub_id, $finding_jenis_id, $finding_judul, $finding_date, $finding_kondisi, $finding_kriteria, $finding_sebab, $finding_akibat, $finding_attachment, $tanggapan_auditee, $tanggapan_auditor, $finding_penyebab_id, $finding_nilai, $finding_penyebab_sub_id, $finding_penyebab_sub_sub_id)
    {
        $sql = "update finding_internal set finding_auditee_id = '" . $finding_auditee . "', finding_no = '" . $finding_no . "', finding_type_id = '" . $finding_type . "', finding_sub_id = '" . $finding_sub_id . "', finding_jenis_id = '" . $finding_jenis_id . "', finding_judul = '" . $finding_judul . "', finding_date = '" . $finding_date . "', finding_kondisi = '" . $finding_kondisi . "', finding_kriteria = '" . $finding_kriteria . "', finding_sebab = '" . $finding_sebab . "', finding_akibat = '" . $finding_akibat . "', finding_attachment = '" . $finding_attachment . "', finding_tanggapan_auditee = '" . $tanggapan_auditee . "', finding_tanggapan_auditor = '" . $tanggapan_auditor . "', finding_penyebab_id = '" . $finding_penyebab_id . "', finding_nilai = '" . $finding_nilai . "', finding_sub_penyebab_id = '" . $finding_penyebab_sub_id . "', finding_sub_sub_penyebab_id = '" . $finding_penyebab_sub_sub_id . "'
				where finding_id = '" . $id . "' ";
        $sql1 = "update finding_internal_matrix set finding_auditee_id = '" . $finding_auditee . "', finding_no = '" . $finding_no . "', finding_type_id = '" . $finding_type . "', finding_sub_id = '" . $finding_sub_id . "', finding_jenis_id = '" . $finding_jenis_id . "', finding_judul = '" . $finding_judul . "', finding_date = '" . $finding_date . "', finding_kondisi = '" . $finding_kondisi . "', finding_kriteria = '" . $finding_kriteria . "', finding_sebab = '" . $finding_sebab . "', finding_akibat = '" . $finding_akibat . "', finding_attachment = '" . $finding_attachment . "', finding_tanggapan_auditee = '" . $tanggapan_auditee . "', finding_tanggapan_auditor = '" . $tanggapan_auditor . "', finding_penyebab_id = '" . $finding_penyebab_id . "', finding_nilai = '" . $finding_nilai . "'
                where finding_id = '" . $id . "' ";
        $aksinyo = "Mengubah Temuan Audit No " . $finding_no;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
        $this->_db->_dbquery($sql1);
    }

    public function finding_matrix_edit($id, $finding_kondisi, $finding_sebab, $finding_akibat)
    {
        $sql = "update finding_internal_matrix set finding_kondisi = '" . $finding_kondisi . "', finding_sebab = '" . $finding_sebab . "', finding_akibat = '" . $finding_akibat . "'
                where finding_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function finding_delete($id)
    {
        $sql     = "delete from finding_internal where finding_id = '" . $id . "' ";
        $aksinyo = "Menghapus Temuan Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function finding_update_status($id, $status)
    {
        $sql = "update finding_internal set finding_status = '" . $status . "'
				where finding_id = '" . $id . "' ";
        $sql2 = "update finding_internal_matrix set finding_status = '" . $status . "'
                where finding_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
        $this->_db->_dbquery($sql2);  
    }
    public function finding_komentar_viewlist($id)
    {
        $sql = "select auditor_name, find_comment_desc, find_comment_date
				FROM finding_internal_comment
				left join user_apps on find_comment_user_id = user_id
				left join auditor on user_id_internal = auditor_id
				where find_comment_find_id = '" . $id . "' order by find_comment_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function finding_add_komentar($id, $komentar, $tanggal)
    {
        $sql = "insert into finding_internal_comment
				(find_comment_id, find_comment_find_id, find_comment_user_id, find_comment_desc, find_comment_date)
				values
				('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komentar . "','" . $tanggal . "')";
        $aksinyo = "Mengomentari Temuan dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function cek_posisi($id_assign)
    {
        $sql = "select DISTINCT assign_auditor_id_posisi from assignment_auditor
				left join user_apps on assign_auditor_id_auditor = user_id_internal
				where user_id = '" . $this->userId . "' and assign_auditor_id_assign = '" . $id_assign . "'";
        // echo $sql;
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    // end finding

    //lha
    public function assign_find_open($auditee_id, $tahun)
    {
        $sql = "select DISTINCT assign_id, assign_no_lha, assign_tahun, audit_type_name, assign_date_lha
				from finding_internal
				join assignment on finding_assign_id = assign_id
				join par_audit_type on assign_tipe_id = audit_type_id
				where finding_auditee_id = '" . $auditee_id . "' and finding_status != 8 and assign_tahun < " . $tahun . " ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_jml_temuan_selesai($assign_id)
    {
        $sql = "select count(finding_id) as jml
				from finding_internal
				where finding_assign_id = '" . $assign_id . "' and finding_status = 8 ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    public function get_jml_temuan_proses($assign_id)
    {
        $sql = "select count(finding_id) as jml
				from finding_internal
				where finding_assign_id = '" . $assign_id . "' and finding_status != 8 ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    public function get_temuan_proses($assign_id)
    {
        $sql = "select finding_id, finding_no, finding_judul, finding_kondisi, finding_sebab, finding_akibat, finding_tanggapan_auditee
				from finding_internal
				where finding_assign_id = '" . $assign_id . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    // Temuan Ketidakpatuhan Terhadap Peraturan
    public function get_temuan_proses_1($assign_id)
    {
        $sql = "select finding_internal.finding_id, finding_internal.finding_no, finding_internal.finding_judul, finding_internal.finding_kondisi, finding_internal.finding_sebab, finding_internal.finding_akibat, finding_internal.finding_tanggapan_auditee
				from finding_internal
				left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
				where finding_assign_id = '" . $assign_id . "' and par_finding_type.finding_type_id = '764275e74b69abcec96c2ac76140f7da774249f7'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    // Temuan Kelemahan Sistem Pengendalian Internal
    public function get_temuan_proses_2($assign_id)
    {
        $sql = "select finding_internal.finding_id, finding_internal.finding_no, finding_internal.finding_judul, finding_internal.finding_kondisi, finding_internal.finding_sebab, finding_internal.finding_akibat, finding_internal.finding_tanggapan_auditee
				from finding_internal
				left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
				where finding_assign_id = '" . $assign_id . "' and par_finding_type.finding_type_id = 'dc06264262364cb2318c3282ed7590b1aa45b779'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    // Temuan 3E
    public function get_temuan_proses_3($assign_id)
    {
        $sql = "select finding_internal.finding_id, finding_internal.finding_no, finding_internal.finding_judul, finding_internal.finding_kondisi, finding_internal.finding_sebab, finding_internal.finding_akibat, finding_internal.finding_tanggapan_auditee
				from finding_internal
				left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
				where finding_assign_id = '" . $assign_id . "' and par_finding_type.finding_type_id = 'f307316cfcfebc20fefa473768679afbf39a1de6'";

        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_rekomendasi_proses($find_id)
    {
        $sql = "select rekomendasi_desc
				from rekomendasi_internal
				where rekomendasi_finding_id = '" . $find_id . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_rekomendasi_list($find_id)
    {
        $sql = "select rekomendasi_id, rekomendasi_desc, rekomendasi_status, kode_rek_code
				from rekomendasi_internal
				left join par_kode_rekomendasi on rekomendasi_id_code = kode_rek_id
				where rekomendasi_finding_id = '" . $find_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_rekomendasi_list_matrix($find_id)
    {
        $sql = "select rekomendasi_id, rekomendasi_desc, rekomendasi_status, kode_rek_code
                from rekomendasi_internal_matrix
                left join par_kode_rekomendasi on rekomendasi_id_code = kode_rek_id
                where rekomendasi_finding_id = '" . $find_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_list_temuan_by_aspek($id_aspek, $id_assign, $id_auditee)
    {
        $sql = "select finding_id, finding_judul, finding_kondisi, finding_tanggapan_auditee, finding_kriteria, finding_sebab, finding_akibat, rekomendasi_desc
				from finding_internal
				left join kertas_kerja on finding_kka_id = kertas_kerja_id
				left join program_audit on kertas_kerja_id_program = program_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join rekomendasi_internal on rekomendasi_finding_id = finding_id
				where ref_program_aspek_id = '" . $id_aspek . "' and finding_assign_id = '" . $id_assign . "' and finding_auditee_id = '" . $id_auditee . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function get_aspek_by_temuan($id_assign, $id_auditee)
    {
        $sql = "select distinct aspek_id, aspek_name
				FROM finding_internal
				left join kertas_kerja on finding_kka_id = kertas_kerja_id
				left join program_audit on kertas_kerja_id_program = program_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join par_aspek on ref_program_aspek_id = aspek_id
				where finding_assign_id = '" . $id_assign . "' and finding_auditee_id = '" . $id_auditee . "' and par_aspek.ikhtisar = 'Iya' order by par_aspek.urutan asc ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    //lha
    public function add_tanggapan_auditee($finding_id, $tanggapan_auditee)
    {
        $sql = "update finding_internal set  finding_tanggapan_auditee = '{$tanggapan_auditee}' where finding_id = '{$finding_id}'";
        // $aksinyo = "Berhasil menambah tanggapan auditee!";
        $this->_db->_dbquery($sql);
    }

    //history finding
    public function finding_history_add($id_assign, $finding_auditee, $finding_no, $finding_type, $finding_sub_id, $finding_jenis_id, $finding_judul, $finding_date, $finding_kondisi, $finding_kriteria, $finding_sebab, $finding_akibat, $finding_attachment, $id_kka, $tanggapan_auditee, $tanggapan_auditor, $finding_penyebab_id, $finding_nilai, $finding_history_id, $finding_history_date)
    {
        $sql = "insert into finding_internal_history
				(finding_id, finding_assign_id, finding_auditee_id, finding_no, finding_type_id, finding_sub_id, finding_jenis_id, finding_judul, finding_date, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_kka_id, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, finding_nilai, finding_history_id, finding_history_date)
				values
				('" . $this->uniq_id() . "', '" . $id_assign . "', '" . $finding_auditee . "', '" . $finding_no . "', '" . $finding_type . "', '" . $finding_sub_id . "', '" . $finding_jenis_id . "', '" . $finding_judul . "', '" . $finding_date . "', '" . $finding_kondisi . "', '" . $finding_kriteria . "', '" . $finding_sebab . "', '" . $finding_akibat . "', '" . $finding_attachment . "', '" . $id_kka . "', '" . $tanggapan_auditee . "', '" . $tanggapan_auditor . "', '" . $finding_penyebab_id . "', '" . $finding_nilai . "', '" . $finding_history_id . "', '" . $finding_history_date . "')";
        // $aksinyo = "Menambah Temuan No " . $finding_no;
        $this->_db->_dbquery($sql);
        // echo $sql;
    }
    public function list_date_temuan_history($id)
    {
        $sql  = "SELECT DISTINCT(finding_history_date) FROM finding_internal_history WHERE finding_history_id = '" . $id . "' ORDER BY finding_history_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function count_list_date_finding_history($id)
    {
        $sql = "SELECT COUNT(*) FROM finding_internal_history WHERE  finding_history_id = '" . $id . "'";
        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function list_temuan_by_history_date_by_id($date, $id)
    {
        $sql = "select finding_id, finding_auditee_id, finding_no, finding_internal_history.finding_type_id, finding_judul, finding_date, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_attachment, finding_status, finding_tanggapan_auditee, finding_tanggapan_auditor, finding_penyebab_id, auditee_name, finding_type_name, assign_surat_no, finding_sub_id, finding_jenis_id, jenis_temuan_name, jenis_temuan_code, finding_assign_id, finding_nilai, kode_penyebab_name, finding_history_id
				FROM finding_internal_history
				left join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				left join par_finding_type on finding_internal_history.finding_type_id = par_finding_type.finding_type_id
				left join par_finding_jenis on finding_jenis_id = jenis_temuan_id
				left join auditee on finding_auditee_id = auditee_id
				left join par_kode_penyebab on finding_penyebab_id = kode_penyebab_id
				where finding_history_date = '" . $date . "' AND finding_history_id = '" . $id . "'";

        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_tujuan_by_rinci($id_aspek, $id_assign, $id_auditee)
    {
        $sql = "select pka_intro_tujuan from finding_internal
				left join kertas_kerja on finding_kka_id = kertas_kerja_id
				left join program_audit on kertas_kerja_id_program = program_id
				left join program_audit_intro on pka_intro_id_assign = program_id_assign
				left join ref_program_audit on program_id_ref = ref_program_id
				left join rekomendasi_internal on rekomendasi_finding_id = finding_id
				where ref_program_aspek_id = '" . $id_aspek . "' and finding_assign_id = '" . $id_assign . "' and finding_auditee_id = '" . $id_auditee . "' and program_tahapan = 'PKA Rinci' ";

        $data       = $this->_db->_dbquery($sql);
        $arr_tujuan = $data->FetchRow();

        return $arr_tujuan[0];
    }
}
