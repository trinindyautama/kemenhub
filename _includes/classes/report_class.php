<?php
if (@$position == 1) {
    include_once "_includes/DB.class.php";
} else {
    include_once "../_includes/DB.class.php";
}
class report
{
    public $_db;
    public $userId;
    public function report($userId = "")
    {
        $this->_db    = new db();
        $this->userId = $userId;
    }

    public function uniq_id()
    {
        return $this->_db->uniqid();
    }

    public function report_siklus_risiko($kategori_id, $tahun, $auditee)
    {
        $condition = "";
        if ($auditee != "") {
            $condition = "and penetapan_auditee_id = '" . $auditee . "'";
        }

        $sql = "select identifikasi_id, risk_kategori, identifikasi_no_risiko, identifikasi_nama_risiko, identifikasi_penyebab, identifikasi_selera,
                analisa_bobot_kat_risk, analisa_ri, analisa_bobot_risk, analisa_kemungkinan, analisa_kemungkinan_name, analisa_dampak, analisa_dampak_name, analisa_nilai_ri,
                evaluasi_risiko_residu, evaluasi_komponen, evaluasi_efektifitas, evaluasi_risiko_residu, evaluasi_efektifitas_name, evaluasi_risiko_residu_name,
                penanganan_risiko_id, penanganan_plan, penanganan_date, penanganan_pic_id, risk_penanganan_jenis, pic_name,
                penetapan_auditee_id,
                monitoring_action, monitoring_date, monitoring_plan_action, monitoring_tenggat_waktu
                FROM risk_identifikasi
                left join risk_penetapan on identifikasi_penetapan_id = penetapan_id
                left join par_risk_kategori on identifikasi_kategori_id = risk_kategori_id
                left join par_risk_penanganan on penanganan_risiko_id = risk_penanganan_id
                left join auditee_pic on penanganan_pic_id = pic_id
                where penetapan_status = '2' and identifikasi_kategori_id = '" . $kategori_id . "' and penetapan_tahun = '" . $tahun . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_assignment_id($auditee_id, $tahun)
    {
        $sql = "select assign_id
                FROM assignment
                join assignment_auditee on assign_id = assign_auditee_id_assign
                where assign_auditee_id_auditee = '" . $auditee_id . "' and assign_tahun = '" . $tahun . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    public function get_anggota($assign_id, $auditee_id, $posisi_code)
    {
        $sql = "select auditor_name, jenis_jabatan_sub
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditor_id_auditee = '" . $auditee_id . "' and posisi_code = '" . $posisi_code . "' ";
        $rs              = $this->_db->_dbquery($sql);
        $arr             = $rs->FetchRow();
        $data['nama']    = $arr['auditor_name'];
        $data['jabatan'] = $arr['jenis_jabatan_sub'];
        return $data;
    }

    public function get_anggota1($assign_id, $auditee_id, $inspektur1)
    {
        $sql = "select auditor_name, jenis_jabatan_sub
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                left join par_inspektorat on auditor_unit = inspektorat_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditor_id_auditee = '" . $auditee_id . "' and inspektorat_id = '" . $inspektur1 . "' ";
        echo $sql;
        $rs              = $this->_db->_dbquery($sql);
        $arr             = $rs->FetchRow();
        $data['nama']    = $arr['auditor_name'];
        $data['jabatan'] = $arr['jenis_jabatan_sub'];
        return $data;
    }

    public function get_anggota2($assign_id, $auditee_id, $inspektur2)
    {
        $sql = "select auditor_name, jenis_jabatan_sub
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                left join par_inspektorat on auditor_unit = inspektorat_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditor_id_auditee = '" . $auditee_id . "' and inspektorat_id = '" . $inspektur2 . "' ";
        $rs              = $this->_db->_dbquery($sql);
        $arr             = $rs->FetchRow();
        $data['nama']    = $arr['auditor_name'];
        $data['jabatan'] = $arr['jenis_jabatan_sub'];
        return $data;
    }

    public function get_anggota3($assign_id, $auditee_id, $inspektur3)
    {
        $sql = "select auditor_name, jenis_jabatan_sub
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                left join par_inspektorat on auditor_unit = inspektorat_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditor_id_auditee = '" . $auditee_id . "' and inspektorat_id = '" . $inspektur3 . "' ";
        $rs              = $this->_db->_dbquery($sql);
        $arr             = $rs->FetchRow();
        $data['nama']    = $arr['auditor_name'];
        $data['jabatan'] = $arr['jenis_jabatan_sub'];
        return $data;
    }

    public function get_anggota4($assign_id, $auditee_id, $inspektur4)
    {
        $sql = "select auditor_name, jenis_jabatan_sub
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                left join par_inspektorat on auditor_unit = inspektorat_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditor_id_auditee = '" . $auditee_id . "' and inspektorat_id = '" . $inspektur4 . "' ";
        $rs              = $this->_db->_dbquery($sql);
        $arr             = $rs->FetchRow();
        $data['nama']    = $arr['auditor_name'];
        $data['jabatan'] = $arr['jenis_jabatan_sub'];
        return $data;
    }

    public function get_anggota5($assign_id, $auditee_id, $inspektur5)
    {
        $sql = "select auditor_name, jenis_jabatan_sub
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                left join par_inspektorat on auditor_unit = inspektorat_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditor_id_auditee = '" . $auditee_id . "' and inspektorat_id = '" . $inspektur5 . "' ";
        $rs              = $this->_db->_dbquery($sql);
        $arr             = $rs->FetchRow();
        $data['nama']    = $arr['auditor_name'];
        $data['jabatan'] = $arr['jenis_jabatan_sub'];
        return $data;
    }


    public function assign_program_audit_list($aspek_id, $assign_id, $auditee_id, $tahapan = "")
    {
        $condition = "";
        if ($tahapan != "") {
            $condition .= "and program_tahapan = '" . $tahapan . "'";
        }

        $sql = "select program_id, program_day, auditor_name, assign_kegiatan, kertas_kerja_desc, kertas_kerja_no, kertas_kerja_jam, ref_program_procedure, ref_program_title, program_jam
                FROM program_audit
                left join assignment on program_id_assign = assign_id
                left join ref_program_audit on program_id_ref = ref_program_id
                left join auditor on program_id_auditor = auditor_id
                left join kertas_kerja on kertas_kerja_id_program = program_id
                where ref_program_aspek_id = '" . $aspek_id . "' and program_id_assign = '" . $assign_id . "' and program_id_auditee = '" . $auditee_id . "'" . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function program_kka_list($program_id)
    {
        $sql = "select kertas_kerja_no
                FROM kertas_kerja
                where kertas_kerja_id_program = '" . $program_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function kertas_kerja_list($assign_id, $auditee_id = '', $tahapan = '')
    {
        $condition = "";
        if ($tahapan != "") {
            $condition .= " and program_tahapan = '" . $tahapan . "'";
        }
        if ($auditee_id != "") {
            $condition .= " and program_id_auditee = '" . $auditee_id . "'";
        }
        $sql = "select kertas_kerja_id, kertas_kerja_no, kertas_kerja_desc, kertas_kerja_kesimpulan, auditor_name, ref_program_title, program_tahapan, ref_program_procedure, concat(kertas_kerja_no,' (',auditor_name,')') as lengkap
                FROM kertas_kerja
                left join program_audit on kertas_kerja_id_program = program_id
                left join ref_program_audit on program_id_ref  = ref_program_id
                left join auditor on program_id_auditor = auditor_id
                where program_id_assign = '" . $assign_id . "'" . $condition;
        $data = $this->_db->_dbquery($sql);
        // echo $sql;
        return $data;
    }

    public function assign_list($assign_id)
    {
        $sql = "select assign_id, assign_surat_no, assign_no_lha, assign_date_lha, assign_periode, lha_ringkasan, assign_dasar, lha_metodologi, assign_kegiatan, lha_tujuan_audit, lha_ruanglingkup, lha_batasan, lha_kegiatan, assign_start_date, assign_end_date, lha_strategi_laporan, lha_hasil, lha_status, audit_type_name
                FROM assignment
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join par_audit_type on audit_type_id = assign_tipe_id
                join assignment_lha on assign_id = lha_id_assign
                where assign_id = '" . $assign_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function temuan_list($assign_id, $auditee_id)
    {
        $sql = "select finding_id, finding_judul, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_tanggapan_auditee, finding_tanggapan_auditor
                FROM finding_internal
                where finding_assign_id = '" . $assign_id . "' and finding_auditee_id = '" . $auditee_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function rekomendasi_list($finding_id)
    {
        $sql = "select rekomendasi_id, rekomendasi_desc
                FROM rekomendasi_internal
                where rekomendasi_finding_id = '" . $finding_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function tindak_lanjut_list($finding_id)
    {
        $sql = "select tl_desc
                FROM tindaklanjut_internal
                left join rekomendasi_internal on rekomendasi_id = tl_rek_id
                where rekomendasi_finding_id = '" . $finding_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function plan_data_viewlist($tahun)
    {
        $sql = "select audit_plan_id, audit_plan_start_date, audit_plan_end_date
                FROM audit_plan
                where audit_plan_tahun = '" . $tahun . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function list_auditor_per_assign($tahun, $auditor_id, $tipe_audit)
    {
        $condition = "";
        if ($tahun != "") {
            $condition .= "and assign_tahun = '" . $tahun . "'";
        }

        if ($auditor_id != "") {
            $condition .= "and auditor_id = '" . $auditor_id . "'";
        }

        if ($tipe_audit != "") {
            $condition .= "and assign_tipe_id = '" . $tipe_audit . "'";
        }

        $sql = "select distinct auditor_name, auditor_nip, assign_surat_no, assign_surat_tgl, assign_auditor_start_date, assign_auditor_end_date, assign_tahun, assign_kegiatan, audit_type_name, posisi_name
                FROM assignment_auditor
                join assignment on assign_auditor_id_assign = assign_id
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                left join par_audit_type on assign_tipe_id = audit_type_id
                join auditor on assign_auditor_id_auditor = auditor_id
                where 1=1 " . $condition . " order by assign_auditor_start_date ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    //kma
    public function surat_tugas_viewlist_km($tahun = "", $auditor_id = "", $inspektorat_id = "", $auditee_id = "")
    {
        $condition = "";
        if ($tahun != "") {
            $condition .= " and assign_tahun = '" . $tahun . "' ";
        }

        if ($inspektorat_id == '0') {
            $inspektorat_id = "";
        }

        if ($inspektorat_id != "") {
            $condition .= " and assign_pelaksana_id = '" . $inspektorat_id . "' ";
        }

        if ($auditor_id == "0" || $auditor_id == "") {
            $auditor_id == "";
        }

        if ($auditor_id != "") {
            $condition .= " and assign_auditor_id_auditor = '" . $auditor_id . "'";
        }

        if ($auditee_id != "") {
            $condition .= " and assign_auditee_id_auditee = '" . $auditee_id . "'";
        }

        $sql = "select distinct assign_surat_id_assign, assign_surat_no, assign_kegiatan, assign_start_date, assign_end_date, assign_surat_tgl
                from assignment_surat_tugas
                left join assignment on assign_surat_id_assign = assign_id
                left join assignment_auditor on assign_id = assign_auditor_id_assign
                join assignment_auditee on assign_id = assign_auditee_id_assign " . $condition . " order by assign_created_date desc";
        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_surat_tugas_no($id = "")
    {
        $sql = "select distinct assign_surat_no
                from assignment_surat_tugas
                where assign_surat_id_assign = '" . $id . "'";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    //km06
    public function km06_list($id_assign)
    {
        $sql = "select km06_id, km06_no, km06_tanggal, km06_ket, km06_st, km06_no_rencana, km06_tahun_terakhir, km06_resiko, km06_tujuan, km06_anggaran_ajukan, km06_anggaran_setujui
                from laporan_km_06
                where km06_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km06_add($assign_id, $nomor_kartu, $tanggal_kartu, $ket, $status, $no_rencana, $tahun_terakhir, $resiko, $tujuan, $anggaran_ajukan, $anggaran_setuju)
    {
        $sql     = "insert into laporan_km_06 (km06_id_assign, km06_no, km06_tanggal, km06_ket, km06_st, km06_no_rencana, km06_tahun_terakhir, km06_resiko, km06_tujuan, km06_anggaran_ajukan, km06_anggaran_setujui) values ('" . $assign_id . "','" . $nomor_kartu . "','" . $tanggal_kartu . "','" . $ket . "','" . $status . "','" . $no_rencana . "','" . $tahun_terakhir . "','" . $resiko . "','" . $tujuan . "','" . $anggaran_ajukan . "','" . $anggaran_setuju . "')";
        $aksinyo = "Menambah KM 06 ID Assign " . $assign_id;
        // echo $sql;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km06_edit($id, $nomor_kartu, $tanggal_kartu, $ket, $status, $no_rencana, $tahun_terakhir, $resiko, $tujuan, $anggaran_ajukan, $anggaran_setuju)
    {
        $sql     = "update laporan_km_06 set km06_tanggal = '" . $tanggal_kartu . "', km06_no = '" . $nomor_kartu . "', km06_ket = '" . $ket . "', km06_st = '" . $status . "', km06_no_rencana = '" . $no_rencana . "', km06_tahun_terakhir = '" . $tahun_terakhir . "', km06_resiko = '" . $resiko . "', km06_tujuan = '" . $tujuan . "', km06_anggaran_ajukan = '" . $anggaran_ajukan . "', km06_anggaran_setujui = '" . $anggaran_setuju . "' where km06_id = '" . $id . "' ";
        $aksinyo = "Mengubah KM 06 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function cek_status_km6($id_assign)
    {
        $sql = "select km06_st
                from laporan_km_06
                where km06_id_assign = '" . $id_assign . "' ";
        $rs   = $this->_db->_dbquery($sql);
        $arr  = $rs->FetchRow();
        $data = false;
        if ($arr['km06_st'] == 2) {
            $data = true;
        }

        return $data;
    }
    //km06

    //km08
    public function get_nama_auditor($id)
    {
        $sql = "select auditor_name
                FROM auditor
                where auditor_id = '" . $id . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    // km12
    public function km12_count($id_assign)
    {
        $sql = "select count(*) FROM laporan_km_12
                left join kertas_kerja on km12_id_kka = kertas_kerja_id
                left join program_audit on kertas_kerja_id_program = program_id
                left join auditor on program_id_auditor = auditor_id
                where km12_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function km12_view_grid($id_assign, $offset, $num_row)
    {
        $sql = "select km12_id, km12_masalah, km12_komen, concat(kertas_kerja_no,' (',auditor_name,')') as lengkap_kka, kertas_kerja_id_program
                FROM laporan_km_12
                left join kertas_kerja on km12_id_kka = kertas_kerja_id
                left join program_audit on kertas_kerja_id_program = program_id
                left join auditor on program_id_auditor = auditor_id
                where km12_id_assign = '" . $id_assign . "' LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km12_program_komentar($program_id, $posisi_name)
    {
        $sql = "SELECT distinct program_comment_desc, auditor_name, program_id FROM laporan_km_12
                LEFT JOIN kertas_kerja on km12_id_kka = kertas_kerja_id
                LEFT JOIN program_audit ON kertas_kerja_id_program = program_id
                LEFT JOIN program_audit_comment ON program_comment_program_id = program_id
                LEFT JOIN user_apps ON user_id = program_comment_user_id
                LEFT JOIN auditor ON user_id_internal = auditor_id
                LEFT JOIN assignment ON assign_id = program_id_assign
                LEFT JOIN assignment_auditor ON assign_auditor_id_auditor = auditor_id
                LEFT JOIN par_posisi_penugasan ON posisi_id = assign_auditor_id_posisi
                WHERE program_comment_program_id = '" . $program_id . "' AND posisi_name = '".$posisi_name."'";
        $data = $this->_db->_dbquery($sql);
        // $rs = $data->FetchRow();
        // echo $sql;
        return $data;
    }
    public function km12_data_viewlist($id = "", $id_assign = "")
    {
        $condition = "";
        if ($id != "") {
            $condition = " and km12_id = '" . $id . "' ";
        }

        if ($id_assign != "") {
            $condition = " and km12_id_assign = '" . $id_assign . "' ";
        }

        $sql = "select km12_id, km12_masalah, km12_komen, kertas_kerja_no, auditor_name, km12_id_kka, kertas_kerja_id_program
                FROM laporan_km_12
                left join kertas_kerja on km12_id_kka = kertas_kerja_id
                left join program_audit on kertas_kerja_id_program = program_id
                left join auditor on program_id_auditor = auditor_id
                where 1=1 " . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km12_add($assign_id, $masalah, $kka_id, $jawab)
    {
        $sql     = "insert into laporan_km_12 (km12_id, km12_id_assign, km12_masalah, km12_id_kka, km12_komen, km12_id_user_from) values ('" . $this->uniq_id() . "','" . $assign_id . "','" . $masalah . "','" . $kka_id . "','" . $jawab . "','" . $this->userId . "')";
        $aksinyo = "Menambah Permasalahan KM 12 ID Assign " . $assign_id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km12_edit($id, $masalah, $kka_id, $jawab)
    {
        $sql     = "update laporan_km_12 set km12_masalah = '" . $masalah . "', km12_id_kka = '" . $kka_id . "', km12_komen = '" . $jawab . "' where km12_id = '" . $id . "' ";
        $aksinyo = "Mengubah Permasalahan KM 12 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km12_delete($id)
    {
        $sql     = "delete from laporan_km_12 where km12_id = '" . $id . "' ";
        $aksinyo = "Menghapus Permasalahan KM 12 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    // km12

    //km13
    public function get_data_tao_perAuditor($id_assign, $id_auditor)
    {
        $sql = "select program_id, tao_desc, program_jam, program_start, program_end
                FROM program_audit
                left join ref_program_audit_tao on program_id_tao = tao_id
                where program_id_assign = '" . $id_assign . "' and program_id_auditor = '" . $id_auditor . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    // end km13

    //km14
    public function get_data_st_km14($id_assign, $kode)
    {
        $sql = "select km14_st
                FROM laporan_km_14
                where km14_id_assign = '" . $id_assign . "' and km14_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km14_st'];
    }

    public function get_data_persen_km14($id_assign, $kode)
    {
        $sql = "select km14_progress
                FROM laporan_km_14
                where km14_id_assign = '" . $id_assign . "' and km14_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km14_progress'];
    }

    public function get_data_id_km14($id_assign, $kode)
    {
        $sql = "select km14_id
                FROM laporan_km_14
                where km14_id_assign = '" . $id_assign . "' and km14_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km14_id'];
    }

    public function get_data_ket_km14($id_assign, $kode)
    {
        $sql = "select km14_ket
                FROM laporan_km_14
                where km14_id_assign = '" . $id_assign . "' and km14_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km14_ket'];
    }

    public function insert_km_14($id_assign, $kode, $st, $progres, $ket, $tgl_dalnis, $tgl_katim)
    {
        $sql = "insert into laporan_km_14 (km14_id_assign, km14_kode, km14_st, km14_progress, km14_ket, km14_dalnis, km14_katim) values ('" . $id_assign . "', '" . $kode . "', '" . $st . "', '" . $progres . "', '" . $ket . "', '" . $tgl_dalnis . "', '" . $tgl_katim . "') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_14($id, $st, $progres, $ket, $tgl_dalnis, $tgl_katim)
    {
        $sql = "update laporan_km_14 set km14_st = '" . $st . "', km14_progress = '" . $progres . "', km14_ket = '" . $ket . "', km14_dalnis = '" . $tgl_dalnis . "', km14_katim = '" . $tgl_katim . "' where km14_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function delete_km_14($id)
    {
        $sql = "delete from laporan_km_14 where km14_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function get_data_km14($column, $id_assign, $kode = '')
    {
        $condition = '';

        if ($kode != '') {
            $condition .= "and km14_code = '" . $kode . "'";
        }

        $sql = "select " . $column . "
                FROM laporan_km_14
                where km14_id_assign = '" . $id_assign . "'" . $condition . " LIMIT 1";
        $rs = $this->_db->_dbquery($sql);

        $arr = $rs->FetchRow();

        return $arr[$column];
    }
    //km14

    //km15
    public function get_data_nama_km15($id_assign, $kode)
    {
        $sql = "select km15_nama
                FROM laporan_km_15
                where km15_id_assign = '" . $id_assign . "' and km15_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km15_nama'];
    }

    public function get_data_tgl_mulai_km15($id_assign, $kode)
    {
        $sql = "select km15_tanggal_mulai
                FROM laporan_km_15
                where km15_id_assign = '" . $id_assign . "' and km15_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km15_tanggal_mulai'];
    }

    public function get_data_tgl_selesai_km15($id_assign, $kode)
    {
        $sql = "select km15_tanggal_selesai
                FROM laporan_km_15
                where km15_id_assign = '" . $id_assign . "' and km15_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km15_tanggal_selesai'];
    }

    public function get_data_id_km15($id_assign, $kode)
    {
        $sql = "select km15_id
                FROM laporan_km_15
                where km15_id_assign = '" . $id_assign . "' and km15_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km15_id'];
    }

    public function insert_km_15($id_assign, $kode, $nama, $tanggal_mulai, $tanggal_selesai)
    {
        $sql = "insert into laporan_km_15 (km15_id_assign, km15_kode, km15_nama, km15_tanggal_mulai, km15_tanggal_selesai) values ('" . $id_assign . "', '" . $kode . "', '" . $nama . "', '" . $tanggal_mulai . "', '" . $tanggal_selesai . "') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_15($id, $nama, $tanggal_mulai, $tanggal_selesai)
    {
        $sql = "update laporan_km_15 set km15_nama = '" . $nama . "', km15_tanggal_mulai = '" . $tanggal_mulai . "', km15_tanggal_selesai = '" . $tanggal_selesai . "' where km15_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function get_data_km15($column, $id_assign, $kode = '')
    {
        $condition = '';

        if ($kode != '') {
            $condition .= "and km15_kode = '" . $kode . "'";
        }

        $sql = "select " . $column . "
                FROM laporan_km_15
                where km15_id_assign = '" . $id_assign . "'" . $condition . " LIMIT 1";
        $rs = $this->_db->_dbquery($sql);

        $arr = $rs->FetchRow();

        return $arr[$column];
    }
    //km15

    // km16
    public function km16_count($id_assign)
    {
        $sql = "select count(*) FROM laporan_km_16
                left join kertas_kerja on km16_id_kka = kertas_kerja_id
                left join program_audit on kertas_kerja_id_program = program_id
                left join auditor on km16_id_user_to = auditor_id
                where km16_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function km16_view_grid($id_assign, $offset, $num_row)
    {
        $sql = "select km16_id, km16_lha, km16_masalah, km16_komen, kertas_kerja_no, auditor_name, km16_ket, km16_status
                FROM laporan_km_16
                left join kertas_kerja on km16_id_kka = kertas_kerja_id
                left join program_audit on kertas_kerja_id_program = program_id
                left join auditor on km16_id_user_to = auditor_id
                where km16_id_assign = '" . $id_assign . "' LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km16_data_viewlist($id = "", $id_assign = "")
    {
        $condition = "";
        if ($id != "") {
            $condition .= " and km16_id = '" . $id . "' ";
        }

        if ($id_assign != "") {
            $condition .= " and km16_id_assign = '" . $id_assign . "' ";
        }

        $sql = "select km16_id, km16_lha, km16_masalah, km16_komen, kertas_kerja_no, auditor_name, km16_id_kka, km16_ket, km16_id_user_to
                FROM laporan_km_16
                left join kertas_kerja on km16_id_kka = kertas_kerja_id
                left join program_audit on kertas_kerja_id_program = program_id
                left join auditor on km16_id_user_to = auditor_id
                where 1=1 " . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km16_add($assign_id, $lha, $masalah, $kka_id, $jawaban, $auditor_id, $ket)
    {
        $sql     = "insert into laporan_km_16 (km16_id, km16_id_assign, km16_lha, km16_masalah, km16_id_kka, km16_komen, km16_id_user_to, km16_ket, km16_id_user_from) values ('" . $this->uniq_id() . "', '" . $assign_id . "', '" . $lha . "', '" . $masalah . "', '" . $kka_id . "', '" . $jawaban . "', '" . $auditor_id . "', '" . $ket . "', '" . $this->userId . "') ";
        $aksinyo = "Menambah Permasalahan KM 16 ID Assign " . $assign_id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km16_edit($id, $lha, $masalah, $kka_id, $jawaban, $auditor_id, $ket)
    {
        $sql     = "update laporan_km_16 set km16_lha = '" . $lha . "', km16_masalah = '" . $masalah . "', km16_id_kka = '" . $kka_id . "', km16_komen = '" . $jawaban . "', km16_id_user_to = '" . $auditor_id . "', km16_ket = '" . $ket . "' where km16_id = '" . $id . "' ";
        $aksinyo = "Mengubah Permasalahan KM 16 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km16_delete($id)
    {
        $sql     = "delete from laporan_km_16 where km16_id = '" . $id . "' ";
        $aksinyo = "Menghapus Permasalahan KM 16 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km16_update_status($id, $status)
    {
        $sql     = "update laporan_km_16 set km16_status= '" . $status . "' where km16_id = '" . $id . "' ";
        $aksinyo = "Mengubah Satus KM 16 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km16_add_komentar($id, $komentar, $tanggal)
    {
        $sql = "insert into laporan_km_16_comment
                (km16_comment_id, km16_comment_id_km, km16_comment_user_id, km16_comment_desc, km16_comment_date)
                values
                ('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komentar . "','" . $tanggal . "')";
        $this->_db->_dbquery($sql);
    }
    public function km16_komentar_viewlist($id)
    {
        $sql = "select auditor_name, km16_comment_desc, km16_comment_date
                FROM laporan_km_16_comment
                left join user_apps on km16_comment_user_id = user_id
                left join auditor on user_id_internal = auditor_id
                where km16_comment_id_km = '" . $id . "' order by km16_comment_date ASC ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    // km16

    // km21
    public function km21_count($id_assign)
    {
        $sql = "select count(*) FROM laporan_km_21
                where km21_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function km21_view_grid($id_assign, $offset, $num_row)
    {
        $sql = "select km21_id, km21_date, km21_daftar_hadir, km21_jml_temuan_before, km21_jml_tl, km21_jml_temuan_after
                FROM laporan_km_21
                where km21_id_assign = '" . $id_assign . "' LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km21_data_viewlist($id = "", $id_assign = "")
    {
        $condition = "";
        if ($id != "") {
            $condition .= " and km21_id = '" . $id . "' ";
        }

        if ($id_assign != "") {
            $condition .= " and km21_id_assign = '" . $id_assign . "' ";
        }

        $sql = "select km21_id, km21_id_assign, km21_date, km21_periode, km21_jml_temuan_before, km21_nilai_temuan_before, km21_jml_tl, km21_nilai_tl, km21_jml_temuan_after, km21_nilai_temuan_after, km21_daftar_hadir, FROM_UNIXTIME(km21_date, '%d-%m-%Y') as tanggal
                from laporan_km_21
                where 1=1 " . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km21_add($assign_id, $tanggal, $periode, $jml_temuan_before, $nilai_temuan_before, $jml_tl, $nilai_tl, $jml_temuan_after, $nilai_temuan_after, $daftar_hadir)
    {
        $sql     = "insert into laporan_km_21 (km21_id, km21_id_assign, km21_date, km21_periode, km21_jml_temuan_before, km21_nilai_temuan_before, km21_jml_tl, km21_nilai_tl, km21_jml_temuan_after, km21_nilai_temuan_after, km21_daftar_hadir) values ('" . $this->uniq_id() . "', '" . $assign_id . "', '" . $tanggal . "', '" . $periode . "', '" . $jml_temuan_before . "', '" . $nilai_temuan_before . "', '" . $jml_tl . "', '" . $nilai_tl . "', '" . $jml_temuan_after . "', '" . $nilai_temuan_after . "', '" . $daftar_hadir . "') ";
        $aksinyo = "Menambah BAP KM 21 ID Assign " . $assign_id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km21_edit($id, $tanggal, $periode, $jml_temuan_before, $nilai_temuan_before, $jml_tl, $nilai_tl, $jml_temuan_after, $nilai_temuan_after, $daftar_hadir)
    {
        $sql     = "update laporan_km_21 set km21_date = '" . $tanggal . "', km21_periode = '" . $periode . "', km21_jml_temuan_before = '" . $jml_temuan_before . "', km21_nilai_temuan_before = '" . $nilai_temuan_before . "', km21_jml_tl = '" . $jml_tl . "', km21_nilai_tl = '" . $nilai_tl . "', km21_jml_temuan_after = '" . $jml_temuan_after . "', km21_nilai_temuan_after = '" . $nilai_temuan_after . "', km21_daftar_hadir = '" . $daftar_hadir . "' where km21_id = '" . $id . "' ";
        $aksinyo = "Mengubah BAP KM 21 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km21_delete($id)
    {
        $sql     = "delete from laporan_km_21 where km21_id = '" . $id . "' ";
        $aksinyo = "Menghapus BAP KM 21 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    // km21

    //km17
    public function get_data_st_km17($id_assign, $kode)
    {
        $sql = "select km17_st
                FROM laporan_km_17
                where km17_id_assign = '" . $id_assign . "' and km17_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km17_st'];
    }

    public function get_data_id_km17($id_assign, $kode)
    {
        $sql = "select km17_id
                FROM laporan_km_17
                where km17_id_assign = '" . $id_assign . "' and km17_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km17_id'];
    }

    public function get_data_ket_km17($id_assign, $kode)
    {
        $sql = "select km17_ket
                FROM laporan_km_17
                where km17_id_assign = '" . $id_assign . "' and km17_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km17_ket'];
    }

    public function insert_km_17($id_assign, $kode, $st, $ket, $dalnis, $katim)
    {
        $sql = "insert into laporan_km_17 (km17_id_assign, km17_kode, km17_st, km17_ket, km17_dalnis, km17_katim) values ('" . $id_assign . "', '" . $kode . "', '" . $st . "', '" . $ket . "', '".$dalnis."', '".$katim."') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_17($id, $st, $ket, $dalnis, $katim)
    {
        $sql = "update laporan_km_17 set km17_st = '" . $st . "', km17_ket = '" . $ket . "', km17_dalnis = '" . $dalnis . "', km17_katim = '" . $katim . "' where km17_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function delete_km_17($id)
    {
        $sql = "delete from laporan_km_17 where km17_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function get_data_km17($column, $id_assign, $kode = '')
    {
        $condition = '';

        if ($kode != '') {
            $condition .= "and km17_kode = '" . $kode . "'";
        }

        $sql = "select " . $column . "
                FROM laporan_km_17
                where km17_id_assign = '" . $id_assign . "'" . $condition . " LIMIT 1";
        $rs = $this->_db->_dbquery($sql);

        $arr = $rs->FetchRow();

        return $arr[$column];
    }
    //km17

    //km27
    public function get_data_km27($id_assign)
    {
        $sql = "select km27_id, km27_nomor, km27_lampiran, km27_perihal
                FROM laporan_km_27
                where km27_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function insert_km_27($assign_id, $nomor, $lampiran, $perihal)
    {
        $sql = "insert into laporan_km_27 (km27_id_assign, km27_nomor, km27_lampiran, km27_perihal) values ('" . $assign_id . "', '" . $nomor . "', '" . $lampiran . "', '" . $perihal . "') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_27($id, $nomor, $lampiran, $perihal)
    {
        $sql = "update laporan_km_27 set km27_nomor = '" . $nomor . "', km27_lampiran = '" . $lampiran . "', km27_perihal = '" . $perihal . "' where km27_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    //km27

    // km26
    public function km26_count($id_assign)
    {
        $sql = "select count(*) FROM laporan_km_26
                where km26_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function km26_view_grid($id_assign, $offset, $num_row)
    {
        $sql = "select km26_id, km26_no, auditor_name, km26_tentang_file, km26_tgl_pinjam, km26_tgl_kembali
                FROM laporan_km_26
                left join auditor on km26_id_auditor = auditor_id
                where km26_id_assign = '" . $id_assign . "' LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km26_data_viewlist($id = "", $id_assign = "")
    {
        $condition = "";
        if ($id != "") {
            $condition .= " and km26_id = '" . $id . "' ";
        }

        if ($id_assign != "") {
            $condition .= " and km26_id_assign = '" . $id_assign . "' ";
        }

        $sql = "select km26_id, km26_id_assign, km26_no, km26_id_auditor, km26_tgl_pinjam, km26_tgl_kembali, km26_tentang_file, km26_no_dosier, km26_no_urut, km26_disetujui, km26_tgl_disetujui, km26_petugas, km26_tgl_petugas, auditor_name
                from laporan_km_26
                left join auditor on km26_id_auditor = auditor_id
                left join assignment on km26_id_assign = assign_id
                where 1=1 " . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km26_add($assign_id, $no_bon, $anggota_id, $tanggal_pinjam, $tanggal_kembalikan, $file_tentang, $no_dosier, $no_urut, $disetujui, $tgl_disetujui, $fpetugas, $tgl_petugas)
    {
        $sql     = "insert into laporan_km_26 (km26_id, km26_id_assign, km26_no, km26_id_auditor, km26_tgl_pinjam, km26_tgl_kembali, km26_tentang_file, km26_no_dosier, km26_no_urut, km26_disetujui, km26_tgl_disetujui, km26_petugas, km26_tgl_petugas) values ('" . $this->uniq_id() . "', '" . $assign_id . "', '" . $no_bon . "', '" . $anggota_id . "', '" . $tanggal_pinjam . "', '" . $tanggal_kembalikan . "', '" . $file_tentang . "', '" . $no_dosier . "', '" . $no_urut . "', '" . $disetujui . "', '" . $tgl_disetujui . "', '" . $fpetugas . "', '" . $tgl_petugas . "') ";
        $aksinyo = "Menambah data peminjaman berkas KM 26 ID Assign " . $assign_id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km26_edit($id, $no_bon, $anggota_id, $tanggal_pinjam, $tanggal_kembalikan, $file_tentang, $no_dosier, $no_urut, $disetujui, $tgl_disetujui, $fpetugas, $tgl_petugas)
    {
        $sql     = "update laporan_km_26 set km26_no = '" . $no_bon . "', km26_id_auditor = '" . $anggota_id . "', km26_tgl_pinjam = '" . $tanggal_pinjam . "', km26_tgl_kembali = '" . $tanggal_kembalikan . "', km26_tentang_file = '" . $file_tentang . "', km26_no_dosier = '" . $no_dosier . "', km26_no_urut = '" . $no_urut . "', km26_disetujui = '" . $disetujui . "', km26_tgl_disetujui = '" . $tgl_disetujui . "', km26_petugas = '" . $fpetugas . "', km26_tgl_petugas = '" . $tgl_petugas . "' where km26_id = '" . $id . "' ";
        $aksinyo = "Mengubah data peminjaman berkas KM 26 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km26_delete($id)
    {
        $sql     = "delete from laporan_km_26 where km26_id = '" . $id . "' ";
        $aksinyo = "Menghapus data peminjaman berkas KM 26 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    // km26

    //km28
    public function get_data_km28($id_assign)
    {
        $sql = "select km28_id, km28_kepada, km28_dari, km28_perihal, km28_tanggal
                FROM laporan_km_28
                where km28_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function insert_km_28($assign_id, $kepada, $dari, $perihal, $tanggal)
    {
        $sql = "insert into laporan_km_28 (km28_id_assign, km28_kepada, km28_dari, km28_perihal, km28_tanggal) values ('" . $assign_id . "', '" . $kepada . "', '" . $dari . "', '" . $perihal . "', '" . $tanggal . "') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_28($id, $kepada, $dari, $perihal, $tanggal)
    {
        $sql = "update laporan_km_28 set km28_kepada = '" . $kepada . "', km28_dari = '" . $dari . "', km28_perihal = '" . $perihal . "', km28_tanggal = '" . $tanggal . "' where km28_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    //km28

    // km29
    public function km29_count($id_assign)
    {
        $sql = "select count(*) FROM laporan_km_29
                where km29_id_assign = '" . $id_assign . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function km29_view_grid($id_assign, $offset, $num_row)
    {
        $sql = "select km29_id, penilai.auditor_name, dinilai.auditor_name, km29_pelayanan, km29_integritas, km29_komitmen, km29_disiplin, km29_kerjasama, km29_kepemimpinan
                FROM laporan_km_29
                left join auditor as penilai on km29_id_penilai = penilai.auditor_id
                left join auditor as dinilai on km29_id_dinilai = dinilai.auditor_id
                where km29_id_assign = '" . $id_assign . "' LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km29_data_viewlist($id = "", $id_assign = "")
    {
        $condition = "";
        if ($id != "") {
            $condition .= " and km29_id = '" . $id . "' ";
        }

        if ($id_assign != "") {
            $condition .= " and km29_id_assign = '" . $id_assign . "' ";
        }

        $sql = "select km29_id, km29_id_assign, km29_id_penilai, km29_id_dinilai, km29_pelayanan, km29_integritas, km29_komitmen, km29_disiplin, km29_kerjasama, km29_kepemimpinan, auditor_name
                from laporan_km_29
                left join auditor on km29_id_penilai = auditor_id
                where 1=1 " . $condition;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function km29_add($assign_id, $penilai, $dinilai, $pelayanan, $integritas, $komitmen, $disiplin, $kerjasama, $kepemimpinan, $tgl)
    {
        $sql     = "insert into laporan_km_29 (km29_id, km29_id_assign, km29_id_penilai, km29_id_dinilai, km29_pelayanan, km29_integritas, km29_komitmen, km29_disiplin, km29_kerjasama, km29_kepemimpinan, km29_date) values ('" . $this->uniq_id() . "', '" . $assign_id . "', '" . $penilai . "', '" . $dinilai . "', '" . $pelayanan . "', '" . $integritas . "', '" . $komitmen . "', '" . $disiplin . "', '" . $kerjasama . "', '" . $kepemimpinan . "', '" . $tgl . "') ";
        $aksinyo = "Menambah data Penilaian KM 29 ID Assign " . $assign_id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km29_edit($id, $dinilai, $pelayanan, $integritas, $komitmen, $disiplin, $kerjasama, $kepemimpinan, $tgl)
    {
        $sql     = "update laporan_km_29 set km29_id_dinilai = '" . $dinilai . "', km29_pelayanan = '" . $pelayanan . "', km29_integritas = '" . $integritas . "', km29_komitmen = '" . $komitmen . "', km29_disiplin = '" . $disiplin . "', km29_kerjasama = '" . $kerjasama . "', km29_kepemimpinan = '" . $kepemimpinan . "', km29_date = '" . $tgl . "' where km29_id = '" . $id . "' ";
        $aksinyo = "Mengubah data Penilaian KM 29 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km29_delete($id)
    {
        $sql     = "delete from laporan_km_29 where km29_id = '" . $id . "' ";
        $aksinyo = "Menghapus data Penilaian KM 29 ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function km29_cek_owner($id, $id_auditor)
    {
        $sql = "select count(km29_id) from laporan_km_29 where km29_id = '" . $id . "' and km29_id_penilai = '" . $id_auditor . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function km29_cek_jml($id_auditor, $id_assign)
    {
        $sql = "select count(km29_id) as jml
                from laporan_km_29
                where km29_id_dinilai = '" . $id_auditor . "' and km29_id_assign = '" . $id_assign . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function get_penilai($id_auditor, $id_assign)
    {
        $sql = "select distinct km29_id_penilai, auditor_name, km29_date
                from laporan_km_29
                left join auditor on km29_id_penilai = auditor_id
                left join assignment_auditor on auditor_id = assign_auditor_id_auditor
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where km29_id_dinilai = '" . $id_auditor . "' and km29_id_assign = '" . $id_assign . "' order by posisi_sort";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function get_nilai($id_penilai, $id_auditor, $assign_id, $field)
    {
        $sql = "select sum(" . $field . ") as nilai
                from laporan_km_29
                where km29_id_dinilai = '" . $id_auditor . "' and km29_id_assign = '" . $assign_id . "' and km29_id_penilai = '" . $id_penilai . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    // km29

    //km30
    public function assign_list_by_auditor($auditor_id)
    {
        $sql = "select DISTINCT assign_id, assign_surat_no, assign_tugas
                FROM assignment
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join assignment_auditor on assign_id = assign_auditor_id_assign
                where assign_auditor_id_auditor = '" . $auditor_id . "' order by assign_start_date ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function get_nilai_all($id_auditor, $assign_id)
    {
        $sql = "select sum(km29_pelayanan) as nilai_km29_pelayanan, sum(km29_integritas) as nilai_km29_integritas, sum(km29_komitmen) as nilai_km29_komitmen, sum(km29_disiplin) as nilai_km29_disiplin, sum(km29_kerjasama) as nilai_km29_kerjasama, sum(km29_kepemimpinan) as nilai_km29_kepemimpinan
                from laporan_km_29
                where km29_id_dinilai = '" . $id_auditor . "' and km29_id_assign = '" . $assign_id . "' ";
        $rs        = $this->_db->_dbquery($sql);
        $arr       = $rs->FetchRow();
        $total     = $arr[0] + $arr[1] + $arr[2] + $arr[3] + $arr[4] + $arr[5];
        $rata_rata = round($total / 6);
        return $rata_rata;
    }
    //end km30
    public function get_data_st_km10($id_assign, $kode)
    {
        $sql = "select km10_st
                FROM laporan_km_10
                where km10_id_assign = '" . $id_assign . "' and km10_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km10_st'];
    }

    public function get_data_persen_km10($id_assign, $kode)
    {
        $sql = "select km10_progress
                FROM laporan_km_10
                where km10_id_assign = '" . $id_assign . "' and km10_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km10_progress'];
    }

    public function get_data_km10($id_assign, $column)
    {
        $sql = "select $column
                FROM laporan_km_10
                where km10_id_assign = '" . $id_assign . "' limit 1 ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[$column];
    }

    public function get_data_id_km10($id_assign, $kode)
    {
        $sql = "select km10_id
                FROM laporan_km_10
                where km10_id_assign = '" . $id_assign . "' and km10_kode = '" . $kode . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['km10_id'];
    }

    public function insert_km_10($id_assign, $kode, $st, $progres, $tanggal)
    {
        $sql = "insert into laporan_km_10 (km10_id_assign, km10_kode, km10_st, km10_progress, km10_date) values ('" . $id_assign . "', '" . $kode . "', '" . $st . "', '" . $progres . "', '".$tanggal."') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_10($id, $st, $progres, $tanggal)
    {
        $sql = "update laporan_km_10 set km10_st = '" . $st . "', km10_progress = '" . $progres . "', km10_date = '".$tanggal."' where km10_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function delete_km_10($id)
    {
        $sql = "delete km10_id from laporan_km_10 where km10_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function get_km_11($id)
    {
        $sql  = "select km11_id, km11_hari, km11_tanggal, km11_waktu, km11_tim_auditee, km11_ttd_auditee, km11_prosedur, km11_survei, km11_penyelesaian_awal, km11_penyelesaian_akhir, km11_pic, km11_ket_tambahan, km11_date, km11_auditor from laporan_km_11 where km11_id_assign = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function insert_km_11($id_assign, $hari, $tanggal, $waktu, $tim_auditee, $ttd_auditee, $survei_prosedur, $survei_pendahuluan, $penyelesaian_awal, $penyelesaian_akhir, $pic_auditee, $ket_tambahan, $date, $auditor)
    {
        $sql = "insert into laporan_km_11 (km11_id_assign, km11_hari, km11_tanggal, km11_waktu, km11_tim_auditee, km11_ttd_auditee, km11_prosedur, km11_survei, km11_penyelesaian_awal, km11_penyelesaian_akhir, km11_pic, km11_ket_tambahan, km11_date, km11_auditor) values ('" . $id_assign . "', '" . $hari . "', '" . $tanggal . "', '" . $waktu . "', '" . $tim_auditee . "', '" . $ttd_auditee . "', '" . $survei_prosedur . "', '" . $survei_pendahuluan . "', '" . $penyelesaian_awal . "', '" . $penyelesaian_akhir . "', '" . $pic_auditee . "', '" . $ket_tambahan . "', '" . $date . "', '" . $auditor . "') ";
        $this->_db->_dbquery($sql);
    }

    public function update_km_11($id, $hari, $tanggal, $waktu, $tim_auditee, $ttd_auditee, $survei_prosedur, $survei_pendahuluan, $penyelesaian_awal, $penyelesaian_akhir, $pic_auditee, $ket_tambahan, $date, $auditor)
    {
        $sql = "update laporan_km_11 set km11_hari = '" . $hari . "', km11_tanggal = '" . $tanggal . "', km11_waktu = '" . $waktu . "', km11_tim_auditee = '" . $tim_auditee . "', km11_ttd_auditee = '" . $ttd_auditee . "', km11_prosedur = '" . $survei_prosedur . "', km11_survei = '" . $survei_pendahuluan . "', km11_penyelesaian_awal = '" . $penyelesaian_awal . "', km11_penyelesaian_akhir = '" . $penyelesaian_akhir . "', km11_pic = '" . $pic_auditee . "', km11_ket_tambahan = '" . $ket_tambahan . "', km11_date = '".$date."', km11_auditor = '".$auditor."' where km11_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }

    public function get_lha_no($id = "")
    {
        $sql = "select distinct lha_no
                from assignment_lha
                where lha_id_assign = '" . $id . "' ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function get_team_penugasan($assign_id, $id_auditor = "")
    {
        $condition = "";
        if ($id_auditor != "") {
            $condition = " and auditor_id != '" . $id_auditor . "' ";
        }

        $sql = "select auditor_id, auditor_name, auditor_foto
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where assign_auditor_id_assign = '" . $assign_id . "' " . $condition . " order by posisi_sort, auditor_name ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    //kma 7
    public function getPosPenugasanKatimAtim(array $array, $clause = '')
    {
        $arr = array();

        if (count($array) > 0 && $clause != '') {
            foreach ($array as $key => $value) {
                $arr[] = "'{$array[$key]}'";
            }
            $arrClause = array('IN', 'NOT IN');
            if (in_array($clause, $arrClause)) {
                $sql  = "SELECT * FROM par_posisi_penugasan WHERE posisi_id " . $clause . " (" . implode(',', $arr) . ") ORDER BY posisi_sort";
                $data = $this->_db->_dbquery($sql);
            } else {
                echo 'clause tidak sesuai pilih (IN / NOT IN)';
            }
        } else {
            $sql  = "SELECT * FROM par_posisi_penugasan ORDER BY posisi_sort";
            $data = $this->_db->_dbquery($sql);
        }

        return $data;
    }

    public function count_auditor_per_assign_in($assign_id)
    {
        $sql = "select count(*)
                from assignment_auditor
                join assignment on assign_auditor_id_assign = assign_id
                left join auditee on assign_auditor_id_auditee = auditee_id
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where assign_auditor_id_assign = '" . $assign_id . "' and posisi_id in ('8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '6a70c2a39af30df978a360e556e1102a2a0bdc02')";
        // echo $sql;
        $data = $this->_db->_dbquery($sql);

        $rs = $data->FetchRow();

        return $rs[0];
    }

    public function getNamaKatimAtimByAssignPos($assign_id, $pos_id)
    {
        $sql = "select auditor_id, auditor_name, auditor_foto
                FROM assignment_auditor
                join auditor on assign_auditor_id_auditor = auditor_id
                join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where assign_auditor_id_assign = '" . $assign_id . "' and posisi_id = '" . $pos_id . "' order by posisi_sort, auditor_name ";
        $data = $this->_db->_dbquery($sql);
        // echo $sql;
        return $data;
    }

    public function insert_km_7($assign_id, $auditor_id_1, $auditor_id_2, $auditor_id_3, $date_1, $date_2, $time_1, $time_2, $kode, $cost, $created_by, $approved_by, $created_date)
    {
        $sql = "INSERT INTO laporan_km_7 (km7_assign_id, km7_auditor_id_1, km7_auditor_id_2, km7_auditor_id_3, km7_code, km7_date_1, km7_date_2, km7_time_1, km7_time_2, km7_cost, km7_created_by, km7_approved_by, km7_created_date) VALUES ('" . $assign_id . "', '" . $auditor_id_1 . "', '" . $auditor_id_2 . "', '" . $auditor_id_3 . "', '" . $kode . "',  '" . $date_1 . "', '" . $date_2 . "', '" . $time_1 . "', '" . $time_2 . "', '" . $cost . "', '" . $created_by . "', '" . $approved_by . "', '" . $created_date . "')";
        // echo $sql;
        $this->_db->_dbquery($sql);
    }

    public function get_data_km7($column, $id_assign, $kode = '')
    {
        $condition = '';

        if ($kode != '') {
            $condition .= "and km7_code = '" . $kode . "'";
        }

        $sql = "select " . $column . "
                FROM laporan_km_7
                where km7_assign_id = '" . $id_assign . "'" . $condition . " LIMIT 1";
        $rs = $this->_db->_dbquery($sql);

        $arr = $rs->FetchRow();

        return $arr[$column];
    }

    public function update_km_7($id, $auditor_id_1, $auditor_id_2, $auditor_id_3, $date_1, $date_2, $time_1, $time_2, $cost, $created_by, $approved_by, $created_date)
    {
        $sql = "UPDATE laporan_km_7 SET km7_auditor_id_1 = '" . $auditor_id_1 . "', km7_auditor_id_2 = '" . $auditor_id_2 . "', km7_auditor_id_3 = '" . $auditor_id_3 . "', km7_date_1 = '" . $date_1 . "', km7_date_2 = '" . $date_2 . "', km7_time_1 = '" . $time_1 . "', km7_time_2 = '" . $time_2 . "', km7_cost = '" . $cost . "', km7_created_by = '" . $created_by . "', km7_approved_by = '" . $approved_by . "', km7_created_date = '" . $created_date . "' WHERE km7_id = '" . $id . "'";

        $this->_db->_dbquery($sql);
    }

    public function sum_km_7_per_assign($assign_id, $column)
    {
        $columns = array('km7_cost', 'km7_time');

        if (in_array($column, $columns)) {
            $sql = "SELECT SUM($column) FROM laporan_km_7 WHERE km7_assign_id = '" . $assign_id . "'";

            $data = $this->_db->_dbquery($sql);
        } else {
            echo 'kolom hanya berisi km7_cost dan km7_time';
        }

        $arr = $data->FetchRow();

        return $arr[0];
    }

    public function sum_biaya_auditor($id = "")
    {
        $sql = "select sum(anggota_assign_detil_total)
                from assignment_auditor_detil
                join assignment_auditor on anggota_assign_detil_id = assign_auditor_id
                where assign_auditor_id_assign = '" . $id . "' group by assign_auditor_id_assign ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    public function get_uang_detil_anggota_by_bulan($bulan, $tahun, $kode_sbu)
    {
        $sql = "select FROM_UNIXTIME(assign_auditor_start_date, '%m-%y') as bulan, sum(`anggota_assign_detil_total`) as biaya
                from assignment_auditor_detil
                join assignment_auditor on anggota_assign_detil_id = assign_auditor_id
                where FROM_UNIXTIME(assign_auditor_start_date, '%m')='" . $bulan . "' and FROM_UNIXTIME(assign_auditor_start_date, '%Y') = '" . $tahun . "' and anggota_assign_detil_kode_sbu like '%" . $kode_sbu . "%' group by bulan ";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr['biaya'];
    }
	
	// rekap temuan hasil audit
	function list_rekap_temuan_hasil_audit($tahun) {
        $condition = "";
		if($tahun!="") $condition .= " and assign_tahun = '".$tahun."' ";
        $sql = "select distinct assign_id, assign_tahun, assign_surat_no, assign_surat_tgl, propinsi_name, auditee_name
				FROM assignment
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				left join assignment_auditee on assign_id = assign_auditee_id_assign
				left join auditee on assign_auditee_id_auditee = auditee_id
				left join par_propinsi on auditee_propinsi_id = propinsi_id
                where 1=1 ".$condition." order by assign_tahun desc ";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
	
	
	function list_temuan_per_assign($id) {
    	$sql = "select distinct assign_id, assign_tahun, assign_surat_no, assign_surat_tgl, propinsi_name, auditee_name
				FROM assignment_nha
				left join assignment on nha_assign_id = assign_id
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				left join assignment_auditee on assign_id = assign_auditee_id_assign
				left join auditee on assign_auditee_id_auditee = auditee_id
				left join par_propinsi on auditee_propinsi_id = propinsi_id
				where nha_assign_id = ".$id." order by assign_tahun desc ";
 		$data = $this->_db->_dbquery ( $sql );
        return $data;
    }
	
	function list_temuan_hasil_audit($id) {
        $sql = "select distinct finding_id, finding_judul
				from finding_internal
				where finding_assign_id = '".$id."'";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
	
	function count_tl_per_rekomendasi($id) {
        $sql = "select count(tl_id)
				from tindaklanjut_internal
				inner join rekomendasi_internal on tl_rek_id = rekomendasi_id 
				inner join finding_internal on rekomendasi_finding_id = finding_id 
				where finding_id  = '".$id."'";
        $data = $this->_db->_dbquery ( $sql );
        $arr = $data->FetchRow();

        return $arr[0];
    }
	
	function get_rekomendasi_per_temuan_list($id) {
        $sql = "select rekomendasi_id, rekomendasi_desc, rekomendasi_nilai
          		from
  				rekomendasi_internal 
  				where rekomendasi_finding_id = '".$id."' ";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
	
	function get_tl_per_rekomendasi_list($id) {
        $sql = "select tl_id, tl_desc, tl_nilai
          		from
  				tindaklanjut_internal 
  				where tl_rek_id = '".$id."' ";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
	
	function check_status_rekap_temuan_hasil_audits($id) {
        $sql = "select tl_status
          		from
  				tindaklanjut_internal 
  				where tl_id = '".$id."' ";
        $data = $this->_db->_dbquery ( $sql );
        $arr = $data->FetchRow();
        return $arr['tl_status'];
    }
	function tindaklanjut_count_tlt($id_rekomendasi) {
		$sql = "select count(*) FROM tindaklanjut_internal
				where tl_rek_id = '" . $id_rekomendasi . "' and tl_status = 2 ";
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function tindaklanjut_count_tlp($id_rekomendasi) {
		$sql = "select count(*) FROM tindaklanjut_internal
				where tl_rek_id = '" . $id_rekomendasi . "' and tl_status = 3 ";
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function tindaklanjut_count_btl($id_rekomendasi) {
		$sql = "select count(*) FROM tindaklanjut_internal
				where tl_rek_id = '" . $id_rekomendasi . "' and tl_status = 4 ";
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function tindaklanjut_count_tdtl($id_rekomendasi) {
		$sql = "select count(*) FROM tindaklanjut_internal
				where tl_rek_id = '" . $id_rekomendasi . "' and tl_status = 5 ";
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
}
