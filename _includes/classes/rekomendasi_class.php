<?php
if (@$position == 1) {
    include_once "_includes/DB.class.php";
} else {
    include_once "../_includes/DB.class.php";
}
class rekomendasi
{
    public $_db;
    public $userId;
    public function rekomendasi($userId = "")
    {
        $this->_db    = new db();
        $this->userId = $userId;
    }
    public function uniq_id()
    {
        return $this->_db->uniqid();
    }

    // rekomendasi
    public function rekomendasi_count($id_finding, $status = "", $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($status != "") {
            $condition = "and rekomendasi_status = '" . $status . "' ";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select count(*) FROM rekomendasi_internal
				where rekomendasi_finding_id = '" . $id_finding . "' " . $condition . $condition2;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function rekomendasi_nha_count($id_finding, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select count(*) FROM rekomendasi_internal
				where rekomendasi_finding_id = '" . $id_finding . "' ".$condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function rekomendasi_view_grid($id_finding, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }
        $sql = "select rekomendasi_id, rekomendasi_desc, rekomendasi_dateline, rekomendasi_pic, rekomendasi_nilai,
				case rekomendasi_status
					when '1' then 'Selesai'
					else 'Belum Selesai'
				end as rekomendasi_status
				from rekomendasi_internal
				where rekomendasi_finding_id = '" . $id_finding . "' " . $condition2 . "
				order by rekomendasi_date DESC
				LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function rekomendasi_viewlist($id)
    {
        $sql = "select rekomendasi_id, rekomendasi_date, rekomendasi_finding_id, rekomendasi_desc, rekomendasi_pic, rekomendasi_dateline, assign_surat_no, finding_no, finding_kondisi, auditee_name, rekomendasi_id_code, rekomendasi_status, concat(kode_rek_code, ' - ', kode_rek_desc) as lengkap, rekomendasi_nilai
				from rekomendasi_internal
				join finding_internal on rekomendasi_finding_id = finding_id
				join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				join auditee on finding_auditee_id = auditee_id
				left join par_kode_rekomendasi on kode_rek_id = rekomendasi_id_code
				where rekomendasi_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function rekomendasi_add($id_finding, $rekomendasi_code, $rekomendasi_desc, $rekomendasi_pic, $rekomendasi_dateline, $rekomendasi_date, $nilai)
    {
        $id = $this->uniq_id();
        $sql = "insert into rekomendasi_internal
				(rekomendasi_id, rekomendasi_finding_id, rekomendasi_id_code, rekomendasi_desc, rekomendasi_pic, rekomendasi_dateline, rekomendasi_date, rekomendasi_nilai)
				values
				('" . $id . "', '" . $id_finding . "', '" . $rekomendasi_code . "', '" . $rekomendasi_desc . "', '" . $rekomendasi_pic . "', '" . $rekomendasi_dateline . "', '" . $rekomendasi_date . "',  '" . $nilai . "')";

        $sql1 = "insert into rekomendasi_internal_matrix
                (rekomendasi_id, rekomendasi_finding_id, rekomendasi_id_code, rekomendasi_desc, rekomendasi_pic, rekomendasi_dateline, rekomendasi_date)
                values
                ('" . $id . "', '" . $id_finding . "', '" . $rekomendasi_code . "', '" . $rekomendasi_desc . "', '" . $rekomendasi_pic . "', '" . $rekomendasi_dateline . "', '" . $rekomendasi_date . "')";
        $aksinyo = "Menambah Rekomendasi dengan ID " . $id;

        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
        $this->_db->_dbquery($sql1);
    }
    public function rekomendasi_edit($id, $rekomendasi_code, $rekomendasi_desc, $rekomendasi_pic, $rekomendasi_dateline, $nilai)
    {
        $sql = "update rekomendasi_internal set rekomendasi_id_code = '" . $rekomendasi_code . "', rekomendasi_desc = '" . $rekomendasi_desc . "', rekomendasi_pic = '" . $rekomendasi_pic . "', rekomendasi_dateline = '" . $rekomendasi_dateline . "', rekomendasi_nilai = '" . $nilai. "'
				where rekomendasi_id = '" . $id . "' ";

        $sql1 = "update rekomendasi_internal_matrix set rekomendasi_id_code = '" . $rekomendasi_code . "', rekomendasi_desc = '" . $rekomendasi_desc . "', rekomendasi_pic = '" . $rekomendasi_pic . "', rekomendasi_dateline = '" . $rekomendasi_dateline . "'
                where rekomendasi_id = '" . $id . "' ";

        $aksinyo = "Mengubah Rekomendasi ID " . $id;
        
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
        $this->_db->_dbquery($sql1);
    }
    public function rekomendasi_matrix_edit($id, $rekomendasi_desc)
    {
        $sql = "update rekomendasi_internal_matrix set rekomendasi_desc = '" . $rekomendasi_desc . "'
                where rekomendasi_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function rekomendasi_delete($id)
    {
        $sql     = "delete from rekomendasi_internal where rekomendasi_id = '" . $id . "' ";
        $aksinyo = "Menghapus rekomendasi dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function update_status_rekomendasi($id)
    {
        $sql     = "update rekomendasi_internal set rekomendasi_status = '1' where rekomendasi_id = '" . $id . "' ";
        $aksinyo = "Mengubah Status Rekomendasi ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function get_temuan_id($id_rek)
    {
        $sql  = "select distinct rekomendasi_finding_id FROM rekomendasi_internal where rekomendasi_id = '" . $id_rek . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function rekomendasi_histori_add($id_finding, $rekomendasi_internal_id, $rekomendasi_histori_date, $rekomendasi_code, $rekomendasi_desc, $rekomendasi_pic, $rekomendasi_dateline, $rekomendasi_date)
    {
        $sql = "insert into rekomendasi_internal_histori
				(rekomendasi_id, rekomendasi_internal_id, rekomendasi_histori_date, rekomendasi_finding_id, rekomendasi_id_code, rekomendasi_desc, rekomendasi_pic, rekomendasi_dateline, rekomendasi_date)
				values
				('" . $this->uniq_id() . "', '" . $rekomendasi_internal_id . "', '" . $rekomendasi_histori_date . "', '" . $id_finding . "', '" . $rekomendasi_code . "', '" . $rekomendasi_desc . "', '" . $rekomendasi_pic . "', '" . $rekomendasi_dateline . "', '" . $rekomendasi_date . "')";
        $this->_db->_dbquery($sql);
    }
    public function list_date_histori_rekomendasi($id)
    {
        $sql  = "SELECT DISTINCT(rekomendasi_histori_date) FROM rekomendasi_internal_histori WHERE rekomendasi_internal_id = '" . $id . "' ORDER BY rekomendasi_histori_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function count_list_date_rek_history($id)
    {
        $sql = "SELECT COUNT(*) FROM rekomendasi_internal_histori WHERE  rekomendasi_internal_id = '" . $id . "'";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function list_histori_rekomendasi_by_date_by_id($date, $id)
    {
        $sql = "select rekomendasi_id, rekomendasi_date, rekomendasi_finding_id, rekomendasi_desc, rekomendasi_pic, rekomendasi_dateline, assign_surat_no, finding_no, finding_kondisi, auditee_name, rekomendasi_id_code, rekomendasi_status, concat(kode_rek_code, ' - ', kode_rek_desc) as lengkap
				from rekomendasi_internal_histori
				join finding_internal on rekomendasi_finding_id = finding_id
				join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_id = assign_surat_id_assign
				join auditee on finding_auditee_id = auditee_id
				left join par_kode_rekomendasi on kode_rek_id = rekomendasi_id_code
				where rekomendasi_internal_id = '" . $id . "' and  rekomendasi_histori_date = '" . $date . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
}
