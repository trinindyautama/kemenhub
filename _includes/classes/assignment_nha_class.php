<?php
if (@$position == 1) {
	include_once "_includes/DB.class.php";
} else {
	include_once "../_includes/DB.class.php";
}
class assignment_nha {

	var $_db;
	var $userId;
	
	function assignment_nha($userId = "") {
		$this->_db = new db ();
		$this->userId = $userId;
	}
	
	function uniq_id() {
		return $this->_db->uniqid ();
	}
	
	// nha
	function assignment_nha_count($key_search, $val_search, $all_field) {
		$condition = "";
		if($val_search!=""){
			if($key_search!="") $condition = " and ".$key_search." like '%".$val_search."%' ";
			else {
				for($i=0;$i<count($all_field);$i++){
					$or = " or ";
					if($i==0) $or = "";
					$condition .= $or.$all_field[$i]." like '%".$val_search."%' ";
				}
				$condition = " and (".$condition.")";
			}
		}	
		
		$sql = "select count(*) 
				from assignment_nha
				left join par_inspektorat on nha_inspektorat_id = inspektorat_id
				left join auditee on nha_auditee_id = auditee_id
				left join par_propinsi on auditee_propinsi_id = propinsi_id 
				where 1=1 ".$condition;
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function assignment_nha_view_grid($key_search, $val_search, $all_field, $offset, $num_row) {
		$condition = "";
		if($val_search!=""){
			if($key_search!="") $condition = " and ".$key_search." like '%".$val_search."%' ";
			else {
				for($i=0;$i<count($all_field);$i++){
					$or = " or ";
					if($i==0) $or = "";
					$condition .= $or.$all_field[$i]." like '%".$val_search."%' ";
				}
				$condition = " and (".$condition.")";
			}
		}	
		
		$sql = "select distinct nha_id, inspektorat_name, auditee_name, assign_surat_no, propinsi_name, nha_tahun, nha_start_date, nha_end_date, nha_keterangan, nha_assign_id, nha_auditee_id
				from assignment_nha
				left join par_inspektorat on nha_inspektorat_id = inspektorat_id
				left join auditee on nha_auditee_id = auditee_id
				left join par_propinsi on auditee_propinsi_id = propinsi_id 
				left join assignment_surat_tugas on nha_no_spt = assign_surat_id_assign
				where 1=1".$condition."
				LIMIT $offset, $num_row ";
		$data = $this->_db->_dbquery ( $sql );
		return $data;
	}
	function assignment_nha_add($inspektorat_id, $auditee_id, $nha_no_spt, $propinsi_id, $nha_tahun, $nha_start_date, $nha_end_date, $nha_keterangan, $assign_id) {
		$sql = "insert into assignment_nha
				(nha_id, nha_inspektorat_id, nha_auditee_id, nha_no_spt, nha_propinsi_id, nha_tahun, nha_start_date, nha_end_date, nha_keterangan, nha_assign_id) 
				values
				('".$this->uniq_id()."', '".$inspektorat_id."', '".$auditee_id."', '".$nha_no_spt."', '".$propinsi_id."', '".$nha_tahun."', '".$nha_start_date."', '".$nha_end_date."', '".$nha_keterangan."', '".$assign_id."')";
		$aksinyo = "Menambah Data NHA dengan auditi ".$auditee_id;
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
	function assignment_nha_viewlist($id) {
		$sql = "select nha_id, nha_inspektorat_id, inspektorat_name, nha_auditee_id, auditee_name, nha_no_spt, nha_propinsi_id, propinsi_name, nha_tahun, nha_start_date, nha_end_date, nha_keterangan, nha_no_spt, assign_surat_no
				from assignment_nha
				left join par_inspektorat on nha_inspektorat_id = inspektorat_id
				left join auditee on nha_auditee_id = auditee_id
				left join par_propinsi on auditee_propinsi_id = propinsi_id 
				left join assignment_surat_tugas on nha_no_spt = assign_surat_id_assign
				where nha_id = '".$id."' ";
		$data = $this->_db->_dbquery ( $sql );
		return $data;
	}
    function assignment_nha_viewlist_status($id) {
        $sql = "select nha_id, lha_status
				from assignment_nha
				join assignment_lha on nha_auditee_id = lha_id_auditee
				where nha_id = '".$id."' ";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
	function assignment_nha_edit($id, $inspektorat_id, $auditee_id, $nha_no_spt, $propinsi_id, $nha_tahun, $nha_start_date, $nha_end_date, $nha_keterangan, $assign_id) {
		$sql = "update assignment_nha set nha_inspektorat_id = '".$inspektorat_id."', nha_auditee_id = '".$auditee_id."', nha_no_spt = '".$nha_no_spt."', nha_propinsi_id = '".$propinsi_id."', nha_tahun = '".$nha_tahun."', nha_start_date = '".$nha_start_date."', nha_end_date = '".$nha_end_date."', nha_keterangan = '".$nha_keterangan."', nha_assign_id = '".$assign_id."'
				where nha_id = '".$id."' ";
		$aksinyo = "Mengubah Data NHA";
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
    function assignment_nha_delete($id) {
        $this->assign_nha_finding_delete ( $id );
        $sql = "delete from assignment_nha where nha_id = '".$id."' ";
        $aksinyo = "Menghapus Data NHA ".$id;
        $this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
    }
    function assign_nha_finding_delete($id) {
        $sql = "delete from assignment_nha_finding where finding_nha_id = '".$id."' ";
        $this->_db->_dbquery ( $sql );
    }
    function assign_nha_cek_spt($spt, $id = "") {
        $condition = "";
        if ($id != "")
            $condition = "and nha_id != '".$id."' ";
        $sql = "select nha_id, nha_no_spt FROM assignment_nha where LCASE(nha_no_spt) = '".strtolower ( $spt )."' ".$condition;
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
    function assign_nha_upload_del($id) {
        $sql = "delete from assignment_nha where nha_id = '".$id."' ";
        $aksinyo = "Menghapus Data NHA".$id;
        $this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
    }
    function st_data_combo($id = "", $id2 = "") {
        $condition = "";
        if($id!="") $condition = " and auditee_id = '".$id."' ";
        if($id2!="") $condition = " and assign_surat_id_assign = '".$id2."' ";
        $sql = "select assign_surat_id, assign_surat_no, assign_surat_id_assign
				FROM assignment_surat_tugas
 				where 1=1 order by assign_surat_no".$condition;
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
    function st_sub_obyek($id="") {
        $sql = "select auditee_id, auditee_name 
				FROM assignment_surat_tugas
                left join assignment_auditee on assign_surat_id_assign = assign_auditee_id_assign
                left join auditee on assign_auditee_id_auditee  = auditee_id   
                where auditee_id = '".$id."' and auditee_del_st = 1";
        echo $sql;
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
    //end nha

	//temuan nha
	function finding_nha_count($nha_id, $auditee_id, $key_search, $val_search, $all_field) {
		$condition = "";
		if($val_search!=""){
			if($key_search!="") $condition = " and ".$key_search." like '%".$val_search."%' ";
			else {
				for($i=0;$i<count($all_field);$i++){
					$or = " or ";
					if($i==0) $or = "";
					$condition .= $or.$all_field[$i]." like '%".$val_search."%' ";
				}
				$condition = " and (".$condition.")";
			}
		}
		$sql = "select count(*) 
				from finding_internal
				left join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_surat_id_assign = assign_id
				left join assignment_nha on nha_no_spt = assign_surat_id_assign
				left join auditee on finding_auditee_id = auditee_id
				where nha_id = '".$nha_id."' and finding_auditee_id = '".$auditee_id."' ".$condition;
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function finding_nha_view_grid($nha_id, $auditee_id, $key_search, $val_search, $all_field, $offset, $num_row) {
		$condition = "";
		if($val_search!=""){
			if($key_search!="") $condition = " and ".$key_search." like '%".$val_search."%' ";
			else {
				for($i=0;$i<count($all_field);$i++){
					$or = " or ";
					if($i==0) $or = "";
					$condition .= $or.$all_field[$i]." like '%".$val_search."%' ";
				}
				$condition = " and (".$condition.")";
			}
		}
		$sql = "select DISTINCT finding_id, finding_no, finding_judul, auditee_name, finding_date, finding_status, finding_assign_id
                from finding_internal
				left join assignment on finding_assign_id = assign_id
				left join assignment_surat_tugas on assign_surat_id_assign = assign_id
				left join assignment_nha on nha_no_spt = assign_surat_id_assign
				left join auditee on finding_auditee_id = auditee_id
				where nha_id = '".$nha_id."' and finding_auditee_id = '".$auditee_id."' ".$condition." order by finding_no asc
				LIMIT $offset, $num_row ";
        $data = $this->_db->_dbquery ( $sql );
		return $data;
	}
	//end temuan nha

	// combobox
    function inspektorat_data_viewlist($id = "") {
        $condition = "";
        if ($id != "") $condition = "and nha_inspektorat_id = '".$id."' ";
        $sql = "select inspektorat_id, inspektorat_name FROM par_inspektorat where inspektorat_del_st = 1 ".$condition." order by inspektorat_name";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
    function st_data_viewlist($id = "") {
        $condition = "";
        if ($id != "") $condition = "and auditee_inspektorat_id = '".$id."' ";
        $sql = "select distinct assign_surat_id_assign, auditee_inspektorat_id, assign_surat_no 
				FROM auditee
				join assignment_auditee on auditee_id = assign_auditee_id_auditee
				join assignment on assign_auditee_id_assign = assign_id
				join assignment_surat_tugas on assign_id = assign_surat_id_assign
				where 1=1 ".$condition." order by assign_surat_no";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }
    public function assign_auditee_viewlist($id = "", $auditee_id = ""){
        $condition = "";
        if ($auditee_id != "") {
            $condition .= " and assign_auditee_id_auditee = '" . $auditee_id . "' ";
        }

		if($id!="") {
            $condition = " and assign_surat_id_assign = '".$id."'";
		}

		$sql = "select DISTINCT assign_auditee_id_auditee, auditee_name, auditee_kode, auditee_alamat, auditee_no_file, auditee_telp, assign_auditee_id_assign
				FROM assignment_surat_tugas
				left join assignment_auditee on assign_surat_id_assign = assign_auditee_id_assign
				left join auditee on assign_auditee_id_auditee = auditee_id
				left join par_propinsi on auditee_propinsi_id = propinsi_id
				where 1=1 " . $condition . " and assign_auditee_id_auditee != '' order by auditee_name";
		$data = $this->_db->_dbquery($sql);
		return $data;
	}


	// arsip lha
	function assignment_lha_arsip_count($key_search, $val_search, $all_field) {
		$condition = "";
		if($val_search!=""){
			if($key_search!="") $condition = " and ".$key_search." like '%".$val_search."%' ";
			else {
				for($i=0;$i<count($all_field);$i++){
					$or = " or ";
					if($i==0) $or = "";
					$condition .= $or.$all_field[$i]." like '%".$val_search."%' ";
				}
				$condition = " and (".$condition.")";
			}
		}	
		
		$sql = "select count(*) 
				from assignment_lha_arsip
				left join auditee on lha_arsip_auditee = auditee_id
				left join par_esselon on lha_arsip_esselon = esselon_id
				where 1=1 ".$condition;
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function assignment_lha_arsip_viewlist($id) {
		$sql = "select lha_arsip_id, lha_arsip_auditee, lha_arsip_tahun, lha_arsip_esselon, lha_arsip_file, auditee_name, esselon_name
				from assignment_lha_arsip
				left join auditee on lha_arsip_auditee = auditee_id
				left join par_esselon on lha_arsip_esselon = esselon_id
				where lha_arsip_id = '".$id."' ";
		$data = $this->_db->_dbquery ( $sql );
		return $data;
	}
	function assignment_lha_arsip_view_grid($key_search, $val_search, $all_field, $offset, $num_row) {
		$condition = "";
		if($val_search!=""){
			if($key_search!="") $condition = " and ".$key_search." like '%".$val_search."%' ";
			else {
				for($i=0;$i<count($all_field);$i++){
					$or = " or ";
					if($i==0) $or = "";
					$condition .= $or.$all_field[$i]." like '%".$val_search."%' ";
				}
				$condition = " and (".$condition.")";
			}
		}	
		
		$sql = "select lha_arsip_id, lha_arsip_auditee, lha_arsip_tahun, lha_arsip_esselon, lha_arsip_file, auditee_name, esselon_name
				from assignment_lha_arsip
				left join auditee on lha_arsip_auditee = auditee_id
				left join par_esselon on lha_arsip_esselon = esselon_id
				where 1=1".$condition."
				LIMIT $offset, $num_row ";
		$data = $this->_db->_dbquery ( $sql );
		return $data;
	}
	function assignment_lha_arsip_add($auditee, $esselon, $tahun, $file) {
		$sql = "insert into assignment_lha_arsip
				(lha_arsip_id, lha_arsip_auditee, lha_arsip_esselon, lha_arsip_tahun, lha_arsip_file) 
				values
				('".$this->uniq_id()."', '".$auditee."', '".$esselon."', '".$tahun."', '".$file."')";
		$aksinyo = "Menambah Data Arsip LHA ".$auditee;
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
	function assignment_lha_arsip_edit($id, $auditee, $esselon, $tahun) {
		$sql = "update assignment_lha_arsip set lha_arsip_auditee = '".$auditee."', lha_arsip_esselon = '".$esselon."', lha_arsip_tahun = '".$tahun."' where lha_arsip_id = '".$id."' ";
		$aksinyo = "Mengubah Data Arsip LHA";
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
	function assignment_lha_arsip_edit_upload($id, $auditee, $esselon, $tahun, $file) {
		$sql = "update assignment_lha_arsip set lha_arsip_auditee = '".$auditee."', lha_arsip_esselon = '".$esselon."', lha_arsip_tahun = '".$tahun."', lha_arsip_file = '".$file."' where lha_arsip_id = '".$id."' ";
		$aksinyo = "Mengubah Data Arsip LHA";
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
	function assignment_lha_arsip_delete($id) {
		$sql = "delete from assignment_lha_arsip where lha_arsip_id = '".$id."' ";
		$aksinyo = "Menghapus Data Arsip LHA ".$id;
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
	//end nha
	
}
?>
