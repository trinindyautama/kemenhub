<?php
if (@$position == 1) {
    include_once "_includes/DB.class.php";
} else {
    include_once "../_includes/DB.class.php";
}
class programaudit
{
    public $_db;
    public $userId;
    public function programaudit($userId = "")
    {
        $this->_db    = new db();
        $this->userId = $userId;
    }
    public function uniq_id()
    {
        return $this->_db->uniqid();
    }
    public function program_audit_count($assign_id, $key_search, $val_search, $all_field, $program_id="")
    {
        $condition = "";

        if ($program_id != "") {
            $condition .= " and program_id = '" . $program_id . "' ";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select count(*) FROM program_audit
				left join auditee on program_id_auditee = auditee_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join auditor on program_id_auditor = auditor_id
				where program_id_assign = '" . $assign_id . "' " . $condition . $condition2;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function program_audit_view_grid($assign_id, $key_search, $val_search, $all_field, $offset, $num_row, $program_id="")
    {
        $condition = "";

        if ($program_id != "") {
            $condition .= " and program_id = '" . $program_id . "' ";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select program_id, auditee_name, ref_program_procedure, ref_program_code, ref_program_title, auditor_name, program_status, program_tahapan
				from program_audit
				left join auditee on program_id_auditee = auditee_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join auditor on program_id_auditor = auditor_id
				where program_id_assign = '" . $assign_id . "'  " . $condition . $condition2." LIMIT $offset, $num_row ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function program_audit_viewlist($id)
    {
        $sql = "select program_id, program_id_assign, program_id_auditee, program_id_ref, program_id_auditor, program_start, program_end, program_day, auditee_name, auditor_name, ref_program_title, ref_program_code, ref_program_procedure, program_jam, program_lampiran, program_tahapan, auditee_kode, aspek_id, aspek_name
				FROM program_audit
				left join auditee on program_id_auditee = auditee_id
				left join auditor on program_id_auditor = auditor_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join par_aspek on ref_program_aspek_id = aspek_id
				where program_id = '" . $id . "' ";
        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    function program_audit_add($assign_id, $id_auditee, $id_ref, $id_auditor, $program_jam, $lampiran, $date, $tahapan, $program_start, $program_end) {
        $id = $this->uniq_id ();
        $sql = "insert into program_audit 
                (program_id, program_id_assign, program_id_auditee, program_id_ref, program_id_auditor, program_jam, program_lampiran, program_create_by, program_create_date, program_tahapan, program_start, program_end) 
                values 
                ('".$id."', '".$assign_id."', '".$id_auditee."', '".$id_ref."', '".$id_auditor."', '".$program_jam."', '".$lampiran."', '".$this->userId."', '".$date."', '".$tahapan."', '".$program_start."', '".$program_end."')";
        $aksinyo = "Menambah Program Audit ID ".$id;
        $this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
    }
    public function program_audit_edit($id, $id_auditee, $id_ref, $id_auditor, $program_jam, $lampiran, $tahapan, $program_start)
    {
        $sql = "update program_audit set program_id_auditee = '" . $id_auditee . "',program_id_ref = '" . $id_ref . "', program_id_auditor = '" . $id_auditor . "', program_jam = '" . $program_jam . "', program_lampiran = '" . $lampiran . "', program_tahapan = '" . $tahapan . "', program_start = '" . $program_start . "'
				where program_id = '" . $id . "' ";
        $aksinyo = "Mengubah Program Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function program_audit_edit1($id, $id_auditee, $id_auditor, $program_jam, $lampiran, $tahapan, $program_start)
    {
        $sql = "update program_audit set program_id_auditee = '" . $id_auditee . "', program_id_auditor = '" . $id_auditor . "', program_jam = '" . $program_jam . "', program_lampiran = '" . $lampiran . "', program_tahapan = '" . $tahapan . "', program_start = '" . $program_start . "'
				where program_id = '" . $id . "' ";
        $aksinyo = "Mengubah Program Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function program_audit_update_status($id, $status, $date)
    {
        $sql = "update program_audit set program_status = '" . $status . "', program_approve_by = '" . $this->userId . "', program_approve_date = '" . $date . "'
				where program_id = '" . $id . "' ";
        $aksinyo = "Mengubah Status Program Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function program_audit_komentar_viewlist($id)
    {
        $sql = "select auditor_name, program_comment_desc, program_comment_date
				FROM program_audit_comment
				left join program_audit on program_comment_program_id = program_id
				left join user_apps on program_comment_user_id = user_id
				left join auditor on user_id_internal = auditor_id
				where program_comment_program_id = '" . $id . "' order by program_comment_date ASC";
        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function program_audit_add_komentar($id, $komen, $tanggal)
    {
        $sql = "insert into program_audit_comment
				(program_comment_id, program_comment_program_id, program_comment_user_id, program_comment_desc, program_comment_date)
				values
				('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komen . "','" . $tanggal . "')";
        $this->_db->_dbquery($sql);
    }
    public function kertas_kerja_prog_delete($id)
    {
        $sql = "delete from kertas_kerja where kertas_kerja_id_program = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function program_audit_delete($id)
    {
        $this->kertas_kerja_prog_delete($id);
        $sql     = "delete from program_audit where program_id = '" . $id . "' ";
        $aksinyo = "Menghapus Program Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function get_assign_aspek($id_aspek = "", $id_assign = "", $id_auditee = "", $tahapan = "")
    {
        $condition = "";
        if ($id_aspek != "") {
            $condition .= " and aspek_id = '" . $id_aspek . "'";
        }

        if ($id_assign != "") {
            $condition .= " and program_id_assign = '" . $id_assign . "' ";
        }

        if ($id_auditee != "") {
            $condition .= " and program_id_auditee = '" . $id_auditee . "' ";
        }

        if ($tahapan != "") {
            $condition .= " and program_tahapan = '" . $tahapan . "' ";
        }

        $sql = "select distinct aspek_id, aspek_name
				FROM program_audit
				left join ref_program_audit on program_id_ref = ref_program_id
				inner join par_aspek on ref_program_aspek_id = aspek_id
				where 1=1 " . $condition;
        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_assign_aspek1($id_assign, $id_auditee)
    {
        $sql = "select distinct aspek_id, aspek_name
				FROM finding_internal
				left join kertas_kerja on finding_kka_id = kertas_kerja_id
				left join program_audit on kertas_kerja_id_program = program_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join par_aspek on ref_program_aspek_id = aspek_id
				where finding_assign_id = '" . $id_assign . "' and finding_auditee_id = '" . $id_auditee . "' and par_aspek.ikhtisar = 'Iya' order by par_aspek.urutan asc ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function intro_pka_viewlist($id_aspek = "", $id_assign, $tahapan = "")
    {
        $condition = "";
        if ($tahapan != "") {
            $condition .= " and pka_intro_tahapan = '" . $tahapan . "' ";
        }

        if ($id_aspek != "") {
            $condition .= " and pka_intro_id_aspek = '" . $id_aspek . "'";
        }

        $sql = "select DISTINCT pka_intro_id, pka_intro_tujuan, pka_intro_no_pka, pka_keterangan, pka_intro_arranged_date, pka_intro_reviewed_date, pka_intro_approved_date
				from program_audit_intro
				where pka_intro_id_assign = '" . $id_assign . "'" . $condition;
        $data = $this->_db->_dbquery($sql);
        // echo $sql;
        return $data;
    }

    public function get_pka_auto($tahapan)
    {
        $sql  = "select max(pka_intro_no_pka) as maxPKA from program_audit_intro where pka_intro_no_pka LIKE 'PKA/00%'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function intro_pka_add($id_assign, $fil_aspek, $tujuan, $tahapan, $no_pka, $pka_keterangan, $pka_intro_arranged_date, $pka_intro_reviewed_date, $pka_intro_approved_date)
    {
        $id  = $this->uniq_id();
        $sql = "insert into program_audit_intro
				(pka_intro_id, pka_intro_id_assign, pka_intro_id_aspek, pka_intro_tujuan, pka_intro_tahapan, pka_intro_no_pka, pka_keterangan, pka_intro_arranged_date, pka_intro_reviewed_date, pka_intro_approved_date)
				values
				('" . $id . "', '" . $id_assign . "', '" . $fil_aspek . "', '" . $tujuan . "', '" . $tahapan . "', '" . $no_pka . "', '" . $pka_keterangan . "', '" . $pka_intro_arranged_date . "', '" . $pka_intro_reviewed_date . "', '" . $pka_intro_approved_date . "')";
        $aksinyo = "Menambah Intro Program Audit ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function intro_pka_edit($id, $fil_aspek, $tujuan, $tahapan, $no_pka, $pka_keterangan, $pka_intro_arranged_date, $pka_intro_reviewed_date, $pka_intro_approved_date)
    {
        $sql = "update program_audit_intro set pka_intro_id_aspek = '" . $fil_aspek . "', pka_intro_tujuan = '" . $tujuan . "', pka_intro_tahapan = '" . $tahapan . "', pka_intro_no_pka = '" . $no_pka . "', pka_keterangan = '" . $pka_keterangan . "', pka_intro_arranged_date = '" . $pka_intro_arranged_date . "', pka_intro_reviewed_date = '" . $pka_intro_reviewed_date . "', pka_intro_approved_date = '" . $pka_intro_approved_date . "'
				where pka_intro_id = '" . $id . "' ";
        $aksinyo = "Mengubah Intro Program Audit dengan ID " . $id;

        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }

    public function get_user_by_posisi($id_assign, $id_posisi = "")
    {
        $condition = '';
        if ($id_posisi != '') {
            $condition = " and assign_auditor_id_posisi = '" . $id_posisi . "' ";
        }

        $sql = "select DISTINCT user_id from assignment_auditor
				join user_apps on assign_auditor_id_auditor = user_id_internal
				where assign_auditor_id_assign = '" . $id_assign . "'" . $condition;
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
}
