<?php
if (@$position == 1) {
    include_once "_includes/DB.class.php";
} else {
    include_once "../_includes/DB.class.php";
}
class kertas_kerja
{
    public $_db;
    public $userId;
    public function kertas_kerja($userId = "")
    {
        $this->_db    = new db();
        $this->userId = $userId;
    }
    public function uniq_id()
    {
        return $this->_db->uniqid();
    }
    public function kertas_kerja_count($id_program_audit, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select count(*) FROM kertas_kerja
				join program_audit on kertas_kerja_id_program = program_id
				where kertas_kerja_id_program = '" . $id_program_audit . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function kertas_kerja_view_grid($id_program_audit, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select kertas_kerja_id, kertas_kerja_no, kertas_kerja_kesimpulan, kertas_kerja_date, kertas_kerja_status, program_id_assign
				from kertas_kerja
				join program_audit on kertas_kerja_id_program = program_id
				where kertas_kerja_id_program = '" . $id_program_audit . "' " . $condition . "
				order by kertas_kerja_date DESC
				LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function kertas_kerja_viewlist($id)
    {
        $sql = "select kertas_kerja_id, kertas_kerja_desc, kertas_kerja_no, kertas_kerja_kesimpulan, kertas_kerja_date, kertas_kerja_jam, kertas_kerja_attach, auditee_name, auditee_id, ref_program_title, kertas_kerja_id_program, ref_program_code, create_by.auditor_name as create_name, kerja_kerja_created_date, approve_by.auditor_name as approve_name, kertas_kerja_approve_date, assign_tahun
				FROM kertas_kerja
				join program_audit on kertas_kerja_id_program = program_id
				join assignment on program_id_assign = assign_id
				left join auditee on program_id_auditee = auditee_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join auditor as create_by on kertas_kerja_created_by = create_by.auditor_id
				left join auditor as approve_by on kertas_kerja_approve_by = approve_by.auditor_id
				where kertas_kerja_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function kertas_kerja_add($id_kka, $program_id, $no_kka, $kertas_kerja, $kesimpulan, $kertas_kerja_date, $kertas_kerja_jam, $kka_attach)
    {
        $sql = "insert into kertas_kerja
				(kertas_kerja_id, kertas_kerja_id_program, kertas_kerja_no, kertas_kerja_desc, kertas_kerja_kesimpulan, kertas_kerja_date, kertas_kerja_jam, kertas_kerja_attach)
				values ('" . $id_kka . "', '" . $program_id . "', '" . $no_kka . "', '" . $kertas_kerja . "', '" . $kesimpulan . "', '" . $kertas_kerja_date . "', '" . $kertas_kerja_jam . "', '" . $kka_attach . "')";

        $aksinyo = "Menambah Kertas Kerja Dengan ID " . $id_kka;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function kertas_kerja_edit($id, $no_kka, $kertas_kerja, $kesimpulan, $kertas_kerja_date, $kertas_kerja_jam, $kka_attach)
    {
        $sql = "update kertas_kerja set kertas_kerja_no = '" . $no_kka . "', kertas_kerja_desc = '" . $kertas_kerja . "', kertas_kerja_kesimpulan = '" . $kesimpulan . "', kertas_kerja_date = '" . $kertas_kerja_date . "', kertas_kerja_jam = '" . $kertas_kerja_jam . "', kertas_kerja_attach = '" . $kka_attach . "'
				where kertas_kerja_id = '" . $id . "' ";

        $aksinyo = "Mengubah Kertas Kerja dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function kertas_kerja_history_add($id_history_kka, $kka_id, $kka_history_date, $no_kka, $kertas_kerja, $kesimpulan, $kertas_kerja_date, $kertas_kerja_jam, $kka_attach)
    {
        $sql = "insert into kertas_kerja_history
				(kertas_kerja_history_id, kertas_kerja_history_id_kertas_kerja, kertas_kerja_history_date, kertas_kerja_no, kertas_kerja_desc, kertas_kerja_kesimpulan, kertas_kerja_date, kertas_kerja_jam, kertas_kerja_attach)
				values ('" . $id_history_kka . "', '" . $kka_id . "', '" . $kka_history_date . "', '" . $no_kka . "', '" . $kertas_kerja . "', '" . $kesimpulan . "', '" . $kertas_kerja_date . "', '" . $kertas_kerja_jam . "', '" . $kka_attach . "')";

        $aksinyo = "Menambah History Kertas Kerja Dengan ID " . $no_kka;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function kertas_kerja_delete($id)
    {
        $sql     = "delete from kertas_kerja where kertas_kerja_id = '" . $id . "' ";
        $aksinyo = "Menghapus Kertas Kerja Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function kka_update_status($id, $status)
    {
        $sql = "update kertas_kerja set kertas_kerja_status = '" . $status . "'
				where kertas_kerja_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function kka_komentar_viewlist($id)
    {
        $sql = "select auditor_name, kertas_kerja_comment_desc, kertas_kerja_comment_date
				FROM kertas_kerja_comment
				left join user_apps on kertas_kerja_comment_user_id = user_id
				left join auditor on user_id_internal = auditor_id
				where kertas_kerja_comment_kka_id = '" . $id . "' order by kertas_kerja_comment_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function kka_add_komentar($id, $komentar, $tanggal)
    {
        $sql = "insert into kertas_kerja_comment
				(kertas_kerja_comment_id, kertas_kerja_comment_kka_id, kertas_kerja_comment_user_id, kertas_kerja_comment_desc, kertas_kerja_comment_date)
				values
				('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komentar . "','" . $tanggal . "')";
        $aksinyo = "Mengomentari Kertas Kerja dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }

    public function getdata_assign($program_id)
    {
        $sql = "SELECT program_id_assign, program_id_auditee, program_start
				from program_audit
				where program_id = '" . $program_id . "' "; // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function cek_owner_kka($kka_id, $user_id)
    {
        $condition = "";
        if ($kka_id != "") {
            $condition .= " and kertas_kerja_id = '" . $kka_id . "' ";
        }

        $sql = "SELECT count(*) FROM kertas_kerja
				left join program_audit on kertas_kerja_id_program = program_id
				left join user_apps on program_id_auditor = user_id_internal
				WHERE user_id = '" . $user_id . "' " . $condition; //echo $sql;
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    public function cek_owner_program($program_id, $user_id)
    {
        $condition = "";
        if ($program_id != "") {
            $condition .= " and program_id = '" . $program_id . "' ";
        }

        $sql = "SELECT count(*) FROM program_audit
				left join user_apps on program_id_auditor = user_id_internal
				WHERE user_id = '" . $user_id . "' " . $condition;
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function cek_posisi($id_assign)
    {
        $sql = "select DISTINCT assign_auditor_id_posisi from assignment_auditor
				left join user_apps on assign_auditor_id_auditor = user_id_internal
				where user_id = '" . $this->userId . "' and assign_auditor_id_assign = '" . $id_assign . "'";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function get_user_by_posisi($id_prog, $id_posisi = "")
    {
        $condition = '';
        if ($id_posisi != '') {
            $condition = " and assign_auditor_id_posisi = '" . $id_posisi . "' ";
        }

        //else $condition = " and program_id_auditor = ";
        $sql = "select DISTINCT user_id from assignment_auditor
				join user_apps on assign_auditor_id_auditor = user_id_internal
				join program_audit on assign_auditor_id_assign = program_id_assign
				where program_id = '" . $id_prog . "'" . $condition;
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function get_user_owner($id_prog)
    {
        $sql = "select DISTINCT user_id from kertas_kerja
				join program_audit on kertas_kerja_id_program = program_id
				join user_apps on program_id_auditor = user_id_internal
				where kertas_kerja_id_program = '" . $id_prog . "'";
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }
    public function insert_lampiran_kka($id_kka, $kka_attach)
    {
        $sql = "insert into kertas_kerja_attachment
				(kka_attach_id, kka_attach_kka_id, kka_attach_filename)
				values
				('" . $this->uniq_id() . "', '" . $id_kka . "', '" . $kka_attach . "')";
        $this->_db->_dbquery($sql);
    }
    public function delete_lampiran_kka($id_kka, $kka_attach)
    {
        $sql = "delete from kertas_kerja_attachment where kka_attach_kka_id = '" . $id_kka . "' and kka_attach_filename = '" . $kka_attach . "' ";
        $this->_db->_dbquery($sql);
    }
    public function list_kka_lampiran($id_kka)
    {
        $sql = "select kka_attach_id, kka_attach_filename from kertas_kerja_attachment
				where kka_attach_kka_id = '" . $id_kka . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function list_date_kka_history($id_kka)
    {
        $sql  = "SELECT DISTINCT(kertas_kerja_history_date) FROM kertas_kerja_history WHERE kertas_kerja_history_id_kertas_kerja = '" . $id_kka . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function list_kka_by_history_date($date, $id_kka)
    {
        $sql = "SELECT * FROM kertas_kerja_history WHERE kertas_kerja_history_date = '" . $date . "' and kertas_kerja_history_id_kertas_kerja = '" . $id_kka . "'";
        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function tahapan($program_id)
    {
        $sql = "select distinct(program_tahapan)
				from program_audit
				left join auditee on program_id_auditee = auditee_id
				left join ref_program_audit on program_id_ref = ref_program_id
				left join auditor on program_id_auditor = auditor_id
				left join kertas_kerja on kertas_kerja_id_program = program_id
				where program_id = '" . $program_id . "'";

        // echo $sql;
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function count_list_date_kka_history($id_kka)
    {
    	$sql = "SELECT COUNT(*) FROM kertas_kerja_history WHERE  kertas_kerja_history_id_kertas_kerja = '" . $id_kka . "'";
        // echo $sql;	
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];	
    }
    public function get_kka_auto($tahapan)
    {
        $sql = "select max(kertas_kerja_no) as kkaMAX
                FROM kertas_kerja
                join program_audit on kertas_kerja_id_program = program_id
                where kertas_kerja_no LIKE 'KKA/02%'";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0]; 
    }
    // end kka
    // lampiran
    public function lampiran_count($id_kka, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select count(*) FROM kertas_kerja_attachment
                where kka_attach_kka_id = '" . $id_kka . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function lampiran_view_grid($id_kka, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select * from kertas_kerja_attachment
                where kka_attach_kka_id = '" . $id_kka . "' " . $condition . "
                LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function lampiran_viewlist($id)
    {
        $sql = "SELECT kka_attach_filename FROM kertas_kerja_attachment WHERE kka_attach_id = '".$id."'";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function destroy_lampiran_kka($id, $kka_attach)
    {
        $sql = "delete from kertas_kerja_attachment where kka_attach_id = '" . $id . "' and kka_attach_filename = '" . $kka_attach . "' ";
        $this->_db->_dbquery($sql);   
    }
    // update

    function get_id_program($id) {
        $sql = "select kertas_kerja_id_program
                FROM kertas_kerja 
                where kertas_kerja_id = '".$id."' ";
        $rs = $this->_db->_dbquery ( $sql );
        $arr = $rs->FetchRow();
        return $arr['kertas_kerja_id_program'];
    }
}
