<?php
if (@$position == 1) {
    include_once "_includes/DB.class.php";
} else {
    include_once "../_includes/DB.class.php";
}
class assign
{
    public $_db;
    public $userId;
    public function assign($userId = "")
    {
        $this->_db    = new db();
        $this->userId = $userId;
    }
    public function uniq_id()
    {
        return $this->_db->uniqid();
    }
    public function assign_count($base_on_id_int = "", $key_search, $val_search, $all_field, $assign_id = "")
    {
        $condition = "";
        if ($base_on_id_int == '0') {
            $base_on_id_int = "";
        }

        if ($base_on_id_int != "") {
            $condition = " and assign_auditor_id_auditor = '" . $base_on_id_int . "'";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select count(distinct assign_id) FROM assignment
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join assignment_auditor on assign_id = assign_auditor_id_assign
                left join assignment_lha on assign_id = lha_id_assign
                where 1=1 " . $condition . $condition2;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function assign_view_grid($base_on_id_int = "", $key_search, $val_search, $all_field, $offset, $num_row, $assign_id = "")
    {
        $condition = "";
        if ($base_on_id_int == '0') {
            $base_on_id_int = "";
        }

        if ($base_on_id_int != "") {
            $condition .= " and assign_auditor_id_auditor = '" . $base_on_id_int . "' ";
        }

        if ($assign_id != "") {
            $condition .= " and assign_id = '" . $assign_id . "' ";
        }

        $condition2 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition2 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition2 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition2 = " and (" . $condition2 . ")";
            }
        }

        $sql = "select distinct assign_id, assign_start_date, assign_end_date, assign_status, assign_tahun, assign_surat_no, assign_no_lha, lha_status, assign_kegiatan, assign_surat_tgl
                from assignment
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join assignment_auditor on assign_id = assign_auditor_id_assign
                left join assignment_lha on assign_id = lha_id_assign
                where 1=1 " . $condition . $condition2 . " order by assign_start_date DESC LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_viewlist($id)
    {
        $sql = "select assign_id, assign_tipe_id, assign_tahun, assign_start_date, assign_end_date, assign_dasar, audit_type_name, assign_kegiatan, assign_periode, assign_hari_persiapan, assign_hari_pelaksanaan, assign_hari_pelaporan, assign_pendanaan, assign_keterangan, assign_surat_no, assign_file, assign_persiapan_awal, assign_persiapan_akhir, assign_pelaksanaan_awal, assign_pelaksanaan_akhir, assign_pelaporan_awal, assign_pelaporan_akhir, assign_pendahuluan, assign_tujuan, assign_instruksi, assign_date_lha
                FROM assignment
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join par_audit_type on assign_tipe_id = audit_type_id
                where assign_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_auditee_viewlist($id = "", $auditee_id = "")
    {
        $condition = "";
        if ($auditee_id != "") {
            $condition .= " and assign_auditee_id_auditee = '" . $auditee_id . "' ";
        }

        if ($id != "") {
            $condition .= " and assign_auditee_id_assign = '" . $id . "' ";
        }

        $sql = "select DISTINCT assign_auditee_id_auditee, auditee_name, auditee_kode, auditee_alamat, auditee_no_file, auditee_telp, assign_auditee_id_assign
                FROM assignment_auditee
                join auditee on assign_auditee_id_auditee = auditee_id
                where 1=1 " . $condition . " order by auditee_name";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    function assign_auditee_viewlist2($id) {
        $sql = "select assign_auditee_id, assign_auditee_id_assign, assign_auditee_id_auditee, auditee_name, auditee_kode, auditee_alamat, auditee_no_file, auditee_telp
				FROM assignment_auditee
                join auditee on assign_auditee_id_auditee = auditee_id
				where assign_auditee_id_assign = '".$id."' ";
        $data = $this->_db->_dbquery ( $sql );
        return $data;
    }

    public function assign_auditee_inspektorat_viewlist()
    {
        $sql = "select DISTINCT inspektorat_id, inspektorat_name
                FROM assignment_auditee
                join auditee on assign_auditee_id_auditee = auditee_id
                join par_inspektorat on auditee_inspektorat_id = inspektorat_id
                where 1=1 ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function assign_auditee_get_inspektorat_viewlist($id = "", $auditee_id = "")
    {
        $condition = "";
        if ($auditee_id != "") {
            $condition .= " and assign_auditee_id_auditee = '" . $auditee_id . "' ";
        }

        if ($id != "") {
            $condition .= " and assign_auditee_id_assign = '" . $id . "' ";
        }

        $sql = "select DISTINCT assign_auditee_id_assign, assign_auditee_id_auditee, auditee_name, auditee_kode, auditee_alamat, auditee_no_file, auditee_telp, inspektorat_id, inspektorat_name
                FROM assignment_auditee
                join auditee on assign_auditee_id_auditee = auditee_id
                join par_inspektorat on auditee_inspektorat_id = inspektorat_id
                where 1=1 " . $condition . " order by auditee_name";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function assign_auditee_get_eselon_viewlist($id = "", $auditee_id = "")
    {
        $condition = "";
        if ($auditee_id != "") {
            $condition .= " and assign_auditee_id_auditee = '" . $auditee_id . "' ";
        }

        if ($id != "") {
            $condition .= " and assign_auditee_id_assign = '" . $id . "' ";
        }

        $sql = "select DISTINCT assign_auditee_id_assign, assign_auditee_id_auditee, auditee_name, auditee_kode, auditee_alamat, auditee_no_file, auditee_telp, esselon_name
                FROM assignment_auditee
                join auditee on assign_auditee_id_auditee = auditee_id
                join par_esselon on auditee_esselon = esselon_id
                where 1=1 " . $condition . " order by auditee_name";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function assign_add($id, $tipe_audit, $tahun, $tanggal_awal, $tanggal_akhir, $dasar, $hari_kerja, $periode, $kegiatan, $hari_persiapan, $hari_pelaksanaan, $hari_pelaporan, $pendanaan, $keterangan, $assign_attach, $tanggal_persiapan_awal, $tanggal_persiapan_akhir, $tanggal_pelaksanaan_awal, $tanggal_pelaksanaan_akhir, $tanggal_pelaporan_awal, $tanggal_pelaporan_akhir, $pedahuluan, $tujuan, $instruksi)
    {
        $this->assign_add_lha($id);
        $sql = "insert into assignment
                (assign_id, assign_tipe_id, assign_tahun, assign_start_date, assign_end_date, assign_dasar, assign_hari, assign_periode, assign_kegiatan, assign_hari_persiapan, assign_hari_pelaksanaan, assign_hari_pelaporan, assign_pendanaan, assign_keterangan, assign_file, assign_persiapan_awal, assign_persiapan_akhir, assign_pelaksanaan_awal, assign_pelaksanaan_akhir, assign_pelaporan_awal, assign_pelaporan_akhir, assign_pendahuluan, assign_tujuan, assign_instruksi)
                values
                ('" . $id . "', '" . $tipe_audit . "', '" . $tahun . "', '" . $tanggal_awal . "', '" . $tanggal_akhir . "', '" . $dasar . "', '" . $hari_kerja . "', '" . $periode . "', '" . $kegiatan . "', '" . $hari_persiapan . "', '" . $hari_pelaksanaan . "', '" . $hari_pelaporan . "', '" . $pendanaan . "', '" . $keterangan . "', '" . $assign_attach . "', '" . $tanggal_persiapan_awal . "', '" . $tanggal_persiapan_akhir . "', '" . $tanggal_pelaksanaan_awal . "', '" . $tanggal_pelaksanaan_akhir . "', '" . $tanggal_pelaporan_awal . "', '" . $tanggal_pelaporan_akhir . "', '" . $pedahuluan . "', '" . $tujuan . "', '" . $instruksi . "' )";
        $aksinyo = "Menambah Penugasan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_add_lha($id)
    {
        $sql = "insert into assignment_lha (lha_id, lha_id_assign) values ('" . $this->uniq_id() . "', '" . $id . "')";
        $this->_db->_dbquery($sql);
    }
    public function assign_add_auditee($id, $id_auditee)
    {
        $sql = "insert into assignment_auditee
                (assign_auditee_id_assign, assign_auditee_id_auditee)
                values
                ('" . $id . "','" . $id_auditee . "')";
        $this->_db->_dbquery($sql);
    }
    function assign_edit_auditee($id_detail, $id_auditee) {
        $sql = "update assignment_auditee set assign_auditee_id_auditee = '".$id_auditee."' where assign_auditee_id = '".$id_detail."' ";
        $this->_db->_dbquery ( $sql );
    }
    public function assign_del_auditee($id)
    {
        $sql = "delete from assignment_auditee where assign_auditee_id_assign = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function assign_edit($id, $tipe_audit, $tahun, $tanggal_awal, $tanggal_akhir, $dasar, $hari_kerja, $periode, $kegiatan, $hari_persiapan, $hari_pelaksanaan, $hari_pelaporan, $pendanaan, $keterangan, $assign_attach, $tanggal_persiapan_awal, $tanggal_persiapan_akhir, $tanggal_pelaksanaan_awal, $tanggal_pelaksanaan_akhir, $tanggal_pelaporan_awal, $tanggal_pelaporan_akhir, $pedahuluan, $tujuan, $instruksi)
    {
        $sql = "update assignment set assign_tipe_id = '" . $tipe_audit . "', assign_tahun = '" . $tahun . "', assign_start_date = '" . $tanggal_awal . "', assign_end_date = '" . $tanggal_akhir . "', assign_dasar = '" . $dasar . "', assign_hari = '" . $hari_kerja . "', assign_periode = '" . $periode . "', assign_kegiatan = '" . $kegiatan . "', assign_hari_persiapan = '" . $hari_persiapan . "', assign_hari_pelaksanaan = '" . $hari_pelaksanaan . "', assign_hari_pelaporan = '" . $hari_pelaporan . "', assign_pendanaan = '" . $pendanaan . "', assign_keterangan = '" . $keterangan . "', assign_file = '" . $assign_attach . "', assign_persiapan_awal = '" . $tanggal_persiapan_awal . "', assign_persiapan_akhir = '" . $tanggal_persiapan_akhir . "', assign_pelaksanaan_awal = '" . $tanggal_pelaksanaan_awal . "', assign_pelaksanaan_akhir = '" . $tanggal_pelaksanaan_akhir . "', assign_pelaporan_awal = '" . $tanggal_pelaporan_awal . "', assign_pelaporan_akhir = '" . $tanggal_pelaporan_akhir . "', assign_pendahuluan = '" . $pedahuluan . "', assign_tujuan = '" . $tujuan . "', assign_instruksi = '" . $instruksi . "'
                where assign_id = '" . $id . "' ";
        $aksinyo = "Mengubah Penugasan Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_delete($id)
    {
        $this->assign_del_auditee($id);
        $this->assign_del_auditor($id);
        $sql     = "delete from assignment where assign_id = '" . $id . "' ";
        $aksinyo = "Menghapus Penugasan Audit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function count_auditor_assign($assign_id)
    {
        $sql  = "select count(*) FROM assignment_auditor where assign_auditor_id_assign = '" . $assign_id . "' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function view_auditor_assign($assign_id)
    {
        $sql = "select auditor_id, auditor_name, auditor_nip, posisi_name, MIN(assign_auditor_start_date) as start_date, MAX(assign_auditor_end_date) as end_date, pangkat_name,  jenis_jabatan, jenis_jabatan_sub, assign_auditor_prepday, assign_auditor_execday, assign_auditor_reportday
                FROM assignment_auditor
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_pangkat_auditor on auditor_id_pangkat = pangkat_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                where assign_auditor_id_assign = '" . $assign_id . "'  group by auditor_id, auditor_name, auditor_nip, posisi_name order by posisi_sort";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function view_auditor_pm_lha($assign_id, $auditee_id){
        $sql = "select auditor_id, auditor_name, auditor_nip, posisi_name, MIN(assign_auditor_start_date) as start_date, MAX(assign_auditor_end_date) as end_date, pangkat_desc, pangkat_name, jenis_jabatan, jenis_jabatan_sub, assign_auditor_prepday, assign_auditor_execday, assign_auditor_reportday, auditee_name
                FROM assignment_auditor 
                left join auditor on assign_auditor_id_auditor = auditor_id 
                left join par_pangkat_auditor on auditor_id_pangkat = pangkat_id 
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id 
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id 
                left join assignment_auditee on assign_auditor_id_assign = assign_auditee_id_assign
                left join auditee on assign_auditee_id_auditee = auditee_id
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditee_id_auditee = '" . $auditee_id . "'  and posisi_name = 'Pengendali Mutu'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function view_auditor_lha($assign_id, $auditee_id){
        $sql = "select auditor_id, auditor_name, auditor_nip, posisi_name, MIN(assign_auditor_start_date) as start_date, MAX(assign_auditor_end_date) as end_date, pangkat_desc, pangkat_name, jenis_jabatan, jenis_jabatan_sub, assign_auditor_prepday, assign_auditor_execday, assign_auditor_reportday, auditee_name
                FROM assignment_auditor 
                left join assignment_auditee on assign_auditor_id_assign = assign_auditee_id_assign
                left join auditee on assign_auditee_id_auditee = auditee_id
                left join auditor on assign_auditor_id_auditor = auditor_id 
                left join par_pangkat_auditor on auditor_id_pangkat = pangkat_id 
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id 
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id 
                where assign_auditor_id_assign = '" . $assign_id . "' and assign_auditee_id_auditee = '" . $auditee_id . "' and posisi_name NOT IN ('Penanggung Jawab', 'Pengendali Mutu') group by auditor_id, auditor_name, auditor_nip, posisi_name order by posisi_sort";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_update_status($id, $status)
    {
        $sql     = "update assignment set assign_status = '" . $status . "' where assign_id = '" . $id . "' ";
        $aksinyo = "Mengubah Status Penugasan Audit dengan ID " . $id . " menjadi " . $status;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_add_komentar($id, $komentar, $tanggal)
    {
        $sql = "insert into assignment_comment
                (assign_comment_id, assign_comment_assign_id, assign_comment_user_id, assign_comment_desc, assign_comment_date)
                values
                ('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komentar . "','" . $tanggal . "')";
        $aksinyo = "Mengomentari Penugasan dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_komentar_viewlist($id)
    {
        $sql = "select auditor_name, assign_comment_desc, assign_comment_date
                FROM assignment_comment
                left join user_apps on assign_comment_user_id = user_id
                left join auditor on user_id_internal = auditor_id
                where assign_comment_assign_id = '" . $id . "' order by assign_comment_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_add_upload($id, $nama_file)
    {
        $sql = "insert into assignment_upload_awal
                (upload_awal_id_assign, upload_awal_nama_file)
                values
                ('" . $id . "','" . $nama_file . "')";
        $this->_db->_dbquery($sql);
    }
    public function assign_del_upload($id)
    {
        $sql = "delete from assignment_upload_awal where upload_awal_id_assign = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function assign_upload_viewlist($id)
    {
        $sql = "select upload_awal_nama_file
                FROM assignment_upload_awal
                where upload_awal_id_assign = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    // anggota
    public function auditor_assign_count($assign_id, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select count(*)
                from assignment_auditor
                join assignment on assign_auditor_id_assign = assign_id
                left join auditee on assign_auditor_id_auditee = auditee_id
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where assign_auditor_id_assign = '" . $assign_id . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function auditor_assign_view_grid($assign_id, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select assign_auditor_id, auditor_name, auditee_name, posisi_name, assign_auditor_cost, assign_auditor_day, assign_status
                from assignment_auditor
                join assignment on assign_auditor_id_assign = assign_id
                left join auditee on assign_auditor_id_auditee = auditee_id
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where assign_auditor_id_assign = '" . $assign_id . "' " . $condition . " order by auditee_name asc, posisi_sort asc
                LIMIT $offset, $num_row ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_auditor_add($id, $assign_id, $auditee_id, $anggota_id, $posisi_id, $tanggal_awal, $tanggal_akhir, $jml_hari, $hari_kerja, $hari_persiapan, $hari_pelaksanaan, $hari_pelaporan)
    {
        $sql = "insert into assignment_auditor
                (assign_auditor_id, assign_auditor_id_assign, assign_auditor_id_auditee, assign_auditor_id_auditor, assign_auditor_id_posisi, assign_auditor_start_date, assign_auditor_end_date, assign_auditor_day, assign_auditor_workday, assign_auditor_prepday, assign_auditor_execday, assign_auditor_reportday)
                values
                ('" . $id . "','" . $assign_id . "','" . $auditee_id . "','" . $anggota_id . "','" . $posisi_id . "','" . $tanggal_awal . "','" . $tanggal_akhir . "','" . $jml_hari . "','" . $hari_kerja . "','" . $hari_persiapan . "','" . $hari_pelaksanaan . "','" . $hari_pelaporan . "')";
        $aksinyo = "Menambah anggota dengan id " . $id . " pada penugasan id " . $assign_id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_auditor_viewlist($id)
    {
        $sql = "select assign_auditor_id, assign_auditor_id_auditee, assign_auditor_id_auditor, assign_auditor_id_posisi, assign_auditor_day, assign_auditor_cost, auditor_golongan, assign_auditor_start_date, assign_auditor_end_date, assign_auditor_prepday, assign_auditor_execday, assign_auditor_reportday, assign_auditor_id_assign 
                from assignment_auditor
                left join auditor on assign_auditor_id_auditor = auditor_id
                where assign_auditor_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
	public function assign_auditor_assignment_viewlist($id)
    {
        $sql = "select assign_auditor_id, assign_auditor_id_auditee, assign_auditor_id_auditor, assign_auditor_id_posisi, assign_auditor_day, assign_auditor_cost, auditor_golongan, assign_auditor_start_date, assign_auditor_end_date, assign_auditor_prepday, assign_auditor_execday, assign_auditor_reportday, assign_auditor_id_assign, posisi_sort
                from assignment_auditor
                left join auditor on assign_auditor_id_auditor = auditor_id
				left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
				where assign_auditor_id_assign = '" . $id . "' order by posisi_sort asc";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_auditor_edit($id, $auditee_id, $anggota_id, $posisi_id, $tanggal_awal, $tanggal_akhir, $jml_hari, $hari_kerja, $hari_persiapan, $hari_pelaksanaan, $hari_pelaporan)
    {
        $sql = "update assignment_auditor set assign_auditor_id_auditee = '" . $auditee_id . "', assign_auditor_id_auditor = '" . $anggota_id . "', assign_auditor_id_posisi = '" . $posisi_id . "', assign_auditor_start_date = '" . $tanggal_awal . "', assign_auditor_end_date = '" . $tanggal_akhir . "', assign_auditor_day = '" . $jml_hari . "', assign_auditor_workday = '" . $hari_kerja . "', assign_auditor_prepday = '" . $hari_persiapan . "', assign_auditor_execday = '" . $hari_pelaksanaan . "', assign_auditor_reportday = '" . $hari_pelaporan . "'
                where assign_auditor_id = '" . $id . "' ";
        $aksinyo = "Mengubah Anggota id " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_auditor_delete($id)
    {
        $sql     = "delete from assignment_auditor where assign_auditor_id = '" . $id . "' ";
        $aksinyo = "Menghapus anggota id " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_del_auditor($id)
    {
        $sql = "delete from assignment_auditor where assign_auditor_id_assign = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function assign_auditor_detil_add($id, $kode, $jml_hari, $nilai, $total)
    {
        $sql = "insert into assignment_auditor_detil
                (anggota_assign_detil_id, anggota_assign_detil_kode_sbu, anggota_assign_detil_jml_hari, anggota_assign_detil_nilai, anggota_assign_detil_total)
                values
                ('" . $id . "','" . $kode . "','" . $jml_hari . "','" . $nilai . "','" . $total . "')";
        $this->_db->_dbquery($sql);
    }
    public function assign_auditor_detil_clean($id)
    {
        $sql = "delete from assignment_auditor_detil where anggota_assign_detil_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function assign_auditor_update_sum_biaya($id, $biaya_audit)
    {
        $sql = "update assignment_auditor set assign_auditor_cost = '" . $biaya_audit . "' where assign_auditor_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    public function assign_auditor_detil_viewlist($id)
    {
        $sql = "select anggota_assign_detil_id, anggota_assign_detil_kode_sbu, anggota_assign_detil_jml_hari, anggota_assign_detil_nilai, anggota_assign_detil_total
                from assignment_auditor_detil
                where anggota_assign_detil_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_cek_katim($assign_id, $userId)
    {
        $sql = "select assign_auditor_id_posisi
                from assignment_auditor
                left join user_apps on assign_auditor_id_auditor = user_id_internal
                where assign_auditor_id_assign = '" . $assign_id . "' and user_id = '" . $userId . "'  and assign_auditor_id_posisi = '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7' ";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
	//end anggota

    //surat tugas
    public function surat_tugas_count($assign_id, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select count(*)
                from assignment_surat_tugas
                join assignment on assign_surat_id_assign = assign_id
                left join auditor on assign_surat_id_auditorTTD = auditor_id
                where assign_surat_id_assign = '" . $assign_id . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function surat_tugas_view_grid($assign_id, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
        $sql = "select assign_surat_id, assign_surat_id_assign, assign_start_date, assign_end_date, assign_surat_no, assign_surat_tgl, auditor_name, case assign_surat_status when '1' then 'Sedang Direview' when '2' then 'Disetujui' when '3' then 'Ditolak' else '' end as status, assign_surat_status
                from assignment_surat_tugas
                join assignment on assign_surat_id_assign = assign_id
                left join auditor on assign_surat_id_auditorTTD = auditor_id
                where assign_surat_id_assign = '" . $assign_id . "' " . $condition . "
                LIMIT $offset, $num_row ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function surat_tugas_add($assign_id, $id, $no_surat, $tanggal_surat, $ttd_id, $jabatan_surat, $tembusan, $inspektorat)
    {
        $sql = "insert into assignment_surat_tugas
                (assign_surat_id_assign, assign_surat_id, assign_surat_no, assign_surat_tgl, assign_surat_id_auditorTTD, assign_surat_jabatanTTD, assign_surat_tembusan, assign_inspektorat_id)
                values
                ('" . $assign_id . "', '" . $id . "', '" . $no_surat . "', '" . $tanggal_surat . "', '" . $ttd_id . "', '" . $jabatan_surat . "', '" . $tembusan . "', '" . $inspektorat . "')";
        $this->_db->_dbquery($sql);
    }
    public function surat_tugas_edit($id, $no_surat, $tanggal_surat, $ttd_id, $jabatan_surat, $tembusan, $inspektorat)
    {
        $sql     = "update assignment_surat_tugas set assign_surat_no = '" . $no_surat . "', assign_surat_tgl = '" . $tanggal_surat . "', assign_surat_id_auditorTTD = '" . $ttd_id . "', assign_surat_jabatanTTD = '" . $jabatan_surat . "', assign_surat_tembusan = '" . $tembusan . "', assign_inspektorat_id = '" . $inspektorat . "' where assign_surat_id = '" . $id . "' ";
        $aksinyo = "Mengubah Surat Tugas dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
	public function surat_tugas_shortcut_edit($id, $no_surat, $tanggal_surat, $ttd_id, $jabatan_surat, $tembusan, $inspektorat)
    {
        $sql     = "update assignment_surat_tugas set assign_surat_no = '" . $no_surat . "', assign_surat_tgl = '" . $tanggal_surat . "', assign_surat_id_auditorTTD = '" . $ttd_id . "', assign_surat_jabatanTTD = '" . $jabatan_surat . "', assign_surat_tembusan = '" . $tembusan . "', assign_inspektorat_id = '" . $inspektorat . "' where assign_surat_id_assign = '" . $id . "' ";
        $aksinyo = "Mengubah Surat Tugas dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function surat_tugas_viewlist($id)
    {
        $sql = "select assign_surat_id, assign_surat_no, assign_surat_tgl, assign_surat_id_auditorTTD, assign_surat_tembusan, auditor_name, assign_surat_jabatanTTD, assign_inspektorat_id, inspektorat_name
                from assignment_surat_tugas
                left join auditor on assign_surat_id_auditorTTD = auditor_id
                left join par_inspektorat on assign_inspektorat_id = inspektorat_id
                where assign_surat_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
	public function surat_tugas_assignment_viewlist($id)
    {
        $sql = "select assign_surat_id, assign_surat_no, assign_surat_tgl, assign_surat_id_auditorTTD, assign_surat_tembusan, auditor_name, assign_surat_jabatanTTD, assign_inspektorat_id, inspektorat_name
                from assignment_surat_tugas
                left join auditor on assign_surat_id_auditorTTD = auditor_id
                left join par_inspektorat on assign_inspektorat_id = inspektorat_id
                where assign_surat_id_assign = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function surat_assign_viewlist($id)
    {
        $sql = "select assign_surat_id, assign_surat_no, assign_surat_tgl, assign_dasar, assign_surat_tembusan, auditor_name, assign_surat_jabatanTTD, assign_surat_id_assign, assign_tahun, auditor_nip, assign_surat_status, audit_type_opsi, assign_kegiatan, assign_hari_persiapan, assign_hari_pelaksanaan, assign_hari_pelaporan, assign_start_date, assign_end_date, assign_pendanaan, assign_keterangan, assign_hari, jenis_jabatan_sub, assign_inspektorat_id, inspektorat_name
                from assignment_surat_tugas
                left join auditor on assign_surat_id_auditorTTD = auditor_id
                left join assignment on assign_surat_id_assign = assign_id
                left join par_audit_type on assign_tipe_id = audit_type_id
                left join par_jenis_jabatan on auditor_id_jabatan = jenis_jabatan_id
                left join par_inspektorat on assign_inspektorat_id = inspektorat_id
                where assign_surat_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function surat_tugas_add_komentar($id, $komentar, $tanggal)
    {
        $sql = "insert into assignment_surat_comment
                (surat_comment_id, surat_comment_surat_id, surat_comment_user_id, surat_comment_desc, surat_comment_date)
                values
                ('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komentar . "','" . $tanggal . "')";
        $aksinyo = "Mengomentari Surat Tugas dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function surat_tugas_komentar_viewlist($id)
    {
        $sql = "select auditor_name, surat_comment_desc, surat_comment_date
                FROM assignment_surat_comment
                left join user_apps on surat_comment_user_id = user_id
                left join auditor on user_id_internal = auditor_id
                where surat_comment_surat_id = '" . $id . "' order by surat_comment_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function surat_tugas_update_status($id, $status, $tanggal)
    {
        $sql     = "update assignment_surat_tugas set assign_surat_status = '" . $status . "', assign_surat_id_userPropose = '" . $this->userId . "', assign_surat_tgl_userPropose = '" . $tanggal . "' where assign_surat_id = '" . $id . "' ";
        $aksinyo = "Mengubah Status Surat Tugas dengan ID " . $id . " menjadi " . $status;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function surat_tugas_delete($id)
    {
        $sql = "delete from assignment_surat_tugas where assign_surat_id = '" . $id . "' ";
        $this->_db->_dbquery($sql);
    }
    //end surat tugas

    // laporan
    public function assign_tahun_viewlist()
    {
        $sql  = "select distinct assign_tahun from assignment order by assign_tahun asc";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    // lha
    public function assign_lha_viewlist($id_assign, $id_auditee)
    {
        $sql = "select distinct lha_id, lha_no, lha_date, lha_ringkasan, lha_metodologi, lha_tujuan_audit, lha_ruanglingkup, lha_batasan, lha_kegiatan, lha_strategi_laporan, lha_hasil, assign_id, assign_surat_no, assign_no_lha, assign_date_lha, assign_periode, assign_dasar, assign_kegiatan, assign_start_date, assign_end_date, lha_status, lha_id, assign_tahun, audit_type_name, lha_dasar_audit, lha_data_auditan, lha_penutup, lha_tl_audit_lalu, assign_surat_tgl, propinsi_name, lha_pendekatan, nha_start_date, nha_end_date, auditee_alamat, auditee_telp, auditee_fax, auditee_kode, auditee_kppn, auditee_bank, auditee_fasilitas, auditee_jml_pegawai, auditee_pejabat_name, auditee_pejabat_nip, pangkat_name, auditee_pejabat_jabatan, assign_surat_tgl, nha_id, nha_auditee_id
                from assignment_lha
                left join assignment on assign_id = lha_id_assign
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join par_audit_type on assign_tipe_id = audit_type_id
                left join assignment_auditee on assign_id = assign_auditee_id_assign
                left join assignment_nha on lha_id_assign = nha_assign_id
				left join auditee on nha_auditee_id = auditee_id
				left join par_pangkat_auditor on auditee_pejabat_pangkat = pangkat_id
                left join par_propinsi on auditee_propinsi_id = propinsi_id
                where lha_id_assign = '".$id_assign."' and nha_auditee_id = '".$id_auditee."' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
	public function assign_lha_pagu_damu_viewlist($id_assign, $id_auditee)
    {
        $sql = "select distinct lha_no, lha_date, auditee_name, lha_id_auditee, lha_id_assign
                from assignment_lha 
                join auditee on lha_id_auditee = auditee_id 
                where lha_id_assign = '" . $id_assign . "' and lha_id_auditee = '".$id_auditee."' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    // spl
    public function assign_spl_viewlist($id_assign, $id_auditee)
    {
        $sql = "select lha_no, lha_date, lha_ringkasan, lha_metodologi, lha_tujuan_audit, lha_ruanglingkup, lha_batasan, lha_kegiatan, lha_strategi_laporan, lha_hasil, assign_id, assign_surat_no, assign_no_lha, assign_date_lha, assign_periode, assign_dasar, assign_kegiatan, assign_start_date, assign_end_date, lha_status, lha_id, assign_tahun, audit_type_name, lha_dasar_audit, lha_data_auditan, lha_penutup, lha_tl_audit_lalu, assign_surat_tgl, spl_no, spl_klasifikasi, spl_date, spl_lampiran, spl_perihal, spl_tempat_ttd, spl_date, spl_yth, spl_dasar, spl_temuan_rekomendasi, spl_penutup, spl_tembusan, spl_pj, propinsi_name, auditor_name, auditor_nip, pangkat_name, esselon_name
                from assignment_lha
                left join assignment on assign_id = lha_id_assign
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join par_audit_type on assign_tipe_id = audit_type_id
                left join assignment_auditee on assign_id = assign_auditee_id_assign
				left join assignment_nha on lha_id_assign = nha_assign_id
                left join auditee on assign_auditee_id_auditee = auditee_id
                left join par_esselon on auditee_esselon = esselon_id
                left join par_propinsi on auditee_propinsi_id = propinsi_id
                left join auditor on spl_pj = auditor_id
                left join par_pangkat_auditor on auditor_id_pangkat = pangkat_id
                where lha_id_assign = '".$id_assign."' and nha_auditee_id = '".$id_auditee."' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function list_lha_lampiran($id)
    {
        $sql = "select lha_attach_id_assign, lha_attach_name
                from assignment_lha_attachment where lha_attach_id_assign = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function insert_lampiran_lha($id, $attach_name)
    {
        $sql = "insert into assignment_lha_attachment (lha_attach_id, lha_attach_id_assign, lha_attach_name) values ('" . $this->uniq_id() . "', '" . $id . "', '" . $attach_name . "')";
        $this->_db->_dbquery($sql);
    }
    public function delete_lampiran_kka($id, $attach_name)
    {
        $sql = "delete from assignment_lha_attachment where lha_attach_id_assign = '" . $id . "' and lha_attach_name = '" . $attach_name . "' ";
        $this->_db->_dbquery($sql);
    }
    public function assign_update_lha($id, $no_lha, $tanggal_lha)
    {
        $sql     = "update assignment set assign_no_lha = '" . $no_lha . "', assign_date_lha = '" . $tanggal_lha . "' where assign_id = '" . $id . "' ";
        $aksinyo = "Mengubah LHA Penugasan AUdit dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function lha_update($lha_id, $assign_id, $no_lha, $tanggal_lha, $ringkasan, $dasar_audit, $tujuan_audit, $metodologi_audit, $strategi_laporan, $data_umum_auditan, $lha_ruanglingkup, $batasan, $kegiatan, $hasil, $penutup, $tl_audit_lalu, $status, $spl_no, $spl_klasifikasi, $spl_lampiran, $spl_perihal, $spl_tempat_ttd, $spl_date, $spl_yth, $spl_dasar, $spl_temuan_rekomendasi, $spl_penutup, $spl_tembusan, $spl_pj, $lha_pendekatan, $auditee_id)
    {
        $sql  = "insert into assignment_lha
                 (lha_id, lha_id_assign, lha_no, lha_date, lha_ringkasan, lha_dasar_audit, lha_tujuan_audit, lha_metodologi, lha_strategi_laporan, lha_data_auditan, lha_ruanglingkup, lha_batasan, lha_kegiatan, lha_hasil, lha_penutup, lha_tl_audit_lalu, lha_status, spl_no, spl_klasifikasi, spl_lampiran, spl_perihal, spl_tempat_ttd, spl_date, spl_yth, spl_dasar, spl_temuan_rekomendasi, spl_penutup, spl_tembusan, spl_pj, lha_pendekatan, lha_id_auditee) 
                 values 
                 ('".$lha_id."', '".$assign_id."', '".$no_lha."', '".$tanggal_lha."', '".$ringkasan."', '".$dasar_audit."', '".$tujuan_audit."', '".$metodologi_audit."', '".$strategi_laporan."', '".$data_umum_auditan."', '".$lha_ruanglingkup."', '".$batasan."', '".$kegiatan."', '".$hasil."', '".$penutup."', '".$tl_audit_lalu."', '".$status."', '".$spl_no."', '".$spl_klasifikasi."', '".$spl_lampiran."', '".$spl_perihal."', '".$spl_tempat_ttd."', '".$spl_date."', '".$spl_yth."', '".$spl_dasar."', '".$spl_temuan_rekomendasi."', '".$spl_penutup."', '".$spl_tembusan."', '".$spl_pj."', '".$lha_pendekatan."', '".$auditee_id."')
                  
                 ON DUPLICATE KEY UPDATE lha_id = '".$lha_id."', lha_id_assign = '".$assign_id."', lha_no = '" . $no_lha . "', lha_date = '" . $tanggal_lha . "', lha_ringkasan = '" . $ringkasan . "', lha_dasar_audit = '" . $dasar_audit . "', lha_tujuan_audit = '" . $tujuan_audit . "', lha_metodologi = '" . $metodologi_audit . "', lha_strategi_laporan = '" . $strategi_laporan . "', lha_data_auditan = '" . $data_umum_auditan . "', lha_ruanglingkup = '" . $lha_ruanglingkup . "', lha_batasan = '".$batasan."', lha_kegiatan = '".$kegiatan."', lha_hasil = '".$hasil."', lha_penutup = '" . $penutup . "', lha_tl_audit_lalu = '" . $tl_audit_lalu . "', lha_status = '" . $status . "', spl_no = '" . $spl_no . "', spl_klasifikasi = '" . $spl_klasifikasi . "', spl_lampiran = '" . $spl_lampiran . "', spl_perihal = '" . $spl_perihal . "', spl_tempat_ttd = '" . $spl_tempat_ttd . "', spl_date = '" . $spl_date . "', spl_yth = '" . $spl_yth . "', spl_dasar = '" . $spl_dasar . "', spl_temuan_rekomendasi = '".$spl_temuan_rekomendasi."', spl_penutup = '" . $spl_penutup . "', spl_tembusan = '" . $spl_tembusan . "', spl_pj = '" . $spl_pj . "', lha_pendekatan = '" . $lha_pendekatan . "', lha_id_auditee = '".$auditee_id."' ";
       $data = $this->_db->_dbquery($sql);
    }
    public function lha_update_status($id, $status)
    {
        $sql     = "update assignment_lha set lha_status = '" . $status . "' where lha_id_assign = '" . $id . "' ";
        $aksinyo = "Mengubah Status LHA dengan ID " . $id . " menjadi " . $status;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }

    public function temuan_list($assign_id)
    {
        $sql = "select finding_id, finding_judul, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_tanggapan_auditee, finding_tanggapan_auditor, concat(finding_type_code, '.' ,sub_type_code) as kode_temuan
                FROM finding_internal
                left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
                left join par_finding_sub_type on finding_sub_id = sub_type_id
                where finding_assign_id = '" . $assign_id . "' and (finding_status = 3 or finding_status = 4 or finding_status = 8) ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function temuan_list_nha($assign_id)
    {
        $sql = "select finding_id, finding_judul, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_tanggapan_auditee, finding_tanggapan_auditor, concat(finding_type_code, '.' ,sub_type_code, '.', jenis_temuan_code) as kode_temuan, kel_penyebab_code, kode_penyebab_name, concat(penyebab_sub_sub_code, '.', kode_penyebab_name, '.', penyebab_sub_sub_code) as kode_penyebab_temuan
                FROM finding_internal
                left join par_finding_type on finding_internal.finding_type_id = par_finding_type.finding_type_id
                left join par_finding_sub_type on finding_sub_id = sub_type_id
				left join par_finding_jenis on finding_jenis_id = jenis_temuan_id
                left join par_kelompok_penyebab on finding_penyebab_id = kel_penyebab_id
				left join par_kode_penyebab on finding_sub_penyebab_id = kode_penyebab_id
				left join par_kode_penyebab_sub on finding_sub_sub_penyebab_id = penyebab_sub_sub_id
                where finding_assign_id = '" . $assign_id . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function temuan_list_matrix($assign_id)
    {
        $sql = "select finding_id, finding_judul, finding_kondisi, finding_kriteria, finding_sebab, finding_akibat, finding_tanggapan_auditee, finding_tanggapan_auditor, concat(finding_type_code, '.' ,sub_type_code) as kode_temuan
                FROM finding_internal_matrix
                left join par_finding_type on finding_internal_matrix.finding_type_id = par_finding_type.finding_type_id
                left join par_finding_sub_type on finding_sub_id = sub_type_id
                where finding_assign_id = '" . $assign_id . "' and (finding_status = 3 or finding_status = 4 or finding_status = 8) ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function get_user_by_posisi($id_assign, $id_posisi = "")
    {
        $condition = '';
        if ($id_posisi != '') {
            $condition = " and assign_auditor_id_posisi = '" . $id_posisi . "' ";
        }

        $sql = "select DISTINCT user_id from assignment_auditor
                join user_apps on assign_auditor_id_auditor = user_id_internal
                where assign_auditor_id_assign = '" . $id_assign . "'" . $condition;
        $rs  = $this->_db->_dbquery($sql);
        $arr = $rs->FetchRow();
        return $arr[0];
    }

    public function lha_add_komentar($id, $komentar, $tanggal)
    {
        $sql = "insert into assignment_lha_comment
                (lha_comment_id, lha_comment_lha_id, lha_comment_user_id, lha_comment_desc, lha_comment_date)
                values
                ('" . $this->uniq_id() . "','" . $id . "','" . $this->userId . "','" . $komentar . "','" . $tanggal . "')";
        $aksinyo = "Mengomentari LHA dengan ID " . $id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }

    public function lha_komentar_viewlist($id)
    {
        $sql = "select auditor_name, lha_comment_desc, lha_comment_date
                FROM assignment_lha_comment
                left join user_apps on lha_comment_user_id = user_id
                left join auditor on user_id_internal = auditor_id
                where lha_comment_lha_id = '" . $id . "' order by lha_comment_date ASC";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function anggota_list($assign_id, $posisi = "", $ktplusat = false)
    {
        $condition = "";
        if ($posisi == "kt") {
            $condition = " and posisi_id = '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7' ";
        }

        if ($posisi == "pt") {
            $condition = " and posisi_id = '9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd' ";
        }

        if ($posisi == "pm") {
            $condition = " and posisi_id = '1fe7f8b8d0d94d54685cbf6c2483308aebe96229' ";
        }

        if ($posisi == "at") {
            $condition = " and posisi_id = '6a70c2a39af30df978a360e556e1102a2a0bdc02' ";
        }

        if ($ktplusat) {
            $condition = " and (posisi_id = '6a70c2a39af30df978a360e556e1102a2a0bdc02' or posisi_id = '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7') ";
        }

        $sql = "select distinct auditor_id, auditor_name, posisi_name, user_id
                from assignment_auditor
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                left join user_apps on auditor_id = user_id_internal
                where assign_auditor_id_assign = '" . $assign_id . "' " . $condition . " order by posisi_sort";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function anggota_list_kma($assign_id)
    {
        $sql = "select distinct auditor_id, auditor_name, posisi_name, user_id
                from assignment_auditor
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                left join user_apps on auditor_id = user_id_internal
                where assign_auditor_id_assign = '" . $assign_id . "' and posisi_id in ('9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7') order by posisi_sort";
        $data = $this->_db->_dbquery($sql);
        // echo $sql;
        return $data;
    }

    public function rekomendasi_list($find_id)
    {
        $sql = "select rekomendasi_desc
                from rekomendasi_internal
                where rekomendasi_finding_id = '" . $find_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function auditee_detil($auditee_id)
    {
        $sql = "select auditee_id, auditee_name
                from auditee
                where auditee_id = '" . $auditee_id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    //assign tl

    public function assign_tl_count($auditee_id = "", $key_search, $val_search, $all_field, $base_on_id_int = "")
    {
        $condition = "";
        if ($auditee_id == '0') {
            $auditee_id = "";
        }

        if ($auditee_id != "") {
            $condition = " and assign_auditee_id_auditee = '" . $auditee_id . "' ";
        }
		
		$condition2 = "";
        if ($base_on_id_int == '0') {
            $base_on_id_int = "";
        }

        if ($base_on_id_int != "") {
            $condition2 = " and assign_auditor_id_auditor = '" . $base_on_id_int . "' ";
        }

        $condition3 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition3 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition3 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition3 = " and (" . $condition3 . ")";
            }
        }

        $sql = "select count(distinct assign_id) FROM assignment
                left join assignment_auditee on assign_id = assign_auditee_id_assign
                left join assignment_lha on assign_id = lha_id_assign
				left join assignment_auditor on assign_id = assign_auditor_id_assign
                where 1=1 ".$condition.$condition2.$condition3;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function assign_tl_view_grid($auditee_id = "", $key_search, $val_search, $all_field, $offset, $num_row, $base_on_id_int = "")
    {
        $condition = "";
        if ($auditee_id == '0') {
            $auditee_id = "";
        }

        if ($auditee_id != "") {
            $condition = " and assign_auditee_id_auditee = '" . $auditee_id . "' ";
        }
		
		$condition2 = "";
        if ($base_on_id_int == '0') {
            $base_on_id_int = "";
        }

        if ($base_on_id_int != "") {
            $condition2 = " and assign_auditor_id_auditor = '" . $base_on_id_int . "' ";
        }
		
		$condition3 = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition3 = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition3 .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition3 = " and (" . $condition3 . ")";
            }
        }

        $sql = "select distinct assign_id, assign_start_date, assign_end_date, assign_status, assign_tahun, assign_surat_no, assign_no_lha, lha_status, assign_kegiatan
                from assignment
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join assignment_auditee on assign_id = assign_auditee_id_assign
                left join assignment_lha on assign_id = lha_id_assign
				left join assignment_auditor on assign_id = assign_auditor_id_assign
                where 1=1 " . $condition.$condition2.$condition3. " order by assign_tahun DESC LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    //end assign tl

    //start histori lha
    public function lha_histori_add($lha_histori_id, $lha_histori_date, $lha_id_assign, $no_lha, $tanggal_lha, $ringkasan, $dasar_audit, $tujuan_audit, $ruang_lingkup, $batasan_tanggung_jawab, $metodologi_audit, $strategi_laporan, $data_umum_auditan, $hasil_yang_dicapai, $penutup, $status_lha)
    {
        $sql = "insert into lha_histori
                (lha_id, lha_histori_id, lha_histori_date, lha_id_assign, lha_no, lha_date, lha_ringkasan, lha_dasar_audit, lha_tujuan_audit, lha_metodologi, lha_strategi_laporan, lha_data_auditan, lha_ruanglingkup, lha_batasan, lha_hasil, lha_penutup, lha_status)
                values
                ('" . $this->uniq_id() . "', '" . $lha_histori_id . "', '" . $lha_histori_date . "', '" . $lha_id_assign . "', '" . $no_lha . "', '" . $tanggal_lha . "', '" . $ringkasan . "', '" . $dasar_audit . "', '" . $tujuan_audit . "', '" . $metodologi_audit . "', '" . $strategi_laporan . "', '" . $data_umum_auditan . "', '" . $ruang_lingkup . "', '" . $batasan_tanggung_jawab . "', '" . $hasil_yang_dicapai . "', '" . $penutup . "', '" . $status_lha . "')";

        $this->_db->_dbquery($sql);
    }
    public function list_date_lha_histori($id)
    {
        $sql  = "SELECT DISTINCT(lha_histori_date) FROM lha_histori WHERE lha_histori_id = '" . $id . "'";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function count_list_date_lha_history($id)
    {
        $sql  = "SELECT COUNT(*) FROM lha_histori WHERE  lha_histori_id = '" . $id . "'";
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function list_lha_histori_by_date_by_id($date, $id)
    {
        $sql = "select lha_no, lha_date, lha_ringkasan, lha_metodologi, lha_tujuan_audit, lha_ruanglingkup, lha_batasan, lha_kegiatan, lha_strategi_laporan, lha_hasil, assign_id, assign_surat_no, assign_no_lha, assign_date_lha, assign_periode, assign_dasar, assign_kegiatan, assign_start_date, assign_end_date, lha_status, lha_id, assign_tahun, audit_type_name, lha_dasar_audit, lha_data_auditan, lha_penutup
                from lha_histori
                left join assignment on assign_id = lha_id_assign
                left join assignment_surat_tugas on assign_id = assign_surat_id_assign
                left join par_audit_type on assign_tipe_id = audit_type_id
                where lha_histori_id = '" . $id . "' and lha_histori_date = '" . $date . "'";
        $data = $this->_db->_dbquery($sql);
        // echo $sql;
        return $data;
    }

    //kma
    public function assign_auditee_only()
    {
        $sql = "select DISTINCT assign_auditee_id_auditee, auditee_name
                FROM assignment_auditee
                join assignment on assign_auditee_id_assign = assign_id
                join auditee on assign_auditee_id_auditee = auditee_id
                where 1=1 order by auditee_name";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    public function assign_list_auditor_only($id, $id_assign)
    {
        $condition = "";
        if ($id != "") {
            $condition .= " and auditor_id = '" . $id . "' ";
        }

        if ($id_assign != "") {
            $condition .= " and assign_auditor_id_assign = '" . $id_assign . "' ";
        }

        $sql = "select distinct auditor_id, auditor_name, posisi_name
                from assignment_auditor
                left join auditor on assign_auditor_id_auditor = auditor_id
                left join par_posisi_penugasan on assign_auditor_id_posisi = posisi_id
                where 1=1 " . $condition . " order by auditor_name";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    // nha
    public function assign_nha_viewlist($id)
    {
        $sql = "select assign_surat_id_assign
                FROM assignment_surat_tugas
                where assign_surat_id_assign = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_nha_viewlist2($id)
    {
        $sql = "select nha_auditee_id, auditee_name, nha_assign_id
                FROM assignment_nha
                join auditee on nha_auditee_id = auditee_id
                where nha_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }

    // pagu
    public function assign_pagu_count($id_assign, $id_auditee, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select count(*) 
                FROM assignment_lha_pagu
				left join assignment_lha on lha_pagu_lha_id = lha_id
				where lha_pagu_assign_id = '" . $id_assign . "' and lha_pagu_auditee_id = '" . $id_auditee . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function assign_pagu_view_grid($id_assign, $id_auditee, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select lha_pagu_id, lha_pagu_tahun, lha_pagu_no_dipa, lha_pagu_nilai, lha_pagu_pegawai, lha_pagu_barang, lha_pagu_modal, lha_pagu_realisasi, lha_pagu_lha_id, lha_pagu_persentase, lha_pagu_date, lha_pagu_revisi, lha_pagu_date_per
				from assignment_lha_pagu
				where lha_pagu_assign_id = '" . $id_assign . "' and lha_pagu_auditee_id = '" . $id_auditee . "' ".$condition." order by lha_pagu_tahun desc
				LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_pagu_view_lha($id_assign, $id_auditee){
        $sql = "select distinct lha_pagu_id, lha_pagu_tahun, lha_pagu_no_dipa, concat('Rp. ', lha_pagu_nilai) as lha_pagu_nilai, concat('Rp. ', lha_pagu_pegawai) as lha_pagu_pegawai, concat('Rp. ', lha_pagu_barang) as lha_pagu_barang, concat('Rp. ', lha_pagu_modal) as lha_pagu_modal, concat('Rp. ', lha_pagu_realisasi, ' (', lha_pagu_persentase, '%)') as lha_pagu_realisasi_persentase, lha_pagu_lha_id, concat('Rp. ', lha_pagu_realisasi) as lha_pagu_realisasi, concat(lha_pagu_persentase, '%') as lha_pagu_persentase, lha_pagu_date
				from assignment_lha_pagu
				where lha_pagu_assign_id = '" . $id_assign . "' and lha_pagu_auditee_id = '" . $id_auditee . "' order by lha_pagu_tahun asc";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_pagu_viewlist($id)
    {
        $sql = "select lha_pagu_id, lha_pagu_tahun, lha_pagu_no_dipa, lha_pagu_nilai, lha_pagu_pegawai, lha_pagu_barang, lha_pagu_modal, lha_pagu_realisasi, lha_pagu_persentase, lha_pagu_date, lha_pagu_revisi, lha_pagu_date_per
				FROM assignment_lha_pagu
				left join assignment_lha on lha_pagu_lha_id = lha_id
				where lha_pagu_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_pagu_view_parrent($id_assign, $id_auditee)
    {
        $sql = "select lha_pagu_id, lha_pagu_tahun, lha_pagu_no_dipa, lha_pagu_nilai, lha_pagu_pegawai, lha_pagu_barang, lha_pagu_modal, lha_pagu_realisasi, lha_pagu_persentase, lha_pagu_date, lha_pagu_revisi, lha_pagu_date_per
				FROM assignment_lha_pagu
				left join assignment_lha on lha_pagu_lha_id = lha_id
				where lha_pagu_assign_id = '" . $id_assign . "' and lha_pagu_auditee_id = '" . $id_auditee . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_pagu_add($id_lha, $id_assign, $lha_pagu_tahun, $lha_pagu_no_dipa, $lha_pagu_nilai, $lha_pagu_pegawai, $lha_pagu_barang, $lha_pagu_modal, $lha_pagu_realisasi, $lha_pagu_persentase, $id_auditee, $date, $revisi, $date_per)
    {
        $aksinyo = "Menambah Pagu pada LHA ID " . $id_lha;
        $sql = "insert into assignment_lha_pagu
				(lha_pagu_id, lha_pagu_lha_id, lha_pagu_assign_id, lha_pagu_tahun, lha_pagu_no_dipa, lha_pagu_nilai, lha_pagu_pegawai, lha_pagu_barang, lha_pagu_modal, lha_pagu_realisasi, lha_pagu_persentase, lha_pagu_auditee_id, lha_pagu_date, lha_pagu_revisi, lha_pagu_date_per)
				values
				('" . $this->uniq_id() . "', '" . $id_lha . "', '" . $id_assign . "', '" . $lha_pagu_tahun . "', '" . $lha_pagu_no_dipa . "', '" . $lha_pagu_nilai . "', '" . $lha_pagu_pegawai . "', '" . $lha_pagu_barang . "', '" . $lha_pagu_modal . "', '" . $lha_pagu_realisasi . "', '" . $lha_pagu_persentase . "', '" . $id_auditee . "', '" . $date . "', '" . $revisi . "', '" . $date_per . "')";
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_pagu_edit($id, $lha_pagu_tahun, $lha_pagu_no_dipa, $lha_pagu_nilai, $lha_pagu_pegawai, $lha_pagu_barang, $lha_pagu_modal, $lha_pagu_realisasi, $lha_pagu_persentase, $date, $revisi, $date_per)
    {
        $sql = "update assignment_lha_pagu set lha_pagu_tahun = '" . $lha_pagu_tahun . "', lha_pagu_no_dipa = '" . $lha_pagu_no_dipa . "', lha_pagu_nilai = '" . $lha_pagu_nilai . "', lha_pagu_pegawai = '" . $lha_pagu_pegawai . "', lha_pagu_barang = '" . $lha_pagu_barang . "', lha_pagu_modal = '" . $lha_pagu_modal . "', lha_pagu_realisasi = '" . $lha_pagu_realisasi . "', lha_pagu_persentase = '" . $lha_pagu_persentase . "', lha_pagu_date = '" . $date . "', lha_pagu_revisi = '" . $revisi . "', lha_pagu_date_per = '" . $date_per . "'
				where lha_pagu_id = '" . $id . "' ";
        $aksinyo = "Mengubah Pagu";
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_pagu_delete($id)
    {
        $this->assign_damu_delete($id);
        $sql     = "delete from assignment_lha_pagu where lha_pagu_id = '" . $id . "' ";
        $aksinyo = "Menghapus Pagu ".$id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }

    // data umum
    public function assign_damu_count($id_pagu, $key_search, $val_search, $all_field)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select count(*) 
                FROM assignment_lha_data_umum
				left join assignment_lha on lha_damu_lha_id = lha_id
				where lha_damu_pagu_id = '" . $id_pagu . "' " . $condition;
        $data = $this->_db->_dbquery($sql);
        $arr  = $data->FetchRow();
        return $arr[0];
    }
    public function assign_damu_view_grid($id_pagu, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select lha_damu_id, lha_damu_tahun, lha_damu_pekerjaan, lha_damu_no_kontrak, lha_damu_nilai, lha_damu_nama, concat(FROM_UNIXTIME(lha_damu_start_date, '%d-%m-%Y'), ' s/d ' ,FROM_UNIXTIME(lha_damu_end_date, '%d-%m-%Y'), '    '  ,lha_damu_jangka, ' hari kalender') as lha_damu_jangka, concat(lha_damu_fisik, '%') as lha_damu_fisik, lha_damu_keuangan, lha_damu_persentase
				from assignment_lha_data_umum
				where lha_damu_pagu_id = '" . $id_pagu . "' ".$condition."
		        LIMIT $offset, $num_row";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_damu_view_lha($id_pagu)
    {
        $sql = "select lha_damu_id, lha_damu_tahun, lha_damu_pekerjaan, lha_damu_no_kontrak, lha_damu_nilai, lha_damu_nama, concat(FROM_UNIXTIME(lha_damu_start_date, '%d-%m-%Y'), ' s/d ' ,FROM_UNIXTIME(lha_damu_end_date, '%d-%m-%Y'), '    '  ,lha_damu_jangka, ' hari kalender') as lha_damu_jangka, concat(lha_damu_fisik, '%') as lha_damu_fisik, lha_damu_keuangan, lha_damu_date, lha_damu_persentase
				from assignment_lha_data_umum
				where lha_damu_pagu_id = '" . $id_pagu . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_damu_lha_view_grid($id_lha, $key_search, $val_search, $all_field, $offset, $num_row)
    {
        $condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }

        $sql = "select lha_damu_id, lha_damu_tahun, lha_damu_pekerjaan, lha_damu_no_kontrak, lha_damu_nilai , lha_damu_nama, lha_damu_start_date, lha_damu_end_date,lha_damu_jangka, lha_damu_fisik, lha_damu_keuangan
				from assignment_lha_data_umum
				where lha_damu_lha_id = '" . $id_lha . "' ".$condition."
				order by lha_damu_tahun desc, lha_damu_lha_id desc 
				";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_damu_viewlist($id)
    {
        $sql = "select lha_damu_id, lha_damu_tahun, lha_damu_pekerjaan, lha_damu_no_kontrak, lha_damu_nilai, lha_damu_nama, lha_damu_jangka, lha_damu_start_date, lha_damu_end_date, lha_damu_fisik, lha_damu_keuangan, lha_damu_no_kontrak2, lha_damu_date, lha_damu_persentase
				FROM assignment_lha_data_umum
				left join assignment_lha on lha_damu_lha_id = lha_id
				where lha_damu_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
    public function assign_damu_add($id_lha, $id_assign, $lha_damu_tahun, $lha_damu_pekerjaan, $lha_damu_no_kontrak, $lha_damu_nilai, $lha_damu_nama, $lha_damu_jangka, $lha_damu_start_date, $lha_damu_end_date, $lha_damu_fisik, $lha_damu_keuangan, $lha_damu_no_kontrak2, $lha_damu_date, $lha_damu_pagu_id, $lha_damu_persentase)
    {
        $aksinyo = "Menambah Data Umum pada LHA ID " . $id_lha;
        $sql = "insert into assignment_lha_data_umum
				(lha_damu_id, lha_damu_lha_id, lha_damu_assign_id, lha_damu_tahun, lha_damu_pekerjaan, lha_damu_no_kontrak, lha_damu_nilai, lha_damu_nama, lha_damu_jangka, lha_damu_start_date, lha_damu_end_date, lha_damu_fisik, lha_damu_keuangan, lha_damu_no_kontrak2, lha_damu_date, lha_damu_pagu_id, lha_damu_persentase)
				values
				('" . $this->uniq_id() . "', '" . $id_lha . "', '" . $id_assign . "', '" . $lha_damu_tahun . "', '" . $lha_damu_pekerjaan . "', '" . $lha_damu_no_kontrak . "', '" . $lha_damu_nilai . "', '" . $lha_damu_nama . "', '" . $lha_damu_jangka . "', '" . $lha_damu_start_date . "', '" . $lha_damu_end_date . "', '" . $lha_damu_fisik . "', '" . $lha_damu_keuangan . "', '" . $lha_damu_no_kontrak2 . "', '" . $lha_damu_date . "', '" . $lha_damu_pagu_id . "', '" . $lha_damu_persentase  . "')";
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_damu_edit($id, $lha_damu_tahun, $lha_damu_pekerjaan, $lha_damu_no_kontrak, $lha_damu_nilai, $lha_damu_nama, $lha_damu_jangka, $lha_damu_start_date, $lha_damu_end_date, $lha_damu_fisik, $lha_damu_keuangan, $lha_damu_no_kontrak2, $lha_damu_date, $lha_damu_persentase)
    {
        $sql = "update assignment_lha_data_umum set lha_damu_tahun = '" . $lha_damu_tahun . "', lha_damu_pekerjaan = '" . $lha_damu_pekerjaan . "', lha_damu_no_kontrak = '" . $lha_damu_no_kontrak . "', lha_damu_nilai = '" . $lha_damu_nilai . "', lha_damu_nama = '" . $lha_damu_nama . "', lha_damu_jangka = '" . $lha_damu_jangka . "', lha_damu_start_date = '" . $lha_damu_start_date . "', lha_damu_end_date = '" . $lha_damu_end_date . "', lha_damu_fisik = '" . $lha_damu_fisik . "', lha_damu_keuangan = '" . $lha_damu_keuangan . "', lha_damu_no_kontrak2 = '" . $lha_damu_no_kontrak2 . "', lha_damu_date = '" . $lha_damu_date . "', lha_damu_persentase = '" . $lha_damu_persentase . "'
				where lha_damu_id = '" . $id . "' ";
        $aksinyo = "Mengubah Data Umum";
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
    public function assign_damu_delete($id)
    {
        $sql     = "delete from assignment_lha_data_umum where lha_damu_id = '" . $id . "' ";
        $aksinyo = "Menghapus Data Umum ".$id;
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
	function cek_posisi($id_assign){
        $sql = "select DISTINCT user_login_as from assignment_auditor
				left join user_apps on assign_auditor_id_auditor = user_id_internal
				where user_id = '".$this->userId."' and assign_auditor_id_assign = '".$id_assign."'";
        $rs = $this->_db->_dbquery ( $sql );
        $arr = $rs->FetchRow ();
        return $arr [0];
    } 
	// end data umum
	
	// pejabat pagu
	function assign_pejabat_count($id_pagu, $key_search, $val_search, $all_field) {
		$condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
		
		$sql = "select count(*) FROM assignment_lha_pejabat 
				left join par_pangkat_auditor on lha_pejabat_pangkat_id = pangkat_id 
				where lha_pejabat_del_st = 1 and lha_pejabat_pagu_id = '".$id_pagu."' ";
		$data = $this->_db->_dbquery ( $sql );
		$arr = $data->FetchRow ();
		return $arr [0];
	}
	function assign_pejabat_view_grid($id_pagu, $key_search, $val_search, $all_field, $offset, $num_row) {
		$condition = "";
        if ($val_search != "") {
            if ($key_search != "") {
                $condition = " and " . $key_search . " like '%" . $val_search . "%' ";
            } else {
                for ($i = 0; $i < count($all_field); $i++) {
                    $or = " or ";
                    if ($i == 0) {
                        $or = "";
                    }

                    $condition .= $or . $all_field[$i] . " like '%" . $val_search . "%' ";
                }
                $condition = " and (" . $condition . ")";
            }
        }
		$sql = "select lha_pejabat_id, lha_pejabat_nip, lha_pejabat_name, pangkat_name, lha_pejabat_jabatan
				FROM assignment_lha_pejabat 
				left join par_pangkat_auditor on lha_pejabat_pangkat_id = pangkat_id 
				where lha_pejabat_del_st = 1 and lha_pejabat_pagu_id = '".$id_pagu."' order by lha_pejabat_nip LIMIT $offset, $num_row";
		$data = $this->_db->_dbquery ( $sql );
		return $data;
	}
	public function assign_pejabat_viewlist($id){
        $sql = "select lha_pejabat_id, lha_pejabat_nip, lha_pejabat_name, lha_pejabat_pangkat_id, pangkat_name, lha_pejabat_jabatan
				FROM assignment_lha_pejabat
				left join par_pangkat_auditor on lha_pejabat_pangkat_id = pangkat_id 
				where lha_pejabat_id = '" . $id . "' ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
	public function assign_pejabat_lha($id){
        $sql = "select distinct lha_pejabat_id, lha_pejabat_nip, lha_pejabat_name, lha_pejabat_pangkat_id, pangkat_name, lha_pejabat_jabatan, lha_pejabat_pagu_id
				FROM assignment_lha_pejabat
				left join par_pangkat_auditor on lha_pejabat_pangkat_id = pangkat_id 
				where lha_pejabat_pagu_id = '" . $id . "' and lha_pejabat_del_st = 1 ";
        $data = $this->_db->_dbquery($sql);
        return $data;
    }
	function assign_pejabat_add($nip, $name, $pangkat_id, $jabatan, $id_pagu) {
		$sql = "insert into assignment_lha_pejabat (lha_pejabat_id, lha_pejabat_nip, lha_pejabat_name, lha_pejabat_pangkat_id, lha_pejabat_jabatan, lha_pejabat_del_st, lha_pejabat_pagu_id) values ('".$this->uniq_id ()."','".$nip."','".$name."','".$pangkat_id."','".$jabatan."','1','".$id_pagu."')";
		$aksinyo = "Menambah data ".$name." pada pejabat anggaran";
		$this->_db->_dbexecquery ( $sql, $this->userId, $aksinyo );
	}
	public function assign_pejabat_edit($id, $nip, $name, $pangkat_id, $jabatan){
        $sql = "update assignment_lha_pejabat set lha_pejabat_nip = '" . $nip . "', lha_pejabat_name = '" . $name . "', lha_pejabat_pangkat_id = '" . $pangkat_id . "', lha_pejabat_jabatan = '" . $jabatan . "'
				where lha_pejabat_id = '" . $id . "' ";
        $aksinyo = "Mengubah data ".$name." pada pejabat anggaran";
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
	public function assign_pejabat_delete($id){
        $sql     = "delete from assignment_lha_pejabat where lha_pejabat_id = '" . $id . "' ";
        $aksinyo = "Menghapus data ".$name." pada pejabat anggaran";
        $this->_db->_dbexecquery($sql, $this->userId, $aksinyo);
    }
	// end pejabat pagu
}
