<table class="table_grid" cellspacing="0" cellpadding="0">
	<tr>
		<?
		$jmlHeader = count ( $gridHeader );
		echo ("<th width='5%'>No</th>");
		for($j = 0; $j < $jmlHeader; $j ++) {
			echo ("<th width='" . $gridWidth [$j] . "%'>" . $gridHeader [$j] . "</th>");
		}
		if ($widthAksi != "0") {
			echo ("<th width='" . $widthAksi . "%'>Aksi</th>");
		}
		?>
	</tr>
	<?
	if ($recordcount != 0) {
		$i = 0;
		while ( $arr = $rs->FetchRow () ) {
			$i ++;
			?>
	<tr>
		<td><?=$i+$offset?></td> 
	<?
			for($j = 0; $j < count ( $gridDetail ); $j ++) {
				if ($gridHeader [$j] == "Tanggal" || $gridHeader [$j] == "Pengembalian") {
					?>
		<td align="center">
	<?
						echo $comfunc->dateIndo ( $arr [$gridDetail [$j]] );
	?>
		</td> 
	<?
				} elseif ($gridHeader [$j] == "Permasalahan / Komentar") {
					?>
						<td>
							<?php 
								$rs_komentar = $reports->km12_program_komentar($arr[$gridDetail[$j]], "Ketua Tim");
									foreach ($rs_komentar as $key => $komentar) {
										echo ++$key.'. '.$komentar['auditor_name'].' : '.$komentar['program_comment_desc'].'<br>';
									}
							 ?>
						</td>
					<?
				} elseif ($gridHeader [$j] == "Penyelesaian") {
					?>
						<td>
							<?php 
								$rs_komentar = $reports->km12_program_komentar($arr[$gridDetail[$j]], "Pengendali Teknis");
									foreach ($rs_komentar as $key => $komentar) {
										echo ++$key.'. '.$komentar['auditor_name'].' : '.$komentar['program_comment_desc'].'<br>';
									}
							 ?>
						</td>
					<?
				} elseif ($gridHeader [$j] == "Persetujuan") {
					?>
						<td>
							Pengendali Teknis
						</td>
					<?
				} else {
					?>
		<td><?=$arr[$gridDetail[$j]]?></td> 
	<?
				}
				
				if ($method == "validate_kma_29") {
					$status_owner =  $reports->km29_cek_owner ( $arr[0], $ses_id_int);
					if ($status_owner) {
						$iconEdit = 1;
						$iconDel = 1;
					}else{
						$iconEdit = 0;
						$iconDel = 0;
					}
				}
				if ($method == "validate_kma_16") {
					if ($arr['km16_status'] == 2) {
						$iconEdit = 0;
						$iconDel = 0;
					}else{
						$iconEdit = 1;
						$iconDel = 1;
					}
				}
			}
			?>
		<td>
			<?
			if($method=="validate_kma_16" ){
				if($arr['km16_status'] == 0 || $arr['km16_status'] == 3){
			?>
			<fieldset>
				<select name="status" onchange="return set_action('getajukan_km', '<?=$arr[0]?>', this.value)">
					<option value="">Pilih Status</option>
					<option value="1">Ajukan</option>
				</select>
			</fieldset>
				<?
				}else if($dalnis && $arr['km16_status'] == 1){
				?>
			<fieldset>
				<select name="status" onchange="return set_action('getapprove_km', '<?=$arr[0]?>', this.value)">
					<option value="">Pilih Status</option>
					<option value="3">Tolak Data</option>
					<option value="2">Setujui</option>
				</select>
			</fieldset>
			<?
				}
			}
			?>
	<?
			if ($iconDetail) {
				?>
			<input type="image" src="images/icn_alert_info.png" title="Rincian Data" Onclick="return set_action('getdetail', '<?=$arr[0]?>')">
			&nbsp;&nbsp;
	<?
			}
			if ($iconEdit) {
				?>
			<input type="image" src="images/icn_edit.png" title="Ubah Data" Onclick="return set_action('getedit', '<?=$arr[0]?>')">
			&nbsp;&nbsp;
	<?
			}
			if ($iconDel) {
				?>
			<input type="image" src="images/icn_trash.png" title="Hapus Data" Onclick="return set_action('getdelete', '<?=$arr[0]?>', '<?=$arr[1]?>')">
	<?
			}
			?>
		</td>
	</tr>
	<?
		}
	} else {
		?>
		<td colspan="<?=$jmlHeader+2?>">Tidak Ada Data</td> 
	<?
	}
	?>
</table>
<table width="100%">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="td_paging">
	<?
	$showPage = "";
	$jumPage = ceil ( $recordcount / $num_row );
	if ($noPage > 1)
		echo "<a href='" . $paging_request . "&page=" . ($noPage - 1) . "'> <<d </a>";
	for($page = 1; $page <= $jumPage; $page ++) {
		if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) {
			if (($showPage == 1) && ($page != 2))
				echo "<span class='titik_titik'>...</span>";
			if (($showPage != ($jumPage - 1)) && ($page == $jumPage))
				echo "<span class='titik_titik'>...</span>";
			if ($page == $noPage)
				echo "<span class='paging_aktif'>" . $page . "</span> ";
			else
				echo " <a href='" . $paging_request . "&page=" . $page . "'>" . $page . "</a> ";
			$showPage = $page;
		}
	}
	if ($noPage < $jumPage)
		echo "<a href='" . $paging_request . "&page=" . ($noPage + 1) . "'> > </a>";
	?>
	</td>
	</tr>
</table>

