<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="js/select2/select2.css" type="text/css"/>
<script type="text/javascript" src="js/select2/select2.min.js"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
			<?
			switch ($_action) {
				case "getadd" :
					?>
			<fieldset class="hr">
				<label class="span2">User Name</label> <input type="text"
					class="span2" name="name" id="name">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Password</label> <input type="password"
					class="span2" name="password" id="password">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Konfirmasi Password</label> <input
					type="password" class="span2" name="password_confirm"
					id="password_confirm">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Group</label>
				<?=$comfunc->dbCombo("nama_group", "par_group", "group_id", "group_name", "and group_del_st = 1 ", "", "", 1)?>
			</fieldset>
			<fieldset>
				<label class="span2">Login As</label> <label class="span1"> <input
					type="radio" name="login_as" id="login_as" checked="" value="1">
					Internal
				</label>
				<?
				$rs_auditor = $userms->list_auditor();
				$arr_auditor = $rs_auditor->GetArray ();
				echo $comfunc->buildCombo ( "internal_id", $arr_auditor, 0, 1, "", "", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">&nbsp;</label> <label class="span1"> <input
					type="radio" name="login_as" id="login_as" value="2"> Eksternal
				</label>
				<?=$comfunc->dbCombo("eksternal_id", "auditee_pic", "pic_id", "concat(pic_nip,' - ',pic_name) as lengkap", "and pic_del_st = 1 ", "", "", 1)?>
			</fieldset>
			<?
					break;
				case "getedit" :
					?><fieldset class="hr">
				<label class="span2">User Name</label> <input type="text"
					class="span2" name="name" id="name"
					value="<?=$arr['user_username']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Password</label> <input type="password"
					class="span2" name="password" id="password" value="xxxxxxxx">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Konfirmasi Password</label> <input
					type="password" class="span2" name="password_confirm"
					id="password_confirm" value="xxxxxxxx">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Group</label>
				<?=$comfunc->dbCombo("nama_group", "par_group", "group_id", "group_name", "and group_del_st = 1 ", $arr['user_id_group'], "", 1)?>
			</fieldset>
			<fieldset>
				<label class="span2">Login As</label> <label class="span1"> <input
					type="radio" name="login_as" id="login_as"
					<? if($arr['user_login_as']==1) echo "checked" ?> value="1">
					Internal
				</label>
				<?
				$rs_auditor = $userms->list_auditor($arr['user_id_internal']);
				$arr_auditor = $rs_auditor->GetArray ();
				echo $comfunc->buildCombo ( "internal_id", $arr_auditor, 0, 1, $arr['user_id_internal'], "", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">&nbsp;</label> <label class="span1"> <input
					type="radio" name="login_as" id="login_as"
					<? if($arr['user_login_as']==2) echo "checked" ?> value="2">
					Eksternal
				</label>
				<?=$comfunc->dbCombo("eksternal_id", "auditee_pic", "pic_id", "concat(pic_nip,' - ',pic_name) as lengkap", "and pic_del_st = 1 ", $arr['user_id_ekternal'], "", 1)?>	
				<input type="hidden" name="data_id" value="<?=$arr['user_id']?>">
			</fieldset>
			<?
					break;
			}
			?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali"
						onclick="location='<?=$def_page_request?>'"> &nbsp;&nbsp;&nbsp; <input
						type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action"
					value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script> 
$("#internal_id, #eksternal_id").select2({allowClear: true, placeholder: "Select an attribute", width: "300px"})
 
$(function() {
	$("#validation-form").validate({
		rules: {
			name: "required",
			password: {
                required: true,
                minlength: 8
            },
			password_confirm : {
				minlength : 8,
				equalTo : "#password"
			},
			nama_group : "required"
		},
		messages: {
			name: "Silahkan masukan User Name",
            password: {
                required: "Silahkan masukan Password",
                minlength: "Min 8 karakter"
            },
			password_confirm: {
                minlength: "Min 8 karakter",
				equalTo : "Password tidak cocok"
            },
			nama_group : "Pilih group"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>