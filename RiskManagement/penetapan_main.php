<?
include_once "_includes/classes/risk_class.php";
include_once "_includes/classes/param_class.php";
include_once "_includes/classes/auditee_class.php";

$risks = new risk ( $ses_userId );
$params = new param ( $ses_userId );
$auditees = new auditee ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){	
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=risk_penetapantujuan";
$acc_page_request = "penetapan_acc.php";
$acc_page_request_detil = "penetapan_detil.php";
$list_page_request = "risk_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid_risiko.php";
$gridHeader = array ("Satuan Kerja", "Tahun", "Identifikasi", "Analisis", "Evaluasi", "Penanganan");
$gridDetail = array ("auditee_name", "penetapan_tahun", "penetapan_id", "penetapan_profil_risk", "penetapan_profil_risk_residu", "penetapan_id");
$gridWidth = array ("30", "8", "9", "9", "9", "10");

$key_by = array ("Satuan Kerja", "Tahun", "Analisis", "Evaluasi");
$key_field = array ("auditee_name", "penetapan_tahun", "penetapan_profil_risk", "penetapan_profil_risk_residu");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

switch ($_action) {
	case "risk_identifikasi" :
		$_SESSION ['ses_penetapan_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		?>
<script>window.open('main_page.php?method=risk_identifikasi', '_self');</script>
<?
		break;
	case "view_analisa" :
		$_SESSION ['ses_penetapan_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		?>
<script>window.open('main_page.php?method=risk_analisa&data_action=getadd', '_self');</script>
<?
		break;
	case "view_evaluasi" :
		$_SESSION ['ses_penetapan_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		?>
<script>window.open('main_page.php?method=risk_evaluasi&data_action=getadd', '_self');</script>
<?
		break;
	case "view_penanganan" :
		$_SESSION ['ses_penetapan_id'] = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		?>
<script>window.open('main_page.php?method=risk_penanganan&data_action=getadd', '_self');</script>
<?
		break;
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Penetapan Tujuan";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $risks->penetapan_data_viewlist ( $fdata_id );
		$page_title = "Ubah Penetapan Tujuan";
		break;
	case "getdetail" :
		$_nextaction = "poststatus";
		$page_request = $acc_page_request_detil;
		$ses_penetapan_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$page_title = "Detail Risiko";
		break;
	case "postadd" :
		$fsatker = $comfunc->replacetext ( $_POST ["satker"] );
		$ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
		$fnama = $comfunc->replacetext ( $_POST ["nama"] );
		$ftujuan = $comfunc->replacetext ( $_POST ["tujuan"] );
		if ($fsatker != "" && $ftahun != "" && $fnama != "" && $ftujuan != "") {
			$cek_satker = $risks->cek_satker_tahun($fsatker, $ftahun);
			if($cek_satker==0){
				$risks->penetapan_add ( $fsatker, $ftahun, $fnama, $ftujuan );
				$comfunc->js_alert_act ( 3 );
			}else{
				$comfunc->js_alert_act ( 4, "Satker Sudah Ada Pada Tahun Ini");
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fsatker = $comfunc->replacetext ( $_POST ["satker"] );
		$ftahun = $comfunc->replacetext ( $_POST ["tahun"] );
		$fnama = $comfunc->replacetext ( $_POST ["nama"] );
		$ftujuan = $comfunc->replacetext ( $_POST ["tujuan"] );
		if ($fsatker != "" && $ftahun != "" && $fnama != "" && $ftujuan != "") {
			$cek_satker = $risks->cek_satker_tahun($fsatker, $ftahun, $fdata_id);
			if($cek_satker==0){
				$risks->penetapan_edit ( $fdata_id, $fsatker, $ftahun, $fnama, $ftujuan );
				$comfunc->js_alert_act ( 1 );
			}else{
				$comfunc->js_alert_act ( 4, "Satker Sudah Ada Pada Tahun Ini");
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "poststatus" :

		$_nextaction = "postkomentar";
		$page_request = $acc_page_request_detil;
		
		$ses_penetapan_id = $comfunc->replacetext ( $_POST ["penetapan_id"] );
		$fstatus_risiko = $comfunc->replacetext ( $_POST ["status_risk"] );
		
		$page_title = "Reviu Penetapan Tujuan";
		break;
		/*
		
		$fpenetapan_id = $comfunc->replacetext ( $_POST ["penetapan_id"] );
		$fstatus_risiko = $comfunc->replacetext ( $_POST ["status_risk"] );
		$risks->update_status_risiko ( $fpenetapan_id, $fstatus_risiko );
		if($fstatus_risiko==1) $comfunc->js_alert_act ( 7 );
		if($fstatus_risiko==2) $comfunc->js_alert_act ( 8 );
		if($fstatus_risiko==3) $comfunc->js_alert_act ( 9 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
*/
		case "postkomentar" :
			$id_risk = $comfunc->replacetext ( $_POST ["penetapan_id"] );
			$status = $comfunc->replacetext ( $_POST ["status_resiko"] );
			$fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
			$notif_date = $comfunc->dateSql(date("d-m-Y"));
			$comfunc->hapus_notif($id_risk);
			$group_menu_id = "";
			if ($status != "") {
				if($status=='1') { //ajukan
					$group_menu_id = '93';
				}elseif ($status=='3'){//tolak
					$group_menu_id = '92';
				}
				if($group_menu_id!=""){
					$rs_user_accept = $comfunc->notif_user_all_bygroup($group_menu_id);
					while($arr_user_accept = $rs_user_accept->FetchRow()){
						$comfunc->insert_notif($id_risk, $ses_userId, $arr_user_accept['user_id'], 'risk_penetapantujuan', "(Profil Risiko) ".$fkomentar, $notif_date);
					}
				}
				$risks->update_status_risiko ( $id_risk, $status );
			}
		
			$ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
			if ($fkomentar != "") {
				$risks->risk_add_komentar ( $id_risk, $fkomentar, $ftanggal );
				$comfunc->js_alert_act ( 3 );
			}
			?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
				$page_request = "blank.php";
				break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$risks->penetapan_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $risks->penetapan_count ("", $base_on_id_eks, $key_search, $val_search, $key_field);
		$rs = $risks->penetapan_view_grid ( "", $offset, $num_row, $base_on_id_eks, $key_search, $val_search, $key_field);
		$page_title = "Daftar Proses Manajemen Risiko";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
