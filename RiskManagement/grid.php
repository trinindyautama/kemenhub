<table class="table_grid" cellspacing="0" cellpadding="0">
    <tr>
        <?
        $jmlHeader = count ( $gridHeader );
        echo ("<th width='5%'>No</th>");
        for($j = 0; $j < $jmlHeader; $j ++) {
            echo ("<th width='" . $gridWidth [$j] . "%'>" . $gridHeader [$j] . "</th>");
        }
        if ($widthAksi != "0") {
            echo ("<th width='" . $widthAksi . "%'>Aksi</th>");
        }
        ?>
    </tr>
    <?
    if ($recordcount != 0) {
        $i = 0;
        while ( $arr = $rs->FetchRow () ) {
            $i ++;
            ?>
            <tr>
                <td><?=$i+$offset?></td>
                <?
                for($j = 0; $j < count ( $gridDetail ); $j ++) {
                    if ($gridHeader [$j] == "Tanggal") {
                        ?>
                        <td>
	<?
					if($method=='log_aktifitas')
					echo $comfunc->dateTimeIndo ( $arr [$gridDetail [$j]] );
					else
					echo $comfunc->dateIndo ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Status Online") {
                        ?>
                        <td>
                            <?
                            if($arr [$gridDetail [$j]]==0) echo "Offline";
                            else{ echo "Online &nbsp; &nbsp;";
                                ?>
                                <input type="image" src="images/icn_alert_error.png" title="Hapus Data" Onclick="return set_action('getKill', '<?=$arr[0]?>', '<?=$arr[1]?>')">
                                <?
                            }
                            ?>
                        </td>
                        <?
                    } else if ($gridHeader [$j] == "Tanggal Backup") {
                        ?>
                        <td>
	<?
					echo $comfunc->dateTimeIndo ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "DIPA Tanggal") {
                        ?>
                        <td>
	<?
				    echo $comfunc->dateIndo ( $arr ['lha_pagu_date'] ). "<br>" ."Revisi ke ".$arr['lha_pagu_revisi'];
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Warna") {
                        ?>
                        <td bgcolor="<?=$arr[$gridDetail[$j]]?>"><?=$arr[$gridDetail[$j]]?></td>
                        <?
                    } else if ($gridHeader [$j] == "Lampiran") {
                        ?>
                        <td><a href="#" Onclick="window.open('<?=$comfunc->baseurl("Upload_Ref").$arr[$gridDetail[$j]]?>','_blank')"><?=$arr[$gridDetail[$j]]?></a></td>
                        <?
                    } else if ($gridHeader [$j] == "Jumlah") {
                        ?>
                        <td align="right">
	<?
					echo $comfunc->format_uang ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Nilai SPK") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Realisasi Keuangan") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] )."<br>"."(".$arr ['lha_damu_persentase']."%).";
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Periode Audit") {
                        ?>
                        <td>
	<?
					echo $comfunc->dateIndo ( $arr ['nha_start_date'] ) . " s.d " . $comfunc->dateIndo ( $arr ['nha_end_date'] );
					?>
		</td>
                        <?
                    } elseif ($gridHeader [$j] == "Temuan") {
                        ?>
                        <td align="center">
                            <?
                            echo $assignment_nhas->finding_nha_count ( $arr [$gridDetail [$j]], $arr['nha_auditee_id'], "", "", "" );
                            ?>
                            <br> <input type="image" src="images/icn_search.png" title="View Temuan" Onclick="return set_action('finding_nha', '<?=$arr[$gridDetail[$j]]?>', '')">
                            <?
                            ?>
                        </td>
                        <?
                    } elseif ($gridHeader [$j] == "Rekomendasi") {
                        ?>
                        <td align="center">
                            <?
                            echo $rekomendasis->rekomendasi_count ( $arr [$gridDetail [$j]], "", "", "", "" );
                            ?>
                            <br> <input type="image" src="images/icn_search.png"
                                        title="View Rekomendasi" Onclick="return set_action('rekomendasi_nha', '<?=$arr[0]?>', '<?=$arr[1]?>')">
                            <?
                            ?>
                        </td>
                        <?
                    } elseif ($gridHeader [$j] == "Input LHA") {
                        ?>
                        <td align="center">
                            <?
                            $rs_lha = $assignment_nhas->assignment_nha_viewlist_status ( $arr ['nha_id']);
                            $arr_lha = $rs_lha->FetchRow ();
                            if($arr_lha ['lha_status']==0){
                            echo "Belum Diajukan"."<br>";
                            } else if($arr_lha ['lha_status']==1){
                            echo "Sudah Diajukan Katim atau Dalnis, Sedang Direviu Oleh Daltu"."<br>";
                            } else if($arr_lha ['lha_status']==2){
                            echo "Telah Disetujui Daltu"."<br>";
                            } else if($arr_lha ['lha_status']==3){
                            echo "Ditolak oleh Pengendali Mutu"."<br>";
                            }


                            ?>
                            <input type="image" src="images/icn_search.png" title="Input LHA" Onclick="return set_action('getlha_nha', '<?=$arr['nha_assign_id'].":".$arr['nha_auditee_id']?>', '')">
                        </td>
                        <?
                    } elseif ($gridHeader [$j] == "Jenis Kegiatan") {
                        ?>
                        <td align="center">
                            <?
                            echo $assigns->assign_damu_count ( $arr [$gridDetail [$j]], "", "", "" );
                            ?>
                            <br> <input type="image" src="images/icn_search.png" title="View Jenis Kegiatan" Onclick="return set_action('assign_lha_damu', '<?=$arr['lha_pagu_id']?>', '')">
                            <?
                            ?>
                        </td>
                    <?
                    }  elseif ($gridHeader [$j] == "Pengelola Anggaran") {
                        ?>
                        <td align="center">
                            <?
                            echo $params->pic_pagu_count ( $arr [$gridDetail [$j]], "", "", "" );
                            ?>
                            <br> <input type="image" src="images/icn_search.png" title="View Pengelola Anggaran" Onclick="return set_action('assign_lha_pengelola_anggaran', '<?=$arr['lha_pagu_id']?>', '')">
                            <?
                            ?>
                        </td>
                    <?
                    } elseif ($gridHeader [$j] == "Atasan Langsung") {
                        ?>
                        <td align="center">
                            <?
                            echo $params->pic_pagu_count_pejabat ( $arr [$gridDetail [$j]], "", "", "" );
                            ?>
                            <br> <input type="image" src="images/icn_search.png" title="View Pejabat" Onclick="return set_action('assign_lha_pejabat', '<?=$arr['lha_pagu_id']?>', '')">
                            <?
                            ?>
                        </td>
                    <?
                    } else if ($gridHeader [$j] == "Nilai") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Belanja Pegawai") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Belanja Barang") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Belanja Modal") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] );
					?>
		</td>
                        <?
                    } else if ($gridHeader [$j] == "Realisasi Keuangan Pagu") {
                        ?>
                        <td align="left">
	<?
					echo "Rp.".$comfunc->format_uang ( $arr [$gridDetail [$j]] )."<br>"."(".$arr ['lha_pagu_persentase']."%)" ."<br>". "Per Tgl ". $comfunc->dateIndo ( $arr ['lha_pagu_date_per'] );
					?>
		</td>
                        <?
                    }
					else {
                        ?>
                        <td><?=$arr[$gridDetail[$j]]?></td>
                        <?
                    }
                }
                ?>
                <td>
                    <?
                    if ($iconDetail) {
                        ?>
                        <input type="image" src="images/icn_alert_info.png" title="Rincian Data" Onclick="return set_action('getdetail', '<?=$arr[0]?>')">
                        &nbsp;&nbsp;
                        <?
                    }
                    if ($iconEdit) {
                        ?>
                        <input type="image" src="images/icn_edit.png" title="Ubah Data" Onclick="return set_action('getedit', '<?=$arr[0]?>')">
                        &nbsp;&nbsp;
                        <?
                    }
                    if ($iconDel) {
                        ?>
                        <input type="image" src="images/icn_trash.png" title="Hapus Data" Onclick="return set_action('getdelete', '<?=$arr[0]?>', '<?=$arr[1]?>')">
                        <?
                    }
                    if ($method=='backuprestore') {
                        ?>
                        <input type="image" src="images/icn_restore.png" title="Restore Data" Onclick="return set_action('restore_database', '<?=$arr[0]?>', '<?=$comfunc->dateTimeIndo ( $arr [2] )?>')">
                        <?
                    }
                    ?>
                </td>
            </tr>
            <?
        }
    } else {
        ?>
        <td colspan="<?=$jmlHeader+2?>">Tidak Ada Data</td>
        <?
    }
    ?>
</table>
<table width="100%">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="td_paging">
	<?
	$showPage = "";
	$jumPage = ceil ( $recordcount / $num_row );
	if ($noPage > 1)
		echo "<a href='" . $paging_request . "&page=" . ($noPage - 1) . "'> <<d </a>";
	for($page = 1; $page <= $jumPage; $page ++) {
		if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) {
			if (($showPage == 1) && ($page != 2))
				echo "<span class='titik_titik'>...</span>";
			if (($showPage != ($jumPage - 1)) && ($page == $jumPage))
				echo "<span class='titik_titik'>...</span>";
			if ($page == $noPage)
				echo "<span class='paging_aktif'>" . $page . "</span> ";
			else
				echo " <a href='" . $paging_request . "&page=" . $page . "'>" . $page . "</a> ";
			$showPage = $page;
		}
	}
	if ($noPage < $jumPage)
		echo "<a href='" . $paging_request . "&page=" . ($noPage + 1) . "'> > </a>";
	?>
	</td>
    </tr>
</table>

