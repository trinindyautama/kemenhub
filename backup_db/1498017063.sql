<query>DROP TABLE IF EXISTS assignment
<query>CREATE TABLE `assignment` (
  `assign_id` varchar(50) NOT NULL,
  `assign_id_plan` varchar(50) NOT NULL,
  `assign_tipe_id` varchar(50) NOT NULL,
  `assign_no_lha` varchar(50) NOT NULL,
  `assign_date_lha` int(11) NOT NULL,
  `assign_tahun` int(4) NOT NULL,
  `assign_start_date` int(11) NOT NULL,
  `assign_end_date` int(11) NOT NULL,
  `assign_pendanaan` longblob NOT NULL,
  `assign_dasar` longblob NOT NULL,
  `assign_keterangan` longblob,
  `assign_kegiatan` text NOT NULL,
  `assign_periode` varchar(100) NOT NULL,
  `assign_hari` int(3) NOT NULL,
  `assign_hari_persiapan` int(3) NOT NULL DEFAULT '0',
  `assign_hari_pelaksanaan` int(3) NOT NULL DEFAULT '0',
  `assign_hari_pelaporan` int(3) NOT NULL DEFAULT '0',
  `assign_biaya` int(15) NOT NULL,
  `assign_file` varchar(100) DEFAULT NULL,
  `assign_persiapan_awal` int(11) NOT NULL DEFAULT '0',
  `assign_persiapan_akhir` int(11) NOT NULL DEFAULT '0',
  `assign_pelaksanaan_awal` int(11) NOT NULL DEFAULT '0',
  `assign_pelaksanaan_akhir` int(11) NOT NULL DEFAULT '0',
  `assign_pelaporan_awal` int(11) NOT NULL DEFAULT '0',
  `assign_pelaporan_akhir` int(11) NOT NULL DEFAULT '0',
  `assign_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=ajukan, 2=setujui, 3=selesai, 4=tolak',
  `assign_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`assign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment VALUES ('a51852730078d23e3b48bf5972885364a11b17b8', '0813d8ebddbe07fce6236e5d8c251df2f91124f9', '0525307297f9bed8984a8054918087dcf03c4bcf', 'LHA/001/2017', '1495987200', '2017', '1493614800', '1496206800', '', '', '', '<p>tes</p>', 'tes', '-89', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '2', '2017-05-20 20:22:03')
<query>INSERT INTO assignment VALUES ('2039f5600474aed17121d5c58da0e05f57953905', '54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '0525307297f9bed8984a8054918087dcf03c4bcf', '', '0', '2017', '1493568000', '1496160000', '<p>pendanaan tes</p>', '<p>dasar dasar tes</p>', '<p>keterangan tes</p>', 'Kegiatan Audit Kinerja', 'Semester 2 - 2016', '23', '1', '4', '1', '0', '', '0', '0', '0', '0', '0', '0', '2', '2017-05-29 10:42:09')
<query>INSERT INTO assignment VALUES ('ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '953204d3d79e2de0903ea35521570bfaa1f1718d', '6f0349821c12229d4a7a74f1a86292c73fe105a5', '', '0', '2017', '1493568000', '1496160000', '<p>tes pendanaan</p>', '<p>tes dasar</p>', '<p>tes keterangan lain</p>', 'Reviu Lakip', 'Tahun 2016', '23', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '2', '2017-05-29 11:47:14')
<query>DROP TABLE IF EXISTS assignment_auditee
<query>CREATE TABLE `assignment_auditee` (
  `assign_auditee_id_assign` varchar(50) NOT NULL,
  `assign_auditee_id_auditee` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment_auditee VALUES ('a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2')
<query>INSERT INTO assignment_auditee VALUES ('2039f5600474aed17121d5c58da0e05f57953905', '1c8a5553ffb7aa214cca9e10cbe5ecebff1755d7')
<query>INSERT INTO assignment_auditee VALUES ('2039f5600474aed17121d5c58da0e05f57953905', '9cf50648b21dca17b0b45b3b02e1b72cf3280f9c')
<query>INSERT INTO assignment_auditee VALUES ('2039f5600474aed17121d5c58da0e05f57953905', '')
<query>INSERT INTO assignment_auditee VALUES ('ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', 'a9a55b1bc433a1b4128230f7d3b36504f35e3c8a')
<query>INSERT INTO assignment_auditee VALUES ('ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '')
<query>DROP TABLE IF EXISTS assignment_auditor
<query>CREATE TABLE `assignment_auditor` (
  `assign_auditor_id` varchar(50) NOT NULL,
  `assign_auditor_id_assign` varchar(50) NOT NULL,
  `assign_auditor_id_auditee` varchar(50) NOT NULL,
  `assign_auditor_id_auditor` varchar(50) NOT NULL,
  `assign_auditor_cost` varchar(15) NOT NULL,
  `assign_auditor_workday` int(3) NOT NULL DEFAULT '0',
  `assign_auditor_day` int(3) NOT NULL DEFAULT '0',
  `assign_auditor_id_posisi` varchar(50) NOT NULL,
  `assign_auditor_start_date` int(11) NOT NULL,
  `assign_auditor_end_date` int(11) NOT NULL,
  PRIMARY KEY (`assign_auditor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment_auditor VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'a51852730078d23e3b48bf5972885364a11b17b8', '89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', '9ca611db222a22298db1aa06a02564eebc25c3f1', '274780000', '0', '31', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '1493614800', '1496206800')
<query>INSERT INTO assignment_auditor VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'a51852730078d23e3b48bf5972885364a11b17b8', '89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '274780000', '0', '31', '6a70c2a39af30df978a360e556e1102a2a0bdc02', '1493568000', '1496160000')
<query>INSERT INTO assignment_auditor VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', '2039f5600474aed17121d5c58da0e05f57953905', '1c8a5553ffb7aa214cca9e10cbe5ecebff1755d7', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '156609000', '6', '7', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '1493568000', '1494086400')
<query>INSERT INTO assignment_auditor VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', '2039f5600474aed17121d5c58da0e05f57953905', '1c8a5553ffb7aa214cca9e10cbe5ecebff1755d7', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '156609000', '23', '31', '6a70c2a39af30df978a360e556e1102a2a0bdc02', '1493568000', '1496160000')
<query>INSERT INTO assignment_auditor VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', 'a9a55b1bc433a1b4128230f7d3b36504f35e3c8a', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '168549000', '23', '31', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '1493568000', '1496160000')
<query>DROP TABLE IF EXISTS assignment_auditor_detil
<query>CREATE TABLE `assignment_auditor_detil` (
  `anggota_assign_detil_id` varchar(50) NOT NULL,
  `anggota_assign_detil_kode_sbu` varchar(10) NOT NULL,
  `anggota_assign_detil_jml_hari` varchar(5) NOT NULL,
  `anggota_assign_detil_nilai` int(16) NOT NULL,
  `anggota_assign_detil_total` int(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Uang Haria', '30', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Uang Haria', '30', '210000', '6300000')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'us_was', '30', '100000', '3000000')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Uang Haria', '30', '160000', '4800000')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Hotel', '29', '8720000', '252880000')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Transport ', '0', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Taxi', '0', '170000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Uang Repre', '30', '250000', '7500000')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Pesawat Ek', '1', '150000', '150000')
<query>INSERT INTO assignment_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'Pesawat Bi', '1', '150000', '150000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Uang Repre', '30', '250000', '7500000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Taxi', '0', '170000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Transport ', '0', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Hotel', '29', '8720000', '252880000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Uang Haria', '30', '160000', '4800000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'us_was', '30', '100000', '3000000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Uang Haria', '30', '210000', '6300000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Uang Haria', '30', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Pesawat Ek', '1', '150000', '150000')
<query>INSERT INTO assignment_auditor_detil VALUES ('216cfca9cb2caf33caaf0481514bf57975dcccf3', 'Pesawat Bi', '1', '150000', '150000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Repre', '30', '250000', '7500000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Taxi', '0', '80000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Transport ', '0', '340000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Hotel', '29', '4000000', '116000000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Haria', '30', '130000', '3900000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'us_was', '30', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Haria', '30', '170000', '5100000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Haria', '30', '430000', '12900000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Pesawat Ek', '1', '3797000', '3797000')
<query>INSERT INTO assignment_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Pesawat Bi', '1', '7412000', '7412000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Uang Repre', '30', '250000', '7500000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Taxi', '0', '80000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Transport ', '0', '340000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Hotel', '29', '4000000', '116000000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Uang Haria', '30', '130000', '3900000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'us_was', '30', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Uang Haria', '30', '170000', '5100000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Uang Haria', '30', '430000', '12900000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Pesawat Ek', '1', '3797000', '3797000')
<query>INSERT INTO assignment_auditor_detil VALUES ('8c8a11984df453151ab980738dcef5db7cdd299e', 'Pesawat Bi', '1', '7412000', '7412000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Repre', '30', '250000', '7500000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Taxi', '0', '125000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Transport ', '0', '340000', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Hotel', '29', '4680000', '135720000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Haria', '30', '110000', '3300000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'us_was', '30', '0', '0')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Haria', '30', '150000', '4500000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Haria', '30', '380000', '11400000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Pesawat Ek', '1', '2268000', '2268000')
<query>INSERT INTO assignment_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Pesawat Bi', '1', '3861000', '3861000')
<query>DROP TABLE IF EXISTS assignment_comment
<query>CREATE TABLE `assignment_comment` (
  `assign_comment_id` varchar(50) NOT NULL,
  `assign_comment_assign_id` varchar(50) NOT NULL,
  `assign_comment_desc` varchar(255) NOT NULL,
  `assign_comment_user_id` varchar(50) NOT NULL,
  `assign_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`assign_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment_comment VALUES ('1a76796ad3ec53d2f3a88cfe47e6083ab08498f9', 'a51852730078d23e3b48bf5972885364a11b17b8', 'oke', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495288036')
<query>INSERT INTO assignment_comment VALUES ('101e11d9463b8999a27137dbd37e22ed716781d5', 'a51852730078d23e3b48bf5972885364a11b17b8', 'oke', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495288068')
<query>INSERT INTO assignment_comment VALUES ('d486edd337696665987fdd3ce576fa6c689de5dc', '2039f5600474aed17121d5c58da0e05f57953905', 'tes', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029891')
<query>INSERT INTO assignment_comment VALUES ('e22c4ca8dc5f33b49d5b426978a0124e413e34c1', '2039f5600474aed17121d5c58da0e05f57953905', 'setujui', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029908')
<query>INSERT INTO assignment_comment VALUES ('136cff23bdc5fea009ead1e670b933beb29dfb6d', 'ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', 'tes', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129300')
<query>INSERT INTO assignment_comment VALUES ('2b50f0cfbf742f7222d6ccd624100538fd4a7b1b', 'ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', 'tes', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129405')
<query>DROP TABLE IF EXISTS assignment_lha
<query>CREATE TABLE `assignment_lha` (
  `lha_id` varchar(50) NOT NULL,
  `lha_id_assign` varchar(50) NOT NULL,
  `lha_no` varchar(50) NOT NULL,
  `lha_date` int(11) NOT NULL,
  `lha_ringkasan` longblob NOT NULL,
  `lha_metodologi` longblob NOT NULL,
  `lha_sasaran` longblob NOT NULL,
  `lha_ruanglingkup` longblob NOT NULL,
  `lha_batasan` longblob NOT NULL,
  `lha_kegiatan` longblob NOT NULL,
  `lha_informasi` longblob NOT NULL,
  `lha_hasil` longblob NOT NULL,
  `lha_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=ajukan, 2=approve_dalnis, 3=approve_daltu, 4=approve_inspektur, 5=tolak_dalnis, 6=tolak_daltu, 7=tolak_inspektur',
  PRIMARY KEY (`lha_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment_lha VALUES ('c0557f6b9c860c4ceb1f10f6b6adbd4d9cb7ac8d', 'a51852730078d23e3b48bf5972885364a11b17b8', 'LHA/001/2017', '1495987200', '', '', '', '', '', '', '', '', '0')
<query>INSERT INTO assignment_lha VALUES ('63895a45d30a8535484463d4f6457c39003c802a', '2039f5600474aed17121d5c58da0e05f57953905', '', '0', '', '', '', '', '', '', '', '', '0')
<query>INSERT INTO assignment_lha VALUES ('e75e1b678bda3c28f4ccbc596869fa67edb1f350', 'ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '', '0', '', '', '', '', '', '', '', '', '0')
<query>DROP TABLE IF EXISTS assignment_lha_attachment
<query>CREATE TABLE `assignment_lha_attachment` (
  `lha_attach_id` varchar(50) NOT NULL,
  `lha_attach_id_assign` varchar(50) NOT NULL,
  `lha_attach_name` varchar(100) NOT NULL,
  PRIMARY KEY (`lha_attach_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS assignment_lha_comment
<query>CREATE TABLE `assignment_lha_comment` (
  `lha_comment_id` varchar(50) NOT NULL,
  `lha_comment_lha_id` varchar(50) NOT NULL,
  `lha_comment_desc` varchar(255) NOT NULL,
  `lha_comment_user_id` varchar(50) NOT NULL,
  `lha_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`lha_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS assignment_surat_comment
<query>CREATE TABLE `assignment_surat_comment` (
  `surat_comment_id` varchar(50) NOT NULL,
  `surat_comment_surat_id` varchar(50) NOT NULL,
  `surat_comment_desc` varchar(255) NOT NULL,
  `surat_comment_user_id` varchar(50) NOT NULL,
  `surat_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`surat_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS assignment_surat_tugas
<query>CREATE TABLE `assignment_surat_tugas` (
  `assign_surat_id` varchar(50) NOT NULL,
  `assign_surat_id_assign` varchar(50) NOT NULL,
  `assign_surat_no` varchar(50) NOT NULL,
  `assign_surat_tgl` int(11) NOT NULL,
  `assign_surat_jabatanTTD` varchar(50) NOT NULL,
  `assign_surat_tembusan` varchar(100) NOT NULL,
  `assign_surat_id_auditorTTD` varchar(50) NOT NULL,
  `assign_surat_id_userPropose` varchar(50) NOT NULL,
  `assign_surat_tgl_userPropose` int(11) NOT NULL,
  `assign_surat_id_userApprove` varchar(50) NOT NULL,
  `assign_surat_tgl_userApprove` int(11) NOT NULL,
  `assign_surat_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=ajukan, 2=setuju, 3=dikembalikan',
  PRIMARY KEY (`assign_surat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO assignment_surat_tugas VALUES ('a5ba7d503bccd786b4624a08c9a97b26f5362705', 'a51852730078d23e3b48bf5972885364a11b17b8', '001/ST', '1493568000', '', '', 'f349bab5974f65f431e4a7a8facfbec2edfffbde', '', '0', '', '0', '0')
<query>INSERT INTO assignment_surat_tugas VALUES ('1c099860bea4583abe0f70720292618533d391af', 'ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '002', '1493568000', '', 'sasada', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '', '0', '', '0', '0')
<query>INSERT INTO assignment_surat_tugas VALUES ('80f9142dd4053aedb86b3aad4a1e9f1fdfeef695', '2039f5600474aed17121d5c58da0e05f57953905', '003', '1493568000', '', 'asdad, sadsad', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '', '0', '', '0', '0')
<query>DROP TABLE IF EXISTS audit_plan
<query>CREATE TABLE `audit_plan` (
  `audit_plan_id` varchar(50) NOT NULL,
  `audit_plan_code` varchar(20) NOT NULL,
  `audit_plan_tipe_id` varchar(50) NOT NULL,
  `audit_plan_sub_tipe` varchar(50) NOT NULL,
  `audit_plan_tahun` int(4) NOT NULL,
  `audit_plan_start_date` date NOT NULL,
  `audit_plan_end_date` date NOT NULL,
  `audit_plan_kegiatan` text NOT NULL,
  `audit_plan_periode` varchar(100) NOT NULL,
  `audit_plan_hari` int(3) NOT NULL,
  `audit_plan_biaya` int(15) NOT NULL,
  `audit_plan_status` int(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=propose, 2=approve, 3=tolakpengajuan',
  `audit_plan_userID_propose` varchar(50) NOT NULL,
  `audit_plan_userID_approve` varchar(50) NOT NULL,
  `audit_plan_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`audit_plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO audit_plan VALUES ('0813d8ebddbe07fce6236e5d8c251df2f91124f9', '2017_531293', '0525307297f9bed8984a8054918087dcf03c4bcf', '', '2017', '0000-00-00', '0000-00-00', '<p>tes</p>', 'tes', '-89', '0', '2', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '2017-05-20 20:05:31')
<query>INSERT INTO audit_plan VALUES ('54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '2017_537816', '0525307297f9bed8984a8054918087dcf03c4bcf', '', '2017', '0000-00-00', '0000-00-00', 'Kegiatan Audit Kinerja', 'Semester 2 - 2016', '23', '0', '2', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '2017-05-29 10:27:46')
<query>INSERT INTO audit_plan VALUES ('953204d3d79e2de0903ea35521570bfaa1f1718d', '2017_678462', '6f0349821c12229d4a7a74f1a86292c73fe105a5', '', '2017', '0000-00-00', '0000-00-00', 'Reviu Lakip', 'Tahun 2016', '23', '0', '2', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '2017-05-29 11:41:46')
<query>DROP TABLE IF EXISTS audit_plan_auditee
<query>CREATE TABLE `audit_plan_auditee` (
  `audit_plan_auditee_id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_plan_auditee_id_plan` varchar(50) NOT NULL,
  `audit_plan_auditee_id_auditee` varchar(50) NOT NULL,
  `audit_plan_auditee_hari` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`audit_plan_auditee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS audit_plan_auditor
<query>CREATE TABLE `audit_plan_auditor` (
  `plan_auditor_id` varchar(50) NOT NULL,
  `plan_auditor_id_plan` varchar(50) NOT NULL,
  `plan_auditor_id_auditee` varchar(50) NOT NULL,
  `plan_auditor_id_auditor` varchar(50) NOT NULL,
  `plan_auditor_cost` varchar(15) NOT NULL,
  `plan_auditor_workday` int(3) NOT NULL DEFAULT '0',
  `plan_auditor_day` int(3) NOT NULL DEFAULT '0',
  `plan_auditor_id_posisi` varchar(50) NOT NULL,
  `plan_auditor_start_date` int(11) NOT NULL,
  `plan_auditor_end_date` int(11) NOT NULL,
  PRIMARY KEY (`plan_auditor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO audit_plan_auditor VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', '0813d8ebddbe07fce6236e5d8c251df2f91124f9', '89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', 'ad7a0edadec8f623975c0078fa4a292b5b4ab25d', '274780000', '0', '31', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '1493614800', '1496206800')
<query>INSERT INTO audit_plan_auditor VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', '54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '1c8a5553ffb7aa214cca9e10cbe5ecebff1755d7', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '156609000', '6', '7', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '1493568000', '1494086400')
<query>INSERT INTO audit_plan_auditor VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', '953204d3d79e2de0903ea35521570bfaa1f1718d', 'a9a55b1bc433a1b4128230f7d3b36504f35e3c8a', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '168549000', '23', '31', '8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '1493568000', '1496160000')
<query>DROP TABLE IF EXISTS audit_plan_auditor_detil
<query>CREATE TABLE `audit_plan_auditor_detil` (
  `anggota_plan_detil_id` varchar(50) NOT NULL,
  `anggota_plan_detil_nama_sbu` varchar(100) NOT NULL,
  `anggota_plan_detil_jml_hari` varchar(5) NOT NULL,
  `anggota_plan_detil_nilai` int(16) NOT NULL,
  `anggota_plan_detil_total` int(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'rep_a', '30', '250000', '7500000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'taxi', '0', '170000', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'trans_bdr', '0', '0', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'htl_a', '29', '8720000', '252880000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'uh_diklat', '30', '160000', '4800000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'us_was', '30', '100000', '3000000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'uh_8jam', '30', '210000', '6300000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'uh_a', '30', '0', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'pswt_eko', '1', '150000', '150000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('c7aeef66a5264f12c6c4af869d17f48ae6335bee', 'pswt_bis', '1', '150000', '150000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Representasi', '30', '250000', '7500000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Taxi', '0', '80000', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Transport Bandara', '0', '340000', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Hotel', '29', '4000000', '116000000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Harian', '30', '130000', '3900000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'us_was', '30', '0', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Harian', '30', '170000', '5100000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Uang Harian', '30', '430000', '12900000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Pesawat Ekonomi', '1', '3797000', '3797000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('d927a6cefead3b3df74e50aed0720228288184b2', 'Pesawat Bisnis', '1', '7412000', '7412000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Representasi', '30', '250000', '7500000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Taxi', '0', '125000', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Transport Bandara', '0', '340000', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Hotel', '29', '4680000', '135720000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Harian', '30', '110000', '3300000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'us_was', '30', '0', '0')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Harian', '30', '150000', '4500000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Uang Harian', '30', '380000', '11400000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Pesawat Ekonomi', '1', '2268000', '2268000')
<query>INSERT INTO audit_plan_auditor_detil VALUES ('e329c92b1fbb4eb45d4f125278860fd261490f43', 'Pesawat Bisnis', '1', '3861000', '3861000')
<query>DROP TABLE IF EXISTS audit_plan_comment
<query>CREATE TABLE `audit_plan_comment` (
  `plan_comment_id` varchar(50) NOT NULL,
  `plan_comment_plan_id` varchar(50) NOT NULL,
  `plan_comment_user_id` varchar(50) NOT NULL,
  `plan_comment_desc` varchar(255) NOT NULL,
  `plan_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`plan_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO audit_plan_comment VALUES ('0d3f7ee5849a32af77fa37ceb8ef8bef6666fa5b', '0813d8ebddbe07fce6236e5d8c251df2f91124f9', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'bagus', '1495286370')
<query>INSERT INTO audit_plan_comment VALUES ('6978dcd960e431cdc329e9e4f4c1cf6a71021b21', '0813d8ebddbe07fce6236e5d8c251df2f91124f9', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'oke', '1495286523')
<query>INSERT INTO audit_plan_comment VALUES ('42ce3c00cb600eda508d9c7f717b53e78b788b05', '54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'tes', '1496029313')
<query>INSERT INTO audit_plan_comment VALUES ('366270cd333f3b153c582d150f959f42a911d2d2', '54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'tes', '1496029329')
<query>INSERT INTO audit_plan_comment VALUES ('207788b2eaa75710c756447ad4d811c8ef0c119d', '953204d3d79e2de0903ea35521570bfaa1f1718d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'pengajuan', '1496033221')
<query>INSERT INTO audit_plan_comment VALUES ('b5ca2e2fa34fdfe944c3634b6c278a27fc689c2b', '953204d3d79e2de0903ea35521570bfaa1f1718d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'setujui', '1496033234')
<query>DROP TABLE IF EXISTS auditee
<query>CREATE TABLE `auditee` (
  `auditee_id` varchar(50) NOT NULL,
  `auditee_kode` varchar(10) NOT NULL,
  `auditee_name` varchar(100) NOT NULL,
  `auditee_parrent_id` varchar(50) NOT NULL DEFAULT '0',
  `auditee_inspektorat_id` varchar(50) NOT NULL,
  `auditee_propinsi_id` varchar(50) NOT NULL,
  `auditee_kabupaten_id` varchar(50) NOT NULL,
  `auditee_alamat` varchar(500) NOT NULL,
  `auditee_telp` varchar(15) NOT NULL,
  `auditee_ext` varchar(10) NOT NULL,
  `auditee_fax` varchar(15) NOT NULL,
  `auditee_email` varchar(100) DEFAULT NULL,
  `auditee_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`auditee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO auditee VALUES ('89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', '001', 'Tes Auditee', '0', '', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '4e99d9b15a57cb77bfe5701803b57dd3ca46885e', '', '', '', '', '', '0')
<query>INSERT INTO auditee VALUES ('56d04e0f55b7d9a074ee4ac3b81353f59b8ac72a', '2000', 'SEKRETARIAT UTAMA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5c730b7e75cf5af124b8cdae7adfbda7af9d8d7e', '2100', 'BIRO PERENCANAAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3b6f134d4fbccfcb90d76fd8db41c6f9af89f6f2', '2110', 'BAGIAN RENCANA DAN TARIF', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f130d8ec22ea3af82efe4f5d274c05b439532e07', '2120', 'BAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d5ba18c164549dd0a47fc4f53cfe5cf97a0afb48', '2130', 'BAGIAN PEMANTAUAN DAN EVALUASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', '2200', 'BIRO HUKUM DAN ORGANISASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2b37a62a00270ee5c56cb5e6aed7fc6ee659f4ce', '2210', 'BAGIAN PERATURAN PERUNDANG-UNDANGAN DAN PERTIMBANGAN HUKUM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9cb59b06e8139a79d90a8969db0e912fcafa8c11', '2220', 'BAGIAN KERJASAMA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c79b2c38e4a71d465ba3004ebb14966daedf14cb', '2230', 'BAGIAN ORGANISASI DAN TATALAKSANA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9ab2d5426492ad03b3796bd1e9222eadc66fc1f2', '2240', 'BAGIAN HUBUNGAN MASYARAKAT', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d418fd152bfdeed4eefcee87cda50bd6c086ef91', '2300', 'BIRO UMUM DAN SUMBER DAYA MANUSIA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ff7bf3262bec2b88379e03fbd9f16bccd61d514c', '2310', 'BAGIAN SUMBER DAYA MANUSIA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c08ac92e7adeae8163a6d9e57452fdf46f35737f', '2320', 'BAGIAN KEUANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f90af7566497423d2bae16253465d4c14e419989', '2330', 'BAGIAN PERLENGKAPAN DAN BARANG MILIK NEGARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8d1642defa775904130fc349ab9f50947cf89deb', '2340', 'BAGIAN TATA USAHA DAN PROTOKOL', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('db85aeac4b42dfb1824f64dc6cf30ce6be0ab8ff', '2111', 'SUBBAGIAN RENCANA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8d3aaf79b2f93d69ab1e03e660f2b44354b76daa', '2112', 'SUBBAGIAN TARIF', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('87439eae10a330f44fed10e4a2c31d3de11c6f8c', '2113', 'SUBBAGIAN PINJAMAN/HIBAH LUAR NEGERI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b934060ad3da7246fec29f5003bdf0362703c3d8', '2121', 'SUBBAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN I', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d089b5098ad1c540cf237d8ef32995942b066695', '2122', 'SUBBAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('010b29786694ae4a48163f0618e27d9ff8859ab8', '2123', 'SUBBAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN III', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9cabb164a9375642e01bc72ea1af037d2d07e4b8', '2131', 'SUBBAGIAN PEMANTAUAN DAN EVALUASI I', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('15d8543e3d92aa065268b5563da082b68440d3f6', '2132', 'SUBBAGIAN PEMANTAUAN DAN EVALUASI II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('09758f8f60e88e8c8ef4e520a14cc21a9d1898cb', '2133', 'SUBBAGIAN PEMANTAUAN DAN EVALUASI III', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('464ece92eed468c8a005e2cf369d4f8ca2c43319', '2211', 'SUBBAGIAN PERATURAN PERUNDANG-UNDANGAN I', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5e6cafd32538b7c1f26fad3e708f30d532936812', '2212', 'SUBBAGIAN PERATURAN PERUNDANG-UNDANGAN II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6840d9fb287b8a2057fb491aa0f9d60e30fee443', '2213', 'SUBBAGIAN PERTIMBANGAN DAN INFORMASI HUKUM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c8efc7d02eab7e07fb44c3a0066b4742ce4212b4', '2221', 'SUBBAGIAN KERJASAMA LUAR NEGERI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('aedf70b79ad7d86a54d89fcf316384fa0dd4c7fc', '2222', 'SUBBAGIAN KERJASAMA DALAM NEGERI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fd9d6b1cd1cf65abcea342ed35896b6810a5cec5', '2231', 'SUBBAGIAN ORGANISASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('01f3768eb3c6abed4d81603af8e965e3ad6194e1', '2232', 'SUBBAGIAN TATA LAKSANA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('469c0f04fb712c7ea6f2891a7ef4dcd2595b1344', '2241', 'SUBBAGIAN PUBLIKASI DAN DOKUMENTASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7d8180dceb928fa7fdc11afc04ff580550e4a689', '2242', 'SUBBAGIAN HUBUNGAN PERS DAN MEDIA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2dec62a03a142f306b7147f08fe1abe767f49d3d', '2311', 'SUBBAGIAN PERENCANAAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ba3b7c17e5a8b3c2393e2f8ac9bf7f0225a19de5', '2312', 'SUBBAGIAN MANAJEMEN DAN EVALUASI KINERJA DAN KESEJAHTERAAN SUMBER DAYA MANUSIA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9a8824da4b69d7a99e9059f360480ff05fc8d1c0', '2313', 'SUBBAGIAN MUTASI DAN PENGELOLAAN JABATAN FUNGSIONAL SUMBER DAYA MANUSIA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('77d559784d63d854b73498766a621f2b0fb4fcc2', '2321', 'SUBBAGIAN PERBENDAHARAAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('182a5585d136754655c37564447a4e49573f05c7', '2322', 'SUBBAGIAN GAJI DAN PENERIMAAN NEGARA BUKAN PAJAK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('95b2f20172246fe3084f9ee001ad12bd93f1f52d', '2323', 'SUBBAGIAN AKUNTANSI DAN PELAPORAN KEUANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5eae60d489dc5b67a2664c47ebbb7f2df950fcc6', '2331', 'SUBBAGIAN PENGADAAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('404eb5ce20f64ebe85d7d565645ba26d4caa513e', '2332', 'SUBBAGIAN PENGELOLAAN BARANG MILIK NEGARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('723c7d892c059b2305eb204c8a3525bf77b54ad9', '2333', 'SUBBAGIAN PEMELIHARAAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('bcb0c9f1dd2a3dc11fbaf0e56ee928501881a92e', '2341', 'SUBBAGIAN PERSURATAN DAN ARSIP', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('dca3212727138bdae16d061cdc5efa95843ace3c', '2342', 'SUBBAGIAN RUMAH TANGGA DAN PROTOKOL', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('58f2f1ecb818b17d8267b29b5ddc0725f7f8e896', '2343', 'UNIT TATA USAHA PIMPINAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6784c13c54ea627bf5e793a3989fb76298d0b908', '3000', 'DEPUTI SIDANG METEOROLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('64b39728f1120949c38b235988d7d80dc23e844e', '3100', 'PUSAT METEOROLOGI PENERBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d9171a58ed24f2b9660ca5f70b8e97f2b9e38ddb', '3110', 'BIDANG MANAJEMEN OBSERVASI METEOROLOGI PENERBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d6f9165152b97ac15fd3cbbfb7e49d0ce741a34b', '3120', 'BIDANG MANAJEMEN OPERASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f5668483efcf394b3f050cf57bbc6c976764c53e', '3130', 'BIDANG INFORMASI METEOROLOGI PENERBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('cb0d8da839e2340fad7326d021b20468f9a1ac05', '3200', 'PUSAT METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('06d3e3bfa7cb594a7a53e01885a888aab9d9ec57', '3210', 'BIDANG MANAJEMEN METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('35ce1d6350b4f0ab9b7ea95ca134a1f5653954f6', '3220', 'BIDANG INFORMASI METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('405c8393c0b05909fd170cee62b67e9ae19298a9', '3300', 'PUSAT METEOROLOGI PUBLIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0c2556def48fd2712282dc72226ada111cf718cf', '3320', 'BIDANG LAYANAN INFORMASI CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fd6a50c9bdb65025a3724ce8a034bdef74d15e5b', '3330', 'BIDANG PREDIKSI DAN PERINGATAN DINI CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ef7e35a083eefd2663ddf9ee17217e2c411aec26', '3340', 'BIDANG PENGELOLAAN CITRA INDERAJA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b220e633660fc71ef3118ee1e95d49dfad99923b', '3111', 'SUBBIDANG MANAJEMEN OBSERVASI METEOROLOGI PERMUKAAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1e3ecbd4925f41b0548b3df1a6d561169e461e54', '3112', 'SUBBIDANG MANAJEMEN OBSERVASI UDARA ATAS', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('be2c44cdc110f4228633bc085ba91e0f2e8e3d96', '3121', 'SUBBIDANG MANAJEMEN OPERASI METEOROLOGI PENERBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a6bd5ad20836bb354face0fdb0fc353ab6ca3911', '3122', 'SUBBIDANG MANAJEMEN OPERASI METEOROLOGI PUBLIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e58c033456cae9382fb23bb4eceee30ba0823b03', '3131', 'SUBBIDANG LAYANAN INFORMASI METEOROLOGI PENERBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e84cde1b90019080396ac4155ed4cff38dcaf3d8', '3132', 'SUBBIDANG DISEMINASI INFORMASI METEOROLOGI PENERBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('381e9ba16ab92ac874216b1b346466da7864cfdb', '3211', 'SUBBIDANG MANAJEMEN OBSERVASI METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fed00a05083d90973602eea205841ae5845c0ae6', '3212', 'SUBBIDANG MANAJEMEN OPERASI METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('030ca3aae185de53a13877eb608ebdd3b1f824d1', '3221', 'SUBBIDANG ANALISIS DAN PREDIKSI METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('aeeefa12f188043e6987e0dbc7a23b66c4de9f47', '3222', 'SUBBIDANG LAYANAN INFORMASI METEOROLOGI MARITIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7b679e9ab7a562156be54472fcac29ed8ba0ebbd', '3311', 'SUBBIDANG PRODUKSI INFORMASI CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f11905cdfd0e7d45af520a197aa63577f13cd7fa', '3312', 'SUBBIDANG DISEMINASI INFORMASI CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f46fd5ae4e48226906c7719204022d2bf39f430c', '3321', 'SUBBIDANG PREDIKSI CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c19e3f42689ad662ae54a1d2017101969a8fe6c2', '3322', 'SUBBIDANG PERINGATAN CUACA DINI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3c085078197cebfd7ebf520965b671ba5dafce39', '3331', 'SUBBIDANG PENGELOLAAN CITRA RADAR CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('409992c2463bd0dc67975a71fab1d05a17360b61', '3332', 'SUBBIDANG PENGELOLAAN CITRA SATELIT CUACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('16e0d7f343ee4e093addb40411fab99f946c080d', '4000', 'DEPUTI BIDANG KLIMATOLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('daac8ce8e9843cd20856065be39c60528903eba7', '4100', 'PUSAT INFORMASI PERUBAHAN IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('301c10175fbec82fb1cf9d76ac98e7e915e13608', '4110', 'BIDANG ANALISIS PERUBAHAN IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4680932c407cba677a893fadbf1f7ac0bd473533', '4120', 'BIDANG ANALISIS VARIABILITAS IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b78d548474c8bc8d1bc7cdcff2afee8f93e6eec8', '4130', 'BIDANG MANAJEMEN OPERASI IKLIM DAN KUALITAS UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8df5e752834ce5ab8c9027f8417349f68e8b9912', '4200', 'PUSAT LAYANAN INFORMASI IKLIM TERAPAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('399cf15c836ab5d6edc55586d60ff8f2a3f5df58', '4210', 'BIDANG INFORMASI IKLIM TERAPAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0d40688ff212bdc7ea11a1d2c8ff54c05cdc258a', '4220', 'BIDANG INFORMASI KUALITAS UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('570d8a4cf5e8f4b16bbf2539855c2e9bab1c4d18', '4230', 'BIDANG DISEMINASI INFORMASI IKLIM DAN KUALITAS UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('784a0cf8c5f4b4a2c44b2b8e4980046361715e4c', '4111', 'SUBBIDANG ANALISIS DAN PROYEKSI PERUBAHAN IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('03a411151d444d74bc2821e19548bc8f7ff053d2', '4112', 'SUBBIDANG ANALISIS KOMPOSISI KIMIA ATMOSFER', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('20251402fa68d4cf1f92a76251ac1ad3bacd191a', '4121', 'SUBBIDANG ANALISIS DAN INFORMASI IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9704f50ef1caa34186608d7d31b5e131d2cb6e51', '4122', 'SUBBIDANG PERINGATAN DINI IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2b958dca98e7e332df535368b35f950ecebc80c3', '4131', 'SUBBIDANG MANAJEMEN OPERASI IKLIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f1f9fbfdbbf0bc521adb18c5b64f77646cd8976e', '4132', 'SUBBIDANG MANAJEMEN OPERASI KUALITAS UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('58d543bef203e4029efdbf44e6822706ddbae609', '4211', 'SUBBIDANG INFORMASI IKLIM LINGKUNGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('179ebe2e40ff38d879e97db804405917df85d62a', '4212', 'SUBBIDANG INFORMASI IKLIM INFRASTRUKTUR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f0b3b920b90a8c9899e7f9abbfe9715fb6f8214e', '4221', 'SUBBIDANG INFORMASI GAS RUMAH KACA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('eb4c5b84e259720a0bb901484d2452c74c7f2404', '4222', 'SUBBIDANG INFORMASI PENCEMARAN UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('934ef4ae63a8b0290a4d88b718cfa66348dc6a1f', '4231', 'SUBBIDANG PRODUKSI INFORMASI IKLIM DAN KUALITAS UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('207482be6a4898caeb0e5d7f4daf1a94c8f2a231', '4232', 'SUBBIDANG SISTEM INFORMASI IKLIM DAN KUALITAS UDARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3eebc46db418e897bb81454e6d5c69eb16e76ccc', '5000', 'DEPUTI BIDANG GEOFISIKA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('58dad9abfc30a39357cc322192b459a3c4adfd55', '5100', 'PUSAT GEMPA BUMI DAN TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7b7e7193a65f9fb91b9b209f4c49004d117530ed', '5110', 'BIDANG INFORMASI GEMPA BUMI DAN PERINGATAN DINI TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b735c8e8894a792ca50c2889b7b0fe34837afe51', '5120', 'BIDANG MITIGASI GEMPA BUMI DAN TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4b634553a41ec3134de446b76a59c6d39fdac9fb', '5130', 'BIDANG MANAJEMEN  OPERASI GEMPA BUMI DAN TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f991fdf71c3473088aa46af8b069c18cd2b9eb92', '5200', 'BIDANG SEISMOLOGI TEKNIK, GEOFISIKA POTENSIAL DAN TANDA WAKTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b010aba7ddf91edac3bebe86ac977d07cc8c594a', '5210', 'BIDANG SEISMOLOGI TEKNIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('bb463e5639737eee95b643cfb057d97d23851a3a', '5220', 'BIDANG GEOFISIKA POTENSIAL DAN TANDA WAKTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9573e202b1e1bd8375c47302e64bf9f8ebb29680', '5230', 'BIDANG MANAJEMEN OPERASI SEISMOLOGI TEKNIK, GEOFISIKA POTENSIAL DAN TANDA WAKTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('891bfbb19f13f97009e6e3bbb3d2162ba17d19e3', '5111', 'SUBBIDANG INFORMASI GEMPA BUMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('36d1d5711acaf83c43b6b11d878a68bcd03792ee', '5112', 'SUBBIDANG PERINGATAN DINI TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5e169887b6485c341debe8cddf8c157446237d1d', '5121', 'SUBBIDANG MITIGASI GEMPA BUMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('50946653e36dd5243e95b9933d093ef5e0542b8f', '5122', 'SUBBIDANG MITIGASI TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('99f772851f1f226d0f51f604e2b314602da8a03f', '5131', 'SUBBIDANG MANAJEMEN OPERASI GEMPA BUMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('964d508d675455b8e73864e964559629d294e529', '5132', 'SUBBIDANG MANAJEMEN OPERASI TSUNAMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e7151799284e5f428458d99c57f91650c397180b', '5211', 'SUBBIDANG ANALISIS SEISMOLOGI TEKNIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('234eefd0a463fe610b87cd5d4e93295c9ad3d4f4', '5212', 'SUBBIDANG LAYANAN INFORMASI SEISMOLOGI TEKNIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f651edee6b347b8c6c43b7e6f9b79346a243b65a', '5221', 'SUBBIDANG ANALISIS GEOFISIKA POTENSIAL DAN TANDA WAKTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7726b02bf93915fb6b37cf2ea9aadfabe90f9436', '5222', 'SUBBIDANG LAYANAN INFORMASI GEOFISIKA POTENSIAL DAN TANDA WAKTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a6c93b0605af11b4b219eb0d138e57832b668e8a', '5231', 'SUBBIDANG MANAJEMEN OPERASI SEISMOLOGI TEKNIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e0c69ccdbf50fb6856a61c9b167cef37776c1aad', '5232', 'SUBBIDANG MANAJEMEN OPERASI GEOFISIKA POTENSIAL DAN TANDA WAKTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ca00c6b7157eac23277c411db1bb6a51ee93c49a', '6000', 'DEPUTI BIDANG INSTRUMENTASI, KALIBRASI, REKAYASA, DAN JARINGAN KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('09797cae499e0b0a3e25e1afc5aec94ca4a67253', '6100', 'PUSAT INSTRUMENTASI, KALIBRASI, DAN REKAYASA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b91b2fe429c568ab3387b899ff0ea4909936a08a', '6110', 'BIDANG INSTRUMENTASI, KALIBRASI, DAN REKAYASA, PERALATAN METEOROLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('971ab85e1b243bbbd97f6c89914c0f0544735688', '6120', 'BIDANG INSTRUMENTASI, KALIBRASI, DAN REKAYASA PERALATAN KLIMATOLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a86d544038ea986947b40fc3aac761a9e0ec94de', '6130', 'BIDANG INSTRUMENTASI, KALIBRASI, DAN REKAYASA PERALATAN GEOFISIKA', '09797cae499e0b0a3e25e1afc5aec94ca4a67253', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9cc573f4467ea3d3012022afd6e40b78cb523c24', '6200', 'PUSAT DATABASE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7bd19500b190ff02fb053cbc77cbfc75fc325497', '6210', 'BIDANG MANAJEMEN DATABASE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('355cb27ae4edc858c0e623da2642cfac0229b4cc', '6220', 'BIDANG PENGEMBANGAN DATABASE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('91f43779eb78a87715174ed39ffa091e996cdb2f', '6230', 'BIDANG PEMELIHARAAN DATABASE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('caf5949295ed9d0a4185275f32b13e7c5bc2965e', '6300', 'PUSAT JARINGAN KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8da2b9f6ed5e7bafc1b0f1d259ed38bf0f836194', '6310', 'BIDANG OPERASIONAL JARINGAN KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a9ede7084eff8ca6bc0291c478aaa9a942ed289f', '6320', 'BIDANG PENGEMBANGAN JARINGAN KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a9dd1038fd24ab7b31478e26fe22a0d107c9d76a', '6330', 'BIDANG MANAJEMEN JARINGAN KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b14d17c6476e13d8158fadcb9ff83302561d3dc6', '6111', 'SUBBIDANG INSTRUMENTASI DAN REKAYASA PERALATAN METEOROLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('10a1d231299e6d4c4d38aafd044e1570ae009f44', '6112', 'SUBBIDANG KALIBRASI PERALATAN METEOROLOGI', '0', '', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b7933bd6cc9c952fbe36a3a9ed0b8750585ceaee', '6121', 'SUBBIDANG INSTRUMENTASI DAN REKAYASA PERALATAN KLIMATOLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('13c6e913e37a25e8a891fa02e8e803a1df4b15cc', '6122', 'SUBBIDANG KALIBRASI PERALATAN KLIMATOLOGI', '0', '', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a76df8ffc8485d4ca3c40086b48b081cb98608a4', '6131', 'SUBBIDANG INSTRUMENTASI DAN REKAYASA PERALATAN GEOFISIKA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('99b4b851f0f1f0ff286a3aa1909abc145c7b4fb7', '6132', 'SUBBIDANG KALIBRASI PERALATAN GEOFISIKA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fb1dad6d4bc9509ff2a9c0948339194972dc4c6c', '6211', 'SUBBIDANG MANAJEMEN DATABASE MKG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('872e28b0b4ff7ccc060af1eb3b68c82736348846', '6212', 'SUBBIDANG MANAJEMEN DATABASE UMUM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3a8b45970457a8eed6603d020e6881f651b903e5', '6221', 'SUBBIDANG PENGEMBANGAN DATABASE MKG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('937b94b0555e5906da28033dc0c8bb3da8bca2be', '6222', 'SUBBIDANG PENGEMBANGAN DATABASE UMUM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6cb2e510a544bd5150629c04496ddf0afa373ae6', '6231', 'SUBBIDANG PEMELIHARAAN DATABASE MKG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('55e0b2abe2b62c8e2d5d492174f39a480845780a', '6232', 'SUBBIDANG PEMELIHARAAN DATABASE UMUM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a9f63ae95970813bc3af067373dd19f05b5ef500', '6311', 'SUBBIDANG OPERASIONAL TEKNOLOGI KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1121d1e9ba02db4a9b8e004b8d48d96213e7c4f3', '6312', 'SUBBIDANG OPERASIONAL TEKNOLOGI INFORMASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('681b578e75e1809db119f91f9d4f0d66033b7321', '6321', 'SUBBIDANG PENGEMBANGAN TEKNOLOGI KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ffcb86eae643c160f52005694d3ff7b9eb1cb18b', '6322', 'SUBBIDANG PENGEMBANGAN TEKNOLOGI INFORMASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a13942beb6d08721d5d6ddebd6e5200cde47e932', '6331', 'SUBBIDANG MANAJEMEN TEKNOLOGI KOMUNIKASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b5b8afbe19921cfc280012d171fc5674469e8d83', '6332', 'SUBBIDANG MANAJEMEN TEKNOLOGI INFORMASI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('64f8c68205795e44b0312934d283e55ef33e869e', '7000', 'INSPEKTORAT', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a6cf86eee1eb98387deb177526c077fc2b0aa98e', '7100', 'PUSAT PENELITIAN DAN PENGEMBANGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6d91ac0a361cb102873d39ad3c7a21a6fa1634af', '7110', 'SUBBAGIAN TATA USAHA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('53510035965323c6dd6907ee8bc15649655776be', '7120', 'BAGIAN PENELITIAN DAN PENGEMBANGAN METEOROLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c6208dac7e29146636fcd9325ef7a1795b3ae4f6', '7130', 'BIDANG PENELITIAN DAN PENGEMBANGAN KLIMATOLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('747ad09de5a49306683d7b73798dc5ccc9ac0b48', '7140', 'BIDANG PENELITIAN DAN PENGEMBANGAN GEOFISIKA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('303eb55cb225c1187aa0eeafc18444bd76a1e0c9', '7200', 'PUSAT PENDIDIKAN DAN PELATIHAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('20db4de976e5c935d52c453c7280de91e5723547', '7220', 'BIDANG PERENCANAAN, PENGEMBANGAN, DAN PENJAMINAN MUTU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d28b2198fb0fb47f0aaa6242628ccf06d5339d17', '7210', 'BAGIAN TATA USAHA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('bd4dc2d2e2171875b97428045c9120d69bd00dbe', '7230', 'BAGIAN PENYELENGGARAAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5c27ea3ba7e6e586f8ad933636d72b38a0352926', '9100', 'BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH I', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'eb310cbe50c6b2676eb9a37a1f73aeadb9f17d93', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('be097c32eebc0fe4f185e109f60241e02236eb02', '9200', 'BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'a5a1f057a486d998adcf7229c1e3af0c88383d51', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5c8ba13288540c714750ff9e130aad4453866332', '9300', 'BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH III', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '228bec8dd9fec439a87f3069dcc0c31f3a259660', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b1c2ee683178b5a469d1afbc31d723e64f538d19', '9400', 'BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH IV', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'fc8041b5359101d7741e6549e2fdf6e93c731cca', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c6d259cb746b9cafae5a4ef5ef6312328dabb0e0', '9500', 'BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH V', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '3e189576afe30da8c39f0ea3e6fb3f5627c8ab74', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('03b3980371bfbe3ef823da11f3162998de3cf0e6', 'I/01', 'STASIUN METEOROLOGI KUALANAMU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'a5a0aa863b2534f46fa2013a204eaf06ba7aeaba', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ddf44b08349ee894d3fed01c21b796e5dde53b2b', '9102', 'STASIUN METEOROLOGI HANG NADIM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('38e0a9aaaf0ccc48bbc5383b92573cd66a8e7cc0', '9103', 'STASIUN METEOROLOGI SULTAN ISKANDAR MUDA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '7ab23ed9061e5235537a87e813f427f5bc641a37', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4c1b5e2442df7685848ec6a92b99199752c0ff69', '9104', 'STASIUN METEOROLOGI SULTAN SYARIF KASIM II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5989bda524469846bb95c2ae6aad5fb4df5fb519', '9105', 'STASIUN METEOROLOGI MINANGKABAU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '56d0a347fcfeeeaa9849cb42e99856c7177e41c0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9eb765ec2d6d5d7c2d42349e275d331245eedcb9', '9106', 'STASIUN METEOROLOGI MARITIM BELAWAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'eb310cbe50c6b2676eb9a37a1f73aeadb9f17d93', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('302978251ef72cd5ebc8467b2beb73615b1b5e09', '9107', 'STASIUN METEOROLOGI MALIKUSSALEH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '0c03e4b8ee8c2b450d043c082778510a8c6893cc', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8efb1389718ce5eb49a689fbd9681a8080eba6eb', '9108', 'STASIUN METEOROLOGI TJUT NYAK DIEN MEULABOH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5846292803f8f043265f65f112038349040d8302', '9109', 'STASIUN METEOROLOGI CUT BAU MAIMUN SALEH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'a90381c2ac2fbaabefb2abf18d2cd5e1c141d971', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e4197f075ccf90bcf635d546881dd12f7cbc007c', '9110', 'STASIUN METEOROLOGI JAPURA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '6f0c83b8eb8a17d30e93ee56013f03ab0b6b1e5b', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7bb966d907124fd47adb65a6805f9a5a766659fd', '9111', 'STASIUN METEOROLOGI TAREMPA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3cc5a1b742dd053283179e468201df72f0f19064', '9112', 'STASIUN METEOROLOGI DABO SINGKEP', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ecc6c0b9c46b93ed2608f44c7d85195004e7ced9', '9113', 'STASIUN METEOROLOGI RANAI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c40f80349aecb991f2c5839a2e2b546a587879bd', '9114', 'STASIUN METEOROLOGI KIJANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('113dab4e46b9404645733dfdacfab62ed728ca12', '9115', 'STASIUN METEOROLOGI BINAKA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '41f91146873f89e7cac416fe3b5a433436c8cc05', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('02721b708424ef85c296bb5ee1a8de5714cae1ac', '9116', 'STASIUN METEOROLOGI F.L TOBING', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '41f91146873f89e7cac416fe3b5a433436c8cc05', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('73ef0a80f4be34a58c52e26308ff10cb6856aeee', '9117', 'STASIUN METEOROLOGI MARITIM TELUK BAYUR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '6739a66ce91850bae5f1baf4a6ad7ae1f6075f26', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('773e7959d5497aca4f9737051fd29f60e2ed7af1', '9118', 'STASIUN METEOROLOGI AEK GODANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ad910b6b39b0e56d3ad9d7c7d60763a172795b93', '9119', 'STASIUN METEOROLOGI RAJA HAJI ABDULLAH TANJUNG BALAI KARIMUN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0385538efd8abc045dce709b5944c539e2a10138', '9120', 'STASIUN KLIMATOLOGI DELI SERDANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2aab9fd4fe2416310d7b3b4dc7a48ca160b8b4b6', '9121', 'STASIUN KLIMATOLOGI PADANG PARIMANAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '56d0a347fcfeeeaa9849cb42e99856c7177e41c0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fa1e019dd5d7eb6f54091d2e98d3432194753cfa', '9122', 'STASIUN KLIMATOLOGI ACEH BESAR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '63add612f7390a2666b9732fcda23cadb1b3f1b6', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('85d2e28432e91593acb499b27c3c8f6075be5776', '9124', 'STASIUN GEOFISIKA TUNTUNGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'eb310cbe50c6b2676eb9a37a1f73aeadb9f17d93', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('60f75babfe238a5092cb4beba1e276192004fb77', '9123', 'STASIUN KLIMATOLOGI TAMBANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0ac1e8bce0528ff9a142906313e31e22bc6cd816', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('80758fc5bfc2916b72f1244e576b3a8451fb3f9e', '9125', 'STASIUN GEOFISIKA SILAING BAWAH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1d3236d3cb72319cf095915abf83093d74c86b23', '9126', 'STASIUN GEOFISIKA MATAIE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f88bc80292f1d4b420a58056ef305a893482ea84', '9127', 'STASIUN GEOFISIKA TAPAK TUAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d7596c34bcc3d6545c35579cadb04995483affb5', '9128', 'STASIUN GEOFISIKA GUNUNG SITOLI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('327f17fdf8ef381f343a7081f5b2369353aca7ea', '9201', 'STASIUN METEOROLOGI SOEKARNO HATTA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '828f85f9b2301a70d3298e044fc1e35349733622', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('77b161b149594a1ffc927bddec3d788b7d90deb5', '9202', 'STASIUN METEOROLOGI SERANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '68b0618a981c3ebd3fabcdac99c0d2341611d9de', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6f323e930618a35fa113d35c3ab5aa674c0d74b8', '9203', 'STASIUN METEOROLOGI MARITIM TANJUNG PRIOK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '4e99d9b15a57cb77bfe5701803b57dd3ca46885e', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7c045f084d4948cf08442a07e11c7f70f604cddb', '9204', 'STASIUN METEOROLOGI RADEN INTEN II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '7c68e582ef9cc3f1c4572c5d6acbcc5f8b150e79', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2ad99129b7ab75bbbce119f4ab03f9284bbc184a', '9205', 'STASIUN METEOROLOGI SUPADIO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', '1aabb3512a79bc3c047c4fb032126884bd1f88d1', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b1b90f3ad5105610aa8838632d5236e27d222c66', '9206', 'STASIUN METEOROLOGI DEPATI AMIR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '4fb2ead5ebcf2b51db8348c0628969ee6e825901', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b043aef5e6cc7dc4149833f7bf4484ad7a8690c4', '9207', 'STASIUN METEOROLOGI SULTAN THAHA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '675eed8a709d4098541fb8f73805004c84725d49', '79671e078b4c0473cb3f218342dd22ae08e06b28', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a9a55b1bc433a1b4128230f7d3b36504f35e3c8a', '9208', 'STASIUN METEOROLOGI SULTAN MAHMUD BADARUDDIN II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '76d291d5d0d9498f26b37cea62e6cccd8ab2b7ca', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('036ad763c2d64f4c607d28c7b6f561d719278d36', '9209', 'STASIUN METEOROLOGI MARITIM TANJUNG MAS', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '42c6242dd929b77419907065fe0e7c127948bacd', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c42ba2b2873b4ee0dca9aa85e3de7dde439dac2a', '9210', 'STASIUN METEOROLOGI AHMAD YANI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '42c6242dd929b77419907065fe0e7c127948bacd', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4fef04e919ba8767b20e0850c3a689e2ebc5d834', '9211', 'STASIUN METEOROLOGI FATMAWATI SEOKARNO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'd8c82bf22dd5b689e806f6e381dd3ad459e7485c', '4f7673cbcd56748146807f844b656af9644d40bf', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('cefb8672afd220bade80f51f1605945519c1119d', '9212', 'STASIUN METEOROLOGI BUDIARTO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '828f85f9b2301a70d3298e044fc1e35349733622', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('260e0b2ec8b1540ea28e974cfb0143caf6afc184', '9213', 'STASIUN METEOROLOGI H. ASAN HANANJOEDIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '47c9fcbddcc4de7acf7085ef81a4579c62232baf', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('daa6c4cc61c5732bcaf78477e439c9640ca84785', '9214', 'STASIUN METEOROLOGI DEPATI PARBO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '675eed8a709d4098541fb8f73805004c84725d49', '547a3a6ad51a53ed644507c819791eeefb00b498', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('81a3caa51b63c4ebafede707bda0a8600796d816', '9215', 'STASIUN METEOROLOGI TEGAL', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '1b660f43cb2cdfafab5443eb46d709848dcf3f1f', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7d418c03851195ab4fb76ec821ce8ec7daceaa4b', '9216', 'STASIUN METEOROLOGI CILACAP', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '5aba4fb223dae55105d598b2836d680da75246a4', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('91b663c6153f1b6b8dcf2961c011ec5fffb3a47a', '9217', 'STASIUN METEOROLOGI KEMAYORAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '202ecf2aae824cba4b710850e783d708ba62b795', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1cf0103ee1fbc482f0acde1e11844b54f3ce50e9', '9218', 'STASIUN METEOROLOGI PALOH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', 'a117a252776ab0f76fa56030f9daae43461e9149', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1be41bde3de5cc7518e7b99467cd8399b4a19432', '9219', 'STASIUN METEOROLOGI RAHARDI OESMAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', 'fffc00bc924f84fc043aa10c373bdc6211ad8697', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b820963bdfe66dd03368dc41cf626ddee18e4d61', '9220', 'STASIUN METEOROLOGI SUSILO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fcc73e34149e5557657060f44320c7ae9a3e29c0', '9221', 'STASIUN METEOROLOGI NANGAPINOH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2cfa5c683e3d3ddbc85b3ccafa7aa714563987a3', '9222', 'STASIUN METEOROLOGI PANGSUMA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', '925a311b964d3f99e5567ac71c2ecd4622a11010', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c4dd603b81de7d491e65b89c610fd6013548a371', '9223', 'STASIUN METEOROLOGI JATIWANGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '0b486467ca176631589de72c19779fe674f13388', '311d4c6d5b3651a15bc6aa6f9b88555eba5ab5f4', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('da9779cc2096d525aae6232e49fa6fcca660aca0', '9224', 'STASIUN METEOROLOGI CITEKO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '0b486467ca176631589de72c19779fe674f13388', '5f682328b4887d57cdfe40d7b1358150a6102b74', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4088f9c931057757ff6dd58dc6035e1dd220499b', '9225', 'STASIUN METEOROLOGI MARITIM PONTIANAK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', '1aabb3512a79bc3c047c4fb032126884bd1f88d1', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4c4e9de80284867cef02262ef487366ecfb326e9', '9226', 'STASIUN METEOROLOGI MARITIM LAMPUNG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '7c68e582ef9cc3f1c4572c5d6acbcc5f8b150e79', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('af8107710e23c5b05647dd79ca6f4cd0660afe1d', '9227', 'STASIUN KLIMATOLOGI BOGOR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '0b486467ca176631589de72c19779fe674f13388', '5f682328b4887d57cdfe40d7b1358150a6102b74', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c4a763b2395b0a77689428b4dd18c1cafdda485f', '9228', 'STASIUN KLIMATOLOGI SEMARANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '42c6242dd929b77419907065fe0e7c127948bacd', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d1993eb87d22b665d53bfd31f024c104f9d873eb', '9229', 'STASIUN KLIMATOLOGI PALEMBANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '76d291d5d0d9498f26b37cea62e6cccd8ab2b7ca', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('85a6abf9ebee44b5f4ed109140441e22d99ed000', '92230', 'STASIUN KLIMATOLOGI BENGKULU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'd8c82bf22dd5b689e806f6e381dd3ad459e7485c', '4f7673cbcd56748146807f844b656af9644d40bf', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d008312b8dbb8579e8ca7eaf9f4e9d43becfd095', '9231', 'STASIUN KLIMATOLOGI MEMPAWAH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '499db371dac2489052cf151f8e861ac9d9172df7', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fecc74a6987af689b4f24a871a9e29d6991fca4c', '9232', 'STASIUN KLIMATOLOGI TANGERANG SELATAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'a5a1f057a486d998adcf7229c1e3af0c88383d51', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fa42d6c1197594171ba796f192ed0722fd03dcc8', '9233', 'STASIUN KLIMATOLOGI MUARA JAMBI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '675eed8a709d4098541fb8f73805004c84725d49', 'd6462aa470bcb6f02851209a9714d2dd9d6ca239', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('745dff21d66cc71caa24983df8549d3f051c2d10', '9234', 'STASIUN KLIMATOLOGI PESAWARAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '0efd5a20efd958468a9c47127b811e8747e22e93', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d9f3da3de5fe0eef1dce7d8e5c56f7fc2e4138bf', '9235', 'STASIUN KLIMATOLOGI KOBA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'a25db1256f76937a9a76953511d249e14d49b01e', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ad71b22dc49c6da9ac27adb999059442ce390b7a', '9236', 'STASIUN KLIMATOLOGI MLATI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'dc5fb86daf2eca6f8054441b38a433fb555254f4', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a5b4facab082c6881316e7fcf590ad7983a255b2', '9237', 'STASIUN GEOFISIKA TANGERANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '828f85f9b2301a70d3298e044fc1e35349733622', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('187316f11616500877832e232426a9750b0f207f', '9238', 'STASIUN GEOFISIKA BANDUNG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '0b486467ca176631589de72c19779fe674f13388', '16eb985d0c4af447fd097ebe70e0f56098b196fb', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('db919a9c218b0a48d3820e1f0b4d00fef616b7e5', '9239', 'STASIUN GEOFISIKA YOGYAKARTA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'dc5fb86daf2eca6f8054441b38a433fb555254f4', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9290eab42588a780f808a50fb4fcfe01dfae2a9c', '9240', 'STASIUN GEOFISIKA BANJARNEGARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5b3f5307893d276198f00179e5165aa02e52bc89', '9241', 'STASIUN GEOFISIKA KEPAHIYANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'd8c82bf22dd5b689e806f6e381dd3ad459e7485c', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ff6b9049814c6fb1e452bf464f6c9c7498498d18', '9242', 'STASIUN GEOFISIKA KOTA BUMI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('60946bb13d898f52dbd5f63df1a667dbfe2ce6b1', '9301', 'STASIUN METEOROLOGI JUANDA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6eafabf98671968b6d35911130b1585335fc8367', '9303', 'STASIUN METEOROLOGI SULTAN AJI MUHAMMAD SULAIMAN SEPINGGAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '2de26d1ada47df2ac0d199042f1f736f5b74c857', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2bfb56d903cdde31495ea39cf7e3086cecf7d4b9', '9304', 'STASIUN METEOROLOGI TJILIK RIWUT', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '175f37cdb244988462e7946a62f190f8bcd3c048', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('77c1a9e57cd8c61127138841bf06d1f2bf9f6607', '9305', 'STASIUN METEOROLOGI ELTARI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '574f1b692a66de71c198b693f04612ebb98983ae', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('dd454917c76e5afb518f15054a5568838c49107a', '9306', 'STASIUN METEOROLOGI SYAMSUDIN NOOR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '329ca1d8b316f42fa977cfc7aee782917001fc59', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d3bb805d7b488285b70fa3988a46c788455b09a7', '9307', 'STASIUN METEOROLOGI MARITIM PERAK II', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5a83d9a32ed7fb95e54ccbc8ffb4e597fdf5c214', '9308', 'STASIUN METEOROLOGI BANDARA INTERNASIONAL LOMBOK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '244d62c59fb427bff7e769ac05df7db3f558b835', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a5592ac5656981b7fcfe5918956c6f1f685b022e', '9309', 'STASIUN METEOROLOGI ISKANDAR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '175f37cdb244988462e7946a62f190f8bcd3c048', 'e247e96758cf1d643393b5e376fb4b535bc416e7', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('cfa602a3b7343d39665fdbb7e447659f36bc4d5f', '9310', 'STASIUN METEOROLOGI BERINGIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '175f37cdb244988462e7946a62f190f8bcd3c048', 'c375d33eef61954d3fe252532aeb8cfbbf5ef099', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('003fc795718d4a8ede7c23493f7432022138bacc', '9311', 'STASIUN METEOROLOGI TEMINDUNG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '02900bbb71566c7a53d5bfbe9835465e5f15b986', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e32e00c63302ab47cc1c74bee7b8c9651b38ec98', '9312', 'STASIUN METEOROLOGI JUWATA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '5ecfef4589a11799683bb20435868c23620b01e8', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e7e832de2a0fd5215e3cd7609e85269e912d3576', '9313', 'STASIUN METEOROLOGI KALIMARAU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '91a26a01c082dfb9478de5eee755c0c267a5d41f', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f5228966f46a48b7e744780d1d1564c07bd79927', '9314', 'STASIUN METEOROLOGI TANJUNG HARAPAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '5ecfef4589a11799683bb20435868c23620b01e8', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('28c590a346b0de6a154517e628f3399728cc4d65', '9315', 'STASIUN METEOROLOGI YUVAI SEMARING', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '5ecfef4589a11799683bb20435868c23620b01e8', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ca8d94aa120302f4d2950d7f39dd2b5e7ec061f3', '9316', 'STASIUN METEOROLOGI GUSTI SYAMSIR ALAM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '268ba60feb5ee3700c1c8eed4645ebb93a3f21d0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('69dfc10b443e887c04666cfdb66f852c21b048c3', '9317', 'STASIUN METEOROLOGI SULTAN MUHAMMAD KAHARUDDIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '83053e9b75b804cd6013d93ec5dee7e8bd2f1da3', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('dfa40db1773f501d63be2729b45429ea76d89dd1', '9318', 'STASIUN METEOROLOGI SULTAN MUHAMMAD SALAHUDDIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'b3758f3ddc87381d3b254a70fb38351852bf3e52', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('357506b606933d2072910719429843c4417a744d', '9319', 'STASIUN METEOROLOGI FRANSISKUS XAVERIUS SEDA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '6d2b356d61ca3fcd929354c3efb64f3e2c6aa271', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('29e01c1a0c3e8a917c3b1aefc1ccd3ffef2d86a8', '9320', 'STASIUN METEOROLOGI UMBU MEHANG KUNDA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8b0a111ec0c94553fffc080c31e2ca9c7ac7a0af', '9321', 'STASIUN METEOROLOGI DAVID CONSTANTIJN SAUDALE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('2b7df90295e4a43f20db1c1a680127c3e1d62798', '9322', 'STASIUN METEOROLOGI GWEYANTANA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '121dafc847f62af9bba85a5843e35b9689667eb2', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('aac05dbeb21049e675a0e9765ebd14b7c50a24fb', '9323', 'STASIUN METEOROLOGI FRANS SALES LEGA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'bea0c22885daa21aa5b9d99a88d050f3df08b0e4', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5950eacf95d9f767e27ff43c78d9e0e18f34373e', '9324', 'STASIUN METEOROLOGI MALI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('68b7ddc764695769ec7cda83e92d9c6a5a593dc9', '9325', 'STASIUN METEOROLOGI TARDAMU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('721e3a53d617f335206d8f0d78b811367263aade', '9326', 'STASIUN METEOROLOGI KALIANGAT', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '3d7c394ae564df7adca3c184b27dc1493c7c2f27', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('147f79358db9cfe780deb7ec64de7b23cb689248', '9327', 'STASIUN METEOROLOGI SANGKAPURA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '1ad86e9840a27ae0aca5d8d6ec30a8bfbe4eb3a5', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('282b9af885f2723b107fc929688569bce919985d', '9328', 'STASIUN METEOROLOGI TUBAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'fbaf2aabdad611593dc2b30b8cf09f771b7d3141', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('700c02db2fb31a7578ae90c11c8e399ad738ae1e', '9329', 'STASIUN METEOROLOGI BANYUWANGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '282027c71f277db5f9f7c2df862b6ba670b61b72', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a371372a45c9eb979682d909dfa49531edeec7f7', '9330', 'STASIUN METEOROLOGI NUNUKAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '5ecfef4589a11799683bb20435868c23620b01e8', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('325faeb327f42ba8d27ed02461782ee3968ea39d', '9331', 'STASIUN METEOROLOGI KOMODO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0a347fec9300cf0f64e8c7335364efa46491b34e', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c01ce6ce3704c36a41c3bc12fc4fb2c0f4d9bbfa', '9332', 'STASIUN METEOROLOGI H. ASAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '175f37cdb244988462e7946a62f190f8bcd3c048', 'e247e96758cf1d643393b5e376fb4b535bc416e7', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('566ffaa1b6e91480763d9db56090ad81a19970d8', '9333', 'STASIUN METEOROLOGI SANGGU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '175f37cdb244988462e7946a62f190f8bcd3c048', 'c375d33eef61954d3fe252532aeb8cfbbf5ef099', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('03069113bfbc59d04eeebbcd13f40631c8cdc71d', '9334', 'STASIUN KLIMATOLOGI BANJARBARU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('10daf828826717a865d866e633d1df42cd0bb151', '9335', 'STASIUN KLIMATOLOGI LOMBOK BARAT', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '244d62c59fb427bff7e769ac05df7db3f558b835', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ba01a6c9332a08dea79731c07b0376ee6f2b90f9', '9336', 'STASIUN KLIMATOLOGI MALANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '893612e13f146d5dec1769659625ac2b97b265dc', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8535e268736de6ed39cc3c6b81636a99692d1f5b', '9337', 'STASIUN KLIMATOLOGI JEMBRANA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'b0e70a8c4fcc25fe391827127a7f39932672a861', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('fd3262f30324054f3f8db5473c0f5ae630eaaaa9', '9338', 'STASIUN KLIMATOLOGI KUPANG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'eccdaffc677d841c2ad263ac20d6fa5857415396', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('539d74c81ea7696615c9e7e21a3d2554e7021350', '9339', 'STASIUN GEOFISIKA KAMPUNG BARU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'eccdaffc677d841c2ad263ac20d6fa5857415396', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7f70888051031cf6849ff28c4bf2d5caec25c512', '9340', 'STASIUN GEOFISIKA TRETES', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '35075dee0755cc4d184b2a961730144c6ca7a8a1', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0721c70d49cbe0b1bff490aa340f74410cb406e7', '9341', 'STASIUN GEOFISIKA SANGLAH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '228bec8dd9fec439a87f3069dcc0c31f3a259660', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5b3b3422a2c95d937b8e5ebeec4a6abffecfb0a5', '9342', 'STASIUN GEOFISIKA SAWAHAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '4a1e06d83cc32deed10864d78f75800f35753816', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3f6d2b28bdc85825e2bbed13f192bb84ec87d16b', '9343', 'STASIUN GEOFISIKA KARANG KATES', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '893612e13f146d5dec1769659625ac2b97b265dc', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1c8a5553ffb7aa214cca9e10cbe5ecebff1755d7', '9344', 'STASIUN GEOFISIKA BALIKPAPAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '2de26d1ada47df2ac0d199042f1f736f5b74c857', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a229b3036f666628b3c6d6c6686f5bdc5a229d4f', '9345', 'STASIUN GEOFISIKA MATARAM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '84237065ec6a2e5064e5331db90603d72a210d12', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('451db43c8cb86ebb05ea5efa540ea045632f913a', '9346', 'STASIUN GEOFISIKA WAINGAPU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'd88cfce9e7ec4281eaab61c3448105b228cd093d', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9cf50648b21dca17b0b45b3b02e1b72cf3280f9c', '9347', 'STASIUN GEOFISIKA ALOR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d4dfda783bc925e1e153e049670d3e962517fcc1', '9401', 'STASIUN METEOROLOGI HASANUDDIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'fc8041b5359101d7741e6549e2fdf6e93c731cca', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('dff1a5cefb8eed1ddd317f739b931a53882238d5', '9402', 'STASIUN METEOROLOGI DJALALUDDIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'cbfa6bd4173f0efb045a5bf1c0601e96d4b1d03c', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('3d4bf47b237624aa94bca592871b1956d8e9352e', '9403', 'STASIUN METEOROLOGI SULTAN BAABULLAH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ee3259666f61524b4c74f3e47a093c52abc178ee', '9404', 'STASIUN METEOROLOGI PATTIMURA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '51fcadcaa188f6ace43611645efd71c950ab5c29', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ee01c49ff75d276436fa73eecd566174c2854d7f', '9405', 'STASIUN METEOROLOGI SAM RATULANGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '66a0d4d6889efb40228e10140dbfc1d1b0ef2f3e', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1369966dfcfc861da6ddf95b486e5eb3d87f27b2', '9406', 'STASIUN METEOROLOGI MARITIM BITUNG', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '69b71095a4f1fadc5941cc695b6b93f77e1f3b4d', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8024e5fcd0b3857c3c67bf6491ef6ebb903c9513', '9407', 'STASIUN METEOROLOGI MUTIARA SIS-AL JUFRI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'b9ce2db479cf9805275549ed1397bb36da9a5f69', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('dbd83803e77be43ebdc55135105831f32b82a3bb', '9408', 'STASIUN METEOROLOGI MARITIM PAOTERE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'fc8041b5359101d7741e6549e2fdf6e93c731cca', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e9b60dfff32afbac974621370a67fc86c1bbd5cd', '9409', 'STASIUN METEOROLOGI MAJENE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'a06e899d14482893ff05fda1d8d2efcf37a23fab', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('40d25636a9a8071766f1af844b4e3e59616c013d', '9410', 'STASIUN METEOROLOGI MARITIM KENDARI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '4ecef38c03d777dcc306b703df8319cd8ef26329', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0c53797a341f4775d2d934f58ee141a11483a14b', '9411', 'STASIUN METEOROLOGI DUMATUBUN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '1ab3ae3aae2d662adabf03699648e1bc50a947c4', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7feab6ba1ae66d6204483310c2068b999a9f4e9d', '9412', 'STASIUN METEOROLOGI AMAHAI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'baa49f9f67a1209bfbb8b82bfd38c0ee1cf347df', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('141bff35c5f4ef35ed2969f6ccb13960739a220f', '9413', 'STASIUN METEOROLOGI GESER', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('287b5a961b0d83da4444476ce09620f28b368ac3', '9414', 'STASIUN METEOROLOGI OESMAN SADIK', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'a2e62a45a351c6f19c57774a0fa2797d88f2dff0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('47d0523f24aaf03b8c9640488ea78d5b57d5efc2', '9415', 'STASIUN METEOROLOGI BANDANEIRA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'baa49f9f67a1209bfbb8b82bfd38c0ee1cf347df', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c523b1d94a01b15f6f71209f761ddd011e708e0c', '9416', 'STASIUN METEOROLOGI NAMLEA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5b4febdd1b79d3ff1af38591fefdf8453c7a3baa', '9417', 'STASIUN METEOROLOGI MATHILDA BATLAYERI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'e0e2a38350a30940d826d314a3615ea045bb0c16', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5595b21cfffb1aa1e1d606f279cdcf82df2d3f36', '9418', 'STASIUN METEOROLOGI GAMAR MALAMO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ed9e3eb864b99869d476f691ed7eff014fc80df0', '9419', 'STASIUN METEOROLOGI EMALAMO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '5f806ad90ec6f387331a6acc165552d31e60438d', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('e01b790d3bf9c22685622aa0794273b8ff448085', '9420', 'STASIUN METEOROLOGI KASIGUNCU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '6d0806fd96d8675880e61a220602ac911b6f6884', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8c1e0d6b745d650a85a11c0231872ff02bbbc9e6', '9421', 'STASIUN METEOROLOGI SYUKURAN AMINUDIN AMIR', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'd1fa44804b6f76dd3b1ee46e77f5294070c39f5d', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('ec96efea0a2cc19249ac1df62402786ab6808441', '9422', 'STASIUN METEOROLOGI SULTAN BANTILAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '6d36a35ab8d54a8b35a8ac13f9355b84e4dd45fd', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5bdb085877a841eb97dc6f5ea71d815e14f7f73c', '9423', 'STASIUN METEOROLOGI BETO AMBARI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '5774f0434a471b6de14fac7b20c1ac7df5b22943', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('411bb6e80b71aef3e7546dd26208f237d84c193a', '9424', 'STASIUN METEOROLOGI SANGAI NI BANDERA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '5774f0434a471b6de14fac7b20c1ac7df5b22943', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('de4229e6528ea8b9525898667bc3317321611290', '9425', 'STASIUN METEOROLOGI ANDI JEMMA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '2518b02710dd68dc9f20fc70c0e5aa5f4933fe68', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0a0b6d0f55886c324653bec184777a2c0c552e24', '9426', 'STASIUN METEOROLOGI NAHA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '67bd6c8944ea4cd516e47c08aa8409cf1b5f2319', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a8aecdaa33a96abd85da00435ac4074ceed7c8a6', '9427', 'STASIUN METEOROLOGI PONGTIKU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('02d9e41fcb20121e5d0d998fc9f074a787437552', '9428', 'STASIUN KLIMATOLOGI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('6bcc2cd67709856202066caae51dff2c0d1004c2', '9429', 'STASIUN KLIMATOLOGI MINAHASA UTARA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '116477f533f0f77aeb7d5250ac5a660d70f81572', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9ed7d7c6cd1a536fcf9f64261422d2347ac3dd39', '9430', 'STASIUN KLIMATOLOGI SERAM BAGIIAN BARAT', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'bbbf3defbcd5216b033bf6c858ecbb02160c1282', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4ff86577ed671d8e0a4760482b093d8ba1f49d3e', '9431', 'STASIUN KLIMATOLOGI RANOMEETO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '705581731d6a8dcf57f94b97eedf2cbe5bb2f96b', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('10ff625c16ef0df0c49f56ec9afc548a12c2b218', '9432', 'STASIUN GEOFISIKA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '51fcadcaa188f6ace43611645efd71c950ab5c29', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('b159fc8a7e014d2206db2f867f4e2b6bc7868e1f', '9433', 'STASIUN GEOFISIKA WINANGUN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7b3bee71e072360663d473c245cecae73dc59a42', '9434', 'STASIUN GEOFISIKA PALU', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('eb7fc63b544abf81342f5373360f24e58906666d', '9435', 'STASIUN GEOFISIKA GOWA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('5f3e488c258ece5b7bf456b39ae60344e10b12d8', '9436', 'STASIUN GEOFISIKA GORONTALO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'cbfa6bd4173f0efb045a5bf1c0601e96d4b1d03c', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c3b0af6f267ada02f9f0d36f81e0459fe3f169e4', '9437', 'STASIUN GEOFISIKA SAUMLAKI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'e0e2a38350a30940d826d314a3615ea045bb0c16', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9f7298ab3c0d2404ef0cea253e0b68c0db303d5c', '9438', 'STASIUN GEOFISIKA TERNATE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a024e101e5088b1011b3ffdad510da35664e0fd1', '9439', 'STASIUN GEOFISIKA KENDARI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '4ecef38c03d777dcc306b703df8319cd8ef26329', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('d796b29e72bc1c4d9c02b4ad7773c4d079c0ee1a', '9501', 'STASIUN METEOROLOGI FRANS KAISIEPO', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'fca822ea88ce14f8a2456244cc1f35447c7539e5', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('0e32993bfc81a12f243413964ec41d93b069def3', '9502', 'STASIUN METEOROLOGI SENTANI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '3e189576afe30da8c39f0ea3e6fb3f5627c8ab74', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f405c0737dd04ea241acddf5af7b1d6a24ecc9fc', '9503', 'STASIUN METEOROLOGI SEIGUN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'cac6bb408da778b1dd986cf581e1405801395cdc', '4a083e19ba8c23791a6b4544c617a41c3db2cd86', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a99aabfd9fc8ab263f68c51d4870ec9a4235612f', '9504', 'STASIUN METEOROLOGI MOPAH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0bc555628bee2f79ef756c3f319c7e7b10d13010', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('37822daee7465a67dca7c71793cda4b5a30285a6', '9505', 'STASIUN METEOROLOGI MOZEZ KILANGIN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '36f383ee1dd094a430f2b856a793438c7c3962a7', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1398bc7df55a78295696144199bfaf4fc9ac9e4d', '9506', 'STASIUN METEOROLOGI TANAH MERAH', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('11dd242f0143dcafee2ac6260056794627de2242', '9507', 'STASIUN METEOROLOGI WAMENA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8279afec895e9f3848578b532f307aaf79a72ff0', '9508', 'STASIUN METEOROLOGI MOANAMANI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f79250f28e2d15e87959b9a02bfe90ecffbc804e', '9510', 'STASIUN METEOROLOGI MARARENA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('837eecc74ca33253e8f50c2869f07b5d8b2f531a', '9511', 'STASIUN METEOROLOGI ENAROTALI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('1c0a029275e4625a9581ad6eb311d9307d43a00b', '9512', 'STASIUN METEOROLOGI DOK II JAYAPURA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8ae133c58c6243cb26ac6ecf9f533a13c0ed0449', '9513', 'STASIUN METEOROLOGI RENDANI', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'b83d059734e7478d94c1336dc5013eb863c52052', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('4753013c0f7e57e6640dcc5af9c9ed0fcd54675c', '9514', 'STASIUN METEOROLOGI UTAROM', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'cac6bb408da778b1dd986cf581e1405801395cdc', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('a1d1165f8aa51513abd88a73c388fceed0fe2af0', '9515', 'STASIUN METEOROLOGI TOREA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'cac6bb408da778b1dd986cf581e1405801395cdc', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('f8880215d308eae9527cc17e9ad77a1841821cbb', '9516', 'STASIUN METEOROLOGI JAYAPURA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('8bec99d5edf1564d0fcfcf4bcfce87071bca8f1d', '9517', 'STASIUN METEOROLOGI MANOKWARI SELATAN', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'b83d059734e7478d94c1336dc5013eb863c52052', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('c73846cc21628fd16d315eeabb79bcec2fc73306', '9518', 'STASIUN METEOROLOGI TANAH MIRING', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0bc555628bee2f79ef756c3f319c7e7b10d13010', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('9d38d25ef6be06fba28cbcc56d23e0f51a88e416', '9519', 'STASIUN  KLIMATOLOGI ANGKASAPURA', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '3e189576afe30da8c39f0ea3e6fb3f5627c8ab74', '', '', '', '', '', '1')
<query>INSERT INTO auditee VALUES ('7d5cb002e32c0d277f92e5837f5445bc9aebf9c2', '9250', 'STASIUN GEOFISIKA NABIRE', '0', 'b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0', '', '', '', '', '', '1')
<query>DROP TABLE IF EXISTS auditee_pic
<query>CREATE TABLE `auditee_pic` (
  `pic_id` varchar(50) NOT NULL,
  `pic_nip` varchar(25) NOT NULL,
  `pic_name` varchar(100) NOT NULL,
  `pic_jabatan_id` varchar(50) NOT NULL,
  `pic_mobile` varchar(15) NOT NULL,
  `pic_telp` varchar(15) NOT NULL,
  `pic_email` varchar(50) NOT NULL,
  `pic_auditee_id` varchar(50) NOT NULL,
  `pic_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`pic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS auditor
<query>CREATE TABLE `auditor` (
  `auditor_id` varchar(50) NOT NULL,
  `auditor_nip` varchar(25) NOT NULL,
  `auditor_name` varchar(100) NOT NULL,
  `auditor_alamat` varchar(255) NOT NULL,
  `auditor_agama` varchar(20) NOT NULL,
  `auditor_jenis_kelamin` varchar(20) NOT NULL,
  `auditor_id_pangkat` varchar(50) NOT NULL,
  `auditor_id_jabatan` varchar(50) NOT NULL,
  `auditor_golongan` varchar(2) NOT NULL,
  `auditor_tempat_lahir` varchar(20) NOT NULL,
  `auditor_tgl_lahir` date NOT NULL,
  `auditor_mobile` varchar(15) NOT NULL,
  `auditor_telp` varchar(15) NOT NULL,
  `auditor_email` varchar(50) NOT NULL,
  `auditor_foto` varchar(255) NOT NULL,
  `auditor_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`auditor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO auditor VALUES ('ad7a0edadec8f623975c0078fa4a292b5b4ab25d', 'tes', 'tessss', '', '', '', '', '', 'A', '', '0000-00-00', '', '', 'angga02@gmail.com', '', '0')
<query>INSERT INTO auditor VALUES ('a112e87849f362320a05ec7234fe88826d98e37e', '777', 'test2', '', '', '', '', '', 'A', '', '0000-00-00', '', '', 'dd@gmail.com', '', '0')
<query>INSERT INTO auditor VALUES ('859407f1e00a3dbd921ca7a108640e65df9a5878', '196101141988032001', 'DARWAHYUNIATI, SH, MH', 'BUKIT INDAH BLOK B6 NO. 5 RT., RW., - Banten', 'Islam', 'Perempuan', '326c8092e1a5d8851d126adb8e5f611515cda883', 'ca4d3c424bd05544c7a145bd2af6cef0a08e92e4', 'A', 'Banyuwangi', '1961-01-14', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('9ca611db222a22298db1aa06a02564eebc25c3f1', '197911052006041001', 'BIMA ENDARYONO, S.Sos', 'ANGKASA I NO 18, Kota Jakarta Pusat - DKI Jakarta', 'Islam', 'Laki Laki', '3afc1f39990d28ecc67bd99c6e251970e334cc7e', '142eda73208917765e2bd4c8357e86fbeb00390d', 'A', 'Jakarta', '1979-11-05', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('97ff0c7ed14c9fea149eb17c3031642d4ae94106', '196005211983031001', 'Drs. KASABARNO, M.Si', 'MASJID NO. 37 RT.13/07 Rt.013 Rw.007, Kota Jakarta Timur - DKI Jakarta', 'Islam', 'Laki Laki', '326c8092e1a5d8851d126adb8e5f611515cda883', 'ac0f7970492000d14b9d4ae01ef588083663e6d7', 'A', 'Ngawi', '1960-05-21', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('b46470ec6f7b61b999ebf3cf4347df918d8d002a', '195906181980031001', 'Drs. WIDARNO, MBA', 'Komp. Pinang Griya Permai Blok E.1088, RT.14 RW.06, Kota Tangerang - Banten', 'Islam', 'Laki Laki', '919748dcf6deebf6f8b06a8da290e7688495f4f8', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Purworejo', '1959-06-18', '081222312231', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '195908141980031002', 'Drs. KHAERUDIN', 'KEMANDORAN 3 Rt.003 Rw.003, Kota Jakarta Selatan - DKI Jakarta', 'Islam', 'Laki Laki', '919748dcf6deebf6f8b06a8da290e7688495f4f8', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Gambir', '1959-08-14', '0818988561', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('7ecb1cb5dcdcc409a70ddffa9b61c143c0b31197', '196012061985031001', 'Drs. NUR HIMAWAN, M.Si', 'Perum Permata Duta Blk BI No.1 Rt. Rw., Kota Depok - Jawa Barat', 'Islam', 'Laki Laki', '919748dcf6deebf6f8b06a8da290e7688495f4f8', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Boyolali', '1960-06-12', '0818141221', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('8c14bddd4fb579d519f68f69cb47d6cd9ade22e1', '195709291979101001', 'Drs. MULYONO', 'Jln. Tabah Raya No.27 RT.01 RW.09 Kodamar, Kota Jakarta Utara - DKI Jakarta', 'Islam', 'Laki Laki', '2c7d2f13f69aae77926eef2ec436c1bfdf61b7b7', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Magelang', '1957-09-29', '081318855823', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('e1b5ec0fc6247d99196eba300bbdcaa98f786510', '196002251983031001', 'BAMBANG WAHYUDI, SE', 'Pulo Sirih Timur 2 Blok CA-11 Rt. 07 Rw.013,  - Jawa Barat', 'Islam', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Jakarta', '1960-02-25', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('7ccaeb2a06cf0dc7648eaaeec67f649660d63823', '196202241982031001', 'Ir. PUTUT ADIWASITO, M.Si', 'Jln. Cempedak IX No. 304 Rt.12/15, Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '919748dcf6deebf6f8b06a8da290e7688495f4f8', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Jakarta', '1962-02-24', '08158873117', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('1334b71d5b007fb4ef3655dd761b623cf5ef0afd', '196112201983031001', 'MOCHAMMAD SAMSUL NIZAR, SE', 'Raya Jatimurni Rt.04 Rw.03 no.8, Kota Bekasi - Jawa Barat', 'Islam', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Surabaya', '1961-12-20', '081210339850', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('e77f0a9d990ae8087e137b974c199570f6dac43a', '195904161979101001', 'Drs. SUBAGYO SARDI', 'Jln. Parakan Salak no.7, RT.03 RW.07, Bogor - Jawa Barat', 'Kristen', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Senori', '1959-04-16', '081213746079', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '196006151980021001', 'DARMINI SUHAMAN, SE', 'Taman Puri Indah Blok D 14 No.21, Serang - Banten', 'Islam', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Kuningan', '1960-06-15', '087877696276', '0254-209534', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('3b5059a4bd01603a5f4903f5d5929f6e7878aea0', '195910011983032001', 'NURNIAWATY MAKSUDI, SE', 'SUNGAI MUSI Rt.001 Rw.019,  -', 'Islam', 'Perempuan', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Jakarta', '1959-10-01', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '196008221981031002', 'DJOKO WAHONO, SH', 'Jln. LA Sucipto Gg.22, Graha Cipta Residence D-2, Kota Malang - Jawa Timur', 'Kristen', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Nganjuk', '1960-08-22', '085210911944', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('c2f6d86f82ce263ae87588175f6bd295e1712b81', '196310201984031001', 'TUKIJO, SE', 'Jln. H. Rijin Rt.01 Rw.09 No. 158, Kota Bekasi - Jawa Barat', 'Islam', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Panjatan, Kulon Prog', '1963-10-20', '081585578987', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('f6d2f70e7bcb3b5fdd7fd915034103c6ec17c527', '196104011982031004', 'LUKMAN HAKIM, SE', 'Perjuangan No.32 RT.002/02 Rt. Rw., Bekasi - Jawa Barat', 'Islam', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Bekasi', '1961-04-01', '081380888019', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('24997bc581df7ef7bb3e98d16bf9a7efc6825af0', '195909261981031003', 'SOFYAN SAURI, SE', 'Gang Menteng Ujung Rt. 02/ 03, Kota Bogor - Jawa Barat', 'Islam', 'Laki Laki', 'dfbdb51d2273a3c42d038dbe03884d657f68f501', '77e5755edba4cbf18d9040f222310a853f4388d8', 'A', 'Bogor', '1959-09-26', '081381854925', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('60f0d3df33ae13b6497d7141abeb7e291ae83839', '196501221989011001', 'MON ELFIANSYAH, S.Kom', 'Vila Indah Permai Blok .E.9/24, Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '71b2743ce715c1418dd7307f4b5f6d98d2c8662a', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'Buo (Padang)', '1965-01-22', '081367378518', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('dd059a1e2dca8475452a3b71c8c1978d26b9592c', '196908041990031002', 'SAMSUL ARIFIN, S.Kom', 'ABDUL RAHMAN SALEH Rt.002 Rw.003, Kota Palu - Sulawesi Tengah', 'Islam', 'Laki Laki', '71b2743ce715c1418dd7307f4b5f6d98d2c8662a', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'Surabaya', '1969-08-04', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('3864646afa7555d28422889d13d953f576afb39d', '197205271994031002', 'IRWAN HADI, S.Sos', 'Jl. Vijaya kusuma blok D.1 No.22 Palasari Cibiru Bandung, Kota Bandung - Jawa Barat', 'Islam', 'Laki Laki', '71b2743ce715c1418dd7307f4b5f6d98d2c8662a', '', 'A', 'Palembang', '1972-05-27', '0811732358 / 08', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('4b6cadde1cb4bfc056415a66660c8a7cd176dec2', '196603151990032001', 'WAHYUTI, S.E', 'TUP SEKTOR V BLOK D/14 NO. 10 RT.12/ 22, Bekasi - Jawa Barat', 'Islam', 'Perempuan', '71b2743ce715c1418dd7307f4b5f6d98d2c8662a', '', 'A', 'Sukoharjo', '1966-03-15', '081388425477', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('e69bb153cce91c377bedf827e30e0c77ec370cc7', '197206181993012001', 'YUNI KHOIRONI, SE', 'R.M Khafi I Gg Asem Rt. Rw., Kota Jakarta Selatan - DKI Jakarta', 'Islam', 'Perempuan', '8411f15577c104b0634a0235105e199d4cae53f3', '', 'A', 'Jakarta', '1972-06-18', '', '021-78880971', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('8279529aeed9547632d755851d5cd97a581d5b4c', '197104041998031001', 'WIDODO SUDIMAN, SE', 'JL. MASJID II GG. DOYOK 2 Rt.004 Rw.011, Kota Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '2fa6054ce5103b6dbf4bfd8fbf1c7cb8c324f283', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'Jakarta', '1971-04-04', '085888567233', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('37064cd53c93d855555b53b1c14345c246494e51', '198306252008121001', 'VINCENTIUS FRANS YUNIAR, SE, M.Acc', 'BUDI MURNI, Kota Jakarta Timur - DKI Jakarta', 'Katolik', 'Laki Laki', '91315041c0bfa3e0ac1a79afbc268f7d90eaec25', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'Jakarta', '1983-06-25', '0817772605 / 08', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('72e94e1c581523ad9a729c3e04ba45311a9d48ec', '198603202009111001', 'BAYU PRASTOWO, SE, MAB', 'Villa Bintaro Indah Blok E13 no 15, Kota Tangerang - Banten', 'Islam', 'Laki Laki', '3afc1f39990d28ecc67bd99c6e251970e334cc7e', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'Jakarta', '1986-03-20', '085210414848', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('3ad2e844404c831c951289e22a61527a1d48689d', '198410282008121001', 'DEWIL HITAM, SE', 'JATI INDAH IV NO.8, Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '3afc1f39990d28ecc67bd99c6e251970e334cc7e', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'BEKASI', '1984-10-28', '085691219212', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('6d40e581506e6f2822f9ce5f82528a17c416b007', '197211041993031001', 'SAMSUHADI, SE', 'Wijaya Kusuma III Rt.004 Rw.010, Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '3afc1f39990d28ecc67bd99c6e251970e334cc7e', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'Purwodadi, Purworejo', '1972-11-04', '081288120194', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('52bfdf93477aa817a47bc2c73499ec52b2a64853', '196004151980032001', 'KAMSILAH', 'CENDEKAWASIH II BLK.B63 Rt.006 Rw.015, Bekasi - Jawa Barat', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '', 'A', 'Purworejo', '1960-04-15', '', '021-8219654', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '197501031996031001', 'ARIEF SARTONO, S.Kom', 'Villa Ciomas Indah Blok Q.13 No. 6, Bogor - Jawa Barat', 'Islam', 'Laki Laki', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'Magelang', '1975-01-03', '08159045156', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('d9fed81440d02aca90fc73c11905d5844389115d', '195909111993032001', 'ELIZABETH ENDANG WINARTI', 'Graha Prima Baru Blok T. No.  Rt.009/020 Mangunjaya Tambun Selatan, Bekasi - Jawa Barat', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '', 'A', 'Bogor', '1959-09-11', '081315503185', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('78267737a509ea48c3e17c22e10864fd63e29522', '197804122008122001', 'EKA KURNIAWATI, SE', 'KOMP DKI, Kota Jakarta Barat - DKI Jakarta', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'JAKARTA', '1978-04-12', '08128244941', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('d5f75725351fa9b432529e5dfe8ddb150d052e26', '198412272008122003', 'CHINDY MEGASARI, SE', 'PESUT 3 NO 25 HARAPAN BARU 2, Bekasi - Jawa Barat', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'Majalengka', '1984-12-27', '081311588046', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('fd6cbb2710e82d424a507ea94a989da55a4cc03d', '198203202008122002', 'ENDANG SURYANI SIHOMBING, SE, Ak', 'WIDURI I RT 002/001, Kota Jambi - Jambi', 'Kristen', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'JAMBI', '1982-03-20', '081280065775', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '198508072008122001', 'CITRA RIANA AGUSTIN, SE', 'perum puri permata blok d1 no 2., Kota Tangerang - Banten', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'JAKARTA', '1985-08-07', '0813 8967 6724', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('ae77ffe4e9f5aeca02ffe21169146a2a9fcee4e3', '198312032008122002', 'SAGITA SINAGA, SE', 'KAYUMAS TENGAH NO. 40, Kota Jakarta Timur - DKI Jakarta', 'Kristen', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '', 'A', 'Jakarta', '1983-12-03', '083875954701', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('f1f60608b51b3c3e4c4c3531a6c39e9f7a040fbe', '197508142008121001', 'IMAM MULIA, S.Sos', 'GG. GANIP KP.PADURENAN, Kota Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '3a41229b90364ece381302f990389a9c141b3800', '', 'A', 'Karawang', '1975-08-14', '081219106042', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('2c436fadfd79633967f2ca1400cb86c9b4a74ec3', '197409071995031003', 'NUROKHIM, ST', 'Villa Mutiara Gading 2 Blok C4 No. 28 Cluster Pallazo, Bekasi - Jawa Barat', 'Islam', 'Laki Laki', '3a41229b90364ece381302f990389a9c141b3800', '', 'A', 'PURBALINGGA', '1974-09-07', '08159454407', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('f349bab5974f65f431e4a7a8facfbec2edfffbde', '197803291998031001', 'FRANSISKUS ROBY TRIRIANTO, S.AP', 'Manggarai Utara II/61A, Kota Jakarta Selatan - DKI Jakarta', 'Katolik', 'Laki Laki', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'SURABAYA', '1978-03-29', '081952005019', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('0e048a7f6842acab26029ce12c9ff5e2b614c3a7', '198308012008012017', 'ZURI ACHYANI, S.T., M.AB', 'Sanggrahan Rt. 03/ 12 , Sleman - DI Yogyakarta', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '', 'A', 'Sleman', '1983-08-01', '081328433322', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('96c19c31f21da8283b45096afb56d66e9a7c2f01', '197508171997031001', 'SUKO, SE', 'Jl. Panca Warga I/23 No.9, Kota Jakarta Timur - DKI Jakarta', 'Islam', 'Laki Laki', '3a41229b90364ece381302f990389a9c141b3800', 'b9610baa4575c677951c04e0050ac0e9a5676594', 'A', 'Pati', '1975-08-17', '08192172791', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('256ff902de9fb52a14ecbccf73b9be30695abbee', '197806012009112001', 'NUR AZIZAH, S.Mn, M.Ak', 'ASRAMA POLRI KEMAYORAN NO 3 RT 012/09, Kota Jakarta Pusat - DKI Jakarta', 'Islam', 'Perempuan', '3a41229b90364ece381302f990389a9c141b3800', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'Jakarta', '1978-06-01', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('0bb9bc3ceed25e9596da093f5d503b4d8a06ea0a', '198102112008122002', 'YOSI JUITA, S.E', 'KOMP. MUTIARA INDAH RT 002/RW 003 NO.15, Kota Padang - Sumatera Barat', 'Islam', 'Perempuan', '91315041c0bfa3e0ac1a79afbc268f7d90eaec25', '', 'A', 'PADANG', '1981-02-11', '081374447575', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('2f19716b6fd9ce889e791e33d6ac184f1d09e736', '198504272007011002', 'VINCENTIUS ANDI MINTARJA KUSUMA, SE', 'Vila Bogor Indah 3, Bogor - Jawa Barat', 'Katolik', 'Laki Laki', '91315041c0bfa3e0ac1a79afbc268f7d90eaec25', '1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'A', 'SURABAYA', '1985-04-27', '081330701136', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('c2d1a2be4f86822de7d4c2662f0be6cb08ada0f6', '199104162015021001', 'HERI PURWADI, S.Pd.Si.', 'DUSUN KALIPASUNG RT004/04, Kabupaten Purbalingga - Jawa Tengah', 'Islam', 'Laki Laki', '91315041c0bfa3e0ac1a79afbc268f7d90eaec25', '', 'A', 'Purbalingga', '1991-04-16', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('b781fac2ee332b13d9b6ec839639dd9d457e96c0', '198504182015021001', 'ADHITYA SAPUTRA, S.T.', 'SRIKATON DALAM I/18 RT.003/07 KEL.PURWOYOSO KEC. NGALIYAN KOTA SEMARANG, Kota Semarang - Jawa Tengah', 'Islam', 'Laki Laki', '91315041c0bfa3e0ac1a79afbc268f7d90eaec25', '', 'A', 'Semarang', '1985-04-18', '', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('05ed71e3fbfaa725aaebde387dacbf64c69fc236', '197701062000031001', 'ARY SUTANTO', 'PLUMPANG Rt. Rw., Kota Jakarta Utara - DKI Jakarta', 'Islam', 'Laki Laki', '85c2a8fa3630cd3b5bc3322202572ca3a2526589', '2f19c5284f462170036bddacd2d8b7ef5831b214', 'A', 'Jakarta', '1977-01-06', '08578158749', '', 'peg123@bmkg.com', '', '1')
<query>INSERT INTO auditor VALUES ('4ec93749bacf7b72bceb3f7143f5680f5d04de85', '198508312008122001', 'ILMIATI HASNA', 'Villa Mutiara Gading Blok A.6/01 Rt.001/ 013, Bekasi - Jawa Barat', 'Islam', 'Perempuan', '85c2a8fa3630cd3b5bc3322202572ca3a2526589', '', 'A', 'LANTO BAUBAU', '1985-08-31', '085242999030 / ', '', 'peg123@bmkg.com', '', '1')
<query>DROP TABLE IF EXISTS auditor_pelatihan
<query>CREATE TABLE `auditor_pelatihan` (
  `pelatihan_id` varchar(50) NOT NULL,
  `pelatihan_auditor_id` varchar(50) NOT NULL,
  `pelatihan_kompetensi_id` varchar(50) NOT NULL,
  `pelatihan_nama` varchar(100) NOT NULL,
  `pelatihan_durasi` varchar(5) NOT NULL,
  `pelatihan_tanggal_awal` int(11) NOT NULL,
  `pelatihan_tanggal_akhir` int(11) NOT NULL,
  `pelatihan_penyelenggara` varchar(100) NOT NULL,
  `pelatihan_sertifikat` varchar(100) NOT NULL,
  PRIMARY KEY (`pelatihan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO auditor_pelatihan VALUES ('db5a0c0d4e370621c5d4d209e284dccc837f6445', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Prajabatan CPNS Gol. III', '-', '1430715600', '1433307600', 'BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('deecf8781b35b6b3ab3806ecfdf7bd06a5f2d076', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENERAPAN WHISTLE BLOWING SYTEM (WBS) DAN SPIP', '-', '1495599300', '1439528400', 'BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e18ce76882d3f49fa6ae9b14f7b287c7a23c01dd', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'RAKORNAS WASDAL', '-', '1446703200', '1446703200', 'BKN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a0cb1cdd06806615887ee0f5b3b149bf10bc2524', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'SEMINAR SAK TEMA EVALUASI PELAKSANAAN KEGIATAN PENGAWASAN THN. 2015', '-', '1447740000', '1447912800', 'BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('2e668c67c8bfbf62ebd1b3c36ae416018234d346', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BIMTEKPBJ PEMERINTAH TK. DASARSESUAI PERPRES 54/2010 BERIKUT PERUBAHANNYA', '-', '1450072800', '1450332000', 'DIPONEGORO SMART SOLUTION', '')
<query>INSERT INTO auditor_pelatihan VALUES ('fbeaac83aa024180f22bdd3f3e4f5ae9e48c0c81', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'AHLI PENGADAAN BARANG DAN JASA PEMERINTAH', '-', '1451887200', '1451887200', 'LKPP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e0c4dfc24e82ab878d6a5efd68b13d5d8e1efd30', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('836437a373731e191ae92d3a1c7cf42be30892be', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYELENGGARAAN SPIP', '-', '1454479200', '1454652000', 'BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('eb71b2c887dc2bf314372b6f819c17a44d27fec8', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8b4009393e8af097c3eee0556571b420ddc6d485', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e484044f73835c1b7cf9815da42a7481fa80f3d2', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Training GAWTEC', '-', '1095656400', '1096606800', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ce3d8f0dc6e598a751f592a405fed7930e6cf7c9', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Keahlian PBJ Pemerintah', '-', '1296108000', '1296108000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('152b38ba09cec02e86e3cb2cb2a392aa3eb752f4', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f05115da3d2316adc910f442dd16e5a8555e5c61', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit BMN', '-', '1323669600', '1324015200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a794d91d1170fa9b71c402b11bd7af967dba5777', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '95715709a119311ce0c1d66622801c8c9e1b8919', 'Diklat Sertifikasi JFA Pembentukan Auditor Ahli', '-', '1328680800', '1330581600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('3f595a1e1c5847eddfb1d41774fe738ed2b86cc2', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP APIP K/L', '-', '1383282000', '1383282000', 'DJA', '')
<query>INSERT INTO auditor_pelatihan VALUES ('0dd012ea0a7631f8b7d6ff25f45fa19b071666b9', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('63eb3cde5f2386dbde6a2e178450fa6570507fcb', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('c13b980458b339d1e132d3806fa52ae8a55abe62', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('516984ced9b662239928b8618b915a6fda968527', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('9e8da8aa4b2cdb3947d0fefcc4704be2ee732c63', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1f9c32df0df93e3d423a72d543605f8714d95714', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e26529a20e7173e410e075a0dda8c64c8d1cf6dc', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat PBJ', '-', '943941600', '943941600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('fa92b4143b8e653777cecb33163ccd7a2614904b', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sertifikasi JFA Pembentukan Auditor Trampil', '-', '1339995600', '1341464400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a15de767f9e5aae1e91f06006f8359c6ca24f504', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('6926e84d63140792acd961e313ec363a806631b1', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Prajabatan Gol.II', '-', '964414800', '965883600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('36bb71d4876e3050284843accd22508c8112a588', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SPIP', '-', '1307941200', '1466139600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('76052c5f1708eba16cb925db7e980f8ca27d922d', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat PBJ', '-', '1312779600', '1313125200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('c168056f8f0f206398903a3116dfa6a56a25c844', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop dan Sertifikasi Perpres 54 Tahun 2010', '-', '1318741200', '1319778000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e759caccdedc30bd9d9e2d26c02e52e4e9b6bda0', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('76b1129023b8089078d17c0a580d94b1d4fe7412', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit BMN', '-', '1323669600', '1324015200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('dcbd0115d7a4f6453b8be05f2ac2587ad479e9e9', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP RKAKL APIP K/L', '-', '1383282000', '1383282000', 'DJA', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d1e99d17211e36b3f5d2c468816ae61562ee9c6c', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('c9092f49b257f4d97fa15dd86382d67ca3cbfe7d', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('64164fced4de64a61879bd2c67a5c36d89ed427e', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1997c0028943c06202814d89486912002ce14ac0', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('eecf7dc8c5150eaeb5a6c862788b201da9a407f1', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7606d35de007199a63b5750cb99508daca31acb8', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('cda5a6b53c22996404492308ddf3a9b904220765', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1101362400', '1101362400', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('497a4cef40a850bc695e528fc044ac6fce6768e3', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'SERTIFIKASI A. MADYA', '-', '943941600', '943941600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d15bf06613d525b509f3047853321b4ebb5c264a', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('60662214cea50a1abad40c057df712110350c5b4', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'SPIP', '-', '1391580000', '1391752800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('427e57ff15568ca3947eaae5529446a996775649', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f055a9cc7312462403a9338442b5c8c5a89a059d', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1f25253723f3827fbb1138d402d914dd493c8674', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('937972c9e3faec0dbd00e154741eccc07a068e2f', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('37a7f4a7f05b24decc24db13fdb7b8db79b4b3a1', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7da2efc6fb6b411ab0b1a04c24040f048d9a4eec', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Akuntansi Terapan', '-', '943941600', '943941600', 'PPAK STAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b3cd5a6bbfe0573a12ee5748ba535dfeb58f2751', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Train For The Trainer Budaya Organisasi', '-', '1292047200', '1355464800', 'Sinergi', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8c44a7e95a41423de1f31bb83d8e78c26ca153e2', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Bimtek PBJ Perpres 54', '-', '1293516000', '1293688800', 'Pusdiklat LPP TVRI', '')
<query>INSERT INTO auditor_pelatihan VALUES ('eacbf61d994d7b0fef13e293f6fbf67c8cd80499', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Administrasi dan Ketatausahaan', '-', '1308459600', '1308978000', 'Pusdiklat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('3d5fc91e6d10f0f925b68c0737e6f06b659033b9', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('9eb37b4c4c34c1f9e2150dab65adb680bef61652', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '95715709a119311ce0c1d66622801c8c9e1b8919', 'Diklat Pembentukan Auditor Ahli', '-', '1342414800', '1344315600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('3000c1861d93564214650b4e329cdf5fa991dfb0', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Bimtek E-Procurement dan LPSE Tentang Pengadaan Barang & Jasa yang aman dari Audit', '-', '1366261200', '1366434000', 'Pusdiklat LPP TVRI', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4cc88b5bdf6cd9e529a5c4b6c0d600cfabcf431f', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Penyusunan Indikator Kinerja Bidang Geofisika', '-', '1368680400', '1368939600', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('cae7096926834bdc81b134c01fb4e7474c0c76df', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Manajemen Kontrak dan Penyelesaian Masalah Kontrak dalam Pengadaan Barang/Jasa Pemerintah', '-', '1383022800', '1383282000', 'Pusdiklat SPIMNAS Lembaga Administrasi Negara', '')
<query>INSERT INTO auditor_pelatihan VALUES ('85cd84984a3262dd82d7abc6304fbd7b7312d6ed', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Analisis Pemecahan Masalah', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b4e0f8974f526dc625aeb5cebd8121483b1e0aef', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Sistem Pengendalian Intern Pemerintah (SPIP)', '-', '1391580000', '1391752800', 'BPKP dan BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('021e367ad3ad7dd96aba66427c7c94241da0233f', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Pedoman Inovasi Publik', '-', '1416895200', '1416895200', 'MenPan RB', '')
<query>INSERT INTO auditor_pelatihan VALUES ('5977199112a98a4b11d253517bf45c5ad604f88e', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Penerapan Whistle Blowing System (WBS) dan Penyelenggaraan SPIP pada Satuan Kerja di Lingku', '-', '1439182800', '1439442000', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('2ebdd05ffd90f8ab7b23bb7bcc8530a7fedc7736', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Penjenjangan Auditor Muda', '-', '1439960400', '1441256400', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('dc5957ad62475dfe939a86d6ce382542f624916b', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('40fdf1b905ef342215e0b39bf9ef4de61ca93b60', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Bimtek SAKIP', '-', '1453096800', '1479794400', 'Pusdiklat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4559fce74821e62c2264c40b0777da2636f0b4cc', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1feb7be994bc3f4694da4ca8f01efdd197328bd6', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Reviu Penyerapan Anggaran dan Pelaksanaan Barang/Jasa', '-', '1468818000', '1479448800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('0ec0795bf3b7b67f270e9107603e721f252e2aed', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b1ad63ef52e8dacba450aeca44b769d0113fce40', '9ca611db222a22298db1aa06a02564eebc25c3f1', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Prajabatan Golongan III', '-', '1157518800', '1166248800', 'Badan Diklat Propinsi Jawa Tengah', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a357cd6ae2b90bd43cacf484fd464989ab81d311', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '95715709a119311ce0c1d66622801c8c9e1b8919', 'Diklat Sertifikasi JFA Pembentukan Auditor Ahli', '-', '1337576400', '1339477200', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4d950c0a62a8f4a24093bae4c65fb1bde2cc4e07', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('91464d4f8c4dc3c9d59eb740859480ea05d64905', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Prajabatan Golongan III', '-', '1236747600', '1239426000', 'Pusdiklat Aparatur Perhubungan', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a4f0db5a3eb5f9e8f64387cb5db4f53580d62794', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SPIP', '-', '1251090000', '1251435600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('15d24cb01e6da5e22055c20ba56e00cc2e3f2b51', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Kinerja', '-', '1265004000', '1265349600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('241c4dee1d9bf0c5f7b51cf35238ecb4e453b7ce', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Penyegaran PBJ', '-', '1266818400', '1267077600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('bcc68491df42b8dcf1e19786741fd7f30b1f21b2', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit SDM', '-', '1275282000', '1275627600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('13bb7c772fd81dc46172254bb1c8e6e9d7546b60', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat PAK JFA', '-', '1321855200', '1322200800', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('15d360c02cf609424124833ede00c5aebd4f071b', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('2d27fbfe8db85f9691188504ed8af14dc01a8077', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('265345df9f37b851449714c83799b17ab40c0b62', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('c427c1ddfb24083a8b33845f9b8ba3f54bfd33e8', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1439787600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('9f1fd71dbe1edc51e9fd239ebb3f589d7112b88b', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7caa7048d26d29fd987e843d5d3fdd6e2953bf9f', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('bd86d50d77ff2fca9f0d2b0e3b1be3da5a2276f0', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'PIM TINGKAT IV', '-', '1462338000', '1471582800', 'PUSDIKLAT BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a4d4f99e4ff1df9b0f044b358e465213acef04cb', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('6b69df6360fae109511e4f909ffb3ae91040ac6c', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklatpim IV', '-', '1457416800', '1459400400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('258c5d2ab8d61be764ef21a37c638d340b4520d3', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('26029f95ae0872c820fa8bf90e169da7eacca150', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BHS INGGRIS', '-', '910332000', '927262800', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('76433a1b39157bbb23f93f4adac6e028b834230c', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BHS INGGRIS', '-', '1189400400', '1191128400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('489cf4a019442107997729fce776fca8dc38c0e6', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BREVET A & B', '-', '1212814800', '1222491600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('6d7a58ac5f2e063136735a62dbe5b5a6502398b4', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Prajabatan Golongan III			 s/d 11-04-2009', '-', '1238302800', '1239426000', 'Pusdiklat Aparatur Perhubungan', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f427ea3f72dbb889a00f5376d7d15460ed7341d6', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SPIP', '-', '1251090000', '1251435600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4937d864c145adc1e3817b2c892a80a3fa30415d', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Kinerja', '-', '1265004000', '1265349600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8abb7756045947953af132b51c246d3a1c00e7f7', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit SDM', '-', '1275282000', '1275627600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('369bcabf50309919f59c50733ad4a424437bda64', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Perencanaan bagi Peg. dilingkungan Inspektorat BMKG', '-', '1289800800', '1290146400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f68cba4e5d352b2c475a6e2e0b794b79b6be49a0', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Dilklat Prosedur PBJ', '-', '1297058400', '1297404000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d22fa48f8bf99df7b75cf193c3150d43408018a1', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Peningkatan Kwalitas Penyusunan Lap. Keuangan', '-', '1308718800', '1308891600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('74e7e53598f2a06052b073769dce2f902fb60e12', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Peningkatan Kemampuan Pengelola SDM', '-', '1328594400', '1328767200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('52d7f52ab49b70fafade568db4c43ef79542f65d', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418968800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('fc504b9d0c2563914a33f244fe4b56a49cc93ffd', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('15b866ea23ba95299e337b54be81ce4a2b0e056b', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d068350b10dcf3d0fe29b578af42181473990bde', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('0a23fb6f61a73a9a9b566d50a5376d3ea68f85f2', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Pembinaan Administrasi dan Pengelolaan Keuangan', '-', '1280206800', '1280379600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e2a283fa95a0554e26cb79329cd4f904f044c761', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'M�ngetik', '-', '271400400', '272437200', 'Kursus 30 Jam', '')
<query>INSERT INTO auditor_pelatihan VALUES ('936ae720982ccc5179ffe74033e8c7d2a49e8f5e', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Bendaharawan', '-', '498373200', '503647200', 'Badan Diklat Keuangan', '')
<query>INSERT INTO auditor_pelatihan VALUES ('149c46806048995cbd63f3ae5e10d018efc4c321', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Penyuluhan Inventarisasi Kekayaan Milik Negara', '-', '780555600', '780642000', 'Balai Wilayah II Ciputat', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4e10cdb55397fbd1ea65ddd904dbc52d96ca37f1', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Penyuluhan Tehnis Pengamat Meteorologi Penerbangan', '-', '881647200', '881820000', 'Balai Wilayah II Ciputat', '')
<query>INSERT INTO auditor_pelatihan VALUES ('aa9b70ec3977edc4a1af1c217b104327db45ca90', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '95715709a119311ce0c1d66622801c8c9e1b8919', 'Diklat Sertifikasi JFA Pembentukan Auditor Trampil', '-', '1066021200', '1067407200', 'BMG & Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('765383fb2d916ee1a64c9545098818c2519fa78b', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Pengadaan Barang dan Jasa', '-', '1130130000', '1130475600', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('cfadf0f02b38afdda31f89c36f312a92f15a54f2', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sistem Akuntansi Instansi (SAI)', '-', '1160456400', '1160802000', 'Inspt BMG & BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('685211b2b0d69dc49beee649f784ea22ef552b4a', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Reviu Lap. Keu Kementrian Negara/ Lembaga', '-', '1178514000', '1115787600', 'Pusdiklat BMG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e0c9b45789da8b4812700bebc089d5022b82f31a', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Evaluasi LAKIP', '-', '1209358800', '1209531600', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b20d3f8fb1724ec6882f6fa4055df1de218f0b71', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Penyusunan LHA yg Efektif', '-', '1240808400', '1241154000', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('9ed01a87c364c1500699d151f619401799af0c74', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SPIP', '-', '1251090000', '1251435600', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e5acc03ab7f529d5581f936d16bcb7457d578645', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Kinerja', '-', '1265004000', '1265349600', 'Inspt BMKG & Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f124e129faa299e7c07565243b5a4978e43ff60b', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit SDM', '-', '1275282000', '1275627600', 'Inspt BMKG & Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('2f0daaf9e12c9681c7462c2bf1b10d771020de8e', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Pembinaan Administrasi dan Pengelolaan Keuangan', '-', '1280206800', '1280379600', 'Biro Umum BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('42365349c989a0d9b67c2901a34d3279c8287727', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Perencanaan bagi Peg. dilingkungan Inspektorat BMKG', '-', '1289800800', '1290146400', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('39ed1bd36c621402b4370588bb205fe65fae7cb8', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '95715709a119311ce0c1d66622801c8c9e1b8919', 'Diklat Sertifikasi JFA Pembentukan Auditor Ahli', '-', '1269234000', '1271221200', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7af180d8412b26a95afaeea88830d9a19598dd78', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Dilklat Prosedur PBJ dan Audit PBJ Pemerintah', '-', '1297058400', '1265868000', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('641a426d791672eb21f0a3de5189a7a47ab9f169', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SPIP', '-', '1307941200', '1308286800', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('3af551536f0b4a96147f4d4dad42dcdb785e2d83', '859407f1e00a3dbd921ca7a108640e65df9a5878', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Program Pendidikan Reguler Angkatan (PPRA) LIII Tahun 2015 LEMHANNAS RI', '-', '943941600', '943941600', 'LEMHANAS', '')
<query>INSERT INTO auditor_pelatihan VALUES ('08bf928feb6a6f7a27c43e940ccf2aecf37440ee', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Bimtek E-Procurement dan LPSE tentang PBJ', '-', '1366261200', '1366434000', 'Pusdiklat LPP TVRI', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b059a484b0a20cb2901091b1fad457406e08133c', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sertifikasi Penjenjangan Auditor Madya', '-', '1360303200', '1361512800', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8a7f5029c9a723b3a8b34f502e4614938d997933', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Fungsional Penjenjangan Auditor Ketua Tim', '-', '1304312400', '1305781200', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('89898d77be1be994269c5fac046a1b1732dfd95e', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Pengelolaan BMN', '-', '1323669600', '1324015200', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7c788d39b7df9a3ab9bd5b950d7013a83b41b8ef', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e5c1557e2a8e5a457d95f4e1caffd5544bd7596f', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Analisis Pemecahan Masalah', '-', '1388988000', '1389333600', 'Pusdiklat BMKG & Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('79600efcc7808c6558536bfcf5135d7d1babd606', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop SPIP', '-', '1391580000', '1391752800', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b6aab96129c0732b955d08fd5dd744cd55230e4f', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Investigatif', '-', '1466658000', '1467003600', 'Pusdiklatwas BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('387d56f5f248554a77c0d06f4dc52d2eab662cc1', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN/RB & BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4b1dfb9e6324cfed4d039663bcc7899d7dc17538', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('98f9bf3a2382655bec822b61d0fd366915bb0f3f', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Penerapan WBS dan Penyelenggaraan SPIP pada Satker di Lingkungan BMKG', '-', '1439182800', '1439442000', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f1fbfd2ac24687e49e1bc0b30bd73bfa0503e30e', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Reviu RKA-K/L di Lingkungan BMKG', '-', '1440651600', '1440651600', 'Biro Perencanaan & Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a53477dc710d439685fbddbe04bfd363f99c8564', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Tentang Informasi Pengawasan, IACM, dan SKP', '-', '1452578400', '1452578400', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('70693f7967539568272a9d73faff226f82f39900', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Reviu LAKIP', '-', '1452837600', '1452837600', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('705a3148289d9bdaf3abc55ed3565644f94e42ef', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Penyelenggaraan SPIP', '-', '1453096800', '1453269600', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('053c371e601122cbc3727c053f147d7aedd95fc2', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Workshop Penyelengaraan SPIP', '-', '1454306400', '1454479200', 'Inspektorat BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('fc7fa55b3f4b56c4a7b6b4b74ecd06d043dcab40', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('240516c8f6d694628a87cbc06867e9df0426d111', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a9cbf46cf85c20474cf53a6077abb074c6f0a8c8', '859407f1e00a3dbd921ca7a108640e65df9a5878', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('062e75c6345d6c1e916edcb76ecf4fefacac2be2', '859407f1e00a3dbd921ca7a108640e65df9a5878', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYELENGGARAAN SPIP', '-', '1454479200', '1454652000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('5963064a3426ba8022d082711328be81f0b2aa6f', '859407f1e00a3dbd921ca7a108640e65df9a5878', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7c175267a697e9ff2801105679ad670cecfaf575', '859407f1e00a3dbd921ca7a108640e65df9a5878', '41640807ef9f277221c36c7fa2207b6708bd297d', 'FORKOM SUBIJAK', '-', '1464670800', '1464757200', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d82b883361f70da590ddbdc6aa606d08a4b8a20f', '859407f1e00a3dbd921ca7a108640e65df9a5878', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BIMTEK MENTOR, COACH DAN PENGUJI DIKLAT PIM IV TAHUN 2016', '-', '1465534800', '1465534800', 'PUSDIKLAT BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ec105d2e8f5f317881c66ae33d18932f8ec2a305', '859407f1e00a3dbd921ca7a108640e65df9a5878', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklatpim II', '-', '1285045200', '1290751200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('72c40d0d868c1c20feb98749f2f7f601d1f7504c', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'KOMPUTER', '-', '1067493600', '1070863200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('6215e8f23317db66ca8037b41c1623a4ab020d1e', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BHS INGGRIS', '-', '943941600', '943941600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ed426afdb6dea58be5c6bfbda6dc017158459efe', '3ad2e844404c831c951289e22a61527a1d48689d', 'f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Diklat Prajabatan Golongan III', '-', '1238302800', '1239426000', 'Pusdiklat Aparatur Perhubungan', '')
<query>INSERT INTO auditor_pelatihan VALUES ('be2c0f347cb9334afdec78c0d82aa64a88b2d6f3', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Dilklat Prosedur PBJ', '-', '1245042000', '1245301200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7baf099cb49bf6b9b03af12f8f6f1fcdfcece8d0', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Kinerja', '-', '1265004000', '1265349600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d80f34d1b6f545dc64d01fdd8d05832ca3043d04', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit SDM', '-', '1275282000', '1275627600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f078d8af67858a2ae50c7e139352e9395d7a1352', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Perencanaan bagi Peg. dilingkungan Inspektorat BMKG', '-', '1289800800', '1290146400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ad7ad79403ca232bcfb9afeaa94ecdd71909e194', '3ad2e844404c831c951289e22a61527a1d48689d', '95715709a119311ce0c1d66622801c8c9e1b8919', 'Diklat Sertifikasi JFA Pembentukan Auditor Ahli', '-', '1276491600', '1278392400', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('03a1ac7d347ad493069343833803021d1aea2093', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SPIP', '-', '1307941200', '1308286800', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d6c37425995c14db29020a01091ead3b69d9f5c1', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('49c0b71824f04f5382c2794fda9b9a809657a2f2', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Pengelolaan BMN', '-', '1323669600', '1324015200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('c9b3b0b3e164562c06af9f7fb6686738c07d55e0', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ca23c4dabe97d8c3d426a78fa8931b8c962f09fb', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('16037175b3570ebc8fafe422e63ccaad3e7f49f1', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP	-		17-12-2014 s/d 17-12-2014', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b645822a42bbb70e02fa66327ba856c0ccf552ac', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('041582a827d7cf29ade1785d851933dab3442c91', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458018000', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a44f1450a77d8e63d0afb4c4ecf83fe3a2f9afe3', '3ad2e844404c831c951289e22a61527a1d48689d', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('fb605bf97441c153fd90890fc0039e52136466d2', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('692fbb45e377e4065a0cf6bb9af2e3c99571ca5e', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Training Peralatan Meteorologi, Geofisika dan Telekomunikasi', '-', '411026400', '416642400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('2fa9f86952846ea74662180c6a66728dcc89076a', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'BENDAHARA A', '-', '505980000', '510386400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1c24b8b0b590dc9e5d9d04e8c4ca788355a68fda', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'PEMBENTUKAN A. AHLI', '-', '1206334800', '1208235600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4895f63b370479a325a9c6f1bffc767bcfe4db86', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ec9ecfeaec8ae357d2c0f88d5585297fc8942239', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('bec8b7725703407ba19bc677302775cc900ae7d4', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1370581200', '1371790800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('da274c8fb84b0e669a735526bea061be0aca3ca4', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('5c45257798a3da12fe9258aae510cb4e3c58d6d4', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4e548bd86141d79c2f1d84f5886217e4dad8a705', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ec9e5106227c93dda40d8bd3d502c8c0f7309433', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Pengelolaan BMN', '-', '1324015200', '1323669600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b1efe904e88ee65d71b37c41ccec8c903517f215', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('99abb8910ee0471301100d5a244ca86e1508526d', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Dilklat Prosedur PBJ dan Audit PBJ Pemerintah', '-', '1297058400', '1297404000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('094c6bcfa3946e1ef2f066f958f6dcc94372d074', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit SDM', '-', '1275282000', '1275627600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('3fce524afa775e50a623fc158ddb9ba0eb6ac333', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Kinerja', '-', '1272690000', '1272776400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8842c7d165d3322eee7ada4f9b0048a80de70dd9', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYUSUNAN IKK BID. METEOROLOGI', '-', '943941600', '943941600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('f3d689e89190edf3d80eb89404e768ea91d73602', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('09a67977bc3e26a75828aed223502ce033c72a1f', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'PRAKIRAWAN ANGKATAN II', '-', '943941600', '943941600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7aff3404374ffb12b5c0f41dae27379d2aa7633c', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'APPLICATION OF SYNERGI SOFT. METEO. PROCES', '-', '796885200', '797317200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7316f7d758185616b7e9f6bda276f49e68cdb1c5', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WAFS WORK STATION OPR STAR 4', '-', '807426000', '807771600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('c81a5ad502da72e3fe345e80d5a1b363b81f58c7', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'APT / METEO SATTELITE PICT. RECIEVING STATION', '-', '815032800', '815378400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('dbc7e68c3f5da88d37f410ef7fa24a49670118c2', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WEATHER FORE CASTING FOR OPER.METEO ASIA PASIFIC', '-', '910245600', '912924000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b3f30f1a3086f33d48d11c78124bc3aa18f9e241', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'OPERATIONAL METEO SYSTEM', '-', '943941600', '943941600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a7d97763492c7b3186b5ac502221bc78fdeb3e39', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sertifikasi JFA Pembentukan Auditor Ahli', '-', '1066021200', '1067839200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b59a70f5fefb4ed24efca073e9fc1aefa985cbd2', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sertifikasi JFA Penjenjangan Ketua Tim', '-', '1110175200', '1111989600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('d9c2e5cbda9f30e8fc95caf0ce257deca91426a6', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Investigasi', '-', '1129525200', '1130389200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8a8da837a118d78c04c086eaa54732af4fd7a018', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Reviu Lap. Keu Kementrian Negara/ Lembaga', '-', '1178514000', '1178859600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('5d81e938869cfe5c49011313f58be6ffa5a94e1a', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Manajemen Pengawasan', '-', '1206421200', '1206680400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('b65223bc7caad44b80f7834764293a06993a1060', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit PBJ', '-', '1207544400', '1207890000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('232918e184de64437254683f36b042ab4f96bc11', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Evaluasi LAKIP', '-', '1209358800', '1209531600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('6fe79e4c087902178399412e1d6fab38a7e8b164', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Penyusunan LHA yg Efektif', '-', '1240808400', '1241154000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ecf90ee95688d8e1bbc3faca89f1aaacf7bc8d54', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('23f6fe2a30d3c1028370d5c8a7134a8a50c3c700', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('48e10540162efe7538c99accd3c9434af578b348', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('9bcc4465fb6b523aaa53bb83f08c1eca6aba17a5', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('05d5ec12e6323af6d3ff7ee9d32fbd6a2bd413ec', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('0eb3a3b9213d397716aac00065d4dee711cede2f', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('46d77ba010e22df09171746eec4b1c57b99eb28c', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYUSUNAN IK BID. KLIMATOLOGI', '-', '1371358800', '1434690000', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('df7c32855f2c306051229f4f137a40d7372782ae', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYUSUNAN IK BID.GEOFISIKAGI', '-', '1368680400', '1368939600', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a5742002083ef123d112b02b8735179c5310fd76', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYUSUNAN IK BID. METEOROLOGI', '-', '1367730000', '1367989200', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('5ed19b3c2f3287b924d667e00363db09952fb01c', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit SDM', '-', '1275282000', '1275627600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('374946f342e4f6e778eaa6cf01aa137691989792', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Kinerja', '-', '1265004000', '1265349600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('7204bbeb301ba40f5f347ffa39231f08a8d3bcbf', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Penyusunan LHA yg Efektif', '-', '1240808400', '1241154000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('33273b3bfc2063ff5e32c82f8ec3382cf83b38e4', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Evaluasi LAKIP', '-', '1209358800', '1209531600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('37125226505fb997a6acf796774f5aa98e2fe884', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit PBJ', '-', '1207544400', '1207890000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('bd0822c402ec6165b7ed634a28ce1b2e96f1b137', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Reviu Lap. Keu Kementrian Negara/ Lembaga', '-', '1178514000', '1178859600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('271b963f9e12a81cfc9ea24dda7f30a3b70ea9c3', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat SAI', '-', '1160456400', '1160802000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('e83372d9aadaeb6bbc82229d76f5a6474cb3663d', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sertifikasi JFA Penjenjangan KT', '-', '1110175200', '1111989600', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('674a7045d96c0f1308c7649ad1436a5e9f80d2fd', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Investigasi', '-', '1129525200', '1130389200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1c7fdf9ccc33d6eb2f3d1d4791b5f1c676732889', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Penataran Calon Pengelola Proyek / Bagian Proyek di Lingk. Dep. Hub. Angk. III', '-', '880351200', '880696800', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('96edff6d6fdb5c37bc972e080f3dd0860a9b599e', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Prog. Pendidikan dan Latihan Perenc. Proyek-Proyek Pembangunan Angk. XXV', '-', '844059600', '845960400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('8cacd273b59412ceda92fa94b15bbdb295c9898d', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Penataran IKMN', '-', '726127200', '726559200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('51b5200a5bb9140eb2a0bc3cfd7338e23745dca6', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PEDOMAN INOVASI PEL. PUBLIK & KOMPETISI PEL. PUBLIK', '-', '1416895200', '1416895200', 'MENPAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('ffc1d5fd2570788220744207a488886fd72c3111', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Studi Banding BPKP Prop. D.I Yogya', '-', '1223874000', '1223874000', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('9cbb41e7b416bfb96ef5f466686a2272953aaa40', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Sertifikasi JFA Penjenjangan Dalnis', '-', '1272862800', '1274158800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('15c550f6ed629ee71a429c8ed434d52310048662', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit Berbasis Resiko', '-', '1323064800', '1323410400', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('aefab2700b80a8355d45a0adb0d9d206b799669b', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'Diklat Audit BMN', '-', '1323669600', '1324015200', '-', '')
<query>INSERT INTO auditor_pelatihan VALUES ('a689005ed7bccc4ae4c5681989bcadee6d2c93d8', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP EV. MATURITAS LEVEL SPIP', '-', '1479880800', '1479880800', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('80c53d2db4cffbdb20a8805bae858133e6136112', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP IKK BBMKG, STA GAW DAN STMKG', '-', '1458018000', '1458190800', 'INSPEKTORAT', '')
<query>INSERT INTO auditor_pelatihan VALUES ('0ef9d906613b1044773c820e18010314e7c1c08d', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP PENYELENGGARAAN SPIP', '-', '1453096800', '1453269600', 'BMKG', '')
<query>INSERT INTO auditor_pelatihan VALUES ('002dc46b280d41a1b9deafcff5a60738f3175ca0', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP INFORMASI PENGAWASAN, IACM DAN SKP', '-', '1452578400', '1452578400', 'KEMENKEU', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1ad37b4fb1ca51ba7282750acffe4a43cf008263', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP REVIU RKA-K/L DILINGKUNGAN BMKG', '-', '1440651600', '1440651600', 'BIRO PERENCANAAN', '')
<query>INSERT INTO auditor_pelatihan VALUES ('1bab9a75b842a9f1fa9794355d9d00dfb44888bc', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'WORKSHOP TONE OF THE TOP KUNCI SUKSES PENYELENGGARAAN SPIP', '-', '1418796000', '1418796000', 'BPKP', '')
<query>INSERT INTO auditor_pelatihan VALUES ('4da706f4a26e04193b92b9761772f74ef2c823b4', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '41640807ef9f277221c36c7fa2207b6708bd297d', 'DIKLAT ANALIS PEMECAHAN MASALAH', '-', '1388988000', '1389333600', 'BPKP', '')
<query>DROP TABLE IF EXISTS auditor_pendidikan
<query>CREATE TABLE `auditor_pendidikan` (
  `pendidikan_id` varchar(50) NOT NULL,
  `pendidikan_auditor_id` varchar(50) NOT NULL,
  `pendidikan_tingkat` varchar(20) NOT NULL,
  `pendidikan_instuisi` varchar(100) NOT NULL,
  `pendidikan_kota` varchar(50) NOT NULL,
  `pendidikan_negara` varchar(50) NOT NULL,
  `pendidikan_tahun` int(11) NOT NULL,
  `pendidikan_jurusan` varchar(50) NOT NULL,
  `pendidikan_nilai` varchar(5) NOT NULL,
  PRIMARY KEY (`pendidikan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO auditor_pendidikan VALUES ('a858be45a5913234321d4fce037a3206884aa1a9', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', 'SD', 'SDN Krapya 01', 'Semarang', 'Indonesia', '1997', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('fa7d521174af710f96a4ae69e0f5efd031a97060', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', 'SMP', 'SMP 18 Semarang', 'Semarang', 'Indonesia', '2000', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('d725a47afc021cefd08c0f20dd9f13e9da787d97', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', 'SMA', 'SMA 6 Semarang', 'Semarang', 'Indonesia', '2003', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('6c3a96f5916a0f4a43cd9395e0076a5829f6c0ef', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', 'D3', 'Universitas Diponegoro', 'Semarang', 'Indonesia', '2006', 'TEKNIK KIMIA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('3cb725fd6382b2bb19bbe915357edec351fa2f3d', 'b781fac2ee332b13d9b6ec839639dd9d457e96c0', 'S1', 'Universitas Diponegoro', 'Semarang', 'Indonesia', '2008', 'TEKNIK KIMIA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('23bdc10d8932b3f214126391d67ce0f9de2b782f', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', 'SD', 'SDN I', 'Salaman', 'Indonesia', '1987', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('f98900164f0e4ac3ee8c9bdbf43657eb68683702', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', 'SMP', 'SMPN 1', 'Salaman', 'Indonesia', '1990', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('f4bd7d4034b1c0c2e0f8871f1aa60bdb6b125f5a', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', 'SMA', 'SMAN 4', 'Magelang', 'Indonesia', '1993', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('1b38a5a7ead3e01dc037581f03170a62e30abe19', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', 'D1', 'BPLMG', 'Jakarta', 'Indonesia', '1995', 'METEOROLOGI', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('f44534e647f4ba465ffc302c5d75138dd802d7c9', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', 'SD', 'SDN 17 PAGI', 'Jakarta', 'Indonesia', '1989', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('397edac24b92924cd1e2b4a02f569fc61b16f24b', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', 'SMP', 'SMPN 121', 'Jakarta', 'Indonesia', '1992', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('6a42bc414fcd311f78d71e0fd680a558ef2a3291', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', 'SMA', 'STM CIKINI', 'Jakarta', 'Indonesia', '1995', 'ELEKTRONIKA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('27363209a7a75bd949e5bdfb9fe5499ff6700af2', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', 'D1', 'AMG', 'Jakarta', 'Indonesia', '1999', 'METEOROLOGI', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('43d0d4cebc60062cf147c32ff69821287b7d41d4', '05ed71e3fbfaa725aaebde387dacbf64c69fc236', 'D3', 'AMG', 'Jakarta', 'Indonesia', '2009', 'METEOROLOGI', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('f994a55f85903dfe700b8ed60ec401f38820b0fc', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', 'SD', 'SDN UTAN KAYU PG III', 'Jakarta', 'Indonesia', '1971', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('c5f13fa1743fac8f227579c165bcfda0c9c2ceb9', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', 'SMP', 'SMPN LXXIV', 'Jakarta', 'Indonesia', '1974', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('2184efb4ebfbf193a1fa1fd557cd277235c858bc', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', 'SMA', 'SMAN XXX', 'Jakarta', 'Indonesia', '1977', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('1a57d89ae2907ddbca57c835fe7a16665567b413', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', 'D1', 'AMG', 'Jakarta', 'Indonesia', '1983', 'METEOROLOGI', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('2bbddb81d375b8189c6fa9e946f859864130fe3e', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', 'D3', 'AKD.AKUN.PERBANAS', 'Jakarta', 'Indonesia', '1986', 'AKUN.PERBANKAN', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('e660f23dc3a3db73262281d55639dcb60c37a5be', 'e1b5ec0fc6247d99196eba300bbdcaa98f786510', 'S1', 'UNIV ISLAM ATTAHIRIYAH', 'Jakarta', 'Indonesia', '2008', 'Akuntansi', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('cc55b50e1ef255384a4ba7997b5f3c9beef72e2e', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', 'SD', 'SD KARTIKA X - 4 PESANGGRAHAN', 'Jakarta', 'Indonesia', '1997', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('2098355d4c6baf542c5f57016fce56be0d2f24ef', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', 'SMP', 'SMPN 235 PESANGGRAHAN', 'Jakarta', 'Indonesia', '2000', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('29387e4a64cddf02d0c122fa58f80776b32c5475', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', 'SMA', 'SMUN 86', 'Jakarta', 'Indonesia', '2003', 'IPS', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('9caec582585b6371338840e36c82eb6f055686a3', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', 'S1', 'UNIVERSITAS PROF. DR. MOESTOPO', 'Jakarta', 'Indonesia', '2008', 'MANAGEMENT', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('c9062d342ede5a40ce2308266a6f9f3f43010be6', '72e94e1c581523ad9a729c3e04ba45311a9d48ec', 'S2', 'SEKOLAH TINGGI ILMU ADMINISTRASI', 'Jakarta', 'Indonesia', '2012', 'MANAJEMEN EKONOMI PUBLIK', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('6265d6d7157a67f283d703477181598dce4395e2', '9ca611db222a22298db1aa06a02564eebc25c3f1', 'SD', 'PERWARA', 'Jakarta', 'Indonesia', '1992', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('536e805725a791de1d3adec4cec5f32b09afece2', '9ca611db222a22298db1aa06a02564eebc25c3f1', 'SMP', 'SMPN 4', 'Jakarta', 'Indonesia', '1995', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('eb8947892ed5a87f7fa9262e072e048790996da1', '9ca611db222a22298db1aa06a02564eebc25c3f1', 'SMA', 'SMAN 1', 'Jakarta', 'Indonesia', '1998', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('3ea8c2e28a92e12131c10459a5f6d3134b330982', '9ca611db222a22298db1aa06a02564eebc25c3f1', 'S1', 'Universitas Padjajaran', 'Bandung', 'Indonesia', '2004', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('53ccc6b40c8e8b163f2bc9ca3152f0b9a9056a74', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', 'SD', 'SD NEGERI KERANJI III', 'Bekasi', 'Indonesia', '1996', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('ffb20791d853b463a3eaf94830260aec59c99076', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', 'SMP', 'SMP NEGERI I BEKASI', 'Bekasi', 'Indonesia', '1999', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('bb72f10592d81f760869a1ec03cc2b2e0877b88d', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', 'SMA', 'SMA NEGERI 2', 'Bekasi', 'Indonesia', '2002', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('7df989b8bd32e2c9d7c68dd74cb9e870159abb08', 'd5f75725351fa9b432529e5dfe8ddb150d052e26', 'S1', 'UNIVERSITAS TRISAKTI', 'Jakarta', 'Indonesia', '2007', 'Akuntansi', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('09de81069cf796f98ad76363883e4174ab94305b', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', 'SD', 'Sekolah Dasar', 'Jakarta', 'Indonesia', '1997', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('bd1876a50152ceb3c98c6afe86ec7ac33f998929', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', 'SMP', 'SMPN 94 Jakarta', 'Jakarta', 'Indonesia', '2000', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('f1846b44761029c0ec7de61d4169d1ed4544e950', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', 'SMA', 'SMUN 35', 'Jakarta', 'Indonesia', '2003', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('5fa8ae101672f29190a48aa5753999749b9a10e4', '4bb0a5231c2c42455a8d4fb44b9078cdc3666467', 'S1', 'STIE YAI', 'Jakarta', 'Indonesia', '2007', 'Akuntansi', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('2a5ec44199b0211ed5ca213d03a3a2489e666879', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', 'SD', 'SD CANTILAN I', 'Kuningan', 'Indonesia', '1971', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('b6cf8d9f5da59512c2c2f718d21500efed3237e5', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', 'SMP', 'SMP 17 AGUST '45', 'Jakarta', 'Indonesia', '1974', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('44db8533c308cb898c414ec1b86ca74725b6b7a9', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', 'SMA', 'SMA 17 AGUST '45', 'Jakarta', 'Indonesia', '1977', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('2451a840b812b92d26e3a9dbe2da5cbafb24a60b', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', 'D1', 'AMG', 'Jakarta', 'Indonesia', '1980', 'METEOROLOGI DAN GEOFISIKA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('3abec339d20f0eecabf2333f3d7f41bfdec950b5', '7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', 'S1', 'UNIVERSITAS ISLAM ATTAHIRIYAH', 'Jakarta', 'Indonesia', '2009', 'Akuntansi', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('76e9ddf62bc2bf2cd2213830628deb8f42148c29', '859407f1e00a3dbd921ca7a108640e65df9a5878', 'S1', 'Universitas Jember', 'Banyuwangi', 'Indonesia', '1985', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('30b882b45ff4fe4cd812d6e8f72d9deae8872305', '859407f1e00a3dbd921ca7a108640e65df9a5878', 'S2', 'Universitas Jayabaya', 'Jakarta', 'Indonesia', '2000', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('adb5070e1563bb43a4e758264abc61435b7c1637', '3ad2e844404c831c951289e22a61527a1d48689d', 'SD', 'SDN PANCA MOTOR I', 'Bekasi', 'Indonesia', '1996', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('3ae64b29e7c7553ea2d5fb5f7b28dba4b5123aed', '3ad2e844404c831c951289e22a61527a1d48689d', 'SMP', 'SMPN 5', 'Bekasi', 'Indonesia', '1999', '.', '.')
<query>INSERT INTO auditor_pendidikan VALUES ('6014eb57cca07384781a9b7842ffc2c9e1b9f6f3', '3ad2e844404c831c951289e22a61527a1d48689d', 'SMA', 'SMUN 4', 'Bekasi', 'Indonesia', '2002', 'IPS', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('53066b7d036a8dc27129781a0c4d8c2964d62541', '3ad2e844404c831c951289e22a61527a1d48689d', 'S1', 'STIE MULIA PRATAMA', 'Bekasi', 'Indonesia', '2006', 'Akuntansi', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('8edd51ee90c54a78848539b07665fbfe97a1fb48', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', 'SD', 'SDN', 'Kertosono', 'Indonesia', '1972', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('c51821e254a8d3ebdc86886d2b867bdd4ef98c94', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', 'SMP', 'SMPN', 'Sidorajo', 'Indonesia', '1975', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('4e7e2cec6332d741c3b080272e8f7bd6d5d9c1e2', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', 'SMA', 'SMN', 'Kertosono', 'Indonesia', '1979', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('30a8c9409be883595484ac3d51390f2b3ea424fe', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', 'D1', 'BPLMG', 'Jakarta', 'Indonesia', '1981', 'GEOFISIKA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('7d6d1cdf8e0aec4881ba190e4b88232dc86b8def', '74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', 'S1', 'Universitas Kristen Cipta Wacana', 'Malang', 'Indonesia', '2004', 'Ilmu Hukum', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('32a55058faaffd8381320a0ebb081345c31a2b04', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', 'SD', 'SDN PANCA SWANA', 'Walikukun', 'Indonesia', '1972', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('8b2d8033885b3aec46244e8ae5fce304a8db4b0b', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', 'SMP', 'SMP N WALIKUKUN', 'Walikukun', 'Indonesia', '1975', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('e1c52e0fc81d34803e3e20176357249b1f154c86', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', 'SMA', 'SMPPN BANYUWANGI', 'Banyuwangi', 'Indonesia', '1979', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('c7ffad16c49769cdd10c2610a84e0ecaa772661e', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', 'D3', 'AMG', 'Jakarta', 'Indonesia', '1983', 'METEOROLOGI', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('7b0898036986a826a58360daad593b21da5e9635', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', 'S1', 'Universitas Indonesia', 'Jakarta', 'Indonesia', '1998', 'GEOGRAFI', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('67b01538fa99922f5ae5850efc40b1cda1c10d9d', '97ff0c7ed14c9fea149eb17c3031642d4ae94106', 'S2', 'STIE YAPPAN', 'Jakarta', 'Indonesia', '2011', 'ADMINISTRASI PUBLIK', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('67c4650a1d70a318d7f0d12cdadf27fae15b859a', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', 'SD', 'SDN KERNOLONG', 'Jakarta', 'Indonesia', '1972', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('062b8c47141ff21839b7df49afa9c51f3cd38e1b', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', 'SMP', 'SMPN 42', 'Jakarta', 'Indonesia', '1975', '-', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('3371784880911a394a2a11fcf61bdacb031ae5c1', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', 'SMA', 'SMAN 10', 'Jakarta', 'Indonesia', '1979', 'IPA', '-')
<query>INSERT INTO auditor_pendidikan VALUES ('c73384c7eb187d9a94827b944b5785f60fa310df', '6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', 'S1', 'STIA-LAN RI', 'Jakarta', 'Indonesia', '1989', 'ADMINISTRASI NEGARA', '-')
<query>DROP TABLE IF EXISTS backup_restore
<query>CREATE TABLE `backup_restore` (
  `backup_restore_id` varchar(50) NOT NULL,
  `backup_restore_id_user` varchar(50) NOT NULL,
  `backup_date` int(11) NOT NULL,
  `restore_date` int(11) DEFAULT NULL,
  `backup_restore_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=restore',
  PRIMARY KEY (`backup_restore_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO backup_restore VALUES ('6b80c27945717398e2bb26f645b7f76c25baac66', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498017063', '', '0')
<query>DROP TABLE IF EXISTS finding_internal
<query>CREATE TABLE `finding_internal` (
  `finding_id` varchar(50) NOT NULL,
  `finding_assign_id` varchar(50) NOT NULL,
  `finding_auditee_id` varchar(50) NOT NULL,
  `finding_kka_id` varchar(50) NOT NULL,
  `finding_no` varchar(20) NOT NULL,
  `finding_type_id` varchar(50) NOT NULL,
  `finding_sub_id` varchar(50) NOT NULL,
  `finding_jenis_id` varchar(50) NOT NULL,
  `finding_penyebab_id` varchar(50) NOT NULL,
  `finding_judul` varchar(500) NOT NULL,
  `finding_date` int(11) NOT NULL,
  `finding_kondisi` longblob NOT NULL,
  `finding_kriteria` longblob NOT NULL,
  `finding_sebab` longblob NOT NULL,
  `finding_akibat` longblob NOT NULL,
  `finding_nilai` varchar(50) NOT NULL,
  `finding_tanggapan_auditee` longblob NOT NULL,
  `finding_tanggapan_auditor` longblob NOT NULL,
  `finding_attachment` varchar(255) NOT NULL,
  `finding_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=ajukan, 2=approve_katim, 3=approvedanis, 4=approvedaltum, 5=tolakkatim, 6=tolakdalnis, 7=tolakdaltum, 8=finis',
  PRIMARY KEY (`finding_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO finding_internal VALUES ('53f938d2e6a61f1bd47451bfda84900086566f02', 'a51852730078d23e3b48bf5972885364a11b17b8', '89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', '4fb99972d03d6ea623c22b797177e2e0639d4e62', '01', '764275e74b69abcec96c2ac76140f7da774249f7', 'e950a0afcb65d466058c5ee19e5adb0646c9769f', 'ab4479b9627e5e983c20306c0f61c2367255fac1', 'a3e829db763976063f2f84d10b6a95686359d2cc', 'Temuan 1', '1493654400', '<p>kondisi</p>', '<p>kriteria</p>', '<p>sebab</p>', '<p>akibat</p>', '1000000', '<p>tanggapan auditee</p>', '<p>tanggapan auditor</p>', '', '0')
<query>DROP TABLE IF EXISTS finding_internal_comment
<query>CREATE TABLE `finding_internal_comment` (
  `find_comment_id` varchar(50) NOT NULL,
  `find_comment_find_id` varchar(50) NOT NULL,
  `find_comment_desc` varchar(255) NOT NULL,
  `find_comment_user_id` varchar(50) NOT NULL,
  `find_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`find_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS kertas_kerja
<query>CREATE TABLE `kertas_kerja` (
  `kertas_kerja_id` varchar(50) NOT NULL,
  `kertas_kerja_id_program` varchar(50) NOT NULL,
  `kertas_kerja_no` varchar(20) NOT NULL,
  `kertas_kerja_desc` longblob NOT NULL,
  `kertas_kerja_hasil` longblob NOT NULL,
  `kertas_kerja_kesimpulan` longblob NOT NULL,
  `kertas_kerja_attach` varchar(100) NOT NULL,
  `kertas_kerja_date` int(11) NOT NULL,
  `kertas_kerja_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=ajukan, 2=approve_katim, 3=approvedanis, 4=approvedaltum, 5=tolakkatim, 6=tolakdalnis, 7=tolakdaltum',
  PRIMARY KEY (`kertas_kerja_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO kertas_kerja VALUES ('b13f78684cf218c66602d52e0af87da107daca46', 'c72fc7e636ae05fd2f7155090378ca7a427c335f', '001', '<p>uraian KKA</p>', '', '<p>kesimpulan 1</p>', 'Pindah ke table kka_attach', '1493654400', '1')
<query>INSERT INTO kertas_kerja VALUES ('4fb99972d03d6ea623c22b797177e2e0639d4e62', 'c72fc7e636ae05fd2f7155090378ca7a427c335f', '001', '<p>uraian&nbsp;</p>', '', '<p>kesimpulan KKA</p>', 'Pindah ke table kka_attach', '1495987200', '2')
<query>DROP TABLE IF EXISTS kertas_kerja_attachment
<query>CREATE TABLE `kertas_kerja_attachment` (
  `kka_attach_id` varchar(50) NOT NULL,
  `kka_attach_kka_id` varchar(50) NOT NULL,
  `kka_attach_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`kka_attach_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO kertas_kerja_attachment VALUES ('52854ad7af9d4602cf4927885faecfbb0a524e0b', '4fb99972d03d6ea623c22b797177e2e0639d4e62', 'PKA _Audit Rinci KEUANGAN.docx')
<query>DROP TABLE IF EXISTS kertas_kerja_comment
<query>CREATE TABLE `kertas_kerja_comment` (
  `kertas_kerja_comment_id` varchar(50) NOT NULL,
  `kertas_kerja_comment_kka_id` varchar(50) NOT NULL,
  `kertas_kerja_comment_desc` varchar(255) NOT NULL,
  `kertas_kerja_comment_user_id` varchar(50) NOT NULL,
  `kertas_kerja_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`kertas_kerja_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO kertas_kerja_comment VALUES ('0520745f170b4986c6da8ff91055fd218aad16ae', '4fb99972d03d6ea623c22b797177e2e0639d4e62', 'komentar', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496038150')
<query>INSERT INTO kertas_kerja_comment VALUES ('c6d96f9d95bee5dc74efa66544b2326861ea7426', '4fb99972d03d6ea623c22b797177e2e0639d4e62', 'komentar dari pengendali teknis', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496038208')
<query>INSERT INTO kertas_kerja_comment VALUES ('eb832c1166803de8d4351cf2d9651696659471df', 'b13f78684cf218c66602d52e0af87da107daca46', 'dafa', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496127423')
<query>DROP TABLE IF EXISTS log_activity
<query>CREATE TABLE `log_activity` (
  `log_activity_id` varchar(50) NOT NULL,
  `log_activity_id_user` varchar(50) NOT NULL,
  `log_activity_date` int(11) NOT NULL DEFAULT '0',
  `log_activity_action` text NOT NULL,
  `log_activity_ip` varchar(10) NOT NULL,
  `log_status` varchar(50) NOT NULL,
  PRIMARY KEY (`log_activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO log_activity VALUES ('024340095a04a5e8c10b52ccd6c7821fc275eb21', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495182593', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d232983c1fb4e8a2378ab563cddf84e5dad386f4', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495182677', 'Menghapus Parameter Menu ID 24', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('643077b58488e224baecf62bac8bdf3589489776', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495182761', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d3a1cc27ea532e11bb76a9e21fba9325c83f1899', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495182774', 'User Logout', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e4167e6b6ce923c7000b818f9487debdc769c93b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495182784', 'User Logout', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4a9d062ce554b28e9726213cf6d09e2fab2b69d7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495182832', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fcb0ff19d89c180fc4af81fdff6b679004abc7bf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183431', 'Menghapus Parameter Jabatan Auditor ID 23ddda7bef0263d7f67c7b1296c11168e72d7555', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('27dfc3d25ebb742a2aae9e3c073862dc30fd2139', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183445', 'Mengubah Parameter Jabatan Auditor dengan ID ca4d3c424bd05544c7a145bd2af6cef0a08e92e4', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1587e3eab44a7b2e6bac25c301b7bc0109de18f3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183522', 'Menghapus Parameter Jabatan Auditor ID d0f4aa0437d8e3360b2faa99abe83c18f8d72c19', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e59463287ba5290ce8bfaea3b8aecbf31fa3c9cb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183543', 'Mengubah Parameter Jabatan Auditor dengan ID c3421e504177579e8f6bf9de290da00dab1b4a35', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e3953fcee68059c391d5f9ad5fff6b93ff6c8115', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183556', 'Mengubah Parameter Jabatan Auditor dengan ID 142eda73208917765e2bd4c8357e86fbeb00390d', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('aacfc57a67c30a77b5e3a24907fa0608f05ffdd0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183573', 'Mengubah Parameter Jabatan Auditor dengan ID ac0f7970492000d14b9d4ae01ef588083663e6d7', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d86614dadc477eebdb973a79b9939bb6ef8c10d0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183633', 'Mengubah Parameter Jabatan Auditor dengan ID 77e5755edba4cbf18d9040f222310a853f4388d8', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('da4ee976fac01751233daa09772a92174c5ce483', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183648', 'Mengubah Parameter Jabatan Auditor dengan ID b9610baa4575c677951c04e0050ac0e9a5676594', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d36a095520068bbbd0ab913b2c5f40878bb901f0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183658', 'Mengubah Parameter Jabatan Auditor dengan ID 1fd86fc0cde73981d66fa6642020f8ed8be256b4', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7ba72f1ebacb9ed050a7346a6793617253f0bfe1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183666', 'Mengubah Parameter Jabatan Auditor dengan ID fa8abbcc16ba9aef09fc10070a2329adbc422fb2', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4f2bbb08e5032ff5f1bf87c0f3e2d390c3a2b30e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183709', 'Mengubah Parameter Jabatan Auditor dengan ID b4dc6bbf8c6786337c430a9dba3557ab75fd5cca', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('91c00bef7df333b5ab34279aea426ba92a20519d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495183799', 'Menambah tes - tessss.Sebagai Auditor', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fee0eec35422974a7173ba0997fd35bcc74aca0a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495184213', 'Menambah Parameter Tipe Audit Audit Keuangan', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d527dc3b0124c4cc1841ecf33f08c8e79cce5d46', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495184233', 'Menambah Parameter Tipe Audit Audit PBJ', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bff8ecae16756fe43d1e7c529dfe7bed76eab797', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495184297', 'Menambah Parameter Tipe Audit Audit Operasional', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e6f55ccb1e5a1330c6bd6948295d9f042cec5077', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495184347', 'Mengubah Data User ID 23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0123cbecad961fcb33c08f4f557a0f905ab2086b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495187131', 'User Login', '110.138.13', 'Sukses')
<query>INSERT INTO log_activity VALUES ('706da60132b432e5c579bf71837547510a8c6684', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495187146', 'User Logout', '110.138.13', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e0dd72f23043eefe18069efee632158b53718192', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495187611', 'User Login', '110.138.13', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c36329ed658b4fcb191bb7f5ef6dd9434918c013', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495191597', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f62984a587b5a5b5f38da8be9e058acbf51cce82', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495191647', 'Mengubah Menu dengan ID 4', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4b7e5413ca6d6f440110bf4809afd1be4a0131e2', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495191664', 'Mengubah Menu dengan ID 14', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5a2e413ad400b91e9743c5632584dd461cd9af92', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495191673', 'Mengubah Menu dengan ID 15', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6c1a3921337a023c45796ba08d18b42e912ab5c0', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495191689', 'User Logout', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2e9c99f8ac2f4c6ab161ea2694f21da0d2db6aa7', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495284499', 'User Login', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('88859217ae6d7b1ea817e935d09235329d512b85', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495285432', 'Menambah Auditee 001 - Tes Auditee', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bfbde889c7c76e2fa4dd791b8c44aac01834b56c', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495285531', 'Menambah Plan ID 0813d8ebddbe07fce6236e5d8c251df2f91124f9', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8f814a1e29fd395f06574a5f2aa8246b422debb4', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495285855', 'Menambah anggota dengan id c7aeef66a5264f12c6c4af869d17f48ae6335bee pada planning id 0813d8ebddbe07fce6236e5d8c251df2f91124f9', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8859bfdbbed42e807007fd272d6b815968cf9362', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495288036', 'Mengubah Status Penugasan Audit dengan ID a51852730078d23e3b48bf5972885364a11b17b8 menjadi 1', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7f7695975d008e5c484202a4d9d426aea29276de', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495288036', 'Mengomentari Penugasan dengan ID a51852730078d23e3b48bf5972885364a11b17b8', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('59e72e5fb2becc28fdc1e7118036a8e7703cab66', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495288068', 'Mengubah Status Penugasan Audit dengan ID a51852730078d23e3b48bf5972885364a11b17b8 menjadi 2', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cddd88713c92717142bba9610d6b9f1927e8a5b7', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495288068', 'Mengomentari Penugasan dengan ID a51852730078d23e3b48bf5972885364a11b17b8', '180.252.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d01b2f828d434ba6cec50e475b2819b1a6d70f60', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495309169', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('52c8728e13190f4d7359ccb1e9d8d5635d5cef45', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495309494', 'Mengubah Menu dengan ID 16', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8021ccfe25841d6343dd0a8ef45ebf89b1b3a159', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495318976', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f03f216c18d87f7f65b75e607581c65a2498d73', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495320272', 'Menambah 777 - test2.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2ff0ca64a2cc88af411b9170480c60a8f6c5d321', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495321561', 'User Logout', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('69cc488a9854b3b267024b2c88bb3a51b5d4e98b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495417013', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2f8b03d777edc50740d75034612ab5eaec2f6236', '', '1495419274', 'Menambah 196101141988032001 - DARWAHYUNIATI, SH, MH.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4cbd269ec8e0ed89c772fd8ed632aa42c20fa027', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495419356', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('520073bcec36d126c46d34028305d43adda2bccc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495419691', 'Mengubah Data Auditor dengan ID 859407f1e00a3dbd921ca7a108640e65df9a5878', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1e2b1fe5b40a7155568bac85e42d3cedb62f55a8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495419848', 'Menambah 197911052006041001 - BIMA ENDARYONO, S.Sos.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0547590293922eae7e59db1355fe19e8b42a9a6c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495419975', 'Menambah 196005211983031001 - Drs. KASABARNO, M.Si.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('56413a8aa713fe63909bc1f28d8f3454ce19681e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495420358', 'Menambah 195906181980031001 - Drs. WIDARNO, MBA.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2721016ac50f67b382827a8952c49753f705818a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495420364', 'Menambah 195908141980031002 - Drs. KHAERUDIN.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('730de0ed05ab9f321545a90177bfdd92c5f52f8e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495420375', 'Menambah 196012061985031001 - Drs. NUR HIMAWAN, M.Si.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('56ae0bf30af424338d03ba5d0f520aacb36991c1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495420800', 'Menambah 195709291979101001 - Drs. MULYONO.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f62088a958f7cd498eeb43d05c47694313d34f6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495420808', 'Menambah 196002251983031001 - BAMBANG WAHYUDI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c053b6e7038edeadc3f410800311c93ec90d2973', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495420817', 'Menambah 196202241982031001 - Ir. PUTUT ADIWASITO, M.Si.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('aa6a1bfd02012147f050c01197e5b6a5874987dc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495421595', 'Menambah 196112201983031001 - MOCHAMMAD SAMSUL NIZAR, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('71b02a8e06acede49e8fca834abcdd972ff15f5e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495421600', 'Menambah 195904161979101001 - Drs. SUBAGYO SARDI.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('30d2a9494490f1aba09fc840798d989034be62f8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495421611', 'Menambah 196006151980021001 - DARMINI SUHAMAN, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2fff9a2ba9f7ef4d46675b33d5381a5be89ff7e7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495425398', 'User Login', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5ff8a6ad76e9e5e04d75ffb57b1a8d956ad7c47a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495425487', 'Menambah Auditee 2000 - SEKRETARIAT UTAMA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c5a997894d1bba28f9fd7f8bc4e1d87b526c8e55', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495425966', 'User Login', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('73a80cde49e8274dfa72446302493491a7b4b104', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495426563', 'Menghapus Auditor ID ad7a0edadec8f623975c0078fa4a292b5b4ab25d', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6a93889307d9671701202fc902237b2e2623d7ba', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495426618', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5186c17d7913814def7447627cb332f98080dd02', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495426737', 'Mengubah Data Auditor dengan ID e77f0a9d990ae8087e137b974c199570f6dac43a', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9a075b484f3337efd6b928166e1b2af07da9213d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495426742', 'User Logout', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4df1ad6532e4d540ff71b2fcaba0cc81d0e02413', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495426749', 'User Login', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('97fe9276909137c68c7674a58122634fd9d35690', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495426766', 'Mengubah Data User ID 23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ea3a70fbc6490558da50c408f31fa864815469ff', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495426795', 'Mengubah Anggota id c7aeef66a5264f12c6c4af869d17f48ae6335bee', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d75f9da6eed95b3a426dc6a832537d3880678543', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495426832', 'Menambah Program Audit ID c72fc7e636ae05fd2f7155090378ca7a427c335f', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7bda26463578f8a33b998bfe58a3accde29fecf6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427129', 'Menambah 195910011983032001 - NURNIAWATY MAKSUDI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1531149d4fcf4563504f9daa6e19b8a566da56f7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427134', 'Menambah 196008221981031002 - DJOKO WAHONO, SH.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('415f256f447854658b3f9598b69a99dcded2d30a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427140', 'Menambah 196310201984031001 - TUKIJO, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3e9d7e7bfa3d6680487a144b89b56fa803278857', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495427435', 'Mengubah Parameter SBU Rinci dengan ID fd0b33dd2c3e5c26368e52ad1fa5a9379924cd57', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f4f3b8b987743164f7da51b60a55fb83827d9f69', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495427444', 'Mengubah Parameter SBU Rinci dengan ID 0ef0ca6577bcda8856e93c8e3c7fada4c4e0dd84', '202.80.219', 'Sukses')
<query>INSERT INTO log_activity VALUES ('50a836b4f0e0db7f565656200096abc6c3d98509', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427632', 'Menambah 196104011982031004 - LUKMAN HAKIM, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0ae06752baa2e682c27f17e41f483b137491df41', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427638', 'Menambah 195909261981031003 - SOFYAN SAURI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('65ee3fa2eab0f8d061c47b0248b96973ba15b9cf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427645', 'Menambah 196501221989011001 - MON ELFIANSYAH, S.Kom.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('05fe2c1f380c0f6fc65690c6dabdb2e8cbdf89ae', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427961', 'Mengubah Data Auditor dengan ID c2f6d86f82ce263ae87588175f6bd295e1712b81', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('58eecb5cc9d096406e870059353f9365382859fc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495427997', 'Mengubah Data Auditor dengan ID 74448f47167cba1ca8fa1c0461924d2ee6b0ec4a', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('03dfd8a2df4f644a0e84396507a1dbb83cb48eb4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495428020', 'Mengubah Data Auditor dengan ID 3b5059a4bd01603a5f4903f5d5929f6e7878aea0', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c6e0e01ea81140aa3b555e5fff8bca0243343cc3', '', '1495428738', 'Menambah Auditee 2100 - BIRO PERENCANAAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('35f2cb1411c085de8717495e24cecebd3a75c1cb', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495428753', 'User Login', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eca243eef91c6fc9c2522004804a4c6ff4789ddf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495428841', 'Mengubah Data Auditee dengan ID 5c730b7e75cf5af124b8cdae7adfbda7af9d8d7e', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c44113a620faaf0b906bbc747859678d0c5fd536', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495428887', 'Menambah Auditee 2110 - BAGIAN RENCANA DAN TARIF', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('27d55624d94497b830bfa382edcef0eaa7c5b3e8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495428926', 'Menambah Auditee 2120 - BAGIAN PROGRAM DAN PENYSUNAN ANGGARAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fcf43ea5b504198c483661eb6841e4db11411bcc', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495428967', 'Menambah Auditee 2130 - BAGIAN PEMANTAUAN DAN EVALUASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c5edf46472516974955ae8d78c8ab3cec4dfcf3f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495429016', 'Menambah Auditee 2200 - BIRO HUKUM DAN ORGANISASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3f0ba18d432c9eccbe5a74c54c503b3d29ddd31d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433137', 'User Login', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9fff39b7d762b6ef8bcaaf62cb146e77d3659e1e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433189', 'Menambah Auditee 2210 - BAGIAN PERATURAN PERUNDANG-UNDANGAN DAN PERTIMBANGAN HUKUM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e3402df25967dab520e00f00b3182499959ece62', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433212', 'Menambah Auditee 2220 - BAGIAN KERJASAMA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a45f82f32131e6fecf12e2fc7d2fb83e6a709b25', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433269', 'Menambah Auditee 2230 - BAGIAN ORGANISASI DAN TATALAKSANA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('06bf18ba7f2a8468c1d2f7c24927af417c2b07e3', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433294', 'Menambah Auditee 2240 - BAGIAN HUBUNGAN MASYARAKAT', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1b113098bccc1d248c4657d27631e651efd6ef60', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433329', 'Menambah Auditee 2300 - BIRO UMUM DAN SUMBER DAYA MANUSIA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f5d833b03de40a549880921b366deb03519a28a7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433360', 'Menambah Auditee 2310 - BAGIAN SUMBER DAYA MANUSIA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('753a49e5ed76fbb4287a0bed37cbc1a111709172', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433389', 'Menambah Auditee 2320 - BAGIAN KEUANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('371b8ae7a799e35756c5f6feb92eb967752c55a5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433410', 'Mengubah Data Auditee dengan ID c08ac92e7adeae8163a6d9e57452fdf46f35737f', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4f3bf2b2e20cb4cdd157b7999d6a0be0e355d32e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433441', 'Menambah Auditee 2330 - BAGIAN PERLENGKAPAN DAN BARANG MILIK NEGARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ad6f403618ec922121f3014f44edbb2698dab12c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433472', 'Menambah Auditee 2340 - BAGIAN TATA USAHA DAN PROTOKOL', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c0b7d7024d5fe7e3b4730d44578cb2defbc89ac7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433683', 'Menambah Auditee 2111 - SUB BAGIAN RENCANA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4643eee5f63c674fac8593c3fcc526094afc95b2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433706', 'Menambah Auditee 2112 - SUB BAGIAN TARIF', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2ccb2c3421b9beac77929a1cbe3e03d45da9d06b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433743', 'Menambah Auditee 2113 - SUB BAGIAN PINJAMAN/HIBAH LUAR NEGERI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('032543f84feaae84de657aa715aee9d8c5c31f35', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433819', 'Menambah Auditee 2121 - SUB BAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN I', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b7b9192120e1a1e3157a9e3409ff4e42bf3d4af4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433852', 'Menambah Auditee 2122 - SUB BAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN II', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b00a6506a7c4f5dd78363d8acd072affc80e2bde', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433881', 'Menambah Auditee 2123 - SUB BAGIAN PROGRAM DAN PENYUSUNAN ANGGARAN III', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('43027ea7825d91d5aa82a80e81b2112dccb474b7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495433960', 'Menambah Auditee 2131 - SUB BAGIAN PEMANTAUAN DAN EVALUASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eb969d0f628b75ac766935d93a1123ee862cbb59', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434002', 'Mengubah Data Auditee dengan ID 9cabb164a9375642e01bc72ea1af037d2d07e4b8', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d2d560ea581c72bd5ac931ea0e07a636705fb86c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434029', 'Menambah Auditee 2132 - SUB BAGIAN PEMANTAUAN DAN EVALUASI II', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b31f180a29693e38f0daa1b6cee127fe81ccc684', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434071', 'Menambah Auditee 2133 - SUB BAGIAN PEMANTAUAN DAN EVALUASI III', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('29817e345ac35e05b0a99788d8c83c0911469cfb', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434129', 'Menambah Auditee 2211 - SUB BAGIAN PERATURAN PERUNDANG-UNDANGAN I', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('07344806b58e8154e822cd5e6a89444a88ad3f05', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434167', 'Menambah Auditee 2212 - SUB BAGIAN PERATURAN PERUNDANG-UNDANGAN II', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a2d2e2736b18068fe43fb23600c70219c5078464', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434249', 'Menambah Auditee 2213 - SUB BAGIAN PERTIMBANGAN DAN INFORMASI HUKUM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f51ac8d5f8691f4edb97f8300a517a858f61b1aa', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434282', 'Menambah Auditee 2221 - SUB BAGIAN KERJASAMA LUAR NEGERI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bad16e3bdc5d023b98fde4723093c42fcebfd266', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434315', 'Menambah Auditee 2222 - SUB BAGIAN KERJASAMA DALAM NEGERI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('081edc11b8b57c973bc01dbdbfb961f3c8c96638', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434365', 'Menambah Auditee 2231 - SUB BAGIAN ORGANISASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f76e3cec7b94854767e19457aecb9ceb8a7efd5b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434390', 'Menambah Auditee 2232 - SUB BAGIAN TATA LAKSANA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('68c68fcbe3d8e749d0f6f204dafdfce21fba97d5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434423', 'Menambah Auditee 2241 - SUB BAGIAN PUBLIKASI DAN DOKUMENTASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1b4c9f482c6d3f133899d4faa4d9bf8cee344cff', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434457', 'Menambah Auditee 2242 - SUB BAGIAN HUBUNGAN PERS DAN MEDIA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ca197894d4b5d55e1d0abdd79ca71790df24d9c7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434557', 'Menambah Auditee 2311 - SUB BAGIAN PERENCANAAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('590753c386c085936c71254cf50b8b1bcda14ba2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434645', 'Menambah Auditee 2312 - SUB BAGIAN MANAJEMEN DAN EVALUASI KINERJA DAN KESEJAHTERAAN SUMBER DAYA MANUSIA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('496eac8304ec5d4d0102c7f5dc95e7aeacdb96b1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434707', 'Menambah Auditee 2313 - SUBBAGIAN MUTASI DAN PENGELOLAAN JABATAN FUNGSIONAL SUMBER DAYA MANUSIA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b72e8511d1078ce0945b986834863e8de752a092', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434761', 'Menambah Auditee 2321 - SUBBAGIAN PERBENDAHARAAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('29c1e1d305d734395200263bf5bc1543ab4a06c8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434797', 'Menambah Auditee 2322 - SUBBAGIAN GAJI DAN PENERIMAAN NEGRA BUKAN PAJAK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('73e23102855a579bef64d751daad4189ad6124be', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434834', 'Menambah Auditee 2323 - SUBBAGIAN AKUNTANSI DAN PELAPORAN KEUANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('62ff84d58e06d3a1231cb7c53b6e9fb7e43227ce', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434865', 'Menambah Auditee 2331 - SUBBAGIAN PENGADAAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b0a1fab16a5e7a6a7c47ba5d6ea550441a3b50ca', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434900', 'Menambah Auditee 2332 - SUBBAGIAN PENGELOLAAN BARANG MILIK NEGARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2f21f0a9449555ac59f5575e4b02ff9d83faadd4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434937', 'Menambah Auditee 2333 - SUBBAGIAN PEMELIHARAAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7e3595896af26465259c0a170af489a8b87a9dd6', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495434971', 'Menambah Auditee 2341 - SUBBAGIAN PERSURATAN DAN ARSIP', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e4914d42da757f7fefb228749b2119d9794f8926', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435009', 'Menambah Auditee 2342 - SUBBAGIAN RUMAH TANGGA DAN PROTOKOL', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8ebe3bf30ccc862b639892b1444139caf32b90e5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435043', 'Menambah Auditee 2343 - UNIT TATA USAHA PIMPINAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('32ecb7ac7309a5f673b011992eb3a1a698991c11', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435098', 'Menambah Auditee 3000 - DEPUTI SIDANG METEOROLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('58b8f1dc9685a55f40c34b3283f26d2385ffd80d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435130', 'Menambah Auditee 3100 - PUSAT METEOROLOGI PENERBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e15c65cebc8f459d077efc3e38e392fc978dce6c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435196', 'Menambah Auditee 3110 - BIDANG MANAJEMEN OBSERVASI METEOROLOGI PENERBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('501f10fb82d19c5b3e812199832f4f5f88ca550c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435227', 'Menambah Auditee 3120 - BIDANG MANAJEMEN OPERASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('55891caae9c398758a5fb9e51175f7bd40e58c00', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435264', 'Menambah Auditee 3130 - BIDANG INFORMASI METEOROLOGI PENERBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f3dc52e881243e50a51fd557c682975dbb31bb69', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435294', 'Menambah Auditee 3200 - PUSAT METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('020d1bdfabc31032e317eff2f67a324ae93f0f77', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435335', 'Menambah Auditee 3210 - BIDANG MANAJEMEN METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cb60eb8862d7be080094c3b50fc5114860cd669a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435365', 'Menambah Auditee 3220 - BIDANG INFORMASI METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d4fcc9c141be885d264c7b36a49ec0d0548bff3f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435388', 'Menambah Auditee 3300 - PUSAT METEOROLOGI PUBLIK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('aaeaa18aa0fd90322784fa055075006cb67a9468', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435419', 'Menambah Auditee 3320 - BIDANG LAYANAN INFORMASI CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f815848294443183530660fe09407e01ac7e1d27', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435463', 'Menambah Auditee 3330 - BIDANG PREDIKSI DAN PERINGATAN DINI CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4b54827c3a5b06e434fc875b72d84423dc777b2a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435509', 'Menambah Auditee 3340 - BIDANG PENGELOLAAN CITRA INDERAJA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5bd7c08ebb8de9ff62bc5cfbbf90ea25cea52354', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435571', 'Menambah Auditee 3111 - SUBBIDANG MANAJEMEN OBSERVASI METEOROLOGI PERMUKAAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c7d64ad9f6c16aae97ea4fd52aae12a3f6cb3486', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435610', 'Menambah Auditee 3112 - SUBBIDANG MANAJEMEN OBSERVASI UDARA ATAS', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cc4d71772438df842321a3e34fd29f0ebd909a69', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435650', 'Menambah Auditee 3121 - SUBBIDANG MANAJEMEN OPERASI METEOROLOGI PENERBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('60ffdfdd162beb69e7231f0fe34ee223e3434be8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435687', 'Menambah Auditee 3122 - SUBBIDANG MANAJEMEN OPERASI METEOROLOGI PUBLIK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ac1343853a0c23cd53e41d563c6722726e726f38', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435732', 'Menambah Auditee 3131 - SUBBIDANG LAYANAN INFORMASI METEOROLOGI PENERBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d0f784fd26ad72bcb20b5c7f6ed3664b269ff3b0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435774', 'Menambah Auditee 3132 - SUBBIDANG DISEMINASI INFORMASI METEOROLOGI PENERBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0fdd421866b065d0b2a0f4e6b7f067269b432d92', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435822', 'Menambah Auditee 3211 - SUBBIDANG MANAJEMEN OBSERVASI METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('890c86a2a60e800cdc5cbf16904cee9da17d95ed', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435865', 'Menambah Auditee 3212 - SUBBIDANG MANAJEMEN OPERASI METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7a620b0b22df7446a0df4a311158c3bbcb0be639', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435908', 'Menambah Auditee 3221 - SUBBIDANG ANALISIS DAN PREDIKSI METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c25107a17ce809477bdb5efeac3a6bbc5867d210', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435950', 'Menambah Auditee 3222 - SUBBIDANG LAYANAN INFORMASI METEOROLOGI MARITIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ba91c5a8ad2b2cf9914ebcf58e4ffa2b9705a2fa', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495435992', 'Menambah Auditee 3311 - SUBBIDANG PRODUKSI INFORMASI CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5c6e680e9cd4e7311b9dd262a80866e4a2f480dd', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436024', 'Menambah Auditee 3312 - SUBIDANG DISEMINASI INFORMASI CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3545ec785781f39857b7d3ca719acb6da4ad94f4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436054', 'Menambah Auditee 3321 - SUBBIDANG PREDIKSI CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('87c2b653097c70e16e163b9bc08a8626d8e213f5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436081', 'Menambah Auditee 3322 - SUBBIANG PERINGATAN CUACA DINI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f303592ffb21aba46e5f074aded23d74952fb7b1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436143', 'Menambah Auditee 3331 - SUBBIDANG PENGELOLAAN CITRA RADAR CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3f714bac306a3fe2c6cdaf5131d93adcd259626f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436181', 'Menambah Auditee 3332 - SUBBIDANG PENGELOLAAN CITRA SATELIT CUACA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0cc6786561ab7a9de6f659cd0bef08ffc72da764', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436212', 'Menambah Auditee 4000 - DEPUTI BIDANG KLIMATOLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6fd64d824b6cb118a79eb71b3ea2f43096e6f8b3', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436241', 'Menambah Auditee 4100 - PUSAT INFORMASI PERUBAHAN IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('db39a9b25277867c2360be742adcb7a9dac62c4e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436270', 'Menambah Auditee 4110 - BIDANG ANALISIS PERUBAHAN IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f966f7d182ad60ca3c9a52c3334ac15b7bc1c6e4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436307', 'Menambah Auditee 4120 - BIDANG ANALISIS VARIABILITAS IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1f86f1166583b84df8c11d9fefe982b5bab0f8f7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436341', 'Menambah Auditee 4130 - BIDANG MANAJEMEN OPERASI IKLIM DAN KUALITAS UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e04428c76ba85033bca47697bd917737cf49e170', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436375', 'Menambah Auditee 4200 - PUSAT LAYANAN INFORMASI IKLIM TERAPAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c1be6484cf2ece3a5f0b7cfdd4e8c767427d9942', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436406', 'Menambah Auditee 4210 - BIDANG INFORMASI IKLIM TERAPAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a50c73f50a5860256aaa6f91bc080688aee6f7c1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436439', 'Menambah Auditee 4220 - BIDANG INFORMASI KUALITAS UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f6b88b04212f96c70d2f4121291aac18e2b48494', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436486', 'Menambah Auditee 4230 - BIDANG DISEMINASI INFORMASI IKLIM DAN KUALITAS UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dfe8c234193e1e990d08ab65cabbb56480616a9c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436550', 'Menambah Auditee 4111 - SUBBIDANG ANALISIS DAN PROYEKSI PERUBAHAN IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4429d858047760b63bf79413ce5985363aba8567', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436595', 'Menambah Auditee 4112 - SUBBIDANG ANALISIS KOMPOSISI KIMIA ATMOSFER', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f5693cf50d3030f7441d4849b4081b1fc6c92409', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436640', 'Menambah Auditee 4121 - SUBBIDANG ANALISIS DAN INFORMASI IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3ebc37a48b7ff82b89fce2b89ecef6dfa81b8da7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436674', 'Menambah Auditee 4122 - SUBBIDANG PERINGATAN DINI IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('434bff9153ddc9bc64ba2dc34b438581d7328325', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436708', 'Menambah Auditee 4131 - SUBBIDANG MANAJEMEN OPERASI IKLIM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1c82ba30567195e3891892cc6bb3b4a2409b0b7e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436747', 'Menambah Auditee 4132 - SUBBIDANG MANAJEMEN OPERASI KUALITAS UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ebc4e7df2eb795a73ec82cbf57a826a707160524', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436805', 'Menambah Auditee 4211 - SUBBIDANG INFORMASI IKLIM LINGKUNGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2081aefb634f524f49329c2d0f7554e29b0d308a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436845', 'Menambah Auditee 4212 - SUBBIDANG INFORMASI IKLIM INFRASTRUKTUR', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e3c9c38aa9161f28dabcbf280efd282491845739', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436876', 'Menambah Auditee 4221 - SUBBIDANG INFORMASI GAS RUMAH TANGGA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5abf661197242f5a4443faaea5b01765cd1e612d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495436974', 'Menambah Auditee 4222 - SUBBIDANG INFORMASI PENCEMARAN UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4290ff9efb3fe0717d5eec6a04dba4b1aa2111a4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437030', 'Menambah Auditee 4231 - SUBBIDANG PRODUKSI INFORMASI IKLIM DAN KUALITAS UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d151bf478f5245bbcba400042ca94f8f1db63577', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437216', 'Menambah Auditee 4232 - SUBBIDANG SISTEM INFORMASI IKLIM DAN KUALITAS UDARA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d5d2fbf067f4141875837480d86a2bb1548ba801', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437246', 'User Logout', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b04a94d7598cc5690273267376e7ba22509bc046', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437261', 'User Login', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1722c37e91bc29a126b4acd45f02119333d8442a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437300', 'Menambah Auditee 5000 - DEPUTI BIDANG GEOFISIKA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9073f23ff2628a1af91e92d4d0edcf6e59c22923', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437376', 'Mengubah Data Auditee dengan ID c19e3f42689ad662ae54a1d2017101969a8fe6c2', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eecb90dec0806301df7f417c261ad817a87792fc', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437447', 'Mengubah Data Auditee dengan ID f0b3b920b90a8c9899e7f9abbfe9715fb6f8214e', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9b9a82d288348fa471120e57a152679ccbb39a9e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437508', 'Menambah Auditee 5100 - PUSAT GEMPA BUMI DAN TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a5c56fe7056930cb275b0b9d872f931d166b216e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437545', 'Menambah Auditee 5110 - BIDANG INFORMASI GEMPA BUMI DAN PERINGATAN DINI TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7b1f23f723906c3ec9eb8dc54e7e6398aa0b41b5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495437558', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8ca83d38e4c066c254e02aa8bbb6f3d2468743d0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437580', 'Menambah Auditee 5120 - BIDANG MITIGASI GEMPA BUMI DAN TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ea531019800f68c17bb098498e151e14689da5c7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437630', 'Menambah Auditee 5130 - BIDANG MANAJEMEN  OPERASI GEMPA BUMI DAN TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7fbdd98d2bf88c02afa1df95dbca8c2a64c3f254', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437673', 'Menambah Auditee 5200 - BIDANG SEISMOLOGI TEKNIK, GEOFISIKA POTENSIAL DAN TANDA WAKTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8976be369e58b81873d9adf2018335cc53b5b60a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437707', 'Menambah Auditee 5210 - BIDANG SEISMOLOGI TEKNIK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9e3206693cf476abac2579a4b5e43d1a31807e47', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437755', 'Menambah Auditee 5220 - BIDANG GEOFISIKA POTENSIAL DAN TANDA WAKTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4235f2c0897d8b3a894b81f5d7fcc5c692071919', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437810', 'Menambah Auditee 5230 - BIDANG MANAJEMEN OPERASI SEISMOLOGI TEKNIK, GEOFISIKA POTENSIAL DAN TANDA WAKTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3c9a15ad6b79a1826fcb02e5c4e6e94dc097aa26', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437854', 'Menambah Auditee 5111 - SUBBIDANG INFORMASI GEMPA BUMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('aa5e7b77e3ffbc69d71c82ffbc052a5f118f9d87', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437897', 'Menambah Auditee 5112 - SUBBIDANG PERINGATAN DINI TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('66e33938a54badf4c998000f83d6070059ccda6a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437935', 'Menambah Auditee 5121 - SUBBIDANG MITIGASI GEMPA BUMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c1717a877a5f83f4f8d62646b09dfd13bbf13801', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495437968', 'Menambah Auditee 5122 - SUBBIDANG MITIGASI TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6fdbc064c910118945579b8ed6201bb25400f0d4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438008', 'Menambah Auditee 5131 - SUBBIDANG MANAJEMEN OPERASI GEMPA BUMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ed665934595429601e84fb1d635f101cf3e6a47b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438037', 'Menambah Auditee 5132 - SUBBIDANG MANAJEMEN OPERASI TSUNAMI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('99d624615b394b93655719074fe1f2a317c37268', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438118', 'Menambah Auditee 5211 - SUBBIDANG ANALISIS SEISMOLOGI TEKNIK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a1c241c322636d11d0ee8d32e7e1f0bfc34ef8c2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438156', 'Menambah Auditee 5212 - SUBBIDANG LAYANAN INFORMASI SEISMOLOGI TEKNIK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8cbd662e14f6f66f82f7147b8306cf6d78f82819', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438200', 'Menambah Auditee 5221 - SUBBIDANG ANALISIS GEOFISIKA POTENSIAL DAN TANDA WAKTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('33804b463363163ea3c3e7b36866e7d18ada6782', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438250', 'Menambah Auditee 5222 - SUBBIDANG LAYANAN INFORMASI GEOFISIKA POTENSIAL DAN TANDA WAKTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c9e0e8a7ef3620e62b5369ca6f67eb7a08ac2c51', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438313', 'Menambah 196908041990031002 - SAMSUL ARIFIN, S.Kom.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9d36cf3c3d49681a43f23e001e37974e2c10053f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438315', 'Menambah Auditee 5231 - SUBBIDANG MANAJEMEN OPERASI SEISMOLOGI TEKNIK', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4f31c698ebd2b248b535467be9b13d95dd0aa823', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438319', 'Menambah 197205271994031002 - IRWAN HADI, S.Sos.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6a77055720b6924353fd604d8cebd60a2691974d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438324', 'Menambah 196603151990032001 - WAHYUTI, S.E.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b5cb1feed3ad761a07f4ec992c126be1b18b6a86', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438370', 'Menambah Auditee 5232 - SUBBIDANG MANAJEMEN OPERASI GEOFISIKA POTENSIAL DAN TANDA WAKTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a56441731062ce89a04d88831d39bf6495119959', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438427', 'Menambah Auditee 6000 - DEPUTI BIDANG INSTRUMENTASI, KALIBRASI, REKAYASA, DAN JARINGAN KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5975fb075fc84e59febec759b93a46a18a016d44', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438474', 'Menambah Auditee 6100 - PUSAT INSTRUMENTASI, KALIBRASI, DAN REKAYASA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c92a17f8c7a261f6283ccde04448fb319db4ef1f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438521', 'Menambah Auditee 6110 - BIDANG INSTRUMENTASI, KALIBRASI, DAN REKAYASA, PERALATAN METEOROLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cf7a81f9b0dae4b83526af84479acdd49682ce70', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438562', 'Menambah Auditee 6120 - BIDANG INSTRUMENTASI, KALIBRASI, DAN REKAYASA PERALATAN KLIMATOLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('48931cf7bfb70cb639a49d2c83c135ddfca36b48', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438590', 'Mengubah Data Auditor dengan ID e77f0a9d990ae8087e137b974c199570f6dac43a', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c225d52c450199027a740edef6f8381d0e3bb024', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438606', 'Menambah Auditee 6130 - BIDANG INSTRUMENTASI, KALIBRASI, DAN REKAYASA PERALATAN GEOFISIKA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8d142acc6f063fd0af8e679569b7b1f6eb38531a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438634', 'Menambah Auditee 6200 - PUSAT DATABASE', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('af62b07296dc8fd9cd015a1219731c317f710a59', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438646', 'Mengubah Data Auditor dengan ID e1b5ec0fc6247d99196eba300bbdcaa98f786510', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cfbc67d840cb7e8c42bb820206acef560ea8ff48', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438666', 'Menambah Auditee 6210 - BIDANG MANAJEMEN DATABASE', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a6c21e6e94a55e2e4180f69b3b40a506feecf910', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438699', 'Menambah Auditee 6220 - BIDANG PENGEMBANGAN DATABASE', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5db00e69f30f94a6ed69db4a327459a6447358f7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438704', 'Mengubah Data Auditor dengan ID 9ca611db222a22298db1aa06a02564eebc25c3f1', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('00a67e7658837139bc8428637d88488808951e31', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438724', 'Menambah Auditee 6230 - BIDANG PEMELIHARAAN DATABASE', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3e771ce667693b03d708bef372a283cf365458f6', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438754', 'Menambah Auditee 6300 - PUSAT JARINGAN KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('02e5aa87f1ef179db27d626cdc6e9b3129a9fdb8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438756', 'Mengubah Data Auditor dengan ID 7d7f57c43d01fbfb696627fb5cf1281f3bb8d363', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('88348b1e36819700d59e4ccd5d808420caccb24a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438781', 'Mengubah Data Auditor dengan ID 859407f1e00a3dbd921ca7a108640e65df9a5878', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fb96593f0324ec08cb3b688fea5fe209b65e8f32', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438787', 'Menambah Auditee 6310 - BIDANG OPERASIONAL JARINGAN KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6cbfcec72804387b076ed90524b1864458943303', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495438798', 'Mengubah Data Auditor dengan ID 859407f1e00a3dbd921ca7a108640e65df9a5878', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('14130e2b72a71b02a3d7beb9fc6a571046eb9865', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438821', 'Menambah Auditee 6320 - BIDANG PENGEMBANGAN JARINGAN KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5267b9e8712f4b458bb522e7755be780d774f537', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438851', 'Menambah Auditee 6330 - BIDANG MANAJEMEN JARINGAN KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('09e13e9ceec5e08d7190fc4b7967c87575b2d801', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438921', 'Menambah Auditee 6111 - SUBBIDANG INSTRUMENTASI DAN REKAYASA PERALATAN METEOROLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7797277d24d53880a5e37cf71c23adee23cd0360', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495438964', 'Menambah Auditee 6112 - SUBBIDANG KALIBRASI PERALATAN METEOROLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('722e03f1928c1f4e50e4cdfd75e74a50867db07d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439006', 'Menambah Auditee 6121 - SUBBIDANG INSTRUMENTASI DAN REKAYASA PERALATAN KLIMATOLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6136590d942974168edb140e64e9758c60cb40d2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439037', 'Menambah Auditee 6122 - SUBBIDANG KALIBRASI PERALATAN KLIMATOLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('24a3e309fd9e7ab1cc93129117ab9e146de53ff8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439081', 'Menambah Auditee 6131 - SUBBIDANG INSTRUMENTASI DAN REKAYASA PERALATAN GEOFISIKA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6fc0f643f65c186f255eb05fdef057775dd6ed9c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439120', 'Menambah Auditee 6132 - SUBBIDANG KALIBRASI PERALATAN GEOFISIKA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8280a29381cb15844d4a37007ca39de2a286fb69', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439160', 'Menambah Auditee 6211 - SUBBIDANG MANAJEMEN DATABASE MKG', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b4c5d776055f0b632a2b45b5bf34dc287729aa77', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439190', 'Menambah Auditee 6212 - SUBBIDANG MANAJEMEN DATABASE UMUM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0b2598c49f95f204f3d877849ea3fa3d4a306ce2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439237', 'Menambah Auditee 6221 - SUBBIDANG PENGEMBANGAN DATABASE MKG', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a54010fcadacdcd332db42dd9a437cd4dd53eb43', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439266', 'Menambah Auditee 6222 - SUBBIDANG PENGEMBANGAN DATABASE UMUM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('836e33bb76fe2bab02731c89cc1db85ee663e55e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439301', 'Menambah Auditee 6231 - SUBBIDANG PEMELIHARAAN DATABASE MKG', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a9961f6e7e76328918c58cda81859e3d352008d8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439337', 'Menambah Auditee 6232 - SUBBIDANG PEMELIHARAAN DATABASE UMUM', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6e6d259f40b1ab3363041b6acda8c2485b119060', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439443', 'Menambah Auditee 6311 - SUBBIDANG OPERASIONAL TEKNOLOGI KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8211ae55530d66a92265fb11730b789d93b5c504', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439481', 'Menambah Auditee 6312 - SUBBIDANG OPERASIONAL TEKNOLOGI INFORMASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('af738080a03b8f5f3924f22bfe392055988ef8fd', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439520', 'Menambah Auditee 6321 - SUBBIDANG PENGEMBANGAN TEKNOLOGI KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b92ea5186de42c63c782c344748b66ff98b6a81e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439554', 'Menambah Auditee 6322 - SUBBIDANG PENGEMBANGAN TEKNOLOGI INFORMASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4b6bf38493f9c09549f98fbcb64859d2fd04b6c0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439593', 'Menambah Auditee 6331 - SUBBIDANG MANAJEMEN TEKNOLOGI KOMUNIKASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f3c473e76543dd8e37062688069cdf5bb115e490', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439630', 'Menambah Auditee 6332 - SUBBIDANG MANAJEMEN TEKNOLOGI INFORMASI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eb4039f0b97ecf144da74c0dd4e1f19142ce91be', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439664', 'Menambah Auditee 7000 - INSPEKTORAT', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bc76989bda8b6ab5b3da77e1676be13100c3c3b2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439697', 'Menambah Auditee 7100 - PUSAT PENELITIAN DAN PENGEMBANGAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('773d3f6b0770fd98bd9725fd3d39e993dc26f28d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439722', 'Menambah Auditee 7110 - SUBBAGIAN TATA USAHA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eadee1a5fba8a2ce9f8f4f2ead86fe41e920534f', '', '1495439729', 'User Logout', '112.215.15', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1e227166443be7e9f90c71920d5630356f43a105', '', '1495439729', 'User Logout', '112.215.15', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f9eb3d801ba96bdc213460ffd30f0b7a9b5e20d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439759', 'Menambah Auditee 7120 - BAGIAN PENELITIAN DAN PENGEMBANGAN METEOROLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('83b064e577be3eb1383f79d00b48bddd678233c6', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439797', 'Menambah Auditee 7130 - BIDANG PENELITIAN DAN PENGEMBANGAN KLIMATOLOGI', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('83e85eb7006e48aa0453782820d0370a507fd355', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439833', 'Menambah Auditee 7140 - BIDANG PENELITIAN DAN PENGEMBANGAN GEOFISIKA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('478961024747af6ace9d4765c362ba25132da723', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439865', 'Menambah Auditee 7200 - PUSAT PENDIDIKAN DAN PELATIHAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2a1c0894c03b7154fe6263524b7c16e9d11d9859', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495439965', 'Menambah Auditee 7220 - BIDANG PERENCANAAN, PENGEMBANGAN, DAN PENJAMINAN MUTU', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8aae7bcd5814b8ddd2b519c57b1b70dac4fe90e3', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440031', 'Menambah Auditee 7210 - BAGIAN TATA USAHA', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d938a1e2e69d47bd486308823740bd7a341256f7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440059', 'Menambah Auditee 7230 - BAGIAN PENYELENGGARAAN', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('193c9b2bb94ff1e1b31b1e9bcc4ab0afac70cdfb', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440126', 'Mengubah Data Auditee dengan ID f130d8ec22ea3af82efe4f5d274c05b439532e07', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('32919725c6ae15136cec47558d158f8a41fc9d1f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440173', 'Mengubah Data Auditor dengan ID 6761fdadcab4a6b9498619489d8ffa7d0ff7c1fd', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('671297eade4664138293921e982eeb6a5da8cd85', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440360', 'Mengubah Data Auditee dengan ID 7d8180dceb928fa7fdc11afc04ff580550e4a689', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('be2c611f6a3b9488a811ab4319766e15f96a9938', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440375', 'Mengubah Data Auditee dengan ID aedf70b79ad7d86a54d89fcf316384fa0dd4c7fc', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4452e2775e136a14dbf29fa6ef8c9dc4084a6d22', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440389', 'Mengubah Data Auditee dengan ID c8efc7d02eab7e07fb44c3a0066b4742ce4212b4', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8050262f4ff86df7cc3ec6959056dbf9f56955f2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440402', 'Mengubah Data Auditee dengan ID ba3b7c17e5a8b3c2393e2f8ac9bf7f0225a19de5', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('80215c1f7a266e4e70ea580856c9c5c3131f940e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440425', 'Mengubah Data Auditee dengan ID fd9d6b1cd1cf65abcea342ed35896b6810a5cec5', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d8d7135af12103a30e22bd2a5553efab37fddc76', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440436', 'Mengubah Data Auditee dengan ID 9cabb164a9375642e01bc72ea1af037d2d07e4b8', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2025ea90110323b4f0ac0f39b185faf49a3a039f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440451', 'Mengubah Data Auditee dengan ID 15d8543e3d92aa065268b5563da082b68440d3f6', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('26c0b8f6fb5140ce783de5c28a8d9cd103f82efa', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440465', 'Mengubah Data Auditee dengan ID 09758f8f60e88e8c8ef4e520a14cc21a9d1898cb', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a8e6830d75e7aed6f585256029910440ba3f6361', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440479', 'Mengubah Data Auditee dengan ID 464ece92eed468c8a005e2cf369d4f8ca2c43319', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fcc2454685aae4c66c5b726ba5de1a8262258361', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440492', 'Mengubah Data Auditee dengan ID 5e6cafd32538b7c1f26fad3e708f30d532936812', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('26a936ae8f51722989b691ba1b66ed7196b0ce14', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440508', 'Mengubah Data Auditee dengan ID 2dec62a03a142f306b7147f08fe1abe767f49d3d', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9a55d63bee8f20dd748cc88ac8f699c80c675e03', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440521', 'Mengubah Data Auditee dengan ID 6840d9fb287b8a2057fb491aa0f9d60e30fee443', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e39283d2736cb8aaf136bab299dfea134377a635', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440534', 'Mengubah Data Auditee dengan ID 87439eae10a330f44fed10e4a2c31d3de11c6f8c', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('84686c6025c8a13d20fb7b7285f99430ce9053ad', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440547', 'Mengubah Data Auditee dengan ID b934060ad3da7246fec29f5003bdf0362703c3d8', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3bc44aa6918038e22b9b2f1f01327045ac38dd1f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440559', 'Mengubah Data Auditee dengan ID d089b5098ad1c540cf237d8ef32995942b066695', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3af8434abc767425b43526413d6cc89e4c6d9803', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440577', 'Mengubah Data Auditee dengan ID 010b29786694ae4a48163f0618e27d9ff8859ab8', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2d6bb33a4e1f6b378942caa25655b0ef568983c5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440591', 'Mengubah Data Auditee dengan ID 469c0f04fb712c7ea6f2891a7ef4dcd2595b1344', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ec69366cd5b90556212f7b9882130138d7e50f9b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440604', 'Mengubah Data Auditee dengan ID db85aeac4b42dfb1824f64dc6cf30ce6be0ab8ff', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2c06b404457daefb81bee4a2f34c7bd909b17f51', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440615', 'Mengubah Data Auditee dengan ID 8d3aaf79b2f93d69ab1e03e660f2b44354b76daa', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('594371223c18848657bbda6d3d6134b75f091377', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440627', 'Mengubah Data Auditee dengan ID 01f3768eb3c6abed4d81603af8e965e3ad6194e1', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d4b5fd1c43db316cf583b9e02a533ab6de280f94', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440645', 'Mengubah Data Auditor dengan ID 8c14bddd4fb579d519f68f69cb47d6cd9ade22e1', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7ebfcb53c344489d7ab0488d9a12ca1c4999364a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495440653', 'Mengubah Data Auditee dengan ID 182a5585d136754655c37564447a4e49573f05c7', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('887774d046714b2fc4b9af50a4eeb9ebbe7f02f3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440744', 'Mengubah Data Auditor dengan ID 7ccaeb2a06cf0dc7648eaaeec67f649660d63823', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('20276f4e762aea0996e04f5ab2fc4213d448a11e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440817', 'Mengubah Data Auditor dengan ID f6d2f70e7bcb3b5fdd7fd915034103c6ec17c527', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f7105ba25859221a647b640edf368ada4979f965', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440860', 'Mengubah Data Auditor dengan ID 1334b71d5b007fb4ef3655dd761b623cf5ef0afd', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('359b9e4965a4de1c9c78eea30d7e969f53a19578', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440913', 'Mengubah Data Auditor dengan ID 60f0d3df33ae13b6497d7141abeb7e291ae83839', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('feee4e7330432ed257664c4a97ca8dd8b8d3587e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495440942', 'Mengubah Data Auditor dengan ID 3b5059a4bd01603a5f4903f5d5929f6e7878aea0', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('aa23f5bceef7a7076de5ad4107746a295fe92c22', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495441007', 'Mengubah Data Auditor dengan ID 24997bc581df7ef7bb3e98d16bf9a7efc6825af0', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('13b61e9e2cb7fb16a6bcc7b6ea811a18914bd418', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495441018', 'Mengubah Data Auditee dengan ID f11905cdfd0e7d45af520a197aa63577f13cd7fa', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a5295fd6758a2813bc116afe99d6dd61c4345977', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495441041', 'Menghapus Auditee ID 89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', '110.138.68', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4378c456718e96f630d023ef69acf447f617a92d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495441045', 'Mengubah Data Auditor dengan ID c2f6d86f82ce263ae87588175f6bd295e1712b81', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('47240853fbac44ab0d5cfb30a9fbc4875a8ee9d5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495448564', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('78f97e93a219f177fcb49fbad369b651325ea853', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495448949', 'Menambah 197206181993012001 - YUNI KHOIRONI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c4d0f5bcdd1cfa821a6aed5db9a105583dadebbe', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495448956', 'Menambah 197104041998031001 - WIDODO SUDIMAN, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('25a355b18365b12399cdaad815fd46d2a7b19d0c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495448962', 'Menambah 198306252008121001 - VINCENTIUS FRANS YUNIAR, SE, M.Acc.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3349b41402f78d3d916dfdbfb537b3fd6fe7b89e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495449365', 'Menambah 198603202009111001 - BAYU PRASTOWO, SE, MAB.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6edffd8fd98a0ff45734165859ee200ce80ea7b3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495449393', 'Menambah 198410282008121001 - DEWIL HITAM, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b9c1a372fe2b0740ce95c9bc228a6036ee70ae30', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495449399', 'Menambah 197211041993031001 - SAMSUHADI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('22ffac751f2ca4485041dee0daa634263e34f945', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495459322', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('10097383dd317088645de1d95ce3d44a845c44a5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495459726', 'Menambah 196004151980032001 - KAMSILAH.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5ce8a10ac70c513dfbbd35075fe1486ec1909b05', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495459732', 'Menambah 197501031996031001 - ARIEF SARTONO, S.Kom.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b72c8539c5b53064ffc452e3604293f692f2d4fb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495459736', 'Menambah 195909111993032001 - ELIZABETH ENDANG WINARTI.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0d33e02c473440e73e90032beeed4e2a4965e5a1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495459775', 'Mengubah Data Auditor dengan ID 52bfdf93477aa817a47bc2c73499ec52b2a64853', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3b5233acca526c1daebc2cd21cb5d95abdcd24d0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495459861', 'Mengubah Data Auditor dengan ID f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f5292132cff0014fc1ebd9e5ed7fbcfe924c6a11', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495460237', 'Menambah 197804122008122001 - EKA KURNIAWATI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('040ac1db2f5b8463dfd9ac380b750b011f6a2307', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495460244', 'Menambah 198412272008122003 - CHINDY MEGASARI, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2327284b2c84f909f91f7789168c2c4d12609b13', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495460250', 'Menambah 198203202008122002 - ENDANG SURYANI SIHOMBING, SE, Ak.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('92dd86bd4db4438b23925fa8da08eaec352681e0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495461294', 'Menambah 198508072008122001 - CITRA RIANA AGUSTIN, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6f8833f33a60b43bc2fcf8638541294574bcd911', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495461309', 'Menambah 198312032008122002 - SAGITA SINAGA, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('99fb7dbc1a0d8926273ba16cd0f1c0389e2ce50e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495461324', 'Menambah 197508142008121001 - IMAM MULIA, S.Sos.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ac2f136989a409152f202b23f2eaaebff478dd01', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495462774', 'Menambah 197409071995031003 - NUROKHIM, ST.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ae0ac1023c6d97e7453bc4ff0b899dfdbd2b51e7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495462782', 'Menambah 197803291998031001 - FRANSISKUS ROBY TRIRIANTO, S.AP.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('38263f7b0ce37896851f41c8ce72d4617c605fd2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495462788', 'Menambah 198308012008012017 - ZURI ACHYANI, S.T., M.AB.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ac276ff8d211727ee3b4b78db182ab7a3d11614d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495464759', 'Menambah 197508171997031001 - SUKO, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ab8a92abaa3d987e41e1ea7a6df4587999e176d6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495464765', 'Menambah 197806012009112001 - NUR AZIZAH, S.Mn, M.Ak.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f4102a7c7e7c3df298600c46446b13b56380f0c9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495464771', 'Menambah 198102112008122002 - YOSI JUITA, S.E.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a19362a4667803b03fe1270cfd7380d65e1a6927', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495464778', 'Menambah 198504272007011002 - VINCENTIUS ANDI MINTARJA KUSUMA, SE.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('335fc1226d236fd035b25a327834d620688e55f1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495504197', 'User Login', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9d62aab102a5172b988206bef621f596f938f7da', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495504280', 'Menambah Auditee 9100 - BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH I', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('99323b08d5d93971988b33f2abb7f1544d4b7a47', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495504387', 'Menambah Auditee 9200 - BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH II', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b134d858f98417100f08f16b83596552f2fbafbf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495504439', 'Menambah Auditee 9300 - BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH III', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8004cd3e10675df1694b6027a70773e5063bda5b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495504502', 'Menambah Auditee 9400 - BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH IV', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fa0163a1b55f10788042c72f1e372ddc5768c961', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495504538', 'Menambah Auditee 9500 - BALAI BESAR METEOROLOGI, KLIMATOLOGI, DAN GEOFISIKA WILAYAH V', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2af7a26137b4d6bda1880077112ad5da04a63136', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495509806', 'User Login', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cea06c78b1f223ca534b7289a903ca74b16bff1c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495509934', 'Menambah Auditee 9101 - STASIUN METEOROLOGI KUALANAMU', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7ed69d98dc72dfde3e28cd817a38e5e059636f3f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510076', 'Menambah Auditee 9102 - STASIUN METEOROLOGI HANG NADIM', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8747b36251bab1890eb5525c7a7c8bb2a3308c58', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510174', 'Menambah Auditee 9103 - STASIUN METEOROLOGI SULTAN ISKANDAR MUDA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e07429b6c001ee63660585bc044de7bb3ab12f1c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510253', 'Menambah Auditee 9104 - STASIUN METEOROLOGI SULTAN SYARIF KASIM II', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5ced87b5e9385f430f70069917725d851f0b571c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510323', 'Menambah Auditee 9105 - STASIUN METEOROLOGI MINANGKABAU', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fde6d56170f37f0903083a413dcdb6a87afed4e6', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510373', 'Menambah Auditee 9106 - STASIUN METEOROLOGI MARITIM BELAWAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fe09c88dd394b73c2d63357730d9a641281b98d4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510489', 'Menambah Auditee 9107 - STASIUN METEOROLOGI MALIKUSSALEH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e6aef0765ffe528d562f5a33b18beb09b27a6a4f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510563', 'Menambah Auditee 9108 - STASIUN METEOROLOGI TJUT NYAK DIEN MEULABOH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('590fcd497b8bad5df090d2e31dbfbaa33f3798a3', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510664', 'Menambah Auditee 9109 - STASIUN METEOROLOGI CUT BAU MAIMUN SALEH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4343e2cc537b854d56c7bd23ee6f17f87871097e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510725', 'Menambah Auditee 9110 - STASIUN METEOROLOGI JAPURA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d07232c6d5d49e8c840a92b308143056cb18becf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510778', 'Menambah Auditee 9111 - STASIUN METEOROLOGI TAREMPA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d7769afa990764c8c48085daa58db915c9f03200', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510839', 'Menambah Auditee 9112 - STASIUN METEOROLOGI DABO SINGKEP', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('879866afeb207ce6cfecfe28324a0ba81bd8a224', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495510919', 'Menambah Auditee 9113 - STASIUN METEOROLOGI RANAI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('75b201afedce21130095c6f04c19248159bd2771', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511063', 'Menambah Auditee 9115 - STASIUN METEOROLOGI KIJANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('75bf45eefe326b61d43c1cb676845e12515e9263', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511232', 'Mengubah Data Auditee dengan ID c40f80349aecb991f2c5839a2e2b546a587879bd', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8e8ceca8c1dea475185c91d68a8c1a02efb5996a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511293', 'Menambah Auditee 9115 - STASIUN METEOROLOGI BINAKA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f02fce2e38b639d76632a53fd2aa1bfd04b1667a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511350', 'Menambah Auditee 9116 - STASIUN METEOROLOGI F.L TOBING', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f855b29c6ea4ff11b7d5951de19efc625ad1e7a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511418', 'Menambah Auditee 9117 - STASIUN METEOROLOGI MARITIM TELUK BAYUR', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1ae2e79f4884b1e5838aead96dc4837fffa1d3c2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511512', 'Menambah Auditee 9118 - STASIUN METEOROLOGI AEK GODANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ba2e74a36932b26b04e8b7b64d7b8065f71d0d47', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511574', 'Menambah Auditee 9119 - STASIUN METEOROLOGI RAJA HAJI ABDULLAH TANJUNG BALAI KARIMUN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dc363e2ebd33bbd25aafc9e368ed0a0c642f9020', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511884', 'Menambah Auditee 9120 - STASIUN KLIMATOLOGI DELI SERDANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9bab2b85b9b4396979c11618d13960887edc0093', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495511940', 'Menambah Auditee 9121 - STASIUN KLIMATOLOGI PADANG PARIMANAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('922a323655b2649a4fcb60e4ca0435e38ac8ad0f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512007', 'Menambah Auditee 9122 - STASIUN KLIMATOLOGI ACEH BESAR', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fb5c632a0465fffa8e2da35692d7ace2dbee755b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512050', 'Menambah Auditee 9124 - STASIUN GEOFISIKA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8594af434825c6832ffaf5f5c74b06e51dfcc62a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512144', 'Menambah Auditee 9123 - STASIUN KLIMATOLOGI TAMBANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e900cbfffd808515f5262527312baed59d77bc90', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512217', 'Mengubah Data Auditee dengan ID 85d2e28432e91593acb499b27c3c8f6075be5776', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f4a997c3f8fa0a36068cc8e3adc506c9c0e76fbe', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512269', 'Menambah Auditee 9125 - STASIUN GEOFISIKA SILAING BAWAH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('462a0a89b31bec27a814d587b714b9528558d9c5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512331', 'Menambah Auditee 9126 - STASIUN GEOFISIKA MATA'IE', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('35bf7be9a8f6afd467aef3ea6ff28918f7041f01', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512383', 'Menambah Auditee 9127 - STASIUN GEOFISIKA TAPAK TUAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('065424f416328ba54de3c3c8bcbbbfdecb1ef425', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512437', 'Menambah Auditee 9128 - STASIUN GEOFISIKA GUNUNG SITOLI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d6f93945e96511ee51768e30a922bfbeaf6ddd7a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512482', 'Menambah Auditee 9201 - STASIUN METEOROLOGI SOEKARNO HATTA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('22faf2e4d4a1666513e231dc324bfadf03db5eb7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512506', 'Menambah Auditee 9202 - STASIUN METEOROLOGI SERANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9ad054ca38e574bed5aa4f6dcb1ecc5d11c52b88', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512555', 'Menambah Auditee 9203 - STASIUN METEOROLOGI MARITIM TANJUNG PRIOK', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2b3d562430d856c26070b24bde2cfa3d83211cf2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495512559', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9a0269b66682bab120a90e826f9baa86faee6fd1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512626', 'Menambah Auditee 9204 - STASIUN METEOROLOGI RADEN INTEN II', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4c75a20baab1e6a9aa24e2148a47711eccc5e65f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512685', 'Menambah Auditee 9205 - STASIUN METEOROLOGI SUPADIO', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f694a2b41d56e2b98c3ffc835bb83d01c5fe39df', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512755', 'Menambah Auditee 9206 - STASIUN METEOROLOGI DEPATI AMIR', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4f2a67f43b7da073ee45b0e6d525a3c1bcaa93e9', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512828', 'Menambah Auditee 9207 - STASIUN METEOROLOGI SULTAN THAHA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e4b670566165306263dd08a2a332d80daa809f32', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512917', 'Mengubah Data Auditee dengan ID b043aef5e6cc7dc4149833f7bf4484ad7a8690c4', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3e48a5c3747627f1ff0eef02a3502aecd8c316f2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495512979', 'Menambah Auditee 9208 - STASIUN METEOROLOGI SULTAN MAHMUD BADARUDDIN II', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('71a22773a20a4a66729262d6baa09ce510482a75', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513031', 'Menambah Auditee 9209 - STASIUN METEOROLOGI MARITIM TANJUNG MAS', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d58889bc0eb3aa5fcc49dadce66a0dfd8f639de0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513080', 'Menambah Auditee 9210 - STASIUN METEOROLOGI AHMAD YANI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6b9a1e988822f8f769496e4353eb9610ae8d5f81', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513155', 'Menambah Auditee 9211 - STASIUN METEOROLOGI FATMAWATI SEOKARNO', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8994191b34969ffa27ff7541c76323399f857cb2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513227', 'Menambah Auditee 9212 - STASIUN METEOROLOGI BUDIARTO', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b219bf8234e09d0f9f520c1109b8a98b019387e7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513283', 'Menambah Auditee 9213 - STASIUN METEOROLOGI H. ASAN HANANJOEDIN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('400bcfa49100fdd5d6529b1997b9c92b3c43c6d3', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513338', 'Menambah Auditee 9214 - STASIUN METEOROLOGI DEPATI PARBO', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('85562ea8c45cdb4d20c667a69db3f6323e47c962', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513393', 'Menambah Auditee 9215 - STASIUN METEOROLOGI TEGAL', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('90eebf7aed1405c292ad04e9bfbd75fbe999116a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513448', 'Menambah Auditee 9216 - STASIUN METEOROLOGI CILACAP', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3f55c4fd1452c78949b5b90a84b8a8715af8d1e7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513502', 'Menambah Auditee 9217 - STASIUN METEOROLOGI KEMAYORAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2e6026ff42398c4d5daf53f27c8422e7ebdfb0df', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513570', 'Menambah Auditee 9218 - STASIUN METEOROLOGI PALOH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0d86ac65baeb936181435d7c76873d2b77907221', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513641', 'Menambah Auditee 9219 - STASIUN METEOROLOGI RAHARDI OESMAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('26380fb55fbc7c686dffc29fa5b07470a30188e4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513736', 'Menambah Auditee 9220 - STASIUN METEOROLOGI SUSILO', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('02959f111574296a8d30cf3043c226bd080b25ee', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513817', 'Menambah Auditee 9221 - STASIUN METEOROLOGI NANGAPINOH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('88d02a10af19bc80e359fcd8dae641563d979048', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513885', 'Menambah Auditee 9222 - STASIUN METEOROLOGI PANGSUMA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('29b10e577b36684548b3b9d5c31d87d3c8f76644', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495513942', 'Menambah Auditee 9223 - STASIUN METEOROLOGI JATIWANGI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dcc03952f0fff28d5e98f3daee0ecd4d88a85d0e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514002', 'Menambah Auditee 9224 - STASIUN METEOROLOGI CITEKO', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('805c4a3271d72f60503f45dbe2711951f4c3a29f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514058', 'Menambah Auditee 9225 - STASIUN METEOROLOGI MARITIM PONTIANAK', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4134681188f40c85b2f2d17f0d55561c16e5253a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514133', 'Menambah Auditee 9226 - STASIUN METEOROLOGI MARITIM LAMPUNG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0a94882be3aeef6db81386cf7ea20f15c48b2c0b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514174', 'Menambah Auditee 9227 - STASIUN KLIMATOLOGI BOGOR', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('59524867a0f7374f7c7f4c7a523cb38e3095e6a4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514223', 'Menambah Auditee 9228 - STASIUN KLIMATOLOGI SEMARANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('84acf11349b46f0867ff70e28c15eb9fe72d9315', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514272', 'Menambah Auditee 9229 - STASIUN KLIMATOLOGI PALEMBANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f280850aadb622bd024c18673e45bdfc3464f6a5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514312', 'Menambah Auditee 92230 - STASIUN KLIMATOLOGI BENGKULU', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b58724a15c028f176e66e5fc66619cc0286eb33c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514355', 'Menambah Auditee 9231 - STASIUN KLIMATOLOGI MEMPAWAH', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('928fbe632b1e49d5b32eeb70263765d14caf4e59', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514409', 'Menambah Auditee 9232 - STASIUN KLIMATOLOGI TANGERANG SELATAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2199764cc358727bf9b02555bd2a7ea4acc02f40', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514463', 'Menambah Auditee 9233 - STASIUN KLIMATOLOGI MUARA JAMBI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3853ac1b631c9b22363b0f47202015cd49e069a0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514511', 'Menambah Auditee 9234 - STASIUN KLIMATOLOGI PESAWARAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('aef9f04f9cfc7bfaa9842889a3f1a1df4a3f7c2c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514555', 'Menambah Auditee 9235 - STASIUN KLIMATOLOGI KOBA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7183677e485a00b463a7aad811ab899e46bc8b02', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514604', 'Menambah Auditee 9236 - STASIUN KLIMATOLOGI MLATI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9054b79a606e20fbf86db2a9f026c28e3db83ead', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514647', 'Menambah Auditee 9237 - STASIUN GEOFISIKA TANGERANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('584c095baebe3b23c325c2b01b010f15546d3d00', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514692', 'Menambah Auditee 9238 - STASIUN GEOFISIKA BANDUNG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6d03558f7243cd5ab853e3dd6eeaba3d57a0b55b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514787', 'Menambah Auditee 9239 - STASIUN GEOFISIKA YOGYAKARTA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('51f377666339ba076c8b8e4686f196353d8fa178', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495514788', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d7e8b4a481c2f2b761b538bad707d6faf4de2e28', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514844', 'Menambah Auditee 9240 - STASIUN GEOFISIKA BANJARNEGARA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6818ebd7723a8c802c7eb81d3700225bab6aca2d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514911', 'Menambah Auditee 9241 - STASIUN GEOFISIKA KEPAHIYANG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ba5ab3a932d38bb2d9f3b78f6f2c0858088e083c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495514980', 'Menambah Auditee 9242 - STASIUN GEOFISIKA KOTA BUMI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('42970e5e58331cc8b150cd7ffff3ed929c354b50', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495515059', 'Menambah Auditee 9301 - STASIUN METEOROLOGI JUANDA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a477a406d3203dc625efb0540204782e45f41da7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495515432', 'Menambah 199104162015021001 - HERI PURWADI, S.Pd.Si..Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('caa118faa55ec1aade9e98a4f60f5ebad4662c94', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495515438', 'Menambah 198504182015021001 - ADHITYA SAPUTRA, S.T..Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bbd108be44d0810afb7e941891c2fed913e2066c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495515444', 'Menambah 197701062000031001 - ARY SUTANTO.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8b1e03e87aa5276f992d96dd01c97d516ea8ef72', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495515449', 'Menambah 198508312008122001 - ILMIATI HASNA.Sebagai Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ece8e577b766bce2398f3f811e24fb86ab59289c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495516934', 'User Login', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fd02d33ecc3ff02abc68b4c8bb686ab410623c04', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495517810', 'User Logout', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5dd5ee33a766f9fdd1b3bda0211a91858cfcace8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495521213', 'User Login', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('97b6cf13a714c677e76f90b1709a4ae1647ffac1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532363', 'User Login', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('04d497902e551154e41e891a77cbef6c47e9b9ae', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532441', 'Menambah Auditee 9303 - STASIUN METEOROLOGI SULTAN AJI MUHAMMAD SULAIMAN SEPINGGAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9f0c36624feea8259379c89275bd8b96b2df12b5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532501', 'Menambah Auditee 9304 - STASIUN METEOROLOGI TJILIK RIWUT', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('33d41144147c7fcec56cb29fe57bc57b26da0f5c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532560', 'Menambah Auditee 9305 - STASIUN METEOROLOGI ELTARI', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('55e833ec198cbb0c28f5a8538525cdc3fd40af28', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532693', 'Menambah Auditee 9306 - STASIUN METEOROLOGI SYAMSUDIN NOOR', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('65a30128aeb4c25b1c0b58d130f1b8ab17d2ab08', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532755', 'Menambah Auditee 9307 - STASIUN METEOROLOGI MARITIM PERAK II', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('084409c8a052bfd7a57fed7f34bd6344421f6545', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532821', 'Menambah Auditee 9308 - STASIUN METEOROLOGI BANDARA INTERNASIONAL LOMBOK', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2126f4c55247606dcb90754bc4a1eeee83401c37', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532879', 'Menambah Auditee 9309 - STASIUN METEOROLOGI ISKANDAR', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4be6c4b014f1bd7ee62a5d4b4c3b079d2df5d9b0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532926', 'Menambah Auditee 9310 - STASIUN METEOROLOGI BERINGIN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('296a1e654521288d02653029dc427251ae73975b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495532991', 'Menambah Auditee 9311 - STASIUN METEOROLOGI TEMINDUNG', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2e4f6085937276ad830455d9295d0ebd6e8d03c4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533062', 'Menambah Auditee 9312 - STASIUN METEOROLOGI JUWATA', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9e502cec4d8c4317bac77e3477d567932274b3a4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533122', 'Menambah Auditee 9313 - STASIUN METEOROLOGI KALIMARAU', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6d6678d1354e46d73fc73ccdc42279c73e8e3dd4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533185', 'Menambah Auditee 9314 - STASIUN METEOROLOGI TANJUNG HARAPAN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('675c2da25989303d042174ce854ecdfe51c6b337', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533248', 'Menambah Auditee 9315 - STASIUN METEOROLOGI YUVAI SEMARING', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e55b05bb0b3bdf800ab66f95fff888caffb74d63', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533323', 'Menambah Auditee 9316 - STASIUN METEOROLOGI GUSTI SYAMSIR ALAM', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6b9b16d543ea1a02bbda0583d6d6bb1de8237c4d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533402', 'Menambah Auditee 9317 - STASIUN METEOROLOGI SULTAN MUHAMMAD KAHARUDDIN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f9e033efee2bfdacb8c5dc3c02a80c99b6531b93', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495533469', 'Menambah Auditee 9318 - STASIUN METEOROLOGI SULTAN MUHAMMAD SALAHUDDIN', '180.243.11', 'Sukses')
<query>INSERT INTO log_activity VALUES ('abcaf2d768a51ef636cf398ec48dfc31a932cb80', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495548490', 'User Login', '36.79.118.', 'Sukses')
<query>INSERT INTO log_activity VALUES ('941f0bb2688fe3e433a1bc5a151f60fa16c8e21e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495548741', 'User Login', '36.79.118.', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3e879abfb681a706510cdcf9e323245427cdc4b7', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495568307', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('712eb3798e22c2937699f1c41b7e6d93de22f6ec', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495568842', 'User Login', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7fba3f07e463d10dec67dd9bd8986fdad3c0ccb8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495568876', 'User Logout', '112.215.17', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0957ad3a5379a42fb9c4a9d45a77ee6f44e4a5d1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588315', 'User Login', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3a02051ba2507ac955630ff90d69b8f2a7006086', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588374', 'Menambah Auditee 9319 - STASIUN METEOROLOGI FRANSISKUS XAVERIUS SEDA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8e019b1aa371fe59ec8240b86ccfe650d20e5ba0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588415', 'Menambah Auditee 9320 - STASIUN METEOROLOGI UMBU MEHANG KUNDA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('614366cf29e079947395099120fd4a37b93d85ac', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588464', 'Menambah Auditee 9321 - STASIUN METEOROLOGI DAVID CONSTANTIJN SAUDALE', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('98356ca6f0f3f3f8314c41ff96f10722d2a81eaa', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588505', 'Menambah Auditee 9322 - STASIUN METEOROLOGI GWEYANTANA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b0dd3c4e9358289f96c922abfdadd8d37486e5c0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588544', 'Menambah Auditee 9323 - STASIUN METEOROLOGI FRANS SALES LEGA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c64dcdf4ab9b0e54a0dbd6ecd9d3776282647d2d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588575', 'Menambah Auditee 9324 - STASIUN METEOROLOGI MALI', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d29dfb3fdd41bb7ad59d5b7b6aef2cfc3f4e2cb4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588607', 'Menambah Auditee 9325 - STASIUN METEOROLOGI TARDAMU', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1a675b577d987562eef2890fcf6f6b52f402037f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588645', 'Menambah Auditee 9326 - STASIUN METEOROLOGI KALIANGAT', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('48c90286d7f9021e44c269de944877f3c5d0d1e6', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588685', 'Menambah Auditee 9327 - STASIUN METEOROLOGI SANGKAPURA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('21e8f7038a97b75f7fb2b4f739a6f9ac168c1aa0', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588729', 'Menambah Auditee 9328 - STASIUN METEOROLOGI TUBAN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8158a04625912b22c4724366fa4913c0626f3251', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588761', 'Menambah Auditee 9329 - STASIUN METEOROLOGI BANYUWANGI', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9bbcd6540708fface7e9e448438c40bf78b53b4c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588799', 'Menambah Auditee 9330 - STASIUN METEOROLOGI NUNUKAN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0d3d5a3cb29f3fca6859fc1634cee708afbe946f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588829', 'Menambah Auditee 9331 - STASIUN METEOROLOGI KOMODO', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f10f1fce962529e53d6bbe033924073c2d4c8be7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588881', 'Menambah Auditee 9332 - STASIUN METEOROLOGI H. ASAN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ab34a0a5d70182bdcd68df1cfec7533bc74101b4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495588917', 'Menambah Auditee 9333 - STASIUN METEOROLOGI SANGGU', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('036b06ceec71a1caab2e86c13e83bce39f9b9e16', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589009', 'Menambah Auditee 9334 - STASIUN KLIMATOLOGI BANJARBARU', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('98f5de59c65b0709b2e38da0031606843be983a9', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589044', 'Menambah Auditee 9335 - STASIUN KLIMATOLOGI LOMBOK BARAT', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('126529edca65389fa62e654a9998c4ada4c89bdc', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589079', 'Menambah Auditee 9336 - STASIUN KLIMATOLOGI MALANG', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('01adf178c191490b251780ee0ba5dc3632d903d4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589117', 'Menambah Auditee 9337 - STASIUN KLIMATOLOGI JEMBRANA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7cf92c7105a69461cdd40cfbc485b33691c39b66', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589151', 'Menambah Auditee 9338 - STASIUN KLIMATOLOGI KUPANG', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a034cefef7e678070e61306661f0b6331d04fc9e', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589189', 'Menambah Auditee 9339 - STASIUN GEOFISIKA KAMPUNG BARU', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ec317f687c792a4ac4418429c1d035821f258ec5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589229', 'Menambah Auditee 9340 - STASIUN GEOFISIKA TRETES', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('58824aa93964115eead42522389f4f1687812482', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589264', 'Menambah Auditee 9341 - STASIUN GEOFISIKA SANGLAH', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('917820bbe2a21d94c99f765f096f0aa504ed0669', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589303', 'Menambah Auditee 9342 - STASIUN GEOFISIKA SAWAHAN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7bec14c159a335227b33856407e42a33f195a439', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589339', 'Menambah Auditee 9343 - STASIUN GEOFISIKA KARANG KATES', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('13360e8f90060077508ca9e1aff1d30c8f0de6ca', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589376', 'Menambah Auditee 9344 - STASIUN GEOFISIKA BALIKPAPAN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('920399282e04241b7d2c8a57b77186404b67d379', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589408', 'Menambah Auditee 9345 - STASIUN GEOFISIKA MATARAM', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b0653bc9e02fcfc8ec2e3d8e846fe4f078a9b978', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589446', 'Menambah Auditee 9346 - STASIUN GEOFISIKA WAINGAPU', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cfc7e4867cf98c238a30b1ad01acd063a7af2b78', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589488', 'Menambah Auditee 9347 - STASIUN GEOFISIKA ALOR', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4623a79b117112cb3900fff893d518dd0a33b017', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589544', 'Menambah Auditee 9401 - STASIUN METEOROLOGI HASANUDDIN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3ff6208bfa0932627933471c1f7cc4d036f46e1f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589589', 'Menambah Auditee 9402 - STASIUN METEOROLOGI DJALALUDDIN', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f68b4b9b7ac74feb052ae4c400defe2cf2ef7c16', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589644', 'Menambah Auditee 9403 - STASIUN METEOROLOGI SULTAN BAABULLAH', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('332922673f7b19799503ecac006a097ad1f70061', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589687', 'Menambah Auditee 9404 - STASIUN METEOROLOGI PATTIMURA', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c04b37bd5843ff8559ecab89160f8f4008862d37', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589741', 'Menambah Auditee 9405 - STASIUN METEOROLOGI SAM RATULANGI', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('368e7de7e66493e91223d8d146f68df46b0eb0d7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495589870', 'Menambah Auditee 9406 - STASIUN METEOROLOGI MARITIM BITUNG', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1eb4abc72c8add2970c8959b7f5c08408fba624d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495591692', 'User Login', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bd3a7bbc7683e0d1d79fa7ebf2b80a0a49a5c0c1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495591703', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4e13a904af890456e4969890459509255fd51e0a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495591822', 'Menambah Auditee 9407 - STASIUN METEOROLOGI MUTIARA SIS-AL JUFRI', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fbb33259e89864f1899d975cdc2e930e6311704f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495591895', 'Menambah Auditee 9408 - STASIUN METEOROLOGI MARITIM PAOTERE', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('963e3c9ad7aabbf2e2217341231a1e4217167b79', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495592110', 'Menambah Auditee 9409 - STASIUN METEOROLOGI MAJENE', '125.165.91', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d65a955d9389bead3d9f7308472aadfa8f69ce07', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495592994', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a8358f45e5305345a18fc04c2ef78e7de1a3f7fa', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593048', 'Mengubah Data Pelatihan Auditor dengan ID db5a0c0d4e370621c5d4d209e284dccc837f6445', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cfa62b752e36978d258bfb8ea0ca9c461666878a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593161', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('81108f674c4cac20b906d1476c6bb295de555810', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593208', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2dc449bea9c39c4d510f5c8e8a269bb0af059964', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593262', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a8da6ed81a319ac2a9905a502f00cc7cb1d99964', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593318', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f0a6404ed039d22672301bd54c63ec67c6205372', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593340', 'Mengubah Data Pendidikan Auditor dengan ID d725a47afc021cefd08c0f20dd9f13e9da787d97', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2c7f3d7a28a313948fd8336726369fab308e9fe8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593394', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('25bad173a50311d1b446016809d366afbd32ba87', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593453', 'Mengubah Data Pelatihan Auditor dengan ID db5a0c0d4e370621c5d4d209e284dccc837f6445', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5690137b7ceb33c88867c47284ecc2d0728fccaa', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593543', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e0b2605338a184408501935d637c66cc64fca906', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593631', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2b3e61905051960c1ccfc0eebb0b702f8ab51616', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593693', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d7ff4ad3d5c7bdf2708ef67910485b5149cae8aa', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593765', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3aeb0f03b3b5f6040a45f47d78bfe9d9773711f0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593822', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8e90d1f97fb02f1583529ad87a966cfa6c1085cf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593895', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8eabf3d421e872714af1919a6f51c4176bccddce', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495593971', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8440bcc4517cdfd03b1a5f5d01ad9596bd26ba11', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495594305', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d9e2d7e18f998bcff923cb6f6e35daeaf7d966ec', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495599593', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5deafc8ad14d55b90fe8f668c4425750fa30a8d9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495599727', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0de40165f9e30307a52d54b25975c09a4dd299bb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495599762', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5f2684b8e482c3e8c70eb38b439c18d8063f9b60', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495599814', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('82db30e16914b0fc74868d832016147f38445642', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495599902', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d4630bde7845b19096a5a00641baf5e4070c8f04', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495600033', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('160d7b45151ebd719cd01f1d7c82d734a0c68d78', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495600443', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e141fcee32b675fcbe509e94dce63ea321a98e69', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495600517', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7d6c89b7e7b42d2dca03a8ad4ae910c911c801c4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495601184', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('768826cc61e60a4587b3829929c4d8ccce835b2f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495601402', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f711d5eeb1a61dec520d2edb535749d66fa5f4da', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495601472', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e4ae4105bcdff883a14e9890c66e3d0bf43cfa58', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495601539', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3348fe0374f220e068d75c1178fdf284b7fad931', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495601596', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6b1fd2861ad5130ed43b1ef5fb27271ebb1a51ae', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495607348', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3cc9e03e5b135b6c35242feca6033f906bbfc5a3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495607431', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('68a4b19ea7fe8d765a28309894628bccf500d11f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495607525', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1824e7cf7f21de66cbf4e53657c4c3425e2a483d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495607873', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('57fa1bda1f95fe2115c79e0abce6bf1d108a65da', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495607979', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9d77e5204bad2628da066f3af0cf624abc9d1f6f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495608085', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4c69b22dd18db25ee3c655960422064415771775', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495608385', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e5a3022363b90904806e3342315f4ba7b5894a93', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495608435', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('57b8bcbb7f2a05439df1ea9c9eebbda34e2300a2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495608491', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fcbf7570cc44293b8cd57b5d7a609c937bef57f7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495608647', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f0865c127ca89650ac4dce4c1e6e5a92595954fc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495608688', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ddad55c1633af02d0b77ca2782889bd94af00881', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495609515', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('15ec0d5bf08b171a980779132b9935fa9f2f252c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495609809', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('23bd3af31968e9a0faab5f52576545af4b13bd0e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495609859', 'Mengubah Data Pelatihan Auditor dengan ID 152b38ba09cec02e86e3cb2cb2a392aa3eb752f4', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0e05f17b656f6c3fd0a2faf479580c2d54418468', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495610183', 'Mengubah Data Pelatihan Auditor dengan ID 152b38ba09cec02e86e3cb2cb2a392aa3eb752f4', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8acaaec26dfb716494724de52f29f74e8ac04564', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495610202', 'Mengubah Data Pelatihan Auditor dengan ID f05115da3d2316adc910f442dd16e5a8555e5c61', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('05177fb4aac94dcc61b91a1ab287ea3f063c9c27', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495610452', 'Mengubah Data Pelatihan Auditor dengan ID 0dd012ea0a7631f8b7d6ff25f45fa19b071666b9', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('46a863e2737e703ba34402e66c7cc7b8d88b9700', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495611798', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8649d9e75bbc9cb848d0d6a6524abd067f29f4ef', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495611931', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('570a81ccec18d0ebf5c1e663a737b5a2acc1048a', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495611987', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9fc02295878f2581a65f0caf82bb4f8d15982790', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612041', 'Menambah Auditee 9410 - STASIUN METEOROLOGI MARITIM KENDARI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('28650d3436fd5acca3780b789c62392738f85cca', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612105', 'Menambah Auditee 9411 - STASIUN METEOROLOGI DUMATUBUN', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('36e14310103df98194b2b3e4d87cd0f522af9d3d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612112', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3f94669af97698af3495bcb021fe9f12842ac0ef', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612147', 'Menambah Auditee 9412 - STASIUN METEOROLOGI AMAHAI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('29d0f68543c84b852eb5b1c2148bfc8e92edb72e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612175', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('96962aff15e76f348198e198da2a9d41a67a0bcc', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612200', 'Menambah Auditee 9413 - STASIUN METEOROLOGI GESER', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ce48f5ba351a5e36d81085b5fdeb6d7c71e2fa10', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612233', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a311a86898c9f054d2798ae65bf945dbcdbfaca5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612243', 'Menambah Auditee 9414 - STASIUN METEOROLOGI OESMAN SADIK', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ba6863012760b7431959a3579e4cde577b60c045', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612290', 'Menambah Auditee 9415 - STASIUN METEOROLOGI BANDANEIRA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4a197644061069dfb2134d9f2e4b655c92726b31', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612335', 'Menambah Auditee 9416 - STASIUN METEOROLOGI NAMLEA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2c61c1c57e77846a43ed713c27d71145da0c8a09', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612365', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e2c96fffa15bd9fb98d15656b86ae329518381cf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612391', 'Menambah Auditee 9417 - STASIUN METEOROLOGI MATHILDA BATLAYERI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a14b0248fc423d60649c6471fe0e57420b4376e3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612395', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('560c356302589beb1cfae9350382f057b76cb811', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612425', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bb835e92a4378da5f346769bab7e38306f57e9a1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612442', 'Menambah Auditee 9418 - STASIUN METEOROLOGI GAMAR MALAMO', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('36bb0adb3ef11cc95aa43968f31a9f87ce50d3af', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612490', 'Menambah Auditee 9419 - STASIUN METEOROLOGI EMALAMO', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('38f6424e3474f3cbf0e0e9507c37e13753acffc5', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495612572', 'Menambah Auditee 9420 - STASIUN METEOROLOGI KASIGUNCU', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('18460dd5ff304465616df8f59643dc19ae3bbd6a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612605', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f652a982088ad8d6dcac564e8697e974a809e2bd', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612609', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b0ceb5361c2e725bdcc9a285930f4bbffe8dbd7b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612638', 'Mengubah Data Pelatihan Auditor dengan ID c9092f49b257f4d97fa15dd86382d67ca3cbfe7d', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9cc513d389a4f8a3e49b1991bee59b9beabe4805', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612688', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ca0bcb5b6290aacb80fdd4f2c87dc9032e88b4bb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612818', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cf57420d873a4751e9a14fc4b9b59c79f7b35069', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612826', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('75fbe7e1e399966757aaa1cfd144e35d36df000f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495612830', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('99df11c5bc7cec52dd2ac17226af04c2806c7570', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495613252', 'Menambah Auditee 9421 - STASIUN METEOROLOGI SYUKURAN AMINUDIN AMIR', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('db5d04381f789939e65b4916f2186286bb2a610d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495613304', 'Menambah Auditee 9422 - STASIUN METEOROLOGI SULTAN BANTILAN', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dd5a8fbdaed3b8eaa3c45c825b582bbb99c2facf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495617372', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c60cf7d97faed395966e209ae8a3fc51857a8dbe', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495618290', 'User Logout', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f7e6a184066c2b31080399ee6b430e26567fbcdf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495618752', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('58bd801f54005b08f20724bd2544409895f37059', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495619428', 'User Logout', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a7c1be69026dce309addb105315c2d5f80a48f33', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626093', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c8726f43b42e5fdee2b66bee36812cd99a3657f5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626239', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('288d333a624c7b50757bd3519c97cb7e27988806', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626503', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('69f3a45fabe7cddcc8d8b91b489f5039bb8b9235', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626509', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('256249f46bc5b6889ad9440cdddc88e8874e7afc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626513', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ee722dc91ec2c8ff86bb622b9cfbd7609d125771', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626516', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a31839e2272fc2a734e099e6b4b00173493aaac6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626523', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('addc541a7aab807bdb613f5da873f3357c3a8abd', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626903', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f9ea6f060f6aab1ee6b8be1107ef8c14424ea9a2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626912', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('89fc6522aa090943e464b843609f2a895e121fd0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626922', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a6c958ba8e830d03ca89da7594ea43e8e84ebc61', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626924', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b40a9e3bb8ba8227700ce34e4828311266e91a79', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626928', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('89ad1502e0fbc87eec53a15330dfdc6c0664b0cf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626932', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('feae6ed3c7e1fba18ad9bf0f57ce57bd4dfcacdf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626935', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5ab203ff8997974884d6422a812f5fd9f887f88a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626941', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5308296113feaaf17a449bce9d872ed13dbefeb7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495626945', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3532f3be20f4ec63c591c4ca2402b7fa373a613c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495627374', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e304c0a348d805acc41f41c4ebe3932321b35715', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495627379', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d742f29ce6ecc085b2e1ecb5055089ebcdd357e6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495627385', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6dfa5c8e7125be26ef1861b55e78767e232b950e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495627391', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1f8e6468f371c02aa1dd036862067667d29af8ae', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495627397', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a5343b6020655d0a193578e24028f8c8d66c610b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647162', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4852d6924f04cd0f5195c01c3037f8c43e58aabf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647748', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('574415fcd09dc854b59c632ec45125bc2c796aaf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647756', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9c1ab05ed0f683a22464497c2e1faf584ff4cfc5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647763', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c75ae71d0cd42fc60387d7481c3bd83aee44b04f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647768', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6e0f93b1d9493fd1225eb6bf7e4e7c90eb417f26', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647771', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('50c835ba0042815a698fd772a9b9ae268873b23c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647775', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9f28681d162a7bca72d4215997c2742e29668e1e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647780', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('508a7a180305f65adc5e67ab471362d7a4c3040e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647785', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ab5fdae3939d2e434dd504485c365929878be2e9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647789', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0bc3307db57f1929e4b99911b1a0431cd2d61b5d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495647797', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c336678fbff69c3c217a5ec40f63580060e79bcf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648430', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7aadbd5b4780101de8e71d9e146473eb06181869', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648437', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5a83a64ecbaae42e28d06aa0527ab8ff1a921022', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648444', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8ef07d4f9504ff867a5aa07f6d3d9139995584c5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648450', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('83d81b2fe00269ca08ddcb85462ce009c5b4c062', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648455', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9af1ee4c88e58629e38417a220ee5e888cdf00fb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648462', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b15bf2fbd6c966fb6a203d85b19f5d447630f1e9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648467', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5ee002b71655639b1d7f5b4dd1168531fc99e8e3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648472', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d72183d348245283ab333b9dad2143647da19875', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648478', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f39d70d4ed9a00d84d525b3aabc0750d9f5c282d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495648677', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bd69d9fea4c7d1e846d789f8d8a60d5a5e9758e4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495649273', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b1c9864dfb8751f72232bbb2fcdfc2526843c0d2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495649279', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b978f51aee48da356e28ef1c0eb7a3d1b58de5c4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495649285', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6dd26a4beae9c1644ff8312502c5dbfaa650f214', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495649292', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a9419fe40470345f99602bc4232a65c873cae14f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495655918', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bd87590d3dd85932def18cedcd119e35837bd2ad', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495656201', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9aa4300fe57f9237c875ab31be0c4cb0197c4ecc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495656207', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e6c77fe6c33c7749275c714ddf70458ae72b5476', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495656213', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3ebc0c8db7bfff81170f27e64969081cda912ad5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495656219', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7afb17a089a0fd684e6e4285c1a560747683961a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657043', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1b679a157facb7211de856080b4221e79d562d38', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657050', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('32f51a5a1badf2d5a8c595ac31ade8edc98a72b8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657055', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('069d5850bf1ed66a95c8b1fe93c32b08954b8270', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657065', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6da3e58f83712adffd801569c5ff434217fd8aec', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657070', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b6fb8a5221d748a2c0ac1383a702a43a25f18ca1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657076', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5b64c6a825f3f4dc50354e9d5805598f6ce055f4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657082', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ab412d05fc5e99cc0e83563e112903ac0a27c7a7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495657088', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7365b6f509d52ae35dfbdf489e109693909933d3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495675972', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e513d1daf6fa10dba837d1574e1f7566828ad4d5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676474', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0b44ce3725ae7f6f6143bc1cd855056f31a6911e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676482', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('29fdd94a33d42da41642d7c2a145e16b9a9ee31b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676488', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a1d74fd53860b2cd415b326983ef78c5a61525ae', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676493', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bd916513d95a4dca0b2e91306729c7b619bb8b08', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676499', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5a2d5926a0d00d0f4503a5eb113ad1fea9b529dc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676505', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2f6954b355ee041b2b32aac4a736d32fdad6d53e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676511', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7fbdee01d80cf7ddc2202b1dc2182f3a219b3092', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676521', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eaefcaaee3c5ead6cc025a26c2d693a427b788e7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495676733', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('417cc3d2299aa69eca7b2887ac2a22bb93fd4aa6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495677596', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5849044a2a1b229d62d41b8e2ac84cd07c1f8b53', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495677603', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7fbcd209ce29ced8a8d060d16c244acc51fd808d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495677607', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ff59838a05037965788d2d458d67feb9df59532d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495677611', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3870c48274e385f77394a8841952bc3a68b8671c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678505', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('05afae17d3fc6fa915d08e5b22949ee59926d650', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678512', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('633e6d8f285beb696c85d3177271cc9b52d299d8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678520', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1aa1b399dc6980e879837aa0d72ea7b1fd816e24', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678526', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('379cd49f7f1100bdc0b39c6c7961190b724c56cb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678530', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8047cff1567fd128e847c19318c4fa6044383333', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678535', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('24b9f7b18fd642cdbdc1f301ed37ae1b6643147f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678553', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('729dc2dcc1a6166b19c607b7c9bb23cc72271536', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678558', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('39ac5f7947fbe53253fcf92fcd2d333fba7aec06', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678562', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('487ed9cd508903c3dab29013dd3c4bfc8d81865e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678569', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3b4528117a0c89ed564c64e83c4192953c63fc81', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678573', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7f8088cce74ed52ef6c417390552bb98e8940f38', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678581', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('785c936eac3ec82e117333af9c92f8cc73cd82c7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678587', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('354624a98e53e83b266ccb40a46624b270f4e03b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678592', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('589b7c6021234ff4d6c482a0bdb5070000058339', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678597', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('744996de2c247c751f3112c69c3974d692f61a3c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678604', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('62c135dc98f535ac6d12b30aefe9932be0185c2c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495678724', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a788724e55330a48b188267e5fb5533535e7a4ec', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495679672', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('81f129ba2e94688ede928b9381c1363fbc72af5d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495679678', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('806e4d9f7c57520652cc97ebff80dd20731e0613', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495679684', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6ff995afc822ad7d8ffbe53b4963bdbcea63348d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495679689', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('654d5354dbdf61e3ef4b87ec9194ad1639894bb1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495679694', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f05fabd11de72f429860a992d6f8b220c7155964', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681407', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e7ba9558655d8235a2497ac02cf0a54c380c306e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681411', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4c7d4e62530ff977d0b3249cde9cbabba70563f6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681416', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4ecaa7b3471e07c555dde64e47ae3b210a02e00a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681426', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('37fc6bbe79b1c29754ea5d78fb85adf430562b82', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681432', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fb1db9f7e05e14990c246c563331eac30fef0ead', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681435', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('07a34b536d18f08b338d4cfdb3a21075c2e34f03', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681441', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7c52612698801b931505c91133694aabce3a2119', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681446', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f06602b733f11d976034ac508fb0b12ae0dee011', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681452', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c1ab36b110a2464eb693e59f199e4b48853d832c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681458', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5eb8d9e64799220ac7bacc274c2b7a98434c8d15', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681463', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3391f15ba89b0f7f718e1daa57165654a10b0e25', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681469', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('59c6f51e9e332ed2d55a7e191ebb3bdd41a4d03d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681476', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e0d665ff9a051bf1d0a56f7dff94cf10962b94c7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681482', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7abb70bc98d752dd8a891d5f14f06ade4a79387b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681485', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ce3da446b1bee616ccf2c65fce9f90649893b3ae', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681490', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d0bbb1b21c1fe3c1086d3af3c78ce80665d56158', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681496', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d0bc5affc2dcc7131e04dcdf5a45eec282dc3206', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495681507', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('588aa5a8de4b6521a25f1cc41fc27f3948065c44', '', '1495685774', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0de06a7703c81a782159d7b9900d3bd0d3cd2ab4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495685788', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f6cd6000a49fe312fd48398c9d7696047b8a75a3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495685798', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e19a5543e2690e7c864ca18339fb2c12efaf99b1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495685865', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c827398e5ac62f39b2d7077e84c755cb79700131', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495685874', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e19f2800de3198ba5cfbab86b798a6e159912295', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495685879', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3f13ff92eb1380700e825eaa8aea5b0a3d997932', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495685886', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d331066ae2800fa8f3cfd781e108de7644ddba30', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686484', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dddb159263be852e81abd35b4eff99ea578557da', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686489', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a930488e6a6dcb0d368d2b57aa65bc4446018c7b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686494', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c0db20d49c60fe51039a3bed187b6bdb7f6a29c1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686498', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('09408779b8320f02c020090aec7b7e4b06c0b526', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686503', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dd06bef3fa55cfcbd79dc9665b067d4e8b3e0c50', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686508', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('818a5e1509aa0abb70805682b3c1bed6b14299de', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686513', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4833daba5775964d79de824d5baf6c35330847d2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686517', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0a83d5385b294a0d63b4f66370b0f5d7465c2361', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686522', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a51cb4de7e0dfdae335920ed6005c4a314ec9fb4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686526', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7faebe58a7f979ea68210c89ca38733ab84d8675', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686531', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1f5a72ba2c3c3a1ff002744bddc4be5ee0e218a9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686535', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('85d760987891ff445e07bb47df97e86c7bef3ef3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686543', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b620ddcad9800127cab8b593cae73024ca1a0811', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686658', 'Menghapus Pelatihan Auditor 0aa0407d44be94ae9a9b81c5e17d24a55eac6b52', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ef3ab3c24dfb5b5d9c6f4ed4ada7cb65d057f26f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686801', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('34fb32cd739017a2d9298af6dd60e994a2d63318', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495686806', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('54229aee8cc046f729eb1ab5b8e05ea386b5fd95', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687466', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bf94b2557396bd03ba9e4177360f20da6ff29e8e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687470', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d5725be471b50f333b045d427e7172cf6665c5ae', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687480', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('49419c315f1bc1e9afc1e3599785b1fbe76e5692', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687486', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('38c9abdb4460669f5eb619de1f7017e75187b00b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687494', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8c9d8bb28b09d3746d03686c5560e71c7b47b6f5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687499', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d04cca4f7da1706c274b2bf75e4e8244201d4cb0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495687564', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('12965d22d878ec8ed6e59dc3ea3fe6e12b26cc6c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495696535', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('52d6be86f503766c2db495496d73fd9a40cd2796', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495696830', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6fdc6b6759392c8fde46308471f720c0480062b1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495696835', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2679a58bddf0ad2d0252e9cbd00b97285c207180', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495696840', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8048279c5d054183ed287daee47119aac1ad24a4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495696846', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2c32b011c73ce204e2efb2a3c2ba91a7621e666a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697494', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bcad9b6be60d5116617645d6c4d45f68bf005ea3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697500', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('336c03e7a1834f5d158545dad95835b5fcf30ef5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697509', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2618e3509d6dcb4072294cc8594054ca19779c52', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697517', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6c904502b3ed57633773b7ebb6ae015eac04ad44', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697522', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ebe5375569cf6d1e2a47a69f7787b0615944c23d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697529', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5328705b6d93668e23cb5193cffc7220661521fc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697534', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c3169bfd76c52ba4179718eae44c26ee492aeed3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697538', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a295ff9472fe2e52d224c2ba3fb4f1c24113b8a6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495697543', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('320a2bfdf11337972ceccba78a4f0fb404cb2c7c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495698982', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('da0f0717f6fffab4a40b310faf581b3814c8de80', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495698988', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d893979ea6c90ec86817199e664fdf32907fc7c6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495698993', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('943473441f9590e393e7ac1e50bd8ff05d023218', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495698998', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4562e3a99342e5f1b26912b44055af49a9c12dca', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703112', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cc637c7c3d46780830ba703a821ff4c302ef7d26', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703160', 'Menambah Auditee 9423 - STASIUN METEOROLOGI BETO AMBARI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('60abbee287902b42c17a53ef85d6640776f65386', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703219', 'Menambah Auditee 9424 - STASIUN METEOROLOGI SANGAI NI BANDERA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b9f20f1372c7b3e353ba1e6383fac5d5ffaa7c66', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703282', 'Menambah Auditee 9425 - STASIUN METEOROLOGI ANDI JEMMA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7eec61e05b263c6de180b949030bdb2347630364', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703325', 'Menambah Auditee 9426 - STASIUN METEOROLOGI NAHA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5f6b939da7e159b8433183529d0e340a7add6dd7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703382', 'Menambah Auditee 9427 - STASIUN METEOROLOGI PONGTIKU', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8649526b25e0609bb0b77f9ed6b5acee8aed4b9b', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703582', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1be9af5385f7b80efd59aa87dfa780f096c0098d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703734', 'Menambah Auditee 9428 - STASIUN KLIMATOLOGI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bf992bd3847e998c365c323e8d16e42a72ab6c37', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703812', 'Menambah Auditee 9429 - STASIUN KLIMATOLOGI MINAHASA UTARA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6a3c98d0213d8578bdf8b731c82b344b5fd843cf', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495703924', 'Menambah Auditee 9430 - STASIUN KLIMATOLOGI SERAM BAGIIAN BARAT', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d935ad4fa7f87be8cb0cbb37534503efec6d6b5c', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705235', 'Menambah Auditee 9431 - STASIUN KLIMATOLOGI RANOMEETO', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5b86e080be7b5296dd7b7730e71544c54dcc0f5d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705317', 'Menambah Auditee 9432 - STASIUN GEOFISIKA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ee313c707b223d334e84c1c3bdbc68af8968d3d1', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705395', 'Menambah Auditee 9433 - STASIUN GEOFISIKA WINANGUN', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6c98b5e8bbb44c7ab8fd712d8405664967f5ebfd', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705471', 'Menambah Auditee 9434 - STASIUN GEOFISIKA PALU', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c0d23e234cdf11f62c0f08789fe394e4027f58f9', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705520', 'Menambah Auditee 9435 - STASIUN GEOFISIKA GOWA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6f3980fa988c3128991df0334f197ae57acda2a7', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705568', 'Menambah Auditee 9436 - STASIUN GEOFISIKA GORONTALO', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d5535d798ffab84144dd76662feef7600bec005f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705656', 'Menambah Auditee 9437 - STASIUN GEOFISIKA SAUMLAKI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f101941fd7cf1c476bb2022ba986595873e8bb8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705710', 'Menambah Auditee 9438 - STASIUN GEOFISIKA TERNATE', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c283e1d21bbe15d566ef9030b42d71e9a2122b09', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705758', 'Menambah Auditee 9439 - STASIUN GEOFISIKA KENDARI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('774dd898378e864f8ee8cdb325ab77daa9c35aab', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705830', 'Menambah Auditee 9501 - STASIUN METEOROLOGI FRANS KAISIEPO', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('00235cbe290c7c0f0a18879042076e0f7a2e6663', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705896', 'Menambah Auditee 9502 - STASIUN METEOROLOGI SENTANI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3d1ba1570202f402249da733b651f1ea3fe41fbb', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495705960', 'Menambah Auditee 9503 - STASIUN METEOROLOGI SEIGUN', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d88bedf3c5582b4c56d163115ef3d7644ac28c64', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706011', 'Menambah Auditee 9504 - STASIUN METEOROLOGI MOPAH', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4ed29c102e5a8647bbb862b04ba742e1c1bbf67d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706080', 'Menambah Auditee 9505 - STASIUN METEOROLOGI MOZEZ KILANGIN', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0c2ac4d4ac8d34f695dd864ca4ca7976928b35af', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706146', 'Menambah Auditee 9506 - STASIUN METEOROLOGI TANAH MERAH', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e1e4b62552845b0243782db10246720bd312eab8', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706228', 'Menambah Auditee 9507 - STASIUN METEOROLOGI WAMENA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6370b38802c686fe56108a21fbbf6f424eafc607', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706293', 'Menambah Auditee 9508 - STASIUN METEOROLOGI MOANAMANI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('26cca688c3f40636b8356c748084db4d3d5ddd49', '', '1495706436', 'Menambah Auditee 9510 - STASIUN METEOROLOGI MARARENA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5a21dfc18467152bfb5a985f13d03e3846138b57', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706457', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1adeaa96e0410de5e26f09a1c25a1ff5557fbc45', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706521', 'Menambah Auditee 9511 - STASIUN METEOROLOGI ENAROTALI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a64133a9c070e59e143e0ec4d8b59b1badfe0117', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706580', 'Menambah Auditee 9512 - STASIUN METEOROLOGI DOK II JAYAPURA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d20b73243ee684d467d51d0fb0c8fe4198f77bcb', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706649', 'Menambah Auditee 9513 - STASIUN METEOROLOGI RENDANI', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b648840d49864c42fd7d5ce59ff64183014e3bd2', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706703', 'Menambah Auditee 9514 - STASIUN METEOROLOGI UTAROM', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('878918f89e042a37cf3ba9df84d88aefa5257dd6', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706769', 'Menambah Auditee 9515 - STASIUN METEOROLOGI TOREA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('01aafd2b6498148df8c06a7101abaaa812db496d', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706831', 'Menambah Auditee 9516 - STASIUN METEOROLOGI JAYAPURA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('27f5282a90c2d3264728b237b471dca3637144f4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706886', 'Menambah Auditee 9517 - STASIUN METEOROLOGI MANOKWARI SELATAN', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('776a4d727c58d482a5f4df82984bfbe3932f3fa4', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495706956', 'Menambah Auditee 9518 - STASIUN METEOROLOGI TANAH MIRING', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9c29844797ed6c195ddc74b0fda7de34d89d700f', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495707036', 'Menambah Auditee 9519 - STASIUN  KLIMATOLOGI ANGKASAPURA', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a386c544593c93555061d91623539115cf6707ca', '214c16bdb75e636b8b984a43f8f60cf044fa057e', '1495707123', 'Menambah Auditee 9250 - STASIUN GEOFISIKA NABIRE', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6c731a8b2caf716adde2527d03fe74b2d12f614b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495724695', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7558f694a598eb1254b12c1ea8944f2145ea07d7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495725300', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('65b881fc3dc97eef8bb4257caf845ffdef95fd79', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495725304', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('40f8bb0a2e70d1d58350f4bcad767bcbe0c71fca', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495725310', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f52630b4dbb338ec677ef4824291e435a3c91055', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495725315', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d47bfc0cdf04120a5888dc846549422b246cdbe7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495768222', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a2832d5d992b9cb1252c95680ee2ece5b9527d35', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495768490', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d5973dc8dbfa84373758fb241f6e8402a57fe13d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495768496', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('056f21d4dd7bf797f847bebee85c5cb42a2b5552', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495768502', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d1d4a4c44b9d49f63042a69527216af9181c7e23', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495768509', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('23425e05ae673fd540a4049091bf6ea2a743277f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495768514', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2dbe26756cf607bba98ecdacda2e7d4b3d5fccde', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769071', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7912f05c9ba741a69fe2d6d4fc2209de6bd30b51', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769078', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a8263b02640398bd1d95784785cba6e2924be8bb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769383', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6d0921dda0e45f2fd41c1c86fcd2b951d5873382', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769387', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e339b0e7feddd7dde9523d5e7746202254b8fb0a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769600', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a8e05c759cf5eae4dd67ccc86a6eb6eb6d6260fc', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769604', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('32020fdd835940eef2fd48b3ba9b32e8ddc9b8a9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769681', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('31d1003ccaa51a5053dee9b9345545bd68fa9e08', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769689', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b23dc3f81941504384568b69d78b96e3f8be5ce4', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769695', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f8cabf3c7652ddd1d51b290815ceddc5e8a6f0bf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495769699', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cf0dae6cd8663c0e4044b0db4c7b9f33d987e224', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495771144', 'User Login', '36.68.225.', 'Sukses')
<query>INSERT INTO log_activity VALUES ('edb146fc1f06ca20971da87bc29f5d2f65918543', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495772019', 'User Login', '36.68.225.', 'Sukses')
<query>INSERT INTO log_activity VALUES ('710bfdcbeed81f71059b68ea79c2986219089db7', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495772488', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9fab476171c1000b169d210f951155f468134df6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495773671', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('82278721fbafa24300f7e45fcbe3c3fee232014c', '18e9dfacc1d05e427c5fec795685bcb0616abaeb', '1495777869', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6e216807ce434726c7df13e346e73ae140424f75', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495778589', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c0c7e2fa5f31ee37dd5ac6ec74a2b88f8115be8e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495778980', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ad6bc21cdbe3912d38289fb48cb8bd4f2a4206b0', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495778988', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d4b8d84884cf2adadf070aa16a49fbdf5cb0ee09', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495778992', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c1313a4f8bc516021b90f9458dce79b6c6b83491', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495778996', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c6b200b0d2f09e8101b10e0ab2efc8ca5888d36a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779001', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c7317725724f859518778572f8796d9c5d4f3a79', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779005', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('500034db70baf51bdc1c6718ca183720c30add74', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779568', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a09c0adedf32ace5bb55aab5b92c9803b8e26c38', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779602', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7cf36ef6ca1117cf6c86c39e55a0cbf435ca5300', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779641', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('677664b9905fa257a9f7e2c7e8aee103182128e9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779715', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c234a1f32fdf88bbefc7a80c2156b0d4f87531b2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779762', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('366f5c02e42cefbd8198057f451f1408f6dd9bd8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779769', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e9cc6976cd4236ddf3b4a22bdc46d5cc56c7f329', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779775', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7dd7e0c8439162651cdb4ba24c2e9d7ac8016ef3', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779782', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bb01600c5e558a83b300a196395e171fc2cc6790', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779790', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3cb78009b61780bbd55476fd64bc6835047b83a6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779795', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5cd9e02c17d253f232e45b157afd73f5f0cdff46', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779852', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('15350bd2bde02df038b507a4bf1790ba99fe5f55', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779903', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e088bd683ffb729d1efee33682128c42bef31fb9', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779938', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9786fc4182600ad12631c6d1d189133851475b85', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495779993', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b56db3350cc04fa1056eb987ba26528c05617679', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780032', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('75892388c17e7c01d2105ef3296337e3d8489e68', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780067', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('91e381530119e0bce26ed962d52ecd04cd17255f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780108', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('558129ad2f1c47d3be271bafb9ccfa5ff53a429d', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780143', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('199f43059ea81134f0241e4fe896a87c05071004', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780179', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1a547980dd6f1099657e6cbcc7dfe183e1f5f1ee', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780225', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ed42b3828aff7fa75f97e1b873098f539bbcfe1e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780276', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('81e6d67152b1ffda3f1a21f75b2d03fb9acb5962', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780678', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dda89ed510bb91f604c37729bd483c12bd192152', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780719', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b025457799afa7007c4c01adcfaf8caf4cd74ee1', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780750', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9599f3c20f9b2685c69fedb10dbe4b69453bee18', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780771', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6473601dff8bf598806153c804e608143d92d382', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780787', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('47b0727f13ffa865dbbd2ef1089ad484a1044c01', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780808', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b3dd141aa5b3b463342c580fcbb4bfb73c8c4b59', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780844', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0e5e8eb32c8ea4f80add16344ed79c6a60a862c2', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780908', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('61c00d2ea1fa8ffc88875f938b0e56ebaf0667fb', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495780945', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3a96b9602e2337a3eb24020500ff9b300d960506', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495783316', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('31a5ea64ddb87a4ba2be420d374a0a7b68e091fd', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495784079', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c66f524866d240a5036a51f7404dbb74b65c8391', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495784084', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f0e90f17b2708946412512e79516cd2432d39994', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495784089', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6ef6f7e1621024604adbe3c4d11d412183b44d0a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495784096', 'Menambah Pendidikan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4043edbd0ecd66080cc31d68e1f373055ac7793b', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495785239', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('332ab301639e2f70f01cd5dcaaa705665ff75836', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495785261', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5ff7cdde735a6eca967bba3b134dcb9675ea49ec', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495785289', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('10a230ba089a1870f1559bbd291bc8e8610f1cbf', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495785318', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bb27cecdf255b27aa279bbd95288265e4967f5ed', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495785391', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6cc855b81d66fffbb99db4005e2426fd6c020b1a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495786095', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3827195030282698f889237213c639df124bf712', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495786623', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('313425fbe905a1138c4b43123e6a2ef5f1775e2a', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495787187', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8d279798556ffe65ec915abb1bad2055b20ce0ee', '18e9dfacc1d05e427c5fec795685bcb0616abaeb', '1495792286', 'User Login', '125.161.53', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1efa155e95b3ce6a403037b54cedcc858c4bd90b', '', '1495794872', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5c70610cdf12c9ce22b16e514cb602d80b50c9ca', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495794885', 'User Login', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e16aa0563cf715e9fd37ba3bc8a2066bf9e3debd', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495794895', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7480e572946acaec3377864015998e3c91fe1eea', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495794922', 'Menghapus Pelatihan Auditor c21c7b53b36df5659d5f734137b8c7f319877f8b', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a43dc128dd66c3123506fb419ca6b719e890b882', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495795258', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c9cfee94ddcb44398034e2a9f5f1f367e99aa06c', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495795305', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f2eb27d3c490d3cf09d1706535b02f15f55cc10e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495795347', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8d64bf45c72ceb001a2cc5d740801055a0662a39', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495795485', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('71de51cf0e9d6ac5455bc316b54efb7f9e58b2d6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495795588', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ec652a82d59c1b3a882d0906315bc673439e1a8f', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495797687', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6b55b478bec8f5fbae2e10b7046b9605de7ca3ec', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495797735', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5c4620e3f4153d5f2c7c5e83d1da96f0f4355b46', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495797778', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3da0e61e4a3d31771abb9a8cf4421a3d344ef3c6', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798085', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('df42dbd56c75fe26cf9d88a65ee3843b980ddead', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798089', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('67031e99af6635dd2f0932af77e9ad187a7698c5', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798104', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0acf4d21b143e7e766e7c0120587a88d81ee8538', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798109', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('100d4c7ed299525323f4bc38168847efc62fd011', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798113', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8f48c659979ec4abdbbc65f343d551b3c3632c2e', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798117', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('db5d9bf67b155c8cc61f15067a71259548310cc8', '144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', '1495798123', 'Menambah Pelatihan Auditor', '139.194.22', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f367e1e285f8ff4f2dbf441035b18c534fa3c86e', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495805220', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('901013a866fb58e27168110f4ca53b60bbc98e31', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495805256', 'User Logout', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('801de35d66dde429bee38fca3f9aefab64549e55', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954079', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2add9a013415f1f75d80a09cfea52c37f3165f5d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954112', 'Mengubah Parameter Tipe Audit dengan ID 0525307297f9bed8984a8054918087dcf03c4bcf', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d19996deb18c8a644c2a799b5ce90b211d6fab93', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954120', 'Mengubah Parameter Tipe Audit dengan ID 4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ed32ab8091db05afe008e1ffada3b2e59e32d6cd', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954128', 'Mengubah Parameter Tipe Audit dengan ID d5aaabf0d82997143dc645655cbc911b4b5de4f4', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ad15c51612646c926bf82ba424f4f857a3792ca9', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954137', 'Mengubah Parameter Tipe Audit dengan ID 98cd0ca731da106235cc474a7608ad5750d8e1e8', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b53678deeb0c220b36320d3436edef72585719e4', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954144', 'Mengubah Parameter Tipe Audit dengan ID 9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c550d8a1f5bc3b13c63059fe091901795a8a1757', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954156', 'Menambah Parameter Tipe Audit Reviu LK', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('7ea73276f87219efe9ec23e0ea10a739f759b4a7', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954172', 'Menambah Parameter Tipe Audit Monitoring TL', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a3fd325d0fbf6e420500d1953303b2187e6c8da1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954184', 'Menambah Parameter Tipe Audit Sosialisasi', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('94e9d87ccce81ceb4a05f9338373443b0a2d9315', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954195', 'Menambah Parameter Tipe Audit DIKLAT', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('992fd5d3309ccaa0b02ed86b237d1e5303af9edc', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954211', 'Menambah Parameter Tipe Audit Bimbingan Teknis', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6737da0c8a3563b56d66e6c9999ce79ef467e608', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495954328', 'Menghapus Auditor ID a112e87849f362320a05ec7234fe88826d98e37e', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ed9e393b3bb04529c28e2c65386d2afaaf869e13', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495960076', 'Menambah anggota dengan id 216cfca9cb2caf33caaf0481514bf57975dcccf3 pada penugasan id a51852730078d23e3b48bf5972885364a11b17b8', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e0a931a8d75877be4b2f8354bf3e46f3ea21a716', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495965909', 'Mengubah Referensi Audit dengan ID 15d660386bb6c6f217af2d6fb7445fd0c2ec3bf3', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9f7b63ef4df022fbaf46076b5e5ca919eeea66de', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495970691', 'Menambah Plan ID 49189a84e0e04133b0ba806a407594dfc0bbe489', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d42af1d3e4ea0d3cd65d82f112c83f6a71e633c5', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495972889', 'Mengubah Parameter Bidang Subtansi dengan ID 688d723bd69b2d06980b519ca49badc09f16ee4d', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('165629c74e14d1be5f2a719752e18421fa44f8f1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973048', 'Mengubah Parameter Bidang Subtansi dengan ID 3c7f1096de7da99484a42852da60792858994c60', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b1ef798bc0cdc7e547e198627e32c2cae25d6e76', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973139', 'Mengubah Parameter Bidang Subtansi dengan ID cd1d3d9ba16a8fd8eadb351dcfe6ef8dcea417d0', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('cff45aa813336363bdb83a28fa6c3574593e7b66', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973169', 'Mengubah Parameter Bidang Subtansi dengan ID 6031fc656ebe93a64d401dc66eace039c475389e', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2278bf1819323f6bbda2cd45c7e466d8cb752152', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973227', 'Mengubah Parameter Bidang Subtansi dengan ID 8f82a264012563a6e9b4105025bb5869d35a34a5', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9bdb446281b77b66c4697ca8269952d5376ee7ca', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973240', 'Mengubah Parameter Bidang Subtansi dengan ID 460d0b13f7a9cb51e5391985bd42f8ea4afbef2c', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ebf57af67789dd566088f4fed5161c2318c64f82', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973289', 'Mengubah Parameter Bidang Subtansi dengan ID 462599baf768cd3bb1bc6ebad7a36d338206c884', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('52b1107b1554d52878ccdd84d3b21959d2799eb2', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495973431', 'Mengubah Parameter Bidang Subtansi dengan ID 3da92cabe8a8473f55e638fa7dc43ea1753f40e8', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1e650f15a33b90748a6e841043132dda0f11ec2a', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1495978923', 'Menghapus Perencanaan Audit dengan ID 49189a84e0e04133b0ba806a407594dfc0bbe489', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('51189b119835060ecb897383aa91fd250b16c08f', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496026163', 'User Logout', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('540695b23adc65dca17fe13a3884f517bcbf0826', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496026578', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('d285903cfd1e9dfd7097d90d9a11626dcec90ecd', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496028002', 'Mengubah Data Auditee dengan ID 539d74c81ea7696615c9e7e21a3d2554e7021350', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('57ef46fcbead3c2cf211f2ca06ec0a34d95b7d70', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496028021', 'Mengubah Data Auditee dengan ID 539d74c81ea7696615c9e7e21a3d2554e7021350', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ac8dc10169950d3daed7bcee7e2049020c9ddfc4', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496028466', 'Menambah Plan ID 54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9c5ce26df9b7bebfeb72ae14054f6f46cf84b50c', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496028485', 'Mengubah Perencanaan Audit dengan ID 54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8d0347520c77cc608eca671071cf01c7aaa779ee', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496028652', 'Menambah anggota dengan id d927a6cefead3b3df74e50aed0720228288184b2 pada planning id 54e1cdc4c51f40a03fbe796b4bc45b95d746ae93', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('bc07e4087b47fbb531f223c9a0170e586f2eef97', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029381', 'Menambah anggota dengan id 8c8a11984df453151ab980738dcef5db7cdd299e pada penugasan id 2039f5600474aed17121d5c58da0e05f57953905', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('25e16952c64b069bc2f13a30403e6b77500a7f29', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029844', 'Mengubah Penugasan Audit dengan ID 2039f5600474aed17121d5c58da0e05f57953905', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('de5ab67983e0aa900f56dbe779bde4ecd84f94c5', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029891', 'Mengubah Status Penugasan Audit dengan ID 2039f5600474aed17121d5c58da0e05f57953905 menjadi 1', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3eceb55da74e267c8fc866d157cc52d016f3ef22', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029891', 'Mengomentari Penugasan dengan ID 2039f5600474aed17121d5c58da0e05f57953905', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('445ee3e289ae49293d4a7f49ae96515562cce911', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029908', 'Mengubah Status Penugasan Audit dengan ID 2039f5600474aed17121d5c58da0e05f57953905 menjadi 2', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8fccd139e77bf3e5e7b0822b60699c118e16a0c8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029908', 'Mengomentari Penugasan dengan ID 2039f5600474aed17121d5c58da0e05f57953905', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a0cce340b2eb6198e2b5bc62e36e9828903ca042', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496029971', 'Mengubah Surat Tugas dengan ID a5ba7d503bccd786b4624a08c9a97b26f5362705', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1e6b59f62cd1d501599bbc61e0062f4a6ce39794', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496030008', 'Mengubah Surat Tugas dengan ID a5ba7d503bccd786b4624a08c9a97b26f5362705', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4c791ad5c281cb0b6c659343d1029ff1879a4eaa', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496030295', 'Menambah Program Audit ID ab0e7aa6f7a329b3848ad9e234a95b133728a99f', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ec66cb3b1099e5ad20a82cd73cd47f2a92822d47', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496030295', 'Menambah Program Audit ID ac7e46a332f19a5b301f5fe1769cfb82a5f754cc', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2d225f8e5850702a90f1c1d034d62c91cfec2b9d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496030295', 'Menambah Program Audit ID dc4e23db077948a7061ed96b146da5852ca1c81f', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4d0a83946d285d8806e1b0649628aa7473a5d548', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496030546', 'Menambah Program Audit ID 56a24b6cdcd82116d9ac39c387a5174f47df7c4b', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('9ad854dd8661f6957df0860f83cb0ebe447d783b', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496032041', 'Mengubah Data Auditee dengan ID 03b3980371bfbe3ef823da11f3162998de3cf0e6', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('767b229c910e6de6e6b76b987a2c3034b8cfa793', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496032482', 'Menambah Kertas Kerja Dengan ID c72fc7e636ae05fd2f7155090378ca7a427c335f', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('24f289a2660e9aee633b52f7ec7c04055b957b5a', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496032906', 'Menambah Plan ID 953204d3d79e2de0903ea35521570bfaa1f1718d', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fc18f6cb6d12535089f410208df1a50a27c2c6f5', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496032952', 'Menambah anggota dengan id e329c92b1fbb4eb45d4f125278860fd261490f43 pada planning id 953204d3d79e2de0903ea35521570bfaa1f1718d', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0c58f3361b26c34a47c33742e061daa875934272', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496033186', 'Mengubah Perencanaan Audit dengan ID 953204d3d79e2de0903ea35521570bfaa1f1718d', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3f50c41e54ac060b6062d0510c85ad95b9ec3981', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496035704', 'Menambah Referensi Program Audit AKeu - 01', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('fb655e6dceb87f3b8d4d6f98aab026a392f9b7bb', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496035842', 'Menambah Referensi Program Audit SARPRAS - 01', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('ea5e2d23a8132c7c4611fa7d4b1ec6175a3d8ea8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496036560', 'Mengubah Data Auditee dengan ID a86d544038ea986947b40fc3aac761a9e0ec94de', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('34107789b7ec8238cb769ef1a010db54f9f15c47', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496037607', 'Menambah Program Audit ID 143a447b7f6eda81db2d9bf606aebeada071ea93', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e1c2ba217ca6533055dc0bf39ed8ee64063b420d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496037607', 'Menambah Program Audit ID bb899eb02e705a1d2210e74091151e60e86c31c3', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3fddc89192b7410f592e4c8d14a802a5d18dfdf0', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496037607', 'Menambah Program Audit ID e89443c7cb18a9543250d8b190e490a3b3625efa', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('491e0ea212eed9c6d68d8a4b19dab8b82a888f7e', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496037754', 'Menambah Kertas Kerja Dengan ID c72fc7e636ae05fd2f7155090378ca7a427c335f', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f6edadf0b3fb4fa9f68101a92cef01a471070fe5', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496038150', 'Mengomentari Kertas Kerja dengan ID 4fb99972d03d6ea623c22b797177e2e0639d4e62', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a6a07f3e9278b16e924618e3b7e97300cdf97597', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496038208', 'Mengomentari Kertas Kerja dengan ID 4fb99972d03d6ea623c22b797177e2e0639d4e62', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('24842c540d93da777f191194089db2696903e8d3', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496038729', 'Menambah Temuan No 01', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a89bcd04fc90041d98f2928d4880ba85584f8d8c', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496039504', 'Mengubah Parameter Kode Penyebab dengan ID a3e829db763976063f2f84d10b6a95686359d2cc', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3798bcf3ebe658cc6bdc686b284708ca5bdeee5d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496039519', 'Mengubah Parameter Kode Penyebab dengan ID eb1d12f6fe6e66fcdd5a3ffc16c3a5a2de6af316', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('97528903712ea6b7ca98097301810f8e3e423c66', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496039533', 'Menambah Parameter Kode Penyebab 03', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('86f7fbd87e212d0e0d5e05256a44c81db57dbded', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496040158', 'Mengubah LHA Penugasan AUdit dengan ID a51852730078d23e3b48bf5972885364a11b17b8', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('dae50c9a664750391f0702ad153800d76662b66d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496040384', 'Mengubah Parameter Tipe Audit dengan ID 0525307297f9bed8984a8054918087dcf03c4bcf', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0bf062c6d4cd7ec0fff6bb36ca2e92f86fc0d022', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496040394', 'Mengubah Parameter Tipe Audit dengan ID 4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('b4ee66baa44e9f6e9bddd2556fa90189542c831d', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496040417', 'Mengubah Parameter Tipe Audit dengan ID 98cd0ca731da106235cc474a7608ad5750d8e1e8', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('4b384adb709eb0274d9a23fb09db8657b0386391', '', '1496041337', 'User Logout', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('f7f8dc735b81a191fd5ab3f931773b5931145516', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496126621', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8edfb4a49a0cbecd0600263c05d8032c52ef0f24', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496127423', 'Mengomentari Kertas Kerja dengan ID b13f78684cf218c66602d52e0af87da107daca46', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f30344a30138d5ca15686446cd583c6ddabb821', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129285', 'Mengubah Penugasan Audit dengan ID ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('e8f881f26be1ac59571ae675ed98b4a8fbcd2b1f', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129300', 'Mengubah Status Penugasan Audit dengan ID ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e menjadi 1', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('0f779f7dbfede55df007e0799664af30c2f14b26', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129300', 'Mengomentari Penugasan dengan ID ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('8051c01675d9bd6482dabca5d44d237035079487', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129405', 'Mengubah Status Penugasan Audit dengan ID ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e menjadi 2', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('344d17dc940e020896b081f896e2adb9351fd898', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496129405', 'Mengomentari Penugasan dengan ID ee2f1e76c554ab07b62bf36fe7e0359e6deafd2e', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('3fad44b7e84a42cc3e9099764f6895394e54400b', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496821197', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('537f999a3a9847bbcbcf5334f58eff0ac8eefc0b', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496821216', 'Mengubah Data Group ID a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('903b70ea1bb1c001f98d814fa66a2db6fe2f85a0', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496822893', 'User Logout', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('105198da4eb5fa3cd164319c77e72c1a631e457f', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1496914315', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('69c661ecb02f3c56953f67838042cb65a41ceca4', '', '1497167667', 'User Logout', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1fbd234114dd41bde0370340303ba3e65fe4a841', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1498013423', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('c77083e7187ffe7632402623bdb4c1055e3e366a', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1498014056', 'User Logout', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('2bf5d62a93e79e66c8bf31cdd156aab9a7c60da3', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1498014629', 'User Login', '127.0.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('1d2b6ba212afb0d6ccaea0971824560343d7481f', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498014795', 'User Login', '172.19.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('31729973d9818e7245482ef102d3d5c8e9d319be', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498015607', 'User Logout', '172.19.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('5d0e027b1cf3e1248fec0e28175573dcb18005b4', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498016204', 'User Login', '172.19.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('a0c2dadfff17c6c05923ae171a35dfcba077e628', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498016510', 'User Logout', '172.19.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('eb35bdc43455b65a40daddf10776162e3f77f68e', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498016519', 'User Login', '172.19.0.1', 'Sukses')
<query>INSERT INTO log_activity VALUES ('6ba7d6adb60b2f4b5e64fbe5c70a7a6aab790447', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498017063', 'Backup Database', '172.19.0.1', 'Sukses')
<query>DROP TABLE IF EXISTS login_expired
<query>CREATE TABLE `login_expired` (
  `login_exp_id` varchar(50) NOT NULL,
  `login_exp_id_user` varchar(50) NOT NULL,
  `login_exp_last_acces` int(11) NOT NULL DEFAULT '0',
  `login_exp_ip` varchar(50) NOT NULL,
  PRIMARY KEY (`login_exp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO login_expired VALUES ('7fd339a28e15e772a1ddfcd9a6f0133f797da0cb', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1498014629', 'DESKTOP-B8DUDJJ')
<query>INSERT INTO login_expired VALUES ('68dbc306ba0c0843c756303e2b903fc6fabe201c', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1498016519', '172.19.0.11')
<query>DROP TABLE IF EXISTS notification
<query>CREATE TABLE `notification` (
  `notif_id` varchar(50) NOT NULL,
  `notif_data_id` varchar(50) NOT NULL,
  `notif_from_user_id` varchar(50) NOT NULL,
  `notif_to_user_id` varchar(50) NOT NULL,
  `notif_desc` varchar(255) NOT NULL,
  `notif_method` varchar(50) NOT NULL,
  `notif_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notif_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO notification VALUES ('84d930a4791875092c6f08aac5f01c2a2fcb3016', 'b13f78684cf218c66602d52e0af87da107daca46', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '(Kertas Kerja)...dafa', 'kertas_kerja', '2017-05-30 13:57:03')
<query>DROP TABLE IF EXISTS par_aspek
<query>CREATE TABLE `par_aspek` (
  `aspek_id` varchar(50) NOT NULL,
  `aspek_name` varchar(100) NOT NULL,
  `aspek_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=del 1=aktif',
  PRIMARY KEY (`aspek_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_aspek VALUES ('c37ccd97c72da477e51f3b71809016a5f13a5765', 'Kesekretariatan', '0')
<query>INSERT INTO par_aspek VALUES ('3c7f1096de7da99484a42852da60792858994c60', 'Perikanan Tangkap', '0')
<query>INSERT INTO par_aspek VALUES ('6031fc656ebe93a64d401dc66eace039c475389e', 'Perikanan Budidaya', '0')
<query>INSERT INTO par_aspek VALUES ('8f82a264012563a6e9b4105025bb5869d35a34a5', 'Pengolahan dan Pemasaran Hasil Perikanan', '0')
<query>INSERT INTO par_aspek VALUES ('460d0b13f7a9cb51e5391985bd42f8ea4afbef2c', 'Kelautan, Pesisir, dan Pulau-Pulau Kecil', '0')
<query>INSERT INTO par_aspek VALUES ('cd1d3d9ba16a8fd8eadb351dcfe6ef8dcea417d0', 'Pengawasan Sumber Daya Kelautan dan Perikanan', '0')
<query>INSERT INTO par_aspek VALUES ('462599baf768cd3bb1bc6ebad7a36d338206c884', 'Penelitian dan Pengembangan Kelautan dan Perikanan', '0')
<query>INSERT INTO par_aspek VALUES ('688d723bd69b2d06980b519ca49badc09f16ee4d', 'Pengembangan Sumber Daya Manusia Kelautan dan Perikanan', '0')
<query>INSERT INTO par_aspek VALUES ('3da92cabe8a8473f55e638fa7dc43ea1753f40e8', 'Karantina Ikan, Pengendalian Mutu, dan Keamanan Hasil Perikanan', '0')
<query>INSERT INTO par_aspek VALUES ('277f1441ce02dbf662e6feb6c669b603a4de35d0', 'ASPEK KEUANGAN', '1')
<query>INSERT INTO par_aspek VALUES ('4db91f046b49f91c14736ad0e82d31c931ce475c', 'ASPEK SARANA DAN PRASARANA', '1')
<query>INSERT INTO par_aspek VALUES ('5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'ASPEK SUMBER DAYA MANUSIA', '1')
<query>INSERT INTO par_aspek VALUES ('8134168f4fda3dafeb174cb7230aca8ce77c2f8b', 'ASPEK TUGAS POKOK DAN FUNGSI', '1')
<query>INSERT INTO par_aspek VALUES ('6d46ae583029560f8debf0fce2036586e97cea7a', 'ASPEK SISTEM PENGENDALIAN INTERN', '1')
<query>INSERT INTO par_aspek VALUES ('e7ee08c5cb7ef028130b0a7d0a0209ba4da86628', 'ASPEK SURVEY PENDAHULUAN', '1')
<query>INSERT INTO par_aspek VALUES ('d620b3fd0164fa5eb03125e497e9aca5f5da3f7a', 'ASPEK AUDIT RINCI/LANJUTAN', '1')
<query>INSERT INTO par_aspek VALUES ('cd4a7c581d450985ff742da4320c12f3bccf3068', 'ASPEK KEWAJARAN HARGA PENGADAAN BARANG/ JASA', '1')
<query>INSERT INTO par_aspek VALUES ('60db6ee43d3d93ea44b62a93ddfa0515badda1ed', 'ASPEK KETEPATAN KUANTITAS PENGADAAN BARANG/ JASA', '1')
<query>INSERT INTO par_aspek VALUES ('2bb0db84a9b8fe6868308a4a186ef79b82ec1ceb', 'ASPEK PERENCANAAN PENGADAAN BARANG/ JASA', '1')
<query>INSERT INTO par_aspek VALUES ('8ebf1eace7c59cc969fbd0734c195aeeeaf9a7b0', 'ASPEK KETAATAN PELAKSANAAN  PROSEDUR PENGADAAN BARANG/ JASA', '1')
<query>DROP TABLE IF EXISTS par_audit_akses
<query>CREATE TABLE `par_audit_akses` (
  `akses_id` varchar(5) NOT NULL,
  `akses_menu` varchar(50) NOT NULL,
  `akses_name` varchar(50) NOT NULL,
  `akses_sort` int(11) NOT NULL,
  PRIMARY KEY (`akses_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_audit_akses VALUES ('1', 'Kertas Kerja', 'Reviu Pengendali Teknis', '2')
<query>INSERT INTO par_audit_akses VALUES ('2', 'Kertas Kerja', 'Reviu Pengendali Mutu', '3')
<query>INSERT INTO par_audit_akses VALUES ('3', 'Kertas Kerja', 'Reviu Ketua Tim', '1')
<query>DROP TABLE IF EXISTS par_audit_type
<query>CREATE TABLE `par_audit_type` (
  `audit_type_id` varchar(50) NOT NULL,
  `audit_type_name` varchar(100) NOT NULL,
  `audit_type_code` varchar(10) NOT NULL,
  `audit_type_desc` varchar(255) NOT NULL,
  `audit_type_opsi` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=pemeriksaan, 2=non pemeriksaan, 3=other',
  `audit_type_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`audit_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_audit_type VALUES ('0525307297f9bed8984a8054918087dcf03c4bcf', 'Audit Kinerja', '', 'KIN', '1', '1')
<query>INSERT INTO par_audit_type VALUES ('4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'Audit Dengan Tujuan Tertentu', '', 'KHS', '1', '1')
<query>INSERT INTO par_audit_type VALUES ('04090de5ed0699ed0293ef73d3bb61d73388e072', 'Audit Dana Dekonsentrasi/TP', '', 'Audit Dana Dekonsentrasi/TP', '0', '0')
<query>INSERT INTO par_audit_type VALUES ('d5aaabf0d82997143dc645655cbc911b4b5de4f4', 'Audit Keuangan', '', 'Audit atas laporan keuangan', '1', '1')
<query>INSERT INTO par_audit_type VALUES ('98cd0ca731da106235cc474a7608ad5750d8e1e8', 'Audit PBJ', '', 'PBJ', '1', '1')
<query>INSERT INTO par_audit_type VALUES ('9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'Audit Operasional', '', '', '1', '1')
<query>INSERT INTO par_audit_type VALUES ('efe9165cb8857eb7822a18c0aae62c36887e3615', 'Reviu LK', '', '', '2', '1')
<query>INSERT INTO par_audit_type VALUES ('7a08823fc15fb164dc0b3cb03664fe3fb1becfc0', 'Monitoring TL', '', '', '2', '1')
<query>INSERT INTO par_audit_type VALUES ('6f0349821c12229d4a7a74f1a86292c73fe105a5', 'Sosialisasi', '', '', '3', '1')
<query>INSERT INTO par_audit_type VALUES ('69b2d4fcc8b0fa804740c2c1f6d99db2fa5357ae', 'DIKLAT', '', '', '3', '1')
<query>INSERT INTO par_audit_type VALUES ('95c4470bc64a292858d57bb1fc312fb370af5064', 'Bimbingan Teknis', '', '', '3', '1')
<query>DROP TABLE IF EXISTS par_esselon
<query>CREATE TABLE `par_esselon` (
  `esselon_id` varchar(50) NOT NULL,
  `esselon_name` varchar(100) NOT NULL,
  `esselon_del_st` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`esselon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_esselon VALUES ('93a401be9c1cb44114e0432f9c31d6a2a662a4fb', 'Sekretariat Jenderal', '1')
<query>INSERT INTO par_esselon VALUES ('a8963b8aa3608dcb206926988c3b9ca91d495e0f', 'Direktorat Jenderal Perikanan Tangkap', '1')
<query>INSERT INTO par_esselon VALUES ('1c56ae798a24600a89c638869b8b9d08f1decc3f', 'Direktorat Jenderal Perikanan Budidaya', '1')
<query>INSERT INTO par_esselon VALUES ('81f350a05bf5797a609f2e9bca287db694a9ab49', 'Direktorat Jenderal PRL', '1')
<query>INSERT INTO par_esselon VALUES ('08029bd17bc2cf8a514c3f363be14d491c65362b', 'Direktorat Jenderal PDSPKP', '1')
<query>INSERT INTO par_esselon VALUES ('c837ebf11517f831fd0f0d7fc8a17567ac72c870', 'Direktorat Jenderal PSDKP', '1')
<query>INSERT INTO par_esselon VALUES ('751a275a83fdaffebc9fd9fb48bc56f9e7c18b20', 'Balitbang Kelautan dan Perikanan', '1')
<query>INSERT INTO par_esselon VALUES ('7f7476dc4fd08ba36ea4ba72f00388690aade41d', 'BPSDM Kelautan dan Perikanan', '1')
<query>INSERT INTO par_esselon VALUES ('d66937adea70f1831c86981aaa6445af65b6081e', 'Badan Karantina Ikan dan Pengendalian Mutu dan Keamanan Hasil Perikanan', '1')
<query>INSERT INTO par_esselon VALUES ('3a25c613197b2199ec9b2ead9ac4946379fc5b5d', 'Inspektorat Jenderal', '1')
<query>DROP TABLE IF EXISTS par_finding_jenis
<query>CREATE TABLE `par_finding_jenis` (
  `jenis_temuan_id` varchar(50) NOT NULL,
  `jenis_temuan_id_sub_type` varchar(50) NOT NULL,
  `jenis_temuan_code` varchar(10) NOT NULL,
  `jenis_temuan_name` varchar(100) NOT NULL,
  `jenis_temuan_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jenis_temuan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_finding_jenis VALUES ('31b8aebd0211b5870fe51e6df8b5694719d3a9a4', 'e950a0afcb65d466058c5ee19e5adb0646c9769f', '01', 'Belanja dan/atau pengadaan barang/jasa fiktif', '1')
<query>INSERT INTO par_finding_jenis VALUES ('11582751dcdcb06ec24e8f76ddbc911d93813eaa', 'e950a0afcb65d466058c5ee19e5adb0646c9769f', '02', 'Rekanan pengadaan barang/jasa tidak menyelesaikan pekerjaan', '1')
<query>INSERT INTO par_finding_jenis VALUES ('29452ebdf92be9fe582e620fa0dd4898349e3448', 'e950a0afcb65d466058c5ee19e5adb0646c9769f', '03', 'Kekurangan volume pekerjaan dan/atau barang', '1')
<query>INSERT INTO par_finding_jenis VALUES ('ab4479b9627e5e983c20306c0f61c2367255fac1', 'e950a0afcb65d466058c5ee19e5adb0646c9769f', '04', 'Kelebihan pembayaran selain kekurangan volume pekerjaan dan/atau barang', '1')
<query>INSERT INTO par_finding_jenis VALUES ('3d2ac3ad074db3e4292954ce1778d635defdd614', '34e821421283c122b2a7105914b5ee6971b7dcfb', '01', 'Kelebihan pembayaran dalam pengadaan barang/jasa tetapi pembayaran pekerjaan belum dilakukan sebagia', '0')
<query>INSERT INTO par_finding_jenis VALUES ('76b4c3f0470e53a774c3522c66f44fbd398c2ae2', '10f3ef2b5e13c9e2662166c432ebc5de8195c1a6', '02', 'Penggunaan langsung penerimaan negara/daerah', '0')
<query>INSERT INTO par_finding_jenis VALUES ('d2a780022cae0fa88ab83f165728e078ab802cc8', '10f3ef2b5e13c9e2662166c432ebc5de8195c1a6', '07', 'Kelebihan pembayaran subsidi oleh pemerintah', '0')
<query>INSERT INTO par_finding_jenis VALUES ('5475064d43ff9b7a21941a06ba2cdc339aa6fd9f', '5c6ae8397e1953e1a2f524bfb8ed95f1d18fb9e2', '02', 'Pekerjaan dilaksanakan mendahului kontrak atau penetapan anggaran', '0')
<query>INSERT INTO par_finding_jenis VALUES ('f2ab012e3dbd11b716349354496b345cd6eaa3ec', '6800e36ea2ac83657c02127de8ca71908072d7cf', '01', 'Pencatatan tidak/belum dilakukan atau tidak akurat', '0')
<query>INSERT INTO par_finding_jenis VALUES ('f1eb355bc9d23e830eb60680f7b64d34a49edf9c', '231b6954045f49c0738fdc9e4c09ed7935ad5d2a', '01', 'Perencanaan kegiatan tidak memadai', '0')
<query>INSERT INTO par_finding_jenis VALUES ('6fe55674b39518c8194114401b00b5f27cab080e', '02cc05566a58319721f5a112b924ae83f863b6bd', '01', 'Pengadaan barang/jasa melebihi kebutuhan', '0')
<query>INSERT INTO par_finding_jenis VALUES ('ed78242071f82d310d08c29f6192bfc0aad455d5', '704863370a5444d29b9cb88e6bfbabc74572b074', '02', 'Penggunaan kualitas input untuk satu satuan output lebih tinggi dari seharusnya', '0')
<query>INSERT INTO par_finding_jenis VALUES ('2a889cc1e20fffa4336572bcca6a53f92ceb9ca3', '1c4fa91c754e6a03ab4c67012c9eea27308768e6', '02', 'Pemanfaatan barang/jasa dilakukan tidak sesuai dengan rencana yang ditetapkan', '0')
<query>INSERT INTO par_finding_jenis VALUES ('73b8db1cd9df51f8e2cdce50d7f3267f31703afe', 'e950a0afcb65d466058c5ee19e5adb0646c9769f', '05', 'Pemahalan harga (Mark up)', '1')
<query>DROP TABLE IF EXISTS par_finding_sub_type
<query>CREATE TABLE `par_finding_sub_type` (
  `sub_type_id` varchar(50) NOT NULL,
  `sub_type_id_type` varchar(50) NOT NULL,
  `sub_type_code` varchar(10) NOT NULL,
  `sub_type_name` varchar(100) NOT NULL,
  `sub_type_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sub_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_finding_sub_type VALUES ('e950a0afcb65d466058c5ee19e5adb0646c9769f', '764275e74b69abcec96c2ac76140f7da774249f7', '01', 'Kerugian negara/daerah atau kerugian negara/daerah yang terjadi pada perusahaan milik negara/daerah', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('5c6ae8397e1953e1a2f524bfb8ed95f1d18fb9e2', '764275e74b69abcec96c2ac76140f7da774249f7', '04', 'Administrasi', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('34e821421283c122b2a7105914b5ee6971b7dcfb', '764275e74b69abcec96c2ac76140f7da774249f7', '02', 'Potensi kerugian negara/daerah atau kerugian negara/daerah yang terjadi pada perusahaan milik negara', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('10f3ef2b5e13c9e2662166c432ebc5de8195c1a6', '764275e74b69abcec96c2ac76140f7da774249f7', '03', 'Kekurangan penerimaan negara/daerah atau perusahaan milik negara/daerah', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('3afc446539c280ff0b767a634ec6847cc881e6e6', '764275e74b69abcec96c2ac76140f7da774249f7', '05', 'Indikasi tindak pidana', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('6800e36ea2ac83657c02127de8ca71908072d7cf', 'dc06264262364cb2318c3282ed7590b1aa45b779', '01', 'Kelemahan sistem pengendalian akuntansi dan pelaporan', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('231b6954045f49c0738fdc9e4c09ed7935ad5d2a', 'dc06264262364cb2318c3282ed7590b1aa45b779', '02', 'Kelemahan sistem pengendalian pelaksanaan anggaran pendapatan dan belanja', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('5fc2e1b11a90ff3880f999070d7b7b43bcac0e68', 'dc06264262364cb2318c3282ed7590b1aa45b779', '03', 'Kelemahan struktur pengendalian intern', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('02cc05566a58319721f5a112b924ae83f863b6bd', 'f307316cfcfebc20fefa473768679afbf39a1de6', '01', 'Ketidakhematan/pemborosan/ketidakekonomisan', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('704863370a5444d29b9cb88e6bfbabc74572b074', 'f307316cfcfebc20fefa473768679afbf39a1de6', '02', 'Ketidakefisienan', '1')
<query>INSERT INTO par_finding_sub_type VALUES ('1c4fa91c754e6a03ab4c67012c9eea27308768e6', 'f307316cfcfebc20fefa473768679afbf39a1de6', '03', 'Ketidakefektifan', '0')
<query>INSERT INTO par_finding_sub_type VALUES ('7f37bdf63c975f80440a7016b87c5f03a2ff182a', 'dc06264262364cb2318c3282ed7590b1aa45b779', '2.01.00', 'Kelemahan sistem pengendalian akuntansi dan pelaporan', '0')
<query>INSERT INTO par_finding_sub_type VALUES ('e666e724f78eb1044639a0c4c6c1c5e69a9bdf35', 'dc06264262364cb2318c3282ed7590b1aa45b779', '2.02.00', 'Kelemahan sistem pengendalian pelaksanaan anggaran pendapatan dan belanja', '0')
<query>INSERT INTO par_finding_sub_type VALUES ('6d3b61d0e2573b317500ae3e35bc8d0e13504775', 'dc06264262364cb2318c3282ed7590b1aa45b779', '2.03.00', 'Kelemahan struktur pengendalian intern', '0')
<query>INSERT INTO par_finding_sub_type VALUES ('437930e8325599b5a5fd0557fc008e0cf477c0db', 'f307316cfcfebc20fefa473768679afbf39a1de6', '3.01.00', 'Ketidakhematan/pemborosan/ketidakekonomisan', '0')
<query>INSERT INTO par_finding_sub_type VALUES ('bf26d1d16ed88aa43268f3719220f5af2e6d46c2', 'f307316cfcfebc20fefa473768679afbf39a1de6', '3.02.00', 'Ketidakefisienan', '0')
<query>INSERT INTO par_finding_sub_type VALUES ('68e993038e99e777e1ed37c06a1b0bb41fba54a7', 'f307316cfcfebc20fefa473768679afbf39a1de6', '3.03.00', 'Ketidakefektifan', '1')
<query>DROP TABLE IF EXISTS par_finding_type
<query>CREATE TABLE `par_finding_type` (
  `finding_type_id` varchar(50) NOT NULL,
  `finding_type_code` varchar(10) NOT NULL,
  `finding_type_name` varchar(255) NOT NULL,
  `finding_type_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hapus 1=aktif',
  PRIMARY KEY (`finding_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_finding_type VALUES ('764275e74b69abcec96c2ac76140f7da774249f7', '1', 'Temuan Ketidakpatuhan Terhadap Peraturan', '1')
<query>INSERT INTO par_finding_type VALUES ('dc06264262364cb2318c3282ed7590b1aa45b779', '2', 'Temuan Kelemahan Sistem Pengendalian Internal', '1')
<query>INSERT INTO par_finding_type VALUES ('f307316cfcfebc20fefa473768679afbf39a1de6', '3', 'Temuan 3E', '1')
<query>DROP TABLE IF EXISTS par_group
<query>CREATE TABLE `par_group` (
  `group_id` varchar(50) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `group_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=del 1=aktif',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_group VALUES ('a38c99db8e8777d33b3b358d59a47ae1a0c69d66', 'Administrator', '1')
<query>INSERT INTO par_group VALUES ('5d3b02abc8c1e0d4d4f7ee3c0c59f09f4ea93f01', 'Risk Owner', '0')
<query>INSERT INTO par_group VALUES ('c220d2e970d8aad6283c45613cce8ed8000df21e', 'Risk Officer', '0')
<query>INSERT INTO par_group VALUES ('0d1db90fabf192910f354e351dc27fd7f37fb041', 'Eksternal', '0')
<query>INSERT INTO par_group VALUES ('ff6c97274745bf929be5d651849c64d7ca1b405d', 'Staf Perencanaan', '0')
<query>INSERT INTO par_group VALUES ('7090cfd596225052fed383a3332fcc8dc72ba918', 'Bagian Program dan Evaluasi', '1')
<query>INSERT INTO par_group VALUES ('98ab4eb2262a169acc9716882a6cac9d0a07e6f3', 'Auditor', '1')
<query>INSERT INTO par_group VALUES ('c64ab15f3c09ce0faf0e07ac527be323ce985814', 'Pengendali Mutu', '0')
<query>INSERT INTO par_group VALUES ('24e3c696dcb9ff44b17eafb85d9d15ffe330b80d', 'Pengendali Teknis', '0')
<query>INSERT INTO par_group VALUES ('3c4b7c1ac41e48962a837470d96af3ca244113ba', 'Ketua Tim', '0')
<query>INSERT INTO par_group VALUES ('c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', 'Bagian Keuangan dan Umum', '1')
<query>INSERT INTO par_group VALUES ('b646f7e15fc0ca85b3a3fe54c19017a0e933ef82', 'urip', '0')
<query>INSERT INTO par_group VALUES ('4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', 'Inspektur', '1')
<query>INSERT INTO par_group VALUES ('4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', 'Bagian Kepegawaian', '1')
<query>INSERT INTO par_group VALUES ('036b0642d7c660cd501fb07e1e33ec0bdd157534', 'Subbag Tata Usaha', '1')
<query>INSERT INTO par_group VALUES ('dd05ae3db0b445fd8fe9d9341a8ad3a4f41b1d43', 'Bagian SIP', '0')
<query>INSERT INTO par_group VALUES ('ec6e2d83c28a096bd4b75df1e1d463bcdf6dbe8e', 'Kabag Bagian Program', '0')
<query>INSERT INTO par_group VALUES ('12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', 'Sekretaris Inspektorat Jenderal', '1')
<query>INSERT INTO par_group VALUES ('25a0bc88a8aca130913d7b35facf9e3505bf12af', 'Auditee', '1')
<query>DROP TABLE IF EXISTS par_holiday
<query>CREATE TABLE `par_holiday` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_date` int(11) NOT NULL,
  `holiday_desc` varchar(100) NOT NULL,
  `holiday_st` tinyint(1) NOT NULL COMMENT '1=weekdays',
  PRIMARY KEY (`holiday_id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=latin1
<query>INSERT INTO par_holiday VALUES ('1', '1483768800', '', '2')
<query>INSERT INTO par_holiday VALUES ('2', '1483855200', '', '2')
<query>INSERT INTO par_holiday VALUES ('3', '1484373600', '', '2')
<query>INSERT INTO par_holiday VALUES ('4', '1484460000', '', '2')
<query>INSERT INTO par_holiday VALUES ('5', '1484978400', '', '2')
<query>INSERT INTO par_holiday VALUES ('6', '1485064800', '', '2')
<query>INSERT INTO par_holiday VALUES ('7', '1485583200', '', '2')
<query>INSERT INTO par_holiday VALUES ('8', '1485669600', '', '2')
<query>INSERT INTO par_holiday VALUES ('9', '1486188000', '', '2')
<query>INSERT INTO par_holiday VALUES ('10', '1486274400', '', '2')
<query>INSERT INTO par_holiday VALUES ('11', '1486792800', '', '2')
<query>INSERT INTO par_holiday VALUES ('12', '1486879200', '', '2')
<query>INSERT INTO par_holiday VALUES ('13', '1487397600', '', '2')
<query>INSERT INTO par_holiday VALUES ('14', '1487484000', '', '2')
<query>INSERT INTO par_holiday VALUES ('15', '1488002400', '', '2')
<query>INSERT INTO par_holiday VALUES ('16', '1488088800', '', '2')
<query>INSERT INTO par_holiday VALUES ('17', '1488607200', '', '2')
<query>INSERT INTO par_holiday VALUES ('18', '1488693600', '', '2')
<query>INSERT INTO par_holiday VALUES ('19', '1489212000', '', '2')
<query>INSERT INTO par_holiday VALUES ('20', '1489298400', '', '2')
<query>INSERT INTO par_holiday VALUES ('21', '1489813200', '', '2')
<query>INSERT INTO par_holiday VALUES ('22', '1489899600', '', '2')
<query>INSERT INTO par_holiday VALUES ('23', '1490418000', '', '2')
<query>INSERT INTO par_holiday VALUES ('24', '1490504400', '', '2')
<query>INSERT INTO par_holiday VALUES ('25', '1491022800', '', '2')
<query>INSERT INTO par_holiday VALUES ('26', '1491109200', '', '2')
<query>INSERT INTO par_holiday VALUES ('27', '1491627600', '', '2')
<query>INSERT INTO par_holiday VALUES ('28', '1491714000', '', '2')
<query>INSERT INTO par_holiday VALUES ('29', '1492232400', '', '2')
<query>INSERT INTO par_holiday VALUES ('30', '1492318800', '', '2')
<query>INSERT INTO par_holiday VALUES ('31', '1492837200', '', '2')
<query>INSERT INTO par_holiday VALUES ('32', '1492923600', '', '2')
<query>INSERT INTO par_holiday VALUES ('33', '1493442000', '', '2')
<query>INSERT INTO par_holiday VALUES ('34', '1493528400', '', '2')
<query>INSERT INTO par_holiday VALUES ('35', '1494046800', '', '2')
<query>INSERT INTO par_holiday VALUES ('36', '1494133200', '', '2')
<query>INSERT INTO par_holiday VALUES ('37', '1494651600', '', '2')
<query>INSERT INTO par_holiday VALUES ('38', '1494738000', '', '2')
<query>INSERT INTO par_holiday VALUES ('39', '1495256400', '', '2')
<query>INSERT INTO par_holiday VALUES ('40', '1495342800', '', '2')
<query>INSERT INTO par_holiday VALUES ('41', '1495861200', '', '2')
<query>INSERT INTO par_holiday VALUES ('42', '1495947600', '', '2')
<query>INSERT INTO par_holiday VALUES ('43', '1496466000', '', '2')
<query>INSERT INTO par_holiday VALUES ('44', '1496552400', '', '2')
<query>INSERT INTO par_holiday VALUES ('45', '1497070800', '', '2')
<query>INSERT INTO par_holiday VALUES ('46', '1497157200', '', '2')
<query>INSERT INTO par_holiday VALUES ('47', '1497675600', '', '2')
<query>INSERT INTO par_holiday VALUES ('48', '1497762000', '', '2')
<query>INSERT INTO par_holiday VALUES ('49', '1498280400', '', '2')
<query>INSERT INTO par_holiday VALUES ('50', '1498366800', '', '2')
<query>INSERT INTO par_holiday VALUES ('51', '1498885200', '', '2')
<query>INSERT INTO par_holiday VALUES ('52', '1498971600', '', '2')
<query>INSERT INTO par_holiday VALUES ('53', '1499490000', '', '2')
<query>INSERT INTO par_holiday VALUES ('54', '1499576400', '', '2')
<query>INSERT INTO par_holiday VALUES ('55', '1500094800', '', '2')
<query>INSERT INTO par_holiday VALUES ('56', '1500181200', '', '2')
<query>INSERT INTO par_holiday VALUES ('57', '1500699600', '', '2')
<query>INSERT INTO par_holiday VALUES ('58', '1500786000', '', '2')
<query>INSERT INTO par_holiday VALUES ('59', '1501304400', '', '2')
<query>INSERT INTO par_holiday VALUES ('60', '1501390800', '', '2')
<query>INSERT INTO par_holiday VALUES ('61', '1501909200', '', '2')
<query>INSERT INTO par_holiday VALUES ('62', '1501995600', '', '2')
<query>INSERT INTO par_holiday VALUES ('63', '1502514000', '', '2')
<query>INSERT INTO par_holiday VALUES ('64', '1502600400', '', '2')
<query>INSERT INTO par_holiday VALUES ('65', '1503118800', '', '2')
<query>INSERT INTO par_holiday VALUES ('66', '1503205200', '', '2')
<query>INSERT INTO par_holiday VALUES ('67', '1503723600', '', '2')
<query>INSERT INTO par_holiday VALUES ('68', '1503810000', '', '2')
<query>INSERT INTO par_holiday VALUES ('69', '1504328400', '', '2')
<query>INSERT INTO par_holiday VALUES ('70', '1504414800', '', '2')
<query>INSERT INTO par_holiday VALUES ('71', '1504933200', '', '2')
<query>INSERT INTO par_holiday VALUES ('72', '1505019600', '', '2')
<query>INSERT INTO par_holiday VALUES ('73', '1505538000', '', '2')
<query>INSERT INTO par_holiday VALUES ('74', '1505624400', '', '2')
<query>INSERT INTO par_holiday VALUES ('75', '1506142800', '', '2')
<query>INSERT INTO par_holiday VALUES ('76', '1506229200', '', '2')
<query>INSERT INTO par_holiday VALUES ('77', '1506747600', '', '2')
<query>INSERT INTO par_holiday VALUES ('78', '1506834000', '', '2')
<query>INSERT INTO par_holiday VALUES ('79', '1507352400', '', '2')
<query>INSERT INTO par_holiday VALUES ('80', '1507438800', '', '2')
<query>INSERT INTO par_holiday VALUES ('81', '1507957200', '', '2')
<query>INSERT INTO par_holiday VALUES ('82', '1508043600', '', '2')
<query>INSERT INTO par_holiday VALUES ('83', '1508562000', '', '2')
<query>INSERT INTO par_holiday VALUES ('84', '1508648400', '', '2')
<query>INSERT INTO par_holiday VALUES ('85', '1509166800', '', '2')
<query>INSERT INTO par_holiday VALUES ('86', '1509253200', '', '2')
<query>INSERT INTO par_holiday VALUES ('87', '1509771600', '', '2')
<query>INSERT INTO par_holiday VALUES ('88', '1509858000', '', '2')
<query>INSERT INTO par_holiday VALUES ('89', '1510380000', '', '2')
<query>INSERT INTO par_holiday VALUES ('90', '1510466400', '', '2')
<query>INSERT INTO par_holiday VALUES ('91', '1510984800', '', '2')
<query>INSERT INTO par_holiday VALUES ('92', '1511071200', '', '2')
<query>INSERT INTO par_holiday VALUES ('93', '1511589600', '', '2')
<query>INSERT INTO par_holiday VALUES ('94', '1511676000', '', '2')
<query>INSERT INTO par_holiday VALUES ('95', '1512194400', '', '2')
<query>INSERT INTO par_holiday VALUES ('96', '1512280800', '', '2')
<query>INSERT INTO par_holiday VALUES ('97', '1512799200', '', '2')
<query>INSERT INTO par_holiday VALUES ('98', '1512885600', '', '2')
<query>INSERT INTO par_holiday VALUES ('99', '1513404000', '', '2')
<query>INSERT INTO par_holiday VALUES ('100', '1513490400', '', '2')
<query>INSERT INTO par_holiday VALUES ('101', '1514008800', '', '2')
<query>INSERT INTO par_holiday VALUES ('102', '1514095200', '', '2')
<query>INSERT INTO par_holiday VALUES ('103', '1514613600', '', '2')
<query>INSERT INTO par_holiday VALUES ('104', '1514700000', '', '2')
<query>DROP TABLE IF EXISTS par_inspektorat
<query>CREATE TABLE `par_inspektorat` (
  `inspektorat_id` varchar(50) NOT NULL,
  `inspektorat_name` varchar(100) NOT NULL,
  `inspektorat_del_st` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`inspektorat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_inspektorat VALUES ('f7d66dce0a6697a089ec835f1a724d20bba955cd', 'Inspektorat V', '1')
<query>INSERT INTO par_inspektorat VALUES ('71901349bb3fac3edc6887c3d5c3676ca5f954ad', 'Inspektorat IV', '1')
<query>INSERT INTO par_inspektorat VALUES ('3b0761f74ec705e9b72ea6053e3930a3c8294ef4', 'Inspektorat III', '1')
<query>INSERT INTO par_inspektorat VALUES ('2b2d9dad563c8b6a800cfc47579c6fa0928ef8b9', 'Inspektorat II', '1')
<query>INSERT INTO par_inspektorat VALUES ('3d76fa9f0bb3d72f33d775fbd4529f44031c824e', 'Inspektorat I', '1')
<query>INSERT INTO par_inspektorat VALUES ('22d73ea85e0e84ddeecf422ddbcfc18d2c699b94', 'Inspektur Jenderal', '1')
<query>INSERT INTO par_inspektorat VALUES ('b1f4e8296cb0dc37aa322c3b0e2802417b7e155b', 'Sekretariat Inspektorat Jenderal', '1')
<query>DROP TABLE IF EXISTS par_jabatan_pic
<query>CREATE TABLE `par_jabatan_pic` (
  `jabatan_pic_id` varchar(50) NOT NULL,
  `jabatan_pic_name` varchar(100) NOT NULL,
  `jabatan_pic_sort` tinyint(2) NOT NULL DEFAULT '0',
  `jabatan_pic_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`jabatan_pic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_jabatan_pic VALUES ('9421e13da30429a45223757136ba2d93e3c73322', 'Direktur Jenderal', '1', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('86d89c27651df2f6b160093241509f735758da18', 'Sekretaris Jenderal', '1', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('2fb98b3bd76ce53d08d153458515263972dfd638', 'Direktur', '3', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('41ba4351c57dd73ab7e2eae9e4ba2a693c176788', 'Kepala Biro', '4', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('19053177aec2e17387ff7fb76ba70d77cfbdb64a', 'Kepala Bagian', '5', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('cace1a6ca808ed8ee1f6339cb9ebd5e2af3b4a63', 'Kepala Bidang', '6', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('4f090fb7de6f0a696548c3c9296ffcadd2bac2f6', 'Kepala Badan', '1', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('4b7de7c9c587d2c0200d5363b58ea4ea13d972b1', 'Kepala Sekretariat', '2', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('3beda7c76816017b24089b487db64677055a5e7c', 'Kepala Pusat', '2', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('36b9d137b5622b1975da56ebf850a5bd12962320', 'Sekretris Ditjen', '2', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('33eb43af4c4b83b51ea7d0be3b8451c020f68cf5', 'Kepala Balai', '2', '1')
<query>INSERT INTO par_jabatan_pic VALUES ('30122bd46427250f4e01724098f84b080fff40a0', 'Kepala Sub Direktorat', '3', '1')
<query>DROP TABLE IF EXISTS par_jenis_jabatan
<query>CREATE TABLE `par_jenis_jabatan` (
  `jenis_jabatan_id` varchar(50) NOT NULL,
  `jenis_jabatan` varchar(10) NOT NULL,
  `jenis_jabatan_sub` varchar(50) NOT NULL,
  `jenis_jabatan_sort` int(11) NOT NULL,
  `jenis_jabatan_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`jenis_jabatan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_jenis_jabatan VALUES ('c3421e504177579e8f6bf9de290da00dab1b4a35', 'Struktural', 'Kepala Bagian', '2', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('ca4d3c424bd05544c7a145bd2af6cef0a08e92e4', 'Struktural', 'Inspektur', '1', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('23ddda7bef0263d7f67c7b1296c11168e72d7555', 'Struktural', 'Inspektur Jenderal', '1', '0')
<query>INSERT INTO par_jenis_jabatan VALUES ('ac0f7970492000d14b9d4ae01ef588083663e6d7', 'Fungsional', 'Auditor Utama', '4', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('b9610baa4575c677951c04e0050ac0e9a5676594', 'Fungsional', 'Auditor Muda', '6', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('1fd86fc0cde73981d66fa6642020f8ed8be256b4', 'Fungsional', 'Auditor Pertama', '7', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('f391ea222c911367026231dc975c10d4f09f9bce', 'Fungsional', 'Auditor Penyelia', '5', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('d0f4aa0437d8e3360b2faa99abe83c18f8d72c19', 'Struktural', 'Sekretaris Inspektorat Jenderal', '2', '0')
<query>INSERT INTO par_jenis_jabatan VALUES ('142eda73208917765e2bd4c8357e86fbeb00390d', 'Struktural', 'Kepala Subbagian', '3', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('b4dc6bbf8c6786337c430a9dba3557ab75fd5cca', 'Struktural', 'Staf', '10', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('77e5755edba4cbf18d9040f222310a853f4388d8', 'Fungsional', 'Auditor Madya', '5', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('425226edf283c7bac5cbefe0bbfe092e17840de6', 'Fungsional', 'Auditor Pelaksana Lanjutan', '6', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('2f19c5284f462170036bddacd2d8b7ef5831b214', 'Fungsional', 'Auditor Pelaksana', '7', '1')
<query>INSERT INTO par_jenis_jabatan VALUES ('fa8abbcc16ba9aef09fc10070a2329adbc422fb2', 'Fungsional', 'Calon Auditor', '8', '1')
<query>DROP TABLE IF EXISTS par_kabupaten
<query>CREATE TABLE `par_kabupaten` (
  `kabupaten_id` varchar(50) NOT NULL,
  `kabupaten_id_prov` varchar(50) NOT NULL,
  `kabupaten_name` varchar(100) NOT NULL,
  `kabupaten_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kabupaten_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_kabupaten VALUES ('768979fec4eaac076f47ea1ae4724be5d56d7cd3', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Aceh Tamiang', '1')
<query>INSERT INTO par_kabupaten VALUES ('5026b574bab8e5fe5220c64d0aa87ca200dcedd8', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Aceh Tenggara', '1')
<query>INSERT INTO par_kabupaten VALUES ('e44c192393e327ec1ea3afd1066dd5e095314556', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Bireun', '1')
<query>INSERT INTO par_kabupaten VALUES ('0c03e4b8ee8c2b450d043c082778510a8c6893cc', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Aceh Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('e4512ac29103f5fbefcc5501d5fb24eae6d5fbe8', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Aceh Timur', '1')
<query>INSERT INTO par_kabupaten VALUES ('d04f57c46bc68e45c519eff63af2cd3653cda55d', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Aceh Barat Daya', '1')
<query>INSERT INTO par_kabupaten VALUES ('a90381c2ac2fbaabefb2abf18d2cd5e1c141d971', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Sabang', '1')
<query>INSERT INTO par_kabupaten VALUES ('589d260cc09c2b39ca48c3c6912db22f1f6a48aa', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Langkat', '1')
<query>INSERT INTO par_kabupaten VALUES ('41f91146873f89e7cac416fe3b5a433436c8cc05', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Tapanuli Tengah', '1')
<query>INSERT INTO par_kabupaten VALUES ('a5a0aa863b2534f46fa2013a204eaf06ba7aeaba', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Serdang Bedagai', '1')
<query>INSERT INTO par_kabupaten VALUES ('580e0503051ec3020fc9c59d6a43438e355fe873', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Batubara', '1')
<query>INSERT INTO par_kabupaten VALUES ('19533b6fd43c9f3608509dc8cdb55664a017e250', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Nias Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('25a77ce38e092d184194e2c5f3aaea9f11b9d28f', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Simalungun', '1')
<query>INSERT INTO par_kabupaten VALUES ('6c9d42f7de3e866b04701f165695dbacbf13d50d', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Asahan', '1')
<query>INSERT INTO par_kabupaten VALUES ('9ee043d5560d8b7c3b33983b1391c5fac85ec90b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Belawan', '1')
<query>INSERT INTO par_kabupaten VALUES ('d4c1b67db0228db18eaff35e908f7cc8614b2e1a', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Binjai', '1')
<query>INSERT INTO par_kabupaten VALUES ('15d5603665019f85d8931d0920b67a97a082b70c', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Pesisir Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('a6faa55e85ae26c1c5158cca1ec80462d47dbc3c', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Pasaman Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('56d0a347fcfeeeaa9849cb42e99856c7177e41c0', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Padang Pariaman', '1')
<query>INSERT INTO par_kabupaten VALUES ('4506c687c76397855d11f2f5d50245424f7d088c', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Pariaman', '1')
<query>INSERT INTO par_kabupaten VALUES ('a5bc65ce1d11b3af66fdbb747dcfd24f7b4c80d9', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Kepulauan Mentawai', '1')
<query>INSERT INTO par_kabupaten VALUES ('5e60383bfa654e4b8180c866de25e646542f429a', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Agam', '1')
<query>INSERT INTO par_kabupaten VALUES ('6f0c83b8eb8a17d30e93ee56013f03ab0b6b1e5b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Indragiri Hilir', '1')
<query>INSERT INTO par_kabupaten VALUES ('4305cfef7d326a49c637a75f6980666acaf48b24', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Kepulauan Meranti', '1')
<query>INSERT INTO par_kabupaten VALUES ('0ac1e8bce0528ff9a142906313e31e22bc6cd816', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Kampar', '1')
<query>INSERT INTO par_kabupaten VALUES ('21555d7818405c23c5dd734cc1a7cc0324522ef4', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Rokan Hilir', '1')
<query>INSERT INTO par_kabupaten VALUES ('3a262f05653e7cffb46e0c18703325e439b4a32a', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Bengkalis', '1')
<query>INSERT INTO par_kabupaten VALUES ('640b9dd3aee2d6841b7829fddb86cc2f71c1736a', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Palalawan', '1')
<query>INSERT INTO par_kabupaten VALUES ('a29adab2726828c4462d3b842f0a892297425346', 'bc02f4d326c699e711b05bf45925b3652464457a', 'Bintan', '1')
<query>INSERT INTO par_kabupaten VALUES ('61567dc4a581d0486ac456cc8e6f722bf7c7c8e6', 'bc02f4d326c699e711b05bf45925b3652464457a', 'Natuna', '1')
<query>INSERT INTO par_kabupaten VALUES ('923b61e7df9d117198943e9fa118bb0c8d98d88f', 'bc02f4d326c699e711b05bf45925b3652464457a', 'Lingga', '1')
<query>INSERT INTO par_kabupaten VALUES ('c9f88f954d0b71c54b39ed653a6732fde7294c4c', '675eed8a709d4098541fb8f73805004c84725d49', 'Tanjung Jabung Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('d6462aa470bcb6f02851209a9714d2dd9d6ca239', '675eed8a709d4098541fb8f73805004c84725d49', 'Muaro Jambi', '1')
<query>INSERT INTO par_kabupaten VALUES ('547a3a6ad51a53ed644507c819791eeefb00b498', '675eed8a709d4098541fb8f73805004c84725d49', 'Kerinci', '1')
<query>INSERT INTO par_kabupaten VALUES ('c0d8cd1b0ca720818ad38d5c5ba37081a162f9a6', '675eed8a709d4098541fb8f73805004c84725d49', 'Batanghari', '1')
<query>INSERT INTO par_kabupaten VALUES ('db3d4a5851fced2da00bfd5a267c98f26c3aadc9', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'Oki', '1')
<query>INSERT INTO par_kabupaten VALUES ('d3a17b32dc04e0a70988b691e671e6f196872444', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'Musi Banyuasin', '1')
<query>INSERT INTO par_kabupaten VALUES ('2ecbcbe65df4843e40f940d6ce9cb9e1ae6f299d', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'Musi Rawas', '1')
<query>INSERT INTO par_kabupaten VALUES ('47c9fcbddcc4de7acf7085ef81a4579c62232baf', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'Belitung', '1')
<query>INSERT INTO par_kabupaten VALUES ('a25db1256f76937a9a76953511d249e14d49b01e', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'Bangka Tengah', '1')
<query>INSERT INTO par_kabupaten VALUES ('1b660f43cb2cdfafab5443eb46d709848dcf3f1f', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Tegal', '1')
<query>INSERT INTO par_kabupaten VALUES ('e2211d6c80c547c6e9b4be5436b482c3d57b802a', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'Bangka', '1')
<query>INSERT INTO par_kabupaten VALUES ('064986209666082863356b4157e7240ae1d3e5a0', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'Bangka Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('4652955a23a5c6bc35f9de8f718954ca819455bb', 'd8c82bf22dd5b689e806f6e381dd3ad459e7485c', 'Bengkulu Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('a130fcbe33896e76b0babe35838ec1f3d48adf8f', 'd8c82bf22dd5b689e806f6e381dd3ad459e7485c', 'Kaur', '1')
<query>INSERT INTO par_kabupaten VALUES ('0bdda4e5a72b4cc776e34a6257c3e059bda46099', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Lampung Timur', '1')
<query>INSERT INTO par_kabupaten VALUES ('b0739520d183403ae4a89c7096db467c1c1cc672', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Tanggamus', '1')
<query>INSERT INTO par_kabupaten VALUES ('1a1155a7a26268e512d71083c19a8237509fa5af', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Lampung Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('0efd5a20efd958468a9c47127b811e8747e22e93', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Pesawaran', '1')
<query>INSERT INTO par_kabupaten VALUES ('b4b9c55c020de15616e4124618bd25e36e6a1db4', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Mesuji', '1')
<query>INSERT INTO par_kabupaten VALUES ('823f7394401156eabe5a61b89bc43b277b14c517', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Tulang Bawang', '1')
<query>INSERT INTO par_kabupaten VALUES ('a7b3cdd04a547414565b01d44d344fb17a977d33', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Lampung Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('44fb8b7e5c106db00b06f13d641bba6ea9533d0b', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Barru', '1')
<query>INSERT INTO par_kabupaten VALUES ('1b884a8a633a4fe30cb0099c9c97b85c77253997', '0b486467ca176631589de72c19779fe674f13388', 'Sumedang', '1')
<query>INSERT INTO par_kabupaten VALUES ('dc4a1cabacd1059935ccea96e78391d60c8a369e', '0b486467ca176631589de72c19779fe674f13388', 'Karawang', '1')
<query>INSERT INTO par_kabupaten VALUES ('5f682328b4887d57cdfe40d7b1358150a6102b74', '0b486467ca176631589de72c19779fe674f13388', 'Bogor', '1')
<query>INSERT INTO par_kabupaten VALUES ('2af07e4ab064d0bdf27d8a246eed72ae3b935e80', '0b486467ca176631589de72c19779fe674f13388', 'Subang', '1')
<query>INSERT INTO par_kabupaten VALUES ('68778d6ad9e35d1cf62582b71e23f70462da5171', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Tanjung Balai', '1')
<query>INSERT INTO par_kabupaten VALUES ('6739a66ce91850bae5f1baf4a6ad7ae1f6075f26', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Padang', '1')
<query>INSERT INTO par_kabupaten VALUES ('4e99d9b15a57cb77bfe5701803b57dd3ca46885e', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'Jakarta Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('b788c1d6c3d5f74210de83832a4fdad532511d8b', '0b486467ca176631589de72c19779fe674f13388', 'Tasikmalaya', '1')
<query>INSERT INTO par_kabupaten VALUES ('5c8a0922bfd86b6f39f1173a5cc4bcfd573c863c', '0b486467ca176631589de72c19779fe674f13388', 'Ciamis (banjar)', '1')
<query>INSERT INTO par_kabupaten VALUES ('68b0618a981c3ebd3fabcdac99c0d2341611d9de', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'Serang', '1')
<query>INSERT INTO par_kabupaten VALUES ('f83937d12a066e2151e46cd6a41a8179dad50153', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'Pandeglang', '1')
<query>INSERT INTO par_kabupaten VALUES ('9f34285f17286bf6eeb802eb5b2e54ec4461f306', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'Lebak', '1')
<query>INSERT INTO par_kabupaten VALUES ('eb310cbe50c6b2676eb9a37a1f73aeadb9f17d93', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Medan', '1')
<query>INSERT INTO par_kabupaten VALUES ('3dd14a28cc112182374c612cec798325cc564da6', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Jepara', '1')
<query>INSERT INTO par_kabupaten VALUES ('8cb57f27219c358ae8ddf522f9d01f833f295b6f', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Banyumas', '1')
<query>INSERT INTO par_kabupaten VALUES ('5aba4fb223dae55105d598b2836d680da75246a4', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Cilacap', '1')
<query>INSERT INTO par_kabupaten VALUES ('51195dd4c9e2181d48465d5c03b3e10401bd1c74', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Purbalingga', '1')
<query>INSERT INTO par_kabupaten VALUES ('78609a372cd11c380d4c2f4080c886eb6b84710b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Boyolali', '1')
<query>INSERT INTO par_kabupaten VALUES ('47e4c0d137e46f5e8c628b308ac4d05e5cdc461b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Wonogiri', '1')
<query>INSERT INTO par_kabupaten VALUES ('96c03650ad7eb4fb781254643598338f9d51c203', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Kebumen', '1')
<query>INSERT INTO par_kabupaten VALUES ('229f1c578dda332c5ef5f9a52b650bc088ccb800', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Kendal', '1')
<query>INSERT INTO par_kabupaten VALUES ('39b0cb354b587ce8a62726f034170cbdf9647eb5', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Pemalang', '1')
<query>INSERT INTO par_kabupaten VALUES ('631888f1e648aac40d515e63b8852ce1e278ca3a', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'Bantul', '1')
<query>INSERT INTO par_kabupaten VALUES ('dc5fb86daf2eca6f8054441b38a433fb555254f4', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'Sleman', '1')
<query>INSERT INTO par_kabupaten VALUES ('0e78f7408f00fadf6d7eb68f40395e6575458d03', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'Gunung Kidul', '1')
<query>INSERT INTO par_kabupaten VALUES ('84ad70a33e5d2717ff9fb6124ad3a2f3d4400915', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'Kulon Progo', '1')
<query>INSERT INTO par_kabupaten VALUES ('282027c71f277db5f9f7c2df862b6ba670b61b72', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Banyuwangi', '1')
<query>INSERT INTO par_kabupaten VALUES ('35075dee0755cc4d184b2a961730144c6ca7a8a1', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Pasuruan', '1')
<query>INSERT INTO par_kabupaten VALUES ('bcf8a5bb57255a67d187ed5ee19c8dde9aa077d5', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Blitar', '1')
<query>INSERT INTO par_kabupaten VALUES ('a3c5da5c342eeb1d5573b7ed7271ee38bf448a82', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Pacitan', '1')
<query>INSERT INTO par_kabupaten VALUES ('1ad86e9840a27ae0aca5d8d6ec30a8bfbe4eb3a5', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Gresik', '1')
<query>INSERT INTO par_kabupaten VALUES ('4fed488e6f61de9c9d6d540f263e675ca89d9a44', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Sidoarjo', '1')
<query>INSERT INTO par_kabupaten VALUES ('3d7c394ae564df7adca3c184b27dc1493c7c2f27', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Sumenep', '1')
<query>INSERT INTO par_kabupaten VALUES ('582be6220adeba3ca391a2faeadcb429ffaf0c09', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Bondowoso', '1')
<query>INSERT INTO par_kabupaten VALUES ('893612e13f146d5dec1769659625ac2b97b265dc', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Malang', '1')
<query>INSERT INTO par_kabupaten VALUES ('9757835a4208235c42e449cd57e5135eaecd6fdd', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Tulung Agung', '1')
<query>INSERT INTO par_kabupaten VALUES ('b215bce498ead7f8932154f4c65f63996db492f1', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Trenggalek', '1')
<query>INSERT INTO par_kabupaten VALUES ('11403fc113f15f64da93f91271928b8a0b2fca0b', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Madiun', '1')
<query>INSERT INTO par_kabupaten VALUES ('63447ff62118ebe4b797314ae4fbc04547fc755f', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Ngawi', '1')
<query>INSERT INTO par_kabupaten VALUES ('c5ea57743277a627116d2949cdf62c71b60872ec', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Bojonegoro', '1')
<query>INSERT INTO par_kabupaten VALUES ('7021711f66941d82bff24dd90d1a8f7401020c74', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Lamongan', '1')
<query>INSERT INTO par_kabupaten VALUES ('998319805b608680b906818813c9068cebd9d6dc', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Mojokerto', '1')
<query>INSERT INTO par_kabupaten VALUES ('1aeda9001a0b0efe57cc990d08c21532dcce59ab', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Sampang', '1')
<query>INSERT INTO par_kabupaten VALUES ('c92f9d8ee3694bebb4052541a3054c421e86d510', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Pamekasan', '1')
<query>INSERT INTO par_kabupaten VALUES ('ab70b3c9b108b88b6dc07f5aa516ec9eef66d414', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Sibolga', '1')
<query>INSERT INTO par_kabupaten VALUES ('0ffb715918d33c31c5b241fe8724785c188b877f', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Bangkalan', '1')
<query>INSERT INTO par_kabupaten VALUES ('442ad5c7bb776fc39e39ba953e23e6dbdcba37ad', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Probolinggo', '1')
<query>INSERT INTO par_kabupaten VALUES ('fbaf2aabdad611593dc2b30b8cf09f771b7d3141', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Tuban', '1')
<query>INSERT INTO par_kabupaten VALUES ('aba9568fcbffc2b28ffcbac78670789e9ecdf9c2', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Situbondo', '1')
<query>INSERT INTO par_kabupaten VALUES ('4a1e06d83cc32deed10864d78f75800f35753816', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Nganjuk', '1')
<query>INSERT INTO par_kabupaten VALUES ('d5c903e1c582950da75a54e491e5d6b1aa1786b6', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Jember', '1')
<query>INSERT INTO par_kabupaten VALUES ('108ef481f534515b736ec6dd9ee92ba8434bfda0', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Jombang', '1')
<query>INSERT INTO par_kabupaten VALUES ('f375333acf1442988c1dcd377255099c57316d34', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Lumajang', '1')
<query>INSERT INTO par_kabupaten VALUES ('a49f6724a0455fd3996acd5df46b560b40af5a08', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Buleleng', '1')
<query>INSERT INTO par_kabupaten VALUES ('b0e70a8c4fcc25fe391827127a7f39932672a861', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Jembrana', '1')
<query>INSERT INTO par_kabupaten VALUES ('568ba72e9dbde5f0755adef8a29e8f656b9a77b0', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Klungkung', '1')
<query>INSERT INTO par_kabupaten VALUES ('85e468a61fea4705936c350de6550e9f34178c49', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Bangli', '1')
<query>INSERT INTO par_kabupaten VALUES ('7d3199b703d7e072099cc7b4c62f98a53ff2f8ba', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Tabanan', '1')
<query>INSERT INTO par_kabupaten VALUES ('d8187cf2daab4d818731a906250dc4c87cc5e409', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Karangasem', '1')
<query>INSERT INTO par_kabupaten VALUES ('e54e01d2382ed35b08d30a10ff586ed8ade364de', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Badung', '1')
<query>INSERT INTO par_kabupaten VALUES ('244d62c59fb427bff7e769ac05df7db3f558b835', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Lombok Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('83053e9b75b804cd6013d93ec5dee7e8bd2f1da3', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Sumbawa', '1')
<query>INSERT INTO par_kabupaten VALUES ('91ce4d9eff8135242934cdb14605905c087e405e', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Sumbawa Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('efc2dae8eb1324b9097ff48d99c59cf7d30a220c', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Lombok Tengah', '1')
<query>INSERT INTO par_kabupaten VALUES ('a7d96c73586877978f51ab6232da13fe81d38edf', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Timor Tengah Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('6d2b356d61ca3fcd929354c3efb64f3e2c6aa271', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Sikka', '1')
<query>INSERT INTO par_kabupaten VALUES ('121dafc847f62af9bba85a5843e35b9689667eb2', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Flores Timur', '1')
<query>INSERT INTO par_kabupaten VALUES ('d88cfce9e7ec4281eaab61c3448105b228cd093d', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Sumba Timur', '1')
<query>INSERT INTO par_kabupaten VALUES ('eccdaffc677d841c2ad263ac20d6fa5857415396', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Kupang', '1')
<query>INSERT INTO par_kabupaten VALUES ('a12f1d1cade35884f5f9acdde460a24979230080', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Timor Tengah Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('e78265b76155776593f5da65d1b23e35ee32b76e', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Ende', '1')
<query>INSERT INTO par_kabupaten VALUES ('bea0c22885daa21aa5b9d99a88d050f3df08b0e4', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Manggarai', '1')
<query>INSERT INTO par_kabupaten VALUES ('0a347fec9300cf0f64e8c7335364efa46491b34e', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Manggarai Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('60d4877001944ce35f4f8a499e18b308977c069a', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Lembata', '1')
<query>INSERT INTO par_kabupaten VALUES ('1fa9f5ad90a970de7e8bfbb8892c6ec6d6ac92da', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Nagekeo', '1')
<query>INSERT INTO par_kabupaten VALUES ('fffc00bc924f84fc043aa10c373bdc6211ad8697', '499db371dac2489052cf151f8e861ac9d9172df7', 'Ketapang', '1')
<query>INSERT INTO par_kabupaten VALUES ('084015cbf85fddd865122c3410d07c2f8d512f67', '499db371dac2489052cf151f8e861ac9d9172df7', 'Kubu Raya', '1')
<query>INSERT INTO par_kabupaten VALUES ('1aabb3512a79bc3c047c4fb032126884bd1f88d1', '499db371dac2489052cf151f8e861ac9d9172df7', 'Pontianak', '1')
<query>INSERT INTO par_kabupaten VALUES ('925a311b964d3f99e5567ac71c2ecd4622a11010', '499db371dac2489052cf151f8e861ac9d9172df7', 'Kapuas Hulu', '1')
<query>INSERT INTO par_kabupaten VALUES ('9ebeda420c01ee2723901176f8b7720be5845873', '499db371dac2489052cf151f8e861ac9d9172df7', 'Bengkayang', '1')
<query>INSERT INTO par_kabupaten VALUES ('a117a252776ab0f76fa56030f9daae43461e9149', '499db371dac2489052cf151f8e861ac9d9172df7', 'Sambas', '1')
<query>INSERT INTO par_kabupaten VALUES ('0886d683c7ede941d7c08ded605156365d76ef45', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'Penajem Paser Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('77259f6aad26fb14ed23301b88b0d3d6a612b242', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'Tarakan', '1')
<query>INSERT INTO par_kabupaten VALUES ('91a26a01c082dfb9478de5eee755c0c267a5d41f', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'Berau', '1')
<query>INSERT INTO par_kabupaten VALUES ('ac2a0073361c70ca54dd21b9f527b384c33e5b87', '175f37cdb244988462e7946a62f190f8bcd3c048', 'Kapuas', '1')
<query>INSERT INTO par_kabupaten VALUES ('e247e96758cf1d643393b5e376fb4b535bc416e7', '175f37cdb244988462e7946a62f190f8bcd3c048', 'Kotawaringin Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('2746756a841c81568f035d96cc2c99bfdc0f7231', '175f37cdb244988462e7946a62f190f8bcd3c048', 'Seruyan', '1')
<query>INSERT INTO par_kabupaten VALUES ('c375d33eef61954d3fe252532aeb8cfbbf5ef099', '175f37cdb244988462e7946a62f190f8bcd3c048', 'Barito Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('07dd7eaed659f6c955d2aa626f2e26925db1af98', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Barito Kuala', '1')
<query>INSERT INTO par_kabupaten VALUES ('202fd3d93ec62446c550a541b7017d717f0b4d8f', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Tabalong', '1')
<query>INSERT INTO par_kabupaten VALUES ('7d37ae4035ec9334d20f4f86db2b8fc76ae87706', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Tanah Bumbu', '1')
<query>INSERT INTO par_kabupaten VALUES ('5cd47b88d75e2c604aa1c8e06370912d79edb15a', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Tanah Laut', '1')
<query>INSERT INTO par_kabupaten VALUES ('268ba60feb5ee3700c1c8eed4645ebb93a3f21d0', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Kota Baru', '1')
<query>INSERT INTO par_kabupaten VALUES ('0ecf402779abda28028ef38b77f775c17e86836e', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Bolaang Mongondow', '1')
<query>INSERT INTO par_kabupaten VALUES ('116477f533f0f77aeb7d5250ac5a660d70f81572', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Minahasa Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('afce5fa5d3c3541a08441a70c3fafe82114c66d7', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Minahasa Tenggara', '1')
<query>INSERT INTO par_kabupaten VALUES ('c0b6862237e916685f93f487b1dfbd35d83bcb72', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Bolaang Mongondow Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('67bd6c8944ea4cd516e47c08aa8409cf1b5f2319', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Sangihe', '1')
<query>INSERT INTO par_kabupaten VALUES ('85213768f6ec268cd7ec040432a738b223871748', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Kepulauan Talaud', '1')
<query>INSERT INTO par_kabupaten VALUES ('04f5bd897d13e6bd86e4dab89d2dddea4ec7cee7', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Minahasa Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('7db168c8201374b921f8cf01833b386a560a5d51', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Kep. Siau Tagulandang Biaro', '1')
<query>INSERT INTO par_kabupaten VALUES ('a5369ad22f0b81671b923378b74c0eb97dc64a9f', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Minahasa', '1')
<query>INSERT INTO par_kabupaten VALUES ('08d96c680ffc80e08941b7a9df9b81d5174ddc3e', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'Pohuwato', '1')
<query>INSERT INTO par_kabupaten VALUES ('1da800c3b0b3c009ad95bc9018c70ed010196748', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'Gorontalo Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('cbfa6bd4173f0efb045a5bf1c0601e96d4b1d03c', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'Gorontalo', '1')
<query>INSERT INTO par_kabupaten VALUES ('42c6242dd929b77419907065fe0e7c127948bacd', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Semarang', '1')
<query>INSERT INTO par_kabupaten VALUES ('ef5b1fc2ae69edbfc4f9af88a40c34c28bb0a908', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'Boalemo', '1')
<query>INSERT INTO par_kabupaten VALUES ('d3cfd63e134e7a413374554601b4d6c3d2b3e799', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Donggala', '1')
<query>INSERT INTO par_kabupaten VALUES ('6d36a35ab8d54a8b35a8ac13f9355b84e4dd45fd', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Toli-toli', '1')
<query>INSERT INTO par_kabupaten VALUES ('6d0806fd96d8675880e61a220602ac911b6f6884', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Poso', '1')
<query>INSERT INTO par_kabupaten VALUES ('d1fa44804b6f76dd3b1ee46e77f5294070c39f5d', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Banggai', '1')
<query>INSERT INTO par_kabupaten VALUES ('8d6311342c16ed2fb6f15c16e52c47760d3a11cc', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Morowali', '1')
<query>INSERT INTO par_kabupaten VALUES ('cab6a904f1e370ff114f1853989466d6c66e6a7c', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Tojo Una-una', '1')
<query>INSERT INTO par_kabupaten VALUES ('198880a9cd8de959d985e4c26c741185d29f1739', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Parigi Moutong', '1')
<query>INSERT INTO par_kabupaten VALUES ('94a645d22634fb701c9c4ef39ec20aef7aa093be', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Pekalongan', '1')
<query>INSERT INTO par_kabupaten VALUES ('67e468b03685c3a69630d690eff46df3556044d7', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Buton Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('1da08491d10efc45b95c444385b20163a12d22cb', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Konawe', '1')
<query>INSERT INTO par_kabupaten VALUES ('db54e94f030a0bedc56fb29e96320811f819b946', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Buton', '1')
<query>INSERT INTO par_kabupaten VALUES ('705581731d6a8dcf57f94b97eedf2cbe5bb2f96b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Konawe Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('e96fd90b016bf7b3a297b832c0d8cc3f51735401', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Muna', '1')
<query>INSERT INTO par_kabupaten VALUES ('c8b6c0fe317207ae044a67ed1212593c79414461', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Wakatobi', '1')
<query>INSERT INTO par_kabupaten VALUES ('5774f0434a471b6de14fac7b20c1ac7df5b22943', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Kolaka', '1')
<query>INSERT INTO par_kabupaten VALUES ('d0cd06b582876e3dc67555c11e072839148c06e2', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Maros', '1')
<query>INSERT INTO par_kabupaten VALUES ('109ea550ec2840ed6d597ec1273886a00c5772f3', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Luwu', '1')
<query>INSERT INTO par_kabupaten VALUES ('cd6ffa80148284c4fda6af8e4fd0f8a744a2b385', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Sinjai', '1')
<query>INSERT INTO par_kabupaten VALUES ('255acb04ea7198ddd1d0db6e873422284ed50d9e', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Jeneponto', '1')
<query>INSERT INTO par_kabupaten VALUES ('f0260726be07d7efcb40392bfec96ba228334dce', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'Lubuk Linggau', '1')
<query>INSERT INTO par_kabupaten VALUES ('c9884f9aa3dfbabbaeaabc8e64b7324426227dc6', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Pangkajene Kepulauan', '1')
<query>INSERT INTO par_kabupaten VALUES ('f16418f11cca55744ff812de39bf1bec67649860', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Enrekang', '1')
<query>INSERT INTO par_kabupaten VALUES ('66f3e0bede64afb010abc0178d3885dd1499ca31', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Takalar', '1')
<query>INSERT INTO par_kabupaten VALUES ('9c075d4c15763620c1eb4f43fa54d5499e6d2f58', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Sidrap', '1')
<query>INSERT INTO par_kabupaten VALUES ('b0ca629f5c94866a059357b61a258d9aaa06f4a6', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Pinrang', '1')
<query>INSERT INTO par_kabupaten VALUES ('2518b02710dd68dc9f20fc70c0e5aa5f4933fe68', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Luwu Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('976f3c24e4ee8a5af7649b670c7edcd29c601cab', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Bulukumba', '1')
<query>INSERT INTO par_kabupaten VALUES ('696ddf5e7e0a5c9559244b5a899a1e6be66a8da3', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Luwu Timur', '1')
<query>INSERT INTO par_kabupaten VALUES ('55b350aa23b4d423bca420b978cb24f938131ee7', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Bone', '1')
<query>INSERT INTO par_kabupaten VALUES ('4bd41c533c9ec9599964aeb2efd03df357b14c1f', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Gowa', '1')
<query>INSERT INTO par_kabupaten VALUES ('44dfa04f59ce7b45c9562e1e9fe0a1dfd47eaf0c', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'Mamuju', '1')
<query>INSERT INTO par_kabupaten VALUES ('293c10035025716c04f7cf49f2b4382bb9f2b6d5', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'Polewali Mandar', '1')
<query>INSERT INTO par_kabupaten VALUES ('a06e899d14482893ff05fda1d8d2efcf37a23fab', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'Majene', '1')
<query>INSERT INTO par_kabupaten VALUES ('0b4ac0f6b58dd7dc3a876a296edb080334981ab0', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'Mamuju Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('facb1ca8d3637b3474f36f9d2e291df8d24dc467', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Agung Lampung', '1')
<query>INSERT INTO par_kabupaten VALUES ('bbbf3defbcd5216b033bf6c858ecbb02160c1282', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Seram Bagian Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('baa49f9f67a1209bfbb8b82bfd38c0ee1cf347df', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Maluku Tengah', '1')
<query>INSERT INTO par_kabupaten VALUES ('e0e2a38350a30940d826d314a3615ea045bb0c16', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Maluku Tenggara', '1')
<query>INSERT INTO par_kabupaten VALUES ('e74a5b3709c9cc11f2fdc6f287f0251ba91bcb8f', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Kepulauan Aru', '1')
<query>INSERT INTO par_kabupaten VALUES ('76d291d5d0d9498f26b37cea62e6cccd8ab2b7ca', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'Palembang', '1')
<query>INSERT INTO par_kabupaten VALUES ('6ef65939ee4227ceb9deacc46e4ede7c505d51a6', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Maluku Barat Daya', '1')
<query>INSERT INTO par_kabupaten VALUES ('a2e62a45a351c6f19c57774a0fa2797d88f2dff0', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'Halmahera Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('5f806ad90ec6f387331a6acc165552d31e60438d', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'Kepulauan Sula', '1')
<query>INSERT INTO par_kabupaten VALUES ('5bcf85d53123e51694d9d41337cd711494e133bf', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'Halmahera Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('7f668a7e586ddb3dfc7f3c1c1a910e64e2678eef', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'Halmahera Timur', '1')
<query>INSERT INTO par_kabupaten VALUES ('2f7213969ebc204549de951476383cc00bb42acd', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'Pulau Morotai', '1')
<query>INSERT INTO par_kabupaten VALUES ('94c2d31cd68b5114e213dde4637dbcf014358e30', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Teluk Wondama', '1')
<query>INSERT INTO par_kabupaten VALUES ('fca822ea88ce14f8a2456244cc1f35447c7539e5', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Biak Numfor', '1')
<query>INSERT INTO par_kabupaten VALUES ('0bc555628bee2f79ef756c3f319c7e7b10d13010', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Merauke', '1')
<query>INSERT INTO par_kabupaten VALUES ('3e189576afe30da8c39f0ea3e6fb3f5627c8ab74', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Jayapura', '1')
<query>INSERT INTO par_kabupaten VALUES ('36f383ee1dd094a430f2b856a793438c7c3962a7', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Mimika', '1')
<query>INSERT INTO par_kabupaten VALUES ('d767319e6c7ed3bdb7d7a4beb34d0456e6010e10', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Mappi', '1')
<query>INSERT INTO par_kabupaten VALUES ('10b546b7fa9c17ef18a6616e94a1f50a9113b579', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'Raja Ampat', '1')
<query>INSERT INTO par_kabupaten VALUES ('b83d059734e7478d94c1336dc5013eb863c52052', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'Manokwari', '1')
<query>INSERT INTO par_kabupaten VALUES ('4a083e19ba8c23791a6b4544c617a41c3db2cd86', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'Sorong', '1')
<query>INSERT INTO par_kabupaten VALUES ('7c68e582ef9cc3f1c4572c5d6acbcc5f8b150e79', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Bandar Lampung', '1')
<query>INSERT INTO par_kabupaten VALUES ('1e4c48250dfcb2f3fdfee859126a21370aa6726a', '0b486467ca176631589de72c19779fe674f13388', 'Cianjur', '1')
<query>INSERT INTO par_kabupaten VALUES ('6ef7e133549ab39140abe6fc0e7cac29ccafe0c4', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Kediri', '1')
<query>INSERT INTO par_kabupaten VALUES ('5414f487020b08b2a970edfdf540d5a13ccfb244', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Bantaeng', '1')
<query>INSERT INTO par_kabupaten VALUES ('4f7673cbcd56748146807f844b656af9644d40bf', 'd8c82bf22dd5b689e806f6e381dd3ad459e7485c', 'Bengkulu', '1')
<query>INSERT INTO par_kabupaten VALUES ('c886bc7f80479e83d983da27e59f6b277098bbb5', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Selayar', '1')
<query>INSERT INTO par_kabupaten VALUES ('d8e0c5335e2c61b328e263770b7cfc7d712a3f00', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Gianyar', '1')
<query>INSERT INTO par_kabupaten VALUES ('16eb985d0c4af447fd097ebe70e0f56098b196fb', '0b486467ca176631589de72c19779fe674f13388', 'Bandung', '1')
<query>INSERT INTO par_kabupaten VALUES ('4fb2ead5ebcf2b51db8348c0628969ee6e825901', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'Pangkal Pinang', '1')
<query>INSERT INTO par_kabupaten VALUES ('f7d40803e8ea0e8dfdefb2b409d580404c10bad7', '0b486467ca176631589de72c19779fe674f13388', 'Purwakarta', '1')
<query>INSERT INTO par_kabupaten VALUES ('954384a0673088eaf6ef21153e46f88727ac0d15', '0b486467ca176631589de72c19779fe674f13388', 'Garut', '1')
<query>INSERT INTO par_kabupaten VALUES ('e929431491b74a14008b89735021f8f4e29d385a', 'bc02f4d326c699e711b05bf45925b3652464457a', 'Batam', '1')
<query>INSERT INTO par_kabupaten VALUES ('79671e078b4c0473cb3f218342dd22ae08e06b28', '675eed8a709d4098541fb8f73805004c84725d49', 'Jambi', '1')
<query>INSERT INTO par_kabupaten VALUES ('c5afdee5964616e452d524adfb0fdff2fc629874', '0b486467ca176631589de72c19779fe674f13388', 'Ciamis', '1')
<query>INSERT INTO par_kabupaten VALUES ('5952e219566c02168b02114a2f01062c041dd48c', '0b486467ca176631589de72c19779fe674f13388', 'Indramayu', '1')
<query>INSERT INTO par_kabupaten VALUES ('311d4c6d5b3651a15bc6aa6f9b88555eba5ab5f4', '0b486467ca176631589de72c19779fe674f13388', 'Majalengka', '1')
<query>INSERT INTO par_kabupaten VALUES ('7ab23ed9061e5235537a87e813f427f5bc641a37', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Banda Aceh', '1')
<query>INSERT INTO par_kabupaten VALUES ('5f2c10f257725b7a469048e6ca9d1b6deb6b73a1', '0b486467ca176631589de72c19779fe674f13388', 'Kuningan', '1')
<query>INSERT INTO par_kabupaten VALUES ('6ebdd928efda30b5ab2f703193d5cb0f3162e84f', '0b486467ca176631589de72c19779fe674f13388', 'Banjar', '1')
<query>INSERT INTO par_kabupaten VALUES ('907f9fc0da313dd9323556f78c93f2eb3ba3b776', '0b486467ca176631589de72c19779fe674f13388', 'Bekasi', '1')
<query>INSERT INTO par_kabupaten VALUES ('63add612f7390a2666b9732fcda23cadb1b3f1b6', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Aceh Besar', '1')
<query>INSERT INTO par_kabupaten VALUES ('edbabf7a709a4ed80c6678f6714782904641be11', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Nias Utara', '1')
<query>INSERT INTO par_kabupaten VALUES ('cb23b87f8d873c66dc6c381fee089903ba23f9df', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Yapen Waropen', '1')
<query>INSERT INTO par_kabupaten VALUES ('9163eb393abe7906aad765a2b10d4760962a5eff', '0b486467ca176631589de72c19779fe674f13388', 'Pangandaran', '1')
<query>INSERT INTO par_kabupaten VALUES ('828f85f9b2301a70d3298e044fc1e35349733622', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'Tangerang', '1')
<query>INSERT INTO par_kabupaten VALUES ('e547bcf4cae0ebe72a0deaa3cd524d9932972e0e', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'Kaimana', '1')
<query>INSERT INTO par_kabupaten VALUES ('471b402f7507e855591392d1e36c3697671d0981', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Surakarta', '1')
<query>INSERT INTO par_kabupaten VALUES ('228bec8dd9fec439a87f3069dcc0c31f3a259660', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Denpasar', '1')
<query>INSERT INTO par_kabupaten VALUES ('b3758f3ddc87381d3b254a70fb38351852bf3e52', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Bima', '1')
<query>INSERT INTO par_kabupaten VALUES ('84237065ec6a2e5064e5331db90603d72a210d12', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Mataram', '1')
<query>INSERT INTO par_kabupaten VALUES ('c9b7aeae468486108636b1e63426c5a88244858e', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Ternate', '1')
<query>INSERT INTO par_kabupaten VALUES ('574f1b692a66de71c198b693f04612ebb98983ae', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Kupang', '1')
<query>INSERT INTO par_kabupaten VALUES ('fddc14a89a1a24b78b1b87376eaebddaf6341b7f', '499db371dac2489052cf151f8e861ac9d9172df7', 'Singkawang', '1')
<query>INSERT INTO par_kabupaten VALUES ('02900bbb71566c7a53d5bfbe9835465e5f15b986', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'Samarinda', '1')
<query>INSERT INTO par_kabupaten VALUES ('2de26d1ada47df2ac0d199042f1f736f5b74c857', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'Balikpapan', '1')
<query>INSERT INTO par_kabupaten VALUES ('ada36cd2c7ca99abccfd60da9826960a3fc622f8', '175f37cdb244988462e7946a62f190f8bcd3c048', 'Waringin Barat', '1')
<query>INSERT INTO par_kabupaten VALUES ('4fda59d2e523ef898a354f4671c3ce6b19a5987b', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Baru', '1')
<query>INSERT INTO par_kabupaten VALUES ('69b71095a4f1fadc5941cc695b6b93f77e1f3b4d', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Bitung', '1')
<query>INSERT INTO par_kabupaten VALUES ('66a0d4d6889efb40228e10140dbfc1d1b0ef2f3e', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Manado', '1')
<query>INSERT INTO par_kabupaten VALUES ('36e9f3c6dbd0dbef1f25fd93ef902d74a42c82c7', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Tomohon', '1')
<query>INSERT INTO par_kabupaten VALUES ('b9ce2db479cf9805275549ed1397bb36da9a5f69', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Palu', '1')
<query>INSERT INTO par_kabupaten VALUES ('4ecef38c03d777dcc306b703df8319cd8ef26329', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Kendari', '1')
<query>INSERT INTO par_kabupaten VALUES ('abcc425b4dce4c5c6c3a42e57df67a332e7275bf', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'Bau-bau', '1')
<query>INSERT INTO par_kabupaten VALUES ('fc8041b5359101d7741e6549e2fdf6e93c731cca', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Makassar', '1')
<query>INSERT INTO par_kabupaten VALUES ('c15cd01cade7d40c0e95ef37d938a12fccbac87f', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Palopo', '1')
<query>INSERT INTO par_kabupaten VALUES ('db7552a82c0668fce1bc121d9a4c0b9cc554d450', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Pare-pare', '1')
<query>INSERT INTO par_kabupaten VALUES ('51fcadcaa188f6ace43611645efd71c950ab5c29', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Ambon', '1')
<query>INSERT INTO par_kabupaten VALUES ('1ab3ae3aae2d662adabf03699648e1bc50a947c4', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Tual', '1')
<query>INSERT INTO par_kabupaten VALUES ('b654ef707650780e7097b42f0e6c1311d2354d19', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Batu', '1')
<query>INSERT INTO par_kabupaten VALUES ('de44d1ae25b9258d646a2b9b6b0e558ef9608f6b', '0b486467ca176631589de72c19779fe674f13388', 'Cirebon', '1')
<query>INSERT INTO par_kabupaten VALUES ('39411df8b28577d2dd744a88137f9866a52f6fd5', '0b486467ca176631589de72c19779fe674f13388', 'Cimahi', '1')
<query>INSERT INTO par_kabupaten VALUES ('a5a1f057a486d998adcf7229c1e3af0c88383d51', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'Tangerang Selatan', '1')
<query>INSERT INTO par_kabupaten VALUES ('97e98f3a38c4c2943abb479196de0a0164612659', '0b486467ca176631589de72c19779fe674f13388', 'Sukabumi', '1')
<query>INSERT INTO par_kabupaten VALUES ('537904160c839faf80dfc07950f509dbd1f5c7c3', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Pematang Siantar', '1')
<query>INSERT INTO par_kabupaten VALUES ('edb6dda1c461650d55f84c8260445bd069c85a24', '0b486467ca176631589de72c19779fe674f13388', 'Depok', '1')
<query>INSERT INTO par_kabupaten VALUES ('329ca1d8b316f42fa977cfc7aee782917001fc59', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Banjarmasin', '1')
<query>INSERT INTO par_kabupaten VALUES ('202ecf2aae824cba4b710850e783d708ba62b795', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'Jakarta Pusat', '1')
<query>DROP TABLE IF EXISTS par_kategori_ref
<query>CREATE TABLE `par_kategori_ref` (
  `kategori_ref_id` varchar(50) NOT NULL,
  `kategori_ref_name` varchar(100) NOT NULL,
  `kategori_ref_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kategori_ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_kategori_ref VALUES ('fc506ea499fbc0c48b82e584bdab8f560b103e39', 'Pedoman Kementerian', '0')
<query>INSERT INTO par_kategori_ref VALUES ('44a30809871b51ca636e55d757d309c6070b8183', 'Pedoman Pengawasan', '1')
<query>INSERT INTO par_kategori_ref VALUES ('c0c1cf2120d8017e100cc9283bb116e3a294aa7e', 'Pedoman Perudang-undangan', '0')
<query>INSERT INTO par_kategori_ref VALUES ('2ee418634624573b9c9606b4b158e459e5ec8a0e', 'Prosedur Pengawasan', '1')
<query>INSERT INTO par_kategori_ref VALUES ('7125197d541916478b4ba7ff30a0517772af6cf2', 'Undang-Undang', '1')
<query>INSERT INTO par_kategori_ref VALUES ('ed638e2ffcdfe03a75fda6c4f016fa249f10eb74', 'Peraturan Pemerintah', '1')
<query>INSERT INTO par_kategori_ref VALUES ('3af6e7035b0ae166bfb6407955a7449ff63e4f06', 'Peraturan Menteri', '1')
<query>INSERT INTO par_kategori_ref VALUES ('7156d40a9a6c3728bf7e9119b370a97c3df7db08', 'Peraturan Presiden', '1')
<query>INSERT INTO par_kategori_ref VALUES ('4172949868ccda2cd0aac2f914f20885871b6a0a', 'Referensi Manajemen Risiko', '1')
<query>INSERT INTO par_kategori_ref VALUES ('1b21dcf2eb58e25a1533b9b54774040163bb6f2e', 'Referensi Audit Internal', '1')
<query>INSERT INTO par_kategori_ref VALUES ('90b77ebdf7a744deec5e0a34b328686c95e3d0c7', 'Referensi Tata Kelola Kepemerintahan', '1')
<query>DROP TABLE IF EXISTS par_kode_penyebab
<query>CREATE TABLE `par_kode_penyebab` (
  `kode_penyebab_id` varchar(50) NOT NULL,
  `kode_penyebab_name` varchar(30) NOT NULL,
  `kode_penyebab_desc` varchar(255) NOT NULL,
  `kode_penyebab_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode_penyebab_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_kode_penyebab VALUES ('38b6aee675415c0f01db1bf931af3fd3b4c978df', '123456789', 'lemahnya pengendalian intern', '0')
<query>INSERT INTO par_kode_penyebab VALUES ('a3e829db763976063f2f84d10b6a95686359d2cc', '01', 'Temuan PBJ', '1')
<query>INSERT INTO par_kode_penyebab VALUES ('eb1d12f6fe6e66fcdd5a3ffc16c3a5a2de6af316', '02', 'Temuan Kinerja', '1')
<query>INSERT INTO par_kode_penyebab VALUES ('ffb74806249397f2c0a9628bc0a059974a63314c', '03', 'Operasional', '1')
<query>DROP TABLE IF EXISTS par_kode_rekomendasi
<query>CREATE TABLE `par_kode_rekomendasi` (
  `kode_rek_id` varchar(50) NOT NULL,
  `kode_rek_code` varchar(10) NOT NULL,
  `kode_rek_desc` varchar(100) NOT NULL,
  `kode_rek_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode_rek_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_kode_rekomendasi VALUES ('0f732e34b94178c1571f434146ae8012ad802840', '01', 'Penyetoran ke kas negara/daerah, kas BUMN/D, dan masyararakat', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('48f8cbf9c187957d85984c58ad9dc6fba905141b', '05', 'Pelaksanaan sanksi administrasi kepegawaian', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('13da66974c02bdb717370493fa99e1a0ecb80d37', '03', 'Perbaikan fisik barang/jasa dalam proses pembangunan atau penggantian barang/jasa oleh rekanan', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('42020cef597d838ed29c6966788779facf006653', '04', 'Penghapusan barang milik negara/daerah', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('f85674aa63c3d814d6d5f15eef6a823844f4b776', '02', 'Pengembalian barang kepada negara, daerah, BUMN/D, dan masyarakat', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('6ae99131339271f604e9c2bdefcfcf11f41c8b96', '06', 'Perbaikan Laporan dan Penertiban Administrasi/Kelengkapan Administrasi', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('649458938697e9023096569f5ebe8c569ae23608', '07', 'Perbaikan Sistem dan Prosedur Akuntansi dan Pelaporan', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('d1bca669dbbd088662e35d56883b267bac3518a0', '08', 'Peningkatan Kualitas dan Kuantitas Sumberdaya Manusia Pendukung Sistem Pengendalian', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('d759ed18d8404efd096fbd4452dbb36606655388', '09', 'Perubahan atau Perbaikan Prosedur, Peraturan dan Kebijakan', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('d1171a5f85d8bbd2cc1f5a1601388c9efdf10d31', '10', 'Perubahan atau Perbaikan Struktur Organisasi', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('95b73b39296982e2134aaa0fa7481d9329732d8f', '11', 'Koordinasi Antar Instansi termasuk juga Penyerahan Penanganan Kasus Kepada Instansi yang Berwenang', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('25835b83d39699cf923f34b972ebf99e625ef6f6', '12', 'Pelaksanaan Penelitian oleh Tim Khusus atau Audit Lanjutan oleh Unit Pengawas Intern', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('9d95b5750ba51f4c4263347729611d28ed038384', '13', 'Pelaksanaan Sosialisasi', '1')
<query>INSERT INTO par_kode_rekomendasi VALUES ('bf4d37a6e176e96a25a8fbf3dfeb75d9054ad657', '14', 'Lain-lain', '1')
<query>DROP TABLE IF EXISTS par_kompetensi
<query>CREATE TABLE `par_kompetensi` (
  `kompetensi_id` varchar(50) NOT NULL,
  `kompetensi_name` varchar(100) NOT NULL,
  `kompetensi_desc` varchar(100) NOT NULL,
  `kompetensi_del_st` tinyint(4) NOT NULL COMMENT '0=del 1=aktif',
  PRIMARY KEY (`kompetensi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_kompetensi VALUES ('f3ad38fd0cbd91cff7e62b908115f54b2d63fcf1', 'Penjenjangan', 'Auditor Pelaksana, Auditor Pelaksana Lanjutan, Auditor Penyelia, Auditor Pertama, Auditor Muda, Audi', '1')
<query>INSERT INTO par_kompetensi VALUES ('41640807ef9f277221c36c7fa2207b6708bd297d', 'Sertifikasi', 'CIA, CFE, CISA, QIA, CGAP etc', '1')
<query>INSERT INTO par_kompetensi VALUES ('f2a7c0f8efa051fb0d8ebc21fd162598df171ee1', 'Bimbingan Teknis', 'Bimbingan Teknis Tentang Pengawasan', '1')
<query>INSERT INTO par_kompetensi VALUES ('95715709a119311ce0c1d66622801c8c9e1b8919', 'Pembentukan', 'Auditor Ahli; Auditor Terampil', '1')
<query>DROP TABLE IF EXISTS par_menu
<query>CREATE TABLE `par_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  `menu_method` varchar(50) NOT NULL,
  `menu_file` varchar(100) NOT NULL,
  `menu_parrent` int(11) NOT NULL,
  `menu_sort` int(11) NOT NULL,
  `menu_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hide, 1=show',
  `menu_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=latin1
<query>INSERT INTO par_menu VALUES ('1', 'Manajemen Risiko', '#', '', '', '0', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('2', 'Manajemen Pengawasan', '#', '', '', '0', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('3', 'Manajemen Pegawai', '#', '-', '-', '0', '3', '1', '1')
<query>INSERT INTO par_menu VALUES ('4', 'Manajemen Auditee', '#', '-', '-', '0', '4', '1', '1')
<query>INSERT INTO par_menu VALUES ('5', 'Pustaka Audit', '#', '', '', '0', '5', '1', '1')
<query>INSERT INTO par_menu VALUES ('6', 'Parameter Aplikasi', '#', '', '', '0', '6', '1', '1')
<query>INSERT INTO par_menu VALUES ('7', 'Manajemen Pengguna', '#', '-', '-', '0', '8', '1', '1')
<query>INSERT INTO par_menu VALUES ('8', 'Parameter Pegawai', 'main_page.php?method=par_auditor_main', '#', '-', '6', '3', '1', '1')
<query>INSERT INTO par_menu VALUES ('9', 'Group', 'main_page.php?method=par_group', 'par_group', 'UserManagement/group_main.php', '7', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('11', 'Daftar Pengguna', 'main_page.php?method=usermgmt', 'usermgmt', 'UserManagement/user_main.php', '7', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('30', 'Identifikasi Risiko', '#', 'risk_identifikasi', '-', '23', '6', '0', '1')
<query>INSERT INTO par_menu VALUES ('14', 'Daftar Auditee', 'main_page.php?method=auditeemgmt', 'auditeemgmt', 'AuditeeManagement/auditee_main.php', '4', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('15', 'Tambah Auditee', 'main_page.php?method=auditeemgmt&data_action=getadd', 'getadd', 'AuditeeManagement/auditee_main.php', '4', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('16', 'Parameter Auditee', 'main_page.php?method=par_auditee_main', 'par_auditee_main', 'ParameterManagement/list_parAuditee.php', '6', '4', '1', '1')
<query>INSERT INTO par_menu VALUES ('17', 'Tambah Pegawai', 'main_page.php?method=auditormgmt&data_action=getadd', 'getadd', '-', '3', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('18', 'Daftar Pegawai', 'main_page.php?method=auditormgmt', 'auditormgmt', 'AuditorManagement/auditor_main.php', '3', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('19', 'Perencanaan Pengawasan', 'main_page.php?method=auditplan', 'auditplan', '-', '2', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('20', 'Parameter Pengawasan', 'main_page.php?method=par_audit_main', '', '', '6', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('21', 'Pelaksanaan Pengawasan', 'main_page.php?method=auditassign', 'auditassign', '-', '2', '3', '1', '1')
<query>INSERT INTO par_menu VALUES ('22', 'Pustaka Program Audit', 'main_page.php?method=ref_program', 'ref_program', 'PustakaManagement/ref_program_main.php', '5', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('23', 'Siklus Manajemen Risiko', 'main_page.php?method=risk_penetapantujuan', 'risk_penetapantujuan', 'RiskManagement/penetapan_main.php', '1', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('24', 'Parameter Risiko', 'main_page.php?method=par_risk_main', '-', '-', '6', '1', '1', '0')
<query>INSERT INTO par_menu VALUES ('25', 'Pelaporan Risiko', 'main_page.php?method=risk_fil_report', 'risk_report', 'RiskManagement/laporan_filter_risiko.php', '1', '3', '1', '1')
<query>INSERT INTO par_menu VALUES ('26', 'Profil Risiko', 'main_page.php?method=risk_result', 'risk_result', '-', '2', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('28', 'Pelaporan Pengawasan', 'main_page.php?method=reportaudit', 'reportaudit', 'AuditManagement/listReportAudit_main.php', '2', '4', '1', '1')
<query>INSERT INTO par_menu VALUES ('29', 'Tindak Lanjut Audit', 'main_page.php?method=followupassign', 'tindaklanjut', '-', '2', '5', '1', '1')
<query>INSERT INTO par_menu VALUES ('31', 'Analisis Risiko', '#', 'view_analisa', '-', '23', '7', '0', '1')
<query>INSERT INTO par_menu VALUES ('32', 'Evaluasi Risiko', '#', 'view_evaluasi', '-', '23', '8', '0', '1')
<query>INSERT INTO par_menu VALUES ('33', 'Penanganan Risiko', '#', 'view_penanganan', '-', '23', '9', '0', '1')
<query>INSERT INTO par_menu VALUES ('34', 'Monitoring Risiko', '#', 'view_monitoring', '-', '23', '10', '0', '0')
<query>INSERT INTO par_menu VALUES ('35', 'Tambah', '#', 'getadd', '-', '23', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('36', 'Ubah', '#', 'getedit', '-', '23', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('37', 'Hapus', '#', 'getdelete', '-', '23', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('38', 'Set Perencanaan', '#', 'view_plan', '-', '26', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('39', 'Anggota Tim', '#', 'anggota_plan', '-', '19', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('43', 'Tambah', '#', 'getadd', '-', '19', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('41', 'Pengajuan', '#', 'getajukan', '-', '19', '5', '0', '1')
<query>INSERT INTO par_menu VALUES ('42', 'Persetujuan', '#', 'getapprove', '-', '19', '6', '0', '1')
<query>INSERT INTO par_menu VALUES ('44', 'Ubah', '#', 'getedit', '-', '19', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('45', 'Hapus', '#', 'getdelete', '-', '19', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('46', 'Tambah', '#', 'getadd', '-', '21', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('47', 'Ubah', '#', 'getedit', '-', '21', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('48', 'Hapus', '#', 'getdelete', '-', '21', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('49', 'Anggota Tim', '#', 'anggota_assign', '-', '21', '6', '0', '1')
<query>INSERT INTO par_menu VALUES ('50', 'Audit Program', '#', 'programaudit', '-', '21', '8', '0', '1')
<query>INSERT INTO par_menu VALUES ('51', 'Tambah', '#', 'getadd', '-', '50', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('52', 'Ubah', '#', 'getedit', '-', '50', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('53', 'Hapus', '#', 'getdelete', '-', '50', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('54', 'Kertas Kerja', '#', 'kertas_kerja', '-', '50', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('55', 'Tambah', '#', 'getadd', '-', '54', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('56', 'Ubah', '#', 'getedit', '-', '54', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('57', 'Hapus', '#', 'getdelete', '-', '54', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('58', 'Temuan', '#', 'finding_kka', '-', '54', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('59', 'Tambah', '#', 'getadd', '-', '58', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('60', 'Ubah', '#', 'getedit', '-', '58', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('61', 'Hapus', '#', 'getdelete', '-', '58', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('62', 'Rekomendasi', '#', 'rekomendasi', '-', '58', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('63', 'Tambah', '#', 'getadd', '-', '62', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('64', 'Ubah', '#', 'getedit', '-', '62', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('65', 'Hapus', '#', 'getdelete', '-', '62', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('66', 'Tindak Lanjut', '#', '', '', '62', '4', '0', '0')
<query>INSERT INTO par_menu VALUES ('67', 'Tambah', '#', '', '', '66', '1', '0', '0')
<query>INSERT INTO par_menu VALUES ('68', 'Ubah', '#', '', '', '66', '2', '0', '0')
<query>INSERT INTO par_menu VALUES ('69', 'Hapus', '#', '', '', '66', '3', '0', '0')
<query>INSERT INTO par_menu VALUES ('71', 'Tambah', '#', 'getadd', '-', '39', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('72', 'Ubah', '#', 'getedit', '-', '39', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('73', 'Hapus', '#', 'getdelete', '-', '39', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('74', 'Tambah', '#', 'getadd', '-', '49', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('75', 'Ubah', '#', 'getedit', '-', '49', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('76', 'Hapus', '#', 'getdelete', '-', '49', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('77', 'Tambah', '#', 'getadd', '-', '29', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('78', 'Ubah', '#', 'getedit', '-', '29', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('79', 'Hapus', '#', 'getdelete', '-', '29', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('80', 'Tambah', '#', 'getadd', '-', '18', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('81', 'Ubah', '#', 'getedit', '-', '18', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('82', 'Hapus', '#', 'getdelete', '-', '18', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('83', 'Tambah', '#', 'getadd', '-', '14', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('84', 'Ubah', '#', 'getedit', '-', '14', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('85', 'Hapus', '#', 'getdelete', '-', '14', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('86', 'Tambah', '#', 'getadd', '-', '22', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('87', 'Ubah', '#', 'getedit', '-', '22', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('88', 'Hapus', '#', 'getdelete', '-', '22', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('89', 'Tambah', '#', 'getadd', '124', '30', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('90', 'Ubah', '#', 'getedit', '-', '30', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('91', 'Hapus', '#', 'getdelete', '-', '30', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('92', 'Pengajuan', '#', 'getajukan', '-', '23', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('93', 'Persetujuan', '#', 'getapprove', '-', '23', '5', '0', '1')
<query>INSERT INTO par_menu VALUES ('94', 'Back Up Restore Data', 'main_page.php?method=backuprestore', 'backuprestore', 'UserManagement/backuprestore_main.php', '7', '3', '1', '1')
<query>INSERT INTO par_menu VALUES ('95', 'Log Aktifitas', 'main_page.php?method=log_aktifitas', 'log_aktifitas', 'UserManagement/log_main.php', '7', '4', '1', '1')
<query>INSERT INTO par_menu VALUES ('96', 'Pustaka Referensi Audit', 'main_page.php?method=ref_audit', 'ref_audit', 'PustakaManagement/ref_audit_main.php', '5', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('97', 'Monitoring dan Reviu Risiko', 'main_page.php?method=risk_reviu', 'view_monitoring', 'RiskManagement/reviu_main.php', '1', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('98', 'Tambah', '#', 'getadd', '-', '97', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('99', 'Ubah', '#', 'getedit', '-', '97', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('100', 'Tambah', '#', 'getadd', '-', '96', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('101', 'Ubah', '#', 'getedit', '-', '96', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('102', 'Hapus', '#', 'getdelete', '-', '96', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('103', 'Pengajuan', '#', 'getajukan_tl', '-', '29', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('104', 'Persetujuan', '#', 'getapprove_tl', '-', '29', '5', '0', '1')
<query>INSERT INTO par_menu VALUES ('105', 'Pengajuan', '#', 'getajukan_penugasan', '-', '21', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('106', 'Persetujuan', '#', 'getapprove_penugasan', '-', '21', '5', '0', '1')
<query>INSERT INTO par_menu VALUES ('109', 'SBM Rinci', 'main_page.php?method=par_sbu_rinci', 'par_sbu_rinci', 'BudgetManagement/sbu_rinci_main.php', '107', '2', '1', '1')
<query>INSERT INTO par_menu VALUES ('110', 'Tambah', 'main_page.php?method=par_sbu', 'getadd', 'BudgetManagement/sbu_main.php', '108', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('111', 'Ubah', 'main_page.php?method=par_sbu', 'getedit', 'BudgetManagement/sbu_main.php', '108', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('112', 'Hapus', 'main_page.php?method=par_sbu', 'getdelete', 'BudgetManagement/sbu_main.php', '108', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('113', 'Tambah', 'main_page.php?method=par_sbu_rinci', 'getadd', 'BudgetManagement/sbu_rinci_main.php', '109', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('114', 'Ubah', 'main_page.php?method=par_sbu_rinci', 'getedit', 'BudgetManagement/sbu_rinci_main.php', '109', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('115', 'Hapus', 'main_page.php?method=par_sbu_rinci', 'getdelete', 'BudgetManagement/sbu_rinci_main.php', '109', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('107', 'Manajemen Anggaran', '#', '-', '-', '0', '7', '1', '1')
<query>INSERT INTO par_menu VALUES ('108', 'SBM', 'main_page.php?method=par_sbu', 'par_sbu', 'BudgetManagement/sbu_main.php', '107', '1', '1', '1')
<query>INSERT INTO par_menu VALUES ('121', 'Persetujuan', 'main_page.php?method=surattugas', 'getapprove', 'AuditManagement/surat_tugas_main.php', '116', '5', '0', '1')
<query>INSERT INTO par_menu VALUES ('120', 'Pengajuan', 'main_page.php?method=surattugas', 'getajukan', 'AuditManagement/surat_tugas_main.php', '116', '4', '0', '1')
<query>INSERT INTO par_menu VALUES ('119', 'Hapus', 'main_page.php?method=surattugas', 'getdelete', 'AuditManagement/surat_tugas_main.php', '116', '3', '0', '1')
<query>INSERT INTO par_menu VALUES ('118', 'Ubah', 'main_page.php?method=surattugas', 'getedit', 'AuditManagement/surat_tugas_main.php', '116', '2', '0', '1')
<query>INSERT INTO par_menu VALUES ('117', 'Tambah', 'main_page.php?method=surattugas', 'getadd', 'AuditManagement/surat_tugas_main.php', '116', '1', '0', '1')
<query>INSERT INTO par_menu VALUES ('116', 'Surat Tugas', 'main_page.php?method=surattugas', 'surattugas', 'AuditManagement/surat_tugas_main.php', '21', '7', '0', '1')
<query>DROP TABLE IF EXISTS par_pangkat_auditor
<query>CREATE TABLE `par_pangkat_auditor` (
  `pangkat_id` varchar(50) NOT NULL,
  `pangkat_name` varchar(10) NOT NULL,
  `pangkat_desc` varchar(50) NOT NULL,
  `pangkat_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=hide, 1=show',
  PRIMARY KEY (`pangkat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_pangkat_auditor VALUES ('91315041c0bfa3e0ac1a79afbc268f7d90eaec25', 'III/a', 'Penata Muda', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('3a41229b90364ece381302f990389a9c141b3800', 'III/b', 'Penata Muda Tingkat I', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('3afc1f39990d28ecc67bd99c6e251970e334cc7e', 'III/c', 'Penata', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('71b2743ce715c1418dd7307f4b5f6d98d2c8662a', 'III/d', 'Penata Tingkat I', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('dfbdb51d2273a3c42d038dbe03884d657f68f501', 'IV/a', 'Pembina', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('2c7d2f13f69aae77926eef2ec436c1bfdf61b7b7', 'IV/b', 'Pembina Tingkat I', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('b82deb90cb8921db2dc09443ee4c00b44cb0b3c9', 'II/c', 'Pengatur Muda', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('dad7254bdaf3af188270d73c9cbda9849d79b988', 'IV/e', 'Pembina Utama', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('85c2a8fa3630cd3b5bc3322202572ca3a2526589', 'II/d', 'Pengatur Tingkat 1', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('919748dcf6deebf6f8b06a8da290e7688495f4f8', 'IV/c', 'Pembina Utama Muda', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('326c8092e1a5d8851d126adb8e5f611515cda883', 'IV/d', 'Pembina Utama Madya', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('2fa6054ce5103b6dbf4bfd8fbf1c7cb8c324f283', 'II/b', 'Pengatur Muda Tingkat I', '1')
<query>INSERT INTO par_pangkat_auditor VALUES ('8411f15577c104b0634a0235105e199d4cae53f3', 'II/a', 'Pengatur Muda', '1')
<query>DROP TABLE IF EXISTS par_posisi_penugasan
<query>CREATE TABLE `par_posisi_penugasan` (
  `posisi_id` varchar(50) NOT NULL,
  `posisi_name` varchar(30) NOT NULL,
  `posisi_code` varchar(5) NOT NULL,
  `posisi_sort` varchar(255) NOT NULL,
  `posisi_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`posisi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_posisi_penugasan VALUES ('9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd', 'Pengendali Teknis', 'PT', '2', '1')
<query>INSERT INTO par_posisi_penugasan VALUES ('1fe7f8b8d0d94d54685cbf6c2483308aebe96229', 'Pengendali Mutu', 'PM', '1', '1')
<query>INSERT INTO par_posisi_penugasan VALUES ('8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', 'Ketua Tim', 'KT', '3', '1')
<query>INSERT INTO par_posisi_penugasan VALUES ('6a70c2a39af30df978a360e556e1102a2a0bdc02', 'Anggota Tim', 'AT', '4', '1')
<query>DROP TABLE IF EXISTS par_posisi_role
<query>CREATE TABLE `par_posisi_role` (
  `audit_akses_posisi_id` varchar(50) NOT NULL,
  `audit_akses_action` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_posisi_role VALUES ('9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd', '1')
<query>INSERT INTO par_posisi_role VALUES ('1fe7f8b8d0d94d54685cbf6c2483308aebe96229', '2')
<query>INSERT INTO par_posisi_role VALUES ('8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '3')
<query>INSERT INTO par_posisi_role VALUES ('f16b0682bf3574838d396805fd8c4afe3a005430', '3')
<query>INSERT INTO par_posisi_role VALUES ('f16b0682bf3574838d396805fd8c4afe3a005430', '1')
<query>DROP TABLE IF EXISTS par_propinsi
<query>CREATE TABLE `par_propinsi` (
  `propinsi_id` varchar(50) NOT NULL,
  `propinsi_name` varchar(100) NOT NULL,
  `propinsi_del_st` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`propinsi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_propinsi VALUES ('1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'Riau', '1')
<query>INSERT INTO par_propinsi VALUES ('11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'Sumatera Utara', '1')
<query>INSERT INTO par_propinsi VALUES ('4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'Nanggroe Aceh Darussalam', '1')
<query>INSERT INTO par_propinsi VALUES ('675eed8a709d4098541fb8f73805004c84725d49', 'Jambi', '1')
<query>INSERT INTO par_propinsi VALUES ('fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'Sumatera Barat', '1')
<query>INSERT INTO par_propinsi VALUES ('bc02f4d326c699e711b05bf45925b3652464457a', 'Kepulauan Riau', '1')
<query>INSERT INTO par_propinsi VALUES ('c25868ef9cc8b9d93e7043e07e354b2946f554df', 'Sumatera Selatan', '1')
<query>INSERT INTO par_propinsi VALUES ('3a9fd0542122e96ed7de690697ce088ac6311f1b', 'Bangka Belitung', '1')
<query>INSERT INTO par_propinsi VALUES ('d8c82bf22dd5b689e806f6e381dd3ad459e7485c', 'Bengkulu', '1')
<query>INSERT INTO par_propinsi VALUES ('8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'Lampung', '1')
<query>INSERT INTO par_propinsi VALUES ('99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'DKI Jakarta', '1')
<query>INSERT INTO par_propinsi VALUES ('0b486467ca176631589de72c19779fe674f13388', 'Jawa Barat', '1')
<query>INSERT INTO par_propinsi VALUES ('a1a02d8be166d22d766bc596c4a08979dc906c7a', 'Banten', '1')
<query>INSERT INTO par_propinsi VALUES ('8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'Jawa Tengah', '1')
<query>INSERT INTO par_propinsi VALUES ('a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'Yogyakarta', '1')
<query>INSERT INTO par_propinsi VALUES ('aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'Jawa Timur', '1')
<query>INSERT INTO par_propinsi VALUES ('f183f90fb5f85df77a6150d74fef99c75b0608a1', 'Bali', '1')
<query>INSERT INTO par_propinsi VALUES ('93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'Nusa Tenggara Barat', '1')
<query>INSERT INTO par_propinsi VALUES ('3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'Nusa Tenggara Timur', '1')
<query>INSERT INTO par_propinsi VALUES ('499db371dac2489052cf151f8e861ac9d9172df7', 'Kalimantan Barat', '1')
<query>INSERT INTO par_propinsi VALUES ('c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'Kalimantan Timur', '1')
<query>INSERT INTO par_propinsi VALUES ('175f37cdb244988462e7946a62f190f8bcd3c048', 'Kalimantan Tengah', '1')
<query>INSERT INTO par_propinsi VALUES ('260e10323ded2fca85ec8d93337b236b0f29e2f0', 'Kalimantan Selatan', '1')
<query>INSERT INTO par_propinsi VALUES ('6e34f7aeaf90d916efdf09be2db9184a077d466d', 'Sulawesi Utara', '1')
<query>INSERT INTO par_propinsi VALUES ('59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'Gorontalo', '1')
<query>INSERT INTO par_propinsi VALUES ('330bc0efcbfc115f11d36d01b05ede6f42236da0', 'Sulawesi Tengah', '1')
<query>INSERT INTO par_propinsi VALUES ('df2e581894d521cc3f6ed134ccc4d057550337c9', 'Sulawesi Tenggara', '1')
<query>INSERT INTO par_propinsi VALUES ('708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'Sulawesi Selatan', '1')
<query>INSERT INTO par_propinsi VALUES ('9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'Sulawesi Barat', '1')
<query>INSERT INTO par_propinsi VALUES ('ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'Maluku', '1')
<query>INSERT INTO par_propinsi VALUES ('a3959bc615df01ca44c0374239d089f46c8e06d5', 'Maluku Utara', '1')
<query>INSERT INTO par_propinsi VALUES ('eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'Papua', '1')
<query>INSERT INTO par_propinsi VALUES ('cac6bb408da778b1dd986cf581e1405801395cdc', 'Papua Barat', '1')
<query>INSERT INTO par_propinsi VALUES ('5ecfef4589a11799683bb20435868c23620b01e8', 'Kalimantan Utara', '1')
<query>INSERT INTO par_propinsi VALUES ('40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'Kalimantan Tenggara', '1')
<query>DROP TABLE IF EXISTS par_risk_kategori
<query>CREATE TABLE `par_risk_kategori` (
  `risk_kategori_id` varchar(50) NOT NULL,
  `risk_kategori` varchar(100) NOT NULL,
  `risk_kategori_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=del 1=aktif',
  PRIMARY KEY (`risk_kategori_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_kategori VALUES ('302d8fd00a2a86db44e01257269a47a25c94f52d', 'Risiko Reputasi', '0')
<query>INSERT INTO par_risk_kategori VALUES ('1428283071c4ebf2011c6f2a561ee1689d78e605', 'Risiko Operasional', '0')
<query>INSERT INTO par_risk_kategori VALUES ('454b438cdbaf48c9122627c2d8312a2ba6c648c7', 'Risiko Kepatuhan', '0')
<query>INSERT INTO par_risk_kategori VALUES ('1a8fe52a6682d30bc6964bb7167ff2aec83f0227', 'Risiko Hukum', '0')
<query>INSERT INTO par_risk_kategori VALUES ('8e64e875b4b79e345f66541310518e350f3cb124', 'Risiko Kecurangan', '0')
<query>INSERT INTO par_risk_kategori VALUES ('60c5a7a350127b6ca0e23bd51aab45cab21f9af1', 'Risiko Keuangan', '0')
<query>INSERT INTO par_risk_kategori VALUES ('0c310e2975b93e336d06ffd0f5f2ab09fc92d65e', 'Risiko Strategis', '0')
<query>INSERT INTO par_risk_kategori VALUES ('2ece7ba19c14e920eecf7c942e5584655b75cdf2', 'Sangat Rendah', '0')
<query>INSERT INTO par_risk_kategori VALUES ('1261d319fbef14da47b45a6a67ec23b9170af9fd', 'Rendah', '0')
<query>INSERT INTO par_risk_kategori VALUES ('ec9211a486f303943cd7b88af4752c8259722b49', 'Sedang', '0')
<query>INSERT INTO par_risk_kategori VALUES ('0a27659904f302cd623da3ff57773b5c0890fc86', 'Tinggi', '0')
<query>INSERT INTO par_risk_kategori VALUES ('1db2e2fa07d1a031511433ba2cb53f0569e5c9b2', 'Sangat Tinggi', '0')
<query>INSERT INTO par_risk_kategori VALUES ('752b9fa1fcb57d1c76a68e38acb4f55ea94aab01', 'Organisasi', '1')
<query>INSERT INTO par_risk_kategori VALUES ('bbb4f7cbc2b58e5f6a7392dfd76932e93ec284c3', 'Kebijakan/Tata Kelola', '1')
<query>INSERT INTO par_risk_kategori VALUES ('5d686b9376e7068f317ea5bbdfdc59635aef5634', 'Peraturan Perundang-undangan', '1')
<query>INSERT INTO par_risk_kategori VALUES ('55023f763fd5cd2995aa130189179d4796b9f0a3', 'Sumber Daya Manusia', '1')
<query>INSERT INTO par_risk_kategori VALUES ('3ff7bc7d69bab039a826b2bdcf68dd0b5071dbba', 'Akuntabilitas', '1')
<query>INSERT INTO par_risk_kategori VALUES ('b0b376d19f2dd63f13dbbab94f0de10f07b3feb6', 'Pengawasan', '1')
<query>INSERT INTO par_risk_kategori VALUES ('516f80d3a9e2110a46730e29b2667b839db2e0b9', 'Pelayanan Publik', '1')
<query>INSERT INTO par_risk_kategori VALUES ('3f29211e9128ead9352587f620cf77784e59ac21', 'Harga Satuan', '1')
<query>INSERT INTO par_risk_kategori VALUES ('a1343d085aa214e98298b1ccbbfd62daa3d66f9f', 'Akun Mata Anggaran', '1')
<query>DROP TABLE IF EXISTS par_risk_matrix_residu
<query>CREATE TABLE `par_risk_matrix_residu` (
  `matrix_residu_id` varchar(50) NOT NULL,
  `matrix_residu_ri_id` varchar(50) NOT NULL,
  `matrix_residu_pi_id` varchar(50) NOT NULL,
  `matrix_residu_value` varchar(50) NOT NULL,
  PRIMARY KEY (`matrix_residu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_matrix_residu VALUES ('a5693e62570efdd7723691c1c60d225c82f939bd', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c', '55c34ac8042fd28f5a2afcc2ca6e3ebf426c64c9', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('a3bcebd28d735900f95129760fe0acaa6e7523bc', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c', '3637c5bebb616131ce0ec2daec12074e17a57675', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('e066ca7a7f775738579cc254bc0fabcd13a40dbe', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c', 'fa388797df07fb5f2b4d1ec8d6d6301c7d69d301', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('3c250ca58354fad3cfdaa02e8e75575c677fa7dc', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c', '094b2317df42ca10c5cb9bee11ae3d32e4595775', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('e69be84f3c5bc58fc77e9b9b96a7383da3cfa983', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c', 'cafc09184e3a07e59dd71b2f28e5a6f371bb2935', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('2aa26c7f7eb2f4f99a99c2b083a651cf07b09ce5', '294a72871fc127c6d730a3aefaaf0d54254cab54', '55c34ac8042fd28f5a2afcc2ca6e3ebf426c64c9', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('ed737e3201eb766d524b978a91d393bcffa388fe', '294a72871fc127c6d730a3aefaaf0d54254cab54', '3637c5bebb616131ce0ec2daec12074e17a57675', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('9d28f99c822db6f13f6184b4613796c07d7b78c8', '294a72871fc127c6d730a3aefaaf0d54254cab54', 'fa388797df07fb5f2b4d1ec8d6d6301c7d69d301', '294a72871fc127c6d730a3aefaaf0d54254cab54')
<query>INSERT INTO par_risk_matrix_residu VALUES ('87f6db631624252375888329827620f63fd0617b', '294a72871fc127c6d730a3aefaaf0d54254cab54', '094b2317df42ca10c5cb9bee11ae3d32e4595775', '294a72871fc127c6d730a3aefaaf0d54254cab54')
<query>INSERT INTO par_risk_matrix_residu VALUES ('c7cebd617e5aecfd6bb84d6dfbaa80aff6fa6d8a', '294a72871fc127c6d730a3aefaaf0d54254cab54', 'cafc09184e3a07e59dd71b2f28e5a6f371bb2935', '294a72871fc127c6d730a3aefaaf0d54254cab54')
<query>INSERT INTO par_risk_matrix_residu VALUES ('a06b79a1906ba52569eb3fbf86184a9c16eef7b2', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294', '55c34ac8042fd28f5a2afcc2ca6e3ebf426c64c9', 'ca572b48cc5584a5541443b3a33806b2d84e6c2c')
<query>INSERT INTO par_risk_matrix_residu VALUES ('3cf7167d0f85e48e7455125d33c7a9cb32e8ec8a', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294', '3637c5bebb616131ce0ec2daec12074e17a57675', '294a72871fc127c6d730a3aefaaf0d54254cab54')
<query>INSERT INTO par_risk_matrix_residu VALUES ('9dd0b228cfb2de130110d93d9c8214474736d2f2', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294', 'fa388797df07fb5f2b4d1ec8d6d6301c7d69d301', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294')
<query>INSERT INTO par_risk_matrix_residu VALUES ('efbb2c942d70135b2cdbbc29a2f1e073b7b29e84', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294', '094b2317df42ca10c5cb9bee11ae3d32e4595775', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294')
<query>INSERT INTO par_risk_matrix_residu VALUES ('2b4405c4e7f311cc9a3eb46b139ddb726f209ead', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294', 'cafc09184e3a07e59dd71b2f28e5a6f371bb2935', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294')
<query>INSERT INTO par_risk_matrix_residu VALUES ('87fa9d828a6db87db55acf95efc5747e6c41cc5b', 'd4b2ff452635037706f0e07cdd6f173e34be86b0', '55c34ac8042fd28f5a2afcc2ca6e3ebf426c64c9', '294a72871fc127c6d730a3aefaaf0d54254cab54')
<query>INSERT INTO par_risk_matrix_residu VALUES ('a08b9ed913f8027de00a97f95765c8499aebced3', 'd4b2ff452635037706f0e07cdd6f173e34be86b0', '3637c5bebb616131ce0ec2daec12074e17a57675', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294')
<query>INSERT INTO par_risk_matrix_residu VALUES ('8bc8a42df2e65b576fe9d7ba261b5be20a980964', 'd4b2ff452635037706f0e07cdd6f173e34be86b0', 'fa388797df07fb5f2b4d1ec8d6d6301c7d69d301', 'd4b2ff452635037706f0e07cdd6f173e34be86b0')
<query>INSERT INTO par_risk_matrix_residu VALUES ('b78cf8f48a37bf48e84846128b140bc5d610b87b', 'd4b2ff452635037706f0e07cdd6f173e34be86b0', '094b2317df42ca10c5cb9bee11ae3d32e4595775', 'd4b2ff452635037706f0e07cdd6f173e34be86b0')
<query>INSERT INTO par_risk_matrix_residu VALUES ('32342a111787b9f8ecc049cbc0134ed71968b01a', 'd4b2ff452635037706f0e07cdd6f173e34be86b0', 'cafc09184e3a07e59dd71b2f28e5a6f371bb2935', 'd4b2ff452635037706f0e07cdd6f173e34be86b0')
<query>INSERT INTO par_risk_matrix_residu VALUES ('cd1d4d7e9808eab5ce6ff5869f3aa03d602aa191', '731a4f376eed6c4a30ccde31928fb018155a6b3d', '55c34ac8042fd28f5a2afcc2ca6e3ebf426c64c9', '294a72871fc127c6d730a3aefaaf0d54254cab54')
<query>INSERT INTO par_risk_matrix_residu VALUES ('665fa990c4fb3a9187fee1a2998651bf8d8a6b42', '731a4f376eed6c4a30ccde31928fb018155a6b3d', '3637c5bebb616131ce0ec2daec12074e17a57675', '0c6e6b0b4769f5419079b172db2ef6b5a9fcd294')
<query>INSERT INTO par_risk_matrix_residu VALUES ('ac6fef830a2561350a4035e5d5ceadab9ba7828c', '731a4f376eed6c4a30ccde31928fb018155a6b3d', 'fa388797df07fb5f2b4d1ec8d6d6301c7d69d301', 'd4b2ff452635037706f0e07cdd6f173e34be86b0')
<query>INSERT INTO par_risk_matrix_residu VALUES ('0f1281b140f2b7ee3ee3cb1e2822b7adda877683', '731a4f376eed6c4a30ccde31928fb018155a6b3d', '094b2317df42ca10c5cb9bee11ae3d32e4595775', '731a4f376eed6c4a30ccde31928fb018155a6b3d')
<query>INSERT INTO par_risk_matrix_residu VALUES ('3f0eb12b3f1d84fce92ecdf3943d6846ba34b9b5', '731a4f376eed6c4a30ccde31928fb018155a6b3d', 'cafc09184e3a07e59dd71b2f28e5a6f371bb2935', '731a4f376eed6c4a30ccde31928fb018155a6b3d')
<query>DROP TABLE IF EXISTS par_risk_penanganan
<query>CREATE TABLE `par_risk_penanganan` (
  `risk_penanganan_id` varchar(50) NOT NULL,
  `risk_penanganan_jenis` varchar(50) NOT NULL,
  `risk_penanganan_desc` varchar(255) NOT NULL,
  `risk_penanganan_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`risk_penanganan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_penanganan VALUES ('7e861dc0d7a58204659eb765ddd3c8dfcf19eb40', 'Transfer', 'Memindahkan sejumlah risiko dari organisasi ke entitas lain', '0')
<query>INSERT INTO par_risk_penanganan VALUES ('a407ff621bae384b57e896eb23f71ec59f01fb1a', 'Kendalikan', 'Mengendalikan risiko', '1')
<query>INSERT INTO par_risk_penanganan VALUES ('b6935efd01543b229b95b745e51bad0647551715', 'Retensi', 'Menerima dan menyerap suatu risiko', '0')
<query>DROP TABLE IF EXISTS par_risk_pi
<query>CREATE TABLE `par_risk_pi` (
  `pi_id` varchar(50) NOT NULL,
  `pi_name` varchar(50) NOT NULL,
  `pi_value` int(3) NOT NULL,
  `pi_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`pi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_pi VALUES ('fa388797df07fb5f2b4d1ec8d6d6301c7d69d301', 'Sedang', '3', 'Rancangan dan implementasi pengendalian internal cukup efektif')
<query>INSERT INTO par_risk_pi VALUES ('3637c5bebb616131ce0ec2daec12074e17a57675', 'Kuat', '4', 'Rancangan dan implementasi pengendalian internal efektif')
<query>INSERT INTO par_risk_pi VALUES ('094b2317df42ca10c5cb9bee11ae3d32e4595775', 'Lemah', '2', 'Rancangan dan implementasi pengendalian internal tidak efektif')
<query>INSERT INTO par_risk_pi VALUES ('55c34ac8042fd28f5a2afcc2ca6e3ebf426c64c9', 'Sangat Kuat', '5', 'Rancangan dan implementasi pengendalian internal sangat efektif')
<query>INSERT INTO par_risk_pi VALUES ('cafc09184e3a07e59dd71b2f28e5a6f371bb2935', 'Sangat Lemah', '1', 'Rancangan dan implementasi pengendalian internal sangat tidak efektif')
<query>DROP TABLE IF EXISTS par_risk_ri
<query>CREATE TABLE `par_risk_ri` (
  `ri_id` varchar(50) NOT NULL,
  `ri_name` varchar(50) NOT NULL,
  `ri_atas` float NOT NULL,
  `ri_bawah` float NOT NULL,
  `ri_value` int(3) NOT NULL,
  `ri_warna` varchar(10) NOT NULL,
  PRIMARY KEY (`ri_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_ri VALUES ('ca572b48cc5584a5541443b3a33806b2d84e6c2c', 'Sangat Rendah', '2.99', '1', '1', '1CFF3A')
<query>INSERT INTO par_risk_ri VALUES ('294a72871fc127c6d730a3aefaaf0d54254cab54', 'Rendah', '4.99', '3', '2', '0D0DFF')
<query>INSERT INTO par_risk_ri VALUES ('0c6e6b0b4769f5419079b172db2ef6b5a9fcd294', 'Sedang', '9.99', '5', '3', 'F7FF12')
<query>INSERT INTO par_risk_ri VALUES ('d4b2ff452635037706f0e07cdd6f173e34be86b0', 'Tinggi', '14.99', '10', '4', 'FF760D')
<query>INSERT INTO par_risk_ri VALUES ('731a4f376eed6c4a30ccde31928fb018155a6b3d', 'Sangat Tinggi', '25', '15', '5', 'FF1B0A')
<query>DROP TABLE IF EXISTS par_risk_rr
<query>CREATE TABLE `par_risk_rr` (
  `rr_id` varchar(50) NOT NULL,
  `rr_name` varchar(50) NOT NULL,
  `rr_atas` float NOT NULL,
  `rr_bawah` float NOT NULL,
  `rr_value` int(3) NOT NULL,
  `rr_warna` varchar(10) NOT NULL,
  PRIMARY KEY (`rr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_rr VALUES ('728eee16cdf7f9bd5a11a3916ff8975f71f1a93c', 'Sangat Rendah', '1', '0', '1', '1CFF3A')
<query>INSERT INTO par_risk_rr VALUES ('7fbc015ee9a07f9ad3b172010db60d9a3b1cb903', 'Rendah', '2', '1', '2', '0D0DFF')
<query>INSERT INTO par_risk_rr VALUES ('c6934ba34b50a73bd918c58d8f6afaf1fc5a9cf2', 'Sedang', '3', '2', '3', 'F7FF12')
<query>INSERT INTO par_risk_rr VALUES ('9573a66d86be4f054b591ac1e05e4d1e911991b9', 'Tinggi', '4', '3', '4', 'FF760D')
<query>INSERT INTO par_risk_rr VALUES ('d31c5d2f0ce8c8f7d9a70f79490f16042a711d24', 'Sangat Tinggi', '5', '4', '5', 'FF1B0A')
<query>DROP TABLE IF EXISTS par_risk_selera
<query>CREATE TABLE `par_risk_selera` (
  `risk_selera_id` varchar(50) NOT NULL,
  `risk_selera` varchar(50) NOT NULL,
  `risk_selera_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=del 1=aktif',
  PRIMARY KEY (`risk_selera_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_selera VALUES ('8b8cb86119c8c32284df5ab0b236a161cdec5503', 'Sangat Rendah', '1')
<query>INSERT INTO par_risk_selera VALUES ('cc05fa8f918ec35f85d8c057d4126eded084e510', 'Rendah', '1')
<query>INSERT INTO par_risk_selera VALUES ('cf5ac932e274424a01280030fe7e5e18752b45a6', 'Sedang', '1')
<query>INSERT INTO par_risk_selera VALUES ('dc4bc4d1bd56195dba37c18dcb8f1127c39b6032', 'Tinggi', '1')
<query>INSERT INTO par_risk_selera VALUES ('f15ab4e4242ce1af192b339442930395074cf68c', 'Sangat Tinggi', '1')
<query>DROP TABLE IF EXISTS par_risk_td
<query>CREATE TABLE `par_risk_td` (
  `td_id` varchar(50) NOT NULL,
  `td_name` varchar(50) NOT NULL,
  `td_value` int(3) NOT NULL,
  `td_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`td_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_td VALUES ('fbddd2006aa20b0ee3e04b3a8799d058498bf7db', 'Sangat Tinggi', '5', 'Sebagian besar tujuan intansi/kegiatan gagal dilaksanakan; Terganggunya pelayanan lebih dari 1 minggu; Mengancam program dan organisasi serta stakeholders; Kerugian sangat besar bagi organisasi dari segi keuangan maupun non keuangan.')
<query>INSERT INTO par_risk_td VALUES ('b71541cf1cb940f48f4a45f9c122983d94cc2f2e', 'Tinggi', '4', 'Sebagian tujuan intansi/kegiatan gagal dilaksanakan; Terganggunya pelayanan lebih dari 2 hari tetapi kurang dari 1 minggu; Mengancam fungsi program yang efektif dan organisasi; Kerugian besar bagi organisasi dari segi keuangan maupun non keuangan.')
<query>INSERT INTO par_risk_td VALUES ('1aba3d85c7dd32290134f92406311b3b9cd146ac', 'Sedang', '3', 'Mengganggu pencapaian tujuan intansi/kegiatan secara signifikan; Mengganggu kegiatan pelayanan secara signifikan; Mengganggu administrasi program; Kerugian keuangan cukup besar')
<query>INSERT INTO par_risk_td VALUES ('6d85854ceaf21f02c70b3291468fa5eba133ce58', 'Rendah', '2', 'Mengganggu pencapaian tujuan intansi/kegiatan meskipun tidak signifikan; Cukup menggangu jalannya pelayanan; Mengancam efisiensi dan efektivitas beberapa aspek program; Kerugian kurang material dan sedikit mempengaruhi stakeholders')
<query>INSERT INTO par_risk_td VALUES ('889bd5a5ad3f81629d2b1ac982bd42b696a81176', 'Sangat Rendah', '1', 'Tidak berdampak pada pencapaian tujuan intansi/kegiatan secara umum; Agak mengganggu pelayanan; Dampaknya dapat ditangani pada tahap kegiatan rutin; Kerugian kurang material dan tidak mempengaruhi stakeholders')
<query>DROP TABLE IF EXISTS par_risk_tk
<query>CREATE TABLE `par_risk_tk` (
  `tk_id` varchar(50) NOT NULL,
  `tk_name` varchar(50) NOT NULL,
  `tk_value` int(3) NOT NULL,
  `tk_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`tk_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_risk_tk VALUES ('6a14ee046bc1c48ffd58fc491d7d4cb6b1f8286b', 'Hampir Pasti Terjadi', '5', 'Peristiwa selalu terjadi hampir pada setiap kondisi; Persentase > 90%')
<query>INSERT INTO par_risk_tk VALUES ('9226e23a3427750453e8f254e0757a246b8b59dd', 'Sering Terjadi', '4', 'Peristiwa sangat mungkin terjadi pada sebagian kondisi; Persentase > 50-90%')
<query>INSERT INTO par_risk_tk VALUES ('515248412be39eba4cd4efdd29db9dbf1e530c50', 'Kemungkinan Terjadi', '3', 'Peristiwa kadang-kadang bisa terjadi; Persentase > 30-50%')
<query>INSERT INTO par_risk_tk VALUES ('1c1ac421ff4bea47a0703ea661527701a44a280f', 'Hampir Tidak Terjadi', '1', 'Peristiwa hanya akan timbul pada kondisi yang luar biasa; Persentase 0-10%')
<query>INSERT INTO par_risk_tk VALUES ('121aca36b8e9ef874c96814e9c409e8c8e1a92d6', 'Jarang Terjadi', '2', 'Peristiwa diharapkan tidak terjadi; Persentase > 10-30%')
<query>DROP TABLE IF EXISTS par_sbu
<query>CREATE TABLE `par_sbu` (
  `sbu_id` varchar(50) NOT NULL,
  `sbu_kode` varchar(10) NOT NULL,
  `sbu_name` varchar(100) NOT NULL,
  `sbu_sort` int(3) NOT NULL,
  `sbu_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=kosong, 1=sesuai tanggal 2=sesuai tanngal - 1, 3=1',
  `sbu_del_st` tinyint(1) NOT NULL COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`sbu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_sbu VALUES ('7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'uh_c', 'Uang Harian', '10', '1', '1')
<query>INSERT INTO par_sbu VALUES ('476ad41e4a4594d3be69db9c4b06a94096887ddd', 'uh_b', 'Uang Harian', '9', '1', '1')
<query>INSERT INTO par_sbu VALUES ('e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'htl_a', 'Hotel', '3', '2', '1')
<query>INSERT INTO par_sbu VALUES ('ebf228d8a650167838ab269a8c55dd64c41528c8', 'uh_a', 'Uang Harian', '8', '1', '1')
<query>INSERT INTO par_sbu VALUES ('58473fc553632a96d6a97544578c2e4061614615', 'htl_e', 'Hotel', '7', '2', '1')
<query>INSERT INTO par_sbu VALUES ('4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'htl_d', 'Hotel', '6', '2', '1')
<query>INSERT INTO par_sbu VALUES ('be36de438ed04d253d192a7abae9ef7cf0c4738b', 'pswt_bis', 'Pesawat Bisnis', '2', '3', '1')
<query>INSERT INTO par_sbu VALUES ('4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', 'pswt_eko', 'Pesawat Ekonomi', '1', '3', '1')
<query>INSERT INTO par_sbu VALUES ('6ce322773d75e0d26197deafc9e6dbde34f83dea', 'htl_b', 'Hotel', '4', '2', '1')
<query>INSERT INTO par_sbu VALUES ('b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'htl_c', 'Hotel', '5', '2', '1')
<query>INSERT INTO par_sbu VALUES ('da5ceab13e436f7766534a536032fca6e1db957c', 'taxi', 'Taxi', '16', '0', '1')
<query>INSERT INTO par_sbu VALUES ('4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', 'trans_bdr', 'Transport Bandara', '17', '0', '1')
<query>INSERT INTO par_sbu VALUES ('3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'rep_a', 'Uang Representasi', '18', '1', '1')
<query>INSERT INTO par_sbu VALUES ('4abd6b5e5c0834565115dba565d1a0b8b96da553', 'uh_d', 'Uang Harian', '11', '1', '1')
<query>INSERT INTO par_sbu VALUES ('0a728653630619fce4b65553fb6ec31afce577a1', 'uh_e', 'Uang Harian', '12', '1', '1')
<query>INSERT INTO par_sbu VALUES ('22900bd4275c4132998f08ddb2a085964ddaed38', 'uh_diklat', 'Uang Harian', '13', '1', '1')
<query>INSERT INTO par_sbu VALUES ('d763948fb1b2529c209a3404bf0f5ca05a67cf0c', 'uh_8jam', 'Uang Harian', '14', '1', '1')
<query>INSERT INTO par_sbu VALUES ('35800a846ff500f3bbafe348fe19eb20726bfc92', 'us_was', 'us_was', '15', '1', '1')
<query>INSERT INTO par_sbu VALUES ('fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'rep_b', 'Uang Representasi', '19', '1', '1')
<query>INSERT INTO par_sbu VALUES ('1b1596d427c76aada53c972f6e87c9e3d573828e', 'rep_c', 'Uang Representasi', '20', '1', '1')
<query>DROP TABLE IF EXISTS par_sbu_rinci
<query>CREATE TABLE `par_sbu_rinci` (
  `sbu_rinci_id` varchar(50) NOT NULL,
  `sbu_rinci_prov_id` varchar(50) NOT NULL,
  `sbu_rinci_sbu_id` varchar(50) NOT NULL,
  `sbu_rinci_gol` varchar(2) NOT NULL,
  `sbu_rinci_amount` int(11) NOT NULL,
  `sbu_rinci_del_st` tinyint(1) NOT NULL COMMENT '0=hapus, 1=aktif',
  PRIMARY KEY (`sbu_rinci_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_sbu_rinci VALUES ('8a98b3a0013268cd6a36f531be2ca932ad353343', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '3412000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('49f393bcbfa9e63cfa2f2353e5c48eeefcae2d37', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2139000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('589b756cc1900d1439a7968c863ba9ebc6777866', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5b7f92aa48d09ace04b7974080e5ebb98d214ae8', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aaa7eb8b620ed3abeec2b614cb0946b5324f0568', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('765eb2007d049f6cec58dd4ad568cc9266f78f65', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('708b28c847712d5d47fe8956db358d52afb3d98c', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b747494680d75777da8bee88db94f1a0bdcfc036', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '160000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('99e51a7a365ac802e1f6087a7219aec3553e5cb1', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a3b27209e408fed5919eddddd1256865120fce44', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '120000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('443a4039bd71e1cce62aa48d651bc41e6b10e3ae', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3335000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1ae4274676e0051fa36b3b1e7abf13d2773f7016', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1350000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('86597561e10fd24e0147e63558239f34e96a2e51', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '850000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('43b77fb04d08d5df827769e70bfa5dbce4d66ed1', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c81f0bacdba56c6e2349711f040a33968e33f880', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '58473fc553632a96d6a97544578c2e4061614615', 'E', '300000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8037bca0303ec3a9be173c03da1759ba73a4be44', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('80de0c963f721077b9f85621c90f4f413a5b916e', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '90000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c6eb4940f207094dd420121cad60639c2c09aa18', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2cf2917ffdf56aa125e2e44e46035c2db841d09d', '3a9fd0542122e96ed7de690697ce088ac6311f1b', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4c579e8929a9b0314e8442ccc3a3d86de33e7fb7', '3a9fd0542122e96ed7de690697ce088ac6311f1b', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6e16b2a42c8d26d52bc29d632173090834dea63a', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3c5312baed0344b3b654c906a848d0f86b0fb935', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3810000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('95b2fb7db38facac1454720b486fbe4c51c41f6a', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('edb5432b8e9af5bd92149a77520e07d1883f3a90', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0237377a8910a47783bde8e9948d8b42d4c5c5b3', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2a00a4b38402b2ed733fb27f40d0405f9d722d86', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cb34ac51b4d13d7a4c51b0e38218c93948079ea9', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('896e96557f129204f0362242111eaa77c622a91e', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('02993fd64759f5272c4893f68c19a09f2a41e58c', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('74063b0ed01035b6b4c0812c38a1f98a286c6ca9', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8b500bb80461e4fcae808008466a41d9160abb4d', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '500000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dd2d19d9e7a9dedc53766b21e776186d70ac7dd1', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '500000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d785fb74489ac215faf3662f552377caab790bbc', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '800000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e122d506f62be36782c419aa2066440c5c738e8f', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '640000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d852f58bd368b82c76dc2f74351dee616d5ea78a', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '58473fc553632a96d6a97544578c2e4061614615', 'E', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('60c4e349f526ecf407c320ded28096537572e8c1', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0493fdaaad98442ecaae3db1396c207674e8f63b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '306000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ec655756278d53a09e7f612c97eb261b4a6d21bf', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f1f344e7468cfcf4c88cdc3635070788dffbe063', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5d2faa4d9173584713e4e2be18f7c67ae342e78b', 'a1a02d8be166d22d766bc596c4a08979dc906c7a', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0ef0ca6577bcda8856e93c8e3c7fada4c4e0dd84', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', 'A', '5305000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('57b519572008825604af1c814115b82a6240cecb', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('97f2c65734cb9281b4d38345e52c0a6647857f81', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('59983a59087852107d0b0cfcc45b1803c8151f22', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '58473fc553632a96d6a97544578c2e4061614615', 'E', '660000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c5cc6f7cefb7b9f32c72656206bd07cbc3cc522f', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '910000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c4e15ad093112408c38a2dc653d98dc99a5f0c39', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '990000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4b62fc29826f98087d2643a1b1ae9162d89c5bfc', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1810000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0f8dff04cbc67b99bfd7db61e42231c8ff367afa', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4890000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('86cd0c4c0d4c1d33161c4edb25e30732d4f6bd70', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '140000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3b9061f2c81993fdc4e09c9571ff0cb1565b2d58', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2c9a90fe3c98d7d4b56056e042a8d33ac2a5fa6b', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '190000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e750e99226bef82b67a86154691f665138a92ab5', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('77684230aba91e6bc476918ac006f41cb27049f1', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fa6c23e0e5f20c657fe8a1155266475181b4cc53', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9abd0542549aef4d21d6df1e046c797e62afb222', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5d478c4a7112cbc64a62a97fc21f5f9dea1150b9', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fd0b33dd2c3e5c26368e52ad1fa5a9379924cd57', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', 'A', '3262000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('21e5cf48ad471a8d76c613ad44ef29cff33e1589', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e2144aadf5e291549671dcdaeaddb4bc1cddd0a0', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('13025678bcf6696eff463c29788033f6027a3f27', 'f183f90fb5f85df77a6150d74fef99c75b0608a1', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('08a6bbe3aeccc5bb46efd2f025104769790b484d', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '4107000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('977bc83aca9830fd16c350eeff15ad7bbb9765cf', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2268000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('28143b0a2cc2b4a39a28bb6e72aadb069489f7c8', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fa40a2a2179383e02ea940e4a3cdb113c1d82e23', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('341bc74acf0a436db9f460639062b8ccaef1c6fd', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6d8ad7b47d581ac0d7a74bc5b75cfaeb9011bb96', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('481b07e525acd2254de2bdd78a84174aab7be2a4', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f533853943de6a34b0e44414a3b242b153eac750', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e85753d8ae544fa4814667b686f06cfe40f480e1', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8152331bbbe82d6adcb494b87f045c6aee264d6d', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d6d4fffb2be274eb0679bf5c9a1bc3fc00d86796', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4700000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f02fde3b3af4410a39a2e768f86dd32e9c623aef', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1350000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3db52b9c7b708add280e8d01ed22c79c7f08c76c', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '810000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('91c554200363cc2a1c9e75a6e7704f1fd0555b3f', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '630000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ad7b2a3aa9411f3554b1fff784d40399e6ba0a64', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '58473fc553632a96d6a97544578c2e4061614615', 'E', '460000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f13de06db341b4b512f3fa96879cf699addfba8d', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6092e75883de7da915339019be8df114711e4d30', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '94000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('db07e333751a533c66cfaa2b478c1183074f61a1', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('958cfebba698813837bf7d1f9c6e655b2db8e04b', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5d59366e2dcabe5ebcf4255af5858736cff56470', 'a3f1a121069a6b60f84cbf2e8924b3cba3efddce', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('229cebe87970b613096fa94f06b8c81f5abf9a33', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('37669ad8df612bd9cfa32494f01d9ba0046ac949', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fcaf721a4769a3971fbf08633b1d0cc009881480', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('df50efe2a12660c02eb3aa0e9662811f1ef047e8', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('651519f97eaffc8dbb9ee24ccb4436dc057369a5', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6922766241b57415fb0ad8e3a3b22f71c6e88b4e', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('75fde9d7a6aee039d25864025f72766b8441e0e2', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b30207acebe9dc8d5e42bb96aef8c507096c8a3b', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '210000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0b17b23221e2e8ef82fc9afa35bbe762cc03fdc2', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '100000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2dd4edc459bb1c111b736ed7e8d272b3e87cfe14', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '160000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0ffa2ba07d9cfff3976d2d1af99da6a1d3d2c561', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '8720000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f2fdc655ba0a63de89d32622fc921628f2ee374d', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1490000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5cf1b733b2b795f45085b33d3064585f60af1b39', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '870000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9f0e603d1d7e55adc71dbf8c9fd8b15ff4a7b596', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '610000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ff3b23a922675edfeacf03f9a8eaa301bceec1f6', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '58473fc553632a96d6a97544578c2e4061614615', 'E', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('400083788084922449ad75d4d049a3447b5244b1', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e0dbfbcca5ff67bd791b4af9519e24f3f8d78922', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6c7526950db1a8fa2671f01193bfbae4f5575376', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0fdf618b8f83f241ef0dd8666c021b61d29be021', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('df7078187b867ef75584f58b4c7ced8d4f6d177a', '99c28a4fe33ae2eb8efb09a2eeb1d91fdd3f654d', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('43e939f4dc68c28393fb1c457349cc3ab31bafb6', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7231000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('da9f1861eb23d013a8efbae5f664ab713a3c965b', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '4824000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('db5896967aa737cfc54b892ef42da24f0d816191', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d1ee8fefa4abf63988455d48a287116fe6f8c2a8', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('38510b5c9401b3a0b824dd5b27a25d8babf2529e', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9c0820ef39a00f03e45e75243931fa41ecf93921', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3b4e0333dc3d67f38a6d09c9254e04fb8afb10ba', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('612eac20f70cabb02bb0881f7f40acef68b79938', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dd811fb1dbf7a568f3ce2b06cdab1b1408e81258', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fcc4e426e53d1789411a9048126d6db5ee47cfbd', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1068ba32c9d757f87bb14c0cc8ab9eb880422d05', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '1320000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('22e8f48d567741f39309055bc3685f667466c2e7', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6e4409edc387137d47c348114b787cda41019f66', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '550000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bd6577e1896e8f38fde6c75b42b48b6635e98029', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3bb7fb5928ada1eb2e0884ad709f7c6ef45ff7ed', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '58473fc553632a96d6a97544578c2e4061614615', 'E', '260000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('351f4e4f393773fde0cac8e6320e462263bc8e63', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('621ef721dab8fa3d024b288d7eec19339ec5e234', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('673a6391ab43945f8313f11207cd63958bc66210', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aa3b27b71436f996cce4f7b87c61615b56266ebd', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ba262419a83a705e6e873ab31ed4da3d46aea1ac', '59862ce41d1636f2cfee5ee8756129b2c9af7a70', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5ffecf9ce8442d41b353d5c5a96ac334cf39f627', '0b486467ca176631589de72c19779fe674f13388', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('127ebaa035d6ce7ec94cd71ac5afedfbb307ad5e', '0b486467ca176631589de72c19779fe674f13388', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7b0e5ed446bf86156d04f238cf39172f74fadfd3', '0b486467ca176631589de72c19779fe674f13388', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1f9ed64fa827b5b394ce5a26d9f51bafdb0daf36', '0b486467ca176631589de72c19779fe674f13388', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('22ad18817883fa38738aa575befc8e15a367573b', '0b486467ca176631589de72c19779fe674f13388', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('19eb12f62671d3ac2a9a903d30a2ac4d95b34980', '0b486467ca176631589de72c19779fe674f13388', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d09391a409940f8337f77c12a3c2bba4d1e3754d', '0b486467ca176631589de72c19779fe674f13388', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('72b6005770a88fc69753950f5cf5f9a7db449a8d', '0b486467ca176631589de72c19779fe674f13388', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8039e7d41745d4cc123942d916046fa26f19ac54', '0b486467ca176631589de72c19779fe674f13388', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('772c7dc9290b63071e5765c2d3d3835c6c3b0c70', '0b486467ca176631589de72c19779fe674f13388', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('41585bc498c9816df27f4c62d803c4a42e6feb4f', '0b486467ca176631589de72c19779fe674f13388', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3700000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3739f97ada94b074367dd115bcc5af7c2e097990', '0b486467ca176631589de72c19779fe674f13388', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1760000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('839e9ffa2abc417a99e7ae3179dd2af402f62085', '0b486467ca176631589de72c19779fe674f13388', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '800000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f62b26ada68f9db5db827f8326c85d8fc8385fe1', '0b486467ca176631589de72c19779fe674f13388', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '560000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c4ede6d35de55b5af1c4c2326e47569cd6057e98', '0b486467ca176631589de72c19779fe674f13388', '58473fc553632a96d6a97544578c2e4061614615', 'E', '460000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('61d9278ee904cceb5fc75a8a5131ad9443616288', '0b486467ca176631589de72c19779fe674f13388', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2989c6252711a7fb428d6d0c7ecea50538ba44cb', '0b486467ca176631589de72c19779fe674f13388', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '140000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dd265375d4fb9735d95a86db0e0ab9caecc51145', '0b486467ca176631589de72c19779fe674f13388', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5198f7ab545c6fd68c63c0c92581112650572627', '0b486467ca176631589de72c19779fe674f13388', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('23f7a4566dd7df8636335b2f725d626043fb9078', '0b486467ca176631589de72c19779fe674f13388', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1119f07af920f52ff790109c80a88dc3982c8c82', '675eed8a709d4098541fb8f73805004c84725d49', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '4065000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b3f0c2afb0108699cf11c4e50d61405224b60d2e', '675eed8a709d4098541fb8f73805004c84725d49', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2460000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1677ac6b0ed56b6dfd5f60ca1f27ea59d975999d', '675eed8a709d4098541fb8f73805004c84725d49', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b9b7eab76379e5c9575a4034fdf6b0fbc3a9e2c7', '675eed8a709d4098541fb8f73805004c84725d49', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0b8231ec08f7cc0684c85861b74bcdda03b7d208', '675eed8a709d4098541fb8f73805004c84725d49', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fc07d936a7336446cbe5408963c653f23c9d79df', '675eed8a709d4098541fb8f73805004c84725d49', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('856b33868f39e0831f2bb7aa9472f9b10f29a180', '675eed8a709d4098541fb8f73805004c84725d49', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0582a8e97292125d6f0a2e77ab3542fa1f91f3ce', '675eed8a709d4098541fb8f73805004c84725d49', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('16c0044ec7f0e4121ed86816ec69ce68e65274c0', '675eed8a709d4098541fb8f73805004c84725d49', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e1b1b9632d59b250a097c1d10535f5e678b9f64b', '675eed8a709d4098541fb8f73805004c84725d49', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8f17cba1d3cee20856d4c9da6eed02f5a33461f1', '675eed8a709d4098541fb8f73805004c84725d49', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b014886001f8ec5ddaf0ab91ef54c9ecd08a9a4d', '675eed8a709d4098541fb8f73805004c84725d49', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9a0cb31be84c5f04a4b5a3dad64f7859738f191a', '675eed8a709d4098541fb8f73805004c84725d49', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '740000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('edd7027cfdf939c9f57ff017caaf3a90a6aa86d2', '675eed8a709d4098541fb8f73805004c84725d49', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c842b3065e3f137f5672c100431abc02d2612291', '675eed8a709d4098541fb8f73805004c84725d49', '58473fc553632a96d6a97544578c2e4061614615', 'E', '290000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fcc46bac74ba888c3d03d50fd03a0e60d79fa882', '675eed8a709d4098541fb8f73805004c84725d49', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fc4a5790a338fa3f1b2f7f11669f464d46dbca31', '675eed8a709d4098541fb8f73805004c84725d49', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '120000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8910e31a69a29e365a3626a6e9bb784d115636f4', '675eed8a709d4098541fb8f73805004c84725d49', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('616f6b3ae5285d33a09c6d6d43d952f8196d9c95', '675eed8a709d4098541fb8f73805004c84725d49', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('80fcc4bae4f738ee6d6e781dc0f8f55a7cdfdf8f', '675eed8a709d4098541fb8f73805004c84725d49', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '130002', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c5060e8f8376d26536e35cee7df70432aa880bd9', '675eed8a709d4098541fb8f73805004c84725d49', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d46560d3696fd9eab122a210d1c8abc6cc05637b', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '3861000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a605040dd3ff00cdeaae7c7cbed53d4b9caf0ee5', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2182000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e9e9d158d7bf6e1a07a7bb266d8014f3e9914ef4', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6cf13b404fd2cea03eb14cc08fd332014e8b1e8f', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cc8e1a7e8513c2565c69ab1930b39f766a279848', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('35d985e63e7be9a5d092822ca8cf25649eb75d91', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e0f7442ea931de49d413f770ff5409e1420e26a7', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('027b402502564bd3527b9af9fe9b6aebae54f9e7', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0bc9db4460d9bcfab0a01ff9ea0016af6e7cf1b0', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('76e3ecc28c5639f75059b02a0f8d2ff33a1b70c7', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f1b0096e22452e9f94736685fac6547f8ca60906', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('93c6587a5d34f88fc86084d323eca3d51dc09140', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aeed14d49cff3dde70b8893cd543f67c40ab037e', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '850000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7b45e671373c638ba0cc3286298a9353f989205e', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '450000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('070b7dcfb039f244cf2ed129188729ff4e8162ea', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '58473fc553632a96d6a97544578c2e4061614615', 'E', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fcbb2509668bf455c01dc2ba119310fd2a0cd4bd', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4b6ba3b64a5aabb6438ac62b49f4873080f28620', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '75000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1ab007f03543147545fa9f3a43c0b6af389aedb6', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4128807f534ad53987f50b3b00ba900b809fecbe', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d40057829d3f5471c88b86b025071710bf01007d', '8f446869f734b6f2ff6d65761a18ba945a10cbd6', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('07fbd8213340588998252ff4a321a60c7965becc', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '5466000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b803528b467afa3af801955fc0a80df8a3687744', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2674000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c85c9b13b1a489d327a211628f8f06c56bc442f3', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b25cbe908829ea68851f96948935484cb7cee7a0', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ea05150a6108b419d4e0fc22fb90393b45a4faab', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('452b8b56b618bb1deec49f36a756ac1031a4cfa7', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('031d6b187bab1e8795de76bdffa23f302916550f', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('70dc02d21321f77e074380617c5702e4b2823898', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '160000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9ca7f8dd5ddb714486e1b5fb313857394a16d8d7', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('28a07e3081e00f4623fa2ba89136a5f635872eef', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '120000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fd5d6b3b339d9d62bbbb2749a1fa7a38383af111', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b053892a6a3d49931c275d30a14f5555f73e1887', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c1b6c32212d94820e635859dcf3853283bd01260', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '850000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ba6ebe5da486881d975b9faf2879fdad14491e23', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '450000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5843634b01270a18b73d0c1a15f69160cddcd8d8', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '58473fc553632a96d6a97544578c2e4061614615', 'E', '330000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a556aff2183e4d97cc6f41917b0619793df079d8', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6d9dc437da573ad20c0004df144e8ece5ef7b813', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '148000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a6ac89a3a5f502aaa4c6550e9e63b3020e88bfa6', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('54284925c8947d71e831a0c7998ba36a710a9f0c', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cdb182587dd0e96775bf9c37e4c4ba19608c5ce0', 'aaeb8fcb443f06605035fbc68fa7c1598f53c6aa', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2cc1298f7c816fae307359c55470b2370620ed6b', '499db371dac2489052cf151f8e861ac9d9172df7', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '4353000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('02d81a5856f1bc2820f86107610c21a3627a5899', '499db371dac2489052cf151f8e861ac9d9172df7', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2781000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d8b426c2bb36fa4e2aed9eb58eab787692625bfe', '499db371dac2489052cf151f8e861ac9d9172df7', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('efd0a98842f2364f35c688d85ec705ca77cc4fa4', '499db371dac2489052cf151f8e861ac9d9172df7', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6b30492ee2cb9c453a41922373012aa49d22505a', '499db371dac2489052cf151f8e861ac9d9172df7', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5b81258bb32006b204726c43e8d9172f9ae98b1d', '499db371dac2489052cf151f8e861ac9d9172df7', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b985cd637e89538086f9c9258d896779975766bd', '499db371dac2489052cf151f8e861ac9d9172df7', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fd215ffc11eb643463e7ad1acb6d3960f60ef0e5', '499db371dac2489052cf151f8e861ac9d9172df7', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a1df7c5ae685b52e32c1b8b2fd787f3d12212ac2', '499db371dac2489052cf151f8e861ac9d9172df7', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c4a0349074b9b72fda09b82bade3d41a17705616', '499db371dac2489052cf151f8e861ac9d9172df7', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bdda1c0c573528f641ed12d96d6bf8b538f55845', '499db371dac2489052cf151f8e861ac9d9172df7', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '2400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('33105987b39fb0b0bef8f82b6d84e27576de9f1f', '499db371dac2489052cf151f8e861ac9d9172df7', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1230000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1502266267813a3b08b452add18445ca7d31a777', '499db371dac2489052cf151f8e861ac9d9172df7', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '900000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('47115c39f7e39e6184349b80951092535fa0a9cd', '499db371dac2489052cf151f8e861ac9d9172df7', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f52c075ee31f2ebb528393fad0f0e4660fcbc917', '499db371dac2489052cf151f8e861ac9d9172df7', '58473fc553632a96d6a97544578c2e4061614615', 'E', '350000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1d2c93a69ae3cb2f0fcb03b9f5e79a565152c731', '499db371dac2489052cf151f8e861ac9d9172df7', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dd115fd0bba41a7fa2476a2b1c65260d82a2322a', '499db371dac2489052cf151f8e861ac9d9172df7', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '107000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c018ab0653f5859cf6abe4168b36a7728d839ddb', '499db371dac2489052cf151f8e861ac9d9172df7', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a5b3927bb575ec5d55d964ba757fde13b178f567', '499db371dac2489052cf151f8e861ac9d9172df7', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('424a71a95f4a16d9a94f68dadc78003bc3ff6fd0', '499db371dac2489052cf151f8e861ac9d9172df7', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c538ba3db3b3b4af136424dede4c4d3fe230f46c', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '5252000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('47a21c8e3d2dbe7acd6e3d182baf05a03493891d', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2995000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d34c968ca1f0491cb8710faaa38ce94c3a2f47a0', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c3a65e54112014f4cf78948dc76fca67477bc8dc', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('136a52e046cbbf01a098f67c09e43c3fc6c9b55d', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('674b333f85665e40c84005b18d2b0e4848dcc31f', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('66fc4e8ac2b27df234fb5b5fb62b9f2e962e1b52', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3cd8d5f440885185fa45366d5dab7694761c3046', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bd04f910f827f9f01a0a8f66c7c09934533d05fd', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6006354f48cd4fdad68fedb1559249b5527fc370', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4a12ede885129be7a8600201d09dd3f3a7ef5139', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6e7f59b71638a6bbde24b058514c057b2417fe16', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1680000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c9b75a2b011ea35e44cfb071c1d43a06689da02e', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '820000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('80651eb738951a6c61e240f031aa2de3cc79f159', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '540000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a29330319d10fe488a6f90dc439e39e7a6b96dc2', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '58473fc553632a96d6a97544578c2e4061614615', 'E', '390000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1536ead630af2e53ef3d0e639c67434fecb41830', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cd3beeb091c2be9aa240010968c7d929ceb94891', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '100000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a26edda1bf18cb84382ba3b6481ca7066c0fbfcb', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3f1ea18b8771f1ab54d261bca75e65b8d878b853', '260e10323ded2fca85ec8d93337b236b0f29e2f0', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b7c50ff9fbededf9ae9d153a8d92f0c02cad8971', '260e10323ded2fca85ec8d93337b236b0f29e2f0', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ccbb702a0017785ab9aee7564d76f247486c71f5', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1ef9675e3861abcbd7c24cb598d9dafdcd37cf4c', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1750000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e5a9378402ffd0d328a7ebae8bdf011eb2790b91', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('55f00b8bf58c684bfe73c0b8b67737a4f6ea1338', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4130de186b077e0a8bf0bdeaaa84db395803dace', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '75000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('61edabf3105af3216716dfe25079aee7accc2a7f', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('59dca3d38a9a5b25ef92807bed27282be93a3adc', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3797000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9652e139e3a9f640d16ab883eec2caa8707c3298', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6c76d3d0b9769b82d111c0575ab84c075d1be25a', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '620000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('78ae471c90a97e4d7a45c5d3e54d4285b8d474f1', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a74e3b55317fc2c498b188d5d707dc2e14de058d', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f86215ffd078427dc1d881fce34d22b264f4bfcb', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8d8fa186f309b921f63787cdb7be562413faf53d', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '58473fc553632a96d6a97544578c2e4061614615', 'E', '350000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e179391321f9b80eee54ea034d3ed0b7fb83b541', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ffc4729d2e1a5bbbda7c2df5953734be6e6a1df4', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c06249331aaf6e5d47f4544a1587718d16fb1eb8', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a7e17e4e345bd134e9e3f85a0904703e0f04cd45', '40bf7c41fa82117b980797d4ef41105f05bfbc0e', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2225afc73767a028926af80a585f80a2a4cf932a', '175f37cdb244988462e7946a62f190f8bcd3c048', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '4984000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('daa9a21992b77bdfeecf1ea4e6b1b66da257c4f2', '175f37cdb244988462e7946a62f190f8bcd3c048', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2984000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8b6217b20583b7de765392cc1e8794ccdc351ae1', '175f37cdb244988462e7946a62f190f8bcd3c048', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('14b492b49814bbe83e6f9b909efa49ea18ba4730', '175f37cdb244988462e7946a62f190f8bcd3c048', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cb74a6fe81f456655151251763f46c63f3353a6e', '175f37cdb244988462e7946a62f190f8bcd3c048', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bc406d17cd2012c21f4ffb28c0cd0efbbf7af50f', '175f37cdb244988462e7946a62f190f8bcd3c048', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('51517d783033aed13d0726fd08ed9b3e6c7fb9ef', '175f37cdb244988462e7946a62f190f8bcd3c048', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1eed764e48860e5d497c53d35d55995387331cb3', '175f37cdb244988462e7946a62f190f8bcd3c048', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '140000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3e1d4f497adeed0d3e410b9ae9ba30d8acb5509a', '175f37cdb244988462e7946a62f190f8bcd3c048', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('17f31ec9052581c46f3918c9ef54034167231b86', '175f37cdb244988462e7946a62f190f8bcd3c048', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4d30be92bca79e0d1ecc818b9a6b580c8a157cde', '175f37cdb244988462e7946a62f190f8bcd3c048', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('53f5c1f866fb61c5bfe676b8e97dacd810cb65ef', '175f37cdb244988462e7946a62f190f8bcd3c048', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1560000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4e446e4b677bc394649be749dd1063577025e6eb', '175f37cdb244988462e7946a62f190f8bcd3c048', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '750000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('39bca7dd3e26d5a5981de67f5949e678204ee52d', '175f37cdb244988462e7946a62f190f8bcd3c048', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '560000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2011de8a8b4e1d0511d7b6e807bc9f2ebfb26b01', '175f37cdb244988462e7946a62f190f8bcd3c048', '58473fc553632a96d6a97544578c2e4061614615', 'E', '350000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5d37e615ccb566156fa54b84bdfa6ee31cf582ec', '175f37cdb244988462e7946a62f190f8bcd3c048', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('97592ef7b1257fba78adf08e3234d2efb7b4d320', '175f37cdb244988462e7946a62f190f8bcd3c048', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '90000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('05b34316cb64d735b17d16782270ed23889e3158', '175f37cdb244988462e7946a62f190f8bcd3c048', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bd9b1c9875632f2801d1dd3c8916a88cc8c362da', '175f37cdb244988462e7946a62f190f8bcd3c048', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8f682ce3cf46c1a031e73fbaa6708cd5d598f9c8', '175f37cdb244988462e7946a62f190f8bcd3c048', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('37bb51413e78ef420bb7abfde7c32f7bba80f47b', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7412000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('54afd15900982d42f2df3bca372c2245bede7509', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3797000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3746d0f47f0e9f564c89eaaef9cd491a006dee74', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e1caf9300d2723c82fe8409cfb76408e70482f99', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a671a0cdba58d8f950ffd6c0b69606e3a8aa60ea', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9c77e36bfc59cb4397ef6ea6ab383b084ddb0197', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('34f44bf3091ac5b5d337a98373759f414d85d23e', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('82b70be614dd1c16e4a406bb1942d6f78df97336', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1a178d41ec8a1b66100b7c2a20028518ee99d047', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e3a2817bd193f159f25a493c4c60473c6fd2ca17', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('be7cf825c2ded07de5b28913e10648a6459e48b2', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5c04b2d5e43fcefe3aad84f3bc8b7d8da8fedafa', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1750000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0cba9aeba492cf6a59303319da3bb59099815aee', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '950000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bc43a77ad17098348cfa0865cf49f57491be25a5', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '550000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8457d4914755aa3936507ffee665e68ff9dbb202', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '58473fc553632a96d6a97544578c2e4061614615', 'E', '450000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2b22c2c1274a4f355ee6cee1ac9d44e944c590a9', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('900b0807feefd036684e40f310982b537da6f0f3', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '80000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a2e7a37f31e393aa616de797cb3a7781212ccbe3', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b28648386b6f6b26c1ab82b05f3ca90e62f02802', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('83ea1fcd51e9ce11d0be73f6d3a11d30eb8ae33f', 'c7aad831ec287e77fb3ec028ea29602a12d68fa4', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ca91693884067b9289eaf7c91476c74d4e0a350a', '5ecfef4589a11799683bb20435868c23620b01e8', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7412000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('82b54b209e53b4148069cea7b4771c8314edadc4', '5ecfef4589a11799683bb20435868c23620b01e8', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3797000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5c8e4ed65c21f3fc708744e6996362792868076b', '5ecfef4589a11799683bb20435868c23620b01e8', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f4d06bbc181c1dab9407c0afbc04e592bf7be733', '5ecfef4589a11799683bb20435868c23620b01e8', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('143ef1ea96591d28dc31f9756c98d8db3342be85', '5ecfef4589a11799683bb20435868c23620b01e8', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('28b97a84b6fce7f4bcd93eca129854b7cdc4c536', '5ecfef4589a11799683bb20435868c23620b01e8', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4c58540d6b477ed6cac8651787410a142cbef02a', '5ecfef4589a11799683bb20435868c23620b01e8', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cdd768d78cce97909143bbdc91c030eb33085f7b', '5ecfef4589a11799683bb20435868c23620b01e8', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b277a702a1c59c5cc5e860f2b0149c95b3b77ddf', '5ecfef4589a11799683bb20435868c23620b01e8', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('eead5fb67bf05b2cf3167640d1e833bd9857cd33', '5ecfef4589a11799683bb20435868c23620b01e8', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('41691975ca4f6db9c3596248d431a4038a6dfbd0', '5ecfef4589a11799683bb20435868c23620b01e8', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4b7120e4fda443fead183cbe1a5e4d1cde78c569', '5ecfef4589a11799683bb20435868c23620b01e8', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1750000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4952b8233b87d7e46937e215657c9f740d094865', '5ecfef4589a11799683bb20435868c23620b01e8', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '620000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6c459c05e91e1dbff9eb1a96bfe279405a3d447e', '5ecfef4589a11799683bb20435868c23620b01e8', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('290f18226b5c4ebf8b3e4c642d94ac6ee038ce10', '5ecfef4589a11799683bb20435868c23620b01e8', '58473fc553632a96d6a97544578c2e4061614615', 'E', '350000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('14b4c034595198601427bc491b96bb4124cb189e', '5ecfef4589a11799683bb20435868c23620b01e8', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ea6748df0eebf6a8ebb985f40b551360ebbbdb8b', '5ecfef4589a11799683bb20435868c23620b01e8', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('07f030c5bc8b907dd971ebd581e95e49f16e389e', '5ecfef4589a11799683bb20435868c23620b01e8', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5d6a2167d92d23f711a220703c211964e1f719cd', '5ecfef4589a11799683bb20435868c23620b01e8', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6e13bad696e348b58502b17399711765e305d9c4', '5ecfef4589a11799683bb20435868c23620b01e8', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('98fcb2cb87834457c779f0c90288c149377a5b20', 'bc02f4d326c699e711b05bf45925b3652464457a', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '4867000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b903bcdeb4e484b024914531aba7abbb6bbd3480', 'bc02f4d326c699e711b05bf45925b3652464457a', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2888000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('42ca42ddd5c146d618a34f424525a7693a81a4ea', 'bc02f4d326c699e711b05bf45925b3652464457a', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2f0d304c35219cf690524de2a327c2e227bba5f2', 'bc02f4d326c699e711b05bf45925b3652464457a', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('85087f90a47d40bb17bf0b82623be11d798d90b9', 'bc02f4d326c699e711b05bf45925b3652464457a', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2414b7bbafc081a95367349603447b52080310dc', 'bc02f4d326c699e711b05bf45925b3652464457a', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0b764961ce5787e7f79e71337540c87df3e48f8b', 'bc02f4d326c699e711b05bf45925b3652464457a', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4051468cc0138bcd5aba86bae3af3aaa8ba89ce9', 'bc02f4d326c699e711b05bf45925b3652464457a', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('989c312cefeeb18deafb0b1d8d880f4d6a26f2dd', 'bc02f4d326c699e711b05bf45925b3652464457a', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9b3ba5b8300cb0a6025a7516ceec0826d072a564', 'bc02f4d326c699e711b05bf45925b3652464457a', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c3ef1306526e4a4a0efbb115571e6aa3fe6ac111', 'bc02f4d326c699e711b05bf45925b3652464457a', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4275000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0a336cec8074a3732564f9b8d74d0a0fbc65af5d', 'bc02f4d326c699e711b05bf45925b3652464457a', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1300000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ab284ae943ac71a55cbda0b8452920c9f40d4e76', 'bc02f4d326c699e711b05bf45925b3652464457a', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '650000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('101f0bb12939be782c222055936584e260a677d5', 'bc02f4d326c699e711b05bf45925b3652464457a', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '510000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5789fbcdc35a552d36a4634401ca4f840e3f5428', 'bc02f4d326c699e711b05bf45925b3652464457a', '58473fc553632a96d6a97544578c2e4061614615', 'E', '280000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2c0d3498de795f5bf93831ae46a90ed143187ea7', 'bc02f4d326c699e711b05bf45925b3652464457a', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('93383b2cc6acdf0905d1e62b51ff132f6f94200c', 'bc02f4d326c699e711b05bf45925b3652464457a', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '120000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3c7bc892126397bc57963640c1217f77e781a596', 'bc02f4d326c699e711b05bf45925b3652464457a', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4d8bda8339761c6fb1724429c6350912395ca824', 'bc02f4d326c699e711b05bf45925b3652464457a', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a74d429549468eb1a5af85ade8136c5c0a64ffc0', 'bc02f4d326c699e711b05bf45925b3652464457a', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fec26877b1c7602404028dece2a9b92c5600fb4f', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '2407000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e83279759e66d00c2282302facb9a572fbd8c266', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '1583000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5b755901141ab49b72cee5f486ef493679dbd6bf', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c7e5436cbb87ac61114b9e5753ded444b8ef19a1', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dc87bb5d9497a097cdd6143eb6e72d9d073c5c5f', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9306a8f0b46c5fa515c0f1aebabd238a5c94a49f', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2e51ec27550937b7d04f76246298980fc6db4019', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0dc8fbe15631de5a7e517fad0124521a78a1b9d0', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c1f1d869c640fbc9c5d1abe95b96a18ff5f278ca', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d2f2bc09476b9ddd869ac9a07f5835539114eb90', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a667b2da9c846b08e369c0c951cb77c80948b688', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3960000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('244a3ad48d810e3194c0b15a6ca838f22f4d63db', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1300000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1f6fdc64ff1af3e251f7def175d4c10b65823642', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '790000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('987cacc211a2232340d724a48ee6c31267495cac', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('eb4d8a354672ee91c910d0534fe7c931019654ed', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '58473fc553632a96d6a97544578c2e4061614615', 'E', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1dce1623b4f027dea66aed0c5478fff01441ed29', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2996403d3bac1de54314246394a6e5e78d980590', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '145000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('afab06900b1cd2b457f219a08d1613ffb28d3bf5', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('82bcd0a18282fee5204394b799baec5c7cbbffbc', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0fb7b755816fdf4d77d8647d3ebed6f74c8f7fad', '8f5fa3636c7a0a7ae049b0c1d9fcc4439f0e0b54', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4885c2a0d1ddcc808bf5028a683cfa8e6205a122', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '13285000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d35859844900a7d6b6813a033c79272434e3a04a', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '7081000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('02ba9c34daa606faffe3a082699b04b63b6e30d8', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('933b0ab91597c916a0d47722d4b27a4d7bc72af9', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7e82c9194868a31f7563b6cd83ef74df02d15d90', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('12a81381d35e7055598e90a835d5485bd099ae00', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d8ff3ee3797ef1a5c48b6a10391e87ab4f5ebf57', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d35dacfd9a2221420c642afbfd74558231086d8d', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6c10ce3dbdd640cb6dde0af42aea45b839639fad', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('435bd066daf3d31fc1900b356394c83074e08e16', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ca6bf14a0a1440dacf4802fb6e8d9ab1896a543f', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('21286dc00540c8efecf2247ff78ec44592943a5a', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1030000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d1ebc6489891eef65aa7a47baaf9c5a2472599fb', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '740000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f92cc9e533e75a13ddad60ebcb80319e54028b2a', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d012eee6a8cb323c1391ae02daf174423bb5eb29', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '58473fc553632a96d6a97544578c2e4061614615', 'E', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dcbfd3625edf82351b1bee78afc15fbf249e731a', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ead5f2a3d05ad8dc2ba7abf4d4d1b8ec20ddf27a', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '210000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b8d0fc41aa1e689bb2e63340a97426f48314ecb2', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4567708a73756f8858258b7eb326dda0b5941070', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d05389f0f9c349c2563822f984d5f8b98895026c', 'ba7fcdeb2444bf4850947ba34bb8b8520ac0868a', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('91e2e41aa64cba72d5bbeef79625bb16dec7f127', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '10001000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('49bc847792aa008a15a56a0c65a9fc1d06b72638', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '6664000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a37d6580e6df57357019deacb34f754b705cfcd1', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3bbe2c5623bd59584376eed9c952229f717e3014', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5bcb24321b59b4fcc85cf1355062a7dd447e2254', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1c34faecfd285123bfb7ff1129caede661d625d8', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bc7b002b5f29b44f47df73b13efb11d90dbfcabd', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f48f6f67649696b27e0aff4b687b7f572f953c42', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('028fedb4745b569067259a953cf2cec58a3e7937', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4dd3be8c94b9fb532fec95e7cc1cdbcee6c41675', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c60bf38c41f658713a250677d8e80338ee919029', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c3f9cf6d1494ff8b3904db08eaa16f01eda335f2', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1520000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('631dd0fd3ecddd94187cf86a8a8301bcfb5f75b3', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '600000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b3bd48571d092465c1ff8f41e2cde94dcf779602', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('afcaf3d0d65360728e7d9b3c39beb72a427d600e', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '58473fc553632a96d6a97544578c2e4061614615', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3d50c73ce31bd2a4381f97b9c5a6fe82f22b3fe9', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b8789a0295a605d65fefac8ae4f5772782a85050', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '174000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6ae9be5a779f3a3aa5fcdb17308d0c5f2e864b04', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('23e7505d60dc8837ffcc7adf430e24daa7531ed0', 'a3959bc615df01ca44c0374239d089f46c8e06d5', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8274be6ab28ab5478bdf625c5eda6689779aab7a', 'a3959bc615df01ca44c0374239d089f46c8e06d5', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fdb2b94eab744212079da9e2def21ec167b078b4', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7519000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c6f8a6642da12d0698bd07e80551da68ebd0effd', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '4492000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aa33bc05be06bb35c81eacf6ea490c572d4f1a1a', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b160854d7e2adb55a0cc34521cfd302ef595e136', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a67294948bea66f3d7057b26646e1f6d2659cee8', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c649250bdd076295f94844cd6cc2190001299975', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3639941ed533e9a0a78fcc2dc569298e70c9429d', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fa3415e5d5c9e2834707b23b8d283bf53fa7cec6', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '140000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7e347e0ef4b00a7ca2d5b80bd79abf458d4b845b', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c386ec84aa172cfffcb2e73d7653a8b20bfd4d6a', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5e902edb0bd41620b557d90c3505b51271c1b890', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9e15b3665b066cda5ca1959a9da9bb9f3653b9a3', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1300000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dd79346ad025d95373d4494bfcc9e1ee44a78071', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '850000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ca44d5a31ad3a528a60bd29f58b5989a01bba3b7', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '450000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ce26211c3054cf5b5134b98e072ef94cf9c310a1', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '58473fc553632a96d6a97544578c2e4061614615', 'E', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('275efe5afbefc62280db0515ff0a3fa29020ffba', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aed392c71ea0b3195a78353ede994182248c8ada', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '120000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a58c4a9ac8abbcdbf44733ada8d7113bb6dceecd', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6225371b1282db7d4424228349df2a424e6cc6ae', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b2f90419de94ec42d1ff429c8ce2b240b3fda6b2', '4fde941e7d1c5995200493f1bdbc7723cdf1e67a', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('16f72dab372efc6f4a669c4e63387f1d2c5a238c', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '5316000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('68663282b824a1ff4e8c1876c72f24c3ab6a38f7', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3230000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('443b7e900f89bc6ce5e994be06aedb3ad077dd00', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '440000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4b6f19c740afbc038efa2ae7ea166357d2833714', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '440000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4618103ece327a5933738adfd407205f1928b6d6', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '440000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('14d311a5698b3d5b2049eea2ec3f033ff9d105f4', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '440000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a12a8f22c540f5c78057e1ddfe5a0fca9376b5d4', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '440000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a999e63dfbdb068750323e21d63a6d2fb68cf7ee', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '180000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6b51a5aa61dff065c0a6669588a76242b02d8be6', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4ae54016998f38fea560c314675adb0fa86c4291', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d76789a82bd75ef38f240b16988a7252770306c5', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3500000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d630bc1a8390465e77037fd82484f3e76c925d5a', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1760000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f52d14dbe1d637ac03faef8e36310d1a8085ebb7', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '800000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('eb7dd807769eda42e3fe6415907b13d23b10aed1', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7187d566da9038d3949ab98d0b0f4cf42f738ae3', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '58473fc553632a96d6a97544578c2e4061614615', 'E', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0031b222058b44f44de6c1e8d05b9f3df81914d8', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5870df5c7a9db9d7cc94a9fde0590126e79af9dd', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '213000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8fd6f8dc9436614c8afd6bc822dabe011677fd7f', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('51a0c45b78ed7ce6dc0cdb60060ad7731d3102e1', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('29ae2ca4fad3fe32badd211e653c852f5e187890', '93da267ef9bc93a8c840ecc4cc0f253901d0ee8f', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ee8abe112a1130616fa1ca526c72c5b312340d9e', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '9413000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6f8431de65f9880340193baf8bf335c126ae9cd8', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '5081000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6f50ca1e3ab38129eed312b497f1e4fadc322100', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f57372cd3ec8b6acaf093e47f7fd90c09d2a5420', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5645c83eb69b2d86f03d98efe6e860d3a680c288', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('de761676ac9792b155cf82e1a81fc7956e6f2f2e', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5eaafbbe582b600246545f25f498f86ad84629e9', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a6d728a6d216f6c5b71bcc66a7d34fae1aa9b49d', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f1e333c59d911ff1f63200c270da61f1f42afbe2', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fe5dacf9ca681de3490e16fe7ffa02f37f040188', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('237623ac6ecbb5bc30fb77a6cef7434e463c4e65', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3000000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('41654a056f773dc136fdce104ec168f342d03f75', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1050000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('27bdb2023b06f30336b0e4d71469121323dab40c', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '750000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('67be6cd1cf3466c7dca78753b1f522885c8a4d82', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '550000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5b7efb32cfb1e5c103b63aa35c21a9a5737f00b4', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '58473fc553632a96d6a97544578c2e4061614615', 'E', '300000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('18d8cb01505c8ae8c1e52fbb4510faa560028cde', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a8bc521a4cf2585aa013fb7c065e14537bff0028', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '80000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3a13dacff00fa10cfd41bde680ac226e178a022e', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('41d2c99d1e4fd3eaeebde89fc6a9ea81d7dec237', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e74909a68984d3ea2e239ec059c8ed42b06b795b', '3a1d27e07bf54c0c65f366de3be44e869a751bbd', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cf32ab2fa1d0a37d1e745c86d7f252973cdf21c6', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '16226000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('722f30e0fbab0596af1b73c6370907f5ceee241b', 'cac6bb408da778b1dd986cf581e1405801395cdc', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '10824000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('56d8abf4c5378ca1319bab79843dc98cad5a8d1a', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7e8f78fa0547184f4da1fb8795b2d73a515898ce', 'cac6bb408da778b1dd986cf581e1405801395cdc', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aa3a91d5b0908ac7cf8a53c5f4ce52542c17175d', 'cac6bb408da778b1dd986cf581e1405801395cdc', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7d4786aaa7d894eb975fbfc4c546bc266988c784', 'cac6bb408da778b1dd986cf581e1405801395cdc', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7954ef15ca92d08cbc63f5c87b03b909dddfe485', 'cac6bb408da778b1dd986cf581e1405801395cdc', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '480000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('23eb982bd8dd514bd70680900e902e0a591c209a', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '190000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a39c5ce7269bed4e2e6da46531d7cfa13b34b5bf', 'cac6bb408da778b1dd986cf581e1405801395cdc', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('228dde71f571328e307c54e6f5f1cb1b97d5f187', 'cac6bb408da778b1dd986cf581e1405801395cdc', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('244a808ffd7f4c3c9066150394686e16f3ec71a6', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '2750000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('fdc53bfbc0b36584b7d786bb85cc506ae5598537', 'cac6bb408da778b1dd986cf581e1405801395cdc', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1490000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2f7ca213068677cbd8fe9b59f3c00df6a58d71be', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '760000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8f24bf02707996a90d48f6b54ae587fd2a1c74c9', 'cac6bb408da778b1dd986cf581e1405801395cdc', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '500000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3f006d2eb650120133865fbb370711274311bc69', 'cac6bb408da778b1dd986cf581e1405801395cdc', '58473fc553632a96d6a97544578c2e4061614615', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('788ab4e4a4efba5cab12351368f8db36074329d5', 'cac6bb408da778b1dd986cf581e1405801395cdc', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('848d5477cc72f25dcc682e8c73040c2f4615dc65', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '145000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d3f90d91540da1412d8e147606afd667d0c2de3f', 'cac6bb408da778b1dd986cf581e1405801395cdc', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d1279aea8a2a348c60917f8b3b64f027ea8bbfbd', 'cac6bb408da778b1dd986cf581e1405801395cdc', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('154a6cfe4818ffe8a32a6272528f445a9f9b7508', 'cac6bb408da778b1dd986cf581e1405801395cdc', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('67cb6ea7de67c663b438092ee91d970b790be305', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '14568000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d7f7b4a7c0223f8d1f5f6dc89c0127dc3dfc57d1', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '8193000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('af4c2e8a3f4a6038723b8d50be95c55b3c96483c', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8e7ed59591e7860e36ec72760391630319c52b0e', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5ec79e334ff68b874144f24062d057f7374e3dca', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cc5855c0f7a4bbf8011babceaf5b64eabe1ea9e4', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ddb268cf61f356f02d545378fcbddcfc40be9cec', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6046e0d0501b9082b7a5a621da70f34cb2f8e385', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '230000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b7a5845d4ba41ca7b2a1c6141e7134c541e862b7', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1419a86a806ffe58936746699d08156115f74aa2', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('eb75efdac6ac116fdcd1ac127645c1d7dd383e15', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '2850000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('906222beceefde1e5015d2ffe70422d13bb8d168', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1670000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c444c220338b6638d44090dd21f4c3c728775ac9', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '760000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c8238dbf3518bd98a017578ce7e5a4d88d4b1477', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '500000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a6321937f5256e6fcc334aa3c0b91f2f75706680', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '58473fc553632a96d6a97544578c2e4061614615', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9b21278a16a17c9c81555b3208d319b99f0197b6', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('894d2d914fb4d7253fb9c7d0560bf2c4103558a6', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '355000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4a84141c3971b758235d40eacc8f2f5984245e21', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b2a3066f651dcf82ffe2e49beb0711b21437dcc2', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('487c56f073f879dc15e25c39b4632ad0dd073c53', 'eb2b2b2ba18dfad8157b206fb4e1f03d6a985017', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5ce770ccf6020a21caf1f86a13a2f22e23dbd0ff', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '5583000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1fba41e97cbf0c8b0129f085fc3a839744e411b9', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3016000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3630f94b100b5dd7f642ef9f6a49f04c6229b2b7', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('77ded29e3abb61f5aa7a309907f4461ff8c86b4d', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('783519ce82d7144811554971727d5941d822a091', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d73878b21e15afb843d99c9f5371987892021709', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a2543c35a65a6c751dae347aa0683d9b4d54bc85', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('30fcc58d2181dd1f21b73768d709c9ec20dbea5b', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3e9d8ac4a588d3cd7d8ad65a0cfebd006f47bfe7', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('894ef53f7ac9ef044a84b84ad5130a25659c19ac', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dc7720c8a07dea16c2ad105d5bccce700ff3a4e2', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3820000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d0b1456fa3db4b3aaa00925c87ccbd140592a9e7', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9b4d32022dca42c39bce13489d63abe9ec79be4c', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '868000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2e960633f0f1e3dcd0cc2bf35310b53f87e194dd', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '450000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d7e6add84c43b0ee6d04c3e40147bffa496dfc7f', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '58473fc553632a96d6a97544578c2e4061614615', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4ca13260efcf5631cd1ae312ffe1549cffa77842', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dcddd1150efe89bfa233dc316979c44f2681439f', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '75000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('43a5179d07937c05fbcdad9d287a90e0536674cf', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('85af05798605abe17ecf936e1ca7aaba51fc5265', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ba10899b14af84b8b9311b719113ffc475fc148c', '1da7130fd358dbdbe719cf99f9af0b44b84a5e5c', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4b5c72c97002ba1f5bea3826b8f996ca521ac421', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7295000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1ef744c59285a9fdaf8228f9ba10bd665a546c94', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '4867000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5da3c353075e9b07ae66a7137f4ad0887b97a259', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('018504a26cb8c6f7f484128a5b3a2e11a81fcea6', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9993779a53ccc9c313f73e111871de812b83f4d7', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7342637d907279841280e99074f8211099fd131e', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('66fc811a5bc415ebf3cc4cf6494a83811a42ba50', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '410000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8d9558e8120cb8e36a7e474bdf25d82fb55afba2', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '160000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f4ef6678f9f8329be30c6226737f15e52165000f', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('aa86d7c1ebd24de82b284ac1de62ff828f4e7019', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '120000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c33846b2ba86440ad4f26688703b95fb7c2430db', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '1260000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5945b1aad7ef23a8b33b54d777927dee0be3cfa5', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1030000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5165919538ad1e02afc702495dd65a19eaaff0cb', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '860000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c31e84ff1581598d714cd9d7e524aa94fcaa5922', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '400000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('312630c5611176fbff455785183d99b6b5cfd98e', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '58473fc553632a96d6a97544578c2e4061614615', 'E', '360000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c6a967f2dbf238eb41fc81b78724b6472ea69311', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('893f139463c49400ff385039a86e65370899cfe2', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '217000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bc818a152a2850a97a1f0b97c16761199b7974a5', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b978ba9a746daf7ced5f6e66ff04462a56a67a95', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a29c8d9f779824ea3da806f4c5b2a806d3ae16c9', '9dce0fd18b08c20bf09f4ea40efc03d9a0053f1a', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2aaa56f68417720046daf3bb4dfd1c363f237f13', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7444000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d1e66cceb7106f22c38ee650695318bae649dd0f', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3829000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0b71ae43dfe5d75f662dd9d061e93af612721de6', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ec836c086fa325da0218bb29a2ab2aa1dac68541', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('11b4b21bd3d276a337743880465d3dd76528c150', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d55ef6071fb838f7ecc2658aef827b4f64c162cd', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a794ca64a62dc9b642d526f39e0857cde3db7b4a', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '430000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a3b4856354fb06af677da26d594238b4052a6644', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '170000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2716da1927c564554eb7e614d539e1f962142a00', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d64926175f6b01317a37ffb82cb3f8cbeeb09291', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8b082f9324ae1459c19ef094669bf1c062d1daaf', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4820000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4d19603e285d5570c0fa38df7e12146c412f7de8', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1550000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5c10ab17fd0b52a6c5fbdfd48a3359b28ab530b7', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '810000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2681766afdb9c49c6b63287bcb7c605427e1a0a9', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '580000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('08c23302b22261eb0136bf43c711e8b8698aff7c', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '58473fc553632a96d6a97544578c2e4061614615', 'E', '390000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cd651975787d5bf9c42a5b072f355ca65ba74e93', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b17c1f0816fe9bb9e443646a72ab815d912dd758', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '145000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('00724ef0ed3b40c9ebc570e3327fcd33932c207d', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9d15cd53333fc631e5c1d36651adf12cbb4b559a', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('babdd915afa01e962052b0d0c984bc83ec07fb1e', '708a274ce52bc33ed7801d71b0d1f4e5307e7f1e', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('70433ff4635af47df666d7df4676ddd2d8b677ca', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '9348000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7e8a5befdb1d952b736be57298a24e1a195de76a', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '5113000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d64ca9cbe453f07951d65126368a3a60c082b5d8', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a41802c2001f3267aa9373f32a9618599fbb3602', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f5b230cd76bc9c69b8f1e6462f9a53e1a44c15d4', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7a39ea41b251d75fb2d793cc56f0a820455875b0', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cb9b5978a7761788fcffc43749d1f20a0820ef85', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('97c859d19f1da3e06f3f0565be85b246590b5d1c', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1d07c248270f1a414032ee858aa6ed7ee5e2c229', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('915af1da4ea9d06a7aa82ee7900895909da866e9', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4c89404fde37fa2911d76dfd7233b30936d212c0', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '2030000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('94e84125bf062eecd8ee292e4f057217a57b3e58', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1300000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b8a26e0b44e0c9256eab5c4558d791952bf83264', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '900000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('dca56bcd2ed32a6bab29ae76ecd62821c9b262bb', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '520000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b8d09427cce2a4739ce6f247a4924b607749cf3b', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '58473fc553632a96d6a97544578c2e4061614615', 'E', '390000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d025ca35a70e390be4391f266bda034ca32eb0b0', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('87451f4bd81d8ebaaade3735f87e0088cb26b34c', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '75000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a0e345464d78bdbb60a519b771a4aaa86a8cdf2d', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2df76fb2633c99da848fd8d7be5bfc056cea11cb', '330bc0efcbfc115f11d36d01b05ede6f42236da0', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('45a87df71f98d59fff78a9971f677d310b21c8dd', '330bc0efcbfc115f11d36d01b05ede6f42236da0', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('63a28cd24102803d282df4ef247b772f6eec141a', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7658000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('11dfbf614b5a68954763c882168f24ffc98de70f', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '4182000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0147c8402870bbf8097403611ec4f659d31bd5eb', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9c1608c1cc7de266327ce55a4fef75d0ef99230b', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7a779d17fce3e5fa99f30f858ecfd2ca3d1d98c4', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('acf9c9a1daef1544f7b8f611463651212fb1f6a2', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8a74a75657d5b8fb22c04b1d1f29568c691435b1', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('850718455dfae2a806b0a9ee0a67784901c0dd1a', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('823d678ca7b8551ae7ca0904b3e4e35160cbe989', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f2848195f29a564788668fe8fd5550dca71c2bd7', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9aadb84e557eafdfd4ecb9fd82d8d6490811e15c', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '1850000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ec367f0971b868b4390767d625047101f7e3b237', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1100000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ac71b3bfc3ed4238d5f11df5cafbaa924f503ead', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '600000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8bc067ab32584bb35fd74bc8f630f5c4005ef0f5', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '450000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0b06b642ee98c043d8694e04a01b0a123b7613d7', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '58473fc553632a96d6a97544578c2e4061614615', 'E', '420000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('732609487c5f6f4b6f9fa45f24826e8ee56aea25', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ce15e3fa4827f62a3d3b2211160acc5b86839b9d', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '131000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('669007d45973059303cbc4ecb6df9e7bf754c059', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('497e307069170f974b0e7c85181185e09e8e72d6', 'df2e581894d521cc3f6ed134ccc4d057550337c9', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('eec75322cbdac170433a215af3d8b790841a530a', 'df2e581894d521cc3f6ed134ccc4d057550337c9', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5a139fd3cb090c7d799be9cab8c3c1c35de5d0e5', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '10824000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('def42f729c22c0f10c743aa186cdfef697744f80', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '5102000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('508c1d3bce21058a6e3b4a11724a72262e787704', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('65bcf0ae7653d93ceec6de37198fb1189ae593f3', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('cfb3a3f9e0975651b5dd91b2077fe1f1b33c52e7', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('36211a299dfd3bfc4ae14c4ac8b90e13dbba6a13', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d64eb68eb45014b24de60ee48cb83f1314555e63', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ebde15c5daa09188d369a8385b9f20560fbb87b1', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7936513b827bf829c83b292ce66736d33fcd1e84', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e9f3673d43ed1638a4898283313830a8134db823', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bd6ecc4ba09d03c7a40f3a074c50889c3b6623df', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '3200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8f3280c73ca629c8ba3bfba3d1af618da279aeaf', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1560000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5481c577280cf0f87cf6c0831ec3fc4c72d198d5', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '690000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('14d901227d50f1a70f2919020739ca7ec01acd7e', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '550000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('47cc40768d7011e085a53c5a6e2883dc8f684c70', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '58473fc553632a96d6a97544578c2e4061614615', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3328b0ed12ff2c7b4dbd25e3603e2aad76988d8c', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('23f67d044eb916e659d09636f9b8f093291a61f6', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('08f1e2b6fffd8320d00c0feec44714e0f49a32c8', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('21f7afebbd8c75da7263f9e9dc36e289984bec0f', '6e34f7aeaf90d916efdf09be2db9184a077d466d', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3743b63d85a8588ba614fd2eaaa0cc63c82f593e', '6e34f7aeaf90d916efdf09be2db9184a077d466d', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9cb7dc771a3e3898a21d5bc589df8313309e51b5', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '5530000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b609675a6aaa3b50ef3b35db8566f7d57600e284', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2952000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0716ae550c36ecfdeb4f8d83f2b3b9d19cf449fd', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d093b8a63dc66560171421e7534015046e502fd3', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('591ccc69a5f00a9a4af497cf0c6f3725fc2920a4', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('0c1a96cb2b956031804caa506a35497ff78e6960', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3884ed12899f8835732c9ea9df81ff7ec0fb54e7', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b374065771ef1b4331e7da942edf722e9f3281b6', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9aff90e59f47833a577d18812eceed4d22199a23', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1e740ef1faa650dc0d27f515b76403f3156fe9ee', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8627d4bfd68fd008092edc8efca4a737cc88e316', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4240000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c629b66e1c43fa1866481c3e44bf22e006615e15', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1160000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('6ddb4c5a040c82eff01eb4da0fc136eda03086fe', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '890000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a2cae1ccb93b6e8702eaa592d413822896cc7a62', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '520000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4d57b3da9a69ed958bffc051fd62164b0aa1fcc2', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '58473fc553632a96d6a97544578c2e4061614615', 'E', '310000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b47cf37f8c8c4bf83711311bcf118eaa9b92ba2b', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ab673c7271a28e703b9b459c60cfe9ab4fbbf024', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '217000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('e55069683b5fd993457b004d1bc0d2f8b1a891f9', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2f314f193dd18cb85bc129a9e78a3e7577e994c4', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1503f1dea2a327a8de1f1cbf39efc640872fc14d', 'fb3f0d79e8b5bd2fcb9d82d7b79b81847b21341f', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('28ed060b761eb24d62e9e65f209d1c6d8789fcd8', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '3861000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('350930a612208eca8f84c07e98aefdffe82a52c0', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '2268000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3b142099c57a4dbe53080928a8fee75382c5a87f', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7cfe0e9fde9ed52f47ff2bd727982305a8c4611f', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4a0a4f166d96130f73e8c8a65564a7f6c41af28a', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7785f596aa5e5cf55ce8eba19cc6d8f56169fc6b', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('218311e3466600b3c71b23639d3b2871c8639d79', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '380000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b96e63e4645f771ffe05536f33d2833fc6fa4725', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('03984f265a3d257ed1a771bebfec8375a356f8ff', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2cfd70a06dee2627a191865e474c98333320eeba', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('b7bbc83757efc187e7aa49bf54a1911f5bc0cf86', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4680000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('a3a2e84bf8230e31ebea142ed3e9dfb95fc23994', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2913b5eba4c813e797163d1961e52718667c4be7', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '630000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('257b22a5e1458247b03a9d55438fdce1055f6e0d', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '560000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d76d54b3fca9c5befeac800baebe1370c8174796', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '58473fc553632a96d6a97544578c2e4061614615', 'E', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('51f4ba171d349421b856b1f26d6356b14546cbcd', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('957a674b4b5db36edb3d33cec62e391ac2a67f98', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '125000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('7582091ce2aeb20decd20d2d14cc718553f0bcef', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '130000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('1783aac3a64e616b5acdfae095ec6c522f224693', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c5dad49b111a1893ee44edd39f30130b2e0aa98b', 'c25868ef9cc8b9d93e7043e07e354b2946f554df', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('c690686bdd30688e6eeeb266117eb9f39970dce2', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'be36de438ed04d253d192a7abae9ef7cf0c4738b', '', '7252000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('be1f44922a69216c29dea37266588f09b6826e50', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '4d84ac8a38fd5e8cb2617c89c4de23475b9173c5', '', '3808000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('99c3238f694f82945fdabcb61463bf9f7f87a6c4', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'ebf228d8a650167838ab269a8c55dd64c41528c8', 'A', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('ecfa24a3450ff401f93de87bf8ffaa3c88408c1d', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '476ad41e4a4594d3be69db9c4b06a94096887ddd', 'B', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('9a87a8ab2105463a73b439d99ed0cb3a59008e75', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '7d6b672b9b7d323a88dec61826ac28826f00d7ff', 'C', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('5fed1e5b9702234a4603f64bfc0252cc283efd6d', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '4abd6b5e5c0834565115dba565d1a0b8b96da553', 'D', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2e297d44e5d447b91516e97bc12c768b98cca9cc', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '0a728653630619fce4b65553fb6ec31afce577a1', 'E', '370000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('bc76e02c9d5b0da021721dcb3ffa2518cd578159', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'd763948fb1b2529c209a3404bf0f5ca05a67cf0c', '', '150000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3b6dd6c6fac0e90c03a0a2f87d5e8045dd53a4be', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '35800a846ff500f3bbafe348fe19eb20726bfc92', '', '0', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('01a047d18b668ce1ad261591a0c740d543323331', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '22900bd4275c4132998f08ddb2a085964ddaed38', '', '110000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('d5f23d39c78d64b6a82cbcf969e9a47d593b8c3b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'e163e044f3f4ce8fbf39e8e363424b013089a2b4', 'A', '4960000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f3fa44b8371cdc1eded951fce21df8f562ac8c9b', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '6ce322773d75e0d26197deafc9e6dbde34f83dea', 'B', '1214000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('3c69ba026c49ee8b3e842b00e45df68045b02c46', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'b3b7d4c8291c2decf5616f3a64dec20d34cdd68f', 'C', '703000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('f0aac8cedd1d3510b68877e8d1f0d894ab1f3f29', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '4c2dae5ca007b65671d75198c66942ca4ed26a9d', 'D', '510000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('12b9869f43a32dfc292c9b680085d46380f19273', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '58473fc553632a96d6a97544578c2e4061614615', 'E', '310000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('8dbea11c5f6a74d74af06ef17f42f941aaacd161', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '4d7115c27736fb04e9ed9a3bd2d5c9b1067c1c43', '', '340000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('728a570aef5cc6a456401f92a70bea63fdaeb3b7', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'da5ceab13e436f7766534a536032fca6e1db957c', '', '232000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2187262029f1cd8950239cceef67d0a44380da66', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', 'fcfc7b460a4d4cc3e67141cb4f314d7bbfecfb07', 'B', '200000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('4af3a77f32b937ad8828b9fc32b9279a832e8292', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '3b234f8bbff671844bfca273ad05bd95e1f2d2d8', 'A', '250000', '1')
<query>INSERT INTO par_sbu_rinci VALUES ('2f9f13578684db9bca8f4f00a747a2b4ddad73a1', '11bc8ef9a02ab3813d9b695bab55ffe2a746e080', '1b1596d427c76aada53c972f6e87c9e3d573828e', 'C', '150000', '1')
<query>DROP TABLE IF EXISTS par_status_tl
<query>CREATE TABLE `par_status_tl` (
  `status_tl_id` varchar(50) NOT NULL,
  `status_tl_name` varchar(50) NOT NULL,
  `status_tl_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`status_tl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO par_status_tl VALUES ('42cea4fea50038ba22fcb8d00764c90b9bf8a2c0', 'Selesai TL', '1')
<query>INSERT INTO par_status_tl VALUES ('1451760f495995d4195af60bcd7ae117630ecd71', 'Tidak Dapat Ditindaklanjuti', '0')
<query>INSERT INTO par_status_tl VALUES ('31623703c9a29ae59d3f06aeb11fd82515403b2b', 'Dalam Proses TL', '1')
<query>INSERT INTO par_status_tl VALUES ('1674c2c98df4c2ae5984c91e0db68769c5370282', 'Belum TL', '1')
<query>INSERT INTO par_status_tl VALUES ('ddbfa5b4e34d302e1d12e28c0983d6b6fefe72d1', 'Tidak Dapat Ditindaklanjuti Dengan Alasan Sah', '1')
<query>DROP TABLE IF EXISTS par_sub_audit_type
<query>CREATE TABLE `par_sub_audit_type` (
  `sub_audit_type_id` varchar(50) NOT NULL,
  `sub_audit_type_id_type` varchar(50) NOT NULL,
  `sub_audit_type_name` varchar(100) NOT NULL,
  `sub_audit_type_desc` varchar(255) DEFAULT NULL,
  `sub_audit_type_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=delete, 1=aktif',
  PRIMARY KEY (`sub_audit_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS program_audit
<query>CREATE TABLE `program_audit` (
  `program_id` varchar(50) NOT NULL,
  `program_id_assign` varchar(50) NOT NULL,
  `program_id_auditee` varchar(50) NOT NULL,
  `program_id_ref` varchar(50) NOT NULL,
  `program_id_auditor` varchar(50) NOT NULL,
  `program_start` int(11) NOT NULL,
  `program_end` int(11) NOT NULL,
  `program_jam` float NOT NULL,
  `program_day` varchar(5) NOT NULL,
  `program_lampiran` varchar(255) DEFAULT NULL,
  `program_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=ajukan katim, 2=approve dalnis, 3=reject_dalnis',
  PRIMARY KEY (`program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO program_audit VALUES ('c72fc7e636ae05fd2f7155090378ca7a427c335f', 'a51852730078d23e3b48bf5972885364a11b17b8', '89a9a89e9ab60b3a2b4effd30baa16ce08e502b3', 'cb20d038bcf91d41ab8164ece8920a340e1afb8e', '9ca611db222a22298db1aa06a02564eebc25c3f1', '0', '0', '1.5', '', '', '0')
<query>INSERT INTO program_audit VALUES ('ab0e7aa6f7a329b3848ad9e234a95b133728a99f', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', '142d19e55edfc7d6f12fb4e5893582bc8b1c1c48', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '0', '0', '4', '', '', '0')
<query>INSERT INTO program_audit VALUES ('ac7e46a332f19a5b301f5fe1769cfb82a5f754cc', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', '2274713232a1ef625c8733e64c0bbc154826d5dd', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '0', '0', '4', '', '', '0')
<query>INSERT INTO program_audit VALUES ('dc4e23db077948a7061ed96b146da5852ca1c81f', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', 'fd12f5b03a995475ad758363e1c0abd9b6b0f30a', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '0', '0', '4', '', '', '0')
<query>INSERT INTO program_audit VALUES ('56a24b6cdcd82116d9ac39c387a5174f47df7c4b', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', 'e7ebbf13ec5cd2bb7b4eb74beed161b3760ef731', 'f892539385e88cf3bf1a0a332d5e3bc75b1c7488', '0', '0', '8', '', '', '0')
<query>INSERT INTO program_audit VALUES ('143a447b7f6eda81db2d9bf606aebeada071ea93', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', 'cb20d038bcf91d41ab8164ece8920a340e1afb8e', '9ca611db222a22298db1aa06a02564eebc25c3f1', '0', '0', '8', '', '', '0')
<query>INSERT INTO program_audit VALUES ('bb899eb02e705a1d2210e74091151e60e86c31c3', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', 'a07dffb7f397c31080a945aad570d0a34607dfdc', '9ca611db222a22298db1aa06a02564eebc25c3f1', '0', '0', '8', '', '', '0')
<query>INSERT INTO program_audit VALUES ('e89443c7cb18a9543250d8b190e490a3b3625efa', 'a51852730078d23e3b48bf5972885364a11b17b8', 'cd4a39ce251cbd6cac31f86a890cdc44fb0613c2', 'c0477d4c4c27faba27a157df65e4c6868511c96d', '9ca611db222a22298db1aa06a02564eebc25c3f1', '0', '0', '8', '', '', '0')
<query>DROP TABLE IF EXISTS program_audit_comment
<query>CREATE TABLE `program_audit_comment` (
  `program_comment_id` varchar(50) NOT NULL,
  `program_comment_program_id` varchar(50) NOT NULL,
  `program_comment_desc` varchar(255) NOT NULL,
  `program_comment_user_id` varchar(50) NOT NULL,
  `program_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`program_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS ref_audit
<query>CREATE TABLE `ref_audit` (
  `ref_audit_id` varchar(50) NOT NULL,
  `ref_audit_id_kategori` varchar(50) NOT NULL,
  `ref_audit_nama` varchar(100) NOT NULL,
  `ref_audit_desc` varchar(255) NOT NULL,
  `ref_audit_link` text,
  `ref_audit_attach` varchar(100) NOT NULL,
  `ref_audit_del_st` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_audit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO ref_audit VALUES ('fc506ea499fbc0c48b82e584bdab8f560b103e39', 'fc506ea499fbc0c48b82e584bdab8f560b103e39', 'qwerty', 'Memindahkan sejumlah risiko dari organisasi ke entsdasda', '', '', '0')
<query>INSERT INTO ref_audit VALUES ('c0a22838f9b6bd16e4b05494220fa32123f26383', 'c0c1cf2120d8017e100cc9283bb116e3a294aa7e', 'Tambah', 'Hari Raya Idul Adha d asd', '', '', '0')
<query>INSERT INTO ref_audit VALUES ('0fff295efd34ddf23946432e1ce00c9fcdac7a88', '44a30809871b51ca636e55d757d309c6070b8183', 'Pedoman Pengawasan Intern', 'Pedoman Pengawasan Intern Lingkup KKP', '', '', '0')
<query>INSERT INTO ref_audit VALUES ('f7f70541028393d52b4e3271e005246a2670e9ab', '7125197d541916478b4ba7ff30a0517772af6cf2', 'referensi prosedur pengawasan', 'Memindahkan sejumlah risiko dari organisasi ke entsdasda', '', 'P_20160226_094547.jpg', '0')
<query>INSERT INTO ref_audit VALUES ('15d660386bb6c6f217af2d6fb7445fd0c2ec3bf3', 'ed638e2ffcdfe03a75fda6c4f016fa249f10eb74', 'PP 60 Tahun 2008', 'SIstem Pengendalian Intern Pemerintah (SPIP)', '', 'PP60Tahun2008_SPIP.pdf', '1')
<query>INSERT INTO ref_audit VALUES ('d4e5a0e9d28128b5d3769ec228b86f842e66c1a2', 'ed638e2ffcdfe03a75fda6c4f016fa249f10eb74', 'PERMEN 21 Tahun 2011', 'Pedoman Manajemen Risiko', '', 'per-21-men-2011.pdf', '1')
<query>INSERT INTO ref_audit VALUES ('0903ea6c5f058aad630d8f7895a8d82f31e209fc', '3af6e7035b0ae166bfb6407955a7449ff63e4f06', 'PERMEN 23 Tahun 2015', 'Organisasi dan Tata Kerja', '', 'Organisasi dan Tata Laksana KKP.pdf', '1')
<query>INSERT INTO ref_audit VALUES ('a45604f47012d42de7ecdfbcaa8fffb367c5b40a', 'ed638e2ffcdfe03a75fda6c4f016fa249f10eb74', 'Permen KP Nomor 48 Tahun 2015', 'Pedoman Umum PSKPT dan Kawasan Perbatasan', '', 'R. Permen ttg P2K2PT 27 12 2015 (rapat bersama) + fr (tm_271215).doc', '1')
<query>INSERT INTO ref_audit VALUES ('954fe996b9380dfdb647bbd869ba9780bc2311d4', '2ee418634624573b9c9606b4b158e459e5ec8a0e', 'Program Kerja Evaluasi/Audit', 'Program kegiatan dalam bentuk evaluasi', '', 'TAO PK2PT (15 Feb 2016) .doc', '1')
<query>DROP TABLE IF EXISTS ref_program_audit
<query>CREATE TABLE `ref_program_audit` (
  `ref_program_id` varchar(50) NOT NULL,
  `ref_program_id_type` varchar(50) NOT NULL,
  `ref_program_code` varchar(25) NOT NULL,
  `ref_program_aspek_id` varchar(50) NOT NULL,
  `ref_program_title` varchar(255) NOT NULL,
  `ref_program_procedure` longblob NOT NULL,
  `ref_program_del_st` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=del, 1=active, 2=inactive',
  PRIMARY KEY (`ref_program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO ref_program_audit VALUES ('fe817c7f315682e58ac9941f7e709e09cfd86480', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'PA.01.01', 'c37ccd97c72da477e51f3b71809016a5f13a5765', 'Sistem Teknologi Informasi', '<ol>
	<li>Pahami struktur organisasi pelaksana fungsi perencanaan TI;</li>
	<li>Pahami kebijakan dan prosedur perencanaan TI;</li>
	<li>Pahami alur proses perencanaan TI;</li>
	<li>Evaluasi kecukupan subtansi kebijakan dan prosedur perencanaan TI;</li>
	<li>Dapatkan dokumentasi penyelenggaraan fungsi perencanaan TI;</li>
	<li>Uji kepatuhan perencanaan TI terhadap ketentuan yang berlaku;</li>
	<li>Identifikasi dan analisis penyimpangan/kelalaian/indikasi kecurangan dalam perencanaan TI</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('cfb99952ad415ceef069a5a758d7759f3427be19', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'PA.01.02', 'c37ccd97c72da477e51f3b71809016a5f13a5765', 'Sistem Teknologi Informasi', '<ol>
	<li>Pahami struktur organisasi pelaksana fungsi perencanaan TI;</li>
	<li>Pahami kebijakan dan prosedur pengembangan/pengadaan TI;</li>
	<li>Pahami alur proses pengembangan/pengadaan TI;</li>
	<li>Evaluasi kecukupan subtansi kebijakan dan prosedur pengembangan/pengadaan TI;</li>
	<li>Dapatkan dokumentasi penyelenggaraan fungsi pengembangan/pengadaan TI;</li>
	<li>Uji kepatuhan pengembangan/pengadaan TI terhadap ketentuan yang berlaku;</li>
	<li>Identifikasi dan analisis penyimpangan/kelalaian/indikasi kecurangan dalam pengembangan/pengadaan TI</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('717a2be16deab6c0c29a7da224a94a7224a1e157', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'PA.01.03', 'c37ccd97c72da477e51f3b71809016a5f13a5765', 'Sistem Teknologi Informasi', '<ol>
	<li>Pahami struktur organisasi pelaksana fungsi perencanaan TI;</li>
	<li>Pahami kebijakan dan prosedur pengoperasian TI;</li>
	<li>Pahami alur proses pengoperasian TI;</li>
	<li>Evaluasi kecukupan subtansi kebijakan dan prosedur pengoperasian TI;</li>
	<li>Dapatkan dokumentasi penyelenggaraan fungsi pengoperasian TI;</li>
	<li>Uji kepatuhan pengoperasian TI terhadap ketentuan yang berlaku;</li>
	<li>Identifikasi dan analisis penyimpangan/kelalaian/indikasi kecurangan dalam pengoperasian TI</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('c3e8c7432632fd3d34d591f1ca284055e34c0c71', '0525307297f9bed8984a8054918087dcf03c4bcf', '01', 'c37ccd97c72da477e51f3b71809016a5f13a5765', 'Sekretariat', '<p>Pelajari strukstur organisasi </p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('2537a73118a5b3dcee923af926f02d52c1de7b1f', '0525307297f9bed8984a8054918087dcf03c4bcf', '02', '3c7f1096de7da99484a42852da60792858994c60', 'peningktan kesejahteraan nelayan', '<p>1. Dapatkan data kelompok nelayan yang akan diberikan bantuan</p>

<p>2. cek identitas nelayan</p>

<p>3. Bandingkan identits nelayan dengan data kelompok yang akan diberi bantuan</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('c867194b3a60785549fc50899c4e621f0adb8f4a', '0525307297f9bed8984a8054918087dcf03c4bcf', '03', '6031fc656ebe93a64d401dc66eace039c475389e', 'IT', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('aa214bc9b69cd26e60b8030fa501ae21fae77938', '0525307297f9bed8984a8054918087dcf03c4bcf', '04', '8f82a264012563a6e9b4105025bb5869d35a34a5', 'IT', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('2132929fe9b84e32b677460729c66ff2a56afb77', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', '05', '8f82a264012563a6e9b4105025bb5869d35a34a5', 'Pengolahan Hasil', '<p>AAA</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('cb20d038bcf91d41ab8164ece8920a340e1afb8e', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.01', 'cd1d3d9ba16a8fd8eadb351dcfe6ef8dcea417d0', 'Penyelesaian Tindak Pidana Kelautan dan Perikanan (TPKP) yang disidik tidak sesuai dengan ketentuan yang berlaku', '<ol>
	<li>Minta dokumen yang dibuat oleh Pengawas Perikanan pada pemeriksaan pendahuluan. <em>Cek</em> kelengkapan dokumen-dokumen (form 01 s.d. form 15) dan berita acara serah terima dari Pengawas Perikanan kepada Petugas Penerimaan dan Penelitian (Juknis: halaman 9-10)</li>
	<li><em>Uji</em> kebenaran ketepatan waktu penyerahan dokumen pemeriksaan pendahuluan dari PPNS penerimaan dan penelitian kepada Kepala Satker/Penyidik (maksimal 1 x 24 jam)</li>
</ol>

<p><strong>Jukni</strong>s :&rdquo;<em>Hasil penerimaan dan penelitian sesegera mungkin dilaporkan kepada atasan PPNS Perikanan dalam waktu 1x 24 Jam&rdquo; </em></p>

<p><em>3. Apabila dari hasil penerimaan dan penelitian ditemukan adanya bukti-bukti yang memenuhi unsur TPP maka hasil penerimaan dilanjutkan ke tahap penyidikan.</em></p>

<p><em>4. Apabila dari hasil penerimaan dan penelitian tidak ditemukan alat bukti yang cukup untuk memenuhi unsur TPP maka perkara dapat dihentikan untuk tidak ditindaklanjuti ke tahap penyidikan dan dibuat alasan-alasan hukum apakah terdapat tidak cukup bukti yang dituangkan dalam Berita Acara Pendapat.</em></p>

<p><em>5. Verifikasi dokumen</em> penyidikan pada setiap tahapan (1.Surat Perintah Tugas, 2.Surat Perintah Penyidikan, 3.Surat Pemberitahuan Dimulainya Penyidikan, 4.Pemanggilan, 5.Penangkapan, 6.Penahanan, 7.Penggeledahan, 8.Penyitaan, 9.Pemeriksaan, dan 10.<em>in Absentia</em> (apabila ada) serta Penyelesaian dan Penyerahan Berkas Perkara, Penghentian Penyidikan (apabila ada). Tanyakan sebab dan akibat jika tidak lengkap,</p>

<p><em>6. Teliti</em> surat penahanan apakah sudah sesuai dengan ketentuan. (Juknis: lamanya waktu penahanan demi kepentingan penyidik maksimal 20 hari dan dapat diperpanjang 10 hari dengan dilengkapi Berita Acara tersendiri).</p>

<p>7. Untuk Pemberkasan, teliti kesesuaian waktu penyampaian hasil penyidikan berkas perkara ke Penuntut Umum (<em>paling lama 30 hari sejak pemberitahuan dimulainya penyidikan</em>) dan dokumen yang diminta oleh Penuntut Umum (PU) apakah sudah dilengkapi dokumennya sesuai dengan waktu yang ditentukan (termasuk Surat Pengantar dan Berita Acara Penyerahan tersangka dan barang bukti dari Penyidik kepada PU)</p>

<p>(Juknis: <em>Apabila berkas perkara tahap pertama telah diserahkan dinyatakan belum lengkap (P-18) oleh PU dan disertai petunjuk (P-19), Penyidik dalam waktu 10 (sepuluh ) hari harus melengkapi berkas perkara dengan memenuhi seluruh petunjuk P19 dari PU</em>&rdquo;.</p>

<p>8. Terhadap Penghentian Penyidikan, Apabila tidak terdapat cukup alat bukti atau peristiwa tersebut bukan merupakan tindak pidana perikanan, &nbsp;<em>Teliti </em>&nbsp;notulensi hasil gelar perkara, (termasuk tanda tangan/paraf peserta gelar perkara) dan selanjutnya analisa hasil kesimpulan dari notulesi hasil gelar perkara.</p>

<p><em>9. Cek dan uji</em> Dokumen pertanggung jawaban keuangan atas penyelesaian TPKP yang disidik apakah telah didukung bukti pertangungjawaban yang sah.</p>

<p>10. Lakukan diskusi dengan Pengawas Perikanan, PPSPM, dan/atau PPNS Perikanan apabila terjadi perbedaan selanjutnya analisa penyebab perbedaan tersebut<em>.</em></p>

<p>11. Buat Simpulan hasil audit dan rumuskan temuan&nbsp;</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('5873e83b1ca02490a07c608d9664a0f172f76ff2', '0525307297f9bed8984a8054918087dcf03c4bcf', '07', '462599baf768cd3bb1bc6ebad7a36d338206c884', 'IT', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('7b03954f39b3b105f6f7b970ebd4a92486f7fbfe', '0525307297f9bed8984a8054918087dcf03c4bcf', '08', '688d723bd69b2d06980b519ca49badc09f16ee4d', 'IT', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('8089b2719ec87cd9e8f29aef56c2e9673aa052b5', '0525307297f9bed8984a8054918087dcf03c4bcf', '09', '3da92cabe8a8473f55e638fa7dc43ea1753f40e8', 'IT', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('c926472c96051eeff9bc79905aafd7b33e9ade6a', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.02', 'cd1d3d9ba16a8fd8eadb351dcfe6ef8dcea417d0', 'Penyelesaian Penanganan barang bukti dan awak kapal Tidak Sesuai Ketentuan', '<ol>
	<li>Minta dan Cek kelengkapan dokumen pada Petugas Barang Bukti:&nbsp; form BA-3 s.d. L.TPP2 (<em>Juknis Barang Bukti : Lampiran II Juknis Barang Bukti</em>). Jika tidak tanyakan sebab dan akibatnya</li>
	<li>Terhadap barang bukti pada PE 5 dan PE 6, cek apakah telah dilengkapi Surat Perintah Penyitaan dan atau ijin dari ketua pengadilan negeri setempat. &nbsp;Jika tidak tanyakan sebab dan akibatnya</li>
</ol>

<p>(Juknis : &ldquo;<em>Dalam Keadaan mendesak dan memerlukan tindakan segera, penyitaan barang bukti atau dokumen/surat dapat dilakukan tanpa ijin dari ketua pengadilan negeri tatapi terbatas pada benda-benda bergerak dan selanjutnya melaporkan kepada Ketua Pengadilan Neeri setempat untuk memperoleh penetapan</em>&rdquo;)</p>

<p>3. Terhadap barang bukti yang dilelang, cek apakah telah ada ijin dari pengadilan negeri setempat dan persetujuan dari tersangka. Jika tidak tanyakan sebab dan akibatnya</p>

<p>4. Terhadap barang bukti yang disita, cek apakah telah dilengkapi tanda terima bukti penyitaan, Jika tidak tanyakan sebab dan akibatnya</p>

<p>5. Lakukan Pengecekan, apakah PPNS Perikanan melakukan perawatan dan penjagaan/pengamanan barang sitaan yang meliputi jenis barangnya, kualitas dan jumlah bandingkan dengan hasil laporan pemantauan perawatan dan pengamanan barang bukti, Jika tidak tanyakan sebab dan akibatnya</p>

<p>6. Lakukan Pengecekan, apakah Barang bukti dilengkapi dengan kartu barang bukti dan label yang diisi secara lengkap. Jika tidak tanyakan sebab dan akibatnya</p>

<p>7. Lakukan&nbsp;<em>trasir</em>&nbsp;bukti-bukti pertanggungjawaban perawatan dan pengamanan barang bukti kemudian bandingkan dengan laporan pemantauan perawatan dan pengamanan barang bukti.</p>

<p>8. Apabila terdapat perbedaan, diskusikan dengan Petugas barang bukti, dan atau PPNS Perikanan kemudian lakukan analisa untuk mencari sebab</p>

<p>9. Buat simpulan hasil audit dan rumuskan temuan</p>

<p>10. Buat Ringkasan temuan dan rencana aksi</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('a07dffb7f397c31080a945aad570d0a34607dfdc', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.03', 'cd1d3d9ba16a8fd8eadb351dcfe6ef8dcea417d0', 'Pengelolaan hasil sitaan barang bukti tidak tertib', '<ol>
	<li>Minta matriks data perkara TPP-43 yang sudah mempunyai hukum yang tetap</li>
	<li>Cek Surat Persetujuan dari pengadilan negeri atas pelelangan barang bukti dari KPKNL.</li>
	<li>Cek dan verifikasi Bukti setor PNBP hasil pelelangan barang bukti</li>
	<li>Cek hasil pengadilan terhadap pidana denda, apakah sudah dilakukan penyetoran PNBP.</li>
	<li>Cek hasil lelang sitaan, apakah hasil lelang sitaan telah disetorkan sebagai PNBP?</li>
	<li>Diskusikan dengan Bendahara Penerimaan dan/atau Kepala Satker Apabila terjadi perbedaan, tanyakan sebab akibatnya, kemudian lakukan analisa.&nbsp;</li>
	<li>Buat Simpulan hasil audit dan rumuskan temuan</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('2274713232a1ef625c8733e64c0bbc154826d5dd', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.04', 'cd1d3d9ba16a8fd8eadb351dcfe6ef8dcea417d0', 'Register dan Pelaporan  Penanganan Pelanggaran dan Penanganan Barang Bukti  Tidak Tertib', '<ol>
	<li>Cek Register Perkara apakah telah diisi sebagaimana mestinya yaitu hasil-hasil perkara TPP dan/atau pengaduan masyarakat, temuan dari pengawas perikanan, temuan dari patroli bersama atau instansi lain termuat pada register perkara dan ditandatangani. Jika belum tanyakan sebab dan akibatnya</li>
	<li>Minta dan Cek apakah laporan bulanan dan triwulan telah dibuat secara rutin</li>
	<li>Diskusikan dengan Kepala Satker jika terdapat ketidaksesuaian, kemudian lakukan analisa.&nbsp;</li>
	<li>Buat Simpulan &nbsp;hasil audit dan rumuskan temuan</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('fd12f5b03a995475ad758363e1c0abd9b6b0f30a', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.01', '3c7f1096de7da99484a42852da60792858994c60', 'Target Kinerja Tidak Tercapai', '<ol>
	<li>Dapatkan Laporan Kinerja beserta manual IKU dan Data Dukung IKU;</li>
	<li>Bandingkan data antara capaian kinerja dengan perjanjian kinerja, jika terdapat perbedaan konfirmasi dengan Tim Penyusun LKj;</li>
	<li>Periksa capaian kinerja apakah telah disusun sesuai manual IKU yang ditetapkan;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('3b82acc8e80f5450598051f21b031b65089c4b3e', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.02', '3c7f1096de7da99484a42852da60792858994c60', 'Pelaksanaan Pengadaan Barang/Jasa TA 2015 tidak Efisien, tidak Ekonomis dan tidak sesuai dengan ketentuan', '<ol>
	<li>Dapatkan dokumen dan daftar pengadaan barang dan jasa (melalui penyedia jasa dan secara swakelola);</li>
	<li>Lakukan uji petik terhadap pengadaan yang strategis;</li>
	<li>Lakukan cek fisik hasil pengadaan (bandingkan hasil pengadaan dengan dokumen kontrak);</li>
	<li>Periksa apakah hasil pengadaan telah sesuai dengan TOR dan Dokumen Pengadaan, serta pengadaan telah dilakukan secara efisien, efektif, dan ekonomis;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('c950ad83407d0e7f3ac548838cabbe6abddc095e', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.03', '3c7f1096de7da99484a42852da60792858994c60', 'Penetapan target PNBP belum disusun secara realistis', '<ol>
	<li>Dapatkan dokumen terkait target PNBP yang sudah ditetapkan,&nbsp; lakukan pemeriksaan, apakah dalam penyusunan target (rencana) PNBP telah dikoordinasikan dengan Sekretariat Ditjen Perikanan Tangkap serta Direktorat Pelabuhan Perikanan;</li>
	<li>Lakukan analisa target PNBP telah disusun secara realistis dengan menggunakan: a)&nbsp;formula volume x tarif per jenis PNBP, dan b)&nbsp;realisasi PNBP selama 3 tahun terakhir.</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('1843c33fb6b91f3e788fca1bc360cbfc150c80d5', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ4.KINERJA.UPT.06', '8f82a264012563a6e9b4105025bb5869d35a34a5', 'Target PNBP tidak tercapai', '<ol>
	<li>Dapatkan Buku Penerimaan PNBP;</li>
	<li>Bandingkan penerimaan PNBP dengan target yang telah ditetapkan;</li>
	<li>Diskusikan dengan penanggungjawab jika penerimaan PNBP tidak mencapai target;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('c0477d4c4c27faba27a157df65e4c6868511c96d', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.05', '3c7f1096de7da99484a42852da60792858994c60', 'Terdapat keterlambatan dalam penyetoran PNBP', '<ol>
	<li>Dapatkan Buku Penerimaan PNBP;</li>
	<li>Periksa apakah terdapat penerimaan PNBP yang disetorkan ke Kas Negara melebihi ketentuan;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('142d19e55edfc7d6f12fb4e5893582bc8b1c1c48', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.06', '3c7f1096de7da99484a42852da60792858994c60', 'Pungutan PNBP tidak sesuai dengan PP 75 Tahun 2015', '<ol>
	<li>Dapatkan data jenis pungutan PNBP yang berlaku di satker;</li>
	<li>Bandingkan dengan penerimaannya, apakah terdapat pungutan PNBP yang dipungut melebihi ketentuan;</li>
	<li>Periksa apakah terdapat pengguna jasa pelabuhan yang tidak dikenakan pungutan PNBP;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('829a9458b8e902951cb8ae311e8f0da11f6d3cb5', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.07', '3c7f1096de7da99484a42852da60792858994c60', 'Standar Pelayanan dan Maklumat Pelayanan belum dibuat/publikasikan', '<ol>
	<li>Dapatkan Standar Pelayanan (SP);</li>
	<li>Periksa apakah penyusunannya telah mengacu Permen PANRB Nomor 15/2014, jika telah sesuai periksa pelaksanaannya dilapangan;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('43b26b60773e310015ab22f2085da0149a357e1e', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.08', '3c7f1096de7da99484a42852da60792858994c60', 'Pelabuhan Perikanan belum melakukan Survei Kepuasan Masyarakat', '<ol>
	<li>Dapatkan hasil Survei Kepuasan Masyarakat (SKM), periksa apakah hasil telah dipublikasikan dan telah ditindaklanjuti;</li>
	<li>Periksa apakah pelaksanaan survei telah mengacu Permen PANRB Nomor 16/2016;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('fa823520e5f4534b2da5e7309ec79b6281ec9e1d', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.09', '3c7f1096de7da99484a42852da60792858994c60', 'Penerbitan Surat Persetujuan Berlayar (SPB) belum sesuai dengan ketentuan', '<ol>
	<li>Dapatkan SOP Penerbitan SPB;</li>
	<li>Lakukan uji petik terhadap dokumen SPB yang telah diterbitkan, apakah pelaksanaannya telah sesuai dengan SOP dan Permen KP Nomor 3 tahun 2013 tentang Kesyahbandaran di Pelabuhan Perikanan;</li>
	<li>Periksa dokumen pendukung SPB terkait syarat teknis dan administrasi apakah telah sesuai dengan ketentuan;</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('c5a00fc6cebca18537aec37f33645c8e7772ea83', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ2.KINERJA.UPT.10', '3c7f1096de7da99484a42852da60792858994c60', 'Terdapat Dokumen Pertanggungjawaban Keuangan yang Tidak Sesuai Ketentuan', '<ol>
	<li>Dapatkan dokumen pertanggungjawaban keuanagn TA.2015 dan 2016;</li>
	<li>Periksa dokumen pertanggungjawaban keuangan dan laporannya telah sesuai ketentuan (SBU, SBK dan ketentuan lainnya terkait keuangan);</li>
	<li>Diskusikan dengan penanggungjawab jika ditemukan permasalahan;</li>
	<li>Buat kesimpulan.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('e1077ef1da5107fcbf1753003e387f73daf962d1', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'ITJ.5/BPSDMKP.1/2016', '688d723bd69b2d06980b519ca49badc09f16ee4d', 'SUPMN Bone', '<p>&nbsp;</p>

<p><strong>A. Pengumpulan Data dan Bahan</strong>, berupa:</p>

<ol>
	<li>Surat permohonan perceraian dari Sdr. Jumardi kepada Kepala SUPM Negeri Bone;</li>
	<li>Berita acara mediasi dalam usaha mendamaikan atau mencegah terjadinya perceraian antara Sdr. Jumardi dengan istrinya (Sdri. Mastiana) yang dilakukan oleh pimpinan SUPM Negeri Bone;</li>
	<li>Surat persetujuan atau penolakan perceraian a.n. Sdr. Jumardi yang dikeluarkan oleh Kepala SUPM Negeri Bone;</li>
	<li>Berita acara atau surat keterangan dari Kepala Desa terkait usaha mendamaikan atau mencegah terjadinya perceraian antara Sdr. Jumardi dengan istrinya (Sdri. Mastiana);</li>
	<li>Surat pernyataan dari Sdr. Jumardi terkait akan bertanggungjawab akibat terjadinya perceraian rumah tangganya apabila ditemukan pelanggaran yang terjadi;</li>
	<li>Putusan Pengadila Agama terkait perceraian antara Sdr jumardi dengan Sdri. Mastiana;</li>
	<li>dll.</li>
	<li><strong>Konfirmasi atau wawancara dengan pihak terkait, </strong>seperti:</li>
	<li>Kepala SUPM Negeri Bone;</li>
	<li>Kepala Subbag Tata Usaha SUPM Negeri Bone;</li>
	<li>Sdri. Mastiana (mantan istri sah dari Sdr. Jumardi);</li>
	<li>Sdri. Andi Harmila (yang diduga pasangan dari Sdr. Jumardi);</li>
	<li>Pihak Pengadilan Agama Bone / Watampone (bila diperlukan);</li>
	<li>Orang tua dari Sdri. Mastiana;</li>
	<li>Orang tua dari sdri. Andi Harmila;</li>
	<li>Kepala Desa/Kelurahan Mattiro Wafie (tempat kediaman Sdr. Jumardi dan Sdri. Mastiana);</li>
	<li>Teman atau kerabat dari Sdr. Jumardi</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('e7ebbf13ec5cd2bb7b4eb74beed161b3760ef731', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ1.PRL', '460d0b13f7a9cb51e5391985bd42f8ea4afbef2c', 'Pengadaan Barang dan Jasa', '<p>1. Dapatkan Kontrak</p>

<p>2. Pelajari Volume yang ada di kontrak</p>

<p>3. Lakukan Pengecekan terhadap voleme di lapangan</p>

<p>4. Bandingkan apakah ada deviasi</p>

<p>5. Analisis&nbsp;Deviasi sebagai temuan dan cari penyebab dan akibatnya</p>

<p>6. Lakukan Klarifikasi terhadap ppk dan kesepakatan terhadap adanya deviasi</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('42365358678acb7415fd43fed5f52faadc63dd9b', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'ITJ.5/ATT/SETJEN/DJPT/DJP', 'c37ccd97c72da477e51f3b71809016a5f13a5765', 'Dinas Kelautan dan Perikanan Provinsi Papua Barat, Dinas Kelautan dan Perikanan Kab. Sorong, Dinas Kelautan dan Perikanan Kab. Raja Ampat', '<p>Audit akan dilaksanakan dengan teknik:</p>

<ol>
	<li>Konfirmasi atau wawancara dengan pihak terkait, yaitu:
	<ul>
		<li>Kuasa Pengguna Anggaran dan Penanggung Jawab Operasional Kegiatan yang menjadi Sasaran TATD;</li>
		<li>Pihak yang bertanggungjawab menindaklanjuti rekomendasi hasil audit;</li>
		<li>Pihak lain yang kompeten.</li>
	</ul>
	</li>
	<li>Cek atau uji fisik terkait keberadaan pihak yang bertanggungjawab menindaklanjuti rekomendasi hasil audit.</li>
	<li>Pengumpulan bukti, dokumen, data, dan informasi yang terkait dengan pihak yang bertanggungjawab menindaklanjuti rekomendasi hasil audit;</li>
	<li>Analisis terhadap bukti-bukti yang didapatkan termasuk bukti dukung permohonan usulan TATD yang diajukan pihak Satker.</li>
	<li>gw ganti ye</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('487dccf102af69592146352fe976540aaa300e60', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', '06.01.01', '3c7f1096de7da99484a42852da60792858994c60', 'Pelabuhan Perikanan Jakarta', '<p>Langkah Audit :</p>

<p>1. dapatkan tupoksi unit kerja yang di duga melakukan penyalahgunaan wewenang.</p>

<p>2. lakukan wawancara dengan bahwahan terduga, buat dengan BA Pemeriksaan.</p>

<p>3. Lakukan wawancara dengan atasan terduga, buat dengan BA Pemeriksaan</p>

<p>4. Lakukan wawancara dengan terduga, buat simpulkan buat BA Pemeriksaan</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('32d01ecc96acf2f30d169d3f90dfaafd9365a644', '0525307297f9bed8984a8054918087dcf03c4bcf', 'ITJ.1.PSKPT BIAK NUMFOR', '460d0b13f7a9cb51e5391985bd42f8ea4afbef2c', 'PSKPT', '<ol>
	<li>Dapatkan dokumen perencanaan PKSPT 2016 di Ditjen PRL, antara lain: profil, pedoman umum, <em>business plan, master plan, action plan</em>, SK Tim PSKPT.</li>
</ol>

<ul>
	<li>Kaji apakah dokumen perencanaan tersebut sudah sesuai/relevan dengan dokumen perencanaan lainnya?</li>
	<li>Jika tidak sesuai, tanyakan ke penanggung jawab kegiatan. &nbsp;</li>
	<li>Analisis dan tuangkan dalam KKA</li>
</ul>

<ol>
	<li>Lakukan uji lapang untuk meyakini bahwa perencanaan PSKPT Tahun 2016 dapat dilaksanakan di Kab. Biak Numfor, Sarmi, Morotai, dan Maluku Barat Daya (Moa/Kisar) termasuk menilai kesiapan daerah dalam mendukung PSKPT</li>
	<li>Lakukan konfirmasi/koordinasi dengan Dinas KP, Pertamina, dan PLN terkai dengan pembangunan Dermaga Pendaratan Ikan, SPDN, Cold Storage, Pabrik Es, Pengadaan Kapal dan alat tangkap, Sarana Prasarana Lingkungan (<em>Tracking Mangrove</em>), Sarana Air Bersih</li>
</ol>

<ul>
	<li>Dapatkan eksisting Dermaga Pendaratan Ikan, SPDN, Cold Storage, Pabrik Es, Pengadaan Kapal dan alat tangkap, Sarana Prasarana Lingkungan (Tracking Mangrove), Sarana Air Bersih</li>
	<li>Jika ada, apakah sarana prasarana tersebut operasional? Jika tidak diskusikan dengan pihak terkait untuk mengetahui penyebabnya.</li>
	<li>Jika belum, apakah Pemda sudah siap dengan dokumen pendukung untuk pembangunan sarana prasarana tersebut, seperti pembebasan lahan, dukungan PLN, Pertamina.</li>
	<li>Analisis dan tuangkan dalam KKE</li>
</ul>

<ol>
	<li>
	<p>Diskusi atau lakukan pertemuan dengan Pemda seperti Bappeda, Setda, Pertamina, PLN, Dinas Koperasi dan UKM untuk menilai kesiapan daerah dalam mendukung PSKPT, seperti Rencana Tata Ruang di Bappeda, kapasitas listrik di PLN, Stok BBM di Pertamina, dukungan anggaran di Sekretariat Daerah, dukungan Dinas Koperasi dan UKM dalam pembentukan koperasi KUB/Pokdakan/Poklahsar</p>
	</li>
</ol>

<ul>
	<li>
	<p>Jika dokumen-dokumen pendukung belum ada, diskusikan lebih lanjut untuk mengetahui penyebabnya</p>
	</li>
	<li>
	<p>Tanyakan, apakah Pemda telah mempersiapkan APBD untuk mendukung PSKPT di daerahnya</p>
	</li>
	<li>
	<p>Jika belum, kaji penyebabnya</p>
	</li>
	<li>
	<p>Analisis hasil diskusi atau pertemuan dengan&nbsp;<span style="line-height:1.6em">SKPD terkait dan tuangkan dalam KKE.</span></p>
	</li>
</ul>

<ol>
	<li>
	<p><span style="line-height:1.6em">​</span>Buat kesimpulan hasil evaluasi dan tuangkan dalam rencana aksi</p>
	</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('5dd82fda451ab9961258d2c45b59e240520dcc3f', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'itj 1. BKIPM. TAO 1', '3da92cabe8a8473f55e638fa7dc43ea1753f40e8', 'Kesiapan BKIPM atas pelimpahan kewenangan', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('d0d2f3c34a55db195c08f881ff9773bc8ff948b5', '4e66b7491b324b8ebd2a7262ad2847dbf31eb39b', 'itj 1. BKIPM. TAO 2', '3da92cabe8a8473f55e638fa7dc43ea1753f40e8', 'Kesiapan BKIPM atas pelimpahan kewenangan', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('8ecf34f686ae299223013de2818c156865c3e1ee', '0525307297f9bed8984a8054918087dcf03c4bcf', 'Program kerja audit Menta', 'c37ccd97c72da477e51f3b71809016a5f13a5765', 'Perikanan Budidaya, Tangkap, PRL, BPSDM, PSDKP, PDSPKP Mentawai', '<p><strong>TAO/TEO PENGAWASAN</strong></p>

<p><strong>PENGEMBANGAN KAWASAN KELAUTAN DAN PERIKANAN TERPADU (PK2PT)</strong></p>

<p>&nbsp;</p>

<ol>
	<li>Reviu persiapan dan evaluasi pelaksanaan</li>
</ol>

<p>Tujuan :</p>

<ol>
	<li>Mengidentifikasi kegiatan perencanaan, pelaksanaan dan evaluasi percepatan pembangunan kawasan kelautan dan perikanan terintegrasi di pulau-pulau kecil Tahun 2015-2016 di 15 pulau.</li>
</ol>

<ol>
	<li>TEO 1. Evaluasi Hasil Pelaksanaan Kegiatan TA 2015 belum dilaksanakan;</li>
	<li>TEO 2. Perencanaan Kegiatan TA 2016 Belum Memadai.</li>
</ol>

<ol>
	<li>Menilai pemanfaatan pembangunan kawasan kelautan dan perikanan terintegrasi di pulau-pulau kecil Tahun 2015-2016 di 15 pulau</li>
</ol>

<ol>
	<li>TEO 1. Pembangunan Kawasan PK2PT TA 2015 Tidak Sesuai dengan Perencanaan;</li>
	<li>TEO 2. Pelaksanaan Kegiatan PK2PT TA 2015 Tidak Sesuai dengan Kontrak;</li>
	<li>TEO 3. Hasil Pelaksanaan Kegiatan PK2PT TA 2015 Belum Dimanfaatkan.</li>
</ol>

<p>&nbsp;</p>

<ol>
	<li>Pendampingan Pelaksanaan Kegiatan TA 2016</li>
</ol>

<p>Tujuan :</p>

<p>Meyakini pelaksanaan kegiatan TA 2016 telah sesuai dengan perencanaan, termasuk kegiatan pengadaan barang dan jasa yang bersumber dari dana pusat dan TP</p>

<ol>
	<li>TEO 1. Pelaksanaan kegiatan belum sesuai rencana;</li>
	<li>TEO 2. Pelaksanaan PBJ belum sesuai ketentuan.</li>
</ol>

<p>&nbsp;</p>

<ol>
	<li>Monev Kegiatan DAK terhadap Program PK2PT</li>
</ol>

<p>Tujuan :</p>

<p>Memantau pelaksanaan kegiatan PK2PT yang bersumber dari DAK</p>

<ol>
	<li>TEO 1. Kegiatan DAK belum memberikan kontribusi yang memadai terhadap</li>
</ol>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PK2PT (Perencanaan Pelaksanaan DAK belum dikoordinasikan dengan</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Tim PK2PT);</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<ol>
	<li>TEO 2. Perkembangan pelaksanaan kegiatan PK2PT yang bersumber dari</li>
</ol>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;DAK belum dilaporkan kepada Menteri KP selaku koordinator teknis</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Permen KP No 51/PERMEN-KP/2014 tentang petunjuk teknis penggunaan</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DAK bidang Kelautan dan Perikanan Tahun 2015)</p>

<p>&nbsp;</p>

<ol>
	<li>Audit Pelaksanaan Program PK2PT</li>
</ol>

<p>Tujuan :</p>

<p>Memperoleh keyakinan yang memadai bahwa pengembangan kawasan kelautan dan perikanan di pulau-pulau kecil terluar telah dilaksanakan secara efektif, efisien dan ekonomis</p>

<ol>
	<li>TAO 1. Pelaksanaan kegiatan belum efektif (hasil pelaksanaan kegiatan telah</li>
</ol>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dimanfaatkan sesuai dengan rencana/tujuan)</p>

<ol>
	<li>TAO 2. Pelaksanaan kegiatan belum efisien (hasil pelaksanaan kegiatan tidak</li>
</ol>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; memberikan nilai tambah)</p>

<ol>
	<li>TAO 3. Pelaksanaan kegiatan belum ekonomis (hasil pelaksanaan kegiatan tidak</li>
</ol>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; sesuai kontrak)</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('d15a26d62e73e45b9ec115b47508e87dffa84072', '0525307297f9bed8984a8054918087dcf03c4bcf', 'PKA P3D', '688d723bd69b2d06980b519ca49badc09f16ee4d', 'penyuluhan', '<ol>
	<li>Pastikan Sosialisasi UU Nomor 23 Tahun 2014 dan Sosialisasi Pengalihan P3D telah diterima cukup jelas/tidak oleh Pemda (Bakorluh/BKPP/Dinas KP Prov dan Bapeluh Kab./kota) dan Penyuluh Perikanan PNS&nbsp; dengan menggunakan kuesioner terlampir;</li>
	<li>Pastikan Surat Edaran Kemendagri no 120/253/SJ tanggal 15 Januari 2015, telah diterima oleh Pemda setempat;</li>
	<li>Apakah Pedoman/Juklak/Juknis Inventarisasi Personil P3D telah diterima oleh daerah. Jika belum diterima, kaji upaya percepatan pengalihan oleh Pemda setempat.</li>
	<li>Pelajari data jumlah Penyuluh Perikanan PNS di tingkat Provinsi dan kab/kota, selanjutnya dapatkan Daftar Penyuluh PNS yang sudah melakukan pendaftaran P3D baik secara Online ke www.pusluh.kkp.go.id maupun secara langsung.</li>
	<li>Kaji lebih lanjut apabila belum seluruh Penyuluh PNS melakukan pendaftaran melalui konfirmasi ke Penyuluh yang bersangkutan dan atau ke Atasan Langsung Penyuluh bersangkutan untuk mendapatkan kendala/permasalahan dan upaya penyelesaiannya.</li>
	<li>Pelajari data sarana/prasarana Penyuluh Perikanan PNS&nbsp; terkait jenis, jumlah dan kond</li>
	<li>isi barang (baik, rusak, hilang, pinjam pakai) serta dokumen&nbsp; Penyuluhan terkait di Pemda setempat (gunakan lampiran 3 sebagai alat uji).</li>
	<li>Inventarisir kendala/permasalahan yang dihadapi dalam proses pengalihan sarana/prasarana Penyuluh Perikanan dan upaya penyelesaian oleh Pemda setempat;</li>
	<li>Buatkan simpulan dan Rencana Aksi evaluasi serta tuangkan dalam KKE.</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('7f944c264f75889756e3d43ab566406cc016e842', '0525307297f9bed8984a8054918087dcf03c4bcf', 'KPA.01', '6031fc656ebe93a64d401dc66eace039c475389e', 'Kawasan Budidaya', '<ol>
	<li>Dapatkan</li>
	<li>Reviu</li>
	<li>Uji</li>
	<li>Observasi</li>
</ol>', '0')
<query>INSERT INTO ref_program_audit VALUES ('d917bd94618760e3579409da1c51d313719be59d', '0525307297f9bed8984a8054918087dcf03c4bcf', 'KPA.02', '6031fc656ebe93a64d401dc66eace039c475389e', 'Kawasan Budidaya', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('4456037e8e3d892c43deda4d06ae237555d3a28a', '0525307297f9bed8984a8054918087dcf03c4bcf', 'PK2PT It 4', '8f82a264012563a6e9b4105025bb5869d35a34a5', 'ICS Persiapan', '', '0')
<query>INSERT INTO ref_program_audit VALUES ('c04cd0ffd00bd26686d30c047aed335219e3df79', '0525307297f9bed8984a8054918087dcf03c4bcf', '123qwe', '3c7f1096de7da99484a42852da60792858994c60', 'Dinas Kelautan dan Perikanan Provinsi Papua Barat, Dinas Kelautan dan Perikanan Kab. Sorong, Dinas Kelautan dan Perikanan Kab. Raja Ampat', '<p>123qweadszxc</p>', '0')
<query>INSERT INTO ref_program_audit VALUES ('ad1da4b82408bc0185b3717b68a84347b799b394', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO001', '277f1441ce02dbf662e6feb6c669b603a4de35d0', 'Administrasi Keuangan', '<ol>
	<li>Laksanakan opname kas baik rutin maupun PNBP dan buatkan Berita Acara serta Registernya.</li>
	<li>Teliti buku-buku yang di pergunakan oleh Bendahara.</li>
	<li>Hitung semua uang tunai dan surat-surat berharga yang ada di dalam brankas, apakah ada uang tunai dan surat berharga yang tidak disimpan didalam brankas.</li>
	<li>Lakukan pengujian terhadap kebenaran bukti-bukti yang ada. Teliti apakah semua penerimaan dan pengeluaran didukung dengan bukti-bukti yang syah dan dicatat dalam BKU dan buku pembantu sesuai pembebanannya.</li>
	<li>Teliti apakah saat penutupan BKU telah di laksanakan sesuai dengan ketentuan.</li>
	<li>Lakukan pencocokan antara saldo Buku Bank per saat audit, mintakan R/C-nya.</li>
	<li>Teliti apakah atasan langsung Bendahara telah mengadakan pemeriksaan secara periodik dan dibuatkan BA audit kas sepanjang petugas yang ditunjuk untuk itu tidak melakukan audit.</li>
	<li>Cocokkan antara SP2D yang telah diterbitkan KPPN dengan nilai DIPA-nya dan penjadwalannya menurut RPU.</li>
	<li>Teliti saldo akhir tahun anggaran lalu dan teliti berdasarkan SPP yang&nbsp; diajukan.</li>
	<li>Teliti apakan SPM yang diterima berdasarkan SPP yang diajukan.</li>
	<li>Teliti sumber dana lain diluar DIK untuk membiayai kegiatan rutin.</li>
	<li>Teliti penerimaan penyetoran dll dan penerimaan dan pengeluaran non tax (PNBP).</li>
	<li>Lakukan footing (penjumlahan kebawah) dan crossfooting (penjumlahan kesamping) terhadap Buku Kas Umum dan Buku Pembantu lainnya. Bandingkan antara saldo kas menurut BKU dengan jumlah uang tunai yang ada di dalam brankas dan R/C bila ada perbedaan telusuri penyebabnya.</li>
	<li>Teliti rekening bank atas nama Bendahara dan R/C terakhir.</li>
	<li>Teliti adakah pergeseran MAK begitu juga untuk MAK yang penyediaan dananya lebih, untuk menutup MAK yang kurang dan untuk melunasi tagihan.</li>
	<li>Tutup BKU dan dapatkan saldo kas menurut BKU dengan membuat Berita Acara.</li>
	<li>Teliti apakah pemberian Tunjangan Keluarga telah sesuai dengan ketentuan.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('e5f00a396cef35488463f371b289a6506c3d265c', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO002', '277f1441ce02dbf662e6feb6c669b603a4de35d0', 'Perjalanan Dinas', '<ol>
	<li>Teliti daftar nama pegawai yang melakukan perjalanan dinas dengan realisasinya.</li>
	<li>Teliti tarif resmi transport antar kota dan lumpsumnya.</li>
	<li>Teliti prosedur pemrosesan SPPD dan Keuangannya.</li>
	<li>Teliti dokumen perjalanan dinas tiap pegawai periode yang diaudit.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('aff52852a70273f85b8a6ef2fb96891ed9aa8ff6', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO003', '277f1441ce02dbf662e6feb6c669b603a4de35d0', 'Pertanggung Jawaban', '<ol>
	<li>Teliti pertanggung jawaban administrasi keuangan yang dibuat oleh Bendahara apakah telah sesuai dengan ketentuan yang berlaku.</li>
	<li>Teliti&nbsp; sistem laporan keuangan dan dokumen pengelolaan keuangan apakah telah sesuai dengan ketentuan yang berlaku.</li>
	<li>Teliti pertanggungjawaban pengelolaan keuangan dan administrasi yang dilaksanakan oleh KPA.</li>
	<li>Teliti honor pengelola anggaran.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('279cc6f919cc049c27b696579f26b45cb689fa45', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO004', '4db91f046b49f91c14736ad0e82d31c931ce475c', 'Tanah, Gedung dan Taman Alat', '<ol>
	<li>Teliti dokumen kepemilikan &amp; fisik tanah</li>
	<li>Teliti standard dan hambatan terhadap taman alat/ gedung tempat alat.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('8b01613ac18c52ee0bd02b18fe51a8d69c1f8437', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO005', '4db91f046b49f91c14736ad0e82d31c931ce475c', 'Komunikasi', '<ol>
	<li>Teliti komunikasi auditan dengan pihak terkait.</li>
	<li>Teliti jenis-jenis komunikasi dengan pihak lain dalam penyampaian data.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('560ec571168d5cfc432c05803d85de0771d83ede', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO006', '4db91f046b49f91c14736ad0e82d31c931ce475c', 'Peralatan Operasional & Non Operasional', '<ol>
	<li>Teliti dokumen daftar SIMAK-BMN yang dimiliki Auditi, apakah sesuai ketentuan.</li>
	<li>Teliti penatausahaan peralatan operasional dan apakah telah dibukukan sesuai ketentuan</li>
	<li>Bandingkan peralatan yang dibukukan dengankenyataan di lapangan.</li>
	<li>Teliti rencana pengadaan barang/ peralatan/ pemeliharaan, apakah sudah sesuai dengan alokasi dana dalam RPU, Rincian Penggunaan dana untuk keperluan riil selama 1 bulan serta surat pernyataan Penggunaan Dana.</li>
	<li>Teliti Kalibrasi peralatan operasional.</li>
	<li>Teliti pengelolaan SIMAK-BMN&nbsp; apakah telah dilaporkan secara periodik sesuai ketentuan yang berlaku (intra dan ektra komptabel)</li>
	<li>Teliti apakah setiap penerimaan barang/ pekerjaan yang sudah selesai, selalu dibuat kan Berita Acara Penerimaan Barang/ Penyelesaian Pekerjaan dan dilak kan uji petik.</li>
	<li>Teliti sarana dan prasarana berlebih atau tidak dapat digunakan lagi dipindah tangankan atau dihapuskan sesuai ketentuan yang berlaku</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('6f9fe9a231084a247ed07c34c4c3391a65a7ad04', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO007', '5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'Perencanaan', '<ol>
	<li>Teliti jumlah dan kompetensi pegawai, apakah telah sesuai dengan beban kerja.</li>
	<li>Teliti penempatan pegawai, apakah telah sesuai rencana dengan kompetensinya.</li>
	<li>Teliti, apakah perencanaan dan&nbsp; pengembangan karier yang mengacu pada peraturan jenjang karir PNS yang berlaku telah di buat secara transparan</li>
	<li>Teliti dan Evaluaasi kebutuhan jabatan fungsional.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('63642bf948a2d038ef75530bf5e4b6d76d512a62', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO008', '5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'Pengadaan dan Penempatan', '<ol>
	<li>Teliti pengadaan pegawai baru telah sesuai dengan peraturan yang berlaku dan terdapat unsur KKN.</li>
	<li>Teliti penerimaan pegawai baru telah sesuai dengan formasi yang di tetapkan.</li>
	<li>Teliti penempatan pegawai telah sesuai dengan kompetensinya.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('d02cf622ee99c030cf2c6d3c1334da1abc7a4f87', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO009', '5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'Penilaian dan Penegakkan Disiplin', '<ol>
	<li>Teliti tingkat kedisiplinan pegawai dan apakah pelanggaran disiplin yang dilakukan telah diberi sanksi sesuai ketentuan yang berlaku.</li>
	<li>Teliti apakah dasar penyusunan DP3 telah sesuai dengan ketentuan.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('6a75dddf76980809a88d3859024d20aa9ccca691', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO010', '5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'Mutasi dan Kenaikan Pangkat', '<ol>
	<li>Teliti apakah terjadi Back Log Kenaikan pangkat.</li>
	<li>Teliti formasi jabatan struktural</li>
	<li>Teliti apakah terdapat pegawai yang pangkatnya lebih tinggi dari atasannya.</li>
	<li>Teliti mutasi jabatan/tenaga fungsional telah sesuai ketentuan yang berlaku</li>
	<li>Teliti apakah kenaikan pangkat pejabat fungsional telah sesuai dengan ketentuan.</li>
	<li>Teliti apakah tim penilai angka kredit telah melaksanakan tugasnya.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('cbc58a363109e93cf951958096411375c5b5f3cb', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO011', '5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'Kesejahteraan, Pemberhentian dan Pensiun', '<ol>
	<li>Teliti pegawai yang belum mendapat penghargaan dari pemerintah sesuai ketentuan.</li>
	<li>Teliti pemberhentian Pegawai, apakah telah dilaksanakan sesuai ketentuan yang berlaku</li>
	<li>Teliti pensiun Pegawai / Janda atau Duda telah diberikan tepat waktu sesuai ketentuan yang berlaku</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('9688c1f7f3b059ccdae654c27bad4ab90189f228', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO012', '5af063d9b993c0c43b38e34c80fe60e94189c3ff', 'Penatausahaan Kepegawaian', '<ol>
	<li>Teliti penyusunan dan pengiriman daftar Nominatif Pegawai &amp; DUK</li>
	<li>Teliti pengelolaan data kepegawaian (Karis, Karsu, Karpeg, dll.)</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('7d98203937109e97febe79eebc4325d0eab4dfaa', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO013', '8134168f4fda3dafeb174cb7230aca8ce77c2f8b', 'Struktur Organisasi', '<ol>
	<li>Teliti uraian dan pembagian tugas pegawai,</li>
	<li>Teliti apakah terdapat kegiatan tambahan dalam struktur organisasi,</li>
	<li>Teliti apakah tugas pokok sudah dijabarkan menurut bidangnya,</li>
	<li>Teliti apakah Auditan telah menyelenggarakan pengamatan, pengumpulan dan penyebaran, pengolahan data, analisa dan prakiraan cuaca, pelayanan Jasa Meteorologi/ Klimatologi dan Kualitas udara/ Geofisika.</li>
	<li>Teliti apakah Auditi telah menyusun rencana dan program kerja serta laporan tahunan.</li>
	<li>Teliti apakah kelompok jabatan fungsional dan fungsional umum (TU) telah melaksanakan tugas sesuai dengan TUPOKSI-nya.</li>
	<li>Teliti apakah ada kerjasama dengan instansi luar (misal : SMPK, Stasiun Penguapan, Stasiun Hujan dll.) didalam wilayah kerja termasuk pengumpulan datanya&nbsp;</li>
	<li>Teliti apakah ada kegiatan pengendalian / pengawasan melekat dan bagaimana cara pelaksanaannya.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('c69be875fcc43892303207ad17b82079c6f35d34', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO014', '6d46ae583029560f8debf0fce2036586e97cea7a', 'Pahami SPI yang ada dengan cara:', '<ol>
	<li>Peninjauan lapangan<em> (<strong>on site tour</strong>),</em> melihat dari dekat secara sepintas sumber daya yg dimiliki auditi.</li>
	<li>Pelajari Dokumen yang ada, untuk mendeteksi perubahan2 sistem pengendalian yg telah dilakukan dan tidak terdeteksi dalam permanent file.</li>
	<li>Menulis uraian kegiatan auditi<strong><em>, </em></strong>misalnya tentang: &nbsp;&nbsp;</li>
</ol>

<ul>
	<li>Tujuan kegiatan operasional auditi,</li>
	<li>Batasan2 lingkungan,</li>
	<li>Fungsi2 dari komponen2 dalam organisasi,</li>
	<li>sumberdaya (jumlah dan klasifikasi tenaga kerja, arus kas, tanah, gedung dan peralatan, sistem informasi sb),</li>
	<li>Aspek manajemen.</li>
</ul>', '1')
<query>INSERT INTO ref_program_audit VALUES ('7efbd2bac1a10487907bb92ab61907443bd6c01f', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO015', '6d46ae583029560f8debf0fce2036586e97cea7a', 'Lakukan prosedur analisis terhadap kondisi SPI auditan melalui', '<ol>
	<li>Bagan arus (flow chart), menyajikan kegiatan operasional suatu sistem pengendalian dalam bentuk grafis menggunakan simbol-simbol<strong>.</strong></li>
	<li>Narasi</li>
	<li>Internal control questionaire (ICQ) yang diteliti dan disusun oleh auditor untuk dijawab oleh pejabat auditi/dijawab sendiri oleh auditor berdasarkan hasil observasi, analisis dan pengujian dokumen terhadap unsur-unsur:</li>
</ol>

<ul>
	<li>Lingkungan Pengendalian</li>
	<li>Penilaian Risiko</li>
	<li>Kegiatan Pengendalian</li>
	<li>Informasi dan Komunikasi</li>
	<li>Pemantauan</li>
</ul>', '1')
<query>INSERT INTO ref_program_audit VALUES ('0338356e7c15f2dc3b913edb69113aa590aabade', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO016', '6d46ae583029560f8debf0fce2036586e97cea7a', 'Bandingkan antara kondisi pengendalian dengan pengendalian kunci dan teliti apakah terjadi kesenjangan,', '<ol>
	<li>Lingkungan Pengendalian</li>
	<li>Penilaian Risiko</li>
	<li>Kegiatan Pengendalian</li>
	<li>Informasi dan Komunikasi</li>
	<li>Pemantauan</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('0e078f925eab33b54b6c382d712344f2f8263879', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO017', '6d46ae583029560f8debf0fce2036586e97cea7a', 'Lakukan pengujian terbatas atas pelaksanaan SPI dan identifikasikan akibat potensial yg mungkin timbul dari kelemahan SPI tsb serta unsur pengendalian yang diperlukan untuk menutupi kelemahan tsb.', '<ol>
	<li>Pengendalian Preventive</li>
	<li>Pengendalian Detectif</li>
	<li>Pengendalian Korektiv</li>
	<li>Pengendalian langsung</li>
	<li>Pengendalian konvrensative<strong>.</strong></li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('636a403e52e27dc2df6cc87fe43e22195fff61ac', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO018', 'e7ee08c5cb7ef028130b0a7d0a0209ba4da86628', 'Dapatkan (kumpulkan) Informasi mengenai gambaran umum Auditi, seperti:', '<ol>
	<li>Visi, Misi dan Strategi Auditi.</li>
	<li>Peraturan perundang-undangan yang menjadi dasar organisasi Auditi.</li>
	<li>Kebijakan-kebijakan dan pedoman peraturan internal.</li>
	<li>Lingkungan internal, eksternal, pemangku kepentingan dan pihak terkait lainnya.</li>
	<li>Tugas Pokok dan Fungsi Auditi.</li>
	<li>Struktur Organisasi dan personil yang terlibat didalamnya dan <em>Job Description</em>.</li>
	<li>Anggaran, realisasi dan/ atau laporan kinerja lainnya.</li>
	<li>Pedoman dan petunjuk pelaksanaan tentang Sistem Informasi Manajemen.</li>
	<li>Hasil Audit Periode Sebelumnya baik dari internal maupun eksternal.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('6b84f3384536c68b44be40e05d68778075b440ae', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO019', 'e7ee08c5cb7ef028130b0a7d0a0209ba4da86628', 'Pahami dan telaah Tugas Pokok dan Fungsi (Tupoksi) Auditi', '<ol>
	<li>Dapatkan gambaran secara utuh kegiatan utama Auditi.</li>
	<li>Buat Peta (<em>road map</em>) atas kegiatan utama tersebut.</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('0a43982d248c51d494724beb0d6da0aebbc657f4', '9a5725d64d7c8ff7822e7f87d41c42a06a0cf9fb', 'AO020', 'd620b3fd0164fa5eb03125e497e9aca5f5da3f7a', 'Langkah Kerja Audit', '<ol>
	<li>Identifikasi dokumen kontrak pekerjaan yyyy</li>
	<li>Lakukan analisis atas standar mutu hasil kerja (output) dari tiap-tiap pekerjaan utama dari tiap bagian/fungsi,</li>
	<li>Lakukan pembandingan antara output yang dihasilkan dengan standar mutu,</li>
	<li>Lakukan konfirmasi kepada pejabat yang berwenang terhadap adanya perbedaan kualitas,</li>
	<li>Dst .......( sesuai kebuthan) &nbsp;</li>
	<li>Buatlah simpulan</li>
</ol>', '1')
<query>INSERT INTO ref_program_audit VALUES ('6a07b84fdf890def594e18f3efb39e18dcf954f5', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP001', 'cd4a7c581d450985ff742da4320c12f3bccf3068', 'Penetapan Harga Perhitungan Sendiri Pengadaan Barang', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>1</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan HPS yang dibuat PPK. (HPS dapat dibuat sendiri oleh PPK, atau berdasarkan EE yang disusun oleh konsultan perencana, setelah di reviu kembali oleh Pokja ULP/ Pejabat Pengadaan)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>2</p>
			</td>
			<td style="width:260px">
			<p>Teliti penetapan volume yang digunakan dalam perhitungan HPS, terutama koefisien-koefisien upah, bahan dan peralatan apakah telah wajar atau telahberdasarkan bukti yang dapat dipertanggungjawabkan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>3</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah penetapan harga dalam perhitungan HPS telah didasarkan pada harga setempat menjelang diadakanproses pengadaan (HPS disusun paling lama 28 (dua puluh delapan) hari kerja sebelum batas akhir pemasukan penawaran), informasi biaya satuan yang dipublikasikan secara resmi (BPS)/ asosiasi terkait dan data lain yang dapat dipertanggungjawabkan, daftar biaya/tarifbarang/jasa yang dikeluarkan oleh agen tunggal/pabrikan, biaya kontrak sebelumnya(yang sedang berjalan) dan daftar biaya standar yang dikeluarkan oleh instansi yang berwenang.&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>4</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan keabsahan referensi- referensi penyusunan HPS yang diberikan auditan. (Bisa terjadi bahwa referensi atau daftar harga supplier yang digunakan auditan menyusun HPS tidak benar/Palsu)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>5</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa spesifikasi barang/peralatan dengan harga referensi yang digunakan oleh auditan dalam penyusunan HPS telah sesuai dengan spesifikasi yang akan ditawarkan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>6</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa HPS yang ditetapkan sudah dijadikan acuan dalam menentukan Jaminan Pelaksanaan.</p>

			<p>Catatan: Apabila harga penawaran dari penyedia barang/jasa yang akan ditetapkan menjadi pemenang berada di bawah 80% HPS, maka jaminan pelaksanaan harus dihitung minimal sebesar 5% dari total HPS.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>7</p>
			</td>
			<td style="width:260px">
			<p>Teliti RAB HPS apakah telah disusun berdasarkan PO</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>8</p>
			</td>
			<td style="width:260px">
			<p>Buat Simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('e5c9f28e04f8070436209150e683a7b899d07718', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP002', 'cd4a7c581d450985ff742da4320c12f3bccf3068', 'Penetapan Harga Perhitungan Sendiri Jasa Konsultansi', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>9</p>
			</td>
			<td style="width:260px">
			<p>Uji data yang diuraikan dalam <em>curriculum vitae (CV)</em> tenaga konsultan, a.l. dengan meminta salinan ijazah, dan meneliti pengalaman substansialnya yang didukung dengan referensi dari pengguna jasa sebelumnya</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>10</p>
			</td>
			<td style="width:260px">
			<p>Uji <em>billing rate</em> yang ditetapkan dalam kontrak dengan ketentuan yang berlaku (apabila ada).</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>11</p>
			</td>
			<td style="width:260px">
			<p>Bandingkan biaya personil dalam kontrak dengan biaya personil yang diatur dalam TOR.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>12</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa biaya non personil yang dibebankan telah sesuai (diperbolehkan) dalam ketentuan kontrak. Uji apakah dalam biaya non personil terdapat biaya pembelian/pengadaan barang. Jika ada, teliti apakah telah dilaksanakan secara efisien dan ekonomis, sesuai dengan ketentuan dalam Keppres No. 80 tahun 2003, (Misalnya melalui perbandingan harga diantara beberapa penawar).</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>13</p>
			</td>
			<td style="width:260px">
			<p>Periksa nama-nama personil yang ditetapkan dalam kontrak, bandingkan dengan daftar hadir konsultan. Uji apakah daftar hadir tersebut dapat diandalkan (<em>reliable</em>). Apabila ada penggantian, teliti apakah personil penggantinya minimal setara dengan personil yang dicantumkan.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>14</p>
			</td>
			<td style="width:260px">
			<p>Periksa kontrak lain yang ditandatangani oleh konsultan dengan proyek lain dengan waktu yang bersamaan dan teliti apakah personil-personil yang terdapat dalam kontrak tidak terdapat dalam kontrak lain tersebut.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>15</p>
			</td>
			<td style="width:260px">
			<p>Periksa bukti realisasi pembayaran kepada konsultan, dan teliti apakah pembayaran tersebut telah sesuai dengan prestasi yang dilaksanakan, termasuk <em>progress report.</em></p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>16</p>
			</td>
			<td style="width:260px">
			<p>Teliti kebenaran daftar kehadiran konsultan di tempat pekerjaan, dan membandingkan dengan daftar hadir rapat-rapat konsultansi dan dalam kegiatan lain yang berkaitan dengan kontrak yang diperiksa.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>17</p>
			</td>
			<td style="width:260px">
			<p>Periksa apakah terdapat penggantian tenaga konsultan selama masa pelaksanaan kontrak konsultan baik yang diajukan oleh penyedia jasa maupun pengguna jasa. Bila ada, periksa apakah penggantian&nbsp; tersebut tidak mengakibatkan kualifikasi tenaga konsultan lebih rendah daripada yang diganti, serta adanya penambahan biaya.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>18</p>
			</td>
			<td style="width:260px">
			<p>Bandingkan jangka waktu kontrak jasa konsultansi dengan jangka waktu kontrak jasa konstruksi yang diawasi. Kaitkan setiap <em>progress</em> kemajuan bulanan pekerjaan konstruksi yang diawasi dengan jumlah pembayaran bulanan kepada konsultan pengawas. (Pada bulan-bulan tertentu <em>progress </em>pekerjaan konstruksi nol persen, namun pembayaran atas <em>man month</em> konsultan tetap penuh)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>19</p>
			</td>
			<td style="width:260px">
			<p>Buat Simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('10e250595e3f6c15ddeeb0fafa9a4a6fa01c5d00', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP003', 'cd4a7c581d450985ff742da4320c12f3bccf3068', 'Kewajaran Harga', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>20</p>
			</td>
			<td style="width:260px">
			<p>Evaluasi apakah harga tidak melewati pagu anggaran</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>21</p>
			</td>
			<td style="width:260px">
			<p>Evaluasi kewajaran harga kontrak apabila HPS tidak dipakai sebagai acuan (tidak wajar)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>22</p>
			</td>
			<td style="width:260px">
			<p>Periksa kebenaran pendekatan perhitungan biaya/unit price dihubungkan dengan metode kerja sesuai spesifikasi teknis yang ditentukan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>23</p>
			</td>
			<td style="width:260px">
			<p>Apabila terdapat perbedaan metode pelaksanaan pekerjaan antara yang ditetapkan dalam dokumen lelang/kontrak dengan realisasi pelaksanaan dilapangan, teliti apakah perubahan tersebut merupakan rekayasa antara PPK dan Penyedia Barang/ Jasa untuk meninggikan harga satuan HPS atau tidak. (Contoh: dalam dokumen lelang disebut pekerjaan harus menggunakan alat berat sehingga harga satuan HPS dan penawaran tinggi namun dalam pelaksanaan cukup dengan tenaga manusia dengan biaya yang lebih murah atau sebaliknya&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>24</p>
			</td>
			<td style="width:260px">
			<p>Periksa realisasi pemakaian bahan, upah dan peralatan berdasarkan dokumen yang dibuat selama pelaksanaan pekerjaaan baik oleh penyedia, PPK maupun konsultan seperti buku harian, laporan harian, back up data dsb. Hasilnya bandingkan dengan volume yang dipergunakan untuk menghitung harga kontrak (bill of quantity)&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>25</p>
			</td>
			<td style="width:260px">
			<p>Teliti perhitungan analisa harga satuan apakah terdapat komponen bahan yang dijadikan dasar untuk menghitung harga satuan dimana bahan tersebut tidak ada atau sulit didapatkan didaerah pelaksanaan dengan tujuan menaikkan harga satuan, namun dalam pelaksanaannya akan diganti dengan bahan yang harganya lebih murah dan biasa digunakan masyarakat (tersedia di toko/ daerah setempat) &nbsp;&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>26</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah dalm pelaksanaan terdapat peralatan yang disediakan oleh PPK (tanpa dipungut biaya), tetapi biaya atas peralatan tersebut dibebankan juga dalam perhitungan harga satuan kontrak</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>27</p>
			</td>
			<td style="width:260px">
			<p>Lakukan konfirmasi kepada supplier penyedia barang/ jasa untuk mengidentifikasi kemungkinan terjadinya KKN dalam menetapkan harga kontrak dan memastikan harga pengadaan barang/jasa tidak dimark up. Apabila terjadi indikasi penyimpangan yang disebabkan adanya mark up harga KKN, agar diperdalam dengan mengumpulkan bukti-bukti yang cukup.&nbsp;&nbsp; &nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>28</p>
			</td>
			<td style="width:260px">
			<p>Atas adanya pekerjaan tambah kurang, pastikan bahwa telah dibuatkan addendum tambah kurang, dan teliti kewajaran volume dan harganya serta pastikan bahwa nilai/harga pekerjaan tambah kurang, <u>tidak melebihi</u> 10% dari nilai kontrak</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>29</p>
			</td>
			<td style="width:260px">
			<p>Apabila pekerjaan dipersyaratkan dalam kontrak untuk diasuransikan oleh penyedia B/J, teliti apakah telah diasuransikan dan dapatkan copy polis dan bukti pembayaran preminya</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>30</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('a025acc1f1dd36b24939a36b67a56b13a61bcd28', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP004', 'cd4a7c581d450985ff742da4320c12f3bccf3068', 'Pajak dan PNBP', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>31</p>
			</td>
			<td style="width:260px">
			<p>Identifikasi jenis pajak/PNBP&nbsp; atas kontrak pengadaan B/J. Perlu dicermati bahwa dalam beberapa dokumen kontrak tidak disebut secara spesifik pajak-pajak yang harus dipungut pemberi kerja</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>32</p>
			</td>
			<td style="width:260px">
			<p>Periksa dokumen pembayaran kontrak dan dokumen penyetoran pajak/PNBP untuk memastikan semua transaksi yang seharusnya dibebankan pajak/PNBP sudah dipungut dan disetorkan ke Kas Negara, termasuk biaya penyelenggaraan dan pembuatan dokumen yang dipungut pada saat pengambilan dokumen. Bila perlu lakukan konfirmasi ke Bank persepsi</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>33</p>
			</td>
			<td style="width:260px">
			<p>Periksa kesesuaian pengenaan tarif atas pajak/PNBP</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>34</p>
			</td>
			<td style="width:260px">
			<p>Lakukan prosedur lainnya untuk memastikan tidak terdapat pajak/PNBP yang belum dipungut dan atau disetor ke Kas Negara. Jika ada&nbsp; tetapkan nilainya.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>35</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('02cccb821b4aef56aa1b64f503c4148e2642607b', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP005', '60db6ee43d3d93ea44b62a93ddfa0515badda1ed', 'Langkah Kerja', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>1</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan dan teliti BA penyelesaian pengadaan Barang/ Jasa dan bandingkan dengan kontrak.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>2</p>
			</td>
			<td style="width:260px">
			<p>Lakukan pengujian/ perhitungan fisik untuk memastikan bahwa jumlah barang yang diterima/ volume konstruksi telah sesuai kontrak. Pengujian fisik/ perhitungan dituangkan dalam Berita Acara Pemeriksaan Fisik yang ditandatangani oleh Auditor dan PPK</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>3</p>
			</td>
			<td style="width:260px">
			<p>Tetapkan ada tidaknya kekurangan kuantitas/volume barang/ jasa. Jika terjadi kekurangan, tentukan jumlahnya termasuk nilai rupiah berdasarkan harga satuan dalam kontrak dikalikan volume pekerjaan yang kurang. Disamping itu hitung juga denda keterlambatan sebesar satu&nbsp; permil perhari.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>4</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah terdapat bagian pekerjaan utama yang dialihkan (disubkontrakkan) kepada pihak lain. Pekerjaan yang dapat disubkontrakkan adalah bukan pekerjaan utama dan diutamakan kepada penyedia barang/ jasa kecil, namun tanggung jawab tetap kepada penyedia barang/ jasa utama.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>5</p>
			</td>
			<td style="width:260px">
			<p>Apabila terdapat pekerjaan tambah/kurang, teliti sebab-sebab adanya pekerjaan tambah/kurang tersebut, dan yakinkan bahwa pekerjaan tambah/kurang tersebut telah didasarkan pada kondisi yang dapat dipertanggungjawabkan/ wajar, antara lain: telah sesuai dengan persyaratan dalam kontrak, telah di dukung dengan addendumnya dan lakukan pemeriksaan fisiknya.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>6</p>
			</td>
			<td style="width:260px">
			<p>Lakukan Konfirmasi (kepada supplier dari penyedia, pemakai atau pihak lain) untuk meyakini bahwa kuantitas telah sesuai dengan kontrak dan dokumen pendukungnya</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>7</p>
			</td>
			<td style="width:260px">
			<p>Lakukan prosedur lainnya untuk memastikan pengadaan B/J telah sesuai kontrak</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>8</p>
			</td>
			<td style="width:260px">
			<p>Teliti saat (tanggal-tanggal) dan jumlah tahapan pembayaran, kemudian bandingkan dengan saat (tanggal-tanggal) atau tingkat progress pengadaan barang/jasa, untuk meyakinkan bahwa pembayaran (termasuk termin) telah didasarkan pada saat dan tingkat kemajuan fisik pekerjaan (progress report), serta tata cara pembayaran yang tercantum dalam kontrak.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>9</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah terdapat pemblokiran dana untuk menghindari hangusnya anggaran dengan cara membuat BA kemajuan pekerjaan yang tidak benar(Biasanya dilakukan menjelang akhir tahun anggaran)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>10</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan transaksi pembayaran dan lakukan pengujian bahwa pembayaran tersebut benar dan diterima oleh rekanan terkait, serta bandingkan antara invoice dan dokumen penerimaan untuk mendeteksi adanya tagihan palsu</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>11</p>
			</td>
			<td style="width:260px">
			<p>Apabila barang berasal dari luar negeri (impor), dapatkan PIB atas barang tersebut, baik dari penyedia barang/ jasa maupun pihak lain seperti KPBC</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>12</p>
			</td>
			<td style="width:260px">
			<p>Buat Simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('99adb82e3552bb8d4b824989dcb4867a32887e45', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP006', '2bb0db84a9b8fe6868308a4a186ef79b82ec1ceb', 'Pedoman Pengadaan Barang/ Jasa pada Satuan Kerja', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>1</p>
			</td>
			<td style="width:260px">
			<p>Teliti dan telaah pedoman Pengadaan Barang/ Jasa yang diberlakukan pada Satuan Kerja telah sesuai Perpres nomor 4 tahun 2016 tentang perubahan keempat Pengadaan Barang/ Jasa</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>2</p>
			</td>
			<td style="width:260px">
			<p>Teliti dan telaah Pedoman Pengadaan Barang/ Jasa yang diberlakukan pada Satuan Kerja apakah</p>

			<ol>
				<li>Telah menerapkan prinsip efisiensi, efektivitas, terbuka dan bersaing, transparan, adil/ tidak diskriminatif dan akuntabel</li>
				<li>Telah mengutamakan kebijakan umum pemerintah dalam Pengadaan Barang/ Jasa:</li>
			</ol>

			<ul>
				<li>Penggunaan Produksi dalam negeri, dengan sasaran perluas kesempatan lapangan kerja,</li>
				<li>Penggunaan peran serta usaha mikro dan usaha kecil serta koperasi kecil,</li>
				<li>Menyederhanakan ketentuan dan tata cara untuk mempercepat proses keputusan Pengadaan Barang/ Jasa</li>
				<li>Meningkatkan profesionalisme, kemandirian, dan tanggung jawab dari PA/KPA, PPK, Pokja ULP/ Pejabat Pengadaan, Panitia/ Pejabat Penerima Hasil Pekerjaan</li>
				<li>Menumbuhkembangkan peran serta usaha nasional</li>
				<li>Dan keharusan melakukan pengumuman secara terbuka, rencana pengadaan pada setiap awal pelaksanaan anggaran kepada masyarakat luas melalui SIRUP, kecuali yang bersifat rahasia&nbsp;&nbsp;</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>3</p>
			</td>
			<td style="width:260px">
			<p>Jika Satuan Kerja tidak mempunyai pedoman pengadaan barang Jasa, Auditor melaksanakan prosedur audit didasarkan pada ketentuan perundang-undangan yang berlaku</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>4</p>
			</td>
			<td style="width:260px">
			<p>Buat Simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('d76d42a4d9a072e5ef7f7f4a88505ea6a1f46c4b', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP007', '2bb0db84a9b8fe6868308a4a186ef79b82ec1ceb', 'Kebutuhan Pengadaan Barang/ Jasa', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>5</p>
			</td>
			<td style="width:260px">
			<p>Teliti ada tidaknya permintaan barang/ jasa dari pemakai (<em>user</em>)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>6</p>
			</td>
			<td style="width:260px">
			<p>Bandingkan usulan pengadaan barang/ jasa dengan rencana kerja tahunan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>7</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan studi kelayakan (<em>feasibility study) </em>atau hasil survei dan design atau dokumen sejenis yang berkaitan dengan pengadaan barang/ jasa yang dilaksanakan.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>8</p>
			</td>
			<td style="width:260px">
			<p>Teliti Studi kelayakan tersebut untuk mengetahui tujuan pengadaannya, kuantitas, kualitas, serta waktu (saat) dibutuhkan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>9</p>
			</td>
			<td style="width:260px">
			<p>Teliti Dokumen pengadaan barang/ jasa yang dilaksanakan untuk mengetahui kuantitas, kualitas, serta waktu penyelesaiannya</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>10</p>
			</td>
			<td style="width:260px">
			<p>Bandingkan Informasi yang diperoleh dari studi kelayakan terebut dengan informasi yang diperoleh dari dokumen pengadaan barang/ jasa, untuk mengetahui apakah:</p>

			<ul>
				<li>pengadaan barang/ jasa telah berdasarkan kebutuhan</li>
				<li>kuantitas, kualitas, serta waktu penyelesaiannya telah sesuai dengan yang dibutukan</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>11</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('ac35b29128646c006958e2ec8a2db4504906ff3d', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP008', '2bb0db84a9b8fe6868308a4a186ef79b82ec1ceb', 'Pembiayaan dan Jadwal Pelaksanaan Pengadaan Barang/ Jasa', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>12</p>
			</td>
			<td style="width:260px">
			<p>Teliti/ telaahdan yakinkan bahwa unsur biaya dan nilai pengadaan barang/ jasa telah termasuk dalam DIPA, minimal terdiri dari:</p>

			<ol>
				<li>Biaya Administrasi untuk mendukung pelaksanaan pengadaan barang/ jasa antara lain:</li>
			</ol>

			<ul>
				<li>Honorarium PA/KPA, PPK, Pokja ULP/ Pejabat Pengadaan, Panitia/ Pejabat Penerima Hasil Pekerjaan, termasuk tim teknis, tim pendukung dan staf proyek</li>
				<li>Biaya Penggandaan dokumen pengadaan barang/ jasa</li>
				<li>Administrasi lainnya yang diperlukan untuk mendukung pelaksanaan pengadaan barang/ jasa</li>
			</ul>

			<ol>
				<li>&nbsp;Biaya/ nilai besaran pengadaan barang/ jasa itu sendiri</li>
			</ol>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>13</p>
			</td>
			<td style="width:260px">
			<p>Periksa DIPA masing-masing satuan kerja atas kontrak yang diaudit, catat tanggal diterimanya DIPA serta uji kesesuaian dengan jadwal pelaksanaan pengadaan barang/ jasa,termasuk pengaruhnya kepada saat mulainya proses pengadaan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>14</p>
			</td>
			<td style="width:260px">
			<p>Telaah alokasi waktu (proses pelelangan)yang dibuat Pokja ULP dan yakinkan bahwa alokasi tersebut telah sesuai dengan ketentuan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>15</p>
			</td>
			<td style="width:260px">
			<p>Teliti dan Telaah Anggaran Belanja Tambahan (ABT) dan catat tanggal diterima serta persyaratannya untuk mengetahui jadwal pelaksanaan dan pengaruhnya pada penyelesaian pekerjaan dengan tenggang waktu tersedia apakah penggunaan ABT tersebut masih realistis atau tidak?</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>16</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('2f2e3b7546d9790f789ffbc8e2a2638b2e1484a1', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP009', '8ebf1eace7c59cc969fbd0734c195aeeeaf9a7b0', 'Pejabat Pembuat Komitmen, Pokja ULP/ Pejabat pengadaan dan Panitia/ Pejabat Penerima Hasil Pekerjaan', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>1</p>
			</td>
			<td style="width:260px">
			<p>Teliti dan telaah, serta pastikan bahwa personil yang terlibat dalam struktur organisasi Pejabat Pembuat Komitmen, Pokja ULP/ Pejabat Pengadaan dan Panitia/ Pejabat Penerima Hasil Pekerjaan tidak terlibat kepentingan:</p>

			<ul>
				<li>Pejabat Pembuat Komitmen tidak boleh merangkap sebagai Pokja ULP/ Pejabat Pengadaan dan Pengelola Keuangan.</li>
				<li>pegawai dari Inspektorat yang menjadi panitia untuk instansinya,&nbsp;</li>
				<li>Pokja ULP/Pejabat Pengadaan tidak boleh mempunyai hubungan keluarga (sedarah dan semenda) dengan pejabat yang menetapkan Pokja ULP/Pejabat Pengadaan</li>
				<li>Panitia/ Pejabat Penerima Hasil Pekerjaan tidak menjabat sebagai Pengelola Keuangan</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>2</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa Pembuat Komitmen dan Pokja ULP/ pengadaan BARANG/ JASAtelah memiliki <em>Sertifikat Keahlian </em>Pengadaan Barang/ Jasa</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>3</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan pakta integritas dan yakinkan bahwa Pejabat Pembuat Komitmen, Pokja ULP/ Pejabat Pengadaan dan Penyedia Pengadaan Barang Jasa telah menandatangani pakta integritas tersebut.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>4</p>
			</td>
			<td style="width:260px">
			<p>Pastikan bahwa Pejabat Pembuat Komitmen, Pokja ULP/ Pejabat Pengadaan dan Panitia/ Pejabat Penerima Hasil Pekerjaan telah mematuhi etika pengadaan sebagaimana diatur dalam ketentuan perundangan yang berlaku, al:</p>

			<ul>
				<li>dilakukan secara tertib dan bertanggungjawab, profesional dan mandiri atas dasar kejujuran,</li>
				<li>tidak saling mempengaruhi serta mencegah/ menghindari pertentangan kepentingan para pihak yang terkait,</li>
				<li>hindari hal-hal yang berindikasi KKN.</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>5</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah keputusan-keputusan yang diambil oleh Pejabat Pembuat Komitmen, Pokja ULP/ Pejabat Pengadaan dan Panitia/ Pejabat Penerima Hasil Pekerjaan telah bebas dari pengaruh pejabat pembuat komitmen atau pimpinan/pejabat yang lebih tinggi.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>6</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah keputusan-keputusan yang diambil oleh Pejabat pembuat komitmen telah bebas dari pengaruh pimpinan/pejabat yang lebih tinggi.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>7</p>
			</td>
			<td style="width:260px">
			<p>Teliti mekanisme kerja panitia</p>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>8</p>
			</td>
			<td style="width:260px">
			<p>Buat Simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('1d59712e324b74540dcdb547ad9194a41d289261', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP010', '8ebf1eace7c59cc969fbd0734c195aeeeaf9a7b0', 'Prosedur Pelaksanaan Pemilihan Penyedia Barang/ Jasa', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>9</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan pemilihan penyedia Barang/Jasa telah menggunakan Sistem Pengadaan Secara Elektronik (Pelelangan Umum, Pelelangan Terbatas, Pemilihan Langsung, atau Penunjukan Langsung). Lakukan cek ke Website lpse.bmkg.go.id/</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>10</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa metode pemilihan penyedia Barang/Jasa yang dipilih (apakah itu melalui pelelangan umum, pelelangan terbatas, pemilihan langsung, atau penunjukan langsung) telah tepat</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>11</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan kelengkapan&nbsp; dokumen pengadaan BARANG/ JASAyang terdiri dari:</p>

			<ul>
				<li>Dokumen prakualifikasi/pasca kualifikasi,</li>
				<li>Dokumen pemilihan penyedia BARANG/ JASA (pelelangan),&nbsp; dan</li>
				<li>Dokumen lain yang berhubungan dengan prosedur pemilihan penyedia BARANG/ JASA, antara lain:</li>
			</ul>

			<ul>
				<li>Dokumen Pengumuman Rencana Umum Pengadaan oleh PA/ KPA. Cek di website <a href="https://sirup.lkpp.go.id">https://sirup.lkpp.go.id</a></li>
				<li>Dokumen prakualifikasi seluruh calon penyedia Barang/Jasa, dan undangan bagi peserta yang lolos prakualifikasi. Lakukan cek ke Website lpse.bmkg.go.id/</li>
				<li>Berita Acara pemasukan dokumen, <em>Aanwijzing</em>, pembukaan dokumen penawaran,&nbsp; dan berita acara lain terkait dengan pemilihan calon penyedia BARANG/ JASA. Lakukan cek ke Website lpse.bmkg.go.id/</li>
				<li>Laporan hasil evaluasi panitia pengadaan Barang/Jasa pada tingkat : evaluasi prakualifikasi, evaluasi penawaran dari calon penyedia BARANG/ JASA. Lakukan cek ke Website lpse.bmkg.go.id/</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>12</p>
			</td>
			<td style="width:260px">
			<p>Teliti pengumuman pengadaan Barang/Jasa melalui Website lpse.bmkg.go.id/ apakah telah sesuai dengan telah sesuai dengan ketentuan dan informatif (antara lain nama dan alamat proyek, jenis dan nilai pekerjaan, lokasi pekerjaan, kualifikasi calon rekanan yang diperlukan, dsb)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>13</p>
			</td>
			<td style="width:260px">
			<p>Teliti dan pastikan bahwa <u>dokumen prakualifikasi</u> telah lengkap sesuai dengan ketentuan, sekurang-kurangnya berisi:</p>

			<ul>
				<li>lingkup pekerjaan, persyaratan peserta, waktu &amp; tempat pengambilan/pemasukan dokumen prakualifikasi, serta penanggung jawab prakualifikasi.</li>
				<li>Persyaratan peserta, minimal antara lain:</li>
			</ul>

			<ol>
				<li>Khusus untuk pengadaan Pekerjaan Konstruksi dan Jasa Lainnya harus memperhitungkan Sisa Kemapuan Paket (SKP) = Kemampuan Paket (KP) &ndash; Paket yang sedang dikerjakan (P)</li>
			</ol>

			<ul>
				<li>Untuk usaha kecil, nilai kemampuan paket (KP) ditentukan sebanyak 5 (lima) paket pekerjaan dan</li>
				<li>untuk usaha non kecil nilai kemampuan paket (KP) ditentukan sebanyak 6 (enam) paket atau 1,2 N (jumlah paket pekerjaan terbanyak yang dapat ditangani selama kurun waktu 5 (lima) tahun terakhir. Pekerjaan Konstruksi KD = 3 NPt dan untuk&nbsp; Jasa Lainnya KD = 5 NPt (NPt untuk 10 tahun terakhir)</li>
			</ul>

			<ul>
				<li>Tidak dalam pengawasan pengadilan, tidak pailit, kegiatan tidak sedang dihentikan, direksi tidak sedang dalam menjalani sanksi pidana,</li>
			</ul>

			<ul>
				<li>Sebagai wajib pajak sudah memenuhi kewajiban perpajakan tahun terakhir (lampirkan fotocopy SPT tahun terakhir, dan SSP PPh pasal 29).</li>
				<li>Dalam kurun waktu empat tahun terakhir pernah memperoleh pekerjaan menyediakan Barang/Jasa di lingkungan pemerintah/swasta termasuk pengalaman sub kontrak, kecuali penyedia Barang/Jasa yang baru berdiri kurang dari tiga tahun.</li>
				<li><em>Tidak masuk dalam daftar hitam.</em></li>
				<li>Tata cara penilaian, meliputi: aspek administrasi, permodalan, tenaga kerja, peralatan, pengalaman dengan menggunakan sistem gugur atau sistem nilai<em> (scoring system)</em>.</li>
			</ul>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>14</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa <u>proses prakualifikasi penyedia B</u><u>arang</u><u>/J</u><u>asa</u> telah sesuai dengan ketentuan yang tercantum dalam dokumen pengadaan Barang/Jasa, dan pastikan pelaksanaannya telah dilaksanakan secara adil, terbuka, transparan dan akuntabel, antara lain dengan cara:</p>

			<ul>
				<li>Teliti kelengkapan dokumen prakualifikasi peserta termasuk peralatan yang harus tersedia, apakah telah sesuai dengan persyaratan yang terdapat dalam dokumen prakualifikasi, dan telah dibuatkan berita acaranya.</li>
				<li>Periksa apakah daftar peserta prakualifikasi dan hasil penilaian kualifikasi telah disetujui oleh pejabat yang berwenang.</li>
				<li>Pastikan bahwa hasil prakualifikasi telah diinformasikan kepada seluruh peserta (melalui website lpse.bmkg.go.id/)</li>
				<li>Identifikasi penyimpangan dalam proses prakualifikasi untuk mengetahui kemungkinan terjadinya (indikasi) kecurangan.</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>15</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan ketepatan metode evaluasi yang digunakan untuk pengadaan Barang/Jasa yang dilaksanakan.</p>

			<p>(Pengadaan Barang/Jasa selain Jasa konsultansi umumnya menggunakan metode penilaian sistem gugur dan sistem nilai, sedangkan Pengadaan Jasa Konsultansi&nbsp; dapat menggunakan metode penilaian kualitas, kualitas dan biaya, pagu anggaran, biaya terendah).</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>16</p>
			</td>
			<td style="width:260px">
			<p>Teliti dan pastikan bahwa dokumen pemilihan penyedia Barang/Jasa telah lengkap, sekurang-kurangnya memuat:&nbsp;</p>

			<ul>
				<li>Undangan kepada penyedia Barang/Jasa yang lolos prakualifikasi. Undangan minimal memuat: tempat, tanggal, hari, waktu untuk memperoleh dokumen pemilihan penyedia Barang/Jasa, Aanwijzing, penyampaian dokumen penawaran, jadual pelaksanaan pengadaan Barang/Jasa sampai penetapan penyedia Barang/Jasa.</li>
				<li>Instruksi kepada peserta pengadaan Barang/Jasa (minimal memuat: umum, isi dokumen pemilihan penyedia Barang/Jasa, syarat bahasa, cara penyampaian &amp; penandaan sampul penawaran dan batas akhir penyampaian penawaran, prosedur pembukaan penawaran, dan penilaian kualifikasi dan kriteria penetapan pemenang ).</li>
				<li>Syarat-syarat umum kontrak.</li>
				<li>Syarat-syarat khusus kontrak.</li>
				<li>Daftar kuantitas &amp; harga, termasuk komponen produksi dalam negeri.</li>
				<li>Spesifikasi teknis &amp; gambar (tidak mengarah pada merek tertentu).</li>
				<li>Bentuk: surat penawaran, kontrak, surat jaminan penawaran, surat jaminan pelaksanaan, dan surat jaminan uang muka.</li>
			</ul>

			<p><u>Khusus: </u></p>

			<ul>
				<li>Untuk kontrak yang jangka waktu pelaksanaannya lebih dari 12 (dua belas) bulan, dapat dicantumkan ketentuan penyesuaian harga (price adjustment), dgn penjelasan rumus yang digunakan.</li>
				<li>Untuk pengadaan dengan pascakualifikasi, dokumen pascakualifikasi dimasukkan dalam dokumen pemilihan penyedia Barang/Jasa.</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>17</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah syarat-syarat administrasi yang ditetapkan tidak membatasi calon kontraktor untuk ikut berpartisipasi mengikuti lelang, khususnya untuk calon peserta dari negara asing.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>18</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah spesifikasi barang tidak menunjuk kepada salah satu merk tertentu dan dibuat sedemikian rupa sehingga uraian spesifikasi dapat menjamin akan diperoleh barang sesuai kualitas yang diinginkan misalnya; jaminan purna jual, kemampuan melakukan pemeliharaan (sebaran lokasi), volume penjualan dan lain-lain.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>19</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa proses pemilihan penyedia Barang/Jasa telah sesuai dengan ketentuan yang tercantum dalam dokumen pengadaan Barang/Jasa, dan pastikan pelaksanaannya telah dilaksanakan secara adil, terbuka, transparan dan akuntabel, antara lain dengan cara:</p>

			<ul>
				<li>Dapatkan daftar calon peserta lelang yang mengambil (download) dokumen lelang dan teliti tanggal-tanggal pengambilan dokumen untuk meyakini bahwa pengambilan dokumen dilakukan pada periode yang telah ditentukan.</li>
				<li>Lakukan konfirmasi kepada beberapa calon peserta lelang yang telah mengambil (download) dokumen lelang tetapi batal mengikuti lelang (tidak mengupload dokumen penawaran)&nbsp; dan dapatkan alasannya untuk meyakini bahwa panitia pengadaan telah berlaku adil dan transparan</li>
				<li>Bandingkan dokumen yang diserahkan (diupload) oleh rekanan yang menang dan yang kalah</li>
				<li>Periksa berita acara, daftar hadir aanwijzing dan jarak waktu antara pengumuman dengan aanwijzing, serta yakinkan bahwa pelaksanaannya telah diikuti oleh cukup peserta.</li>
				<li>Cek dan Teliti hasil aanwijzing secara online dengan meminta rekap hasil percakapan kepada Pokja ULP.</li>
				<li>Cek apakah ada calon peserta lelang yang digugurkan karena tidak mengikuti aanwijzing online.</li>
				<li>Yakinkan bahwa Panitia Pengadaan telah menyampaikan perubahan dokumen pengadaan (upload) tersebut pada aplikasi SPSE</li>
				<li>Cek apakah ada dokumen dari Penyedia tidak dapat dibuka oleh Pokja ULP, Jika ada teliti apa penyebabnya.</li>
				<li>Dapatkan semua dokumen penawaran dan periksa apakah semua penawar yang lulus prakualifikasi telah menyertakan jaminan penawaran (copy) yang masih berlaku pada saat dilaksanakan proses pengadaan. (Nominal jaminan penawaran sebesar 1% s.d. 3% dari nilai HPS).</li>
				<li>Periksa laporan dan dokumen lainnya atas hasil penilaian penawaran peserta dan lakukan pengujian apakah hasil penilaian penawaran telah dilakukan sesuai dengan cara evaluasi yang ditetapkan sebelumnya, baik terhadap aspek administrasi, teknis maupun harga.</li>
				<li>Lakukan pengujian atas metode penilaian penawaran yang dilakukan oleh Pokja ULP, apakah telah sesuai dengan metode penilaian yang telah ditetapkan dalam dokumen lelang.</li>
				<li>Lakukan analisis harga penawaran diantara para peserta apakah ada indikasi harga yang diatur. (Indikasinya antara lain berupa format dan bentuk huruf yang sama, interval perbedaan yang hampir sama, adanya perbedaan harga yang sangat ekstrem pada beberapa item, adanya penawaran yang sangat rendah/tidak wajar).</li>
				<li>Evaluasi apakah pemenang lelang mempunyai hubungan istimewa dengan Atasan Langsung/ Pengguna/Bendaharawan/Panitia Pengadaan. Jika terdapat indikasi adanya hubungan istimewa, telusuri lebih lanjut adanya kemungkinan harga menjadi tidak ekonomis..</li>
				<li>Yakinkan pemenang lelang merupakan calon peserta yang memiliki score tertinggi. Score ditentukan sesuai dengan metode penilaian (teknis/kualitas atau gabungan antara teknis/kualitas dan harga).</li>
				<li>Yakinkan bahwa urutan calon pemenang lelang telah sesuai dengan hasil evaluasi atau scoring dokumen penawaran (hasil berita acara evaluasi proposal teknis, evaluasi proposal biaya dan evaluasi proposal gabungan)</li>
				<li>Yakinkan ada atau tidaknya syarat negosiasi harga kepada calon pemenang (untuk Penunjukan Langsung, Pengadaan Langsung)</li>
				<li>&nbsp;Yakinkan negosiasi harga telah dilakukan secara benar dan dibuatkan berita acara dan notulen secara tertulis dan ditandatangani oleh panitia dan calon pemenang, sebelum dilakukan penandatangan kontrak</li>
				<li>Periksa apakah hasil penetapan pemenang telah diumumkan dan disampaikan kepada seluruh peserta lelang.</li>
				<li>Teliti ada tidaknya keberatan/sanggahan atas penetapan pemenang, dan apabila ada apakah telah ditindaklanjuti.</li>
				<li>Dapatkan informasi dari panitia pengadaan mengenai ada/tidak ada sanggahan, atau lakukan konfirmasi kepada beberapa calon peserta yang memiliki potensi persaingan tetapi kalah dalam scoring untuk mengetahui ada tidaknya sanggahan</li>
				<li>Teliti apakah sanggahan diterima pada saat yang tepat dan ditindaklanjuti oleh panitia pengadaan sesuai ketentuan</li>
				<li>Apabila terdapat sanggahan, pelajari dan telaah isi sanggahan dan lakukan pengujian atas informasi yang tercantum dalam surat sanggahan untuk melakukan pendalaman audit.</li>
				<li>Periksa tanggal penetapan pemenang, dan tanggal penandatanganan kontrak, apakah telah melebihi masa sanggah sesuai ketentuan yang berlaku.</li>
				<li>Bila perlu, lakukan konfirmasi kepada peserta yang kalah atas keikutsertaannya. (Untuk meyakinkan bahwa penilaian dan penetapan pemenang telah benar dilakukan sesuai dengan ketentuan dan tidak ada indikasi KKN).</li>
			</ul>

			<p>Catatan:</p>

			<ul>
				<li>Penandatanganan kontrak paling lambat empat belas hari kerja sejak diterbitkannya Surat Penetapan Penyedia Barang/Jasa (SPPBJ).</li>
				<li>Periksa bahwa penyedia Barang/Jasa telah menyerahkan surat jaminan pelaksanaan sebesar 5% (lima persen) dari nilai kontrak dan jika nilai penawaran terkoreksi dibawah 80% dari total HPS, Besarnya Jaminan pelaksanaan 5% dari nilai total HPS.</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>20</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('52d2b9dff25cf283057cad116ebedcf01aada340', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP011', '8ebf1eace7c59cc969fbd0734c195aeeeaf9a7b0', 'Kontrak Pengadaan Barang/ Jasa', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>21</p>
			</td>
			<td style="width:260px">
			<p>Pelajari kontrak pengadaan Barang/Jasa yang bersangkutan dan bandingkan dengan syarat umum dan syarat khusus kontrak yang disampaikan sebelumnya dalam dokumen pemilihan penyedia Barang/Jasa, serta yakinkan kesesuaiannya.</p>

			<p>Isi Kontrak (minimal):</p>

			<ul>
				<li>Para pihak (nama, jabatan, alamat)</li>
				<li>Pokok pekerjaan yang diperjanjikan</li>
				<li>Hak dan kewajiban para pihak</li>
				<li>Nilai atau harga kontrak, serta syarat pembayaran</li>
				<li>Persyaratan &amp; spesifikasi teknis yg jelas dan terinci</li>
				<li>Tempat &amp; jangka waktu penyelesaian/ penyerahan dgn jadual waktu dan syarat penyerahannya.</li>
				<li>Jaminan teknis/hasil pekerjaan yg dilaksanakan /ketentuan kelaikan</li>
				<li>Ketentuan mengenai cidera janji dan sanksi</li>
				<li>Ketentuan pemutusan kontrak secara sepihak</li>
				<li>Ketentuan keadaan memaksa</li>
				<li>Ketentuan kewajiban para pihak dalam hal terjadi kegagalan pelaksanaan pekerjaan</li>
				<li>Ketentuan perlindungan tenaga kerja</li>
				<li>Ketentuan bentuk &amp; tanggung jawab gangguan lingkungan</li>
				<li>Ketentuan mengenai penyelesaian perselisihan.</li>
			</ul>

			<p>Kontrak pengadaan B/J dapat batal, karena:</p>

			<ul>
				<li>bila isi kontrak melanggar ketentuan perundangan yang berlaku (batal demi hukum).</li>
				<li>bila para pihak terbukti melakukan KKN, kecurangan dan pemalsuan dalam proses pengadaan maupun pelaksaan kontrak (dibatalkan).</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>22</p>
			</td>
			<td style="width:260px">
			<p>Teliti sistem kontrak yang ditandatangani/digunakan (berupa: lumpsum, unit price atau lainnya) apakah telah tepat, dan telah sesuai dengan realisasi pelaksanaannya</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>23</p>
			</td>
			<td style="width:260px">
			<p>Bila ada pekerjaan supervisi atas kontrak, yakinkan bahwa pelaksanaan supervisi telah dilaksanakan sesuai kontrak. Jika tidak, periksa apakah sudah terdapat supervisi yang cukup atas pelaksanaan kontrak tersebut.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>24</p>
			</td>
			<td style="width:260px">
			<p>Bila ada klausul pemberian uang muka, pastikan bahwa uang muka yang diberikan kepada penyedia B/J telah sesuai dengan ketentuan, sebagai berikut:</p>

			<ul>
				<li>Usaha kecil maksimal 30%&nbsp; nilai kontrak,</li>
				<li>Usaha Non kecil, maksimal 20% nilai kontrak.</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>25</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('737057183d4501e1608cc03aada2e860206c10f0', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP012', '8ebf1eace7c59cc969fbd0734c195aeeeaf9a7b0', 'Pelaksanaan Pengadaan Jasa Konsultansi', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>26</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah jasa yang dibutuhkan oleh pengguna jasa konsultansi harus dilaksanakan oleh pihak ketiga (tidak dapat dilaksanakan sendiri oleh pengguna jasa).</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>27</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa pengadaan jasa konsultansi telah dilengkapi dengan Kerangka Acuan Kerja (KAK)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>28</p>
			</td>
			<td style="width:260px">
			<p>Teliti apakah dalam KAK dan Berita Acara pemberian penjelasan, kriteria evaluasi penawaran telah disampaikan kepada peserta.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>29</p>
			</td>
			<td style="width:260px">
			<p>Dapatkan hasil evaluasi panitia atas proses seleksi jasa konsultansi dan uji hasil penilaian tersebut apakah telah sesuai dengan kriteria evaluasi yang telah ditetapkan dalam dokumen seleksi. Unsur-unsur pokok yang dinilai adalah pengalaman, pendekatan dan metodologi dan kualifikasi tenaga ahli.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>30</p>
			</td>
			<td style="width:260px">
			<p>Yakinkan bahwa informasi yang tertera dalam curriculum vitae&nbsp; dan keaslian ijasah pendidikan formal tenaga ahli, telah sesuai dengan kualifikasi yang ditetapkan.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>31</p>
			</td>
			<td style="width:260px">
			<p>Periksa proses sebelum penetapan pemenang, apakah ada negosiasi dan klarifikasi, lakukan pengujian terhadap proses tersebut (bandingkan dengan berita acaranya) dan pastikan telah sesuai dengan ketentuan dan tidak ada indikasi KKN. (Untuk pekerjaan Jasa Konsultansi tidak diperlukan jaminan pelaksanaan).</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>32</p>
			</td>
			<td style="width:260px">
			<p>Teliti biaya langsung non personil untuk pekerjaan konsultansi tidak melebihi 40% dari total biaya (terkecuali pekerjaan pemetaan udara, survey lapangan, pengukuran dan penyelidikan tanah).</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>33</p>
			</td>
			<td style="width:260px">
			<p>Lakukan penelaahan hasil kerja tim teknis yang mengevaluasi laporan hasil kegiatan dengan data hasil pelaksanaan kegiatan konsultansi.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>34</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit.</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>INSERT INTO ref_program_audit VALUES ('f576b3c70c40d9cfd39adea4b5db1922d4156015', '98cd0ca731da106235cc474a7608ad5750d8e1e8', 'AP013', '8ebf1eace7c59cc969fbd0734c195aeeeaf9a7b0', 'Pelaksanaan Pengadaan Jasa secara Swakelola', '<table border="1" cellpadding="0" cellspacing="0" style="width:702px">
	<tbody>
		<tr>
			<td style="width:36px">
			<p>35</p>
			</td>
			<td style="width:260px">
			<p>Identifikasi apakah pekerjaan pengadaan barang/jasa telah memenuhi ketentuan untuk dilaksanakan secara swakelola (ada 8 kriteria sesuai pasal 39 ayat (3) Keppres No. 80/2003)</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>36</p>
			</td>
			<td style="width:260px">
			<p>Apakah pelaksanaan swakelola dengan menggunakan tenaga ahli dari luar tidak melebihi 50% dari tenaga sendiri</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>37</p>
			</td>
			<td style="width:260px">
			<p>Identifikasi apakah pelaksanaan swakelola dilaksanakan oleh pengguna Barang/ Jasa, instansi pemerintah lain atau kelompok masyarakat/LSM penerima hibah</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>38</p>
			</td>
			<td style="width:260px">
			<p>Untuk pelaksanaan swakelola yang oleh pengguna Barang/ Jasa apakah telah memenuhi ketentuan pada Lampiran 1 Bab III butir B.1 Untuk pelaksanaan swakelola yang oleh Instansi Pemerintah lain non swadana apakah telah memenuhi ketentuan pada Lampiran 1 Bab III butir B.2</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>39</p>
			</td>
			<td style="width:260px">
			<p>Untuk pelaksanaan swakelola yang oleh LSM Penerima Hibah apakah telah memenuhi ketentuan pada Lampiran 1 Bab III butir B.3</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>40</p>
			</td>
			<td style="width:260px">
			<p>Apakah pelaksanaan swakelola telah dilaporkan (laporan kemajuan, realisasi fisik dan keuangan) oleh pelaksana lapangan/pelaksana swakelola kepada pengguna Barang/ Jasa setiap bulan</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>41</p>
			</td>
			<td style="width:260px">
			<p>Apakah laporan kemajuan, realisasi fisik dan keuangan telah dilaporkan setiap bulan oleh pengguna Barang/ Jasa kepada Menteri/Kepala LPND/Gubernur/Bupati/ Walikota/ Pejabat yang disamakan.</p>
			</td>
		</tr>
		<tr>
			<td style="width:36px">
			<p>42</p>
			</td>
			<td style="width:260px">
			<p>Buat simpulan hasil audit.</p>
			</td>
		</tr>
	</tbody>
</table>', '1')
<query>DROP TABLE IF EXISTS rekomendasi_internal
<query>CREATE TABLE `rekomendasi_internal` (
  `rekomendasi_id` varchar(50) NOT NULL,
  `rekomendasi_finding_id` varchar(50) NOT NULL,
  `rekomendasi_id_code` varchar(50) NOT NULL,
  `rekomendasi_desc` longblob NOT NULL,
  `rekomendasi_pic` varchar(50) NOT NULL,
  `rekomendasi_dateline` int(11) NOT NULL,
  `rekomendasi_date` int(11) NOT NULL,
  `rekomendasi_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=Selesai',
  PRIMARY KEY (`rekomendasi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS risk_identifikasi
<query>CREATE TABLE `risk_identifikasi` (
  `identifikasi_id` varchar(50) NOT NULL,
  `identifikasi_penetapan_id` varchar(50) NOT NULL,
  `identifikasi_kategori_id` varchar(50) NOT NULL,
  `identifikasi_selera` varchar(50) NOT NULL,
  `identifikasi_no_risiko` varchar(10) NOT NULL,
  `identifikasi_nama_risiko` varchar(255) NOT NULL,
  `identifikasi_penyebab` varchar(255) NOT NULL,
  `analisa_kemungkinan` int(3) NOT NULL,
  `analisa_kemungkinan_name` varchar(50) NOT NULL,
  `analisa_dampak` int(3) NOT NULL,
  `analisa_dampak_name` varchar(50) NOT NULL,
  `analisa_ri` varchar(3) NOT NULL,
  `analisa_ri_name` varchar(50) NOT NULL,
  `analisa_bobot_risk` varchar(3) NOT NULL,
  `analisa_nilai_ri` float(5,2) NOT NULL,
  `analisa_bobot_kat_risk` varchar(3) NOT NULL,
  `evaluasi_komponen` varchar(255) NOT NULL,
  `evaluasi_efektifitas` int(3) NOT NULL,
  `evaluasi_efektifitas_name` varchar(50) NOT NULL,
  `evaluasi_risiko_residu` int(3) NOT NULL,
  `evaluasi_risiko_residu_name` varchar(50) NOT NULL,
  `penanganan_risiko_id` varchar(50) NOT NULL,
  `penanganan_plan` varchar(255) NOT NULL,
  `penanganan_date` int(11) NOT NULL,
  `penanganan_pic_id` varchar(50) NOT NULL,
  `monitoring_action` varchar(255) NOT NULL,
  `monitoring_date` int(11) NOT NULL,
  `monitoring_plan_action` varchar(255) NOT NULL,
  `monitoring_tenggat_waktu` int(11) NOT NULL,
  PRIMARY KEY (`identifikasi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS risk_identifikasi_attach
<query>CREATE TABLE `risk_identifikasi_attach` (
  `iden_attach_id_iden` varchar(50) NOT NULL,
  `iden_attach_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS risk_penetapan
<query>CREATE TABLE `risk_penetapan` (
  `penetapan_id` varchar(50) NOT NULL,
  `penetapan_auditee_id` varchar(50) NOT NULL,
  `penetapan_tahun` int(4) NOT NULL,
  `penetapan_nama` varchar(100) NOT NULL,
  `penetapan_tujuan` varchar(255) NOT NULL,
  `penetapan_profil_risk` varchar(50) NOT NULL,
  `penetapan_profil_risk_residu` varchar(50) NOT NULL,
  `penetapan_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=propose, 2=approve, 3=tolak',
  `penetapan_set_pkat` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=pkat',
  PRIMARY KEY (`penetapan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS risk_penetapan_comment
<query>CREATE TABLE `risk_penetapan_comment` (
  `penetapan_comment_id` varchar(50) NOT NULL,
  `penetapan_comment_penetapan_id` varchar(50) NOT NULL,
  `penetapan_comment_user_id` varchar(50) NOT NULL,
  `penetapan_comment_desc` varchar(255) NOT NULL,
  `penetapan_comment_date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS role_group_data
<query>CREATE TABLE `role_group_data` (
  `role_data_id_group` varchar(50) NOT NULL,
  `role_data_menu` varchar(255) NOT NULL,
  `role_data_method` varchar(255) NOT NULL,
  `role_data_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=All, 2=base On ID'
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO role_group_data VALUES ('25a0bc88a8aca130913d7b35facf9e3505bf12af', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('25a0bc88a8aca130913d7b35facf9e3505bf12af', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('25a0bc88a8aca130913d7b35facf9e3505bf12af', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('7090cfd596225052fed383a3332fcc8dc72ba918', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('7090cfd596225052fed383a3332fcc8dc72ba918', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('7090cfd596225052fed383a3332fcc8dc72ba918', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('25a0bc88a8aca130913d7b35facf9e3505bf12af', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('7090cfd596225052fed383a3332fcc8dc72ba918', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '2')
<query>INSERT INTO role_group_data VALUES ('c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '3', 'auditormgmt', '2')
<query>INSERT INTO role_group_data VALUES ('98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '2')
<query>INSERT INTO role_group_data VALUES ('4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '3', 'auditormgmt', '2')
<query>INSERT INTO role_group_data VALUES ('4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('036b0642d7c660cd501fb07e1e33ec0bdd157534', '4', 'auditeemgmt', '1')
<query>INSERT INTO role_group_data VALUES ('036b0642d7c660cd501fb07e1e33ec0bdd157534', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('036b0642d7c660cd501fb07e1e33ec0bdd157534', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '3', 'auditormgmt', '1')
<query>INSERT INTO role_group_data VALUES ('12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '2', 'risk_result;auditplan;auditassign;followupassign;reportaudit', '1')
<query>INSERT INTO role_group_data VALUES ('12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '1', 'risk_penetapantujuan;risk_reviu;risk_fil_report', '1')
<query>INSERT INTO role_group_data VALUES ('a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '4', 'auditeemgmt', '1')
<query>DROP TABLE IF EXISTS role_group_menu
<query>CREATE TABLE `role_group_menu` (
  `role_menu_id` varchar(50) NOT NULL,
  `role_menu_group_id` varchar(50) NOT NULL,
  `role_menu_menu_id` varchar(50) NOT NULL,
  PRIMARY KEY (`role_menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>INSERT INTO role_group_menu VALUES ('016a17077c81f9869b905195967c425084f338b8', '6635583845', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('998c57343e1734ecc3f7ffbe74b3c5e87d5c1463', '89352701', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('d1c46cb0f99804334d60462b3c4a860782d52ec3', '86333312', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('18f0bc9be109fb0422e709967dbc9d2880cb862f', '25a0bc88a8aca130913d7b35facf9e3505bf12af', '103')
<query>INSERT INTO role_group_menu VALUES ('40350baa7d87b3585fbebdd6bd9cf13fa3bee23b', '25a0bc88a8aca130913d7b35facf9e3505bf12af', '79')
<query>INSERT INTO role_group_menu VALUES ('559e32d0b1a3747d13bef4eaffa7ef673145489b', '25a0bc88a8aca130913d7b35facf9e3505bf12af', '78')
<query>INSERT INTO role_group_menu VALUES ('dfb9f6beba7727743a29e33e1fb65a996bc57160', '25a0bc88a8aca130913d7b35facf9e3505bf12af', '77')
<query>INSERT INTO role_group_menu VALUES ('a0973aaefd68418d4d8f4caf9cd1abd32f511bcf', '6468517789', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('c88a484c5bffb4769ba8289324f6f9604936dfba', 'bae57a2fa5c2b3d79cb349b2e152f0d4c09d2a2c', '4')
<query>INSERT INTO role_group_menu VALUES ('02560e7d4a61c5deb288ff971802b5461b2bef7b', 'bae57a2fa5c2b3d79cb349b2e152f0d4c09d2a2c', '14')
<query>INSERT INTO role_group_menu VALUES ('bab95b13f2f76696dbdef9b06076a3cd320841e3', 'bae57a2fa5c2b3d79cb349b2e152f0d4c09d2a2c', '84')
<query>INSERT INTO role_group_menu VALUES ('5be11d72aa4585dd643289d9061371c2fec0fd4d', '25a0bc88a8aca130913d7b35facf9e3505bf12af', '29')
<query>INSERT INTO role_group_menu VALUES ('c94e361768f705187881520c92e86ca4ae156cc0', '25a0bc88a8aca130913d7b35facf9e3505bf12af', '2')
<query>INSERT INTO role_group_menu VALUES ('b6965896e25697edf2f6820d87b8baef0fcce3fe', '34411101', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('b0194be0327f1e363c3ce79ffa9381a538d89474', '5159727082', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('76dcbcf4f1337ba043c9ea5b3d30e4474370a4f6', '94863601', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('d2ec747f1e274c37a49b215748cef26811967c5b', '10144701', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('91e0299697c13c21536bcb02322b315c850a5a84', '7884675562', '5d3b02abc8c1e0d4d4f7ee3c0c59f09f4ea93f01')
<query>INSERT INTO role_group_menu VALUES ('d39564025ddea661c487206c8f4707a2f005424b', '20134601', '5d3b02abc8c1e0d4d4f7ee3c0c59f09f4ea93f01')
<query>INSERT INTO role_group_menu VALUES ('6a0a162248913a35f78429c5fd0c81f81a6f8319', '20954301', '5d3b02abc8c1e0d4d4f7ee3c0c59f09f4ea93f01')
<query>INSERT INTO role_group_menu VALUES ('395d8ca7c97072e53000f0860f90b6bd9386e7e9', '94758901', 'ff6c97274745bf929be5d651849c64d7ca1b405d')
<query>INSERT INTO role_group_menu VALUES ('07a48c23d8833ca813d74ba9c553855b23de231d', '14329001', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('751f47d1c18123e859d84a30f148c6d679380218', '5672584754', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('399d87fe20af089176697df395e7ce0c69a39465', '67301501', 'b646f7e15fc0ca85b3a3fe54c19017a0e933ef82')
<query>INSERT INTO role_group_menu VALUES ('9d211c1c0f9d7482deef1a996733d168912d623c', '4351176577', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('ad099da448f26f50837c14c06ee26fc8bc6f5d78', '96805801', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('361b181b61066257dbea7638f43847c1e29216e5', '37470412', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('e0995fb697149868a1b5c24ae72243b94df6aed5', '42304401', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('e6199785bf045f5ade0706ba7dfabe23eecc6dee', '10470101', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('0d921bf2e158909ca437b89a8b9a84b8b10ff828', '8547794855', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('8c856fa2c1ab16cacab9f58913927f9b1be281f1', '37617601', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('c931c72786bb56c52599cf520e88a2bab00259a6', '51888101', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('4a4d910ab9a2fab1c552e00017fec3b4ed73a8c8', '5282026173', '24e3c696dcb9ff44b17eafb85d9d15ffe330b80d')
<query>INSERT INTO role_group_menu VALUES ('e82fc8eb02ce76fa1be29fe54065e9ca6f6d0965', '95639101', '24e3c696dcb9ff44b17eafb85d9d15ffe330b80d')
<query>INSERT INTO role_group_menu VALUES ('670baa5104d9ced245190f91a6d912cb121018c1', '7995884552', '24e3c696dcb9ff44b17eafb85d9d15ffe330b80d')
<query>INSERT INTO role_group_menu VALUES ('bc32df35131c35adbd4c7caf81bbfaaf40d89917', '17223201', '24e3c696dcb9ff44b17eafb85d9d15ffe330b80d')
<query>INSERT INTO role_group_menu VALUES ('bafa6e54f0fb478f9c8b85a491acba54d4a41f34', '41205901', '24e3c696dcb9ff44b17eafb85d9d15ffe330b80d')
<query>INSERT INTO role_group_menu VALUES ('ba9371d1b5f87458a3596018f4fd990eaa060f7d', '35515912', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('3b3d83d1823d4aca3d240102b9da9cf8c8a1173e', '43413101', '3c4b7c1ac41e48962a837470d96af3ca244113ba')
<query>INSERT INTO role_group_menu VALUES ('a1dda293c4b5e88ec85f00870dd668930fb5bffe', '2087755971', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('e19714010924325174be78abc2d0f87ec7b494cf', '55181801', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('f1285f52eb020aa02a274698908d0ccc2477d6fc', '95034012', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('cfedf74494cf47680f9765cdd7d8cf4b340bb4e3', '98203112', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('954d806e1176e490791aba133e0a3ef564779a53', '3174155057', '3c4b7c1ac41e48962a837470d96af3ca244113ba')
<query>INSERT INTO role_group_menu VALUES ('4c80deb3828194f5b8a6d2bce247943730c31dcf', '77590101', '3c4b7c1ac41e48962a837470d96af3ca244113ba')
<query>INSERT INTO role_group_menu VALUES ('bfdd39ce77aafb4502c639fbbb86c35aa48f8e5c', '40969601', '3c4b7c1ac41e48962a837470d96af3ca244113ba')
<query>INSERT INTO role_group_menu VALUES ('17ac0855199f1b176ca2043489cb0cf83432ca26', '38792701', '3c4b7c1ac41e48962a837470d96af3ca244113ba')
<query>INSERT INTO role_group_menu VALUES ('06acae17bde821d36c5193a5efb5148460fae34d', '42069212', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('10a989cc16e4b71c4c2c790a011f54c959928313', '6303357991', 'ff6c97274745bf929be5d651849c64d7ca1b405d')
<query>INSERT INTO role_group_menu VALUES ('f34b79bdae007ccfcea4d78bb270517fb3a6d17e', '18519301', 'ff6c97274745bf929be5d651849c64d7ca1b405d')
<query>INSERT INTO role_group_menu VALUES ('31a5c6fcccf0f662a72818bcb84a6d346077ad36', '11083412', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('fc2b83cbb9aa4695bc948d3bf77f319fe473ffde', '42429912', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('ff44bbb81419bccd86b23f4fc5257de732583703', '13719112', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('d22166817fd171b3d60c9eff5f2ff8ae74749feb', '49057501', 'c64ab15f3c09ce0faf0e07ac527be323ce985814')
<query>INSERT INTO role_group_menu VALUES ('d3642d5e548696744300e67addec27bf5fdb2e57', '51667601', '24e3c696dcb9ff44b17eafb85d9d15ffe330b80d')
<query>INSERT INTO role_group_menu VALUES ('957c4de7f02f6ef6f853d7917665c3d43b509105', '78078601', '3c4b7c1ac41e48962a837470d96af3ca244113ba')
<query>INSERT INTO role_group_menu VALUES ('6b0d77ea65834c204712d998c9e52b6918f74341', '18133945', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3')
<query>INSERT INTO role_group_menu VALUES ('023f50085810b625620cdd49484567f1282b2786', '79215801', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3')
<query>INSERT INTO role_group_menu VALUES ('641f78a22cea75e1c0f7d7ddd82c292794297d95', '1335113238', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('fdb08cfbe47e4f156e33041f83148285e54bffcf', '15337901', '7090cfd596225052fed383a3332fcc8dc72ba918')
<query>INSERT INTO role_group_menu VALUES ('37bd4af874181da0f70df1a3fcb310501f0434e8', '1713602225', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb')
<query>INSERT INTO role_group_menu VALUES ('4d4640615395b35a5a1d27b94c05a5f58ffc16ed', '46960001', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb')
<query>INSERT INTO role_group_menu VALUES ('c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', '')
<query>INSERT INTO role_group_menu VALUES ('31cd35dc857aa4305db0ff4cf789321464e8b7e8', '40529712', '0d1db90fabf192910f354e351dc27fd7f37fb041')
<query>INSERT INTO role_group_menu VALUES ('a682793da33c369d689a4c14d074da067ee55447', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '73')
<query>INSERT INTO role_group_menu VALUES ('d626893eb9cabacf8ebe92e3eb5dc69b91435fea', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '74')
<query>INSERT INTO role_group_menu VALUES ('7fe77dcc96471bb837a215091214cf7a91f78e50', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '75')
<query>INSERT INTO role_group_menu VALUES ('ff14f835218c63c68aa60e9cd1d7543e2b1302e2', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '72')
<query>INSERT INTO role_group_menu VALUES ('08ed078c415f0148699a92a02c64e2c4085b0dfe', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '71')
<query>INSERT INTO role_group_menu VALUES ('2e2857df8b3bb065ce5851be2e2b787515b37559', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '104')
<query>INSERT INTO role_group_menu VALUES ('618d185d19bcb35e295ceec0367987ae1e27f59a', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '103')
<query>INSERT INTO role_group_menu VALUES ('224d25ee31199e9fc1b19e16d9d6588323f7d95c', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '102')
<query>INSERT INTO role_group_menu VALUES ('695c04851a0dbd31b3b13c6d3b525dfd732b8380', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '101')
<query>INSERT INTO role_group_menu VALUES ('ae4e7192f300dc8aa478984e3da25d362ea0f8a6', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '100')
<query>INSERT INTO role_group_menu VALUES ('8966ca2db08532db42a976f302a8c729d17ae182', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '99')
<query>INSERT INTO role_group_menu VALUES ('3b8d698c46aa33f229308488636955d7527979f2', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '98')
<query>INSERT INTO role_group_menu VALUES ('2743a0b8f5ed2f1dcab8427bedb6debaba5b39ca', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '97')
<query>INSERT INTO role_group_menu VALUES ('11793356a2205be9894170765f0e1f213868aaea', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '96')
<query>INSERT INTO role_group_menu VALUES ('f8c209000369476f88d7db1d98ab9bf358098124', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '95')
<query>INSERT INTO role_group_menu VALUES ('51f3cf22abc9b364c6a2f2b88f90123c7e2dd597', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '94')
<query>INSERT INTO role_group_menu VALUES ('607ae3d321d992c80d0d14520660e1b63813b87f', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '93')
<query>INSERT INTO role_group_menu VALUES ('4fb66b50344f1ad9df0bb4c75027cf4a042a2a25', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '92')
<query>INSERT INTO role_group_menu VALUES ('562924d4b0c6f3c925a43a14fabf14a8cd4f426d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '91')
<query>INSERT INTO role_group_menu VALUES ('98ed9b5e5ef59aa6e5a50f7375a429ed9680959f', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '90')
<query>INSERT INTO role_group_menu VALUES ('c836261bd9d1aaa1edf6be1655dc57090060c14d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '89')
<query>INSERT INTO role_group_menu VALUES ('9f3275a03f05b75d886d01c4ff361efa6f16d7a2', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '88')
<query>INSERT INTO role_group_menu VALUES ('5e8e663082860b6f5988c501724707d1461f3690', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '87')
<query>INSERT INTO role_group_menu VALUES ('b3198b97bccc5283199a815993d20f9fe9e55180', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '86')
<query>INSERT INTO role_group_menu VALUES ('1ad91783883ef0a494ca58ed1ed7de8f34039455', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '106')
<query>INSERT INTO role_group_menu VALUES ('bc446c38c4b4b1cfc3bb943337997b8d711307ef', '7090cfd596225052fed383a3332fcc8dc72ba918', '116')
<query>INSERT INTO role_group_menu VALUES ('e03182c1fb80b6a43636582286dd02e742cf8418', '7090cfd596225052fed383a3332fcc8dc72ba918', '117')
<query>INSERT INTO role_group_menu VALUES ('ab91d74c8f19b92ce58057d79c1a9eaa03502003', '7090cfd596225052fed383a3332fcc8dc72ba918', '118')
<query>INSERT INTO role_group_menu VALUES ('eb93a0eb189ca4aa61db4296a29aa87813041be4', '7090cfd596225052fed383a3332fcc8dc72ba918', '119')
<query>INSERT INTO role_group_menu VALUES ('7b45311a98b5aa22a7551cef0470d0c3dfa75e72', '7090cfd596225052fed383a3332fcc8dc72ba918', '120')
<query>INSERT INTO role_group_menu VALUES ('8006a3db6e00bcb6623999969d3783d6d7aa384f', '7090cfd596225052fed383a3332fcc8dc72ba918', '85')
<query>INSERT INTO role_group_menu VALUES ('34b9ae1217f8e89a1aa90e063b5057c3b1fac5d6', '7090cfd596225052fed383a3332fcc8dc72ba918', '84')
<query>INSERT INTO role_group_menu VALUES ('9a748995415c10a36feddc0632180f18629bd22a', '7090cfd596225052fed383a3332fcc8dc72ba918', '83')
<query>INSERT INTO role_group_menu VALUES ('92e492b7a1c16444cd766c974ad5f0eb79418da7', '7090cfd596225052fed383a3332fcc8dc72ba918', '21')
<query>INSERT INTO role_group_menu VALUES ('dcf2ce90d952d6d2280ff0db7a0021f199bfb407', '7090cfd596225052fed383a3332fcc8dc72ba918', '15')
<query>INSERT INTO role_group_menu VALUES ('632c729e7ef1612b737d7c1804cd4dd594719f5b', '7090cfd596225052fed383a3332fcc8dc72ba918', '14')
<query>INSERT INTO role_group_menu VALUES ('2a673a42540e4f755db64352562375653672feca', '7090cfd596225052fed383a3332fcc8dc72ba918', '2')
<query>INSERT INTO role_group_menu VALUES ('b373d15a98dfdaa9640f3028e858c6efe941d5ce', '7090cfd596225052fed383a3332fcc8dc72ba918', '4')
<query>INSERT INTO role_group_menu VALUES ('c599c7d20b44b78c7643bc9668f69379e8939e03', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '48')
<query>INSERT INTO role_group_menu VALUES ('af8419ab67bbad00177f000befc16fa4250029c7', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '49')
<query>INSERT INTO role_group_menu VALUES ('3f2a5276be82c963d3e971a1865f33733b2f1e2e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '85')
<query>INSERT INTO role_group_menu VALUES ('52e1f96ac8cf924f5f2d827b060eb0123cd9f0ad', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '47')
<query>INSERT INTO role_group_menu VALUES ('6bb14c6551deae10e3299688399ee41e37037701', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '46')
<query>INSERT INTO role_group_menu VALUES ('372c0d8b517c73623dc57e35b16d6561a941acb3', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '45')
<query>INSERT INTO role_group_menu VALUES ('f10b3dc59fe6f9f325254d5ed1b1497f005c3505', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '44')
<query>INSERT INTO role_group_menu VALUES ('cf761585e97c0a9ac35b71738097de265e0b04bd', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '41')
<query>INSERT INTO role_group_menu VALUES ('8831c51b470544930bb536a6b384b57bacbd4fff', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '43')
<query>INSERT INTO role_group_menu VALUES ('77b09857b5851e183771114bd2e457feb02ed9f3', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '39')
<query>INSERT INTO role_group_menu VALUES ('d9d33772c11e401d478eec7d4b1bac1753e997e5', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '21')
<query>INSERT INTO role_group_menu VALUES ('ba249535e0881ff4392870d806ae9bc7f8451dd7', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '2')
<query>INSERT INTO role_group_menu VALUES ('967a3e4e24646bb6138f496635f96a92b2752de2', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '19')
<query>INSERT INTO role_group_menu VALUES ('50eca88ad154c2adc7963092e8699df5a7b0602a', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '84')
<query>INSERT INTO role_group_menu VALUES ('0004f34c60926d1ebd490684daf795c5f725792d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '83')
<query>INSERT INTO role_group_menu VALUES ('83cc3a9c03b6951f6cd856a9b140ae4f3aeae01b', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '82')
<query>INSERT INTO role_group_menu VALUES ('177beb1f39cacf6fc8af5bd4e70615f48716f756', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '81')
<query>INSERT INTO role_group_menu VALUES ('a22db3e41e03ba6b42c2ebf882f07a6aecc989a4', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '80')
<query>INSERT INTO role_group_menu VALUES ('45bc11a25551d85f9fb67fea078da7b661f08566', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '79')
<query>INSERT INTO role_group_menu VALUES ('7e0c3b30e3f01b2acc7e3d24c47693cf2d9173a3', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '78')
<query>INSERT INTO role_group_menu VALUES ('bc186f9011ced953b784487c1069116c2dd1f2a1', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '77')
<query>INSERT INTO role_group_menu VALUES ('22e3a410466472a5d5cbbb9345c3ec3e6c6c111c', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '50')
<query>INSERT INTO role_group_menu VALUES ('a93c6140063a111553c0d67b87d5b2ffbf3dc855', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '54')
<query>INSERT INTO role_group_menu VALUES ('79e276bdc483f3e2dca1a25f65f5c3b1af28ce61', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '58')
<query>INSERT INTO role_group_menu VALUES ('ce5bd91108c6c2487ed43caeec1db52b46650624', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '62')
<query>INSERT INTO role_group_menu VALUES ('d0c2632ac24a4e656a6f2478e5db51c9e741bd93', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '42')
<query>INSERT INTO role_group_menu VALUES ('afd83abcb9a79cdd926c0ec73772b8851c2ada7f', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '2')
<query>INSERT INTO role_group_menu VALUES ('2a5202ac22176516f712b07429090f28f765b053', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '21')
<query>INSERT INTO role_group_menu VALUES ('dc18809c653b510c162057568f867b4cae755960', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '19')
<query>INSERT INTO role_group_menu VALUES ('0d0b38006fcc1674d128ed76a1a882b0284738cb', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '18')
<query>INSERT INTO role_group_menu VALUES ('7565891d2211e4ac4c6d6fd819e4e87fe6abebd0', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '3')
<query>INSERT INTO role_group_menu VALUES ('747962d00f886ad596e2e2d82c0391013cd6a9db', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '76')
<query>INSERT INTO role_group_menu VALUES ('73ec4a9d44e2ba149c6694bc86895df8ea7ab6f4', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '75')
<query>INSERT INTO role_group_menu VALUES ('022b034c340357f243dccc8be4f699694effac62', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '74')
<query>INSERT INTO role_group_menu VALUES ('b436c2a2d879e30aec928faf108deba5fe2bfadb', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '73')
<query>INSERT INTO role_group_menu VALUES ('f573e3c77925f0f6e67ce718b89494ee86852ed5', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '72')
<query>INSERT INTO role_group_menu VALUES ('f3d2b1e89e3226e73b3d2f8586d46c3ecabaef5a', '12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '121')
<query>INSERT INTO role_group_menu VALUES ('35f260d5436f39111b49eb3de1fff64cbf38eeb8', '12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '116')
<query>INSERT INTO role_group_menu VALUES ('d77bb6e709a57c7c2edb2c41369cc50e38e209a4', '12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '21')
<query>INSERT INTO role_group_menu VALUES ('03ea674f18276a285c84052ab5dce9d26bff28ff', '12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '2')
<query>INSERT INTO role_group_menu VALUES ('d42f65f99f949540d92ce2997ebe62d31a2f4525', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '104')
<query>INSERT INTO role_group_menu VALUES ('9f874e50241c6226dc69b769cae6821459f9a1ed', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '101')
<query>INSERT INTO role_group_menu VALUES ('ddf1a024a3efe2e1dfe565fd52c6aa10ceb7c031', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '102')
<query>INSERT INTO role_group_menu VALUES ('1709f97da34ed50ea70b6ff6134c88684ce0cea4', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '71')
<query>INSERT INTO role_group_menu VALUES ('e0244bd2b8048888b0ad53120f47afc33335f77e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '65')
<query>INSERT INTO role_group_menu VALUES ('39ff38cbc5d5bc8ab0115939e1c8a55a74971cc5', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '64')
<query>INSERT INTO role_group_menu VALUES ('85453888408893fc629e24fd9d854c4f8c9e6d68', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '63')
<query>INSERT INTO role_group_menu VALUES ('049295509a54fafd2fa3865ca096fbd783b8127e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '62')
<query>INSERT INTO role_group_menu VALUES ('ee1208e7b5b3e0cb5750b169664351e096911c52', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '61')
<query>INSERT INTO role_group_menu VALUES ('ee0f69ee2e30c29bfd50a43e94231ebae08c59c8', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '60')
<query>INSERT INTO role_group_menu VALUES ('7deca72774b32f26fcd33bc06a2e24c96a973a63', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '59')
<query>INSERT INTO role_group_menu VALUES ('6a28c23397d93c8fbc4a91601dfaab073b1636c4', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '58')
<query>INSERT INTO role_group_menu VALUES ('f8a952369d0505a71f2080619fce490d7a499d6a', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '57')
<query>INSERT INTO role_group_menu VALUES ('f75489a981a9e577a16d0b8f56bd44a989c19931', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '56')
<query>INSERT INTO role_group_menu VALUES ('d8eb44e50283fac2ee3c08b4158db767d076a126', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '55')
<query>INSERT INTO role_group_menu VALUES ('5118544005e6945fd9ece6f941d4cd867a346ae5', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '54')
<query>INSERT INTO role_group_menu VALUES ('61fc512bc67c5d511a79b191ba27c54e9a0cf73d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '53')
<query>INSERT INTO role_group_menu VALUES ('e04497ffe7a8c9971893ac19f8e81224f2b51d4e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '52')
<query>INSERT INTO role_group_menu VALUES ('8cf444f8e3a958aac85dda8218364c9e09c4bf6d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '51')
<query>INSERT INTO role_group_menu VALUES ('9c1cac5807358183d7f6e0b121b2a1ecfda842f5', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '50')
<query>INSERT INTO role_group_menu VALUES ('0e1028ed7fa38d24e50040ebe182ef1fe1dff410', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '49')
<query>INSERT INTO role_group_menu VALUES ('87cf2a2e6592b4049625e14dee1d6529e68181cf', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '48')
<query>INSERT INTO role_group_menu VALUES ('34f975509dbae3b3d84dfd1e2018d1e3e1042e5e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '47')
<query>INSERT INTO role_group_menu VALUES ('371b07a55d4d7154d770179c0f4bf648b49bf30e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '46')
<query>INSERT INTO role_group_menu VALUES ('f5c5bea5fb92e2fe69d55722ae11e2321fc46688', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '45')
<query>INSERT INTO role_group_menu VALUES ('d183cde1dfd2e2db18770bf246ebd22aec36f8a9', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '44')
<query>INSERT INTO role_group_menu VALUES ('02e9edb4c917415e612b4fba13b9543a4fb5b6ea', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '42')
<query>INSERT INTO role_group_menu VALUES ('7094b86bb707f9824906506d011225844b42a897', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '41')
<query>INSERT INTO role_group_menu VALUES ('4fa4adfbdcaff19d854e055da0a5376ab826e37a', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '43')
<query>INSERT INTO role_group_menu VALUES ('86ab678c83a96a0b6f995c8b3c79d2249737c4ea', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '39')
<query>INSERT INTO role_group_menu VALUES ('8c40a5e97a23f2ee19ae71b325ba5625b25dae6e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '37')
<query>INSERT INTO role_group_menu VALUES ('50b94e5f68a3ea5212a468805078e5ddf28d6300', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '36')
<query>INSERT INTO role_group_menu VALUES ('b2af475e49fc724e5d890c20db02274cc864b456', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '35')
<query>INSERT INTO role_group_menu VALUES ('876255d084f28d1431dca688566f00e4bd7e178d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '33')
<query>INSERT INTO role_group_menu VALUES ('c35f48e13b066323f808e28720ab04a115ee1ea3', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '32')
<query>INSERT INTO role_group_menu VALUES ('54a2a5359ac59cc762211ed4a3687e630ef52660', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '31')
<query>INSERT INTO role_group_menu VALUES ('e48643a51bcac40e57b0de2520389081b129aa0a', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '29')
<query>INSERT INTO role_group_menu VALUES ('dc49c49a5db5eada6e1ec5d475b30a17a489dff8', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '28')
<query>INSERT INTO role_group_menu VALUES ('ece22756ea84c5048a85d20facf220e088ba0f9e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '25')
<query>INSERT INTO role_group_menu VALUES ('b6c0c752bd9b6f077883f7e02809001bf45ca318', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '23')
<query>INSERT INTO role_group_menu VALUES ('7822f6c84c20c4a0a8a19416b1d730233b73654e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '22')
<query>INSERT INTO role_group_menu VALUES ('4f49e78e8884d5342d5a7207bf731a0e6ac23c22', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '21')
<query>INSERT INTO role_group_menu VALUES ('4e865bb14e60c9df2790fe81fbace9a65701450f', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '20')
<query>INSERT INTO role_group_menu VALUES ('3c19c2515471e51493e26950ce127d00ec692840', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '19')
<query>INSERT INTO role_group_menu VALUES ('f86e7ebe32552cb0d475e333661a9bda25372a06', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '18')
<query>INSERT INTO role_group_menu VALUES ('9e8768b831ebba0f0023549ceb2ecef9b9c6f188', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '17')
<query>INSERT INTO role_group_menu VALUES ('822b01a0517b2d7b045ffed85900f221210dab7d', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '105')
<query>INSERT INTO role_group_menu VALUES ('7a21751f84f99dfe7dc4a43a201061305002dc60', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '76')
<query>INSERT INTO role_group_menu VALUES ('9ac626ec740b70a0e1d35402a18a53e4d6fbddb5', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '82')
<query>INSERT INTO role_group_menu VALUES ('7f869f3c7c7c863c4772c98d2e7777ba5ded47a7', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '81')
<query>INSERT INTO role_group_menu VALUES ('427a668eac2f5ebe3b75e81b887a765b4276e35e', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '3')
<query>INSERT INTO role_group_menu VALUES ('b7131a8387325898b79cd6e938b8a7555faf32be', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '17')
<query>INSERT INTO role_group_menu VALUES ('100481d233348bf45c12defa44e5455c6874b2ed', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '18')
<query>INSERT INTO role_group_menu VALUES ('33e5f8542476e97416b3debf446c3dea6059d4d5', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '80')
<query>INSERT INTO role_group_menu VALUES ('36dc21f27db1d8e879b5d27d9edfc830fd610e47', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '109')
<query>INSERT INTO role_group_menu VALUES ('942c0bad64265a7fb725a2c99d9762bf03085767', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '110')
<query>INSERT INTO role_group_menu VALUES ('f33b6b1b041fb05202cefd1c36c8c8334e944514', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '111')
<query>INSERT INTO role_group_menu VALUES ('8b868ce7e9eac07e1f70a5f8a53f9e4e26e4aafc', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '112')
<query>INSERT INTO role_group_menu VALUES ('22651ddf3e4798bd9e235e90350fab1340195e3c', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '113')
<query>INSERT INTO role_group_menu VALUES ('9512f48f781079a26882e910bdb1a7ba108688f2', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '114')
<query>INSERT INTO role_group_menu VALUES ('4d8e51feae7753d5359f62cf3f6ef6448d553d4c', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '115')
<query>INSERT INTO role_group_menu VALUES ('a994f9d95354803e461d9f7e3b580630a7a3b0f1', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '107')
<query>INSERT INTO role_group_menu VALUES ('c857717a624f754ad61026bf73b42f5ea3233f0f', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '108')
<query>INSERT INTO role_group_menu VALUES ('abbb021de53507c5bb1c0e657cd9f4b911ec050b', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '100')
<query>INSERT INTO role_group_menu VALUES ('4f01955e41e53dcb8e527ee0146aec740bf340d1', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '96')
<query>INSERT INTO role_group_menu VALUES ('30a689a96fdb4ac5565bf2971fe4e44cfa434811', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '88')
<query>INSERT INTO role_group_menu VALUES ('c9763e1a680884c3df485e46e685840ecc988279', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '87')
<query>INSERT INTO role_group_menu VALUES ('1dfce0bad0da75f0cb2fec561e239f7c2d0a5aab', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '86')
<query>INSERT INTO role_group_menu VALUES ('b238acbd183ec338e10a7e4ddcd4b78df9ff0d05', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '65')
<query>INSERT INTO role_group_menu VALUES ('a94558556b83f3b32d4cac6f77b27355f68d2862', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '64')
<query>INSERT INTO role_group_menu VALUES ('6c253774180c2048ba61aab9aac6f57b777d208f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '63')
<query>INSERT INTO role_group_menu VALUES ('9e8388a297f5ceeb3e3a86c891a1b296f6341b12', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '62')
<query>INSERT INTO role_group_menu VALUES ('9eff3f7d7f41db4d77266f0bb1a9db61fe630cd6', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '61')
<query>INSERT INTO role_group_menu VALUES ('25030c335b0dcc273d55f136e91e190341900f4e', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '60')
<query>INSERT INTO role_group_menu VALUES ('97d08739730b9b7ab373da96506a1f5c296ba471', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '59')
<query>INSERT INTO role_group_menu VALUES ('1d7e1ce653289a147571045face90321f89f1c17', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '58')
<query>INSERT INTO role_group_menu VALUES ('7240ee5dffbc65ecdddfe9168ddc92f2c1d29547', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '57')
<query>INSERT INTO role_group_menu VALUES ('26e47a7cc1708475a410cdb5370a29006351e90f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '56')
<query>INSERT INTO role_group_menu VALUES ('7e76ff22818928ac1752393f9723ecd6d0534a04', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '55')
<query>INSERT INTO role_group_menu VALUES ('235574e22836309ead2fcccce86ad229f9f44ab3', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '54')
<query>INSERT INTO role_group_menu VALUES ('f02c2dd946c71bb9cd2628924c0ce26d80f32166', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '53')
<query>INSERT INTO role_group_menu VALUES ('c3be4aecc6188c2385b5a75208f8882a4119797e', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '52')
<query>INSERT INTO role_group_menu VALUES ('49fccb70b65613d0ba2742e033d667cb058dfd37', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '51')
<query>INSERT INTO role_group_menu VALUES ('d10bd6fed35cec3cdbdf730bc49469798279cb94', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '50')
<query>INSERT INTO role_group_menu VALUES ('4dffd4ea2494dc44651d4c903969ea4f7eceec31', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '29')
<query>INSERT INTO role_group_menu VALUES ('b813011e53c7f62c3796f7b988b6694e36f30170', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '28')
<query>INSERT INTO role_group_menu VALUES ('39fb824642c0a4a9d7398c1967d00e32c59a7409', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '22')
<query>INSERT INTO role_group_menu VALUES ('36162c10f4bfd12eb4968981534aaae36a53d1bc', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '21')
<query>INSERT INTO role_group_menu VALUES ('334f26a5d070899adaed2ea7042fbb5aad141458', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '5')
<query>INSERT INTO role_group_menu VALUES ('f7d353551898dd25010172392631e6cdd1e25956', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '2')
<query>INSERT INTO role_group_menu VALUES ('0c3fd8f376cfa7050af3eb7a4347f4c2653635ce', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '16')
<query>INSERT INTO role_group_menu VALUES ('5999b658fbdd2f72cedf44ffa82c584bbf194da0', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '15')
<query>INSERT INTO role_group_menu VALUES ('20caf5adaa1c0679b5d0556f9ae06862690afac6', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '14')
<query>INSERT INTO role_group_menu VALUES ('a17a5ae4adc3b9753e2f20fa655b3243c2f62dd9', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '30')
<query>INSERT INTO role_group_menu VALUES ('56b156564621f9bda064c2183bf34d138b507170', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '11')
<query>INSERT INTO role_group_menu VALUES ('9974d40e205dfdcccb73be83a6cd8c93936573dd', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '9')
<query>INSERT INTO role_group_menu VALUES ('eca9202ff9a8cf0ace02ec84424213f21aa8d8c9', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '8')
<query>INSERT INTO role_group_menu VALUES ('4425fcca94d0fdf5d346656d48e741b19fb42429', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '7')
<query>INSERT INTO role_group_menu VALUES ('b34ec5bc655bcc5ddb38f813e46abdabc28d84fa', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '6')
<query>INSERT INTO role_group_menu VALUES ('40f148b65d5dea56809ddff848b07b4d83dc1de5', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '5')
<query>INSERT INTO role_group_menu VALUES ('93789fffbd7b89d7b3513afa1504547a3fdba254', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '4')
<query>INSERT INTO role_group_menu VALUES ('f54ef2a6ae890ef2681129dc431bf378dac08103', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '3')
<query>INSERT INTO role_group_menu VALUES ('b983fdc7ac61be48504810299f6cdea7121d5078', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '2')
<query>INSERT INTO role_group_menu VALUES ('dc8dcfa95af5502e73fc4f14e4c44bea53d116b2', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1')
<query>INSERT INTO role_group_menu VALUES ('212309f890aeab743681553780b8cea8a62a65ab', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '105')
<query>INSERT INTO role_group_menu VALUES ('c3e81b59a3d1f2a70ea3f989759a5e06cdf5fc9c', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '106')
<query>INSERT INTO role_group_menu VALUES ('e74c89d76fae26ac9183384f32e9b2cdd276e435', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '109')
<query>INSERT INTO role_group_menu VALUES ('c4908216a77ead4d269d0b438d41db3ce5246b20', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '110')
<query>INSERT INTO role_group_menu VALUES ('bb1f3a2e75aceb17840a04b051aa4c17402e56ba', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '111')
<query>INSERT INTO role_group_menu VALUES ('c95fb65de36167c605980a4519e2aada78d68b79', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '112')
<query>INSERT INTO role_group_menu VALUES ('fcc0b7a438dc2b03119409287c637adc7026666a', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '113')
<query>INSERT INTO role_group_menu VALUES ('3c04f113f25953d2c489ee17538dc75d7055ae49', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '114')
<query>INSERT INTO role_group_menu VALUES ('a38630bfa34173e18c16044d2030b5cccd1c1830', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '115')
<query>INSERT INTO role_group_menu VALUES ('1b5622b950bfc76b76e600403f0c562e1dde7a12', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '107')
<query>INSERT INTO role_group_menu VALUES ('d83a72301c09a516bd29e275edb6becd17afa12c', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '108')
<query>INSERT INTO role_group_menu VALUES ('01ab8ab346f8d947435928d66f6ba7b372ba5213', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '121')
<query>INSERT INTO role_group_menu VALUES ('ee6f68e1021da043425d6ea56b338a78fa799162', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '120')
<query>INSERT INTO role_group_menu VALUES ('cd435d1bcc4832a944ea69313b9abef4bbe09a0b', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '119')
<query>INSERT INTO role_group_menu VALUES ('34e4dd29c9cb5bb3de81c7557fabfc92b9921d7c', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '118')
<query>INSERT INTO role_group_menu VALUES ('3fd49910003890b7625b44799cc97a0c1da5efa1', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '117')
<query>INSERT INTO role_group_menu VALUES ('4df1904b15792aba57c9f085d94240d0988f404e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '116')
<query>DROP TABLE IF EXISTS tindaklanjut_internal
<query>CREATE TABLE `tindaklanjut_internal` (
  `tl_id` varchar(50) NOT NULL,
  `tl_rek_id` varchar(50) NOT NULL,
  `tl_desc` longblob NOT NULL,
  `tl_date` int(11) NOT NULL,
  `tl_update_date` int(11) NOT NULL,
  `tl_attachment` varchar(100) NOT NULL,
  `tl_status` tinyint(1) NOT NULL COMMENT '0=default, 1=ajukan, 2=setujui, 3=tolak',
  PRIMARY KEY (`tl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS tindaklanjut_internal_comment
<query>CREATE TABLE `tindaklanjut_internal_comment` (
  `tl_comment_id` varchar(50) NOT NULL,
  `tl_comment_tl_id` varchar(50) NOT NULL,
  `tl_comment_desc` varchar(255) NOT NULL,
  `tl_comment_user_id` varchar(50) NOT NULL,
  `tl_comment_date` int(11) NOT NULL,
  PRIMARY KEY (`tl_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
<query>DROP TABLE IF EXISTS user_apps
<query>CREATE TABLE `user_apps` (
  `user_id` varchar(50) NOT NULL,
  `user_username` varchar(20) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_id_group` varchar(50) NOT NULL,
  `user_login_as` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=internal, 2=eksternal',
  `user_id_internal` varchar(50) NOT NULL DEFAULT '0',
  `user_id_ekternal` varchar(50) NOT NULL DEFAULT '0',
  `user_status` int(1) NOT NULL DEFAULT '0' COMMENT '0=del, 1=active, 2=inactive',
  `user_create_by` varchar(50) NOT NULL,
  `user_create_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `fk_user_id_group` (`user_id_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
<query>INSERT INTO user_apps VALUES ('23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', 'fariz', 'ed47cb2ce4521277d1e2af021b2ca99f', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '', '0', '1', 'system', '0')
<query>INSERT INTO user_apps VALUES ('23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', 'angga', '070667f23e128dccd425c95249cd55a1', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '9ca611db222a22298db1aa06a02564eebc25c3f1', '0', '1', 'system', '0')
<query>INSERT INTO user_apps VALUES ('d7e9dde4e18a1e8b66380ae236ecd6c4a06ed715', 'rahendro', '9aa7cff28a73fe749aed9f024057142b', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '386a8e59e01d7809a56d25712c732517a41c8f22', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1446961490')
<query>INSERT INTO user_apps VALUES ('e4599338f7327828f7d63ede832c73ff8b479cd6', 'saifuddin', 'dc520e6803537e6d64629efaebdbca95', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '2ef918b90cceea0cb221f8beba3dfeb71efac35a', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1448155424')
<query>INSERT INTO user_apps VALUES ('78b3a9771697fb3133f292e0981c3a2044ee6a35', 'iqbal', '1bca512af46c47ece60151cba38dacde', '7090cfd596225052fed383a3332fcc8dc72ba918', '1', '466b75ee6ab08d2791ab12a8bfa0f9d1839e3931', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1453338876')
<query>INSERT INTO user_apps VALUES ('9643952cd0956e5747f8df6f8158267efdc18214', 'soma', 'a6e6d9f341954a0450f065bc0e58e9e5', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '71a471602a26b982364134fd720496108e238c94', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1453338946')
<query>INSERT INTO user_apps VALUES ('e3041d9ce1f190e6149fc2316d2827c269a4697c', 'riza', '98054aa020f3398727395711922c9c23', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '7e9c38d8681b442bb7d0522f1db758a8dbf56edc', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1453339242')
<query>INSERT INTO user_apps VALUES ('e5ff8db6c05a9623fc08e4efcad2ad91205fe85d', 'urip', '43b34abf7518fc672156d2cf1b7a7762', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '125b77c8182f0ec54100ea366979b833622c3e1e', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1453345560')
<query>INSERT INTO user_apps VALUES ('e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', 'dimas', '7cc8ca1f9e151059071ecb3322f59ba8', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '9e8da557a7d9964daa8688b220fda26c3afb35df', '0', '1', '9643952cd0956e5747f8df6f8158267efdc18214', '1453345653')
<query>INSERT INTO user_apps VALUES ('35ac15035d7d084fd184adfd0a83d3193bffcb3f', 'bachtiar', '6a44799cbf7585810e893643cbaa44b2', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '789861fcbaf539a61b6846a9d662c1504bf43b3c', '0', '1', '9643952cd0956e5747f8df6f8158267efdc18214', '1453345683')
<query>INSERT INTO user_apps VALUES ('077df46bf729f15897dea1bda35bfcad39d019ed', 'ade.tabah', '3dfa7f400c6a5c8a540401d92100cf6f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '860bc3ca65e75bbfe6cb1d621000baa60b0239dd', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453349303')
<query>INSERT INTO user_apps VALUES ('20a0668532b48a8b991c22b65d69a575fbc55380', 'adelaide.siregar', '7bd66ef332e8c746be8a02906bf77a05', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'c5013107fd95a22256e248acb6da5652c8827e25', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453349709')
<query>INSERT INTO user_apps VALUES ('0e15b3f9a3ab15c75108b9ca469a0b5dfdbe7468', 'adi.nika', '7e93c9553b1af530b75ee9c6689505a7', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'ebcba52e593aee7138fd2977fa7ab892f471e46f', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453349893')
<query>INSERT INTO user_apps VALUES ('94a5b1bc0ebad0402e5ef37dfe1764c904d2ed2f', 'adianto.nugroho', 'd9a4e53182a376eb4c140314d4446dbb', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'b78d298fc4a0cefbe1960a207a715a57a5528403', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453349924')
<query>INSERT INTO user_apps VALUES ('c0c4fa1dc0b6e7e5687f5e2ac3b108e0ade35c3f', 'agus.sulistiyono', 'ad07a311ab1c5c4453f1723f299f759a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '213a170fa9c25cce43c9f92c9d69f3a8cdab29e5', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453349977')
<query>INSERT INTO user_apps VALUES ('3e283a9468af1ec392019a2a38686eed52af7e1c', 'ahmad.rofiq', 'd3196c1d8dbafabf527f67dcd35acc88', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5553e2fb14392cc03800cfc6884c6596efb39954', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453349992')
<query>INSERT INTO user_apps VALUES ('4c94ac2110c670b15d2c3c7395d4c5916d0a4266', 'akhmad.samudera', '03354f45547fc6b24249d08b0cda0057', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'b1ad6402bfff82c6482117e510bb2c023b30155c', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453350011')
<query>INSERT INTO user_apps VALUES ('f32b243c46eb3b4b9f080c72707cbfb6ac8e7aad', 'akmaluddin', 'e57e4a2ebe09b9039cdb96166e7ae3ce', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '233a068f9fc8b02ed16efb754d106bbe224139ab', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453350026')
<query>INSERT INTO user_apps VALUES ('a62f5a85de32a80eee1d21fc25ac85d45b643e02', 'andi.nuraisyah', 'f03b8bde39c5bcf3b3f6d9a2e4b39a5b', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '7d8bc0406f44c677b3da589bb7faa29e525605eb', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453350042')
<query>INSERT INTO user_apps VALUES ('666fbf6831246ed7dc0fabb22ce6dc0ab0a12795', 'anton.apriyanto', '9fb89124ca14af82dd663d93305c638a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '090e98582e7507d854ba75986806899002003856', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453350055')
<query>INSERT INTO user_apps VALUES ('22d8ae8ac1f1ff743cceaf3fabc9b0d545259877', 'ari.purwandari', '72ec5703d262739ac9821f8e6507d83d', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '1', '32665e0a655d1255519381cd5ed2bfb9ca90cf74', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453350082')
<query>INSERT INTO user_apps VALUES ('4f70f9b3266468016dda00c473f09c0423b9e19d', 'arief.bachtillah', '1c40c5ce538d78330c507e601cefb85c', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'a9d47f5a6ab7c52eae1cb7c69ae19199d2dcdd20', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453350094')
<query>INSERT INTO user_apps VALUES ('98ca75b301eebed72b813f5aeac9067ede7ef820', 'arief.yudi', '7125dd67cba519287e675831df63793a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'ec88cba6f2bf41480eda19be990e77200527a899', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453357161')
<query>INSERT INTO user_apps VALUES ('0f39eae351485479ae9b3af568ec5b63b1bb1332', 'arif.satriyo', 'b3f3ba261b8ae03143bc280bcc18f3ee', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4677aa85be960a0547abd509f93e80862e896ffc', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453357181')
<query>INSERT INTO user_apps VALUES ('ea6a55a3c3da249fe3dd0ba41508e8e7efaf95c2', 'arlin.zulkarnain', '92c1e30d0217769ce6ae2a9d6fd4a067', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '3b99a384de8537b58fcd88642a77ae6aeb3b8bb8', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453357579')
<query>INSERT INTO user_apps VALUES ('fb25f1af8816a6f94740df2e9d73618064e0844b', 'asih.wahyuni', '702c036141c15764cf4782eca3421fa3', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'b5d373335658c7570ca050c2e158a899d2ab44a6', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453357614')
<query>INSERT INTO user_apps VALUES ('8b862a03a30e19e2ca5cc2e80b977ad7ad90f1af', 'asnawi', '865cc922493d39667ffc949b5882d30f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '59fcd0cf61d741ef7ad5f24d4d2338fd7b280d02', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358240')
<query>INSERT INTO user_apps VALUES ('934117f347d3f4fe90bb6785b8919549eaebf229', 'bambang.kusyudhono', '2902e6e019c86d0355ef4b7d55b76198', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'fc90c49a0ae6e55446ad3bd83affbe66db748722', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358280')
<query>INSERT INTO user_apps VALUES ('ae6c2f7f5e65b950a8742a027a66315f0d428d8c', 'baseni', '23811332ea940dd75c302beb56848394', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', '0983a71325d08b912aabf45b1b7a05d9cc2988d9', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358299')
<query>INSERT INTO user_apps VALUES ('8b9a6b1f189a8c23c5c8e2df0ec401b2b57aacd3', 'beni', '37f7b693bd4611e6f9e488c29926024c', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'cb544d1c3bad9d627f7e0a0986624559e1c7eb48', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358315')
<query>INSERT INTO user_apps VALUES ('b9bf500ec812501a81def49bca627d9e6abe1476', 'betty.shanti', 'ef6c71a2f463181476878e5992603780', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'a0a3d36903e0fc6ea8fa805677accaa655089dca', '0', '0', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358342')
<query>INSERT INTO user_apps VALUES ('fe171a7ac0dea7b8ca1ea3c81fb2cb4161fd271a', 'dadang.ismaryana', '1230cc738925bc40c30aa1c033ad658f', 'dd05ae3db0b445fd8fe9d9341a8ad3a4f41b1d43', '1', '50b397b32ddea2ef3ffa8a900a828cffed020f27', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358407')
<query>INSERT INTO user_apps VALUES ('ef27c889a4f5368749d42dc0ee860c5519fa98c5', 'daryono', 'a0d0d139dae8681b025d0916d80d5973', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', '922b5ba97fdb767dc6ff64ed4710e8602e7f93b3', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358448')
<query>INSERT INTO user_apps VALUES ('fe0358359aa392a5c4ecad6b07e388d92ae09945', 'denis.puspitasari', '3a4114a77ef1e6880e5ba58494a6f1f1', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4bf1fac689becfbe246368ab0c12aaa75dca3eca', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358467')
<query>INSERT INTO user_apps VALUES ('7a8b899c3a6386e8f5985c959caf077ca1725415', 'dicky.rachmanzah', 'e417ef2a755b710d8a01cc1f398cfdce', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9fcf43da5763d3cb1b19d05affbaa7ff7466b207', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358641')
<query>INSERT INTO user_apps VALUES ('a4bddda63106a2746d0d10d1a266c1affe875a3a', 'doni.wiryadinata', 'c86618812f0f066530c0f0b45bf7cb0e', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '355c46afcd99ceba4b1b70590266d26c7e4af7f3', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358715')
<query>INSERT INTO user_apps VALUES ('f407ec07d07298dd2e91ac139ce4127aff0dc50e', 'andrijati.isnaini', '7081646b5a8f1f5700360a1d1ee1cb75', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '664a1e43d2d5becdfc996e2ed1a85f1441a682cc', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358739')
<query>INSERT INTO user_apps VALUES ('68d11817eee161f797e78fa73d195d8af64d472a', 'agus.purnawan', 'd6304d4f3294fa52321f6610ee819d7b', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9c5090675728a587f4f0f47455a0dccc2b3e9c7e', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358764')
<query>INSERT INTO user_apps VALUES ('90ab8b9e29ac845df37078f8cf291101130544c2', 'cipto.hadi', '17ef0b25fba4dd02168b50a4ea84b6a5', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '1', 'e11018945aa6a3f2ca12c02b527174e163fad485', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358790')
<query>INSERT INTO user_apps VALUES ('075ae3862aeddb8184f050a0a5c4539418724646', 'm.nurochman', 'eb7bd4cf020bd36d6aa393d15fdad2b2', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'c905bd016fe30e0b0adeac312a0298430a47c088', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358837')
<query>INSERT INTO user_apps VALUES ('2a7dec025dca242afb1d091dde502d0b41308c17', 'mudji.prayitno', '19801de49feeb9d0f520f7f94de37757', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '1', '6d34b61ca6cea4fd5420465aa8952f4e7e824dd9', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358861')
<query>INSERT INTO user_apps VALUES ('3c68fbaed06095db8722cf84123fe99500261d8b', 'aris.pambudi', '10e5a25d8b505c104d745df0d1d841f7', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'de5af966e5fbfe0a7c30702820853e124cb89a43', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358891')
<query>INSERT INTO user_apps VALUES ('dd73997e69d621535ec0438afb0ef0ad41cd6bde', 'dungdung.septina', '68e1bcbe4de343ec4df08481d3694fff', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'fecf5d238edff69d20100a54050b7e4c0ae2dadb', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358915')
<query>INSERT INTO user_apps VALUES ('05cb2516b93691dc4b8b128506963c15ca144b6e', 'dwi.purwanto', 'a0ec1d9a58018a1232e68451001d4d80', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', '57271af1eb3e62cf86d3be222f3ccf6f2d849813', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358968')
<query>INSERT INTO user_apps VALUES ('8fbb90b037036c71246441a0c99c76ffe0b163ad', 'dyah.ika', '908923ff0ae9795423c0980d9e9f6a5e', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4ea8978809a1c438c145d54ce9d21b0c96423b77', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453358992')
<query>INSERT INTO user_apps VALUES ('1f48bda7be8fe87a43932a963ae68de14d94759d', 'efriyadi', '2597b9fe35163ae5e99bab26d3ddfc21', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', '78a47e164590638d7a21f0192566f2b267b89f06', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359055')
<query>INSERT INTO user_apps VALUES ('f9eab887dd689f3a87fae9acaf321d4a4f9e93d9', 'eko.sugeng', '5a0490fea9191b91f9f12287b8fc94a4', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'b4bbf634a06a4f220186a36cbfb67d1d75904913', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359075')
<query>INSERT INTO user_apps VALUES ('5d29c356f600abc2af52c2f3f3180c4fe792da04', 'erwin.trias', '893e9b867f5d7c5625f299eca229ffca', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'bb8da27abb0438da4026d01865aec243a330e983', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359142')
<query>INSERT INTO user_apps VALUES ('64bc1136f5d7529ab9d0c547c1c39ce538f2ff8e', 'erwyansyah', 'bb98d12bd8eab2e0ae2e782d40595248', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'bf52997c9b73c786d11b3df5c3b265eae37ddaaa', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359179')
<query>INSERT INTO user_apps VALUES ('67bd6c3af1569154d071fbfacc2c96a54c5f11b2', 'evin.nurviani', '7125dd67cba519287e675831df63793a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'b5b79ca5f67b63983fe77603ac4a95faa5fb27ca', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359206')
<query>INSERT INTO user_apps VALUES ('70c770b5f5e9b5e063ac6be0379f525295f664ec', 'faisal.reza', '0ee1d1f695a955e7906efef07f6ec102', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '2e313692dce791f77733ec53f601b718e761734e', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359230')
<query>INSERT INTO user_apps VALUES ('56a30426bb8950ea14a17fc270730ebd160e7ab5', 'farida.farid', '9fb89124ca14af82dd663d93305c638a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '0558bc17b2fde7dd1e1b1256f6be887d10b88fdc', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359268')
<query>INSERT INTO user_apps VALUES ('933bfc8278308a9e154f85766973f26d9f543f5e', 'febry.budianto', '0b39631463dd3db52ef646f12a811ac7', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', 'f00a92353d7390562031914b6c442f4cff8c05ae', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359289')
<query>INSERT INTO user_apps VALUES ('983da3a3cd59e253ce016d00d5b14a8b27043b74', 'firman.fachrudin', '951f618a7228dc7444263bc289ed7f76', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '150694793c3457826e8f43208084c41e900a6d5d', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359341')
<query>INSERT INTO user_apps VALUES ('3741742dfd147d96a5a5750c2e4c0531f28c74ab', 'fitria.ayu', 'ad5ed2e884b504a62fd4ec8b29301a95', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4f3b45eb4c841ef2c55e4b56ea23171e9786e5fb', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359361')
<query>INSERT INTO user_apps VALUES ('602e20a76480c9ec223403b4481d1f55ff68b026', 'fredy.haryanto', '8d90843c500deb648c4e2a65f064b7c9', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'a18b8bbe1c9f47592328bf8571302c8b15bf2ecc', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359389')
<query>INSERT INTO user_apps VALUES ('b16800310a6e5592ac3254e9034c45389cdba58f', 'greget.anggaraito', '8913b67a3036441926e4b44dd701826c', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '99df5bc36afafe43c7d15aa27b23e2e93ae6fa7b', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359412')
<query>INSERT INTO user_apps VALUES ('d769e2a5d35493bfb42df39adbeefd75c9adc156', 'hamdan.n', '5cd4c2d35f7f23a54b52cdef17060a0a', 'ff6c97274745bf929be5d651849c64d7ca1b405d', '1', 'b53a680bb6bff1ed22dd6401b462689c929a8ff7', '0', '0', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359438')
<query>INSERT INTO user_apps VALUES ('74450c8033b79175106fcdb141a7ddba265ac2c3', 'haris.budianto', 'ab7245c638773200ebcfb61f5a784c36', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9fee3b29ce9ca7801d043f4ee1aec99ee22ddf68', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359490')
<query>INSERT INTO user_apps VALUES ('76bdcda3cc2ee1882285f9a836a119641a467fe2', 'hendrawan.saptabrata', 'e417ef2a755b710d8a01cc1f398cfdce', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'f9778e7cb63959bdb451ab15a7e40fc7e821ddd7', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359527')
<query>INSERT INTO user_apps VALUES ('737eefcc5c796602a4c47081fdd5e18a1177dfe5', 'herimawan.masykur', '0f32f472d8dd5235a5743a1d7e678b56', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '3e2bb3a859d31fd94fe79a89616b35b67a947150', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453359570')
<query>INSERT INTO user_apps VALUES ('2f13591a5a473af95f41f9d7ea2c4087c256800b', 'herwindo', '32815839eaaec7c06cfe1b6953395bd2', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '48a88a5a1a4343bcf070f57d8dc51a0812670dd7', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360027')
<query>INSERT INTO user_apps VALUES ('b98fc2700cc120ed1ade601f06a99161668eb30f', 'ikhwan.syahbani', '03354f45547fc6b24249d08b0cda0057', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '88f9f768caef0e1884c91428877b426266b9cbb9', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360101')
<query>INSERT INTO user_apps VALUES ('0023679bcfbf4690c6d800482359b235b786ead6', 'indra.kurnia', '3279290eface9cfc35be005acbde925a', '7090cfd596225052fed383a3332fcc8dc72ba918', '1', 'fb38af2556b5a8c3fd579058cbeec5f55dd9eb4b', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360119')
<query>INSERT INTO user_apps VALUES ('ce6f35b346059d8a6421f91b33b490b4a2588fe2', 'teuku.nilwan', '7e0c661884533c571e47b4d59852c6d9', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '1', '3d6f5cc54bc0141e60268a720baee1488e4bcbfb', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360139')
<query>INSERT INTO user_apps VALUES ('63b269a5c738267cef05ac82d52b88a16ccc8e92', 'bambang.sutejo', '47a4416b3b7a620b71889b28b17446db', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '1', '24f76a5fc3736611b6c721eebea68040a70d3f15', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360161')
<query>INSERT INTO user_apps VALUES ('1df5a26b9907d398e045cea4c493db1f1b9e9570', 'herarto.h', 'b76fddb5fc3b1aaae9a83b20bfc2bc79', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9bbc008c24b3abc825be76e6514fe25c9fe8b903', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360187')
<query>INSERT INTO user_apps VALUES ('594bb46f52c9eab1684bcc80370dac50a766d766', 'agus.saptono', '03354f45547fc6b24249d08b0cda0057', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1893b1f5bb6ac24c8a9bd36ae44926abf9a6d451', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453360239')
<query>INSERT INTO user_apps VALUES ('d5279a58955bc211e344f31127ea8f4dc2a32009', 'tini.martini', 'e67b2ffe13aed290bc5878a8310b57ca', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '091b78690ea7ab56b3610166f094e0019e4d415a', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453697516')
<query>INSERT INTO user_apps VALUES ('99f081dbb23951d278837b3794eb2424f23ec519', 'emmy.zein', 'e49325f1ace5d4183c9a1237c4dad7bb', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'e3145d9927a9ec5274b0e410adbdd8bf24293470', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453431261')
<query>INSERT INTO user_apps VALUES ('8b9d3a18b68d3c59ef42177a35c004687c812fc2', 'i.gede', '9b32db87ee318324709713b4362581b4', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '0d2d4d532d66621551eebec2830d5df8fc49c1d0', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453431338')
<query>INSERT INTO user_apps VALUES ('4e8c7a0e8b46693ac1cce2a5cf40048a0acfe4ce', 'ida.kusuma', '49b73275ce8853ff6dc8bb3cd13c1c57', '12d8d424f7b5b2cdc7e5f1fec41ca54e365eca66', '1', '76f2c93587f3eb9048391f6f599e8e7d41566055', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453431403')
<query>INSERT INTO user_apps VALUES ('1412dcb4628c56f72c73f5ff8b5c49c5e7f5cbc5', 'iriawanti', '7e0c661884533c571e47b4d59852c6d9', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1d5ebf543995629fa3fd24eec79bc6a6e6742d2c', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453431429')
<query>INSERT INTO user_apps VALUES ('3b5c50961c07252b34b044b7cde1f2c277b0ddb8', 'ivy.silfia', '82af42b1ab8c24574b2c39fec9ed873e', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '48c22d37f7caebb8149f6824b1dec8c24ae947d3', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432228')
<query>INSERT INTO user_apps VALUES ('a3dc9f8097f0f635af52c369b7640c5d98aab450', 'jayeng.c', 'b743abff7825d65d494256364a9dc963', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '1', '29316704116d545c86beb6f062df10461b8b6db7', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432253')
<query>INSERT INTO user_apps VALUES ('b373149b2fbee3aec3ccc56bfe82c95fb832e1b2', 'lutfi', 'b5d29fc3913f5f373356e7759746c24a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'ba145cdf14d0c219865505b83dd7dafc3e22db8d', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432271')
<query>INSERT INTO user_apps VALUES ('eb344b478578506ba86c98a5114cf79a102621af', 'noor.adram', '8913b67a3036441926e4b44dd701826c', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '7921e0d94d5b6b32f08a4267f5d3a514e36f406b', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432296')
<query>INSERT INTO user_apps VALUES ('343094f9918e021c1bfa747e06c597375f56543a', 'nur.arif', '04796f513a4d3771d1bb915c8fb5a1dd', '4d6cb5538c7cfbd6b1ccbd881e0e97e69183b4a0', '1', '13bbf75217cde8d494f986fd9b545ab20b2d8250', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432320')
<query>INSERT INTO user_apps VALUES ('c106a9bf59a6c596a5a97b45aa7c65f356d14382', 'raymond.r', '6573ee2ef02aca5ffdae21de5ab43108', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '51931282a237a358f6af29c0d2d1e732a182c809', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432345')
<query>INSERT INTO user_apps VALUES ('c37f1b95fe6acbc3a19ecd8f2ea692876fed4c89', 'wahjudi.poerwanto', '083b66e977b04ca321a96e47319e8c0d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'cd39b4bf645b30a1c4100045df6cedbd7b65ce65', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432368')
<query>INSERT INTO user_apps VALUES ('e3fbe4b49424c22009439316402300b37c264482', 'irman.suwandi', '8aa24a0c7d96be4e45349bc4bcfc7906', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '82eb977b0a3dacfb016e09600d7f2647dfd2363d', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432422')
<query>INSERT INTO user_apps VALUES ('3b4db1df5e5d67c28d40d600348d84336f2f8717', 'irwan', '7a2e2a048126ae49a607b3d89afb1b85', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'da97345c3dc9f8fc3f0a3da5d4f7093287cc178b', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432443')
<query>INSERT INTO user_apps VALUES ('68c8c01171b31f47a6392564fe47ea417a0ba91f', 'iswahyudi', '584ba1ba218bc1bac2a0346b6cf8d01a', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', '3224b271a4bbf1872842e175d003d86c47137779', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432547')
<query>INSERT INTO user_apps VALUES ('c4042cc3ea33af8135124ef91f13b12583e5b878', 'joko.sutrisno', '870f3ce243614850f47499567fcdd404', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'f354242110a9139e0bd32245bd64f1cc61b32bb2', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432608')
<query>INSERT INTO user_apps VALUES ('afc446ae1f35daa600c9c64fd85992a2b44aa13c', 'junaedi', 'a81697b0b4e53ae74b4c551959b3cdb1', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'd3e052ab1f21b087ffb7d1e95d46619c10d01035', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453432628')
<query>INSERT INTO user_apps VALUES ('75b8880db5d7e9e06d0dab37bf2bc3f99083632d', 'kholipah', '7081646b5a8f1f5700360a1d1ee1cb75', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', 'dd6efe7b7db60f0f47ab67cb1b99d430063a7dee', '0', '0', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453433128')
<query>INSERT INTO user_apps VALUES ('2670cea0f1d365c7d0f820aac582ad30c93b449c', 'kukuh.priambodo', '888271d7d1af33904b29c7f2b118477b', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1d9587d2aca9ea7de2df6491fc5f6a9352595b8d', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453433270')
<query>INSERT INTO user_apps VALUES ('e9502b9a03937d3dcf5968631e1dbfe1782643b1', 'lalu.sukarta', '1fd3a211cad163d7fc3f70d1b6fce7d3', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '46317c2e00f7bb8b077de0a8e79580178c5da902', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453433327')
<query>INSERT INTO user_apps VALUES ('4c5dc152d6f2d79831cb4f733531a2432f014384', 'lestari.tirtohening', '72ec5703d262739ac9821f8e6507d83d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '0e060309909aeb34bb054b9ae4ab7654a0c8ebb4', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453433431')
<query>INSERT INTO user_apps VALUES ('d1a46f5b2c35edf36431399772aea76b54254caa', 'm.rachmad', 'fc07589936874f3bdca8279e43ace779', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '8d6e4fb240a86b5716ab889d5ea703e0eb912bf9', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453433882')
<query>INSERT INTO user_apps VALUES ('70f041f5931014b1d61a85a30ff689f84de24c9c', 'syukrinasution', 'cb8463876d0f2e8297fb6f55037a5123', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'bfb8ffe87e633a2dc63ea0a0ee08585c5c07f703', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1453434103')
<query>INSERT INTO user_apps VALUES ('c7d5c0996c6439ec311cb8e3d39d31673314a10f', 'maman', '16be77af390e42f255dd034b61f5251f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5a582592ef40ff4e8456db19a09e865175d2f969', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453453774')
<query>INSERT INTO user_apps VALUES ('832c9f9ea454a9d02f32c1938572465d741fe32d', 'maria', 'ad07a311ab1c5c4453f1723f299f759a', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '76acba87461e36ae35574577f267afb5ad7c18d6', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453453856')
<query>INSERT INTO user_apps VALUES ('25b88a610a089f181957ef49df808f1496e9f627', 'maryani', 'a1a87d82d1869368e8a702b1679cf80d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5d2b247387f4b802cb01b34fc5c304c522582fad', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453453899')
<query>INSERT INTO user_apps VALUES ('6af9532551cf14c8ecc88c05f0622483441da466', 'mashudi', 'f23c016f66cf39aa0bfc4827a65677f3', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1084cb61c26e1927d9b918fdd6b66b834f7e41de', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453453934')
<query>INSERT INTO user_apps VALUES ('c0695b36712134a4c7b906f40fcc6b53d272d495', 'maskuri', '870f3ce243614850f47499567fcdd404', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'bdb46504d1deacfb5c2d4ae943632c9a179836e7', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453453973')
<query>INSERT INTO user_apps VALUES ('0a235a810350eafe67fd203782cf63e527343c5e', 'mufidin', 'a1a87d82d1869368e8a702b1679cf80d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '720d5dd80b2d56c07306a4598adc4455323e90a0', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454076')
<query>INSERT INTO user_apps VALUES ('44b0dd579b2bdf888df5f4549908d1d5e3031bb9', 'nasrum', '36c23efb832790cd2767dd606fa8d087', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '82a298e0952151ec7d87a4541471cad8effad6a2', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454110')
<query>INSERT INTO user_apps VALUES ('7ad298521be2ae7fb205ad4e5e049602dbb0ecd8', 'widodo', '9c695bdcae5e7ebcd10e3176e15edbcf', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'f904a2b03b89e3d8a5713ebc69d1f53d8491505a', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454143')
<query>INSERT INTO user_apps VALUES ('19d66ee0cfcac169ef436d1ca4d1b5315799ff47', 'natalisa.fitriani', '8ce8489c1d83fc941d9a3d86d25e4fc1', 'dd05ae3db0b445fd8fe9d9341a8ad3a4f41b1d43', '1', '6f66e764868f5820f27d2b00cd2ea56c5c265e75', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454242')
<query>INSERT INTO user_apps VALUES ('69c67b77af50d6208dd7b15bff8f6ed5887245cb', 'neneng', '6ca72657a6ed5df5156e3ed22245dbfe', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'cfb54194ee11dcb1d79f37b9a5b75813cc3d6d47', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454270')
<query>INSERT INTO user_apps VALUES ('41cbf54f9d1d7762c989a74eb45bfb11382d3dd4', 'ngali', '095cfdace96a70b50e66c6e566f3b996', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'a1e5bef88b8cd95ec6f213c2a059d8942da21cb3', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454299')
<query>INSERT INTO user_apps VALUES ('9fd2fe0ca7102534cebfb8e3e03b2b4c30944e6f', 'nine', '1d74a5ed904dd77140b7b86697085ac3', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1de030e87579f9c8e4edd76ec29b37a142ba12dc', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454333')
<query>INSERT INTO user_apps VALUES ('8bf430ca7b1684d1f421f3a2cd91e046a1c285a4', 'novi', '640a397b49789f8186c763d80bc42dd8', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9421c99f97dfd5a9e801a24cca093dac283a09f9', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454370')
<query>INSERT INTO user_apps VALUES ('e15282c0cab4a2565cc28199a3076140e86cf28b', 'octa.agung', '86b9f1a32112e89ea5681a19a4d335e2', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '3abda02791b4247c508b06a9ccdecbbbdd0ce9cb', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454400')
<query>INSERT INTO user_apps VALUES ('13f244cc8d1066e74162f7817693da86323b998b', 'wiwin', 'a90630a041696add0aeb9a0ca2b91e66', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5acbe3461adb35da71cfd946c12d6f04a2918952', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454456')
<query>INSERT INTO user_apps VALUES ('e736cf7f067a5cf7cb6160ff9ce1e3864a2779d3', 'ria.novitasari', '03354f45547fc6b24249d08b0cda0057', 'dd05ae3db0b445fd8fe9d9341a8ad3a4f41b1d43', '1', 'db8721470f106a10430bab9055932726d8b5ef0b', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454600')
<query>INSERT INTO user_apps VALUES ('9a92413ebe4a8006fa6dfe02dfbbddf0075e2114', 'rian.samara', 'b76fddb5fc3b1aaae9a83b20bfc2bc79', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1e2ae91733f9ae8f25517b03ad308b802e245576', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454630')
<query>INSERT INTO user_apps VALUES ('c15cf6427de0a5edc29097439d0bf55fa135d9e2', 'rianawati', 'd827f66d4f05ac8b433270e2592c10fa', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', 'd17b4999ecdd956626895f9da275e1fc3f41c432', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454662')
<query>INSERT INTO user_apps VALUES ('87246ac33c1606c0fb5c4e834e081ac1d28d3ed4', 'riyan.ramadian', 'e07492ee8ca7ba861f611f474edf797d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'af76e898a62208a8d0762e75ee2f7062327a8f65', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454702')
<query>INSERT INTO user_apps VALUES ('1f3ebfa51c790ccdc8770459abf0687936561982', 'rokhati', '5bdcc83d8cf219fbe00a2d53f0ab379b', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '3414bd406150355dd1d76180a7f9340567fc6862', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454737')
<query>INSERT INTO user_apps VALUES ('5277eeefc38ae1033d0406b9e48592a2e66e7567', 'rusihan.anwari', 'a5def8180f63ffc4b5f0deb12698475d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'e44e6d98005d6689f9e72f2e7d422275a4650886', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454785')
<query>INSERT INTO user_apps VALUES ('5eb9743f6df9b5077163cad874c4dccf9fd859fc', 'salsabil', 'd827f66d4f05ac8b433270e2592c10fa', '7090cfd596225052fed383a3332fcc8dc72ba918', '1', '2a1e61abc85bc11afceecfe91ce6e7ebd8cc496a', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454831')
<query>INSERT INTO user_apps VALUES ('c248d381212397562980ef52304db13605a1af29', 'sapnianti', 'd874b47269dda1fa651f394750e78e8f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '58fea1c2d6a47daad26cfdbcac409af0a955fd2f', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454865')
<query>INSERT INTO user_apps VALUES ('40055ff1cd8bf3d47c85c0154687b95207167352', 'setyawati', '296a0e259855e539468bbb4d35b13eda', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'c310aa10767e6d46c784f5e97463f53034e241cf', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454913')
<query>INSERT INTO user_apps VALUES ('76664a30ea1932c88cbf921e5f69bbcd71cc5308', 'sidik.parmono', '095102a6b6f240e1cfc80de1403aecba', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '48342f36526d0aed453fc8ded5931ff423bdf99a', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454939')
<query>INSERT INTO user_apps VALUES ('351e3e71dae7b53bf49823b77e1c0c3c2353f029', 'sigit', '08e346dd6d1cbb219a2ebbc7f33c1f36', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '1', '95519be464790115243437effe585452f7c67b9d', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453454979')
<query>INSERT INTO user_apps VALUES ('50fb3b18b502dc93e11ecf9535a4fc03c7b13e09', 'siti.kustilah', '8dcee0861396943059571ddb621363c6', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '394d2663e534ccac3c6308cf9ccf2808e0f23205', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455049')
<query>INSERT INTO user_apps VALUES ('366f11fdff9945c566eff4485a6d9d8308d3e079', 'sri.endah', '47a4416b3b7a620b71889b28b17446db', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '2a79ee6da8ec14aee71f9d9bff67fa1f26099383', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455084')
<query>INSERT INTO user_apps VALUES ('fd3f04dfe11477987da66bc9aebd7be9df143608', 'sri.leni', '8668aca285a747831e0a2c1756b624e4', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9c5dbc65953d3908b3d06698da1c843190e1b3a3', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455116')
<query>INSERT INTO user_apps VALUES ('7db12cb76e22f56cb93958006fee58b37ce41af1', 'sri.muryanto', '02bbf30e2ee666e513aacf56a6b91d7f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '49c1c3d9e697a6a46a8497b092e9d01a272b48fe', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455161')
<query>INSERT INTO user_apps VALUES ('bcae250088d7a11a0ae146791a975a212b0ebe75', 'sri.yuliawati', '662aeb26d4dd1a32431dddadbbff22d5', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '0c05407ee1e99c64b94e85196093e0418866ca38', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455188')
<query>INSERT INTO user_apps VALUES ('e15682d5d6305340699c0b13e3c2efd4af383738', 'srianto', '29f2dbc4180e827a911568800e1a6c74', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5fcd3ccccd512e05b8f7506d029513bac24a4b92', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455212')
<query>INSERT INTO user_apps VALUES ('26b6c86b57ea428d734028c371a3ae44618ada46', 'sulastri', '90cf975f4576482057e8ff66c07fcd31', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', '781552e6dff02c6dcb876b1cdcf075e973450ff0', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455258')
<query>INSERT INTO user_apps VALUES ('fa3d2ccb2447bd91e92d5b33ecb006ecff8e86e1', 'sumini', '36e821be6fc985c3829e81a15b985ad3', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '99896c7ec5140e8436c1065b9ddce5beac8a5089', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455290')
<query>INSERT INTO user_apps VALUES ('bb67248fe5b20567b0c55eb7e1a813ee9cc7bdf4', 'sunu', '9f7ef1c6f80fa911134bd0b377a63025', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '8d6d57e51e626c837ec7e22c59d1fd0798bea76f', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455315')
<query>INSERT INTO user_apps VALUES ('d468e5da7f81d0df3110234bc3babb01c4b485cc', 'susiyanti', 'e7e8fe4f44451cb2a5ffa524f5b66f6f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '3af618afbff484191c1977a29d2b1eb8e98e9847', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455347')
<query>INSERT INTO user_apps VALUES ('d4526969fac5de4b63fd0dbbd60487dbc5d17039', 'suyatno', '298e66e7e5045a6fb10209029d76d6b1', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '9fe7b21495df39576b14098224484da85753046f', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455376')
<query>INSERT INTO user_apps VALUES ('34d993f4eaa0f03b4aa2107e620f28ba274173d7', 'syarif', '04f8b055901f242a0603275c57f56116', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5e7c1c66ded949e0750cc0e0b6960f4c0e807fe1', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455403')
<query>INSERT INTO user_apps VALUES ('c2326ab188c111b3d008ab63ecbb1aaff3a63385', 'taufik.hidayatullah', '485ff02fdeb7c7afa555dd68b0dc4b44', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '040231d84d7f51354c27d1ea67e1fae3df508042', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455705')
<query>INSERT INTO user_apps VALUES ('8c45f6e4b25a8cfe98fb55da20bfba2f366c80f7', 'taufiqur.rahman', 'b44e677e556b240d720ea74e80102864', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'a498229a7e894f0d827b090f81fa280055ad13db', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455750')
<query>INSERT INTO user_apps VALUES ('117801742e07875f4cde95b3ae7fd1c8501fd291', 'sonya', 'f64de30cc687b0c8da270fdfc3e08310', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '67e93fede132408c76a025f15810c7dbc278faa5', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455806')
<query>INSERT INTO user_apps VALUES ('baf2b45c7fa11b493c6ac9daa193bfeb489d83d6', 'nano', 'bcf573ff51225c1e9f1651f7ee685d2f', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '118a57c3e501168475189337173359309567b03c', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455903')
<query>INSERT INTO user_apps VALUES ('fc215c0dc9d8143bfc6470fa8c25d96d8213e30c', 'tri.aswanto', 'c0ecb6142ec2575cdbf7bc42bf2ebd12', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '5f93f6212f54526b3ec0adeb3eb82d1477365b61', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453455969')
<query>INSERT INTO user_apps VALUES ('4295abf212376088b866777aa4f5718c3f63dc6a', 'tripati', '6ca72657a6ed5df5156e3ed22245dbfe', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4c46bf5973899e8b1c20ce4a8e9753f9a89e4b43', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456007')
<query>INSERT INTO user_apps VALUES ('c7cc55a5df8e02663341e524b4b9bf70ec9e7abb', 'tri.rahayu', 'f90bdf7a6f4cd91d59fd67c0d82a1811', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'd9e22feb1f9e986639c17c9f99b2ec9d1363bfdb', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456056')
<query>INSERT INTO user_apps VALUES ('1779b4ea913b04995fcd4e68b70f9b5d78c00380', 'tribo', 'efe194901a5aa6aec7fa04162dd3e6d2', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4e963d344c7a25dcebc5977e1cf3f7e193c1a9bd', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456167')
<query>INSERT INTO user_apps VALUES ('79186a01a23de8cddb74794715472684a4fabbd9', 'tutut', 'fb185debd21f00d1b5414ecd6b51306c', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '8e1d2459253460b964ab76304f20ff770770402b', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456191')
<query>INSERT INTO user_apps VALUES ('fbf2fc416d141eee55069d6ec63c6ccdafff43cc', 'marso', '2131095ba4e6d79d29188c74759b2cbb', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'ac9c5e66caf65e450148b1a559e02b1a0edc605b', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456222')
<query>INSERT INTO user_apps VALUES ('6230c192d65d3c742f4cd8f2b668511bcf93ffc6', 'mawan', 'fb185debd21f00d1b5414ecd6b51306c', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '2b2fcb9da080eabc3cf8f8843c81a60e1621a7b5', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456272')
<query>INSERT INTO user_apps VALUES ('4ad21901b9061a9373d5b6fec99baaf6198cf010', 'windy', '818f303bb6edee4309be2790628755a7', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '2c82e6da6c364dc682325ccccd36d95b9320016a', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456339')
<query>INSERT INTO user_apps VALUES ('d0b26614183a0a162b321481abd83dbf28f99295', 'wirata', '870f3ce243614850f47499567fcdd404', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '980910c7410ad7abb3c5f857b5b68b4e904e786d', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456367')
<query>INSERT INTO user_apps VALUES ('10b226c84828d5564d2e6c95c900ece92efb7765', 'wiwit', '36e821be6fc985c3829e81a15b985ad3', '4e7cdfdb2fb5f5b7f731eec2b2729dd912a6dbfa', '1', 'a6885f4e3652e8f137755cf41c63d23ed33e783d', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456395')
<query>INSERT INTO user_apps VALUES ('1d4edfcb410de2fb44efb3e346f20488cff51b64', 'yan', 'e948501d8a184273d64e6ec439087506', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '4195ab9cf7e72a19f0ec9ec8de4255d55119c642', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456426')
<query>INSERT INTO user_apps VALUES ('e3932fa5232cd6159eaf74db35cb3b833787d8d1', 'yayan', '99567ebd741c94b9292de4dd2330fc4d', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', '576e29c0315944c79914ca2e043fb7cd5391fc9f', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456472')
<query>INSERT INTO user_apps VALUES ('0ca60443bc41e554222280b405066741dba1c0f8', 'yenny', '65e81f22abfe30fe9deed055f07e2131', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', 'cb82e04cd25cb63230e1c204409be8c80dcb8cc6', '0', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456501')
<query>INSERT INTO user_apps VALUES ('abaf50cccc50eb4ffad27a6a02c6e331465df30a', 'yudha.setyawan', 'cd059d397e516c1bb55d5e74fa4487ca', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '68a35d2968c337b8588f131a7f4974a716302f86', '0', '0', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456540')
<query>INSERT INTO user_apps VALUES ('2fc79fcbb2b0eed6a66abc2b0822744d592bdc41', 'yudha.wijaya', '5b796e32703d17076f8cd790b1cd5fdf', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'f39daa030c81935ebc8c875c857eab1d3bd259e0', '0', '0', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453456594')
<query>INSERT INTO user_apps VALUES ('94feaa0991c13582d70c6f60cb09c7d3ac0ca2c0', 'supranawa.yusuf', '68356ac7a4c16ba9bb4e8e407289f967', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '56e321a00bdf88b638589c73bd3c4f4cda9b35a4', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453697555')
<query>INSERT INTO user_apps VALUES ('6776509a5e4963ce581f85d304339b57422bdd6a', 'ishartini', 'a48186c68b6411df1aaa463de4aaccc8', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '150726073dae2e508f94f12c5604fb00d7dd554c', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453697948')
<query>INSERT INTO user_apps VALUES ('290a2bf88095617895ac2671002b7caa5c84914d', 'riyani', '40d93ab6306c1f28d4908637fbb92eb7', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '476a96c15aa9191fb2236ffabd08d24424dc1e6a', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453697985')
<query>INSERT INTO user_apps VALUES ('5522ef1a4bb2b7a97760e47b3d2fde534535538d', 'darmadi', 'b4b2859346269071a613ea0d17fe6787', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '552e62313bac815ff3df4674dca677843629af9f', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453698085')
<query>INSERT INTO user_apps VALUES ('5da792dd847d605ae2d4a0560fba775cf482039c', 'tri.hariyanto', '3d8f7817e9a19f0f59883ea12a25d75e', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '49b9b06071b348923551643a87ff58d74545a2a4', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453703409')
<query>INSERT INTO user_apps VALUES ('63f948ad31d94811cbf36212408100b8e4243683', 'hari.wibowo', '11c3c61084e706432012cc15c7525285', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', 'c1f87bd12124aee94355e6c4934c1584d56b310a', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453703669')
<query>INSERT INTO user_apps VALUES ('31989954e892119fa4c64d3a1f9ba954f98eff36', 'agus.suherman', '8d125061a07223307405c9d046fc90c2', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '674cb9860117996244633d2bf2d459ae6594c675', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453766986')
<query>INSERT INTO user_apps VALUES ('2a4c87c5e064694a76424628ae871b5ce2ef3353', 'minhadi', '470c75229c350751572f8c4bf33eeb14', '0d1db90fabf192910f354e351dc27fd7f37fb041', '2', '0', '724455e2b77c49bb9730a8d32d5f08bc01a7315c', '1', '35ac15035d7d084fd184adfd0a83d3193bffcb3f', '1453767019')
<query>INSERT INTO user_apps VALUES ('4c8aea81229a3a3060ac36472202b94808418592', 'suparyanto', '7a3fffec6e74de5c15e0329cf48a27e0', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '1b4f109815202ef497950b7b8bee95f9baa600d4', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1454028261')
<query>INSERT INTO user_apps VALUES ('ab2961e6361348a6b20b2764f62dc94326b5d673', 'adi.hersetio', 'd43eedf5bdbb2135c984b776ac98dcea', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '05d92e1ce1c12e2e129a2d51b1f57c8dc062379b', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1454029128')
<query>INSERT INTO user_apps VALUES ('b7c0182dec232443a333cbd86b9d79b17add8806', 'abdillah', '596262a06427fe0f288f59ecb8ec5bc1', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '980af1a9245cacdfd42c2d01c0f6f3593ac58b20', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1454037066')
<query>INSERT INTO user_apps VALUES ('172d8b264f76e3f58ae5f2bba21fc46d83f3c8eb', 'puji.astutik', 'ca5073f81ec12c46dde0ca98d7e85ffe', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '403f942b2939e06a34caf16808bffa749c304622', '0', '1', 'e5ff8db6c05a9623fc08e4efcad2ad91205fe85d', '1455237206')
<query>INSERT INTO user_apps VALUES ('3c06c1b2e971a7bdd9dc389596b9615dad7c62e1', 'hamdan', '19c1ee09e5556fd2516945c8cfabb065', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', 'b53a680bb6bff1ed22dd6401b462689c929a8ff7', '0', '1', '9643952cd0956e5747f8df6f8158267efdc18214', '1461546021')
<query>INSERT INTO user_apps VALUES ('06f932612b2820649decb204550abd6da16486a9', 'dian.d', 'ae6e4de606d8db4b63a7bdb68183ec2c', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', '3a69f9370e7b777845b003ce613ff739171b5022', '0', '1', 'e5ff8db6c05a9623fc08e4efcad2ad91205fe85d', '1461814671')
<query>INSERT INTO user_apps VALUES ('a8dcb2efbc925745b82f1fe96d594d86b80eb921', 'aris', '5fde41d5aaa585381cc3575c216ba5b6', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '94a2dd1d80af60a385e277d8999134838487a98c', '0', '1', 'e6a7389cb1cbc3ce90f5273523574ce889bdfe3d', '1462278760')
<query>INSERT INTO user_apps VALUES ('fa7f4dc732c16753e3b7034e4e34e54d367c0ea7', 'yudha', '27b4cee8129af8773c9d5f32ef487a51', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'f39daa030c81935ebc8c875c857eab1d3bd259e0', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1462760044')
<query>INSERT INTO user_apps VALUES ('43a6fcbc653ecf25df51a83993319ee99af19332', 'lina.herlina', 'd4e02e105c02fe31f9b0732aaf386a5d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', 'b6252c236614f1a76110ba9af64fb972f8352edc', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1462760440')
<query>INSERT INTO user_apps VALUES ('59a9d12c325f5a9338648c48aeafce88d14664d6', 'ismail', 'e417ef2a755b710d8a01cc1f398cfdce', 'c70d9b7dbcf26781bab4d7a3198e5a6ab59ca3fb', '1', '989ee6538e0e8b72935a4b45e381049163652bb7', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1462760492')
<query>INSERT INTO user_apps VALUES ('73f6b107d3d5d38ff75de7fa88e7b77ea2d32a08', 'yuliadi', '72ec5703d262739ac9821f8e6507d83d', '98ab4eb2262a169acc9716882a6cac9d0a07e6f3', '1', '6245672b0c5ec08948828ffeb5b7eadc7a0ab0d7', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb8', '1462761714')
<query>INSERT INTO user_apps VALUES ('6e76a1cbb2ad89ee40a7b51e4f26a4e16b424eea', 'mustopa', '35f6e3c9d7ba47521f622e7f580dcd8f', '036b0642d7c660cd501fb07e1e33ec0bdd157534', '1', '084732a29519e3fd84180ea97109f04441edc8f5', '0', '1', '3c06c1b2e971a7bdd9dc389596b9615dad7c62e1', '1463103377')
<query>INSERT INTO user_apps VALUES ('144d20c55087d1bcab20fe8f4aa1dbe29f375c1b', 'winny', 'c09d427ed78f157046cd5f8af609eb48', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '0174f62c8089a6f2d44da7c5803e261e2152994c', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495179962')
<query>INSERT INTO user_apps VALUES ('214c16bdb75e636b8b984a43f8f60cf044fa057e', 'user1', '4e23e9d8b96844421599b7ebc2060c5e', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '4cd34284eedc890c9ee7038eca87f9cd52fe47c4', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495179989')
<query>INSERT INTO user_apps VALUES ('18e9dfacc1d05e427c5fec795685bcb0616abaeb', 'user2', '63ea934cf9e434b74972c96bb0c6388d', 'a38c99db8e8777d33b3b358d59a47ae1a0c69d66', '1', '88932f73e0423181b0f3a19fc25b9670c1df09f0', '0', '1', '23d87af87ec2fc29f5a4de3fed576e2ec9c4ffb7', '1495180013')
