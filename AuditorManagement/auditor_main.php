<?
include_once "_includes/classes/auditor_class.php";
$auditors = new auditor ( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_POST["val_search"])){
	@session_start();
	$_SESSION['key_search'] = $comfunc->replacetext($_POST["key_search"]);
	$_SESSION['val_search'] = $comfunc->replacetext($_POST["val_search"]);
	$_SESSION['val_method'] = $method;
}

$key_search = @$_SESSION['key_search'];
$val_search = @$_SESSION['val_search'];
$val_method = @$_SESSION['val_method'];

if(@$method!=@$val_method){
	$key_search = "";
	$val_search = "";
	$val_method = "";
}

$paging_request = "main_page.php?method=auditormgmt";
$acc_page_request = "auditor_acc.php";
$acc_page_request_detil = "main_page.php?method=auditor_detil";
$list_page_request = "auditor_view.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid.php";
$gridHeader = array ("NIP", "Nama", "Golongan/Pangkat", "Jabatan");
$gridDetail = array ("1", "2", "3", "4");
$gridWidth = array ("15", "30", "20", "20");

$key_by = array ("NIP", "Nama", "Golongan Biaya", "Jabatan");
$key_field = array ("auditor_nip", "auditor_name", "auditor_golongan", "jenis_jabatan");

$widthAksi = "15";
$iconDetail = "1";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Pegawai Inspektorat";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $auditors->auditor_data_viewlist ( $fdata_id );
		$page_title = "Ubah Pegawai Inspektorat";
		break;
	case "getdetail" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		echo "<script>window.open('" . $acc_page_request_detil . "&auditor=" . $fdata_id . "', '_self');</script>";
		break;
	case "postadd" :
		$fnip = $comfunc->replacetext ( $_POST ["nip"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		$ftempat_lahir = $comfunc->replacetext ( $_POST ["tempat_lahir"] );
		$ftanggal_lahir = $comfunc->replacetext ( $_POST ["tanggal_lahir"] );
		$falamat = $comfunc->replacetext ( $_POST ["alamat"] );
		$fjenis_kelamin = $comfunc->replacetext ( $_POST ["jenis_kelamin"] );
		$fagama = $comfunc->replacetext ( $_POST ["agama"] );
		$finsppektorat = $comfunc->replacetext ( $_POST ["inspektorat_name"] );
		$fpangkat_id = $comfunc->replacetext ( $_POST ["pangkat_id"] );
		$ftmt = $comfunc->replacetext ( $_POST ["tmt"] );
		$fjabatan = $comfunc->replacetext ( $_POST ["jabatan"] );
		$fgolongan = $comfunc->replacetext ( $_POST ["golongan"] );
		$fmobile = $comfunc->replacetext ( $_POST ["mobile"] );
		$ftelp = $comfunc->replacetext ( $_POST ["telp"] );
		$femail = $comfunc->replacetext ( $_POST ["email_auditor"] );
		$ffoto = $comfunc->replacetext ( $_FILES ["foto"] ["name"] );
		if ($fnip != "" && $fname != "" && $femail != "") {
			$rs_nip = $auditors->cek_nip_auditor ( $fnip );
			$arr_nip = $rs_nip->FetchRow ();
			$fauditor_id = $arr_nip ['auditor_id'];
			$del_st = $arr_nip ['auditor_del_st'];
			if ($fauditor_id == "") {
				$auditors->auditor_add ( $fnip, $fname, $ftempat_lahir, $ftanggal_lahir, $fpangkat_id, $fjabatan, $fgolongan, $fmobile, $ftelp, $femail, $falamat, $fjenis_kelamin, $fagama, $ffoto, $ftmt, $finsppektorat );
				$comfunc->UploadFile ( "Upload_Foto", "foto", "" );
				$comfunc->js_alert_act ( 3 );
			} else {
				if ($del_st == "0") {
					$auditors->update_auditor_del ( $fauditor_id, $fnip, $fname, $ftempat_lahir, $ftanggal_lahir, $fpangkat_id, $fjabatan, $fgolongan, $fmobile, $ftelp, $femail, $falamat, $fjenis_kelamin, $fagama, $ffoto, $ftmt, $finsppektorat );
					$comfunc->js_alert_act ( 3 );
				} else {
					$comfunc->js_alert_act ( 4, $fnip );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fnip = $comfunc->replacetext ( $_POST ["nip"] );
		$fname = $comfunc->replacetext ( $_POST ["name"] );
		$ftempat_lahir = $comfunc->replacetext ( $_POST ["tempat_lahir"] );
		$ftanggal_lahir = $comfunc->replacetext ( $_POST ["tanggal_lahir"] );
		$falamat = $comfunc->replacetext ( $_POST ["alamat"] );
		$fjenis_kelamin = $comfunc->replacetext ( $_POST ["jenis_kelamin"] );
		$fagama = $comfunc->replacetext ( $_POST ["agama"] );
		$fpangkat_id = $comfunc->replacetext ( $_POST ["pangkat_id"] );
		$ftmt = $comfunc->replacetext ( $_POST ["tmt"] );
		$finsppektorat = $comfunc->replacetext ( $_POST ["inspektorat_name"] );
		$fjabatan = $comfunc->replacetext ( $_POST ["jabatan"] );
		$fgolongan = $comfunc->replacetext ( $_POST ["golongan"] );
		$fmobile = $comfunc->replacetext ( $_POST ["mobile"] );
		$ftelp = $comfunc->replacetext ( $_POST ["telp"] );
		$femail = $comfunc->replacetext ( $_POST ["email_auditor"] );
		$ffoto = $comfunc->replacetext ( $_FILES ["foto"] ["name"] );
		$ffoto_old = $comfunc->replacetext ( $_POST ["foto_old"] );
		if ($fnip != "" && $fname != "" && $femail != "") {
			$rs_nip = $auditors->cek_nip_auditor ( $fnip, $fdata_id );
			$arr_nip = $rs_nip->FetchRow ();
			$fauditor_id = $arr_nip ['auditor_id'];
			$del_st = $arr_nip ['auditor_del_st'];
			if ($fauditor_id == "") {
				if (! empty ( $ffoto )) {
					$comfunc->UploadFile ( "Upload_Foto", "foto", $ffoto_old );
				} else {
					$ffoto = $ffoto_old;
				}
				$auditors->auditor_edit ( $fdata_id, $fnip, $fname, $ftempat_lahir, $ftanggal_lahir, $fpangkat_id, $fjabatan, $fgolongan, $fmobile, $ftelp, $femail, $falamat, $fjenis_kelamin, $fagama, $ffoto, $ftmt, $finsppektorat );
				$comfunc->js_alert_act ( 1 );
			} else {
				if ($del_st == "0") {
					if (! empty ( $ffoto )) {
						$comfunc->UploadFile ( "Upload_Foto", "foto", $ffoto_old );
					} else {
						$ffoto = $ffoto_old;
					}
					$auditors->update_auditor_del ( $fauditor_id, $fnip, $fname, $ftempat_lahir, $ftanggal_lahir, $fpangkat_id, $fjabatan, $fgolongan, $fmobile, $ftelp, $femail, $falamat, $fjenis_kelamin, $fagama, $ffoto, $ftmt, $finsppektorat );
					$auditors->auditor_delete ( $fdata_id );
					$comfunc->js_alert_act ( 1 );
				} else {
					$comfunc->js_alert_act ( 4, $fnip );
				}
			}
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$auditors->auditor_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $auditors->auditor_count ($base_on_id_int, $key_search, $val_search, $key_field);
		$rs = $auditors->auditor_viewlist ( $base_on_id_int, $key_search, $val_search, $key_field, $offset, $num_row );
		$page_title = "Daftar Pegawai Inspektorat";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
