<link rel="stylesheet" href="css/jquery-ui.css">
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span2">No Bon Peminjaman</label> 
				<input type="text" class="span1" name="no_bon" id="no_bon">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nama Peminjam</label> 
				<?php
				$rs_team = $reports->get_team_penugasan($assign_id);
				$arr_team = $rs_team->GetArray ();
				echo $comfunc->buildCombo ( "anggota_id", $arr_team, 0, 1, "", "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Peminjaman</label> 
				<input type="text" class="span1" name="tanggal_pinjam" id="tanggal_pinjam">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Pengembalian</label> 
				<input type="text" class="span1" name="tanggal_kembalikan" id="tanggal_kembalikan">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">File Tentang</label> 
				<input type="text" class="span7" name="file_tentang" id="file_tentang">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No Dosier</label> 
				<input type="text" class="span1" name="no_dosier">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No. Urut Ordner</label> 
				<input type="text" class="span1" name="no_urut">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Disetujui Oleh</label> 
				<span class="mandatory">*</span>
				<input type="text" class="span1" name="disetujui" id="disetujui">
				<label class="span0">&nbsp;</label> 
				<label class="span1">Tanggal</label> 
				<input type="text" class="span1" name="tgl_disetujui" id="tgl_disetujui">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Petugas Arsip</label> 
				<span class="mandatory">*</span>
				<input type="text" class="span1" name="petugas" id="petugas">
				<label class="span0">&nbsp;</label> 
				<label class="span1">Tanggal</label> 
				<input type="text" class="span1" name="tgl_petugas" id="tgl_petugas">
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span2">No Bon Peminjaman</label> 
				<input type="text" class="span1" name="no_bon" id="no_bon" value="<?=$arr['km26_no']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nama Peminjam</label> 
				<?php
				$rs_team = $reports->get_team_penugasan($assign_id);
				$arr_team = $rs_team->GetArray ();
				echo $comfunc->buildCombo ( "anggota_id", $arr_team, 0, 1, $arr['km26_id_auditor'], "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Peminjaman</label> 
				<input type="text" class="span1" name="tanggal_pinjam" id="tanggal_pinjam" value="<?=$comfunc->dateIndo($arr['km26_tgl_pinjam'])?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Pengembalian</label> 
				<input type="text" class="span1" name="tanggal_kembalikan" id="tanggal_kembalikan" value="<?=$comfunc->dateIndo($arr['km26_tgl_kembali'])?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">File Tentang</label> 
				<input type="text" class="span7" name="file_tentang" id="file_tentang" value="<?=$arr['km26_tentang_file']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No Dosier</label> 
				<input type="text" class="span1" name="no_dosier" value="<?=$arr['km26_no_dosier']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No. Urut Ordner</label> 
				<input type="text" class="span1" name="no_urut" value="<?=$arr['km26_no_urut']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Disetujui Oleh</label> 
				<span class="mandatory">*</span>
				<input type="text" class="span1" name="disetujui" id="disetujui" value="<?=$arr['km26_disetujui']?>">
				<label class="span0">&nbsp;</label> 
				<label class="span1">Tanggal</label> 
				<input type="text" class="span1" name="tgl_disetujui" id="tgl_disetujui" value="<?=$comfunc->dateIndo($arr['km26_tgl_disetujui'])?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Petugas Arsip</label> 
				<span class="mandatory">*</span>
				<input type="text" class="span1" name="petugas" id="petugas" value="<?=$arr['km26_petugas']?>">
				<label class="span0">&nbsp;</label> 
				<label class="span1">Tanggal</label> 
				<input type="text" class="span1" name="tgl_petugas" id="tgl_petugas" value="<?=$comfunc->dateIndo($arr['km26_tgl_petugas'])?>">
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['km26_id']?>">	
		<?
				break;
			case "getdetail" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span2">No Bon Peminjaman</label> 
				<?=$arr['km26_no']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nama Peminjam</label> 
				<?=$arr['auditor_name']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Peminjaman</label> 
				<?=$comfunc->dateIndo($arr['km26_tgl_pinjam'])?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tanggal Pengembalian</label> 
				<?=$comfunc->dateIndo($arr['km26_tgl_kembali'])?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">File Tentang</label>
				<?=$arr['km26_tentang_file']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No Dosier</label>
				<?=$arr['km26_no_dosier']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No. Urut Ordner</label>
				<?=$arr['km26_no_urut']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Disetujui Oleh</label> 
				<label class="span2"><?=$arr['km26_disetujui']?></label>
				<label class="span0">&nbsp;</label> 
				<label class="span1">Tanggal</label> 
				<?=$comfunc->dateIndo($arr['km26_tgl_disetujui'])?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Petugas Arsip</label> 
				<label class="span2"><?=$arr['km26_petugas']?></label>
				<label class="span0">&nbsp;</label> 
				<label class="span1">Tanggal</label> 
				<?=$comfunc->dateIndo($arr['km26_tgl_petugas'])?>
			</fieldset>	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> 
					<?
					if($_action!='getdetail'){
					?>
					&nbsp;&nbsp;&nbsp; 
					<input type="submit" class="blue_btn" value="Simpan">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<?
					}
					?>
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script> 
$("#tanggal_pinjam, #tanggal_kembalikan, #tgl_disetujui, #tgl_petugas").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true
}); 

$(function() {
	$("#validation-form").validate({
		rules: {
			no_bon: "required",
			anggota_id: "required",
			tanggal_pinjam: "required",
			tanggal_kembalikan: "required",
			file_tentang: "required",
			disetujui: "required",
			tgl_disetujui: "required",
			petugas: "required",
			tgl_petugas: "required"
		},
		messages: {
			no_bon: "Silahkan Masukan No Bon Peminjaman",
			anggota_id: "Silahkan Pilih Peminjam",
			tanggal_pinjam: "Masukan Tanggal Peminjaman",
			tanggal_kembalikan: "Masukan Tanggal Pengembalian",
			file_tentang: "Masukan penjelasan tentang berkas yang dipinjam",
			disetujui: "Masukan Nama Persetujuan",
			tgl_disetujui: "Masukan Tanggal Persetujuan",
			petugas: "Masukan Nama Petugas Arsip",
			tgl_petugas: "Masukan Tanggal Petugas Arsip"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>