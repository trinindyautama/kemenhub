<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	Formulir KMA 14
	<br>
	<table border='0' class="table_reni" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="6" align="center">CHECKLIST<br>PENYELESAIAN PENGUJIAN DAN AUDIT</td>
		</tr>
		<tr>
			<th width="4%" align="center">No</th>
			<th width="60%" align="center" colspan="2">Keterangan</th>
			<th width="10%" align="center">Sudah/belum</th>
			<th width="10%" align="center">Persentase penyelesaian</th>
			<th width="10%" align="center">Ket</th>
		</tr>
		<tr>
			<td align="center">1</td>
			<td align="center" colspan="2">2</td>
			<td align="center">3</td>
			<td align="center">4</td>
			<td align="center">5</td>
		</tr>
		<tr>
			<td align="center">1</td>
			<td colspan="2">Sudahkah dilakukan penjelasan penugasan kepada anggota tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '1') == 'Sudah') ? "v" : "";?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '1')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '1')?></td>
		</tr>
		<tr>
			<td align="center">2</td>
			<td colspan="2">Sudahkah dibuat perencanaan audit</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '2') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '2')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '2')?></td>
		</tr>
		<tr>
			<td align="center">3</td>
			<td colspan="2">Sudahkah dilakukan audit sesuai program audit</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '3') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '3')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '3')?></td>
		</tr>
		<tr>
			<td align="center">4</td>
			<td colspan="2">Sudahkah dilakukan review terhadap hasil kerja anggota tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '4') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '4')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '4')?></td>
		</tr>
		<tr>
			<td align="center">5</td>
			<td colspan="2">Sudahkah hasil review ditindaklanjuti oleh anggota tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '5') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '5')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '5')?></td>
		</tr>
		<tr>
			<td align="center">6</td>
			<td colspan="2">Sudahkah anggota tim membuat KKA dan disimpan pada tempat yang telah disiapkan untuknya</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '6') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '6')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '6')?></td>
		</tr>
		<tr>
			<td align="center">7</td>
			<td colspan="2">Sudahkah PKA dikerjakan oleh Ketua Tim dan disimpan pada tempat yang telah disiapkan sebelumnya</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '7') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '7')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '7')?></td>
		</tr>
		<tr>
			<td align="center">8</td>
			<td colspan="2">Sudahkan direview oleh Pengendali teknis:</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '8') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '8')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '8')?></td>
		</tr>
		<?
		$data_8_1 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '8_1'));
		if($data_8_1!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review I tanggal <?=$data_8_1?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		$data_8_2 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '8_2'));
		if($data_8_2!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review II tanggal  <?=$data_8_2?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		$data_8_3 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '8_3'));
		if($data_8_3!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review III tanggal <?=$data_8_3?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		$data_8_4 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '8_4'));
		if($data_8_4!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review IV tanggal <?=$data_8_4?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		?>
		<tr>
			<td align="center">9</td>
			<td colspan="2">Sudahkah dibuat ringkasan arahan review dari Pengendali Teknis</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '9') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '9')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '9')?></td>
		</tr>
		<tr>
			<td align="center">10</td>
			<td colspan="2">Sudahkah hasil review Pengendali Teknis ditindaklanjuti oleh Tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '10') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '10')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '10')?></td>
		</tr>
		<tr>
			<td align="center">11</td>
			<td colspan="2">Sudahkah dikembangkan temuan hasil audit dan rekomendasi perbaikan</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '11') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '11')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '11')?></td>
		</tr>
		<tr>
			<td align="center">12</td>
			<td colspan="2">Sudahkah dilakukan komunikasi temuan dan rekomendasi perbaikan dengan manajemen auditi</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '12') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '12')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '12')?></td>
		</tr>
		<tr>
			<td align="center">13</td>
			<td colspan="2">Sudahkah diperoleh kata sepakat atas rekomendasi yang diberikan</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '13') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '13')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '13')?></td>
		</tr>
		<tr>
			<td align="center">14</td>
			<td colspan="2">Adakah Pengendali Mutu melakukan review:</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '14') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '14')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '14')?></td>
		</tr>
		<?
		$data_14_1 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '14_1'));
		if($data_14_1!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review I tanggal <?=$data_14_1?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		$data_14_2 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '14_2'));
		if($data_14_2!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review II tanggal <?=$data_14_2?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		$data_14_3 = $comfunc->dateIndo($reports->get_data_st_km14 ( $assign_id, '14_3'));
		if($data_14_3!=""){
		?>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Review III tanggal <?=$data_14_3?></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?
		}
		?>
		<tr>
			<td align="center">15</td>
			<td colspan="2">Sudahkah dibuat ringkasan hasil review Pengendali Mutu</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '15') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '15')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '15')?></td>
		</tr>
		<tr>
			<td align="center">16</td>
			<td colspan="2">Sudahkah hasil review Penanggungjawab ditindaklanjuti oleh Tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '16') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '16')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '16')?></td>
		</tr>
		<tr>
			<td align="center">17</td>
			<td colspan="2">Sudahkah dilakukan penyusunan dokumentasi hasil audit</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '17') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '17')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '17')?></td>
		</tr>
		<tr>
			<td align="center">18</td>
			<td colspan="2">Sudahkah dokumentasi hasil audit dibahas</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center" width="5px">o</td>
			<td>di Tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '18_1') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '18_1')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '18_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Teknis</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '18_2') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '18_2')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '18_2')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Mutu</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '18_3') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '18_3')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '18_3')?></td>
		</tr>
		<tr>
			<td align="center">19</td>
			<td colspan="2">Sudahkah dilakukan penelaahan kesesuaian KKA dan isinya dengan standar audit APIP,</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>oleh Tim</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '19_1') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '19_1')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '19_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Teknis</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '19_2') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '19_2')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '19_2')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Mutu</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '19_3') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '19_3')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '19_3')?></td>
		</tr>
		<tr>
			<td align="center">20</td>
			<td colspan="2">Sudahkah dilakukan penelaahan kesesuaian KKA dengan tujuan audit,</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Teknis</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '20_1') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '20_1')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '20_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Mutu</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '20_2') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '20_2')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '20_2')?></td>
		</tr>
		<tr>
			<td align="center">21</td>
			<td colspan="2">Sudahkah dilakukan penambahan simpulan hasil audit,	</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>di Tim Pemeriksa</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '21_1') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '21_1')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '21_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Teknis</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '21_2') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '21_2')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '21_2')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>dengan Pengendali Mutu</td>
			<td align="center"><?=($reports->get_data_st_km14 ( $assign_id, '21_3') == 'Sudah') ? 'v' : ''?></td>
			<td align="center"><?=$reports->get_data_persen_km14 ( $assign_id, '21_3')?></td>
			<td align="center"><?=$reports->get_data_ket_km14 ( $assign_id, '21_3')?></td>
		</tr>
	</table>
	<table border='0' class="table_reni" cellspacing='0' cellpadding="0">
		<tr>
			<td style="border:0px;" colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border:0px">Direview oleh,<br>Pengandali Teknis<br><br><br><br><?=$get_dalnis?></td>
			<td style="border:0px">Tanggal <?= $comfunc->dateIndo($reports->get_data_km14('km14_dalnis', $assign_id, '')) ?></td>
			<td colspan="2" align="center" style="border:0px">Diisi oleh,<br>Ketua Tim<br><br><br><br><?=$get_katim?></td>
			<td style="border:0px">Tanggal <?= $comfunc->dateIndo($reports->get_data_km14('km14_katim', $assign_id, '')) ?></td>
		</tr>
	</table>
</body>
</html>