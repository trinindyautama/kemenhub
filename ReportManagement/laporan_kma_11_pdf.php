<?php
set_time_limit(0);
ini_set('memory_limit', '1024M');

require '../_includes/html2pdf/vendor/autoload.php';
include_once "../_includes/common.php";

$comfunc = new comfunction();
define('DOMPDF_ENABLE_AUTOLOAD', false);
define("DOMPDF_ENABLE_FONTSUBSETTING", true);
define("DOMPDF_UNICODE_ENABLED", true);
 
require_once '../_includes/html2pdf/vendor/dompdf/dompdf/dompdf_config.inc.php';
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/classes/auditor_class.php";

$reports = new report ();
$assigns = new assign();
$auditors = new auditor();

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr_assign = $rs->FetchRow();

$rs_km_6 = $reports->km06_list($assign_id);
$arr_km_6 = $rs_km_6->FetchRow();

$rs_km_11 = $reports->get_km_11($assign_id);
$arr_km_11 = $rs_km_11->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$get_daltu = $arr_daltu['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$htmlString = '';
ob_start();
include('laporan_kma_11_template.php');
$htmlString .= ob_get_clean();

$dompdf = new DOMPDF();
$dompdf->load_html($htmlString);
$dompdf->set_paper('a4','potrait');
$dompdf->render();
$dompdf->stream("Formulir KMA 11.pdf");
?>