<?php 	
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/report_class.php";

$assigns = new assign( $ses_userId );
$reports = new report( $ses_userId );

$fil_surat_tugas = "";

$fil_assign_id = $comfunc->replacetext ( $_REQUEST ["fil_surat_tugas"] );

$rs = $assigns->assign_viewlist($fil_assign_id);
$arr = $rs->FetchRow();

if($fil_assign_id!=""){
?>
<section id="main" class="column">	
	<article class="module width_3_quarter">
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<td width="10%">Auditee</td>
				<td width="2%">:</td>
				<td>
					<?
					$rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
					while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
						echo $arr_id_auditee ['auditee_name'] . "<br>";
					}
					?>
				</td>
			</tr>
			<tr>
				<td width="10%">No Surat Tugas</td>
				<td width="2%">:</td>
				<td><?=$reports->get_surat_tugas_no($arr ['assign_id']);?></td>
			</tr>
			<tr>
				<td width="10%">Tim Audit</td>
				<td width="2%">:</td>
				<td>
					<table cellspacing='0' cellpadding="0">
						<?
						$rs_id_auditor = $assigns->anggota_list ( $arr ['assign_id'], "" );
						foreach ($rs_id_auditor->GetArray() as $key => $arr_id_auditor ) {
							?>
							<tr>
								<td style="border-style: none;" align="center"><?= $key + 1 ?>. </td><td style="border-style: none;"><?php echo $arr_id_auditor ['auditor_name'] ?></td><td style="border-style: none;"><?= $arr_id_auditor['posisi_name'] ?></td>
							</tr>
							<?
						}
						?>
					</table>
				</td>
			</tr>
		</table>
		<?php 
			// $km6_17 = $comfunc->cek_akses ( $ses_group_id, $method, 'km6_17' );
		?>

		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<!-- <th width="30%">Tahapan Audit</th> -->
				<th width="60%" colspan="2">KMA PERMENPAN</th>
				<!-- <th width="20%" colspan="">Lampiran</th> -->
				<th width="10%">Aksi</th>
				<!-- <th width="20%" colspan="">Lampiran</th> -->
			</tr>
			<tr>
				<!-- <td rowspan="3">Penyusunan Rencana dan Program Kerja Audit pada Tingkat Tim Audit</td> -->
				<td width="2%" style="" class="td_ganjil">6.</td>
				<td style="" class="td_ganjil">Formulir Kartu Penugasan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_6&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<!-- <td rowspan="3">Penyusunan Rencana dan Program Kerja Audit pada Tingkat Tim Audit</td> -->
				<td width="2%" style="" class="td_ganjil">7.</td>
				<td style="" class="td_ganjil">Alokasi Anggaran Waktu Audit</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_7&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
<!-- 			<tr>
				<td width="2%" style="">8.</td>
				<td style="">Formulir Laporan Mingguan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_8&fil_id=<?=$fil_assign_id?>">View</a></td>
			</tr> -->
			<tr>
				<td style=""class="td_ganjil">10.</td>
				<td style=""class="td_ganjil">Formulir Check List Penyelesaian Penugasan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_10&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<td style="">11.</td>
				<td style="">Notulensi Kesepakatan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_11&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<!-- <td>Supervisi Audit</td> -->
				<td style="" class="td_ganjil">12.</td>
				<td style="" class="td_ganjil">Formulir Lembar Reviu Supervisi</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_12&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<!-- <td>Pelaksanaan Audit</td> -->
				<td style="">14.</td>
				<td style="">Check List Penyelesaian Pengujian dan Evaluasi</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_14&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<!-- <td rowspan="3">Pelaporan Audit</td> -->
				<td style="" class="td_ganjil">15.</td>
				<td style="" class="td_ganjil">Formulir Pengendalian Penyusunan Laporan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_15&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<td style="">16.</td>
				<td style="">Formulir Reviu Konsep Laporan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_16&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr class="td_ganjil">
				<td style="">17.</td>
				<td style="">Formulir Checklist Penyelesaian Laporan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_17&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<!-- <td>Pemantauan Tindak Lanjut Hasil Audit</td> -->
				<td style="">21.</td>
				<td style="">Berita Acara Pemutakhiran Data</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_21&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<tr>
				<!-- <td rowspan="3">Tata Usaha</td> -->
				<td style="" class="td_ganjil">26.</td>
				<td style="" class="td_ganjil">Bon Peminjaman Berkas</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_26&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
<!-- 			<tr>
				<td style="">27.</td>
				<td style="">Surat Tugas</td>
				<td align="center"><a target="_self" href="main_page.php?method=validate_kma_27&fil_id=<?=$fil_assign_id?>">View</a></td>
			</tr> -->
			<tr>
				<td style="" class="td_ganjil">28.</td>
				<td style="" class="td_ganjil">Surat Penyampaian Temuan</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_28&fil_id=<?=$fil_assign_id?>">View</a></td>
				<!-- <td width="20%" colspan=""></td> -->
			</tr>
			<!-- <tr> -->
				<!-- <td>SDM</td> -->
				<!-- <td style="">29.</td>
				<td style="">Penilaian Kinerja Auditor Atas Penugasan Audit</td>
				<td class="td_ganjil" align="center"><a target="_self" href="main_page.php?method=validate_kma_29&fil_id=<?=$fil_assign_id?>">View</a></td>
				<td width="20%" colspan=""></td>
			</tr> -->
		</table>
	</article>
</section>
<?
}else{
	$comfunc->js_alert_act ( 5 );
?>
	<script>window.open('main_page.php?method=validasi_kma_audit', '_self');</script>
<?
}
?>