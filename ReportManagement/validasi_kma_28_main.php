<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = "main_page.php?method=validate_kma_28&fil_id=".$assign_id;

switch ($_action) {
	case "postadd" :
		$km_id = $comfunc->replacetext ( $_POST ["km_id"] );
		$fkepada = $comfunc->replacetext ( $_POST ["kepada"] );
		$fdari = $comfunc->replacetext ( $_POST ["dari"] );
		$fperihal = $comfunc->replacetext ( $_POST ["perihal"] );
		$ftanggal = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal"] ));
		if($fkepada!="" && $fdari!="" && $fperihal!="" && $ftanggal!="") {
			if($km_id=="") 
				$reports->insert_km_28($assign_id, $fkepada, $fdari, $fperihal, $ftanggal);
			else 
				$reports->update_km_28($km_id, $fkepada, $fdari, $fperihal, $ftanggal);
			
			$comfunc->js_alert_act ( 3 );
		}else{
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	default :
		$rs_data = $reports->get_data_km28($assign_id);
		$arr_data = $rs_data->FetchRow();
		$_nextaction = "postadd";
		$page_title = 'Formulir KMA 28';
		$page_request = "validasi_kma_28_acc.php";
		break;
}
include_once $page_request;
?>