<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<?
include_once "_includes/classes/risk_class.php";

$risks = new risk ( $ses_userId );
?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Filter Pelaporan Risiko</h3>
		</header>
		<form method="post" name="f" action="main_page.php?method=risk_report" class="form-horizontal" id="validation-form">
			<fieldset class="hr">
				<label class="span2">Tahun</label>
				<?php
				$rs_tahun = $risks->risk_tahun_viewlist();
				$arr_tahun = $rs_tahun->GetArray ();
				echo $comfunc->buildCombo ( "fil_tahun_id", $arr_tahun, 0, 0, "", "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Satuan Kerja</label>
				<?php
				$rs_auditee = $risks->risk_auditee_viewlist($base_on_id_eks);
				$arr_auditee = $rs_auditee->GetArray ();
				echo $comfunc->buildCombo ( "fil_satker_id", $arr_auditee, 0, 1, $base_on_id_eks, "", "", false, false, true );
				?>
			</fieldset><fieldset class="hr">
				<label class="span2">Siklus Manajemen Risiko</label>
				<select name="fil_siklus" id="fil_siklus">
					<option value="">==== Pilih Satu</option>
					<option value="identifikasi">Identifikasi Risiko</option>
					<option value="analisa">Analisis Risiko</option>
					<option value="evaluasi">Evaluasi Risiko</option>
					<option value="penanganan">Penanganan Risiko</option>
					<option value="monitoring">Monitoring Risiko</option>
				</select>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset>
				<center>
					<input type="submit" class="blue_btn" value="Lihat">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>
$(function() {
	$("#validation-form").validate({
		rules: {
			fil_tahun_id: "required",
			fil_siklus: "required"
		},
		messages: {
			fil_tahun_id: "Silahkan Pilih Tahun",
			fil_siklus: "Silahkan Pilih Siklus Manajemen Risiko"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>