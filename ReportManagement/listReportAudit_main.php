<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">List Laporan Pengawasan</h3>
		</header>
		<center>
		<fieldset>
			<input type="button" class="span3" value="Rekap PKPT" Onclick="window.open('main_page.php?method=laporan_rekap_perencanaan_filter', '_self');">
		</fieldset>
		<fieldset>
			<input type="button" class="span3" value="Rekap Surat Tugas" Onclick="window.open('main_page.php?method=rekap_surat_tugas_filter', '_self');">
		</fieldset>
		<fieldset>
			<input type="button" class="span3" value="Program Kerja Audit" Onclick="window.open('main_page.php?method=laporan_program_audit_filter', '_self');">
		</fieldset>
		<fieldset>
			<input type="button" class="span3" value="Kertas Kerja Audit" Onclick="window.open('main_page.php?method=laporan_kka_filter', '_self');">
		</fieldset>
		<fieldset>
			<input type="button" class="span3" value="Matriks Temuan" Onclick="window.open('main_page.php?method=laporan_temuan_filter', '_self');">
		</fieldset>
		<fieldset>
			<input type="button" class="span3" value="Laporan Hasil Audit" Onclick="window.open('main_page.php?method=laporan_lha_filter', '_self');">
		</fieldset>
		<fieldset>
			<input type="button" class="span3" value="Rekapitulasi Temuan Hasil Audit" Onclick="window.open('main_page.php?method=laporan_rekap_temuan_hasil_audit_filter', '_self');">
		</fieldset>
		</center>
	</article>
</section>