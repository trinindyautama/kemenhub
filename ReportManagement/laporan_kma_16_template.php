<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	<header>Formulir KMA 16</header>
	<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="3" align="center" style="border:0px">REVIU KONSEP LAPORAN<br>Pengendali Teknis/Pengendali Mutu</td>
		</tr>
		<tr>
			<td width="15%" style="border:0">Nama Auditi</td>
			<td width="2%" style="border:0">:</td>
			<td style="border:0">
			<?
			$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
			$assign_id_auditee = "";
			while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
				echo $arr_id_auditee ['auditee_name']."<br>";
			}
			?>
			</td>
		</tr>
		<tr>
			<td style="border:0">No. Kartu Penugasan</td>
			<td style="border:0">:</td>
			<td style="border:0"><?=$arr_km06['km06_no'];?></td>
		</tr>
	</table>
	<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
		<tr>
			<th width="5%">No</th>
			<th width="10%">Halaman LHA</th>
			<th width="28%">Masalah yang dijumpai</th>
			<th width="10%">No KKA</th>
			<th width="28%">Penyelesaian Masalah</th>
			<th width="16%">Dilakukan Oleh</th>
			<th width="16%">Ket</th>
		</tr>
		<tr>
			<td align="center">1</td>
			<td align="center">2</td>
			<td align="center">3</td>
			<td align="center">4</td>
			<td align="center">5</td>
			<td align="center">6</td>
			<td align="center">7</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<?	
		$no=0;
		$rs_data = $reports->km16_data_viewlist("", $assign_id);
		while($arr_data = $rs_data->FetchRow()){
			$no++;
		?>
		<tr>
			<td align="center"><?=$no?></td>
			<td align="center"><?=$arr_data['km16_lha']?></td>
			<td><?=$arr_data['km16_masalah']?></td>
			<td align="center"><?=$arr_data['kertas_kerja_no']?></td>
			<td><?=$arr_data['km16_komen']?></td>
			<td><?=$arr_data['auditor_name']?></td>
			<td><?=$arr_data['km16_ket']?></td>
		</tr>
		<?
		}
		?>
	</table>
	<table border='0' class="table_risk" cellspacing='0' cellpadding="0">				
		<tr>
			<td colspan="3">Pengendali Teknis,</td>
		</tr>
		<tr>
			<td width="10%">Tanda Tangan</td>
			<td width="2%">:</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><?=$get_dalnis?></td>
		</tr>
		<tr>
			<td>Tanggal</td>
			<td>:</td>
			<td><?=date('d-m-Y')?></td>
		</tr>
	</table>
</body>
</html>