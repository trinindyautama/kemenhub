<?
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_REQUEST ["fil_id"])){
	@session_start();
	$_SESSION['ses_assign'] = $comfunc->replacetext ( $_REQUEST ["fil_id"] );
}

$assign_id = $_SESSION['ses_assign'];

$paging_request = "main_page.php?method=validate_kma_12";
$acc_page_request = "validasi_kma_12_acc.php";
$list_page_request = "report_view.php";
$view_parrent = "validasi_kma_12_parrent.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid_km.php";
$gridHeader = array ("Permasalahan / Komentar", "Indeks KKA", "Penyelesaian", "Persetujuan");
$gridDetail = array ("4","3", "4", "0");
$gridWidth = array ("30", "20", "25", "10");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

$rs_assign = $assigns->assign_viewlist($assign_id);
$arr_assign = $rs_assign->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Pemasalahan";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $reports->km12_data_viewlist ( $fdata_id );
		$page_title = "Ubah Pemasalahan";
		break;
	case "postadd" :
		$fkka_id = $comfunc->replacetext ( $_POST ["kka_id"] );
		if ($fkka_id != "") {
			$reports->km12_add ( $assign_id, "" , $fkka_id, "" );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fkka_id = $comfunc->replacetext ( $_POST ["kka_id"] );
		if($fkka_id != "") {
			$reports->km12_edit ( $fdata_id, "" , $fkka_id, "" );
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$reports->km12_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $reports->km12_count ($assign_id);
		$rs = $reports->km12_view_grid ($assign_id, $offset, $num_row );
		$page_title = "Daftar Permasalahan KKA (KMA 12)";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
