<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr_assign = $rs->FetchRow();

$rs_km06 = $reports->km06_list($assign_id);
$arr_km06 = $rs_km06->FetchRow();


$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
$jml_auditee = $rs_id_auditee->RecordCount();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$get_daltu = $arr_daltu['auditor_name'];

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = "main_page.php?method=validate_kma_15&fil_id=".$assign_id;

switch ($_action) {
	case "postadd" :
		if ($_POST['pkat'] != '') {
			if($_POST['pkat_id']=="") $reports->insert_km_15($assign_id, 'pkat', $_POST['pkat'], '', '');
				else $reports->update_km_15($_POST['pkat_id'], $_POST['pkat'], '', '');
		}
		for($i=1;$i<=3;$i++){	
			$fid = $comfunc->replacetext ( $_POST ["id_".$i] );
			$fnama = $comfunc->replacetext ( $_POST ["nama_".$i] );
			$ftanggal_mulai = $comfunc->date_db ($comfunc->replacetext ( $_POST ["tanggal_mulai_".$i] ));
			$ftanggal_selesai = $comfunc->date_db ($comfunc->replacetext ( $_POST ["tanggal_selesai_".$i] ));
			if($fnama!="") {
				if($fid=="") $reports->insert_km_15($assign_id, $i, $fnama, $ftanggal_mulai, $ftanggal_selesai);
				else $reports->update_km_15($fid, $fnama, $ftanggal_mulai, $ftanggal_selesai);
			}
		}
		$jml = $jml_auditee+2;
		for($x=1;$x<=$jml;$x++){	
			$fid_1 = $comfunc->replacetext ( $_POST ["id_6_".$x] );
			$fnama_1 = $comfunc->replacetext ( $_POST ["nama_6_".$x] );
			$ftanggal_mulai_1 = $comfunc->date_db ($comfunc->replacetext ( $_POST ["tanggal_mulai_6_".$x] ));
			$ftanggal_selesai_1 = $comfunc->date_db ($comfunc->replacetext ( $_POST ["tanggal_selesai_6_".$x] ));
			if($fnama_1!="") {
				if($fid_1=="") $reports->insert_km_15($assign_id, "6_".$x, $fnama_1, $ftanggal_mulai_1, $ftanggal_selesai_1);
				else $reports->update_km_15($fid_1, $fnama_1, $ftanggal_mulai_1, $ftanggal_selesai_1);
			}			
		}

		foreach (range(1,4) as $value) {
			$fid_1 = $comfunc->replacetext ( $_POST ["id_1_".$value] );
			$ftanggal_1 = $comfunc->date_db ($comfunc->replacetext ( $_POST ["tanggal_1_".$value] ));
			// if($fnama!="") {
				if($fid_1=="") $reports->insert_km_15($assign_id, "tgl_1_".$value, '', $ftanggal_1, "");
				else $reports->update_km_15($fid_1, '', $ftanggal_1, "");
			// }

			$fid_2 = $comfunc->replacetext ( $_POST ["id_2_".$value] );
			$ftanggal_2 = $comfunc->date_db ($comfunc->replacetext ( $_POST ["tanggal_2_".$value] ));
			// if($fnama!="") {
				if($fid_2=="") $reports->insert_km_15($assign_id, "tgl_2_".$value, '', $ftanggal_2, "");
				else $reports->update_km_15($fid_2, '', $ftanggal_2, "");
			// }	
		}
		
		$comfunc->js_alert_act ( 3 );
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	default :
		$_nextaction = "postadd";
		$page_request = "validasi_kma_15_acc.php";
		break;
}
include_once $page_request;
?>