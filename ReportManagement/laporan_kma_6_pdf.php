<?php
set_time_limit(0);
ini_set('memory_limit', '1024M');

require '../_includes/html2pdf/vendor/autoload.php';
include_once "../_includes/common.php";

$comfunc = new comfunction();
define('DOMPDF_ENABLE_AUTOLOAD', false);
define("DOMPDF_ENABLE_FONTSUBSETTING", true);
define("DOMPDF_UNICODE_ENABLED", true);
 
require_once '../_includes/html2pdf/vendor/dompdf/dompdf/dompdf_config.inc.php';
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/assignment_class.php";

$reports = new report ();
$assigns = new assign();

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

$rs_km06 = $reports->km06_list($assign_id);
$arr_km06 = $rs_km06->FetchRow();
$status = 0;
$status = $arr_km06['km06_st'];

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$nama_katim = $arr_katim['auditor_name'];
$id_katim = $arr_katim['auditor_id'];
$user_id_katim = $arr_katim['user_id'];

// $as_katim = false;
// if($ses_id_int==$id_katim) $as_katim = true;

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$nama_dalnis = $arr_dalnis['auditor_name'];
$id_dalnis = $arr_dalnis['auditor_id'];
$user_id_dalnis = $arr_dalnis['user_id'];

// $as_dalnis = false;
// if($ses_id_int==$id_dalnis && $ses_id_int!="") $as_dalnis = true;

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$nama_daltu = $arr_daltu['auditor_name'];
$id_daltu = $arr_daltu['auditor_id'];
$user_id_daltu = $arr_daltu['user_id'];

// $as_daltu = false;
// if($ses_id_int==$id_daltu && $ses_id_int!="") $as_daltu = true;

$htmlString = '';
ob_start();
include('laporan_kma_6_template.php');
$htmlString .= ob_get_clean();

$dompdf = new DOMPDF();
$dompdf->load_html($htmlString);
$dompdf->set_paper('legal', 'potrait');
$dompdf->render();
$dompdf->stream("Formulir KMA 6.pdf");
?>