<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Formulir KMA 14</h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="6" align="center">Checklist<br>PENYELESAIAN PENGUJIAN DAN AUDIT</td>
				</tr>
				<tr>
					<th width="5%" align="center">No</th>
					<th width="55%" align="center" colspan="2">Keterangan</th>
					<th width="10%" align="center">Sudah/belum <br>
						<input type="checkbox" id="check_all">
					</th>
					<th width="10%" align="center">% penyelesaian</th>
					<th width="20%" align="center">Ket</th>
				</tr>
				<tr>
					<td align="center">1</td>
					<td align="center" colspan="2">2</td>
					<td align="center">3</td>
					<td align="center">4</td>
					<td align="center">5</td>
				</tr>
				<tr>
					<td align="center">1</td>
					<td colspan="2">Sudahkah dilakukan penjelasan penugasan kepada anggota tim</td>
					<td align="center"><input type="checkbox" name="st_1" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '1') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_1" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '1')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_1" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '1')?>"></td>
				</tr>
				<tr>
					<td align="center">2</td>
					<td colspan="2">Sudahkah dibuat perencanaan audit</td>
					<td align="center"><input type="checkbox" name="st_2" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '2') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_2" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '2')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_2" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '2')?>"></td>
				</tr>
				<tr>
					<td align="center">3</td>
					<td colspan="2">Sudahkah dilakukan audit sesuai program audit</td>
					<td align="center"><input type="checkbox" name="st_3" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '3') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '3')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_3" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '3')?>"></td>
				</tr>
				<tr>
					<td align="center">4</td>
					<td colspan="2">Sudahkah dilakukan review terhadap hasil kerja anggota tim</td>
					<td align="center"><input type="checkbox" name="st_4" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '4') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_4" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '4')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_4" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '4')?>"></td>
				</tr>
				<tr>
					<td align="center">5</td>
					<td colspan="2">Sudahkah hasil review ditindaklanjuti oleh anggota tim</td>
					<td align="center"><input type="checkbox" name="st_5" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '5') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_5" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '5')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_5" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '5')?>"></td>
				</tr>
				<tr>
					<td align="center">6</td>
					<td colspan="2">Sudahkah anggota tim membuat KKA dan disimpan pada tempat yang telah disiapkan untuknya</td>
					<td align="center"><input type="checkbox" name="st_6" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '6') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_6" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '6')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_6" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '6')?>"></td>
				</tr>
				<tr>
					<td align="center">7</td>
					<td colspan="2">Sudahkah PKA dikerjakan oleh Ketua Tim dan disimpan pada tempat yang telah disiapkan sebelumnya</td>
					<td align="center"><input type="checkbox" name="st_7" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '7') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_7" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '7')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_7" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '7')?>"></td>
				</tr>
				<tr>
					<td align="center">8</td>
					<td colspan="2">Sudahkan direview oleh Pengendali teknis:</td>
					<td align="center"><input type="checkbox" name="st_8" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '8') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_8" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '8')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_8" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '8')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review I tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review II tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review III tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review IV tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">9</td>
					<td colspan="2">Sudahkah dibuat ringkasan arahan review dari Pengendali Teknis</td>
					<td align="center"><input type="checkbox" name="st_9" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '9') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_9" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '9')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_9" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '9')?>"></td>
				</tr>
				<tr>
					<td align="center">10</td>
					<td colspan="2">Sudahkah hasil review Pengendali Teknis ditindaklanjuti oleh Tim</td>
					<td align="center"><input type="checkbox" name="st_10" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '10') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_10" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '10')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_10" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '10')?>"></td>
				</tr>
				<tr>
					<td align="center">11</td>
					<td colspan="2">Sudahkah dikembangkan temuan hasil audit dan rekomendasi perbaikan</td>
					<td align="center"><input type="checkbox" name="st_11" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '11') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_11" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '11')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_11" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '11')?>"></td>
				</tr>
				<tr>
					<td align="center">12</td>
					<td colspan="2">Sudahkah dilakukan komunikasi temuan dan rekomendasi perbaikan dengan manajemen auditi</td>
					<td align="center"><input type="checkbox" name="st_12" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '12') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_12" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '12')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_12" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '12')?>"></td>
				</tr>
				<tr>
					<td align="center">13</td>
					<td colspan="2">Sudahkah diperoleh kata sepakat atas rekomendasi yang diberikan</td>
					<td align="center"><input type="checkbox" name="st_13" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '13') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_13" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '13')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_13" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '13')?>"></td>
				</tr>
				<tr>
					<td align="center">14</td>
					<td colspan="2">Adakah Pengendali Mutu melakukan review:</td>
					<td align="center"><input type="checkbox" name="st_14" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '14') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '14')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_14" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '14')?>"></td>
				</tr>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review I tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review II tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>Review III tanggal</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">15</td>
					<td colspan="2">Sudahkah dibuat ringkasan hasil review Pengendali Mutu</td>
					<td align="center"><input type="checkbox" name="st_15" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '15') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_15" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '15')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_15" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '15')?>"></td>
				</tr>
				<tr>
					<td align="center">16</td>
					<td colspan="2">Sudahkah hasil review Penanggungjawab ditindaklanjuti oleh Tim</td>
					<td align="center"><input type="checkbox" name="st_16" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '16') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_16" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '16')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_16" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '16')?>"></td>
				</tr>
				<tr>
					<td align="center">17</td>
					<td colspan="2">Sudahkah dilakukan penyusunan dokumentasi hasil audit</td>
					<td align="center"><input type="checkbox" name="st_17" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '17') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_17" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '17')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_17" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '17')?>"></td>
				</tr>
				<tr>
					<td align="center">18</td>
					<td colspan="2">Sudahkah dokumentasi hasil audit dibahas</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>di Tim</td>
					<td align="center"><input type="checkbox" name="st_18_1" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '18_1') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_18_1" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '18_1')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_18_1" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '18_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Teknis</td>
					<td align="center"><input type="checkbox" name="st_18_2" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '18_2') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_18_2" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '18_2')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_18_2" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '18_2')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Mutu</td>
					<td align="center"><input type="checkbox" name="st_18_3" <?=  $reports->get_data_st_km14 ( $assign_id, '18_3') == "Sudah"? "checked" : "" ?> class="view"></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_18_3" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '18_3')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_18_3" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '18_3')?>"></td>
				</tr>
				<tr>
					<td align="center">19</td>
					<td colspan="2">Sudahkah dilakukan penelaahan kesesuaian KKA dan isinya dengan standar audit APIP,</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>oleh Tim</td>
					<td align="center"><input type="checkbox" name="st_19_1" <?=  $reports->get_data_st_km14 ( $assign_id, '19_1') == "Sudah"? "checked" : "" ?> class="view"></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_19_1" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '19_1')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_19_1" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '19_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Teknis</td>
					<td align="center"><input type="checkbox" name="st_19_2" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '19_2') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_19_2" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '19_2')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_19_2" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '19_2')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Mutu</td>
					<td align="center"><input type="checkbox" name="st_19_3" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '19_3') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_19_3" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '19_3')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_19_3" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '19_3')?>"></td>
				</tr>
				<tr>
					<td align="center">20</td>
					<td colspan="2">Sudahkah dilakukan penelaahan kesesuaian KKA dengan tujuan audit,</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Teknis</td>
					<td align="center"><input type="checkbox" name="st_20_1" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '20_1') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_20_1" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '20_1')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_20_1" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '20_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Mutu</td>
					<td align="center"><input type="checkbox" name="st_20_2" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '20_2') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_20_2" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '20_2')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_20_2" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '20_2')?>"></td>
				</tr>
				<tr>
					<td align="center">21</td>
					<td colspan="2">Sudahkah dilakukan penambahan simpulan hasil audit,	</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>di Tim Pemeriksa</td>
					<td align="center"><input type="checkbox" name="st_21_1" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '21_1') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_21_1" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '21_1')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_21_1" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '21_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Teknis</td>
					<td align="center"><input type="checkbox" name="st_21_2" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '21_2') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_21_2" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '21_2')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_21_2" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '21_2')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">&diams;</td>
					<td>dengan Pengendali Mutu</td>
					<td align="center"><input type="checkbox" name="st_21_3" class="view" <?=  $reports->get_data_st_km14 ( $assign_id, '21_3') == "Sudah"? "checked" : "" ?>></td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_21_3" size="4" value="<?=$reports->get_data_persen_km14 ( $assign_id, '21_3')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_21_3" size="25" value="<?=$reports->get_data_ket_km14 ( $assign_id, '21_3')?>"></td>
				</tr>
				<script type="text/javascript">
				$("#check_all").on("change", function() {
					$(".view").prop('checked', this.checked);
				});
				</script>
				<tr>
					<td colspan="2" style="border:0px">Direview oleh,<br>Pengandali Teknis<br><br><br><br><?=$get_dalnis?></td>
					<td style="border:0px">Tanggal <input type="text" name="tanggal_dalnis" id="tanggal_dalnis" value="<?= $comfunc->dateIndo($reports->get_data_km14('km14_dalnis', $assign_id, '')) ?>"></td>
					<td colspan="2" align="center" style="border:0px">Diisi oleh,<br>Ketua Tim<br><br><br><br><?=$get_katim?></td>
					<td style="border:0px">Tanggal <input type="text" name="tanggal_katim" id="tanggal_katim" value="<?= $comfunc->dateIndo($reports->get_data_km14('km14_katim', $assign_id, '')) ?>"></td>
				</tr>
			</table>			
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="submit" class="blue_btn" value="Simpan">&nbsp;&nbsp;&nbsp;
					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<input type="hidden" name="id_progres_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '1')?>">
					<input type="hidden" name="id_progres_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '2')?>">
					<input type="hidden" name="id_progres_3" value="<?=$reports->get_data_id_km14 ( $assign_id, '3')?>">
					<input type="hidden" name="id_progres_4" value="<?=$reports->get_data_id_km14 ( $assign_id, '4')?>">
					<input type="hidden" name="id_progres_5" value="<?=$reports->get_data_id_km14 ( $assign_id, '5')?>">
					<input type="hidden" name="id_progres_6" value="<?=$reports->get_data_id_km14 ( $assign_id, '6')?>">
					<input type="hidden" name="id_progres_7" value="<?=$reports->get_data_id_km14 ( $assign_id, '7')?>">
					<input type="hidden" name="id_progres_8" value="<?=$reports->get_data_id_km14 ( $assign_id, '8')?>">
					<input type="hidden" name="id_progres_8_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '8_1')?>">
					<input type="hidden" name="id_progres_8_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '8_2')?>">
					<input type="hidden" name="id_progres_8_3" value="<?=$reports->get_data_id_km14 ( $assign_id, '8_3')?>">
					<input type="hidden" name="id_progres_8_4" value="<?=$reports->get_data_id_km14 ( $assign_id, '8_4')?>">
					<input type="hidden" name="id_progres_9" value="<?=$reports->get_data_id_km14 ( $assign_id, '9')?>">
					<input type="hidden" name="id_progres_10" value="<?=$reports->get_data_id_km14 ( $assign_id, '10')?>">
					<input type="hidden" name="id_progres_11" value="<?=$reports->get_data_id_km14 ( $assign_id, '11')?>">
					<input type="hidden" name="id_progres_12" value="<?=$reports->get_data_id_km14 ( $assign_id, '12')?>">
					<input type="hidden" name="id_progres_13" value="<?=$reports->get_data_id_km14 ( $assign_id, '13')?>">
					<input type="hidden" name="id_progres_14" value="<?=$reports->get_data_id_km14 ( $assign_id, '14')?>">
					<input type="hidden" name="id_progres_14_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '14_1')?>">
					<input type="hidden" name="id_progres_14_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '14_2')?>">
					<input type="hidden" name="id_progres_14_3" value="<?=$reports->get_data_id_km14 ( $assign_id, '14_3')?>">
					<input type="hidden" name="id_progres_15" value="<?=$reports->get_data_id_km14 ( $assign_id, '15')?>">
					<input type="hidden" name="id_progres_16" value="<?=$reports->get_data_id_km14 ( $assign_id, '16')?>">
					<input type="hidden" name="id_progres_17" value="<?=$reports->get_data_id_km14 ( $assign_id, '17')?>">
					<input type="hidden" name="id_progres_18_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '18_1')?>">
					<input type="hidden" name="id_progres_18_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '18_2')?>">
					<input type="hidden" name="id_progres_18_3" value="<?=$reports->get_data_id_km14 ( $assign_id, '18_3')?>">
					<input type="hidden" name="id_progres_19_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '19_1')?>">
					<input type="hidden" name="id_progres_19_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '19_2')?>">
					<input type="hidden" name="id_progres_19_3" value="<?=$reports->get_data_id_km14 ( $assign_id, '19_3')?>">
					<input type="hidden" name="id_progres_20_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '20_1')?>">
					<input type="hidden" name="id_progres_20_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '20_2')?>">
					<input type="hidden" name="id_progres_21_1" value="<?=$reports->get_data_id_km14 ( $assign_id, '21_1')?>">
					<input type="hidden" name="id_progres_21_2" value="<?=$reports->get_data_id_km14 ( $assign_id, '21_2')?>">
					<input type="hidden" name="id_progres_21_3" value="<?=$reports->get_data_id_km14 ( $assign_id, '21_3')?>">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script type="text/javascript">
	$("#tanggal_dalnis, #tanggal_katim").datepicker({
		dateFormat: 'dd-mm-yy',
		nextText: "",
		prevText: "",
		changeYear: true,
		changeMonth: true,
		minDate: '<?= $comfunc->dateIndo($arr['assign_start_date']) ?>',
		maxDate: '<?= $comfunc->dateIndo($arr['assign_end_date']) ?>',
	});
	function download(idAssign){
		window.open("ReportManagement/laporan_kma_14_pdf.php?fil_id="+idAssign); 
	}
</script>