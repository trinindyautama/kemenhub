<section id="main" class="column">
<article class="module width_3_quarter">
	<header>
		<h3 class="tabs_involved"><?=$page_title?></h3>
	</header>
	<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
		<table align="center" width="95%">
			<tr>
				<td colspan="3" align="center"><strong>SURAT TUGAS</strong></td>
			</tr>
			<tr>
				<td colspan="2" width="10%">Nomor</td>
				<td>: <input type="text" class="cmb_risk" name="nomor" size="25" value="<?=$arr_data['km27_nomor']?>"></td>
			</tr>
			<tr>
				<td colspan="2">Lampiran</td>
				<td>: <input type="text" class="cmb_risk" name="lampiran" size="25" value="<?=$arr_data['km27_lampiran']?>"></td>
			</tr>
			<tr>
				<td colspan="2">Perihal</td>
				<td>: <input type="text" class="cmb_risk" name="perihal" size="25" value="<?=$arr_data['km27_perihal']?>"></td>
			</tr>
			<tr>
				<td colspan="3" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" valign="top">Kepada Yth.</td>
				<td>
				<?
				$rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
				$assign_id_auditee = "";
				while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
					echo $arr_id_auditee ['auditee_name']."<br>";
				}
				?>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">Berdasarkan Surat Tugas No <?=$reports->get_surat_tugas_no($assign_id);?>, dengan ini kami menugaskan <?=$get_katim?> untuk melakukan audit atas kegiatan seperti yang tercantum pada pokok surat dalam wilayah unit yang saudara pimpin.</td>
			</tr>
			<tr>
				<td colspan="3" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">Adapun susunan tim auditor adalah sebagai berikut:</td>
			</tr>
			<tr>
				<td colspan="3" align="center">&nbsp;</td>
			</tr>
			<?
				$no=0;
				$rs_auditor = $assigns->view_auditor_assign ( $arr ['assign_id']);
				while ( $arr_auditor = $rs_auditor->FetchRow () ) {
					$no++;
			?>
			<tr>
				<td width="2%"	><?=$no?></td>
				<td colspan="2"><?=$arr_auditor ['auditor_name']?> - <?=$arr_auditor ['posisi_name']?></td>
			</tr>
			<?php 
				}
			?>
			<tr>
				<td colspan="3" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">Kami harapkan bantuan dan kerjasama Saudara dan seluruh staf agar kegiatan audit berjalan dengan lancar.</td>
			</tr>
		</table>
		<br>
		<table align="center" width="95%">
			<tr>
				<td>
					Pimpinan APIP<br><br><br><br><br>
										
				</td>
			</tr>
		</table>
		<fieldset>
			<center>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<input type="submit" class="blue_btn" value="Simpan">
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
				<input type="hidden" name="km_id" value="<?=$arr_data['km27_id']?>">
			</center>
		</fieldset>
	</form>
</article>
</section>