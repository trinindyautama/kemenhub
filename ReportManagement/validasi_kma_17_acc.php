<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Formulir KMA 17</h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="5" align="center">Checklist<br>PENYELESAIAN LAPORAN</td>
				</tr>
				<tr>
					<th width="5%" align="center">No</th>
					<th width="65%" align="center" colspan="2">Uraian</th>
					<th width="10%" align="center">Sudah/belum <br> <input type="checkbox" id="check_all"></th>
					<th width="20%" align="center">Keterangan</th>
				</tr>
				<tr>
					<td align="center">1</td>
					<td align="center" colspan="2">2</td>
					<td align="center">3</td>
					<td align="center">4</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td colspan="2">RINGKASAN PIMPINAN</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">1.</td>
					<td colspan="2">Ringkasan Pimpinan menyajikan overview ringkas atas auditi, tujuan audit, ruang lingkup, referensi atas kriteria audit, metodologi audit, dan simpulan hasil audit atas setiap tujuan audit</td>
					<td align="center"><?=$comfunc->check ( "st_1", $reports->get_data_st_km17 ( $assign_id, '1'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_1" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td colspan="2">BODI LAPORAN</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">2.</td>
					<td colspan="2">Kecukupan informasi latar belakang auditi</td>
					<td align="center"><?=$comfunc->check ( "st_2", $reports->get_data_st_km17 ( $assign_id, '2'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_2" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '2')?>"></td>
				</tr>
				<tr>
					<td align="left">3.</td>
					<td colspan="2">Tujuan audit dan kriteria yang berkaitan</td>
					<td align="center"><?=$comfunc->check ( "st_3", $reports->get_data_st_km17 ( $assign_id, '3'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_3" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '3')?>"></td>
				</tr>
				<tr>
					<td align="left">4.</td>
					<td colspan="2">Ruang lingkup audit sudah dinyatakan secara jelas</td>
					<td align="center"><?=$comfunc->check ( "st_4", $reports->get_data_st_km17 ( $assign_id, '4'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_4" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '4')?>"></td>
				</tr>
				<tr>
					<td align="left">5.</td>
					<td colspan="2">Jadwal audit, metodologi, standar audit yang diacu. Jika ada standar yang tidak diikuti, penjelasan yang memadai telah dibuat</td>
					<td align="center"><?=$comfunc->check ( "st_5", $reports->get_data_st_km17 ( $assign_id, '5'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_5" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '5')?>"></td>
				</tr>
				<tr>
					<td align="left">6.</td>
					<td colspan="2">Hasil observasi yang mendalam yang berkaitan dengan tujuan dan kriteria audit telah diperoleh untuk mencapai simpulan audit</td>
					<td align="center"><?=$comfunc->check ( "st_6", $reports->get_data_st_km17 ( $assign_id, '6'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_6" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '6')?>"></td>
				</tr>
				<tr>
					<td align="left">7.</td>
					<td colspan="2">Setiap observasi berisi pernyataan kondisi, kriteria, penyebab, dampak dan rekomendasi</td>
					<td align="center"><?=$comfunc->check ( "st_7", $reports->get_data_st_km17 ( $assign_id, '7'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_7" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '7')?>"></td>
				</tr>
				<tr>
					<td align="left">8.</td>
					<td colspan="2">Bukti yang cukup dan persuasif telah dikumpulkan untuk mendukung setiap observasi yang dilakukan</td>
					<td align="center"><?=$comfunc->check ( "st_8", $reports->get_data_st_km17 ( $assign_id, '8'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_8" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '8')?>"></td>
				</tr>
				<tr>
					<td align="left">9.</td>
					<td colspan="2">Temuan yang bisa dikuantifisir telah dihitung secara memadai</td>
					<td align="center"><?=$comfunc->check ( "st_9", $reports->get_data_st_km17 ( $assign_id, '9'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_9" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '9')?>"></td>
				</tr>
				<tr>
					<td align="left">10.</td>
					<td colspan="2">Rekomendasi yang diberikan telah mengikuti alur logis dari hasil observasi dan penyebab, jelas dan cost-effective ditujukan pada pihak yang berkompeten</td>
					<td align="center"><?=$comfunc->check ( "st_10", $reports->get_data_st_km17 ( $assign_id, '10'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_10" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '10')?>"></td>
				</tr>
				<tr>
					<td align="left">11.</td>
					<td colspan="2">Simpulan telah disajikan untuk setiap tujuan audit dan telah didukung dengan bukti yang persuasif</td>
					<td align="center"><?=$comfunc->check ( "st_11", $reports->get_data_st_km17 ( $assign_id, '11'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_11" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '11')?>"></td>
				</tr>
				<tr>
					<td align="left">12.</td>
					<td colspan="2">Lampiran-lampiran yang disajikan memang menambah nilai laporan</td>
					<td align="center"><?=$comfunc->check ( "st_12", $reports->get_data_st_km17 ( $assign_id, '12'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_12" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '12')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td colspan="2">FORMAT LAPORAN</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">13.</td>
					<td colspan="2">Daftar isi yang menggambarkan struktur laporan dan judul yang sama dengan judul yang sama pada halaman bodi</td>
					<td align="center"><?=$comfunc->check ( "st_13", $reports->get_data_st_km17 ( $assign_id, '13'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_13" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '13')?>"></td>
				</tr>
				<tr>
					<td align="left">14.</td>
					<td colspan="2">Judul dan huruf yang konsisten</td>
					<td align="center"><?=$comfunc->check ( "st_14", $reports->get_data_st_km17 ( $assign_id, '14'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_14" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '14')?>"></td>
				</tr>
				<tr>
					<td align="left">15.</td>
					<td colspan="2">Bagan dan gambar telah dirujuk secara memadai dalam bodi laporan</td>
					<td align="center"><?=$comfunc->check ( "st_15", $reports->get_data_st_km17 ( $assign_id, '15'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_15" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '15')?>"></td>
				</tr>
				<tr>
					<td align="left">16.</td>
					<td colspan="2">Struktur kalimat dan paragraf yang mudah dipahami</td>
					<td align="center"><?=$comfunc->check ( "st_16", $reports->get_data_st_km17 ( $assign_id, '16'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_16" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '16')?>"></td>
				</tr>
				<tr>
					<td align="left">17.</td>
					<td colspan="2">Singkatan-singkatan telah didefinisikan</td>
					<td align="center"><?=$comfunc->check ( "st_17", $reports->get_data_st_km17 ( $assign_id, '17'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_18" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '17')?>"></td>
				</tr>
				<tr>
					<td align="left">18.</td>
					<td colspan="2">Bahasa dan terminologi yang mudah dipahami</td>
					<td align="center"><?=$comfunc->check ( "st_18", $reports->get_data_st_km17 ( $assign_id, '18'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_17" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '18')?>"></td>
				</tr>
				<tr>
					<td align="left">19.</td>
					<td colspan="2">Tata bahasa dan penulisan kata yang tepat</td>
					<td align="center"><?=$comfunc->check ( "st_19", $reports->get_data_st_km17 ( $assign_id, '19'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_19" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '19')?>"></td>
				</tr>
				<tr>
					<td align="left">20.</td>
					<td colspan="2">Lampiran disajikan secara seragam dan dirujuk pada bodi laporan</td>
					<td align="center"><?=$comfunc->check ( "st_20", $reports->get_data_st_km17 ( $assign_id, '20'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_20" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '20')?>"></td>
				</tr>
				<tr>
					<td align="left">21.</td>
					<td colspan="2">Secara keseluruhan, laporan sudah jelas dan tepat</td>
					<td align="center"><?=$comfunc->check ( "st_21", $reports->get_data_st_km17 ( $assign_id, '21'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_21" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '21')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td colspan="2">LAIN-LAIN</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">22.</td>
					<td colspan="2">Penyusunan telah melalui proses reviu:</td>
					<td align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Pengendali Teknis</td>
					<td align="center"><?=$comfunc->check ( "st_22_1", $reports->get_data_st_km17 ( $assign_id, '22_1'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_22_1" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '22_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center" width="2%">o</td>
					<td>Pengendali Mutu</td>
					<td align="center"><?=$comfunc->check ( "st_22_2", $reports->get_data_st_km17 ( $assign_id, '22_2'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_22_2" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '22_2')?>"></td>
				</tr>
				<tr>
					<td align="left">23.</td>
					<td colspan="2">Distribusi Laporan telah sesuai dengan ketentuan</td>
					<td align="center"><?=$comfunc->check ( "st_23", $reports->get_data_st_km17 ( $assign_id, '23'));?></td>
					<td align="center"><input type="text" class="cmb_risk" name="ket_23" size="25" value="<?=$reports->get_data_ket_km17 ( $assign_id, '23')?>"></td>
				</tr>
				<tr>
					<td colspan="3" style="border:0px">Direview oleh,
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Tanggal <input type="text"  class="span3" name="tanggal_dalnis" id="tanggal_dalnis" value="<?= $comfunc->dateIndo($reports->get_data_km17('km17_dalnis', $assign_id, '')) ?>"><br>Pengendali Teknis<br><br><br><br><?=$get_dalnis?></td>
					<td colspan="2" style="border:0px">Diisi oleh
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Tanggal <input type="text" class="span3"  name="tanggal_katim" id="tanggal_katim" value="<?= $comfunc->dateIndo($reports->get_data_km17('km17_katim', $assign_id, '')) ?>">,<br>Ketua Tim<br><br><br><br><?=$get_katim?></td>
					<td style="border:0px"></td>
				</tr>
			</table>			
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="submit" class="blue_btn" value="Simpan">
					&nbsp;&nbsp;&nbsp;
					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<input type="hidden" name="id_progres_1" value="<?=$reports->get_data_id_km17 ( $assign_id, '1')?>">
					<input type="hidden" name="id_progres_2" value="<?=$reports->get_data_id_km17 ( $assign_id, '2')?>">
					<input type="hidden" name="id_progres_3" value="<?=$reports->get_data_id_km17 ( $assign_id, '3')?>">
					<input type="hidden" name="id_progres_4" value="<?=$reports->get_data_id_km17 ( $assign_id, '4')?>">
					<input type="hidden" name="id_progres_5" value="<?=$reports->get_data_id_km17 ( $assign_id, '5')?>">
					<input type="hidden" name="id_progres_6" value="<?=$reports->get_data_id_km17 ( $assign_id, '6')?>">
					<input type="hidden" name="id_progres_7" value="<?=$reports->get_data_id_km17 ( $assign_id, '7')?>">
					<input type="hidden" name="id_progres_8" value="<?=$reports->get_data_id_km17 ( $assign_id, '8')?>">
					<input type="hidden" name="id_progres_9" value="<?=$reports->get_data_id_km17 ( $assign_id, '9')?>">
					<input type="hidden" name="id_progres_10" value="<?=$reports->get_data_id_km17 ( $assign_id, '10')?>">
					<input type="hidden" name="id_progres_11" value="<?=$reports->get_data_id_km17 ( $assign_id, '11')?>">
					<input type="hidden" name="id_progres_12" value="<?=$reports->get_data_id_km17 ( $assign_id, '12')?>">
					<input type="hidden" name="id_progres_13" value="<?=$reports->get_data_id_km17 ( $assign_id, '13')?>">
					<input type="hidden" name="id_progres_14" value="<?=$reports->get_data_id_km17 ( $assign_id, '14')?>">
					<input type="hidden" name="id_progres_15" value="<?=$reports->get_data_id_km17 ( $assign_id, '15')?>">
					<input type="hidden" name="id_progres_16" value="<?=$reports->get_data_id_km17 ( $assign_id, '16')?>">
					<input type="hidden" name="id_progres_17" value="<?=$reports->get_data_id_km17 ( $assign_id, '17')?>">
					<input type="hidden" name="id_progres_18" value="<?=$reports->get_data_id_km17 ( $assign_id, '18')?>">
					<input type="hidden" name="id_progres_19" value="<?=$reports->get_data_id_km17 ( $assign_id, '19')?>">
					<input type="hidden" name="id_progres_20" value="<?=$reports->get_data_id_km17 ( $assign_id, '20')?>">
					<input type="hidden" name="id_progres_21" value="<?=$reports->get_data_id_km17 ( $assign_id, '21')?>">
					<input type="hidden" name="id_progres_22_1" value="<?=$reports->get_data_id_km17 ( $assign_id, '22_1')?>">
					<input type="hidden" name="id_progres_22_2" value="<?=$reports->get_data_id_km17 ( $assign_id, '22_2')?>">
					<input type="hidden" name="id_progres_23" value="<?=$reports->get_data_id_km17 ( $assign_id, '23')?>">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script type="text/javascript">
$("#check_all").on("change", function() {
	$(".check").prop('checked', this.checked);
});
$("#tanggal_dalnis, #tanggal_katim").datepicker({
	dateFormat: 'dd-mm-yy',
	nextText: "",
	prevText: "",
	changeYear: true,
	changeMonth: true,
	minDate: '<?= $comfunc->dateIndo($arr['assign_start_date']) ?>',
	maxDate: '<?= $comfunc->dateIndo($arr['assign_end_date']) ?>',
});
function download(idAssign){
	window.open("ReportManagement/laporan_kma_17_pdf.php?fil_id="+idAssign, "_self"); 
}
</script>