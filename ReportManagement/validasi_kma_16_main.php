<?
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_REQUEST ["fil_id"])){
	@session_start();
	$_SESSION['ses_assign'] = $comfunc->replacetext ( $_REQUEST ["fil_id"] );
}
$assign_id = $_SESSION['ses_assign'];

$paging_request = "main_page.php?method=validate_kma_16";
$acc_page_request = "validasi_kma_16_acc.php";
$list_page_request = "report_view.php";
$view_parrent = "validasi_kma_16_parrent.php";
$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid_km.php";
$gridHeader = array ("Halaman LHA", "Masalah yang dijumpai", "No KKA", "Penyelesaian Masalah", "Dilakukan Oleh", "Ket");
$gridDetail = array ("km16_lha", "km16_masalah", "kertas_kerja_no", "km16_komen", "auditor_name", "km16_ket");
$gridWidth = array ("10", "25", "5", "25", "10", "10");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

$rs_assign = $assigns->assign_viewlist($assign_id);
$arr_assign = $rs_assign->FetchRow();

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];
$get_user_dalnis = $arr_dalnis['user_id'];
if($ses_id_int==$arr_dalnis['auditor_id'])
	$dalnis = true;
else
	$dalnis = false;
	
$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];
$get_user_katim = $arr_katim['user_id'];
if($ses_id_int==$arr_katim['auditor_id'])
	$katim = true;
else
	$katim = false;

switch ($_action) {
	case "getajukan_km" :
	case "getapprove_km" :
		$_nextaction = "postkomentar";
		$page_request = $acc_page_request;
		$id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$status = $comfunc->replacetext ( $_REQUEST ["status"] );
		$rs = $reports->km16_data_viewlist ( $id );
		$page_title = "Reviu KM 16";
		break;
	case "postkomentar" :
		$id = $comfunc->replacetext ( $_POST ["data_id"] );
		$status = $comfunc->replacetext ( $_POST ["status"] );
		$fkomentar = $comfunc->replacetext ( $_POST ["komentar"] );
		$notif_date = $comfunc->dateSql(date("d-m-Y"));
		$comfunc->hapus_notif($id);
		$group_menu_id = "";
		if ($status != "") {
			if($status=='1') { //ajukan
				$comfunc->insert_notif($id, $ses_userId, $get_user_dalnis, 'validasi_kma_filter', "(KM 16) ".$fkomentar, $notif_date);
			}elseif ($status=='3'){//tolak
				$comfunc->insert_notif($id, $ses_userId, $get_user_katim, 'validasi_kma_filter', "(KM 16) ".$fkomentar, $notif_date);
			}
			$reports->km16_update_status ( $id, $status );
		}
		
		$ftanggal = $comfunc->date_db ( date ( "d-m-Y H:i:s" ) );
		if ($fkomentar != "") {
			$reports->km16_add_komentar ( $id, $fkomentar, $ftanggal );
			$comfunc->js_alert_act ( 3 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Pemasalahan";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $reports->km16_data_viewlist ( $fdata_id );
		$page_title = "Ubah Pemasalahan";
		break;
	case "postadd" :
		$flha = $comfunc->replacetext ( $_POST ["lha"] );
		$fmasalah = $comfunc->replacetext ( $_POST ["masalah"] );
		$fkka_id = $comfunc->replacetext ( $_POST ["kka_id"] );
		$fjawaban = $comfunc->replacetext ( $_POST ["jawaban"] );
		$fauditor_id = $comfunc->replacetext ( $_POST ["auditor_id"] );
		$fket = $comfunc->replacetext ( $_POST ["ket"] );
		if ($fmasalah != "") {
			$reports->km16_add ( $assign_id, $flha, $fmasalah, $fkka_id, $fjawaban, $fauditor_id, $fket );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$flha = $comfunc->replacetext ( $_POST ["lha"] );
		$fmasalah = $comfunc->replacetext ( $_POST ["masalah"] );
		$fkka_id = $comfunc->replacetext ( $_POST ["kka_id"] );
		$fjawaban = $comfunc->replacetext ( $_POST ["jawaban"] );
		$fauditor_id = $comfunc->replacetext ( $_POST ["auditor_id"] );
		$fket = $comfunc->replacetext ( $_POST ["ket"] );
		if ($fmasalah != "") {
			$reports->km16_edit ( $fdata_id, $flha, $fmasalah, $fkka_id, $fjawaban, $fauditor_id, $fket );
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$reports->km16_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $reports->km16_count ($assign_id);
		$rs = $reports->km16_view_grid ($assign_id, $offset, $num_row );
		$page_title = "Daftar Permasalahan (KMA 16)";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
