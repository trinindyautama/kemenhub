<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span2">Penilai</label> 
				<?=$arr_penilai['auditor_name']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Berikan Nilai Kepada</label> 
				<?php
				$rs_team = $reports->get_team_penugasan($assign_id, $ses_id_int);
				$arr_team = $rs_team->GetArray ();
				echo $comfunc->buildCombo ( "dinilai", $arr_team, 0, 1, "", "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Orientasi Pelayanan</label> 
				<input type="text" class="span0" name="pelayanan">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Integritas</label> 
				<input type="text" class="span0" name="integritas">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Komitmen</label> 
				<input type="text" class="span0" name="komitmen">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Disiplin</label> 
				<input type="text" class="span0" name="disiplin">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai kerjasama</label> 
				<input type="text" class="span0" name="kerjasama">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Kepemimpinan</label> 
				<input type="text" class="span0" name="kepemimpinan">
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span2">Penilai</label> 
				 <?=$arr['auditor_name']?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Berikan Nilai Kepada</label> 
				<?php
				$rs_team = $reports->get_team_penugasan($assign_id, $ses_id_int);
				$arr_team = $rs_team->GetArray ();
				echo $comfunc->buildCombo ( "dinilai", $arr_team, 0, 1, $arr['km29_id_dinilai'], "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Orientasi Pelayanan</label> 
				<input type="text" class="span0" name="pelayanan" value="<?=$arr['km29_pelayanan']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Integritas</label> 
				<input type="text" class="span0" name="integritas" value="<?=$arr['km29_integritas']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Komitmen</label> 
				<input type="text" class="span0" name="komitmen" value="<?=$arr['km29_komitmen']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Disiplin</label> 
				<input type="text" class="span0" name="disiplin" value="<?=$arr['km29_disiplin']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai kerjasama</label> 
				<input type="text" class="span0" name="kerjasama" value="<?=$arr['km29_kerjasama']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Nilai Kepemimpinan</label> 
				<input type="text" class="span0" name="kepemimpinan" value="<?=$arr['km29_kepemimpinan']?>">
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['km29_id']?>">	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> 
					<?
					if($_action!='getdetail'){
					?>
					&nbsp;&nbsp;&nbsp; 
					<input type="submit" class="blue_btn" value="Simpan">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<?
					}
					?>
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script> 
$(function() {
	$("#validation-form").validate({
		rules: {
			dinilai: "required"
		},
		messages: {
			dinilai: "Silahkan Pilih Personil Yang Akan Dinilai"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>