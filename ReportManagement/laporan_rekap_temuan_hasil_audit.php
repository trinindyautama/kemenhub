<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/tindaklanjut_class.php";

$reports = new report ( $ses_userId );
$tindaklanjuts = new tindaklanjut ( $ses_userId );

$fil_tahun_id = "";
$fil_tahun_id = $comfunc->replacetext ( $_POST ["fil_tahun_id"] );
?>

<section id="main" class="column">	
	<article class="module width_3_quarter">
		<center>
		<strong><br>
			REKAPITULASI TEMUAN HASIL AUDIT INSPEKTORAT JENDERAL KEMENTERIAN PERHUBUNGAN<br>
			DI LINGKUNGAN<br>
			TAHUN ANGGARAN 
		</strong>
		</center>
		<br>		
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr align="center">
                <th width="1%" rowspan="3" style="text-align: center">No</th>
                <th width="2%" rowspan="3" style="text-align: center">Auditi</th>
                <th width="10%" colspan="3" style="text-align: center">Uraian</th>
                <th width="10%" colspan="3" style="text-align: center">Rekomendasi</th>
                <th width="10%" colspan="12" style="text-align: center">Tindak Lanjut</th>
            </tr>
            <tr align="center">
                <th width="5%" rowspan="2" style="text-align: center">Temuan</th>
                <th width="5%" rowspan="2" style="text-align: center">Rekomendasi</th>
                <th  width="5%" rowspan="2" style="text-align: center">Tindak Lanjut</th>
                <th width="5%" rowspan="2"  width="10%" style="text-align: center">Jml</th>
                <th width="5%" rowspan="2"  width="10%" style="text-align: center">Rp.</th>
                <th width="5%" rowspan="2"  width="10%" style="text-align: center">US.$</th>
                <th width="5%" colspan="3"  width="10%" style="text-align: center">TLT</th>
                <th width="5%" colspan="3"  width="10%" style="text-align: center">TLP</th>
				<th width="5%" colspan="3"  width="10%" style="text-align: center">BTL</th>
                <th width="5%" colspan="3"  width="10%" style="text-align: center">TDTL</th>
            </tr>
			<tr>
				<th width="1%" style="text-align: center">Jml</th>
                <th width="1%" style="text-align: center">Rp.</th>
                <th width="1%" style="text-align: center">US.$</th>
				<th width="1%" style="text-align: center">Jml</th>
                <th width="1%" style="text-align: center">Rp.</th>
                <th width="1%" style="text-align: center">US.$</th>
				<th width="1%" style="text-align: center">Jml</th>
                <th width="1%" style="text-align: center">Rp.</th>
                <th width="1%" style="text-align: center">US.$</th>
				<th width="1%" style="text-align: center">Jml</th>
                <th width="1%" style="text-align: center">Rp.</th>
                <th width="1%" style="text-align: center">US.$</th>
			</tr>
			<?
			$no_tem=1;
			$rs = $reports->list_rekap_temuan_hasil_audit($fil_tahun_id);
			while($arr = $rs->FetchRow()) {
				$rs_temuan = $reports->list_temuan_hasil_audit($arr['assign_id']);
                while ($arr_temuan = $rs_temuan->FetchRow()) {
					$no_rek = 0;
					$rs_rek = $reports->get_rekomendasi_per_temuan_list($arr_temuan['finding_id']);
                    $count_rek = $rs_rek->RecordCount();
                    while ($arr_rek = $rs_rek->FetchRow()) {
						$no_rek++;
						$no_tl = 0;
						$rs_tl = $reports->get_tl_per_rekomendasi_list($arr_rek['rekomendasi_id']);
                        $count_tl = $rs_tl->RecordCount();
                        while ($arr_tl = $rs_tl->FetchRow()) {
							$no_tl++;
							$rs_count_tlt = $reports->tindaklanjut_count_tlt ( $arr_rek ['rekomendasi_id'] );
							$rs_count_tlp = $reports->tindaklanjut_count_tlp ( $arr_rek ['rekomendasi_id'] );
							$rs_count_btl = $reports->tindaklanjut_count_btl ( $arr_rek ['rekomendasi_id'] );
							$rs_count_tdtl = $reports->tindaklanjut_count_tdtl ( $arr_rek ['rekomendasi_id'] );
                ?>
			<tr>
				<?if($no_rek == 1 && $no_tl == 1):?>
                <td align="center" rowspan="<?= $reports->count_tl_per_rekomendasi($arr_temuan['finding_id']) ?>"><?=$no_tem?></td>
                <td rowspan="<?= $reports->count_tl_per_rekomendasi($arr_temuan['finding_id']) ?>"><b><?=$arr['auditee_name']?> Prop. <?=$arr['propinsi_name']?></b><br><?=$arr['assign_surat_no']?><br><?=$comfunc->dateIndo2($arr['assign_surat_tgl'])?></td>
                <td rowspan="<?= $reports->count_tl_per_rekomendasi($arr_temuan['finding_id']) ?>"><?=$no_tem?>.&nbsp;<?=$arr_temuan['finding_judul']?></td>
				<?endif; ?>
				<?if($no_tl == 1):?>
                <td rowspan="<?= $count_tl ?>"><?=$no_rek?>.&nbsp;<?=$arr_rek['rekomendasi_desc']?></td>
                <?endif; ?>
				<td><?=$no_tl?>.&nbsp;<?=$arr_tl['tl_desc']?></td>
                <td>&nbsp;</td>
                <td><?=$arr_rek['rekomendasi_nilai']?></td>
                <td>&nbsp;</td>
                <td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "2" ? $rs_count_tlt : ""  ?></td>
                <td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "2" ? $arr_tl['tl_nilai'] : ""  ?></td>
                <td>&nbsp;</td>
				<td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "3" ? $rs_count_tlp : ""  ?></td>
                <td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "3" ? $arr_tl['tl_nilai'] : ""  ?></td>
                <td>&nbsp;</td>
                <td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "4" ? $rs_count_btl : ""  ?></td>
                <td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "4" ? $arr_tl['tl_nilai'] : ""  ?></td>
                <td>&nbsp;</td>
                <td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "5" ? $rs_count_tdtl : ""  ?></td>
				<td><?= $reports->check_status_rekap_temuan_hasil_audits($arr_tl['tl_id']) == "5" ? $arr_tl['tl_nilai'] : ""  ?></td>
                <td>&nbsp;</td>
            </tr>	
            <?
				
						}
						
					}
					$no_tem++;
				}
			}			
			?>	
		</table>
		<br>
		<br>	
	</article>
</section>