<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">
				Formulir KMA 10
			</h3>	
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="7" align="center">Checklist<br>PENYELESAIAN PENUGASAN PERENCANAAN AUDIT</td>
				</tr>
				<tr>
					<th width="5%" align="center">No</th>
					<th width="75%" align="center" colspan="2">Jenis pekerjaan yang harus dilakukan</th>
					<th width="10%" align="center">Sudah/belum <br> 
						<input type="checkbox" id="check_all" >
					</th>
					<th width="10%" align="center">	% penyelesaian</th>
				</tr>
				<tr>
					<td align="center">1</td>
					<td align="center" colspan="2">2</td>
					<td align="center">3</td>
					<td align="center">4</td>
				</tr>
				<tr>
					<td align="center">1</td>
					<td colspan="2">Sudahkah dibuat Kartu Penugasan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_1" <?= $reports->get_data_st_km10 ( $assign_id, '1') == 1? "checked" : ""  ?>>
					<td align="center"><input type="text" class="cmb_risk" name="progres_1" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '1')?>"></td>
				</tr>
				<tr>
					<td align="center">2</td>
					<td colspan="2">Sudahkan dikembangkan Tujuan Evaluasi, Lingkup Pekerjaan, Penaksiran Risiko Segmen Kegiatan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_2" <?= $reports->get_data_st_km10 ( $assign_id, '2') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_2" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '2')?>"></td>
				</tr>
				<tr>
					<td align="center">3</td>
					<td colspan="2">Apakah sudah diperoleh:</td>
					<td align="center"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center" width="5%">o</td>
					<td>Misi, tujuan dan rencana pelaksanaan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_1" <?= $reports->get_data_st_km10 ( $assign_id, '3_1') == 1? "checked" : ""  ?>>		
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_1" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Informasi organisasi</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_2" <?= $reports->get_data_st_km10 ( $assign_id, '3_2') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_2" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_2')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>KKA terakhir</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_3" <?= $reports->get_data_st_km10 ( $assign_id, '3_3') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_3" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_3')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>File permanen</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_4" <?= $reports->get_data_st_km10 ( $assign_id, '3_4') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_4" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_4')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>LHP auditor ekstern</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_5" <?= $reports->get_data_st_km10 ( $assign_id, '3_5') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_5" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_5')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Data pembanding</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_6" <?= $reports->get_data_st_km10 ( $assign_id, '3_6') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_6" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_6')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Anggaran</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_7" <?= $reports->get_data_st_km10 ( $assign_id, '3_7') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_7" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_7')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Literatur teknis</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_3_8" <?= $reports->get_data_st_km10 ( $assign_id, '3_8') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_3_8" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '3_8')?>"></td>
				</tr>
				<tr>
					<td align="center">4</td>
					<td colspan="2">Jika ada perubahan apakah sudah dibuat memo persetujuan dan sudah dilampirkan ke kartu penugasan di Pengendali Mutu</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_4" <?= $reports->get_data_st_km10 ( $assign_id, '4') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_4" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '4')?>"></td>
				</tr>
				<tr>
					<td align="center">5</td>
					<td colspan="2">Adakah perubahan auditor dari rencana semula</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_5" <?= $reports->get_data_st_km10 ( $assign_id, '5') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_5" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '5')?>"></td>
				</tr>
				<tr>
					<td align="center">6</td>
					<td colspan="2">Apakah sudah dibuat rapat koordinasi</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_6" <?= $reports->get_data_st_km10 ( $assign_id, '6') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_6" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '6')?>"></td>
				</tr>
				<tr>
					<td align="center">7</td>
					<td colspan="2">Apakah sudah dibuat ringkasannya dan telah didistribusikan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_7" <?= $reports->get_data_st_km10 ( $assign_id, '7') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_7" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '7')?>"></td>
				</tr>
				<tr>
					<td align="center">8</td>
					<td colspan="2">Apakah sudah dibuat persiapan survei pendahuluan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_8" <?= $reports->get_data_st_km10 ( $assign_id, '8') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_8" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '8')?>"></td>
				</tr>
				<tr>
					<td align="center">9</td>
					<td colspan="2">Apakah survei pendahuluan telah dilaksanakan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_9" <?= $reports->get_data_st_km10 ( $assign_id, '9') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_9" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '9')?>"></td>
				</tr>
				<tr>
					<td align="center">10</td>
					<td colspan="2">Apakah telah dibuat ikhtisar hasil survei</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_10" <?= $reports->get_data_st_km10 ( $assign_id, '10') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_10" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '10')?>"></td>
				</tr>
				<tr>
					<td align="center">11</td>
					<td colspan="2">Apakah telah ditulis program evaluasi</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_11" <?= $reports->get_data_st_km10 ( $assign_id, '11') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_11" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '11')?>"></td>
				</tr>
				<tr>
					<td align="center">12</td>
					<td colspan="2">Apakah program evaluasi telah mengacu pada program baku dan hasil pengumpulan informasi</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_12" <?= $reports->get_data_st_km10 ( $assign_id, '12') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_12" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '12')?>"></td>
				</tr>
				<tr>
					<td align="center">13</td>
					<td colspan="2">Apakah program evaluasi telah mendapat persetujuan Pengendali Teknis</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_13" <?= $reports->get_data_st_km10 ( $assign_id, '13') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_13" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '13')?>"></td>
				</tr>
				<tr>
					<td align="center">14</td>
					<td colspan="2">Apakah tahapan pekerjaan telah sesuai dengan anggaran waktunya</td>
					<td align="center"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Penetapan tujuan, lingkup dan penaksiran resiko</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_1" <?= $reports->get_data_st_km10 ( $assign_id, '14_1') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_1" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_1')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Pengumpulan informasi awal</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_2" <?= $reports->get_data_st_km10 ( $assign_id, '14_2') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_2" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_2')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Penetapan staf audit</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_3" <?= $reports->get_data_st_km10 ( $assign_id, '14_3') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_3" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_3')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Rapat pendahuluan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_4" <?= $reports->get_data_st_km10 ( $assign_id, '14_4') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_4" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_4')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Survei pendahuluan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_5" <?= $reports->get_data_st_km10 ( $assign_id, '14_5') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_5" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_5')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Penulisan program audit</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_6" <?= $reports->get_data_st_km10 ( $assign_id, '14_6') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_6" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_6')?>"></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center">o</td>
					<td>Persetujuan program audit</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_14_7" <?= $reports->get_data_st_km10 ( $assign_id, '14_7') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_14_7" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '14_7')?>"></td>
				</tr>
				<tr>
					<td align="center">15</td>
					<td colspan="2">Apakah kertas kerja evaluasi telah selesai dikerjakan</td>
					<td align="center">
						<input type="checkbox" class="view" name="st_15" <?= $reports->get_data_st_km10 ( $assign_id, '15') == 1? "checked" : ""  ?>>
					</td>
					<td align="center"><input type="text" class="cmb_risk" name="progres_15" size="4" value="<?=$reports->get_data_persen_km10 ( $assign_id, '15')?>"></td>
				</tr>
				<tr>
					<td colspan="3" align="center">Diketahui,<br>Pengendali Mutu<br><br><br><br><?=$get_daltu?></td>
					<td colspan="2" align="center"><input type="text" size="10" name="tanggal_kartu" id="tanggal_kartu" value="<?=$comfunc->dateIndo($reports->get_data_km10($assign_id, 'km10_date', ''))?>">,<br>Pengendali Teknis<br><br><br><br><?=$get_dalnis?></td>
				</tr>
			</table>			
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="submit" class="blue_btn" value="Simpan"> &nbsp;&nbsp;&nbsp;

					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<input type="hidden" name="id_progres_1" value="<?=$reports->get_data_id_km10 ( $assign_id, '1')?>">
					<input type="hidden" name="id_progres_2" value="<?=$reports->get_data_id_km10 ( $assign_id, '2')?>">
					<input type="hidden" name="id_progres_3_1" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_1')?>">
					<input type="hidden" name="id_progres_3_2" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_2')?>">
					<input type="hidden" name="id_progres_3_3" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_3')?>">
					<input type="hidden" name="id_progres_3_4" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_4')?>">
					<input type="hidden" name="id_progres_3_5" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_5')?>">
					<input type="hidden" name="id_progres_3_6" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_6')?>">
					<input type="hidden" name="id_progres_3_7" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_7')?>">
					<input type="hidden" name="id_progres_3_8" value="<?=$reports->get_data_id_km10 ( $assign_id, '3_8')?>">
					<input type="hidden" name="id_progres_4" value="<?=$reports->get_data_id_km10 ( $assign_id, '4')?>">
					<input type="hidden" name="id_progres_5" value="<?=$reports->get_data_id_km10 ( $assign_id, '5')?>">
					<input type="hidden" name="id_progres_6" value="<?=$reports->get_data_id_km10 ( $assign_id, '6')?>">
					<input type="hidden" name="id_progres_7" value="<?=$reports->get_data_id_km10 ( $assign_id, '7')?>">
					<input type="hidden" name="id_progres_8" value="<?=$reports->get_data_id_km10 ( $assign_id, '8')?>">
					<input type="hidden" name="id_progres_9" value="<?=$reports->get_data_id_km10 ( $assign_id, '9')?>">
					<input type="hidden" name="id_progres_10" value="<?=$reports->get_data_id_km10 ( $assign_id, '10')?>">
					<input type="hidden" name="id_progres_11" value="<?=$reports->get_data_id_km10 ( $assign_id, '11')?>">
					<input type="hidden" name="id_progres_12" value="<?=$reports->get_data_id_km10 ( $assign_id, '12')?>">
					<input type="hidden" name="id_progres_13" value="<?=$reports->get_data_id_km10 ( $assign_id, '13')?>">
					<input type="hidden" name="id_progres_14_1" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_1')?>">
					<input type="hidden" name="id_progres_14_2" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_2')?>">
					<input type="hidden" name="id_progres_14_3" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_3')?>">
					<input type="hidden" name="id_progres_14_4" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_4')?>">
					<input type="hidden" name="id_progres_14_5" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_5')?>">
					<input type="hidden" name="id_progres_14_6" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_6')?>">
					<input type="hidden" name="id_progres_14_7" value="<?=$reports->get_data_id_km10 ( $assign_id, '14_7')?>">
					<input type="hidden" name="id_progres_15" value="<?=$reports->get_data_id_km10 ( $assign_id, '15')?>">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script type="text/javascript">
	$("#check_all").on("change", function() {
		$(".view").prop('checked', this.checked);
	});
	$("#tanggal_kartu").datepicker({
		dateFormat: 'dd-mm-yy',
		nextText: "",
		prevText: "",
		changeYear: true,
		changeMonth: true,
		minDate: '<?= $comfunc->dateIndo($arr['assign_start_date']) ?>',
		maxDate: '<?= $comfunc->dateIndo($arr['assign_end_date']) ?>',
	}); 
	function download(idAssign){
		window.open("ReportManagement/laporan_kma_10_pdf.php?fil_id="+idAssign); 
	}
</script>