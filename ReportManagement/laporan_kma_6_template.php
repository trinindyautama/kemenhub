<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	<header>Formulir KMA 6</header>
	<br>
	<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="6" align="center">
						<u>KARTU PENUGASAN</u><br>Nomor : <?=$arr_km06['km06_no']?>
					</td>
				</tr>
				<tr>
					<td width="2%" rowspan="4" style="border-right: 1px;border-bottom: 0px;border-top:0px;">1</td>
					<td width="2%" style="border: 0">a.</td>
					<td width="30%" style="border: 0">Nama Auditee</td>
					<td width="2%" style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_name']."<br>".$arr_id_auditee ['auditee_alamat']."<br>";
						}
						?>
					</td>
				</tr>
				<tr>
					<td style="border: 0">b.</td>
					<td style="border: 0">No File Permanen</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_no_file'];
						}
						?>
					</td>
				</tr>
				<tr>
					<td style="border: 0">c.</td>
					<td style="border: 0">Rencana Audit Nomor</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
						<?=$arr_km06['km06_no_rencana']?>
					</td>
				</tr>
				<tr>
					<td style="border: 0">d.</td>
					<td style="border: 0">Audit Terakhir Tahun</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
						<?=$arr_km06['km06_tahun_terakhir']?>
					</td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;">2</td>
					<td colspan="2" style="border: 0">Tingkat Risiko Unit/Aktifitas</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
						<?=$comfunc->text_show($arr_km06['km06_resiko'])?>
					</td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;">3</td>
					<td colspan="2" style="border: 0">Tujuan Audit</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px"" colspan="2">
						<?=$comfunc->text_show($arr_km06['km06_tujuan'])?>
					</td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;">4</td>
					<td style="border: 0">a.</td>
					<td style="border: 0">Nama Pengendali Mutu</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
					<?
						$rs_katim = $assigns->anggota_list ( $assign_id, 'pm' );
						$arr_katim = $rs_katim->FetchRow();
						echo $arr_katim['auditor_name'];
					?>
					</td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;"></td>
					<td style="border: 0">b.</td>
					<td style="border: 0">Nama Pengendali Teknis</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
					<?
						$rs_katim = $assigns->anggota_list ( $assign_id, 'pt' );
						$arr_katim = $rs_katim->FetchRow();
						echo $arr_katim['auditor_name'];
					?>
					</td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;"></td>
					<td style="border: 0">c.</td>
					<td style="border: 0">Nama Ketua Tim</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
					<?
						$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
						$arr_katim = $rs_katim->FetchRow();
						echo $arr_katim['auditor_name'];
					?>
					</td>
				</tr>
				<?
					$no_anggota=0;
					$rs_anggota = $assigns->anggota_list ( $assign_id, 'at' );
					$count_anggota = $rs_anggota->RecordCount();
					while($arr_anggota = $rs_anggota->FetchRow()){
						$no_anggota++;
				?>
				<tr>
				<?
					if($no_anggota==1){
				?>
					<td rowspan="<?=$count_anggota?>" style="border-right: 1px;border-bottom: 0px;border-top:0px;"></td>
					<td style="border: 0" rowspan="<?=$count_anggota?>">d.</td>
					<td style="border: 0" rowspan="<?=$count_anggota?>">Nama Anggota Tim</td>
					<td style="border: 0" rowspan="<?=$count_anggota?>">:</td>
				<?
					}
				?>
					<!-- <td style="border: 0"><?=$no_anggota?></td> -->
					<td colspan="2" style="border-left: 1px;border-bottom: 0px;border-top:0px"><?=$no_anggota?>. <?=$arr_anggota['auditor_name']?></td>
				</tr>
				<?
					}
				?>
				<tr>
					<td rowspan="2" style="border-right: 1px;border-bottom: 0px;border-top:0px;">5</td>
					<td style="border: 0">a.</td>
					<td style="border: 0">Audit Dilakukan dengan Surat Tugas Nomor</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2"><?=$reports->get_surat_tugas_no($assign_id);?></td>
				</tr>
				<tr>
					<td style="border: 0">b.</td>
					<td style="border: 0">Audit Direncanakan Mulai Tanggal dan selesai Tanggal</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2"><?=$comfunc->dateIndo($arr['assign_start_date'])?> s/d <?=$comfunc->dateIndo($arr['assign_end_date'])?></td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;">6</td>
					<td colspan="2" style="border: 0">Anggaran yang Diajukan</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">Rp. <?=number_format($arr_km06['km06_anggaran_ajukan'])?></td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;">7</td>
					<td colspan="2" style="border: 0">Anggaran yang Disetujui</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">Rp. <?=number_format($arr_km06['km06_anggaran_setujui'])?></td>
				</tr>
				<tr>
					<td style="border-right: 1px;border-bottom: 0px;border-top:0px;">8</td>
					<td colspan="2" style="border: 0">Catatan Penting dari Pengendali Teknis/ pengendali Mutu</td>
					<td style="border: 0">:</td>
					<td style="border-left: 1px;border-bottom: 0px;border-top:0px" colspan="2">
					<?=$comfunc->text_show($arr_km06['km06_ket'])?>
					</td>
				</tr>
			</table>
			<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td width="50%" style="border-right: 1px; border-bottom: 0px; border-top: 0px;"  >&nbsp;</td>
					<td width="50%" style="border-left: 1px;border-bottom: 0px;border-top: 0px;">&nbsp;</td>
				</tr>
				<tr>
					<td style="border-right: 1px; border-bottom: 0px; border-top: 0px;"  >&nbsp;</td>
					<td align="center" style="border-left: 1px;border-bottom: 0px;border-top: 0px;">
						Jakarta, 
						<?=$comfunc->dateIndo($arr_km06['km06_tanggal'])?>
					</td>
				</tr>
				<tr>
					<td align="center" style="border-right: 1px; border-bottom: 0px; border-top: 0px;"  >Ketua Tim</td>
					<td align="center" style="border-left: 1px;border-bottom: 0px;border-top: 0px;">Mengetahui:<br>Pengendali Teknis</td>
				</tr>
				<tr>
					<td style="border-right: 1px; border-bottom: 0px; border-top: 0px;"  ><br><br><br><br></td>
					<td style="border-left: 1px;border-bottom: 0px;border-top: 0px;"><br><br><br><br></td>
				</tr>
				<tr>
					<td align="center" style="border-right: 1px; border-top: 1px;" ><?=$nama_katim;?></td>
					<td align="center" style="border-left: 1px;border-top: 1px;"><?=$nama_dalnis;?></td>
				</tr>
			</table>
	</body>
</html>