<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/program_audit_class.php";
include_once "_includes/classes/auditee_class.php";

$reports = new report ( $ses_userId );
$programaudits = new programaudit( $ses_userId );
$auditees = new auditee( $ses_userId );

$fil_tahun_id = "";
$fil_auditee_id = "";

$fil_tahun_id = $comfunc->replacetext ( $_POST ["fil_tahun_id"] );
$fil_auditee_id = $comfunc->replacetext ( $_POST ["fil_auditee_id"] );
$tahapan = $comfunc->replacetext($_POST['tahapan']);

$assign_id = $reports->get_assignment_id($fil_auditee_id, $fil_tahun_id);

$rs_auditee = $auditees->auditee_data_viewlist($fil_auditee_id);
$arr_auditee = $rs_auditee->FetchRow();

if($fil_tahun_id!="" & $fil_auditee_id!=""){
?>
<section id="main" class="column">	
	<article class="module width_3_quarter">
		<div style="overflow-x: auto; width: 100%;">
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<td colspan="7" align="center">KERTAS KERJA AUDIT</td>
			</tr>
			<tr>
				<td colspan="4" style="border-bottom: 0;border-right:0">Satuan Kerja</td>
				<td colspan="4" style="border-bottom: 0;border-left:0">: <?=$arr_auditee['auditee_name'];?></td>
			</tr>
			<tr>
				<td colspan="4" style="border-top: 0;border-bottom: 0;border-right:0">Tahun</td>
				<td colspan="4" style="border-top: 0;border-bottom: 0;border-left:0">: <?=$fil_tahun_id-1?>-<?=$fil_tahun_id?></td>
			</tr>
			<?php if ($tahapan != ""): ?>
			<tr>
				<td colspan="4" style="border-top: 0;border-bottom: 0;border-right:0">Tahapan Audit</td>
				<td colspan="4" style="border-top: 0;border-bottom: 0;border-left:0">: <?=strtoupper($tahapan)?></td>
			</tr>
			<?php endif ?>
			<tr align="center">
				<th width="2%">No</th>
				<th width="10%">Tahapan Audit</th>
				<th width="20%">Prosedur</th>
				<th width="8%">No KKA</th>
				<th width="20%">Uraian</th>
				<th width="35%">Kesimpulan</th>
				<th width="10%">Auditor</th>
			</tr>
			<?php
			$i=0;
			$rs_kka = $reports->kertas_kerja_list($assign_id, $fil_auditee_id, isset($tahapan) ? $tahapan : "");
			while($arr_kka = $rs_kka->FetchRow ()){
				$i++;
			?>
			<tr>
				<td><?=$i?></td>
				<td><?=$arr_kka['program_tahapan']?></td>
				<td><?=$comfunc->text_show($arr_kka['ref_program_procedure']) ?></td>
				<td><?=$arr_kka['kertas_kerja_no']?></td>
				<td><?=$comfunc->text_show($arr_kka['kertas_kerja_desc'])?></td>
				<td><?=$comfunc->text_show($arr_kka['kertas_kerja_kesimpulan'])?></td>
				<td><?=$arr_kka['auditor_name']?></td>
			</tr>
			<?
			}
			?>
		</table>
		</div>
		<br>
		<br>
		<fieldset>
			<center>
				<input type="button" class="blue_btn" value="ms-excel" onclick="window.open('ReportManagement/laporan_kka_excel.php?fil_tahun_id=<?= $fil_tahun_id ?>&fil_auditee_id=<?= $fil_auditee_id ?>&tahapan=<?= $tahapan ?>','toolbar=no, location=no, status=no, menubar=yes, scrollbars=yes, resizable=yes');">
			</center>
		</fieldset>
	</article>
</section>
<?
}else{
	$comfunc->js_alert_act ( 5 );
?>
	<script>window.open('main_page.php?method=laporan_kka_filter', '_self');</script>
<?
}
?>