<?php
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/program_audit_class.php";
include_once "_includes/classes/assignment_class.php";
$reports = new report ( $ses_userId );
$programaudits = new programaudit( $ses_userId );
$assigns = new assign ( $ses_userId );
$fil_tahun_id = "";
$fil_auditee_id = "";
$fil_tahun_id = $comfunc->replacetext ( $_POST ["fil_tahun_id"] );
$fil_auditee_id = $comfunc->replacetext ( $_POST ["fil_auditee_id"] );
$tahapan = $comfunc->replacetext($_POST['tahapan']);
$fil_aspek_rinci = $comfunc->replacetext($_POST['fil_aspek_rinci']);
$fil_aspek_pel = $comfunc->replacetext($_POST['fil_aspek_pel']);
if ($fil_aspek_pel) {
	$fil_aspek = $fil_aspek_pel;
} elseif ($fil_aspek_rinci) {
	$fil_aspek = $fil_aspek_rinci;
}
$assign_id = $reports->get_assignment_id($fil_auditee_id, $fil_tahun_id);
$intro = $programaudits->intro_pka_viewlist(isset($fil_aspek) ? $fil_aspek : "", $assign_id, $tahapan);
$intros = $intro->FetchRow();
$rs_assign = $assigns->assign_viewlist ( $assign_id );
$arr = $rs_assign->FetchRow();
$rs_auditee = $assigns->auditee_detil ( $fil_auditee_id );
$arr_auditee = $rs_auditee->FetchRow();
$katim = $reports->get_anggota($assign_id, $fil_auditee_id, 'KT');
$dalnis = $reports->get_anggota($assign_id, $fil_auditee_id, 'PT');
$dalmut = $reports->get_anggota($assign_id, $fil_auditee_id, 'PM');
if($fil_tahun_id!="" && $fil_auditee_id!=""){
?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<table border='0' class="table_report" cellspacing='0' cellpadding="0">
			<tr>
				<td colspan="20">
					<center>
					<img src="images/kka-logo1.png"  width="55" height="48" style="float:left;margin-left: 180px;width:55pt;height:48pt;" />
					<img src="images/kka-logo2.png"  width="55" height="48" style="float:right;margin-right: 180px;width:55pt;height:48pt;" />
					<strong>KEMENTERIAN PERHUBUNGAN REPUBLIK INDONESIA<br>INSPEKTORAT JENDERAL</strong>
					</center><br><br>
				</td>
			</tr>

			<tr>
				<td width="5%" style="border-top:1px solid">&nbsp;</td>
				<td width="5%" style="border-top:1px solid">&nbsp;</td>
				<td style="border-top:1px solid">&nbsp;</td>
				<td style="border-top:1px solid">&nbsp;</td>
				<td width="10%" style="border-top:1px solid">No PKA</td>
				<td align="center" style="border-top:1px solid">:</td>
				<td style="border-top:1px solid"><?= ($intros['pka_intro_no_pka']) ? $intros['pka_intro_no_pka'] : "Tidak ada" ?></td>
				<td style="border-top:1px solid;">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Nama Auditan</td>
				<td align="center">:</td>
				<td><?=$arr_auditee['auditee_name'];?></td>
				<td>Ref. PKA No</td>
				<td align="center">:</td>
				<td>-</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Sasaran Audit</td>
				<td align="center">:</td>
				<td><?=$arr['audit_type_name'];?></td>
				<td>Disusun Oleh</td>
				<td align="center">:</td>
				<td><?=$katim['nama']?></td>
				<td>Paraf :</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Tanggal</td>
				<td align="center">:</td>
				<td><?=$comfunc->dateIndoLengkap($intros['pka_intro_arranged_date'])?></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Periode Audit</td>
				<td align="center">:</td>
				<td>TA. <?=$fil_tahun_id-1?>-<?=$fil_tahun_id?></td>
				<td>Direviu Oleh</td>
				<td align="center">:</td>
				<td><?=$dalnis['nama']?></td>
				<td>Paraf :</td>
			</tr>
			<tr>
				<td colspan="2" style="border-bottom:1px solid;">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">Tanggal</td>
				<td style="border-bottom:1px solid" align="center">:</td>
				<td style="border-bottom:1px solid"><?=$comfunc->dateIndoLengkap($intros['pka_intro_reviewed_date'])?></td>
				<td style="border-bottom:1px solid;">&nbsp;</td>
			</tr>
			<tr>
				<?php
				switch ($tahapan) {
					case "PKA Pendahuluan":
						$tahapans = "PENDAHULUAN";
						break;
					case "PKA Pelaksanaan":
						$tahapans = "PELAKSANAAN";
						break;
					case "PKA Rinci":
						$tahapans = "RINCI";
						break;
				}
				?>
				<td colspan="8" align="center"><br><b>PROGRAM KERJA AUDIT <?= $tahapans ?></b></td>
			</tr>
			<tr>
				<td colspan="8"><strong>Tujuan Audit :</strong> <?=isset($intros['pka_intro_tujuan']) ?$intros['pka_intro_tujuan'] : "Tidak ada"?></td>
			</tr>
			<tr>
				<td colspan="2" style="border-bottom:1px solid;">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
				<td style="border-bottom:1px solid">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="8">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="8"><strong>A. Langkah Kerja :</strong></td>
			</tr>
		</table>
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr align="center">
				<th width="2%" rowspan="2">No.</th>
				<th width="50%" rowspan="2" colspan="2">Langkah Kerja Audit</th>
				<th width="16%" colspan="2">Dilaksanakan Oleh</th>
				<th width="16%" colspan="2">Waktu yang Diperlukan</th>
				<th width="9%" rowspan="2">Nomor KKA</th>
				<th width="7%" rowspan="2">Catatan</th>
			</tr>
			<tr align="center">
				<th>Rencana</th>
				<th>Realisasi</th>
				<th>Rencana</th>
				<th>Realisasi</th>
			</tr>
			<?php
			$no_aspek="A";
			$rs_aspek = $programaudits->get_assign_aspek(isset($fil_aspek) ? $fil_aspek : "",$assign_id, $fil_auditee_id, $tahapan);
			while($arr_aspek = $rs_aspek->FetchRow()){
			?>
			<tr>
				<td><b><?=$no_aspek?></b></td>
				<td colspan="2"><b><?=$arr_aspek['aspek_name']?></b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<?
			$nos=0;
			$rs_program = $reports->assign_program_audit_list($arr_aspek['aspek_id'], $assign_id, $fil_auditee_id, $tahapan);
			while($arr_program = $rs_program->FetchRow()){
			$nos++;
			?>
			<tr>
				<td><?= $nos ?></td>
				<td colspan="2"><?= $comfunc->text_show($arr_program['ref_program_procedure']) ?></td>
				<td><?= $arr_program['auditor_name'] ?></td>
				<td><?= $arr_program['auditor_name'] ?></td>
				<td align="center"><?= $arr_program['program_jam'] ?> Jam</td>
				<td align="center"><?=(null != $arr_program['kertas_kerja_jam']) ? $arr_program['kertas_kerja_jam']." Jam" : "" ?></td>
				<td><?= $arr_program['kertas_kerja_no'] ?></td>
				<td><div style="overflow-x: auto; width: 500px">
					<?= $comfunc->text_show($arr_program['kertas_kerja_desc']) ?>
				</div></td>
			</tr>
			<?
			}
			$no_aspek++;
			}
			?>
		</table>
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td style="border-style: none; border-bottom-style: none" colspan="4">&nbsp;&nbsp;<b>Keterangan : </b><?= $intros['pka_keterangan'] ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Jakarta, <?=$comfunc->dateIndoLengkap($intros['pka_intro_arranged_date'])?></td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td width="10%">&nbsp;</td><td width="30%">Disetujui</td><td width="30%">Direviu</td><td width="30%">Disusun oleh:</td>
			</tr>
			<tr>
				<td>&nbsp;</td><td>tanggal: <?=$comfunc->dateIndoLengkap($intros['pka_intro_approved_date'])?></td><td>tanggal: <?=$comfunc->dateIndoLengkap($intros['pka_intro_reviewed_date'])?></td><td>KETUA TIM,</td>
			</tr>
			<tr>
				<td></td><td>PENGENDALI MUTU,</td><td>PENGENDALI TEKNIS,</td><td></td>
			</tr>
			<?foreach (range(1,5) as $item): ?>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<?endforeach;?>
			<tr>
				<td></td><td><u><?=$dalmut['nama']?></u><br><?=$dalmut['jabatan']?></td><td><u><?=$dalnis['nama']?></u><br><?=$dalnis['jabatan']?></td><td><u><?=$katim['nama']?></u><br><?=$katim['jabatan']?></td>
			</tr>
		</table><br><br>
		<fieldset>
			<center>
				<?php
				switch ($tahapan) {
					case "PKA Pendahuluan":
						?>
							<input type="button" class="blue_btn" value="ms-word" onclick="window.open('AuditManagement/program_audit_pendahuluan_word.php?id_assign=<?= $assign_id ?>&id_auditee=<?= $fil_auditee_id ?>','toolbar=no, location=no, status=no, menubar=yes, scrollbars=yes, resizable=yes');">
						<?
						break;
					case "PKA Pelaksanaan":
						?>
							<input type="button" class="blue_btn" value="ms-word" onclick="window.open('AuditManagement/program_audit_pelaksanaan_word.php?id_assign=<?= $assign_id ?>&id_auditee=<?= $fil_auditee_id ?>&fil_aspek=<?= $fil_aspek ?>','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
						<?
						break;
					case "PKA Rinci":
						?>
							<input type="button" class="blue_btn" value="ms-word" onclick="window.open('AuditManagement/program_audit_rinci_word.php?id_assign=<?= $assign_id ?>&id_auditee=<?= $fil_auditee_id ?>&fil_aspek=<?= $fil_aspek ?>','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes');">
						<?
						break;
				}
				?>
			</center>
		</fieldset>
	</article>
</section>
<?
}else{
$comfunc->js_alert_act ( 5 );
?>
<script>window.open('main_page.php?method=laporan_program_audit_filter', '_self');</script>
<?
}
?>