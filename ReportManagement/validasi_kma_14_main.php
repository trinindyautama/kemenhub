<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = "main_page.php?method=validate_kma_14&fil_id=".$assign_id;

switch ($_action) {
	case "postadd" :
		for($i=1;$i<=17;$i++){	
			$fid = $comfunc->replacetext ( $_POST ["id_progres_".$i] );
			$fst = ($comfunc->replacetext ( $_POST ["st_".$i] ) == TRUE) ? "Sudah" : "Belum";
			$fprogres = $comfunc->replacetext ( $_POST ["progres_".$i] );
			$fket = $comfunc->replacetext ( $_POST ["ket_".$i] );
			if($fst!="") {
				if($fid=="") $reports->insert_km_14($assign_id, $i, $fst, $fprogres, $fket, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_14($fid, $fst, $fprogres, $fket, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}
		}
		for($x=1;$x<=3;$x++){	
			$fid_1 = $comfunc->replacetext ( $_POST ["id_progres_18_".$x] );
			$fst_1 = ($comfunc->replacetext ( $_POST ["st_18_".$x] ) == TRUE) ? "Sudah" : "Belum";
			$fprogres_1 = $comfunc->replacetext ( $_POST ["progres_18_".$x] );
			$fket_1 = $comfunc->replacetext ( $_POST ["ket_18_".$x] );
			if($fst_1!="") {
				if($fid_1=="") $reports->insert_km_14($assign_id, "18_".$x, $fst_1, $fprogres_1, $fket_1, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_14($fid_1, $fst_1, $fprogres_1, $fket_1, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}	
			
			$fid_2 = $comfunc->replacetext ( $_POST ["id_progres_19_".$x] );
			$fst_2 = ($comfunc->replacetext ( $_POST ["st_19_".$x] ) == TRUE) ? "Sudah" : "Belum";
			$fprogres_2 = $comfunc->replacetext ( $_POST ["progres_19_".$x] );
			$fket_2 = $comfunc->replacetext ( $_POST ["ket_19_".$x] );
			if($fst_2!="") {
				if($fid_2=="") $reports->insert_km_14($assign_id, "19_".$x, $fst_2, $fprogres_2, $fket_2, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_14($fid_2, $fst_2, $fprogres_2, $fket_2, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}	
			
			$fid_3 = $comfunc->replacetext ( $_POST ["id_progres_20_".$x] );
			$fst_3 = ($comfunc->replacetext ( $_POST ["st_20_".$x] ) == TRUE) ? "Sudah" : "Belum";
			$fprogres_3 = $comfunc->replacetext ( $_POST ["progres_20_".$x] );
			$fket_3 = $comfunc->replacetext ( $_POST ["ket_20_".$x] );
			if($fst_3!="") {
				if($fid_3=="") $reports->insert_km_14($assign_id, "20_".$x, $fst_3, $fprogres_3, $fket_3, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_14($fid_3, $fst_3, $fprogres_3, $fket_3, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}	
			
			$fid_4 = $comfunc->replacetext ( $_POST ["id_progres_21_".$x] );
			$fst_4 = ($comfunc->replacetext ( $_POST ["st_21_".$x] ) == TRUE) ? "Sudah" : "Belum";
			$fprogres_4 = $comfunc->replacetext ( $_POST ["progres_21_".$x] );
			$fket_4 = $comfunc->replacetext ( $_POST ["ket_21_".$x] );
			if($fst_4!="") {
				if($fid_4=="") $reports->insert_km_14($assign_id, "21_".$x, $fst_4, $fprogres_4, $fket_4, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_14($fid_4, $fst_4, $fprogres_4, $fket_4, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}
						
			$fid_6 = $comfunc->replacetext ( $_POST ["id_progres_14_".$x] );
			$ftanggal_6 = $comfunc->date_db($comfunc->replacetext ( $_POST ["tanggal_14_".$x] ));
			if($fid_6=="" && $ftanggal_6!="") 
				$reports->insert_km_14($assign_id, "14_".$x, $ftanggal_6, "", "", $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			elseif($fid_6!="" && $ftanggal_6=="") 
				$reports->delete_km_14($fid_6);
			else 
				$reports->update_km_14($fid_6, $ftanggal_6, "", "", $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
		}
		
		for($z=1;$z<=4;$z++){
			$fid_5 = $comfunc->replacetext ( $_POST ["id_progres_8_".$z] );
			$ftanggal_5 = $comfunc->date_db($comfunc->replacetext ( $_POST ["tanggal_8_".$z] ));
			if($fid_5=="" && $ftanggal_5!="")
				$reports->insert_km_14($assign_id, "8_".$z, $ftanggal_5, "", "", $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));			
			elseif($fid_5!="" && $ftanggal_5=="")
				$reports->delete_km_14($fid_5);			
			else 
				$reports->update_km_14($fid_5, $ftanggal_5, "", "", $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
		}
		$comfunc->js_alert_act ( 3 );
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	default :
		$_nextaction = "postadd";
		$page_request = "validasi_kma_14_acc.php";
		break;
}
include_once $page_request;
?>