<link rel="stylesheet" href="js/select2/select2.css" type="text/css"/>
<script type="text/javascript" src="js/select2/select2.min.js"></script>
<?
include_once "_includes/classes/assignment_class.php";

$assigns = new assign ( $ses_userId );
?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Filter Rekapitulasi Temuan Hasil Audit</h3>
		</header>
		<form method="post" name="f" action="main_page.php?method=laporan_rekap_temuan_hasil_audit" class="form-horizontal" id="validation-form">
			<fieldset class="hr">
				<label class="span2">Tahun</label>
				<?php
				$rs_tahun = $assigns->assign_tahun_viewlist();
				$arr_tahun = $rs_tahun->GetArray ();
				echo $comfunc->buildCombo ( "fil_tahun_id", $arr_tahun, 0, 0, "", "", "", false, false, true );
				?>
			</fieldset>
			<fieldset>
				<center>
					<input type="submit" class="blue_btn" value="Lihat">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>
$("#fil_tahun_id").select2({allowClear: true, placeholder: "Pilih Satu", width: "300px"})
</script>