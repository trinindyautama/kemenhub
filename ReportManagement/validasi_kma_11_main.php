<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

$rs_km_11 = $reports->get_km_11($assign_id);
$arr_km_11 = $rs_km_11->FetchRow();

$rs_km_6 = $reports->km06_list($assign_id);
$arr_km_6 = $rs_km_6->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$get_daltu = $arr_daltu['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = "main_page.php?method=validate_kma_11&fil_id=".$assign_id;

switch ($_action) {
	case "postadd" :
		$fid = $comfunc->replacetext ( $_POST ["data_id"] );
		$fhari = $comfunc->replacetext ( $_POST ["hari"] );
		$ftanggal = $comfunc->date_db($comfunc->replacetext($_POST ["tanggal"]));
		$fwaktu = $comfunc->replacetext ( $_POST ["waktu"] );
		$tim_auditee = array();
		foreach (range(0,4) as $value) {
			$tim_auditee[] = $_POST['auditee_'.$value];
		}
		$ftim_auditee = implode(';', $tim_auditee);
		$fttd_auditee = $comfunc->replacetext ( $_POST ["ttd_auditee"] );
		$fsurvei_prosedur = $comfunc->replacetext ( $_POST ["survei_prosedur"] );
		$fsurvei_pendahuluan = $comfunc->date_db($comfunc->replacetext($_POST ["survei_pendahuluan"]));
		$fpenyelesaian_awal = $comfunc->date_db($comfunc->replacetext($_POST ["penyelesaian_awal"]));
		$fpenyelesaian_akhir = $comfunc->date_db($comfunc->replacetext($_POST ["penyelesaian_akhir"]));
		$fpic_auditee = $comfunc->replacetext ( $_POST ["pic_auditee"] );
		$fket_tambahan = $comfunc->replacetext ( $_POST ["ket_tambahan"] );
		
		if($fid=="") $reports->insert_km_11($assign_id, $fhari, $ftanggal, $fwaktu, $ftim_auditee, $fttd_auditee, $fsurvei_prosedur, $fsurvei_pendahuluan, $fpenyelesaian_awal, $fpenyelesaian_akhir, $fpic_auditee, $fket_tambahan, $comfunc->date_db($_POST['tanggal_kartu']), $_POST['created_by']);
		else $reports->update_km_11($fid, $fhari, $ftanggal, $fwaktu, $ftim_auditee, $fttd_auditee, $fsurvei_prosedur, $fsurvei_pendahuluan, $fpenyelesaian_awal, $fpenyelesaian_akhir, $fpic_auditee, $fket_tambahan, $comfunc->date_db($_POST['tanggal_kartu']), $_POST['created_by']);
		
		$comfunc->js_alert_act ( 3 );
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	default :
		$_nextaction = "postadd";
		$page_request = "validasi_kma_11_acc.php";
		break;
}
include_once $page_request;
?>