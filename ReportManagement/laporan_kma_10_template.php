<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	<header>Formulir KMA 10</header>
	<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="5" align="center"><font size="1">CHECKLIST</font><br>PENYELESAIAN PENUGASAN PERENCANAAN AUDIT</td>
		</tr>
		<tr>
			<th width="5%" align="center">No</th>
			<th width="75%" align="center" colspan="2">Jenis pekerjaan yang harus dilakukan</th>
			<th width="10%" align="center">Sudah/belum</th>
			<th width="10%" align="center">	% penyelesaian</th>
		</tr>
		<tr>
			<td align="center">1</td>
			<td align="center" colspan="2">2</td>
			<td align="center">3</td>
			<td align="center">4</td>
		</tr>
		<tr>
			<td align="center">1.</td>
			<td colspan="2">Sudahkah dibuat Kartu Penugasan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '1') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '1')?></td>
		</tr>
		<tr>
			<td align="center">2.</td>
			<td colspan="2">Sudahkan dikembangkan Tujuan Evaluasi, Lingkup Pekerjaan, Penaksiran Risiko Segmen Kegiatan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '2') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '2')?></td>
		</tr>
		<tr>
			<td align="center">3.</td>
			<td colspan="2">Apakah sudah diperoleh:</td>
			<td align="center"></td>
			<td align="center"></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center" style="width:5px !important">o</td>
			<td>Misi, tujuan dan rencana pelaksanaan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_1') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Informasi organisasi</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_2') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_2')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>KKA terakhir</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_3') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_3')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>File permanen</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_4') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_4')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>LHP auditor ekstern</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_5') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_5')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Data pembanding</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_6') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_6')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Anggaran</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_7') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_7')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Literatur teknis</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '3_8') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '3_8')?></td>
		</tr>
		<tr>
			<td align="center">4.</td>
			<td colspan="2">Jika ada perubahan apakah sudah dibuat memo persetujuan dan sudah dilampirkan ke kartu penugasan di Pengendali Mutu</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '4') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '4')?></td>
		</tr>
		<tr>
			<td align="center">5.</td>
			<td colspan="2">Adakah perubahan auditor dari rencana semula</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '5') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '5')?></td>
		</tr>
		<tr>
			<td align="center">6.</td>
			<td colspan="2">Apakah sudah dibuat rapat koordinasi</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '6') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '6')?></td>
		</tr>
		<tr>
			<td align="center">7.</td>
			<td colspan="2">Apakah sudah dibuat ringkasannya dan telah didistribusikan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '7') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '7')?></td>
		</tr>
		<tr>
			<td align="center">8.</td>
			<td colspan="2">Apakah sudah dibuat persiapan survei pendahuluan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '8') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '8')?></td>
		</tr>
		<tr>
			<td align="center">9.</td>
			<td colspan="2">Apakah survei pendahuluan telah dilaksanakan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '9') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '9')?></td>
		</tr>
		<tr>
			<td align="center">10.</td>
			<td colspan="2">Apakah telah dibuat ikhtisar hasil survei</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '10') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '10')?></td>
		</tr>
		<tr>
			<td align="center">11.</td>
			<td colspan="2">Apakah telah ditulis program evaluasi</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '11') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '11')?></td>
		</tr>
		<tr>
			<td align="center">12.</td>
			<td colspan="2">Apakah program evaluasi telah mengacu pada program baku dan hasil pengumpulan informasi</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '12') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '12')?></td>
		</tr>
		<tr>
			<td align="center">13.</td>
			<td colspan="2">Apakah program evaluasi telah mendapat persetujuan Pengendali Teknis</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '13') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '13')?></td>
		</tr>
		<tr>
			<td align="center">14.</td>
			<td colspan="2">Apakah tahapan pekerjaan telah sesuai dengan anggaran waktunya</td>
			<td align="center"></td>
			<td align="center"></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Penetapan tujuan, lingkup dan penaksiran resiko</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_1') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Pengumpulan informasi awal</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_2') == 1) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_2')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Penetapan staf audit</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_3') == 1 ) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_3')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Rapat pendahuluan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_4') == 1 ) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_4')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Survei pendahuluan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_5') == 1 ) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_5')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Penulisan program audit</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_6') == 1 ) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_6')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td align="center">o</td>
			<td>Persetujuan program audit</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '14_7') == 1 ) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '14_7')?></td>
		</tr>
		<tr>
			<td align="center">15.</td>
			<td colspan="2">Apakah kertas kerja evaluasi telah selesai dikerjakan</td>
			<td align="center"><?=($reports->get_data_st_km10 ( $assign_id, '15') == 1 ) ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_persen_km10 ( $assign_id, '15')?></td>
		</tr>
		<tr>
			<td colspan="3" align="center">Diketahui,<br>Pengendali Mutu<br><br><br><br><?=$get_daltu?></td>
			<td colspan="2" align="center"><?=$comfunc->dateIndo($reports->get_data_km10($assign_id, 'km10_date', ''))?>,<br>Pengendali Teknis<br><br><br><br><?=$get_dalnis?></td>
		</tr>
	</table>
</body>
</html>