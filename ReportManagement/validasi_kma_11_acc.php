<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">
				Formulir KMA 11
			</h3>	
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="7" align="center" style="border:0px">NOTULENSI KESEPAKATAN</td>
				</tr>
				<tr>
					<td colspan="7" style="border:0px">
						Berdasarkan hasil rapat koordinasi antara tim audit dengan auditi (
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
						$assign_id_auditee = "";
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_name'].",";
						}
						?>) Pada:
					</td>
				</tr>
				<tr>
					<td width="5%" style="border:0px">&nbsp;</td>
					<td width="5%" style="border:0px">&nbsp;</td>
					<td width="10%" style="border:0px">Hari</td>
					<td width="5%" style="border:0px">:</td>
					<td colspan="3" style="border:0px"><input type="text" class="cmb_risk" name="hari" size="10" value="<?=$arr_km_11['km11_hari']?>"></td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td style="border:0px">&nbsp;</td>
					<td style="border:0px">Tanggal</td>
					<td style="border:0px">:</td>
					<td colspan="3" style="border:0px"><input type="text" class="cmb_risk" name="tanggal" id="tanggal" size="10" value="<?=$comfunc->dateIndo($arr_km_11['km11_tanggal'])?>"></td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td style="border:0px">&nbsp;</td>
					<td style="border:0px">Waktu</td>
					<td style="border:0px">:</td>
					<td colspan="3" style="border:0px"><input type="text" class="cmb_risk" name="waktu" size="5" value="<?=$arr_km_11['km11_waktu']?>"></td>
				</tr>
				<tr>
					<td colspan="7" style="border:0px">Dihadiri Oleh:</td>
				</tr>
				<tr>
					<td colspan="4" style="border:0px">Tim Auditee</td><td colspan="3" style="border:0px">Tim Auditor</td>
				</tr>
				<tr>
				<td style="border:0px" colspan="4">
					<?php $tim_auditee = explode(';', $arr_km_11['km11_tim_auditee']) ?>
					<?php foreach (range(0,4) as $value): ?>
						<?= $value+1 ?>. <input type="text" name="auditee_<?=$value?>" value="<?= $tim_auditee[$value] ?>" class="span8"><br><br>
					<?php endforeach ?>
				</td>
				<td style="border:0px" colspan="3">
					<?
					$i=0;
					$rs_auditor = $assigns->anggota_list($assign_id, "", true);
					while($arr_auditor = $rs_auditor->FetchRow()){
						$i++;
					?>
					
						<?= $i ?>. <?=$arr_auditor['auditor_name']?><br><br>
					
					<?
					}
					?>
				</td>
				</tr>
				<tr>
					<td colspan="7" style="border:0px">&nbsp;</td>
				</tr>
			</table>
			<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="7" style="border:0px">Diperoleh kesepakatan sebagai berikut:</td>
				</tr>
				<tr>
					<td align="center" style="border:0px">1</td>
					<td colspan="6" style="border:0px">Tujuan Audit : <br>
						<?= $comfunc->text_show($arr_km_6['km06_tujuan']) ?>
					</td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td colspan="6" style="border:0px">Prosedur audit yang akan dilaksanakan adalah sebagai berikut:</td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td colspan="5" style="border:0px">
						<textarea class="ckeditor" cols="4" rows="40" name="survei_prosedur" id="survei_prosedur"><?=$arr_km_11['km11_prosedur']?></textarea>
					</td>
				</tr>
				<tr>
					<td align="center" style="border:0px">2</td>
					<td colspan="6" style="border:0px">Waktu Pelaksanaan Audit</td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td align="center" style="border:0px">&diams;</td>
					<td colspan="2" style="border:0px">Survei Pendahuluan</td>
					<td colspan="3" style="border:0px">: <input type="text" class="cmb_risk" name="survei_pendahuluan" id="survei_pendahuluan" size="10" value="<?=$comfunc->dateIndo($arr_km_11['km11_survei'])?>"></td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td align="center" style="border:0px">&diams;</td>
					<td colspan="2" style="border:0px">Pelaksanaan audit</td>
					<td colspan="3" style="border:0px">: <?=$comfunc->dateIndo($arr['assign_start_date'])." s.d ".$comfunc->dateIndo($arr['assign_end_date'])?></td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td align="center" style="border:0px">&diams;</td>
					<td colspan="2" style="border:0px">Penyelesaian laporan</td>
					<td colspan="3" style="border:0px">: <input type="text" class="cmb_risk" name="penyelesaian_awal" id="penyelesaian_awal" size="10" value="<?=$comfunc->dateIndo($arr_km_11['km11_penyelesaian_awal'])?>"> s.d <input type="text" class="cmb_risk" name="penyelesaian_akhir" id="penyelesaian_akhir" size="10" value="<?=$comfunc->dateIndo($arr_km_11['km11_penyelesaian_akhir'])?>"></td>
				</tr>
				<tr>
					<td align="center" style="border:0px">3</td>
					<td colspan="6" style="border:0px">Tim Audit yang ditugaskan</td>
				</tr>
				<?
				$i=0;
				$rs_auditor = $assigns->anggota_list($assign_id);
				while($arr_auditor = $rs_auditor->FetchRow()){
					$i++;
				?>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td align="center" style="border:0px">&diams;</td>
					<td colspan="2" style="border:0px"><?=$arr_auditor['posisi_name']?></td>
					<td colspan="3" style="border:0px">: <?=$arr_auditor['auditor_name']?></td>
				</tr>
				<?
				}
				?>
				<tr>
					<td align="center" style="border:0px">4</td>
					<td colspan="6" style="border:0px">Dalam pelaksanaan audit, yang akan menjadi kontak person adalah <input type="text" class="cmb_risk" name="pic_auditee" size="50" value="<?=$arr_km_11['km11_pic']?>">.  Pelaksanaan audit akan dilakukan terhadap lingkungan Pengendalian dan aktifitas pengendalian.</td>
				</tr>
				<tr>
					<td align="center" style="border:0px">5</td>
					<td colspan="6" style="border:0px">
						<textarea class="ckeditor" cols="4" rows="40" name="ket_tambahan" id="survei_prosedur"><?=$arr_km_11['km11_ket_tambahan']?></textarea>
				</tr>
				<tr>
					<td align="center" style="border:0px">6</td>
					<td colspan="6" style="border:0px">Prosedur pelaporan dan tindak lanjut akan mengacu pada standar audit APIP dan tindakan koreksi terhadap rekomendasi temuan audit paling lambat akan dilakukan dalam waktu 60 hari setelah tanggal kesepakatan ditetapkan.</td>
				</tr>
				<tr>
					<td align="center" style="border:0px">7</td>
					<td colspan="6" style="border:0px">Seluruh biaya yang terjadi selama audit ditanggung oleh kantor tim audit.</td>
				</tr>
				<tr>
					<td colspan="7" style="border:0px">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" colspan="5" style="border:0px">&nbsp;<br>Perwakilan Auditi<br><br><br><br>(<input type="text" class="cmb_risk" name="ttd_auditee" size="15" value="<?=$arr_km_11['km11_ttd_auditee']?>">)</td>
					<td align="center" colspan="2" style="border:0px"><input type="text" size="10" name="tanggal_kartu" id="tanggal_kartu" value="<?= $comfunc->dateIndo($arr_km_11['km11_date']) ?>"><br>Perwakilan Auditor<br><br><br><br>(<select name="created_by">
							<option selected="selected" value=""></option>
							<?php foreach ($reports->getPosPenugasanKatimAtim(array('1fe7f8b8d0d94d54685cbf6c2483308aebe96009'), 'NOT IN') as $pos): ?>
							<optgroup label="<?= $pos['posisi_name'] ?>">
								<?php foreach ($reports->getNamaKatimAtimByAssignPos($assign_id, $pos['posisi_id']) as $auditor): ?>
								<option value="<?= $auditor['auditor_id'] ?>"
									<?= $arr_km_11['km11_auditor'] == $auditor['auditor_id'] ? "selected" : "" ?>
									>
									<?= $auditor['auditor_name'] ?>
								</option>
								<?php endforeach ?>
							</optgroup>
							<?php endforeach ?>
						</select>)</td>
				</tr>
			</table>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="submit" class="blue_btn" value="Simpan">&nbsp;&nbsp;&nbsp;
					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<input type="hidden" name="data_id" value="<?=$arr_km_11['km11_id']?>">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>	
$("#tanggal, #survei_pendahuluan, #penyelesaian_awal, #penyelesaian_akhir, #tanggal_kartu").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true,
	 minDate: '<?= $comfunc->dateIndo($arr['assign_start_date']) ?>',
	 maxDate: '<?= $comfunc->dateIndo($arr['assign_end_date']) ?>',
});  
function download(idAssign){
	window.open("ReportManagement/laporan_kma_11_pdf.php?fil_id="+idAssign); 
}
</script>