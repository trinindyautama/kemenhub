<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = "main_page.php?method=validate_kma_17&fil_id=".$assign_id;

switch ($_action) {
	case "postadd" :
		for($i=1;$i<=21;$i++){	
			$fid = $comfunc->replacetext ( $_POST ["id_progres_".$i] );
			$fst = ($_POST ["st_".$i] == true) ? "Sudah" : "Belum";
			$fket = $comfunc->replacetext ( $_POST ["ket_".$i] );
			if($fst!="") {
				if($fid=="") $reports->insert_km_17($assign_id, $i, $fst, $fket, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_17($fid, $fst, $fket, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}else{
				$reports->delete_km_17($fid);
			}
		}
		for($x=1;$x<=2;$x++){	
			$fid_1 = $comfunc->replacetext ( $_POST ["id_progres_22_".$x] );
			$fst_1 = ($_POST ["st_22_".$x] == true) ? "Sudah" : "Belum";
			$fket_1 = $comfunc->replacetext ( $_POST ["ket_22_".$x] );
			if($fst_1!="") {
				if($fid_1=="") $reports->insert_km_17($assign_id, "22_".$x, $fst_1, $fket_1, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
				else $reports->update_km_17($fid_1, $fst_1, $fket_1, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			}else{
				$reports->delete_km_17($fid_1);
			}
		}
			
		$fid_2 = $comfunc->replacetext ( $_POST ["id_progres_23"] );
		$fst_2 = ($_POST ["st_23"] == true) ? "Sudah" : "Belum";
		$fket_2 = $comfunc->replacetext ( $_POST ["ket_23"] );
		if($fst_2!="") {
			if($fid_2=="") $reports->insert_km_17($assign_id, "23", $fst_2, $fket_2, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
			else $reports->update_km_17($fid_2, $fst_2, $fket_2, $comfunc->date_db($_POST['tanggal_dalnis']), $comfunc->date_db($_POST['tanggal_katim']));
		}else{
			$reports->delete_km_17($fid_2);
		}
		$comfunc->js_alert_act ( 3 );
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	default :
		$_nextaction = "postadd";
		$page_request = "validasi_kma_17_acc.php";
		break;
}
include_once $page_request;
?>