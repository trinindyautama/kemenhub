<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span2">Halaman LHA</label> 
				<input type="text" class="span7" name="lha" id="lha">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Masalah yg dijumpai</label> 
				<input type="text" class="span7" name="masalah" id="masalah">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No KKA</label> 
				<?				
				$rs_kka = $reports->kertas_kerja_list($assign_id, '', '');
				$arr_kka = $rs_kka->GetArray ();
				echo $comfunc->buildCombo ( "kka_id", $arr_kka, 0, 'lengkap', "", "", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Penyelesaian Masalah</label> 
				<input type="text" class="span7" name="jawaban">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Dilakukan Oleh</label> 
				
				<?				
				$rs_team = $assigns->anggota_list_kma($assign_id);
				$arr_team = $rs_team->GetArray ();
				echo $comfunc->buildCombo ( "auditor_id", $arr_team, 0, 1, "", "", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Ket</label> 
				<input type="text" class="span7" name="ket">
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span2">Halaman LHA</label> 
				<input type="text" class="span7" name="lha" id="lha" value="<?=$arr['km16_lha']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Masalah yg dijumpai</label> 
				<input type="text" class="span7" name="masalah" id="masalah" value="<?=$arr['km16_masalah']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">No KKA</label> 
				<?				
				$rs_kka = $reports->kertas_kerja_list($assign_id);
				$arr_kka = $rs_kka->GetArray ();
				echo $comfunc->buildCombo ( "kka_id", $arr_kka, 0, 'lengkap', $arr['km16_id_kka'], "", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Penyelesaian Masalah</label> 
				 <input type="text" class="span7" name="jawaban" value="<?=$arr['km16_komen']?>">
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Dilakukan Oleh</label> 
				
				<?				
				$rs_team = $assigns->anggota_list($assign_id);
				$arr_team = $rs_team->GetArray ();
				echo $comfunc->buildCombo ( "auditor_id", $arr_team, 0, 1, $arr['km16_id_user_to'], "", "", false, true, false );
				?>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Ket</label> 
				 <input type="text" class="span7" name="ket" value="<?=$arr['km16_ket']?>">
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['km16_id']?>">	
		<?
				break;
			case "getajukan_km" :
			case "getapprove_km" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<table class="view_parrent">
					<tr>
						<td>Halaman LHA</td>
						<td>:</td>
						<td><?=$arr['km16_lha']?></td>
					</tr>
					<tr>
						<td>Masalah yg dijumpai</td>
						<td>:</td>
						<td><?=$arr['km16_masalah']?></td>
					</tr>
					<tr>
						<td>No KKA</td>
						<td>:</td>
						<td><?=$arr['kertas_kerja_no']?></td>
					</tr>
					<tr>
						<td>Penyelesaian Masalah</td>
						<td>:</td>
						<td><?=$arr['km16_komen']?></td>
					</tr>
					<tr>
						<td>Dilakukan Oleh</td>
						<td>:</td>
						<td><?=$arr['auditor_name']?></td>
					</tr>
					<tr>
						<td>Ket</td>
						<td>:</td>
						<td><?=$arr['km16_ket']?></td>
					</tr>
					<tr>
						<td>Detail Komentar</td>
						<td>:</td>
						<td>
						<?php 
						$z = 0;
						$rs_komentar = $reports->km16_komentar_viewlist ( $arr ['km16_id'] );
						while ( $arr_komentar = $rs_komentar->FetchRow () ) {
							$z ++;
							echo $z.". ".$arr_komentar['auditor_name']." : ".$arr_komentar['km16_comment_desc']." (".$comfunc->dateIndo($arr_komentar['km16_comment_date']).")<br>";
						}
						?></td>
					</tr>
				</table>				
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Isi Komentar</label>
				<textarea id="komentar" name="komentar" rows="1" cols="20" style="width: 475px; height: 3em; font-size: 11px;"></textarea>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['km16_id']?>">	
			<input type="hidden" name="status" value="<?=$status?>">
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> 
					&nbsp;&nbsp;&nbsp; 
					<?
					if(@$status==1){
					?>
						<input type="submit" class="blue_btn" value="Ajukan">
					<?
					}elseif(@$status==2){
					?>
						<input type="submit" class="blue_btn" value="Setujui">
					<?
					}elseif(@$status==3){
					?>
						<input type="submit" class="blue_btn" value="Tolak">
					<?
					}else{
					?>
						<input type="submit" class="blue_btn" value="Simpan">
					<?
					}
					?>
				</center>
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script>  
$(function() {
	$("#validation-form").validate({
		rules: {
			masalah: "required"
		},
		messages: {
			masalah: "Silahkan masukan Pemasalahan"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>