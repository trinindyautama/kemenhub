<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal"
			id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span1">Indeks KKA</label> 
				<?				
				$rs_kka = $reports->kertas_kerja_list($assign_id, '', '');
				$arr_kka = $rs_kka->GetArray ();
				echo $comfunc->buildCombo ( "kka_id", $arr_kka, 0, 'lengkap', "", "", "", false, true, false );
				?>
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span1">Indeks KKA</label> 
				<?				
				$rs_kka = $reports->kertas_kerja_list($assign_id, '', '');
				$arr_kka = $rs_kka->GetArray ();
				echo $comfunc->buildCombo ( "kka_id", $arr_kka, 0, 'lengkap', $arr['km12_id_kka'], "", "", false, true, false );
				?>
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['km12_id']?>">	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> 
					&nbsp;&nbsp;&nbsp; 
					<input type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script>  
$(function() {
	$("#validation-form").validate({
		rules: {
			kka_id: "required"
		},
		messages: {
			kka_id: "Silahkan Pilih No KKA"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>