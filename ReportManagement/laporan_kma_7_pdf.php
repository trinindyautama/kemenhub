<?php
set_time_limit(0);
ini_set('memory_limit', '1024M');

include_once "../_includes/classes/param_class.php"; 
require '../_includes/html2pdf/vendor/autoload.php';
define('DOMPDF_ENABLE_AUTOLOAD', false);
define("DOMPDF_ENABLE_FONTSUBSETTING", true);
define("DOMPDF_UNICODE_ENABLED", true);
 
require_once '../_includes/html2pdf/vendor/dompdf/dompdf/dompdf_config.inc.php';
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/assignment_class.php";
include_once "../_includes/common.php";

$comfunc = new comfunction();
$reports = new report (  );
$assigns = new assign(  );

$assign_id = "";

$array = array(
    'PEKERJAAN-PEKERJAAN PERSIAPAN' => array(
        'Pembicaraan pendahuluan (koordinasi)',
        'Survei pendahuluan (dokumen, peraturan terkait)',
        'Penyusunan Program Audit',
    ),
    'PELAKSANAAN AUDIT'             => array(
        'Pengukuran Kinerja',
        'Analisis Kinerja',
        'Mereviu KKA',
        'Penyusunan Ikhtisar Temuan Hasil Audit',
    ),
    'PENYELESAIAN PEKERJAAN'        => array(
        'Meneliti kelengkapan KKA',
        'Pembahasan Ketua Tim, Pengendali Teknis, dan Pengendali Mutu',
        'Penyusunan Matriks Temuan Hasil Audit dan Konsep Laporan Hasil Audit',
    ),
);

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr_assign = $rs->FetchRow();
	
$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$htmlString = '';
ob_start();
include('laporan_kma_7_template.php');
$htmlString .= ob_get_clean();

$dompdf = new DOMPDF();
$dompdf->load_html($htmlString);
$dompdf->set_paper('a4','landscape');
$dompdf->render();
$dompdf->stream("Formulir KMA 7.pdf");
?>