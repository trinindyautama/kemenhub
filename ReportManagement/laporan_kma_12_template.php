<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	<header>Formulir KMA 12</header>
	<h4 align="center">LEMBAR REVIU SUPERVISI</h4>
	<table border='0' class="table_zero">
		<tr>
			<!-- <?php foreach (range(1,1) as $value): ?> -->
				<!-- <td>&nbsp;</td> -->
			<!-- <?php endforeach ?> -->
			<td width="15%">Nama Auditi</td>
			<td width="2%" align="center">:</td>
			<td>
			<?
			$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
			$assign_id_auditee = "";
			while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
				echo $arr_id_auditee ['auditee_name']."<br>";
			}
			?>
			</td>
		</tr>
		<tr>
			<!-- <?php foreach (range(1,1) as $value): ?> -->
				<!-- <td>&nbsp;</td> -->
			<!-- <?php endforeach ?> -->
			<td>No. Surat Tugas</td>
			<td>:</td>
			<td><?=$reports->get_surat_tugas_no($assign_id);?></td>
		</tr>
		<tr>
			<!-- <?php foreach (range(1,1) as $value): ?> -->
				<!-- <td>&nbsp;</td> -->
			<!-- <?php endforeach ?> -->
			<td>Periode Audit</td>
			<td>:</td>
			<td><?=$arr_assign['assign_periode']?></td>
		</tr>
		<tr>
			<!-- <?php foreach (range(1,1) as $value): ?> -->
				<!-- <td>&nbsp;</td> -->
			<!-- <?php endforeach ?> -->
			<td>Ketua Tim</td>
			<td>:</td>
			<td><?=$get_katim?></td>
		</tr>
	</table>
	<br>
	<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
		<tr>
			<th width="5%">No</th>
			<th>Permasalahan/komentar</th>
			<th width="15%">Indeks KKA</th>
			<th>Penyelesaian</th>
			<th width="10%">Persetujuan</th>
		</tr>
		<tr>
			<td align="center">1</td>
			<td align="center">2</td>
			<td align="center">3</td>
			<td align="center">4</td>
			<td align="center">5</td>
		</tr>
		<?	
		$no=0;
		$rs_data = $reports->km12_data_viewlist("", $assign_id);
		while($arr_data = $rs_data->FetchRow()){
			$no++;
		?>
		<tr>
			<td align="center"><?=$no?></td>
			<td>
				<?php 
					$rs_komentar = $reports->km12_program_komentar($arr_data['kertas_kerja_id_program'], "Ketua Tim");
					foreach ($rs_komentar as $key => $komentar) {
						echo $komentar['auditor_name'].' : '.$komentar['program_comment_desc'].'<br>';
					}
				 ?>
			</td>
			<td align="center"><?=$arr_data['kertas_kerja_no']?></td>
			<td>
				<?php 
					$rs_komentar1 = $reports->km12_program_komentar($arr_data['kertas_kerja_id_program'], "Pengendali Teknis");
					foreach ($rs_komentar1 as $key => $komentar) {
						echo $komentar['auditor_name'].' : '.$komentar['program_comment_desc'].'<br>';
					}
				?>
			</td>
			<td align="center">Pengendali Teknis</td>
		</tr>
		<?
		}
		?>
	</table>
	<br>
	<table border='0' class="table_zero" cellspacing='0' cellpadding="0">
		<tr>
			<?php foreach (range(1,35) as $value): ?>
				<td>&nbsp;</td>
			<?php endforeach ?>
			<td align="center"><?=date('d-m-Y')?>,<br>Pengendali Teknis<br><br><br><br><?=$get_dalnis?></td>
		</tr>			
	</table>
</body>
</html>