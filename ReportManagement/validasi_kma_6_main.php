<?php 
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );
@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

$rs_km06 = $reports->km06_list($assign_id);
$arr_km06 = $rs_km06->FetchRow();
$status = 0;
$status = $arr_km06['km06_st'];

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$nama_katim = $arr_katim['auditor_name'];
$id_katim = $arr_katim['auditor_id'];
$user_id_katim = $arr_katim['user_id'];

$as_katim = false;
if($ses_id_int==$id_katim) $as_katim = true;

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$nama_dalnis = $arr_dalnis['auditor_name'];
$id_dalnis = $arr_dalnis['auditor_id'];
$user_id_dalnis = $arr_dalnis['user_id'];

$as_dalnis = false;
if($ses_id_int==$id_dalnis && $ses_id_int!="") $as_dalnis = true;

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$nama_daltu = $arr_daltu['auditor_name'];
$id_daltu = $arr_daltu['auditor_id'];
$user_id_daltu = $arr_daltu['user_id'];

$as_daltu = false;
if($ses_id_int==$id_daltu && $ses_id_int!="") $as_daltu = true;

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;
$def_page_request = "main_page.php?method=validate_kma_6&fil_id=".$assign_id;

switch ($_action) {
	case "postadd" :
		$f06_id = $comfunc->replacetext ( $_POST ["06_id"] );
		$kode_tipe = $comfunc->replacetext ( $_POST ["kode_tipe"] );
		$kode_pelaksana = $comfunc->replacetext ( $_POST ["kode_pelaksana"] );
		$ftanggal_kartu = $comfunc->date_db($_POST ["tanggal_kartu"] );
		$fket = $comfunc->replacetext ( $_POST ["ket"] );
		$fstatus = $comfunc->replacetext ( $_POST ["status"] );
		$notif_date = $comfunc->dateSql(date("d-m-Y"));
		// if($kode_pelaksana != ""){
			if($fstatus==1){
				$comfunc->hapus_notif($f06_id);
				$comfunc->insert_notif($f06_id, $ses_userId, $user_id_dalnis, 'validate_kma_6&fil_id='.$assign_id, "(KM 06) Mohon Reviu", $notif_date);
				$comfunc->hapus_notif($f06_id);
				$comfunc->insert_notif($f06_id, $ses_userId, $user_id_daltu, 'validate_kma_6&fil_id='.$assign_id, "(KM 06) Mohon Reviu", $notif_date);
			}
			if($fstatus==3){
				$comfunc->hapus_notif($f06_id);
				$comfunc->insert_notif($f06_id, $ses_userId, $user_id_katim, 'validate_kma_6&fil_id='.$assign_id, "(KM 06) ".$fket, $notif_date);
			}	
			if($fstatus==2){
				$comfunc->hapus_notif($f06_id);
			}	
		
			if($f06_id==""){
				$from = "km06";
				$reports->km06_add($assign_id, $_POST['km06_no'], $ftanggal_kartu, $fket, $fstatus, $_POST['nomor_rencana'], $_POST['tahun_terakhir_audit'], $_POST['risiko'], $_POST['tujuan'], $_POST['anggaran_ajukan'], $_POST['anggaran_setujui']);
			}else{
				$reports->km06_edit($f06_id, $_POST['km06_no'], $ftanggal_kartu, $fket, $fstatus, $_POST['nomor_rencana'], $_POST['tahun_terakhir_audit'], $_POST['risiko'], $_POST['tujuan'], $_POST['anggaran_ajukan'], $_POST['anggaran_setujui']);
			}
			
			$comfunc->js_alert_act ( 3 );
		// }
		// else{
		// 	$comfunc->js_alert_act ( 5 );
		// }
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	default :
		$_nextaction = "postadd";
		$page_request = "validasi_kma_6_acc.php";
		break;
}
include_once $page_request;
?>