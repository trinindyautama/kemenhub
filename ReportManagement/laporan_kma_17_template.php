<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	<header>Formulir KMA 17</header>
	<table border='0' class="table_guren" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="5" align="center" style="border:0px">Checklist<br>PENYELESAIAN LAPORAN</td>
		</tr>
		<tr>
			<th width="5%" align="center">No</th>
			<th width="65%" align="center" colspan="2">Uraian</th>
			<th width="10%" align="center">Sudah/Belum</th>
			<th width="20%" align="center">Keterangan</th>
		</tr>
		<tr>
			<td align="center">1</td>
			<td align="center" colspan="2">2</td>
			<td align="center">3</td>
			<td align="center">4</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td colspan="2"><b>RINGKASAN PIMPINAN</b></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">1</td>
			<td colspan="2">Ringkasan Pimpinan menyajikan overview ringkas atas auditi, tujuan audit, ruang lingkup, referensi atas kriteria audit, metodologi audit, dan simpulan hasil audit atas setiap tujuan audit</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '1') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td colspan="2"><b>BODI LAPORAN</b></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">2</td>
			<td colspan="2">Kecukupan informasi latar belakang auditi</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '2') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '2')?></td>
		</tr>
		<tr>
			<td align="center">3</td>
			<td colspan="2">Tujuan audit dan kriteria yang berkaitan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '3') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '3')?></td>
		</tr>
		<tr>
			<td align="center">4</td>
			<td colspan="2">Ruang lingkup audit sudah dinyatakan secara jelas</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '4') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '4')?></td>
		</tr>
		<tr>
			<td align="center">5</td>
			<td colspan="2">Jadwal audit, metodologi, standar audit yang diacu. Jika ada standar yang tidak diikuti, penjelasan yang memadai telah dibuat</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '5') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '5')?></td>
		</tr>
		<tr>
			<td align="center">6</td>
			<td colspan="2">Hasil observasi yang mendalam yang berkaitan dengan tujuan dan kriteria audit telah diperoleh untuk mencapai simpulan audit</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '6') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '6')?></td>
		</tr>
		<tr>
			<td align="center">7</td>
			<td colspan="2">Setiap observasi berisi pernyataan kondisi, kriteria, penyebab, dampak dan rekomendasi</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '7') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '7')?></td>
		</tr>
		<tr>
			<td align="center">8</td>
			<td colspan="2">Bukti yang cukup dan persuasif telah dikumpulkan untuk mendukung setiap observasi yang dilakukan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '8') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '8')?></td>
		</tr>
		<tr>
			<td align="center">9</td>
			<td colspan="2">Temuan yang bisa dikuantifisir telah dihitung secara memadai</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '9') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '9')?></td>
		</tr>
		<tr>
			<td align="center">10</td>
			<td colspan="2">Rekomendasi yang diberikan telah mengikuti alur logis dari hasil observasi dan penyebab, jelas dan cost-effective ditujukan pada pihak yang berkompeten</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '10') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '10')?></td>
		</tr>
		<tr>
			<td align="center">11</td>
			<td colspan="2">Simpulan telah disajikan untuk setiap tujuan audit dan telah didukung dengan bukti yang persuasif</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '11') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '11')?></td>
		</tr>
		<tr>
			<td align="center">12</td>
			<td colspan="2">Lampiran-lampiran yang disajikan memang menambah nilai laporan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '12') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '12')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td colspan="2"><b>FORMAT LAPORAN</b></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">13</td>
			<td colspan="2">Daftar isi yang menggambarkan struktur laporan dan judul yang sama dengan judul yang sama pada halaman bodi</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '13') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '13')?></td>
		</tr>
		<tr>
			<td align="center">14</td>
			<td colspan="2">Judul dan huruf yang konsisten</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '14') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '14')?></td>
		</tr>
		<tr>
			<td align="center">15</td>
			<td colspan="2">Bagan dan gambar telah dirujuk secara memadai dalam bodi laporan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '15') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '15')?></td>
		</tr>
		<tr>
			<td align="center">16</td>
			<td colspan="2">Struktur kalimat dan paragraf yang mudah dipahami</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '16') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '16')?></td>
		</tr>
		<tr>
			<td align="center">17</td>
			<td colspan="2">Singkatan-singkatan telah didefinisikan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '17') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '17')?></td>
		</tr>
		<tr>
			<td align="center">18</td>
			<td colspan="2">Bahasa dan terminologi yang mudah dipahami</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '18') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '18')?></td>
		</tr>
		<tr>
			<td align="center">19</td>
			<td colspan="2">Tata bahasa dan penulisan kata yang tepat</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '19') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '19')?></td>
		</tr>
		<tr>
			<td align="center">20</td>
			<td colspan="2">Lampiran disajikan secara seragam dan dirujuk pada bodi laporan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '20') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '20')?></td>
		</tr>
		<tr>
			<td align="center">21</td>
			<td colspan="2">Secara keseluruhan, laporan sudah jelas dan tepat</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '21') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '21')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<td colspan="2"><b>LAIN-LAIN</b></td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">22</td>
			<td colspan="2">Penyusunan telah melalui proses reviu:</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<!-- <td align="center">o</td> -->
			<td colspan="2"><ul> <li>Pengendali Teknis</li></ul></td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '22_1') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '22_1')?></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
			<!-- <td align="center" width="2%">o</td> -->
			<td colspan="2"><ul> <li>Pengendali Mutu</li></ul></td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '22_2') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '22_2')?></td>
		</tr>
		<tr>
			<td align="center">23</td>
			<td colspan="2">Distribusi Laporan telah sesuai dengan ketentuan</td>
			<td align="center"><?=($reports->get_data_st_km17 ( $assign_id, '23') == 'Sudah') ? "v" : ""?></td>
			<td align="center"><?=$reports->get_data_ket_km17 ( $assign_id, '23')?></td>
		</tr>
		<!-- <tr>
			<td colspan="2" style="border:0px">Direview oleh,
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Tanggal <?= $comfunc->dateIndo($reports->get_data_km17('km17_dalnis', $assign_id, '')) ?><br>Pengendali Teknis<br><br><br><br><?=$get_dalnis?></td>
			<td colspan="3" style="border:0px">Diisi oleh
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Tanggal <?= $comfunc->dateIndo($reports->get_data_km17('km17_katim', $assign_id, '')) ?><br>Ketua Tim<br><br><br><br><?=$get_katim?></td>
		</tr> -->
	</table>
	<br>
	<table border='0' class="table_guren" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="2" style="border:0px">Direviu oleh,
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Tanggal, <?= $comfunc->dateIndo($reports->get_data_km17('km17_dalnis', $assign_id, '')) ?><br>Pengendali Teknis<br><br><br><br><?=$get_dalnis?></td>
			<td colspan="3" style="border:0px">Diisi oleh,
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Tanggal, <?= $comfunc->dateIndo($reports->get_data_km17('km17_katim', $assign_id, '')) ?><br>Ketua Tim<br><br><br><br><?=$get_katim?></td>
		</tr>
	</table>
</body>
</html>