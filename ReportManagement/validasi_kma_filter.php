<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/report_class.php";

$assigns = new assign ( $ses_userId );
$reports = new report ( $ses_userId );

$fil_tahun = "";
$fil_satker = "";

$fil_tahun = $comfunc->replacetext ( @$_POST ["fil_tahun"] );
$fil_satker = $comfunc->replacetext ( @$_POST ["fil_satker"] );

?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Filter Kendali Mutu Audit</h3>
		</header>		
		<form action="" id="f" name="f" method="post">	
			<fieldset>
				<label class="span2">Tahun</label>
				<?php
				$rs_tahun = $assigns->assign_tahun_viewlist();
				$arr_tahun = $rs_tahun->GetArray ();
				echo $comfunc->buildCombo ( "fil_tahun", $arr_tahun, 0, 0, $fil_tahun, "submit()", "", false, false, true );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset>
				<label class="span2">Satker</label>
				<?php
				$rs_satker = $assigns->assign_auditee_only();
				$arr_satker = $rs_satker->GetArray ();
				echo $comfunc->buildCombo ( "fil_satker", $arr_satker, 0, 1, $fil_satker, "submit()", "", false, false, true );
				?>
				<span class="mandatory">*</span>
			</fieldset>
		</form>
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr align="center">
				<th width="2%">No</th>
				<th width="23%">No Surat Tugas</th>
				<th width="50%">Tanggal Surat</th>
				<th width="15%">Tanggal Pengawasan</th>
				<th width="10%">KMA</th>
			</tr>
			<?	
			$i=0;
			if ($base_on_id_int == '0') {
				$base_on_id_int = '';
			}
			$rs_assign = $reports->surat_tugas_viewlist_km($fil_tahun, $base_on_id_int, '', $fil_satker);
			// echo $base_on_id_int;
			while($arr_assign=$rs_assign->FetchRow()){
				$i++
			?>
			<tr>
				<td><?=$i?></td>
				<td><?=$arr_assign['assign_surat_no']?></td>
				<td align="center"><?=$comfunc->dateIndo($arr_assign['assign_surat_tgl'])?></td>
				<td align="center"><?=$comfunc->dateIndo($arr_assign['assign_start_date'])." s.d ".$comfunc->dateIndo($arr_assign['assign_end_date'])?></td>
				<td align="center"><a target="_self" href="main_page.php?method=validasi_kma&fil_surat_tugas=<?=$arr_assign ['assign_surat_id_assign']?>">View</a></td>
			</tr>
			<?
			}
			?>
		</table>
	</article>
</section>