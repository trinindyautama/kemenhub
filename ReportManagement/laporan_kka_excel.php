<?php
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/program_audit_class.php";
include_once "../_includes/classes/auditee_class.php";
include_once "../_includes/common.php";
$comfunc = new comfunction ();
$reports = new report (  );
$programaudits = new programaudit(  );
$auditees = new auditee(  );
$fil_tahun_id = "";
$fil_auditee_id = "";
$tahapan="";
$fil_tahun_id = $comfunc->replacetext ( $_REQUEST ["fil_tahun_id"] );
$fil_auditee_id = $comfunc->replacetext ( $_REQUEST ["fil_auditee_id"] );
$tahapan = $comfunc->replacetext($_REQUEST['tahapan']);
$assign_id = $reports->get_assignment_id($fil_auditee_id, $fil_tahun_id);
$rs_auditee = $auditees->auditee_data_viewlist($fil_auditee_id);
$arr_auditee = $rs_auditee->FetchRow();
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Type: image/jpg");
header('Content-Disposition: attachment; filename=LAPORAN KKA TAHUN '.$fil_tahun_id.'.xls');
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
	<style>
	body{
	width:210mm;
	font-family:Arial;
	font-size: 12;
	}
	</style>
	<body>
		<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<td colspan="7" align="center">KERTAS KERJA AUDIT</td>
			</tr>
			<tr>
				<td colspan="4" style="border-bottom: 0;border-right:0">Satuan Kerja</td>
				<td colspan="3" style="border-bottom: 0;border-left:0">: <?=$arr_auditee['auditee_name'];?></td>
			</tr>
			<tr>
				<td colspan="4" style="border-top: 0;border-bottom: 0;border-right:0">Tahun</td>
				<td colspan="3" style="border-top: 0;border-bottom: 0;border-left:0">: <?=$fil_tahun_id-1?>-<?=$fil_tahun_id?></td>
			</tr>
			<?php if ($tahapan != ""): ?>
			<tr>
				<td colspan="4" style="border-top: 0;border-bottom: 0;border-right:0">Tahapan Audit</td>
				<td colspan="3" style="border-top: 0;border-bottom: 0;border-left:0">: <?=strtoupper($tahapan)?></td>
			</tr>
			<?php endif ?>
			<tr align="center">
				<th width="2%">No</th>
				<th width="10%">Tahapan Audit</th>
				<th width="20%">Prosedur</th>
				<th width="8%">No KKA</th>
				<th width="20%">Uraian</th>
				<th width="35%">Kesimpulan</th>
				<th width="10%">Auditor</th>
			</tr>
			<?php
			$i=0;
			$rs_kka = $reports->kertas_kerja_list($assign_id, $fil_auditee_id, isset($tahapan) ? $tahapan : "");
			while($arr_kka = $rs_kka->FetchRow ()){
				$i++;
			?>
			<tr>
				<td valign="top"><?=$i?></td>
				<td valign="top"><?=$arr_kka['program_tahapan']?></td>
				<td valign="top"><?=$comfunc->text_show($arr_kka['ref_program_procedure']) ?></td>
				<td valign="top"><?=$arr_kka['kertas_kerja_no']?></td>
				<td valign="top"><?=$comfunc->text_show($arr_kka['kertas_kerja_desc'])?></td>
				<td valign="top"><?=$comfunc->text_show($arr_kka['kertas_kerja_kesimpulan'])?></td>
				<td valign="top"><?=$arr_kka['auditor_name']?></td>
			</tr>
			<?
			}
			?>
		</table>
	</body>
</html>