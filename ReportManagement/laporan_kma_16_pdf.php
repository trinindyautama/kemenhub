<?php
set_time_limit(0);
ini_set('memory_limit', '1024M');

require '../_includes/html2pdf/vendor/autoload.php';
include_once "../_includes/common.php";

$comfunc = new comfunction();
define('DOMPDF_ENABLE_AUTOLOAD', false);
define("DOMPDF_ENABLE_FONTSUBSETTING", true);
define("DOMPDF_UNICODE_ENABLED", true);
 
require_once '../_includes/html2pdf/vendor/dompdf/dompdf/dompdf_config.inc.php';
include_once "../_includes/classes/report_class.php";
include_once "../_includes/classes/assignment_class.php";

$reports = new report ();
$assigns = new assign();

$assign_id = "";

$assign_id = $comfunc->replacetext ( $_REQUEST ["fil_id"] );

$rs = $assigns->assign_viewlist($assign_id);
$arr_assign = $rs->FetchRow();

$rs_km06 = $reports->km06_list($assign_id);
$arr_km06 = $rs_km06->FetchRow();

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$htmlString = '';
ob_start();
include('laporan_kma_16_template.php');
$htmlString .= ob_get_clean();

$dompdf = new DOMPDF();
$dompdf->load_html($htmlString);
$dompdf->set_paper('legal', 'landscape');
$dompdf->render();
$dompdf->stream("Formulir KMA 16.pdf");
?>