<html>
	<head>
		<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
	</head>
	<body>
	<header>Formulir KMA 7</header>
		<table border="0" class="table_reni" cellspacing='0' cellpadding="0">
			<tr>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" colspan="8" align="center">ALOKASI ANGGARAN WAKTU AUDIT<br>(hanya jam-jam efektif)</td>
			</tr>
			<tr>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" colspan="8" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" colspan="8" align="center">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" width="15%">
					Nama Audit
				</td>
				<td width="60%" style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">:  
					<?
					$rs_id_auditee = $assigns->assign_auditee_viewlist ($assign_id);
					while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
						echo $arr_id_auditee ['auditee_name']."<br>";
					}
					?>
				</td>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" width="15%">
					Sasaran Audit
				</td>
				<td width="40%" style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">
				: <?= $arr_assign['audit_type_name'] ?>
				</td>
			</tr>
			<tr>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">
					Disusun Oleh
				</td>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">: 
					<?= $reports->get_nama_auditor($reports->get_data_km7('km7_created_by', $assign_id)) ?>
				</td>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">
					Disetujui Oleh
				</td>
				<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">:
					<?= $reports->get_nama_auditor($reports->get_data_km7('km7_approved_by', $assign_id)) ?>
				</td>
			</tr>
		</table>
		<table border='1' class="table_reni" cellspacing='0' cellpadding="0">
			<tr>
				<th width="55%" align="center">Jenis pekerjaan yang harus dilakukan</th>
				<th width="30%" align="center" colspan="3">Ketua Tim & Anggota Tim</th>
				<th width="15%" align="center" colspan="2">Tanggal</th>
				<th width="10%" align="center" colspan="2">Anggaran Waktu</th>
				<th width="10%" align="center">Anggaran Biaya</th>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center" colspan="3">2</td>
				<td align="center" colspan="2">3</td>
				<td align="center" colspan="2">4</td>
				<td align="center">5</td>
			</tr>
			<?php $no = 0; ?>
			<?php foreach ($array as $key => $parent): ?>
			<?php $no++ ?>
			<tr>
				<td><?= $key ?> :</td>
				<td colspan="3"></td>
				<td colspan="2"></td>
				<td colspan="2"></td>
				<td></td>
			</tr>
				<?php $num = 0 ?>
				<?php foreach ($parent as $child): ?>
				<?php $num++ ?>
				<tr>
					<!-- <td width="2%">o</td> -->
					<td width="38%">o <?= $child ?>
					</td>
					<td align="center">
						<?php $explode = explode(" ", str_replace(",", " ", $reports->get_nama_auditor($reports->get_data_km7('km7_auditor_id_1', $assign_id, $no.'_'.$num)))) ?>
						<?=$explode[0]?>
					</td>
					<td align="center">
						<?php $explode = explode(" ", str_replace(",", " ", $reports->get_nama_auditor($reports->get_data_km7('km7_auditor_id_2', $assign_id, $no.'_'.$num)))) ?>
						<?=$explode[0]?>
					</td>
					<td align="center">
						<?php $explode = explode(" ", str_replace(",", " ", $reports->get_nama_auditor($reports->get_data_km7('km7_auditor_id_3', $assign_id, $no.'_'.$num)))) ?>
						<?=$explode[0]?>
					</td>
					<td align="center">
						<?= $comfunc->dateIndo3($reports->get_data_km7('km7_date_1', $assign_id, $no.'_'.$num)) ?>
						
					</td>
					<td align="center">
						<?= $comfunc->dateIndo3($reports->get_data_km7('km7_date_2', $assign_id, $no.'_'.$num)) ?>
					</td>
					<td align="center">
						<?= $reports->get_data_km7('km7_time_1', $assign_id, $no.'_'.$num) ?>
					</td>
					<td align="center">
						<?= $reports->get_data_km7('km7_time_2', $assign_id, $no.'_'.$num) ?>
					</td>
					<td align="center">
						<!-- <input type="number" min="0" name="km_7_cost_<?= $no.'_'.$num ?>" value="<?= $reports->get_data_km7('km7_cost', $assign_id, $no.'_'.$num) ?>"> -->
					</td>
				</tr>
				<?php endforeach ?>
			<?php endforeach ?>
			<tr>
				<?php foreach (range(1,9) as $value): ?>
					<td>&nbsp;</td>
				<?php endforeach ?>
			</tr>
			<tr>
				<td colspan="6" align="center">Jumlah yang dianggarkan</td><td></td><td colspan="2" align="right">Rp. <?= number_format($reports->get_data_km7('km7_cost', $assign_id, '')) ?></td>
			</tr>
		</table>
		<table border='0' class="table_reni" cellspacing='0' cellpadding="0">
			<tr>
				<td width="50%" style="border:0px">&nbsp;</td>
				<td width="50%" style="border:0px">&nbsp;</td>
			</tr>
			<tr>
				<td style="border:0px">&nbsp;</td>
				<td align="center" style="border:0px">
					Jakarta, 
				<?=$comfunc->dateIndo($reports->get_data_km7('km7_created_date', $assign_id, ''))?>
				
				</td>
			</tr>
			<tr>
				<td align="center" style="border:0px">Ketua Tim</td>
				<td align="center" style="border:0px">Mengetahui:<br>Pengendali Teknis</td>
			</tr>
			<tr>
				<td style="border:0px"><br><br><br><br></td>
				<td style="border:0px"><br><br><br><br></td>
			</tr>
			<tr>
				<td align="center" style="border:0px"><?=$get_katim;?></td>
				<td align="center" style="border:0px"><?=$get_dalnis;?></td>
			</tr>
		</table>
	</body>
</html>