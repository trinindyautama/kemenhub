<?
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_REQUEST ["fil_id"])){
	@session_start();
	$_SESSION['ses_assign'] = $comfunc->replacetext ( $_REQUEST ["fil_id"] );
}

$assign_id = $_SESSION['ses_assign'];

$paging_request = "main_page.php?method=validate_kma_21";
$acc_page_request = "validasi_kma_21_acc.php";
$list_page_request = "report_view.php";
$view_parrent = "validasi_kma_21_parrent.php";
$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";

$grid = "grid_km.php";
$gridHeader = array ("Tanggal", "Dihadiri Oleh", "Temuan Sebelum", "Jml TL", "Temuan Sesudah");
$gridDetail = array ("1", "2", "3", "4", "5");
$gridWidth = array ("15", "20", "15", "15", "15");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah BAP";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $reports->km21_data_viewlist ( $fdata_id );
		$page_title = "Ubah BAP";
		break;
	case "postadd" :
		$ftanggal = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal"] ));
		$fperiode = $comfunc->replacetext ( $_POST ["periode"] );
		$fjml_temuan_before = $comfunc->replacetext ( $_POST ["jml_temuan_before"] );
		$fnilai_temuan_before = $comfunc->replacetext ( $_POST ["nilai_temuan_before"], true );
		$fjml_tl = $comfunc->replacetext ( $_POST ["jml_tl"] );
		$fnilai_tl = $comfunc->replacetext ( $_POST ["nilai_tl"], true );
		$fjml_temuan_after = $comfunc->replacetext ( $_POST ["jml_temuan_after"] );
		$fnilai_temuan_after = $comfunc->replacetext ( $_POST ["nilai_temuan_after"], true );
		$fdaftar_hadir = $comfunc->replacetext ( $_POST ["daftar_hadir"] );
		if ($ftanggal != "" && $fperiode != "" && $fjml_temuan_before != "" && $fjml_tl != "" && $fjml_temuan_after != "" && $fdaftar_hadir != "") {
			$reports->km21_add ( $assign_id, $ftanggal, $fperiode, $fjml_temuan_before, $fnilai_temuan_before, $fjml_tl, $fnilai_tl, $fjml_temuan_after, $fnilai_temuan_after, $fdaftar_hadir );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$ftanggal = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal"] ));
		$fperiode = $comfunc->replacetext ( $_POST ["periode"] );
		$fjml_temuan_before = $comfunc->replacetext ( $_POST ["jml_temuan_before"] );
		$fnilai_temuan_before = $comfunc->replacetext ( $_POST ["nilai_temuan_before"], true );
		$fjml_tl = $comfunc->replacetext ( $_POST ["jml_tl"] );
		$fnilai_tl = $comfunc->replacetext ( $_POST ["nilai_tl"], true );
		$fjml_temuan_after = $comfunc->replacetext ( $_POST ["jml_temuan_after"] );
		$fnilai_temuan_after = $comfunc->replacetext ( $_POST ["nilai_temuan_after"], true );
		$fdaftar_hadir = $comfunc->replacetext ( $_POST ["daftar_hadir"] );
		if ($ftanggal != "" && $fperiode != "" && $fjml_temuan_before != "" && $fjml_tl != "" && $fjml_temuan_after != "" && $fdaftar_hadir != "") {
			$reports->km21_edit ( $fdata_id, $ftanggal, $fperiode, $fjml_temuan_before, $fnilai_temuan_before, $fjml_tl, $fnilai_tl, $fjml_temuan_after, $fnilai_temuan_after, $fdaftar_hadir );
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$reports->km21_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $reports->km21_count ($assign_id);
		$rs = $reports->km21_view_grid ($assign_id, $offset, $num_row );
		$page_title = "Daftar BAP (KMA 21)";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
