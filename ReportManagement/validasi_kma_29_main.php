<?
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_REQUEST ["fil_id"])){
	@session_start();
	$_SESSION['ses_assign'] = $comfunc->replacetext ( $_REQUEST ["fil_id"] );
}

$assign_id = $_SESSION['ses_assign'];

$paging_request = "main_page.php?method=validate_kma_29";
$acc_page_request = "validasi_kma_29_acc.php";
$list_page_request = "report_view.php";
$view_parrent = "validasi_kma_29_parrent.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";
$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;

$grid = "grid_km.php";
$gridHeader = array ("Nama Penilai", "Nama yg Dinilai", "Orientasi Pelayanan", "Integritas", "Komitmen", "Disiplin", "Kerjasama", "Kepemimpinan");
$gridDetail = array ("1", "2", "3", "4", "5", "6", "7", "8");
$gridWidth = array ("18", "18", "8", "8", "8", "8", "8", "8");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "0";
// === end grid ===//

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$get_katim = $arr_katim['auditor_name'];

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$get_dalnis = $arr_dalnis['auditor_name'];

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$get_daltu = $arr_daltu['auditor_name'];

$rs_penilai = $assigns->assign_list_auditor_only($ses_id_int, $assign_id);
$arr_penilai = $rs_penilai->FetchRow();

switch ($_action) {
	case "getadd" :
		if($arr_penilai['auditor_id']!="" && $ses_id_int!=""){
			$_nextaction = "postadd";
			$page_request = $acc_page_request;
			$page_title = "Tambah Penilaian Kinerja Auditor Atas Penugasan Audit";
		} else {
			echo "<script>alert('Maaf Anda Tidak Berhak Menilai Tim Ini');</script>";
			echo "<script>window.open('".$def_page_request."', '_self');</script>";
			$page_request = "blank.php";
		}
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $reports->km29_data_viewlist ( $fdata_id );
		$page_title = "Ubah Penilaian Kinerja Auditor Atas Penugasan Audit";
		break;
	case "postadd" :
		$fpenilai = $ses_id_int;
		$fdinilai = $comfunc->replacetext ( $_POST ["dinilai"] );
		$fpelayanan = $comfunc->replacetext ( $_POST ["pelayanan"] );
		$fintegritas = $comfunc->replacetext ( $_POST ["integritas"] );
		$fkomitmen = $comfunc->replacetext ( $_POST ["komitmen"] );
		$fdisiplin = $comfunc->replacetext ( $_POST ["disiplin"] );
		$fkerjasama = $comfunc->replacetext ( $_POST ["kerjasama"] );
		$fkepemimpinan = $comfunc->replacetext ( $_POST ["kepemimpinan"] );
		$ftanggal = $comfunc->date_db(date('d-m-Y'));
		if ($fpenilai != "" && $fdinilai != "") {
			$reports->km29_add ( $assign_id, $fpenilai, $fdinilai, $fpelayanan, $fintegritas, $fkomitmen, $fdisiplin, $fkerjasama, $fkepemimpinan, $ftanggal );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fdinilai = $comfunc->replacetext ( $_POST ["dinilai"] );
		$fpelayanan = $comfunc->replacetext ( $_POST ["pelayanan"] );
		$fintegritas = $comfunc->replacetext ( $_POST ["integritas"] );
		$fkomitmen = $comfunc->replacetext ( $_POST ["komitmen"] );
		$fdisiplin = $comfunc->replacetext ( $_POST ["disiplin"] );
		$fkerjasama = $comfunc->replacetext ( $_POST ["kerjasama"] );
		$fkepemimpinan = $comfunc->replacetext ( $_POST ["kepemimpinan"] );
		$ftanggal = $comfunc->date_db(date('d-m-Y'));
		if ($fdinilai != "") {
			$reports->km29_edit ( $fdata_id, $fdinilai, $fpelayanan, $fintegritas, $fkomitmen, $fdisiplin, $fkerjasama, $fkepemimpinan, $ftanggal );
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$reports->km29_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $reports->km29_count ($assign_id);
		$rs = $reports->km29_view_grid ($assign_id, $offset, $num_row );
		$page_title = "Daftar Penilaian Kinerja Auditor Atas Penugasan Audit (KMA 29)";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
