<html>
	<head>
		<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
	</head>
	<body>
	<header>Formulir KMA 11</header>
	<br>
	<table class="table_ryunmi" border="1" cellspacing='0' cellpadding="0">
		<tr>
			<td>
				<table border='0' class="table_reni" cellspacing='0' cellpadding="0">
					<tr>
						<td colspan="10" align="center" style="border:0px">NOTULENSI KESEPAKATAN</td>
					</tr>
					<tr>
						<td colspan="10" align="center" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">
							Berdasarkan hasil rapat koordinasi antara tim audit dengan auditi (
							<?
							$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
							$assign_id_auditee = "";
							while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
								echo $arr_id_auditee ['auditee_name'].",";
							}
							?>) Pada:
						</td>
					</tr>
					<tr>
						<td width="5%" style="border:0px">&nbsp;</td>
						<td width="5%" style="border:0px">&nbsp;</td>
						<td width="10%" style="border:0px">Hari</td>
						<td width="5%" style="border:0px">:</td>
						<td colspan="3" style="border:0px"><?=$arr_km_11['km11_hari']?></td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td style="border:0px">&nbsp;</td>
						<td style="border:0px">Tanggal</td>
						<td style="border:0px">:</td>
						<td colspan="3" style="border:0px"><?=$comfunc->dateIndo($arr_km_11['km11_tanggal'])?></td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td style="border:0px">&nbsp;</td>
						<td style="border:0px">Waktu</td>
						<td style="border:0px">:</td>
						<td colspan="3" style="border:0px"><?=$arr_km_11['km11_waktu']?></td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">Dihadiri Oleh:</td>
					</tr>
					<tr>
						<td width="5%" style="border:0px">&nbsp;</td>
						<td colspan="4" style="border:0px">Tim Auditee :</td>
						<td width="5%" style="border:0px">&nbsp;</td>
						<td colspan="4" style="border:0px">Tim Auditor :</td>
					</tr>
					<tr>
						<td width="5%" style="border:0px">&nbsp;</td>
						<td colspan="4" style="border:0px">
							<table border="0" class="table_reni" cellspacing='0' cellpadding="0">
								<?
								$explode_tim = explode(";",$arr_km_11['km11_tim_auditee']);
								$count_tim = count($explode_tim);
								// echo $count_tim;
								for($x=0;$x<$count_tim;$x++){
								?>
								<?
								if ($explode_tim[$x] != '') {
								?>
								<tr>
									<td width="5%" style="border:0px"><?=$x+1?>)</td>
									<td style="border:0px"><?=$explode_tim[$x]?></td>
								</tr>
								<?
								}
								?>
								<?
								}
								?>
							</table>
						</td>
						<td width="5%" style="border:0px">&nbsp;</td>
						<td colspan="4" style="border:0px">
							<table cellspacing='0' cellpadding="0">
								<?
								$i=0;
								$rs_auditor = $assigns->anggota_list($assign_id, "", true);
								while($arr_auditor = $rs_auditor->FetchRow()){
								$i++;
								?>
								<tr>
									<td width="5%" style="border:0px"><?=$i?>)</td>
									<td style="border:0px"><?=$arr_auditor['auditor_name']?></td>
								</tr>
								<?
								}
								?>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">Diperoleh kesepakatan sebagai berikut:</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">1.</td>
						<td colspan="9" style="border:0px">Tujuan Audit :</td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="8" style="border:0px"><?= $comfunc->text_show($arr_km_6['km06_tujuan']) ?></td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="9" style="border:0px">Prosedur audit yang akan dilaksanakan adalah sebagai berikut:</td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="8" style="border:0px"><?=$comfunc->text_show($arr_km_11['km11_prosedur'])?></td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">2.</td>
						<td colspan="9" style="border:0px">Waktu Pelaksanaan Audit</td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="3" style="border:0px">Survei Pendahuluan</td>
						<td colspan="3" style="border:0px">: <?=$comfunc->dateIndo($arr_km_11['km11_survei'])?></td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="3" style="border:0px">Pelaksanaan audit</td>
						<td colspan="3" style="border:0px">: <?=$comfunc->dateIndo($arr_assign['assign_start_date'])." s.d ".$comfunc->dateIndo($arr_assign['assign_end_date'])?></td>
					</tr>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="3" style="border:0px">Penyelesaian laporan</td>
						<td colspan="3" style="border:0px">: <?=$comfunc->dateIndo($arr_km_11['km11_penyelesaian_awal'])." s.d ".$comfunc->dateIndo($arr_km_11['km11_penyelesaian_akhir'])?></td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">3.</td>
						<td colspan="9" style="border:0px">Tim Audit yang ditugaskan</td>
					</tr>
					<?
					$i=0;
					$rs_auditor = $assigns->anggota_list($assign_id);
					while($arr_auditor = $rs_auditor->FetchRow()){
					$i++;
					?>
					<tr>
						<td style="border:0px">&nbsp;</td>
						<td colspan="3" style="border:0px"><?=$arr_auditor['posisi_name']?></td>
						<td colspan="3" style="border:0px">: <?=$arr_auditor['auditor_name']?></td>
					</tr>
					<?
					}
					?>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">4</td>
						<td colspan="9" style="border:0px">Dalam pelaksanaan audit, yang akan menjadi kontak person adalah <?=$arr_km_11['km11_pic']?>.  Pelaksanaan audit akan dilakukan terhadap lingkungan Pengendalian dan aktifitas pengendalian.</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">5.</td>
						<td colspan="6" style="border:0px"><?=$comfunc->text_show($arr_km_11['km11_ket_tambahan'])?></td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">6.</td>
						<td colspan="9" style="border:0px">Prosedur pelaporan dan tindak lanjut akan mengacu pada standar audit APIP dan tindakan koreksi terhadap rekomendasi temuan audit paling lambat akan dilakukan dalam waktu 60 hari setelah tanggal kesepakatan ditetapkan.</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="border:0px">7.</td>
						<td colspan="9" style="border:0px">Seluruh biaya yang terjadi selama audit ditanggung oleh kantor tim audit.</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" colspan="5" style="border:0px">&nbsp;<br>Perwakilan Auditi<br><br><br><br>( <?=$arr_km_11['km11_ttd_auditee']?> )</td>
						<?php
							$auditor = $auditors->auditor_data_viewlist($arr_km_11['km11_auditor']);
							$arr_auditor = $auditor->FetchRow();
						?>
						<td align="center" colspan="5" style="border:0px"><?= $comfunc->dateIndo($arr_km_11['km11_date']) ?><br>Perwakilan Auditor<br><br><br><br>( <?=$arr_auditor['auditor_name']?> )</td>
					</tr>
					<tr>
						<td colspan="10" style="border:0px">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>