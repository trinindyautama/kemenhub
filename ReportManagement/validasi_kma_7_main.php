<?php
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report($ses_userId);
$assigns = new assign($ses_userId);

$assign_id = "";

$assign_id = $comfunc->replacetext($_REQUEST["fil_id"]);

$rs = $assigns->assign_viewlist($assign_id);
$arr = $rs->FetchRow();

@$_action = $comfunc->replacetext($_REQUEST["data_action"]);

$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
$arr_katim = $rs_katim->FetchRow();
$nama_katim = $arr_katim['auditor_name'];
$id_katim = $arr_katim['auditor_id'];
$user_id_katim = $arr_katim['user_id'];

$as_katim = false;
if($ses_id_int==$id_katim) $as_katim = true;

$rs_dalnis = $assigns->anggota_list ( $assign_id, 'pt' );
$arr_dalnis = $rs_dalnis->FetchRow();
$nama_dalnis = $arr_dalnis['auditor_name'];
$id_dalnis = $arr_dalnis['auditor_id'];
$user_id_dalnis = $arr_dalnis['user_id'];

$as_dalnis = false;
if($ses_id_int==$id_dalnis && $ses_id_int!="") $as_dalnis = true;

$rs_daltu = $assigns->anggota_list ( $assign_id, 'pm' );
$arr_daltu = $rs_daltu->FetchRow();
$nama_daltu = $arr_daltu['auditor_name'];
$id_daltu = $arr_daltu['auditor_id'];
$user_id_daltu = $arr_daltu['user_id'];

$as_daltu = false;
if($ses_id_int==$id_daltu && $ses_id_int!="") $as_daltu = true;

$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=" . $assign_id;
$def_page_request  = "main_page.php?method=validate_kma_7&fil_id=" . $assign_id;

$array = array(
    'PEKERJAAN-PEKERJAAN PERSIAPAN' => array(
        'Pembicaraan pendahuluan (koordinasi)',
        'Survei pendahuluan (dokumen, peraturan terkait)',
        'Penyusunan Program Audit',
    ),
    'PELAKSANAAN AUDIT'             => array(
        'Pengukuran Kinerja',
        'Analisis Kinerja',
        'Mereviu KKA',
        'Penyusunan Ikhtisar Temuan Hasil Audit',
    ),
    'PENYELESAIAN PEKERJAAN'        => array(
        'Meneliti kelengkapan KKA',
        'Pembahasan Ketua Tim, Pengendali Teknis, dan Pengendali Mutu',
        'Penyusunan Matriks Temuan Hasil Audit dan Konsep Laporan Hasil Audit',
    ),
);

switch ($_action) {
    case "postadd":
    	$no = 0;
        foreach ($array as $key => $values) {
        	$no++;
        	$num = 0;
        	foreach ($values as $value) {
        	$num++;
        		// if ($_POST['km_7_auditor_id_'.$no.'_'.$num] != '') {
        			
        			if ($_POST['km_7_id_'.$no.'_'.$num] == '') {
        			
        				$reports->insert_km_7(
	        				$assign_id, 
	        				$_POST['km_7_auditor_id_'.$no.'_'.$num.'_1'], 
                            $_POST['km_7_auditor_id_'.$no.'_'.$num.'_2'], 
                            $_POST['km_7_auditor_id_'.$no.'_'.$num.'_3'], 
	        				$comfunc->date_db($_POST['km_7_date_'.$no.'_'.$num.'_1']),
                            $comfunc->date_db($_POST['km_7_date_'.$no.'_'.$num.'_2']), 
	        				$_POST['km_7_time_'.$no.'_'.$num.'_1'],
                            $_POST['km_7_time_'.$no.'_'.$num.'_2'], 
                            $no.'_'.$num, 
	        				$_POST['km_7_cost'], 
	        				$_POST['created_by'], 
	        				$_POST['approved_by'],
                            $comfunc->date_db($_POST['tanggal_kartu'])
	        			);

        			} else {
        				$reports->update_km_7(
        					$_POST['km_7_id_'.$no.'_'.$num],
        					$_POST['km_7_auditor_id_'.$no.'_'.$num.'_1'], 
                            $_POST['km_7_auditor_id_'.$no.'_'.$num.'_2'], 
                            $_POST['km_7_auditor_id_'.$no.'_'.$num.'_3'], 
                            $comfunc->date_db($_POST['km_7_date_'.$no.'_'.$num.'_1']),
                            $comfunc->date_db($_POST['km_7_date_'.$no.'_'.$num.'_2']), 
                            $_POST['km_7_time_'.$no.'_'.$num.'_1'],
                            $_POST['km_7_time_'.$no.'_'.$num.'_2'], 
	        				$_POST['km_7_cost'], 
	        				$_POST['created_by'], 
	        				$_POST['approved_by'],
                            $comfunc->date_db($_POST['tanggal_kartu'])	
        				);
        			}
        		// }	
        	}
        }
        $comfunc->js_alert_act(3);
        ?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
        $page_request = "blank.php";
        break;
    default:
        $_nextaction  = "postadd";
        $page_request = "validasi_kma_7_acc.php";
        $array = $array;
        $rs           = $assigns->assign_viewlist($assign_id);
        $arr_assign   = $rs->FetchRow();
        break;
}
include_once $page_request;
?>