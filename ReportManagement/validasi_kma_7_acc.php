<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">
				Formulir KMA 7
			</h3>	
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border="0" class="table_risk">
				<tr>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" colspan="7" align="center">ALOKASI ANGGARAN WAKTU AUDIT<br>(hanya jam-jam efektif)</td>
				</tr>
				<tr>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" width="10%">
						Nama Audit
					</td>
					<td width="50%" style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">:  
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ($assign_id);
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_name']."<br>";
						}
						?>
					</td>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;" width="10%">
						Sasaran Audit
					</td>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">
					: <?= $arr_assign['audit_type_name'] ?>
					</td>
				</tr>
				<tr>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">
						Disusun Oleh
					</td>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">: 
						<select name="created_by">
							<option selected="selected" value=""></option>
							<?php foreach ($reports->getPosPenugasanKatimAtim(array('1fe7f8b8d0d94d54685cbf6c2483308aebe96009'), 'NOT IN') as $pos): ?>
							<optgroup label="<?= $pos['posisi_name'] ?>">
								<?php foreach ($reports->getNamaKatimAtimByAssignPos($assign_id, $pos['posisi_id']) as $auditor): ?>
								<option value="<?= $auditor['auditor_id'] ?>"
									<?= ($reports->get_data_km7('km7_created_by', $assign_id) == $auditor['auditor_id']) ? "selected='selected'": "" ?>
									>
									<?= $auditor['auditor_name'] ?>
								</option>
								<?php endforeach ?>
							</optgroup>
							<?php endforeach ?>
						</select>
					</td>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">
						Disetujui Oleh
					</td>
					<td style="border-bottom-style: none;border-top-style: none;border-left-style: none;border-right-style: none;">:
						<select name="approved_by">
							<option selected="selected" value=""></option>
							<?php foreach ($reports->getPosPenugasanKatimAtim(array('9e8e412b0bc5071b5d47e30e0507fe206bdf8dbd', '1fe7f8b8d0d94d54685cbf6c2483308aebe96229'), 'IN') as $pos): ?>
							<optgroup label="<?= $pos['posisi_name'] ?>">
								<?php foreach ($reports->getNamaKatimAtimByAssignPos($assign_id, $pos['posisi_id']) as $auditor): ?>
								<option value="<?= $auditor['auditor_id'] ?>"
									<?= ($reports->get_data_km7('km7_approved_by', $assign_id) == $auditor['auditor_id']) ? "selected='selected'": "" ?>
									>
									<?= $auditor['auditor_name'] ?>
								</option>
								<?php endforeach ?>
							</optgroup>
							<?php endforeach ?>
						</select>
					</td>
				</tr>
			</table>
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<th width="20%" align="center">Jenis pekerjaan yang harus dilakukan</th>
					<th width="30%" align="center" colspan="3">Ketua Tim & Anggota Tim</th>
					<th width="15%" align="center" colspan="2">Tanggal</th>
					<th width="15%" align="center" colspan="2">Anggaran Waktu</th>
					<th width="10%" align="center">Anggaran Biaya</th>
				</tr>
				<tr>
					<td align="center">1</td>
					<td align="center" colspan="3">2</td>
					<td align="center" colspan="2">3</td>
					<td align="center" colspan="2">4</td>
					<td align="center">5</td>
				</tr>
				<?php $no = 0; ?>
				<?php foreach ($array as $key => $parent): ?>
				<?php $no++ ?>
				<tr>
					<td><?= $key ?> :</td>
					<td colspan="3"></td>
					<td colspan="2"></td>
					<td colspan="2"></td>
					<td></td>
				</tr>
					<?php $num = 0 ?>
					<?php foreach ($parent as $child): ?>
					<?php $num++ ?>
					<tr>
						<td><li><?= $child ?></li>
							<input type="hidden" name="km_7_id_<?= $no.'_'.$num ?>" value="<?= $reports->get_data_km7('km7_id', $assign_id, $no.'_'.$num) ?>">
						</td>
						<td align="center">
							<select name="km_7_auditor_id_<?= $no.'_'.$num.'_1' ?>">
								<option selected="selected" value=""></option>
								<?php foreach ($reports->getPosPenugasanKatimAtim(array('8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '6a70c2a39af30df978a360e556e1102a2a0bdc02'), 'IN') as $pos): ?>
								<optgroup label="<?= $pos['posisi_name'] ?>">
									<?php foreach ($reports->getNamaKatimAtimByAssignPos($assign_id, $pos['posisi_id']) as $auditor): ?>
									<option value="<?= $auditor['auditor_id'] ?>"
										<?= ($reports->get_data_km7('km7_auditor_id_1', $assign_id, $no.'_'.$num) == $auditor['auditor_id']) ? "selected='selected'": "" ?>
										>
										<?php $explode = explode(" ", str_replace(",", " ", $auditor['auditor_name'])) ?>
										<?=$explode[0]?>
									</option>
									<?php endforeach ?>
								</optgroup>
								<?php endforeach ?>
							</select>
						</td>
						<td align="center">
							<select name="km_7_auditor_id_<?= $no.'_'.$num.'_2' ?>">
								<option selected="selected" value=""></option>
								<?php foreach ($reports->getPosPenugasanKatimAtim(array('8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '6a70c2a39af30df978a360e556e1102a2a0bdc02'), 'IN') as $pos): ?>
								<optgroup label="<?= $pos['posisi_name'] ?>">
									<?php foreach ($reports->getNamaKatimAtimByAssignPos($assign_id, $pos['posisi_id']) as $auditor): ?>
									<option value="<?= $auditor['auditor_id'] ?>"
										<?= ($reports->get_data_km7('km7_auditor_id_2', $assign_id, $no.'_'.$num) == $auditor['auditor_id']) ? "selected='selected'": "" ?>
										>
										<?php $explode = explode(" ", str_replace(",", " ", $auditor['auditor_name'])) ?>
										<?=$explode[0]?>
									</option>
									<?php endforeach ?>
								</optgroup>
								<?php endforeach ?>
							</select>
						</td>
						<td align="center">
							<select name="km_7_auditor_id_<?= $no.'_'.$num.'_3' ?>">
								<option selected="selected" value=""></option>
								<?php foreach ($reports->getPosPenugasanKatimAtim(array('8918ca5378a1475cd0fa5491b8dcf3d70c0caba7', '6a70c2a39af30df978a360e556e1102a2a0bdc02'), 'IN') as $pos): ?>
								<optgroup label="<?= $pos['posisi_name'] ?>">
									<?php foreach ($reports->getNamaKatimAtimByAssignPos($assign_id, $pos['posisi_id']) as $auditor): ?>
									<option value="<?= $auditor['auditor_id'] ?>"
										<?= ($reports->get_data_km7('km7_auditor_id_3', $assign_id, $no.'_'.$num) == $auditor['auditor_id']) ? "selected='selected'": "" ?>
										>
										<?php $explode = explode(" ", str_replace(",", " ", $auditor['auditor_name'])) ?>
										<?=$explode[0]?>
									</option>
									<?php endforeach ?>
								</optgroup>
								<?php endforeach ?>
							</select>
						</td>
						<td align="center">
							<input type="text" class="span9" id="km_7_date_<?= $no.'_'.$num.'_1' ?>" name="km_7_date_<?= $no.'_'.$num.'_1' ?>" value="<?= $comfunc->dateIndoDb($reports->get_data_km7('km7_date_1', $assign_id, $no.'_'.$num)) ?>">
							<script>	
							$("#km_7_date_<?= $no.'_'.$num.'_1' ?>").datepicker({
								dateFormat: 'yy-mm-dd',
								 nextText: "",
								 prevText: "",
								 changeYear: true,
								 changeMonth: true,
								 minDate: '<?= $comfunc->dateIndoDb($arr['assign_start_date']) ?>',
								 maxDate: '<?= $comfunc->dateIndoDb($arr['assign_end_date']) ?>'
							});  
							</script>
						</td>
						<td align="center">
							<input type="text" class="span9" id="km_7_date_<?= $no.'_'.$num.'_2' ?>" name="km_7_date_<?= $no.'_'.$num.'_2' ?>" value="<?= $comfunc->dateIndoDb($reports->get_data_km7('km7_date_2', $assign_id, $no.'_'.$num)) ?>">
							<script>	
							$("#km_7_date_<?= $no.'_'.$num.'_2' ?>").datepicker({
								dateFormat: 'yy-mm-dd',
								 nextText: "",
								 prevText: "",
								 changeYear: true,
								 changeMonth: true,
								 minDate: '<?= $comfunc->dateIndoDb($arr['assign_start_date']) ?>',
								 maxDate: '<?= $comfunc->dateIndoDb($arr['assign_end_date']) ?>'
							});  
							</script>
						</td>
						<td align="center">
							<input type="number" class="span5" min="0" step="0.5" name="km_7_time_<?= $no.'_'.$num.'_1' ?>" value="<?= $reports->get_data_km7('km7_time_1', $assign_id, $no.'_'.$num) ?>">
						</td>
						<td align="center">
							<input type="number" class="span5" min="0" step="0.5" name="km_7_time_<?= $no.'_'.$num.'_2' ?>" value="<?= $reports->get_data_km7('km7_time_2', $assign_id, $no.'_'.$num) ?>">
						</td>
						<td align="center">
							<!-- <input type="number" min="0" name="km_7_cost_<?= $no.'_'.$num ?>" value="<?= $reports->get_data_km7('km7_cost', $assign_id, $no.'_'.$num) ?>"> -->
						</td>
					</tr>
					<?php endforeach ?>
				<?php endforeach ?>
				<tr>
					<?php foreach (range(1,9) as $value): ?>
						<td>&nbsp;</td>
					<?php endforeach ?>
				</tr>
				<tr>
					<td colspan="6" align="center">Jumlah yang dianggarkan</td><td></td><td align="center"></td><td><input type="number" class="span10" min="0" name="km_7_cost" value="<?= $reports->get_data_km7('km7_cost', $assign_id, '') ?>"></td>
				</tr>
			</table>
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td width="50%" style="border:0px">&nbsp;</td>
					<td width="50%" style="border:0px">&nbsp;</td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td align="center" style="border:0px">
						Jakarta, 
					<?
					// if($status==1)
					?>
						<input type="text" size="10" name="tanggal_kartu" id="tanggal_kartu" value="<?=$comfunc->dateIndo($reports->get_data_km7('km7_created_date', $assign_id, ''))?>">
					
					</td>
				</tr>
				<tr>
					<td align="center" style="border:0px">Ketua Tim</td>
					<td align="center" style="border:0px">Mengetahui:<br>Pengendali Teknis</td>
				</tr>
				<tr>
					<td style="border:0px"><br><br><br><br></td>
					<td style="border:0px"><br><br><br><br></td>
				</tr>
				<tr>
					<td align="center" style="border:0px"><?=$nama_katim;?></td>
					<td align="center" style="border:0px"><?=$nama_dalnis;?></td>
				</tr>
			</table>
			<fieldset>
				<center>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<input type="submit" class="blue_btn" value="Simpan">&nbsp;&nbsp;&nbsp;
					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>	
$("#tanggal_kartu").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true,
	 minDate: '<?= $comfunc->dateIndo($arr['assign_start_date']) ?>',
	 maxDate: '<?= $comfunc->dateIndo($arr['assign_end_date']) ?>',
});  
function download(idAssign){
	window.open("ReportManagement/laporan_kma_7_pdf.php?fil_id="+idAssign, "_self"); 
}
</script>