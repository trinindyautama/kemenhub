<html>
<head>
	<link rel="stylesheet" href="../css/print.css" type="text/css" media="screen" />
</head>
<body>
	<header>Formulir KMA 15</header>
	<br>
	<table border='0' class="table_ryunmi" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="6" align="center">PENGENDALIAN PENYUSUNAN LAPORAN</td>
		</tr>
		<tr>
			<td colspan="6" align="center">INFORMASI UMUM</td>
		</tr>
		<tr>
			<td width="15%" style="border-right:1px;border-top:0px;border-bottom: 0px">Nama Auditi</td>
			<td width="5%" style="border:0px" align="center">:</td>
			<td width="30%" style="border:0px">
			<?
			$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
			$assign_id_auditee = "";
			while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
				echo $arr_id_auditee ['auditee_name']."<br>";
			}
			?>
			</td>
			<td width="13%" style="border:0px">Tanggal Kartu</td>
			<td width="3%" align="center" style="border:0px" align="center">:</td>
			<td width="20%" style="border-left: 1px; border-bottom: 1px;"><?=$comfunc->dateIndo($arr_km06['km06_tanggal'])?></td>
		</tr>
		<tr>
			<td width="15%" style="border-right:1px;border-top:0px;border-bottom: 0px">Alamat</td>
			<td width="5%" style="border:0px" align="center">:</td>
			<td width="30%" style="border:0px">
			<?
			$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
			$assign_id_auditee = "";
			while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
				echo $arr_id_auditee ['auditee_alamat']."<br>";
			}
			?>
			</td>
			<td width="13%" style="border:0px">No.PKAT</td>
			<td width="3%" align="center" style="border:0px" align="center">:</td>
			<td width="20%" style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$arr_km06['km06_no_rencana']?></td>
		</tr>
		<tr>
			<td style="border-right:1px;border-top:0px;border-bottom: 0px">Telpon</td>
			<td style="border:0px" align="center">:</td>
			<td style="border:0px">
				<?
				$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
				$assign_id_auditee = "";
				while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
					echo $arr_id_auditee ['auditee_telp']."<br>";
				}
				?>
			</td>
			<td style="border:0px">No Surat Tugas</td>
			<td style="border:0px" align="center">:</td>
			<td style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$reports->get_surat_tugas_no($assign_id);?></td>
		</tr>
		<tr>
			<td style="border-right:1px;border-top:0px;border-bottom: 0px" rowspan="3">Tujuan Audit</td>
			<td style="border:0px" rowspan="3" align="center">:</td>
			<td style="border:0px" rowspan="3">
				<?=$comfunc->text_show($arr_km06['km06_tujuan'])?>
			</td>
			<td style="border:0px">RMP</td>
			<td style="border:0px" align="center">:</td>
			<td style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$comfunc->dateIndo($arr_assign['assign_end_date'])?></td>
		</tr>
		<tr>
			<td style="border:0px">RML</td>
			<td style="border:0px" align="center">:</td>
			<td style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$comfunc->dateIndo($arr_assign['assign_start_date']+1209600)?></td>
		</tr>
		<tr>
			<td style="border:0px">Ketua Tim</td>
			<td style="border:0px" align="center">:</td>
			<td style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$get_katim?></td>
		</tr>
		<tr>
			<td style="border-right:1px;border-top:0px;border-bottom: 0px">Periode yang Diaudit</td>
			<td style="border:0px" align="center">:</td>
			<td style="border:0px"><?=$arr_assign['assign_periode']?></td>
			<td style="border:0px">Pengendali Teknis</td>
			<td style="border:0px" align="center">:</td>
			<td style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$get_dalnis?></td>
		</tr>
		<tr>
			<td style="border-right:1px;border-top:0px;border-bottom: 0px">Nomor Kartu Penugasan</td>
			<td style="border:0px" align="center">:</td>
			<td style="border:0px"><?=$arr_km06['km06_no']?></td>
			<td style="border:0px">Pengendali Mutu</td>
			<td style="border:0px" align="center">:</td>
			<td style="border-left: 1px; border-bottom: 0px;border-top:0px;"><?=$get_daltu?></td>
		</tr>
	</table>
	
	<table border='0' class="table_ryunmi" cellspacing='0' cellpadding="0">
		<tr>
			<td colspan="8" align="center">TAHAPAN PENYELESAIAN</td>
		</tr>
		<tr>
			<th align="center" rowspan="2" colspan="3">Uraian</th>
			<th align="center" rowspan="2">Nama</th>
			<th align="center" colspan="4">Tanggal</th>
		</tr>
		<tr>
			<td align="center">I</td>
			<td align="center">II</td>
			<td align="center">III</td>
			<td align="center">IV</td>
		</tr>
		<tr>
			<td align="center" colspan="3">1</td>
			<td align="center">2</td>
			<td align="center">3</td>
			<td align="center">4</td>
			<td align="center">5</td>
			<td align="center">6</td>
		</tr>
		<tr>
			<td align="center" width="2%" style="border-right:0px">a.</td>
			<td style="border-left:0px" width="46%" colspan="2">Diserahkan oleh Ketua Tim kepada Pengendali Teknis</td>
			<td align="left" width="20%"><?=$get_katim?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_1'))?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_2'))?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_3'))?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_4'))?></td>
		</tr>
		<tr>
			<td align="center" width="2%" style="border-right:0px">b.</td>
			<td style="border-left:0px" colspan="2">Diserahkan oleh Pengendali Teknis kepada Pengendali Mutu</td>
			<td align="left"><?= $get_dalnis ?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_1'))?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_2'))?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_3'))?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_4'))?></td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td align="center"></td>
			<td align="center"  colspan="2">Tanggal Mulai</td>
			<td align="center"  colspan="2">Tanggal Selesai</td>
		</tr>
		<tr>
			<td align="center" style="border-right:0px">c.</td>
			<td style="border-left:0px" colspan="2">Diserahkan ke sekretariat untuk digandakan, dicopy dan dijilid</td>
			<td align="left"><?=$reports->get_data_nama_km15 ( $assign_id, '1')?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '1'))?></td>
			<td align="center"></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '1'))?></td>
			<td align="center"></td>
		</tr>
		<tr>
			<td align="center" style="border-right:0px">d.</td>
			<td style="border-left:0px" colspan="2">Diserahkan ke Pimpinan APIP</td>
			<td align="left"><?=$reports->get_data_nama_km15 ( $assign_id, '2')?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '2'))?></td>
			<td align="center"></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '2'))?></td>
			<td align="center"></td>
		</tr>
		<tr>
			<td align="center" style="border-right:0px">e.</td>
			<td style="border-left:0px" colspan="2">Diserahkan kepada Pimpinan Organisasi</td>
			<td align="left"><?=$reports->get_data_nama_km15 ( $assign_id, '3')?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '3'))?></td>
			<td align="center"></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '3'))?></td>
			<td align="center"></td>
		</tr>
		<tr>
			<td align="center" style="border-right:0px">f.</td>
			<td style="border-left:0px" colspan="2">Didistribusikan kepada:</td>
			<td align="center"></td>
			<td align="center"></td>
			<td align="center"></td>
			<td align="center"></td>
			<td align="center"></td>
		</tr>				
		<?
			$i=0;
			$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
			$assign_id_auditee = "";
			while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
				$i++;
		?>
		<tr>
			<td align="center" style="border-right:0px"></td>
			<td colspan="2" style="border-right:0px;border-left:0px"><?=$i?>. Pimpinan <?=$arr_id_auditee ['auditee_name']?></td>
			<td align="left"><?=$reports->get_data_nama_km15 ( $assign_id, '6_'.$i)?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '6_'.$i))?></td>
			<td align="center"></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '6_'.$i))?></td>
			<td align="center"></td>
		</tr>
		<?
		}
		$tambah_1 = $i+1;
		$tambah_2 = $i+2;
		?>
		<tr>
			<td align="center" style="border-right:0px"></td>
			<td style="border-right:0px;border-left:0px" colspan="2"><?= $tambah_1 ?>. Pimpinan Organisasi</td>
			<td align="left"><?=$reports->get_data_nama_km15 ( $assign_id, '6_'.$tambah_1)?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '6_'.$tambah_1))?></td>
			<td align="center"></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '6_'.$tambah_1))?></td>
			<td align="center"></td>
		</tr>
		<tr>
			<td align="center" style="border-right:0px"></td>
			<td style="border-right:0px;border-left:0px" colspan="2"><?= $tambah_2 ?>. BPK</td>
			<td align="left"><?=$reports->get_data_nama_km15 ( $assign_id, '6_'.$tambah_2)?></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '6_'.$tambah_2))?></td>
			<td align="center"></td>
			<td align="center"><?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '6_'.$tambah_2))?></td>
			<td align="center"></td>
		</tr>
	</table>
</body>
</html>