<?
include_once "_includes/classes/report_class.php";
include_once "_includes/classes/assignment_class.php";

$reports = new report ( $ses_userId );
$assigns = new assign( $ses_userId );

@$_action = $comfunc->replacetext ( $_REQUEST ["data_action"] );

if(isset($_REQUEST ["fil_id"])){
	@session_start();
	$_SESSION['ses_assign'] = $comfunc->replacetext ( $_REQUEST ["fil_id"] );
}

$assign_id = $_SESSION['ses_assign'];

$paging_request = "main_page.php?method=validate_kma_26";
$acc_page_request = "validasi_kma_26_acc.php";
$list_page_request = "report_view.php";
$view_parrent = "validasi_kma_26_parrent.php";

// ==== buat grid ===//
$num_row = 10;
@$str_page = $comfunc->replacetext ( $_GET ['page'] );
if (isset ( $str_page )) {
	if (is_numeric ( $str_page ) && $str_page != 0) {
		$noPage = $str_page;
	} else {
		$noPage = 1;
	}
} else {
	$noPage = 1;
}
$offset = ($noPage - 1) * $num_row;

$def_page_request = $paging_request . "&page=$noPage";
$back_page_request = "main_page.php?method=validasi_kma&fil_surat_tugas=".$assign_id;

$grid = "grid_km.php";
$gridHeader = array ("No Bon", "Peminjam", "File Tentang", "Tanggal", "Pengembalian");
$gridDetail = array ("1", "2", "3", "4", "5");
$gridWidth = array ("10", "15", "35", "10", "10");

$widthAksi = "15";
$iconEdit = "1";
$iconDel = "1";
$iconDetail = "1";
// === end grid ===//

switch ($_action) {
	case "getadd" :
		$_nextaction = "postadd";
		$page_request = $acc_page_request;
		$page_title = "Tambah Bon Peminjaman";
		break;
	case "getedit" :
		$_nextaction = "postedit";
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $reports->km26_data_viewlist ( $fdata_id );
		$page_title = "Ubah Bon Peminjaman";
		break;
	case "getdetail" :
		$page_request = $acc_page_request;
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$rs = $reports->km26_data_viewlist ( $fdata_id );
		$page_title = "Rincian Bon Peminjaman";
		break;
	case "postadd" :
		$fno_bon = $comfunc->replacetext ( $_POST ["no_bon"] );
		$fanggota_id = $comfunc->replacetext ( $_POST ["anggota_id"] );
		$ftanggal_pinjam = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal_pinjam"] ));
		$ftanggal_kembalikan = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal_kembalikan"] ));
		$ffile_tentang = $comfunc->replacetext ( $_POST ["file_tentang"] );
		$fno_dosier = $comfunc->replacetext ( $_POST ["no_dosier"] );
		$fno_urut = $comfunc->replacetext ( $_POST ["no_urut"] );
		$fdisetujui = $comfunc->replacetext ( $_POST ["disetujui"] );
		$ftgl_disetujui = $comfunc->date_db($comfunc->replacetext( $_POST ["tgl_disetujui"] ));
		$fpetugas = $comfunc->replacetext ( $_POST ["petugas"] );
		$ftgl_petugas = $comfunc->date_db($comfunc->replacetext( $_POST ["tgl_petugas"] ));
		if ($fno_bon != "" && $fanggota_id != "" && $ftanggal_pinjam != "" && $ftanggal_kembalikan != "" && $ffile_tentang != "" && $fdisetujui != "" && $ftgl_disetujui != "" && $fpetugas != "" && $ftgl_petugas != "") {
			$reports->km26_add ( $assign_id, $fno_bon, $fanggota_id, $ftanggal_pinjam, $ftanggal_kembalikan, $ffile_tentang, $fno_dosier, $fno_urut, $fdisetujui, $ftgl_disetujui, $fpetugas, $ftgl_petugas );
			$comfunc->js_alert_act ( 3 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	case "postedit" :
		$fdata_id = $comfunc->replacetext ( $_POST ["data_id"] );
		$fno_bon = $comfunc->replacetext ( $_POST ["no_bon"] );
		$fanggota_id = $comfunc->replacetext ( $_POST ["anggota_id"] );
		$ftanggal_pinjam = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal_pinjam"] ));
		$ftanggal_kembalikan = $comfunc->date_db($comfunc->replacetext( $_POST ["tanggal_kembalikan"] ));
		$ffile_tentang = $comfunc->replacetext ( $_POST ["file_tentang"] );
		$fno_dosier = $comfunc->replacetext ( $_POST ["no_dosier"] );
		$fno_urut = $comfunc->replacetext ( $_POST ["no_urut"] );
		$fdisetujui = $comfunc->replacetext ( $_POST ["disetujui"] );
		$ftgl_disetujui = $comfunc->date_db($comfunc->replacetext( $_POST ["tgl_disetujui"] ));
		$fpetugas = $comfunc->replacetext ( $_POST ["petugas"] );
		$ftgl_petugas = $comfunc->date_db($comfunc->replacetext( $_POST ["tgl_petugas"] ));
		if ($fno_bon != "" && $fanggota_id != "" && $ftanggal_pinjam != "" && $ftanggal_kembalikan != "" && $ffile_tentang != "" && $fdisetujui != "" && $ftgl_disetujui != "" && $fpetugas != "" && $ftgl_petugas != "") {
			$reports->km26_edit ( $fdata_id, $fno_bon, $fanggota_id, $ftanggal_pinjam, $ftanggal_kembalikan, $ffile_tentang, $fno_dosier, $fno_urut, $fdisetujui, $ftgl_disetujui, $fpetugas, $ftgl_petugas );
			$comfunc->js_alert_act ( 1 );
		} else {
			$comfunc->js_alert_act ( 5 );
		}
		?>
		<script>window.open('<?=$def_page_request?>', '_self');</script>
		<?
		$page_request = "blank.php";
		break;
	case "getdelete" :
		$fdata_id = $comfunc->replacetext ( $_REQUEST ["data_id"] );
		$reports->km26_delete ( $fdata_id );
		$comfunc->js_alert_act ( 2 );
		?>
<script>window.open('<?=$def_page_request?>', '_self');</script>
<?
		$page_request = "blank.php";
		break;
	default :
		$recordcount = $reports->km26_count ($assign_id);
		$rs = $reports->km26_view_grid ($assign_id, $offset, $num_row );
		$page_title = "Daftar Bon Peminjaman (KMA 26)";
		$page_request = $list_page_request;
		break;
}
include_once $page_request;
?>
