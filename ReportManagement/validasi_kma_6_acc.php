<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<section id="main" class="column">	
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">
				Formulir KMA 6
			</h3>	
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="6" align="center">
						<u>KARTU PENUGASAN</u><br>Nomor : <input type="text" name="km06_no" value="<?=$arr_km06['km06_no']?>">
						<input type="hidden" size="50" name="kode_tipe" value="<?=$arr['sub_type_code']?>">
						<input type="hidden" size="50" name="kode_pelaksana" value="<?=$arr['inspektorat_code']?>">
					</td>
				</tr>
				<tr>
					<td width="2%" rowspan="4" style="border: 0">1</td>
					<td width="2%" style="border: 0">a.</td>
					<td width="30%" style="border: 0">Nama Auditee</td>
					<td width="2%" style="border: 0">:</td>
					<td style="border: 0" colspan="2">
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_name']."<br>".$arr_id_auditee ['auditee_alamat']."<br>";
						}
						?>
					</td>
				</tr>
				<tr>
					<td style="border: 0">b.</td>
					<td style="border: 0">No File Permanen</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ( $arr ['assign_id'] );
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_no_file'];
						}
						?>
					</td>
				</tr>
				<tr>
					<td style="border: 0">c.</td>
					<td style="border: 0">Rencana Audit Nomor</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
						<input type="text" size="40" name="nomor_rencana" value="<?=$arr_km06['km06_no_rencana']?>">
					</td>
				</tr>
				<tr>
					<td style="border: 0">d.</td>
					<td style="border: 0">Audit Terakhir Tahun</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
						<select class="span2" name="tahun_terakhir_audit">
							<option value="" selected="selected">== Pilih ==</option>
							<?php foreach (range(2015, date('Y')) as $year): ?>
								<option value="<?= $year ?>" <?=($arr_km06['km06_tahun_terakhir'] == $year) ? "selected='selected'" : ""?>><?= $year ?></option>
							<?php endforeach ?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="border: 0">2</td>
					<td colspan="2" style="border: 0">Tingkat Risiko Unit/Aktifitas</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
						<textarea class="ckeditor" cols="4" rows="40" name="risiko" id="risiko"><?=$arr_km06['km06_resiko']?></textarea>
					</td>
				</tr>
				<tr>
					<td style="border: 0">3</td>
					<td colspan="2" style="border: 0">Tujuan Audit</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
						<textarea class="ckeditor" cols="4" rows="40" name="tujuan" id="tujuan"><?=$arr_km06['km06_tujuan']?></textarea>
					</td>
				</tr>
				<tr>
					<td style="border: 0">4</td>
					<td style="border: 0">a.</td>
					<td style="border: 0">Nama Pengendali Mutu</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
					<?
						$rs_katim = $assigns->anggota_list ( $assign_id, 'pm' );
						$arr_katim = $rs_katim->FetchRow();
						echo $arr_katim['auditor_name'];
					?>
					</td>
				</tr>
				<tr>
					<td style="border: 0"></td>
					<td style="border: 0">b.</td>
					<td style="border: 0">Nama Pengendali Teknis</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
					<?
						$rs_katim = $assigns->anggota_list ( $assign_id, 'pt' );
						$arr_katim = $rs_katim->FetchRow();
						echo $arr_katim['auditor_name'];
					?>
					</td>
				</tr>
				<tr>
					<td style="border: 0"></td>
					<td style="border: 0">c.</td>
					<td style="border: 0">Nama Ketua Tim</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
					<?
						$rs_katim = $assigns->anggota_list ( $assign_id, 'kt' );
						$arr_katim = $rs_katim->FetchRow();
						echo $arr_katim['auditor_name'];
					?>
					</td>
				</tr>
				<?
					$no_anggota=0;
					$rs_anggota = $assigns->anggota_list ( $assign_id, 'at' );
					$count_anggota = $rs_anggota->RecordCount();
					while($arr_anggota = $rs_anggota->FetchRow()){
						$no_anggota++;
				?>
				<tr>
				<?
					if($no_anggota==1){
				?>
					<td style="border: 0" rowspan="<?=$count_anggota?>"></td>
					<td style="border: 0" rowspan="<?=$count_anggota?>">d.</td>
					<td style="border: 0" rowspan="<?=$count_anggota?>">Nama Anggota Tim</td>
					<td style="border: 0" rowspan="<?=$count_anggota?>">:</td>
				<?
					}
				?>
					<td style="border: 0"><?=$no_anggota?></td>
					<td style="border: 0"><?=$arr_anggota['auditor_name']?></td>
				</tr>
				<?
					}
				?>
				<tr>
					<td rowspan="2" style="border: 0">5</td>
					<td style="border: 0">a.</td>
					<td style="border: 0">Audit Dilakukan dengan Surat Tugas Nomor</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2"><?=$reports->get_surat_tugas_no($assign_id);?></td>
				</tr>
				<tr>
					<td style="border: 0">b.</td>
					<td style="border: 0">Audit Direncanakan Mulai Tanggal dan selesai Tanggal</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2"><?=$comfunc->dateIndo($arr['assign_start_date'])?> s/d <?=$comfunc->dateIndo($arr['assign_end_date'])?></td>
				</tr>
				<tr>
					<td style="border: 0">6</td>
					<td colspan="2" style="border: 0">Anggaran yang Diajukan</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2"><input type="number" min="0" size="20" name="anggaran_ajukan" value="<?=$arr_km06['km06_anggaran_ajukan']?>"></td>
				</tr>
				<tr>
					<td style="border: 0">7</td>
					<td colspan="2" style="border: 0">Anggaran yang Disetujui</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2"><input type="number" min="0" size="20" name="anggaran_setujui" value="<?=$arr_km06['km06_anggaran_setujui']?>"></td>
				</tr>
				<tr>
					<td style="border: 0">8</td>
					<td colspan="2" style="border: 0">Catatan Penting dari Pengendali Teknis/ pengendali Mutu</td>
					<td style="border: 0">:</td>
					<td style="border: 0" colspan="2">
					<?
					// if($as_dalnis || $as_daltu){
					?>
					<textarea class="ckeditor" cols="4" rows="40" name="ket" id="ket"><?=$arr_km06['km06_ket']?></textarea>
					<?
					// }else{ 
						// echo $comfunc->text_show($arr_km06['km06_ket']); 
					?>
					<!-- <input type="hidden" name="ket" value="<?=$arr_km06['km06_ket']?>"> -->
					<?
					// }
					?>
					</td>
				</tr>
			</table>
			<table border='1' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td width="50%" style="border:0px">&nbsp;</td>
					<td width="50%" style="border:0px">&nbsp;</td>
				</tr>
				<tr>
					<td style="border:0px">&nbsp;</td>
					<td align="center" style="border:0px">
						Jakarta, 
					<?
					// if($status==1)
					?>
						<input type="text" size="10" name="tanggal_kartu" id="tanggal_kartu" value="<?=$comfunc->dateIndo($arr_km06['km06_tanggal'])?>">
					
					</td>
				</tr>
				<tr>
					<td align="center" style="border:0px">Ketua Tim</td>
					<td align="center" style="border:0px">Mengetahui:<br>Pengendali Teknis</td>
				</tr>
				<tr>
					<td style="border:0px"><br><br><br><br></td>
					<td style="border:0px"><br><br><br><br></td>
				</tr>
				<tr>
					<td align="center" style="border:0px"><?=$nama_katim;?></td>
					<td align="center" style="border:0px"><?=$nama_dalnis;?></td>
				</tr>
			</table>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="hidden" size="10" name="06_id" value="<?=$arr_km06['km06_id']?>">
					<?
					// && ($as_dalnis || $as_daltu)
					// if($status==1 ){
					// ?>
					<!-- // <input type="submit" class="blue_btn" value="Tolak" name="submit" onclick="document.getElementById('status').value=3"> -->
					<!-- // <input type="submit" class="blue_btn" value="Setujui" name="submit" onclick="document.getElementById('status').value=2"> -->
					 <?
					// $as_katim &&
					// }else if( ($status==0 || $status==3)){
						// if($arr_km06['km06_id']==""){
					?>
					<input type="submit" class="blue_btn" value="Simpan" name="submit">
					<?
						// }else{
					?>
					<!-- <input type="submit" class="blue_btn" value="Simpan dan Ajukan" name="submit" onclick="document.getElementById('status').value=1"> -->
					<?
						// }
					// }
					?>
					&nbsp;&nbsp;&nbsp;
					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
					<input type="hidden" value="" name="status" id="status">
					<input type="hidden" value="<?=$_nextaction?>" name="data_action">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>	
$("#tanggal_kartu").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true,
	 minDate: '<?= $comfunc->dateIndo($arr['assign_start_date']) ?>',
	 maxDate: '<?= $comfunc->dateIndo($arr['assign_end_date']) ?>',
});  
function download(idAssign){
	window.open("ReportManagement/laporan_kma_6_pdf.php?fil_id="+idAssign, "_self"); 
}
</script>