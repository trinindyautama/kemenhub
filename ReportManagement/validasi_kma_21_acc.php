<link rel="stylesheet" href="css/jquery-ui.css">
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<script type="text/javascript" src="js/jquery.maskMoney.js"></script>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved"><?=$page_title?></h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
		<?
		switch ($_action) {
			case "getadd" :
				?>
			<fieldset class="hr">
				<label class="span3">Tanggal Pembahasan</label> 
				<input type="text" class="span1" name="tanggal" id="tanggal">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Periode Tindak Lanjut</label> 
				<input type="text" class="span5" name="periode" id="periode">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Temuan Sebelum Pemutakhiran</label> 
				<input type="text" class="span0" name="jml_temuan_before" id="jml_temuan_before">
				<label class="span1">Nilai</label> 
				<input type="text" class="span1" name="nilai_temuan_before" id="nilai_temuan_before">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Tindak Lanjut</label> 
				<input type="text" class="span0" name="jml_tl" id="jml_tl">
				<label class="span1">Nilai</label> 
				<input type="text" class="span1" name="nilai_tl" id="nilai_tl">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Temuan Setelah Pemutakhiran</label> 
				<input type="text" class="span0" name="jml_temuan_after" id="jml_temuan_after">
				<label class="span1">Nilai</label> 
				<input type="text" class="span1" name="nilai_temuan_after" id="nilai_temuan_after">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Dihadiri Oleh</label> 
				<textarea name="daftar_hadir" id="daftar_hadir" cols="1" rows="1"></textarea>
				<span class="mandatory">*</span>
				<br>*gunakan titik koma (;) sebagai pemisah
			</fieldset>
		<?
				break;
			case "getedit" :
				$arr = $rs->FetchRow ();
				?>
			<fieldset class="hr">
				<label class="span3">Tanggal Pembahasan</label> 
				<input type="text" class="span1" name="tanggal" id="tanggal" value="<?=$comfunc->dateIndo($arr['km21_date'])?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Periode Tindak Lanjut</label> 
				<input type="text" class="span5" name="periode" id="periode" value="<?=$arr['km21_periode']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Temuan Sebelum Pemutakhiran</label> 
				<input type="text" class="span0" name="jml_temuan_before" id="jml_temuan_before" value="<?=$arr['km21_jml_temuan_before']?>">
				<label class="span1">Nilai</label> 
				<input type="text" class="span1" name="nilai_temuan_before" id="nilai_temuan_before" value="<?=$arr['km21_nilai_temuan_before']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Tindak Lanjut</label> 
				<input type="text" class="span0" name="jml_tl" id="jml_tl" value="<?=$arr['km21_jml_tl']?>">
				<label class="span1">Nilai</label> 
				<input type="text" class="span1" name="nilai_tl" id="nilai_tl" value="<?=$arr['km21_nilai_tl']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Jumlah Temuan Setelah Pemutakhiran</label> 
				<input type="text" class="span0" name="jml_temuan_after" id="jml_temuan_after" value="<?=$arr['km21_jml_temuan_after']?>">
				<label class="span1">Nilai</label> 
				<input type="text" class="span1" name="nilai_temuan_after" id="nilai_temuan_after" value="<?=$arr['km21_nilai_temuan_after']?>">
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span3">Dihadiri Oleh</label> 
				<textarea name="daftar_hadir" id="daftar_hadir" cols="1" rows="1"><?=$arr['km21_daftar_hadir']?></textarea>
				<span class="mandatory">*</span>
				<br>*gunakan titik koma (;) sebagai pemisah
			</fieldset>
			<input type="hidden" name="data_id" value="<?=$arr['km21_id']?>">	
		<?
				break;
		}
		?>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$def_page_request?>'"> 
					&nbsp;&nbsp;&nbsp; 
					<input type="submit" class="blue_btn" value="Simpan">
				</center>
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
			</fieldset>
		</form>
	</article>
</section>
<script> 
$("#nilai_temuan_before, #nilai_tl, #nilai_temuan_after").maskMoney({precision: 2});
 
$("#tanggal").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true
}); 

$(function() {
	$("#validation-form").validate({
		rules: {
			tanggal: "required",
			periode: "required",
			jml_temuan_before: "required",
			jml_tl: "required",
			jml_temuan_after: "required",
			daftar_hadir: "required"
		},
		messages: {
			tanggal: "Silahkan Pilih Tanggal",
			periode: "Silahkan Masukan Periode Temuan",
			jml_temuan_before: "Masukan Jumlah Temuan Sebelum Pemutakhiran",
			jml_tl: "Masukan Jumlah Tindak Lanjut",
			jml_temuan_after: "Masukan Jumlah Temuan Setelaj Pemutakhiran",
			daftar_hadir: "Masukan Daftar Hadir"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
</script>