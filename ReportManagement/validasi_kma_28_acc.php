<link rel="stylesheet" href="css/jquery-ui.css">
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
<article class="module width_3_quarter">
	<header>
		<h3 class="tabs_involved"><?=$page_title?></h3>
	</header>
	<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
		<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
			<tr>
				<td colspan="4" align="center" style="border:0px">SURAT PENYAMPAIAN TEMUAN</td>
			</tr>
			<tr>
				<td style="border:0px" width="10%">Kepada</td>
				<td style="border:0px" colspan="2">: <input type="text" class="cmb_risk" name="kepada" size="25" value="<?=$arr_data['km28_kepada']?>"></td>
			</tr>
			<tr>
				<td style="border:0px">Dari</td>
				<td style="border:0px" colspan="2">: <?=$get_katim?></td>
			</tr>
			<tr>
				<td style="border:0px">Perihal</td>
				<td style="border:0px" colspan="2">: <input type="text" class="cmb_risk" name="perihal" size="100" value="<?=$arr_data['km28_perihal']?>"></td>
			</tr>
			<tr>
				<td colspan="4">
					Daftar temuan ini disampaikan untuk dibahas<br><br>
					Bersama ini kami sampaikan daftar temuan yang telah dibahas dengan Saudara A, B, dan C yang bertanggung jawab dalam bidang tugasnya masing masing mereka telah menyetujui hal hal yang dimuat dalam daftar temuan. Kami mengharapkan saudara dapat mempelajarinya dengan seksama dan apabila saurada tidak berkeberatan, kami ingin membahasnya bersama pada tanggal <input type="text" class="cmb_risk" name="tanggal" id="tanggal" size="25" value="<?=$comfunc->dateIndo($arr_data['km28_tanggal'])?>"><br><br>
					Atas perhatian Saudara kami ucapkan terima kasih
				</td>
			</tr>
			<tr>
				<td style="border:0px" colspan="3">&nbsp;</td>
				<td style="border:0px" width="25%">Ketua Tim Audit</td>
			</tr>
			<tr>
				<td style="border:0px" colspan="3">&nbsp;</td>
				<td style="border:0px"><br><br><br><br><br><br><br>(<?=$get_katim?>)</td>
			</tr>
		</table>
		<fieldset>
			<center>
				<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
				<input type="submit" class="blue_btn" value="Simpan">
				<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
				<input type="hidden" name="km_id" value="<?=$arr_data['km28_id']?>">
			</center>
		</fieldset>
	</form>
</article>
</section>
<script> 
$("#tanggal").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true
});
</script>