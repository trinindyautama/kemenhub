<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<?
include_once "_includes/classes/assignment_class.php";
include_once "_includes/classes/program_audit_class.php";

$assigns = new assign ( $ses_userId );
$programaudits = new programaudit ( $ses_userId );
?>
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Filter Pelaporan Audit Program</h3>
		</header>
		<form method="post" name="f" action="main_page.php?method=laporan_program_audit" class="form-horizontal" id="validation-form">
			<fieldset class="hr">
				<label class="span2">Satuan Kerja</label>
				<?php
				$rs_auditee = $assigns->assign_auditee_viewlist();
				$arr_auditee = $rs_auditee->GetArray ();
				echo $comfunc->buildCombo ( "fil_auditee_id", $arr_auditee, 0, 1, "", "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
				<label class="span2">Tahun</label>
				<?php
				$rs_tahun = $assigns->assign_tahun_viewlist();
				$arr_tahun = $rs_tahun->GetArray ();
				echo $comfunc->buildCombo ( "fil_tahun_id", $arr_tahun, 0, 0, "", "", "", false, true, false );
				?>
				<span class="mandatory">*</span>
			</fieldset>
			<fieldset class="hr">
                <label class="span2">Tahapan</label>
                <?= $comfunc->combo_tahapan('tahapan', "") ?>
                <span class="mandatory">*</span>
            </fieldset>
            <fieldset id="rinci">
                <label class="span2">Aspek PKA</label>
                <?
                $rs_aspek  = $programaudits->get_assign_aspek("", "", "", "PKA Rinci");
                $arr_aspek = $rs_aspek->GetArray();
                echo $comfunc->buildCombo("fil_aspek_rinci", $arr_aspek, 0, 1, "", "", "", false, true, false);
                ?>
                <span class="mandatory">*</span>
            </fieldset>
            <fieldset class="hr" id="pelaksanaan">
                <label class="span2">Aspek PKA</label>
                <?
                $rs_aspek_pel = $programaudits->get_assign_aspek("", "", "", "PKA Pelaksanaan");
                $arr_aspek_pel = $rs_aspek_pel->GetArray();
                echo $comfunc->buildCombo("fil_aspek_pel", $arr_aspek_pel, 0, 1, "", "", "", false, true, false);
                ?>
                <span class="mandatory">*</span>
            </fieldset>
			<fieldset>
				<center>
					<input type="submit" class="blue_btn" value="Lihat">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>
$(function() {
	$("#validation-form").validate({
		rules: {
			fil_auditee_id: "required",
			fil_tahun_id: "required",
			tahapan: "required",
			fil_aspek_rinci: "required",
			fil_aspek_pel: "required"
		},
		messages: {
			fil_auditee_id: "Silahkan Pilih Satuan Kerja",
			fil_tahun_id: "Silahkan Pilih Tahun",
			tahapan: "Silahkan Pilih Tahapan",
			fil_aspek_rinci: "Silahkan Pilih Aspek PKA",
			fil_aspek_pel: "Silahkan Pilih Aspek PKA"
		},		
		submitHandler: function(form) {
			form.submit();
		}
	});
});
$("#pelaksanaan").hide();
$("#rinci").hide();
$("#tahapan").on("change", function() {
    var tahapan_selected = $("#tahapan option:selected").val(),
        id_aspek = $(this).val(),
        id_assign = $("#id_assign").val();
    if (tahapan_selected == "PKA Pendahuluan") {
        $("#pelaksanaan").hide();
        $("#rinci").hide();
    } else if (tahapan_selected == "PKA Pelaksanaan") {
        $("#pelaksanaan").show();
        $("#rinci").hide();
    } else if (tahapan_selected == "PKA Rinci") {
        $("#rinci").show();
        $("#pelaksanaan").hide();
    } else {
        $("#rinci").hide();
        $("#pelaksanaan").hide();
    }
});
</script>