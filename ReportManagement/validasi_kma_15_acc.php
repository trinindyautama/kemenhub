<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">
<section id="main" class="column">
	<article class="module width_3_quarter">
		<header>
			<h3 class="tabs_involved">Formulir KMA 15</h3>
		</header>
		<form method="post" name="f" action="#" class="form-horizontal" id="validation-form">
			<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="6" align="center">PENGENDALIAN PENYUSUNAN LAPORAN<br>INFORMASI UMUM</td>
				</tr>
				<tr>
					<td width="13%" style="border:0px">Nama Auditi</td>
					<td width="2%" style="border:0px">:</td>
					<td width="43%" style="border:0px">
					<?
					$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
					$assign_id_auditee = "";
					while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
						echo $arr_id_auditee ['auditee_name']."<br>";
					}
					?>
					</td>
					<td width="10%" style="border:0px">Tanggal Kartu</td>
					<td width="2%" style="border:0px">:</td>
					<td width="30%" style="border:0px"><?=$comfunc->dateIndo($arr_km06['km06_tanggal'])?></td>
				</tr>
				<tr>
					<td width="13%" style="border:0px">Alamat</td>
					<td width="2%" style="border:0px">:</td>
					<td width="43%" style="border:0px">
					<?
					$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
					$assign_id_auditee = "";
					while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
						echo $arr_id_auditee ['auditee_alamat']."<br>";
					}
					?>
					</td>
					<td width="10%" style="border:0px">No. PKAT</td>
					<td width="2%" style="border:0px">:</td>
					<td width="30%" style="border:0px">
						<input type="hidden" name="pkat_id" value="<?=$reports->get_data_km15('km15_id', $assign_id, 'pkat')?>">
						<input type="text" name="pkat" value="<?= $reports->get_data_km15('km15_nama', $assign_id, 'pkat') ?>" >
					</td>
				</tr>
				<tr>
					<td style="border:0px">Telpon</td>
					<td style="border:0px">:</td>
					<td style="border:0px">
						<?
						$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
						$assign_id_auditee = "";
						while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
							echo $arr_id_auditee ['auditee_telp']."<br>";
						}
						?>
					</td>
					<td style="border:0px">No Surat Tugas</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$reports->get_surat_tugas_no($assign_id);?>	</td>
				</tr>
				<tr>
					<td style="border:0px" rowspan="3">Tujuan Audit</td>
					<td style="border:0px" rowspan="3">:</td>
					<td style="border:0px" rowspan="3">
						<?=$comfunc->text_show($arr_km06['km06_tujuan'])?>
					</td>
					<td style="border:0px">RMP</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$comfunc->dateIndo($arr_assign['assign_end_date'])?></td>
				</tr>
				<tr>
					<td style="border:0px">RML</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$comfunc->dateIndo($arr_assign['assign_start_date']+1209600)?></td>
				</tr>
				<tr>
					<td style="border:0px">Ketua Tim</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$get_katim?></td>
				</tr>
				<tr>
					<td style="border:0px">Perioide yang Diaudit</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$arr_assign['assign_periode']?></td>
					<td style="border:0px">Pengendali Teknis</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$get_dalnis?></td>
				</tr>
				<tr>
					<td style="border:0px">Nomor Kartu Penugasan</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$arr_km06['km06_no']?></td>
					<td style="border:0px">Pengendali Mutu</td>
					<td style="border:0px">:</td>
					<td style="border:0px"><?=$get_daltu?></td>
				</tr>
			</table>
			<table border='0' class="table_risk" cellspacing='0' cellpadding="0">
				<tr>
					<td colspan="8" align="center">TAHAPAN PENYELESAIAN</td>
				</tr>
				<tr>
					<th width="40%" align="center" colspan="3" rowspan="2">Uraian</th>
					<th width="15%" align="center" rowspan="2">Nama</th>
					<th align="center" colspan="4">Tanggal</th>
				</tr>
				<tr>
					<td align="center">I</td>
					<td align="center">II</td>
					<td align="center">III</td>
					<td align="center">IV</td>
				</tr>
				<tr>
					<td align="center" colspan="3">1</td>
					<td align="center">2</td>
					<td align="center">3</td>
					<td align="center">4</td>
					<td align="center">5</td>
					<td align="center">6</td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px">a</td>
					<td style="border-left:0px" colspan="2">Diserahkan oleh Ketua Tim kepada Pengendali Teknis</td>
					<td><?=$get_katim?></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_1_1" id="tanggal_1_1" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_1'))?>" ></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_1_2" id="tanggal_1_2" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_2'))?>" ></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_1_3" id="tanggal_1_3" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_3'))?>" ></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_1_4" id="tanggal_1_4" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_1_4'))?>" ></td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px">b</td>
					<td style="border-left:0px" colspan="2">Diserahkan oleh Pengendali Teknis kepada Pengendali Mutu</td>
					<td><?= $get_dalnis ?></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_2_1" id="tanggal_2_1" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_1'))?>" ></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_2_2" id="tanggal_2_2" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_2'))?>" ></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_2_3" id="tanggal_2_3" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_3'))?>" ></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_2_4" id="tanggal_2_4" value="<?=$comfunc->dateIndo($reports->get_data_km15('km15_tanggal_mulai', $assign_id, 'tgl_2_4'))?>" ></td>
				</tr>
				<tr>
					<td style="border-left:0px" colspan="3"></td>
					<td align="center"></td>
					<td align="center" width="15%" colspan="2">Tanggal Mulai</td>
					<td align="center" width="15%" colspan="2">Tanggal Selesai</td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px">c</td>
					<td style="border-left:0px" colspan="2">Diserahkan ke sekretariat untuk digandakan, dicopy dan dijilid</td>
					<td align="center"><input type="text" class="cmb_risk" name="nama_1" size="30" value="<?=$reports->get_data_nama_km15 ( $assign_id, '1')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_mulai_1" id="tanggal_mulai_1" value="<?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '1'))?>"></td>
					<td align="center"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_selesai_1" id="tanggal_selesai_1" value="<?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '1'))?>"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px">d</td>
					<td style="border-left:0px" colspan="2">Diserahkan ke Pimpinan APIP</td>
					<td align="center"><input type="text" class="cmb_risk" name="nama_2" size="30" value="<?=$reports->get_data_nama_km15 ( $assign_id, '2')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_mulai_2" id="tanggal_mulai_2" value="<?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '2'))?>"></td>
					<td align="center"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_selesai_2" id="tanggal_selesai_2" value="<?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '2'))?>"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px">e</td>
					<td style="border-left:0px" colspan="2">Diserahkan kepada Pimpinan Organisasi</td>
					<td align="center"><input type="text" class="cmb_risk" name="nama_3" size="30" value="<?=$reports->get_data_nama_km15 ( $assign_id, '3')?>"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_mulai_3" id="tanggal_mulai_3" value="<?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '3'))?>"></td>
					<td align="center"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_selesai_3" id="tanggal_selesai_3" value="<?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '3'))?>"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px">f</td>
					<td style="border-left:0px" colspan="2">Didistribusikan kepada:</td>
					<td align="center"></td>
					<td align="center"></td>
					<td align="center"></td>
				</tr>
				<?
					$i=0;
					$rs_id_auditee = $assigns->assign_auditee_viewlist ( $assign_id );
					$assign_id_auditee = "";
					while ( $arr_id_auditee = $rs_id_auditee->FetchRow () ) {
						$i++;
				?>
					<input type="hidden" name="id_6_<?=$i?>" value="<?=$reports->get_data_id_km15 ( $assign_id, '6_'.$i)?>">
				<tr>
					<td align="center" width="2%" style="border-right:0px"></td>
					<td align="center" width="2%" style="border-right:0px;border-left:0px"><?=$i?></td>
					<td style="border-left:0px">Pimpinan <?=$arr_id_auditee ['auditee_name']?></td>
					<td align="center"><input type="text" class="cmb_risk" name="nama_6_<?=$i?>" size="30" value="<?=$reports->get_data_nama_km15 ( $assign_id, '6_'.$i)?>"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_mulai_6_<?=$i?>" id="tanggal_mulai_6_<?=$i?>" value="<?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '6_'.$i))?>"></td>
					<td align="center"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_selesai_6_<?=$i?>" id="tanggal_selesai_6_<?=$i?>" value="<?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '6_'.$i))?>"></td>
					<td align="center"></td>
				</tr>
				<?
				}
				$tambah_1 = $i+1;
				$tambah_2 = $i+2;
				?>
				
				<input type="hidden" name="id_6_<?=$tambah_1?>" value="<?=$reports->get_data_id_km15 ( $assign_id, '6_'.$tambah_1)?>">
				<input type="hidden" name="id_6_<?=$tambah_2?>" value="<?=$reports->get_data_id_km15 ( $assign_id, '6_'.$tambah_2)?>">
				<tr>
					<td align="center" width="2%" style="border-right:0px"></td>
					<td align="center" width="2%" style="border-right:0px;border-left:0px"><?=$i+1?></td>
					<td style="border-left:0px">Pimpinan Organisasi</td>
					<td align="center"><input type="text" class="cmb_risk" name="nama_6_<?=$tambah_1?>" size="30" value="<?=$reports->get_data_nama_km15 ( $assign_id, '6_'.$tambah_1)?>"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_mulai_6_<?=$tambah_1?>" id="tanggal_mulai_6_<?=$tambah_1?>" value="<?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '6_'.$tambah_1))?>"></td>
					<td align="center"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_selesai_6_<?=$tambah_1?>" id="tanggal_selesai_6_<?=$tambah_1?>" value="<?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '6_'.$tambah_1))?>"></td>
					<td align="center"></td>
				</tr>
				<tr>
					<td align="center" width="2%" style="border-right:0px"></td>
					<td align="center" width="2%" style="border-right:0px;border-left:0px"><?=$tambah_2?></td>
					<td style="border-left:0px">Arsip</td>
					<td align="center"><input type="text" class="cmb_risk" name="nama_6_<?=$tambah_2?>" size="30" value="<?=$reports->get_data_nama_km15 ( $assign_id, '6_'.$tambah_2)?>"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_mulai_6_<?=$tambah_2?>" id="tanggal_mulai_6_<?=$tambah_2?>" value="<?=$comfunc->dateIndo($reports->get_data_tgl_mulai_km15 ( $assign_id, '6_'.$tambah_2))?>"></td>
					<td align="center"></td>
					<td align="center"><input type="text" class="cmb_risk" size="10" name="tanggal_selesai_6_<?=$tambah_2?>" id="tanggal_selesai_6_<?=$tambah_2?>" value="<?=$comfunc->dateIndo($reports->get_data_tgl_selesai_km15 ( $assign_id, '6_'.$tambah_2))?>"></td>
					<td align="center"></td>
				</tr>
			</table>
			<fieldset>
				<center>
					<input type="button" class="blue_btn" value="Kembali" onclick="location='<?=$back_page_request?>'"> &nbsp;&nbsp;&nbsp;
					<input type="submit" class="blue_btn" value="Simpan">&nbsp;&nbsp;&nbsp;
					<input type="button" class="green_btn" value="download PDF" onclick="return download('<?=$assign_id?>')">
					<input type="hidden" name="data_action" id="data_action" value="<?=$_nextaction?>">
					<?php foreach (range(1,4) as $value): ?>
						<input type="hidden" name="id_1_<?=$value?>" value="<?=$reports->get_data_id_km15 ( $assign_id, 'id_1_'.$value)?>">
						<input type="hidden" name="id_2_<?=$value?>" value="<?=$reports->get_data_id_km15 ( $assign_id, 'id_2_'.$value)?>">	
					<?php endforeach ?>
					<input type="hidden" name="id_1" value="<?=$reports->get_data_id_km15 ( $assign_id, '1')?>">
					<input type="hidden" name="id_2" value="<?=$reports->get_data_id_km15 ( $assign_id, '2')?>">
					<input type="hidden" name="id_3" value="<?=$reports->get_data_id_km15 ( $assign_id, '3')?>">
				</center>
			</fieldset>
		</form>
	</article>
</section>
<script>
$("#tanggal_1_1, #tanggal_1_2, #tanggal_1_3, #tanggal_1_4, #tanggal_2_1, #tanggal_2_2, #tanggal_2_3, #tanggal_2_4, #tanggal_mulai_1, #tanggal_selesai_1, #tanggal_mulai_2, #tanggal_selesai_2, #tanggal_mulai_3, #tanggal_selesai_3, #tanggal_mulai_4, #tanggal_selesai_4, #tanggal_mulai_5, #tanggal_selesai_5, #tanggal_mulai_6_1, #tanggal_selesai_6_1, #tanggal_mulai_6_2, #tanggal_selesai_6_2, #tanggal_mulai_6_3, #tanggal_selesai_6_3, #tanggal_mulai_6_4, #tanggal_selesai_6_4, #tanggal_mulai_6_5, #tanggal_selesai_6_5, #tanggal_mulai_6_6, #tanggal_selesai_6_6, #tanggal_mulai_6_7, #tanggal_selesai_6_7, #tanggal_mulai_6_8, #tanggal_selesai_6_8, #tanggal_mulai_6_9, #tanggal_selesai_6_9, #tanggal_mulai_6_10, #tanggal_selesai_6_10").datepicker({
	dateFormat: 'dd-mm-yy',
	 nextText: "",
	 prevText: "",
	 changeYear: true,
	 changeMonth: true,
	 minDate: '<?= $comfunc->dateIndo($arr_assign['assign_start_date']) ?>',
	 maxDate: '<?= $comfunc->dateIndo($arr_assign['assign_end_date']) ?>',
});
function download(idAssign){
	window.open("ReportManagement/laporan_kma_15_pdf.php?fil_id="+idAssign); 
}
</script>