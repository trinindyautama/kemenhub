<?
session_start();
$position = 1;

include_once "_includes/classes/login_class.php";
include_once "_includes/common.php";

$comfunc = new comfunction ();
$logins = new login ();
$logins->deleteLoginsytem();
?>
<!doctype html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="css/login.css">
<title>Login | SIAU</title>
</head>
<?
$index = "index.php";
if (@$_POST ['submit'] == "Login") {
	
	$username = $comfunc->replacetext ( $_POST ['username'] );
	$passwd = $comfunc->replacetext ( $_POST ['password'] );
	if ($username != "" && $passwd != "") {
		$rsCek = 0;
		$userStatusAktif = 0;
		$cekLogin = 0;
		
		$rsCek = $logins->cek_username ( $username, $passwd );
		if (! $rsCek == "0") {
			$rs_user = $logins->data_user ( $username );
			$arr_user = $rs_user->FetchRow ();
			$userStatusAktif = $arr_user ['user_status'];
			if ($userStatusAktif == "1") {
				$cekLogin = $logins->cekLogin ( $arr_user ['user_id'] );
				if ($cekLogin == 1) {
					$date = $comfunc->date_db ( date ( "Y-m-d H:i:s" ) );
					$logins->insertStatus ( $arr_user ['user_id'], $date );
					
					$_SESSION ['ses_userId'] = $arr_user ['user_id'];
					$_SESSION ['ses_userName'] = $arr_user ['user_username'];
					$_SESSION ['ses_groupId'] = $arr_user ['user_id_group'];
					$_SESSION ['ses_id_int'] = $arr_user ['user_id_internal'];
					$_SESSION ['ses_id_eks'] = $arr_user ['user_id_ekternal'];
					echo "<script>location.href='main_page.php';</script>";
				} else {
					$pending = $logins->cekPending ( $arr_user ['user_id'] );
					echo "<script>alert('$pending'); location.href='" . $index . "';</script>";
				}
			} else {
				echo "<script>alert('Username Anda Telah Dihapus/Inactive, Silahkan Hubungi Administrator'); location.href='" . $index . "';</script>";
			}
		} else {
			$alert = "Verifikasi Username dan Password dengan benar dan coba kembali !!!";
			echo "<script>alert('$alert'); location.href='" . $index . "';</script>";
		}
	} else {
		echo "<script>alert('Username / Password tidak boleh kosong !!!'); location.href='" . $index . "';</script>";
	}
}
?>
<body>
	<div id="login">
		<h2>
			<img src="images/logo-menhub.png" width="120" />
            <img src="images/login1.png" width="350" class="title-login">
		</h2>
		<form action="#" method="POST">
			<fieldset>
				<p>
					<label for="email">Kode Pengguna</label>
				</p>
				<p>
					<input type="text" name="username" value="Kode Pengguna" onBlur="if(this.value=='')this.value='Kode Pengguna'" onFocus="if(this.value=='Kode Pengguna')this.value=''">
				</p>

				<p>
					<label for="password">Kata Sandi</label>
				</p>
				<p>
					<input type="password" name="password" value="Kata Sandi" onBlur="if(this.value=='')this.value='Kata Sandi'" onFocus="if(this.value=='Kata Sandi')this.value=''">
				</p>

				<p>
					<input type="submit" name="submit" value="Login">
				</p>

			</fieldset>

		</form>

	</div>
<!--    <div id="footer">
    Copyright PT. Trinindya Utama 2017
    </div>-->
<!--	<div style=" position: absolute; right: 0; bottom: 0; left: 0; padding: 1rem; background-color: #efefef; text-align: center; font-size:8pt;">
		* Versi 1.2 Rilis 2015, Untuk hasil terbaik gunakan Mozilla Firefox dan Google Chrome dengan resolusi 1366 x 768
	</div>-->
	<!-- end login -->

</body>
</html>