$("#pelaksanaan").hide();
$("#rinci").hide();
$("#id").prop('disabled', true);
$("#tujuan").prop('disabled', true);
$("#keterangan").prop('disabled', true);
$("#no_pka").prop('disabled', true);
$("#pka_intro_arranged_date").prop('disabled', true);
$("#pka_intro_reviewed_date").prop('disabled', true);
$("#pka_intro_approved_date").prop('disabled', true);
$("#tahapan").on("change", function() {
    var tahapan_selected = $("#tahapan option:selected").val(),
        id_aspek = $(this).val(),
        id_assign = $("#id_assign").val();
        audit_type = $("#audit_type").val();
        auditee_kode = $("#auditee_kode").val();
    if (tahapan_selected == "PKA Pendahuluan") {
        $("#pelaksanaan").hide();
        $("#rinci").hide();
        $.ajax({
            url: 'AuditManagement/ajax.php?data_action=get_introProgPendahuluan',
            type: 'POST',
            dataType: 'json',
            data: {
                id_assign: id_assign,
                audit_type: audit_type,
                auditee_kode: auditee_kode
            },
            success: function(data) {
                $("#id").val(data.id);
                // $("#pendahuluan").val(data.pendahuluan);
                $("#tujuan").val(data.tujuan);
                $("#keterangan").val(data.keterangan);
                $("#no_pka").val(data.no_pka);
                $("#pka_intro_arranged_date").val(data.pka_intro_arranged_date);
                $("#pka_intro_reviewed_date").val(data.pka_intro_reviewed_date);
                $("#pka_intro_approved_date").val(data.pka_intro_approved_date);
                $("#id").prop('disabled', false);
                $("#tujuan").prop('disabled', false);
                $("#keterangan").prop('disabled', false);
                $("#no_pka").prop('disabled', false);
                $("#pka_intro_arranged_date").prop('disabled', false);
                $("#pka_intro_reviewed_date").prop('disabled', false);
                $("#pka_intro_approved_date").prop('disabled', false);
            }
        });
    } else if (tahapan_selected == "PKA Pelaksanaan") {
        $("#pelaksanaan").show();
        $("#rinci").hide();
        $("#no_pka").val("");
        $("#tujuan").val("");
        $("#keterangan").val("");
        $("#pka_intro_arranged_date").val("");
        $("#pka_intro_reviewed_date").val("");
        $("#pka_intro_approved_date").val("");
        $("#pka_intro_arranged_date1").val("");
        $("#pka_intro_reviewed_date1").val("");
        $("#id").prop('disabled', true);
        $("#tujuan").prop('disabled', true);
        $("#keterangan").prop('disabled', true);
        $("#no_pka").prop('disabled', true);
        $("#pka_intro_arranged_date").prop('disabled', true);
        $("#pka_intro_reviewed_date").prop('disabled', true);
        $("#pka_intro_approved_date").prop('disabled', true);
        pelaksanaan();
    } else if (tahapan_selected == "PKA Rinci") {
        $("#rinci").show();
        $("#pelaksanaan").hide();
        $("#no_pka").val("");
        $("#tujuan").val("");
        $("#keterangan").val("");
        $("#pka_intro_arranged_date").val("");
        $("#pka_intro_reviewed_date").val("");
        $("#pka_intro_approved_date").val("");
        $("#pka_intro_arranged_date1").val("");
        $("#pka_intro_reviewed_date1").val("");
        $("#id").prop('disabled', true);
        $("#tujuan").prop('disabled', true);
        $("#keterangan").prop('disabled', true);
        $("#no_pka").prop('disabled', true);
        $("#pka_intro_arranged_date").prop('disabled', true);
        $("#pka_intro_reviewed_date").prop('disabled', true);
        $("#pka_intro_approved_date").prop('disabled', true);
        rinci();
    } else {
        $("#rinci").hide();
        $("#pelaksanaan").hide();
        $("#no_pka").val("");
        $("#tujuan").val("");
        $("#keterangan").val("");
        $("#pka_intro_arranged_date").val("");
        $("#pka_intro_reviewed_date").val("");
        $("#pka_intro_approved_date").val("");
        $("#pka_intro_arranged_date1").val("");
        $("#pka_intro_reviewed_date1").val("");
        $("#id").prop('disabled', true);
        $("#tujuan").prop('disabled', true);
        $("#keterangan").prop('disabled', true);
        $("#no_pka").prop('disabled', true);
        $("#pka_intro_arranged_date").prop('disabled', true);
        $("#pka_intro_reviewed_date").prop('disabled', true);
        $("#pka_intro_approved_date").prop('disabled', true);

    }
});

function pelaksanaan() {
    $("#fil_aspek_pel").on("change", function() {
        if ($(this).val() != '') {
            var id_aspek = $(this).val(),
                id_assign = $("#id_assign").val();
                audit_type = $("#audit_type").val();
                auditee_kode = $("#auditee_kode").val();
            $.ajax({
                url: 'AuditManagement/ajax.php?data_action=get_introProgPelaksanaan',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_aspek: id_aspek,
                    id_assign: id_assign,
                    audit_type: audit_type,
                    auditee_kode: auditee_kode
                },
                success: function(data) {
                    $("#id").val(data.id);
                    // $("#pendahuluan").val(data.pendahuluan);
                    $("#tujuan").val(data.tujuan);
                    $("#keterangan").val(data.keterangan);
                    $("#no_pka").val(data.no_pka);
                    $("#pka_intro_arranged_date").val(data.pka_intro_arranged_date);
                    $("#pka_intro_reviewed_date").val(data.pka_intro_reviewed_date);
                    $("#pka_intro_approved_date").val(data.pka_intro_approved_date);
                    $("#pka_intro_arranged_date1").val(data.pka_intro_arranged_date);
                    $("#pka_intro_reviewed_date1").val(data.pka_intro_reviewed_date);
                    $("#id").prop('disabled', false);
                    $("#tujuan").prop('disabled', false);
                    $("#keterangan").prop('disabled', false);
                    $("#no_pka").prop('disabled', false);
                    $("#pka_intro_arranged_date").prop('disabled', false);
                    $("#pka_intro_reviewed_date").prop('disabled', false);
                    $("#pka_intro_approved_date").prop('disabled', false);
                }
            });
        } else {
            $("#no_pka").val("");
            $("#tujuan").val("");
            $("#keterangan").val("");
            $("#pka_intro_arranged_date").val("");
            $("#pka_intro_reviewed_date").val("");
            $("#pka_intro_approved_date").val("");
            $("#pka_intro_arranged_date1").val("");
            $("#pka_intro_reviewed_date1").val("");
            $("#id").prop('disabled', true);
            $("#tujuan").prop('disabled', true);
            $("#keterangan").prop('disabled', true);
            $("#no_pka").prop('disabled', true);
            $("#pka_intro_arranged_date").prop('disabled', true);
            $("#pka_intro_reviewed_date").prop('disabled', true);
            $("#pka_intro_approved_date").prop('disabled', true);
        }
    });
}

function rinci() {
    $("#fil_aspek_rinci").on("change", function() {
        if ($(this).val() != '') {
            var id_aspek = $(this).val(),
                id_assign = $("#id_assign").val();
                audit_type = $("#audit_type").val();
                auditee_kode = $("#auditee_kode").val();
            $.ajax({
                url: 'AuditManagement/ajax.php?data_action=get_introProgRinci',
                type: 'POST',
                dataType: 'json',
                data: {
                    id_aspek: id_aspek,
                    id_assign: id_assign,
                    audit_type: audit_type,
                    auditee_kode: auditee_kode
                },
                success: function(data) {
                    $("#id").val(data.id);
                    // $("#pendahuluan").val(data.pendahuluan);
                    $("#tujuan").val(data.tujuan);
                    $("#keterangan").val(data.keterangan);
                    $("#no_pka").val(data.no_pka);
                    $("#pka_intro_arranged_date").val(data.pka_intro_arranged_date);
                    $("#pka_intro_reviewed_date").val(data.pka_intro_reviewed_date);
                    $("#pka_intro_approved_date").val(data.pka_intro_approved_date);
                    $("#pka_intro_arranged_date1").val(data.pka_intro_arranged_date);
                    $("#pka_intro_reviewed_date1").val(data.pka_intro_reviewed_date);
                    $("#id").prop('disabled', false);
                    $("#tujuan").prop('disabled', false);
                    $("#keterangan").prop('disabled', false);
                    $("#no_pka").prop('disabled', false);
                    $("#pka_intro_arranged_date").prop('disabled', false);
                    $("#pka_intro_reviewed_date").prop('disabled', false);
                    $("#pka_intro_approved_date").prop('disabled', false);
                }
            });
        } else {
            $("#no_pka").val("");
            $("#tujuan").val("");
            $("#keterangan").val("");
            $("#pka_intro_arranged_date").val("");
            $("#pka_intro_reviewed_date").val("");
            $("#pka_intro_approved_date").val("");
            $("#pka_intro_arranged_date1").val("");
            $("#pka_intro_reviewed_date1").val("");
            $("#id").prop('disabled', true);
            $("#tujuan").prop('disabled', true);
            $("#keterangan").prop('disabled', true);
            $("#no_pka").prop('disabled', true);
            $("#pka_intro_arranged_date").prop('disabled', true);
            $("#pka_intro_reviewed_date").prop('disabled', true);
            $("#pka_intro_approved_date").prop('disabled', true);
        }
    });
}
$('#simpan').click(function() {
    var data = $("#validation-form").serialize();
    $.ajax({
        url: 'AuditManagement/ajax.php?data_action=postintro',
        type: 'POST',
        data: data,
        success: function(data) {
            console.log(data);
            alert('Berhasil menyimpan Tujuan PKA!');
            location.reload();
        }
    });
});